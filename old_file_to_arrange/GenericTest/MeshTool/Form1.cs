﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace MeshTool
{
    public partial class Form1 : Form
    {
        Mesh3D mesh;

        Point[] vertices;

        public Form1()
        {
            InitializeComponent();


            Point3d[] vertices = new Point3d[4];

            vertices[0] = new Point3d(0, 0, 0);
            vertices[1] = new Point3d(2, 0, 0);
            vertices[2] = new Point3d(1, 2, 0);
            vertices[3] = new Point3d(3, 2, 0);


            int[] indices = new int[] { 0, 2, 1, 2, 3, 1 };


            mesh = new Mesh3D(vertices, indices);

            RecomputePolygon();


        }




        void RecomputePolygon()
        {
            vertices = new Point[mesh.VertsList.Count];

            for (int i=0 ;i<mesh.VertsList.Count;i++ )
            {
                vertices[i] = VertexToScreen(mesh.VertsList[i].v);
            }
        }


        Point VertexToScreen(Point3d p)
        {
            int W = this.ClientSize.Width;
            int H = this.ClientSize.Height;

            int X = (int)(p.x/ 10.0f * W) + W/3;
            int Y = (int)((10.0f-p.y) / 10.0f * H) - H/3;
            return new Point(X, Y);
        }


        protected override void OnResizeEnd(EventArgs e)
        {
            base.OnResizeEnd(e);
            RecomputePolygon();
            this.Invalidate();
        }

        protected override void OnPaint(PaintEventArgs e)
        {

            if (vertices != null && vertices.Length>0)
            {
                e.Graphics.Clear(this.BackColor);
                e.Graphics.DrawPolygon(Pens.Black, vertices);
            }
            base.OnPaint(e);

        }
    }
}
