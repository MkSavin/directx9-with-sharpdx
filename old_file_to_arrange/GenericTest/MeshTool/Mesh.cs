﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace MeshTool
{
    public struct Point3d
    {
        public float x, y, z;

        public Point3d(float x, float y, float z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }
    }


    public class Vertex
    {
        public Point3d v;
        public int idx;
        public HalfEdge he;

        public Vertex(Point3d point, int idx)
        {
            this.v = point;
            this.idx = idx;
        }
       
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return string.Format("v {0}", idx);
        }

 
    }

    public class Face
    {
        public HalfEdge he;

        public Face()
        {
            HalfEdge he0 = new HalfEdge();
            HalfEdge he1 = new HalfEdge();
            HalfEdge he2 = new HalfEdge();
            
            he0.next = he1;
            he1.next = he2;
            he2.next = he0;

            he0.face = he1.face = he2.face = this;

            he0.next = he1;
            he1.next = he2;
            he2.next = he0;

            he = he0;
        }

        public bool Contain(Vertex vertex)
        {
            HalfEdge e = he;
            do
            {
                if (e.head == vertex) return true;
                e = e.next;
            }
            while (e != he);
            
            return false;
        }


        public override string ToString()
        {
            return string.Format("f {0} {1} {2}", he != null ? he.head.idx.ToString() : "-", he.next != null ? he.next.head.idx.ToString() : "-", he.next.next != null ? he.next.next.head.idx.ToString() : "-");
        }

    }
    
    public class HalfEdge
    {
        public Vertex head;
        public Face face;
        public HalfEdge next;
        public HalfEdge opposite;


        public HalfEdge()
        {

        }

        public override string ToString()
        {
            return string.Format("e {0} {1}", head != null ? head.idx.ToString() : "-", next.head != null ? next.head.idx.ToString() : "-");
        }

        /// <summary>
        /// Generate a same hash code for two opposite halfedge
        /// </summary>
        public int GetEdgeHashCode()
        {
            int hash0 = head.GetHashCode();
            int hash1 = next.head.GetHashCode();
            return hash0 > hash1 ? hash0 + hash1 * ushort.MaxValue : hash1 + hash0 * ushort.MaxValue;
        }

    }


    public class Mesh3D
    {
        public List<Vertex> VertsList = new List<Vertex>();
        public List<Face> FaceList = new List<Face>();

        public Mesh3D(IEnumerable<Point3d> vertices, int[] indices)
        {

            int idx = 0;

            foreach (Point3d p in vertices)
            {
                VertsList.Add(new Vertex(p, idx++));
            }

            Dictionary<int, HalfEdge> edgemap = new Dictionary<int, HalfEdge>(indices.Length);

            for (int i = 0; i < indices.Length; i += 3)
            {
                int I = indices[i + 0];
                int J = indices[i + 1];
                int K = indices[i + 2];

                Face face = new Face();

                HalfEdge he0 = face.he;
                HalfEdge he1 = he0.next;
                HalfEdge he2 = he1.next;

                he0.head = VertsList[I];
                he1.head = VertsList[J];
                he2.head = VertsList[K];

                FaceList.Add(face);

                HalfEdge opp0, opp1, opp2;

                edgemap.TryGetValue(he0.GetEdgeHashCode(), out opp0);
                edgemap.TryGetValue(he1.GetEdgeHashCode(), out opp1);
                edgemap.TryGetValue(he2.GetEdgeHashCode(), out opp2);

                if (opp0 == null) edgemap.Add(he0.GetEdgeHashCode(), he0);
                else
                {
                    he0.opposite = opp0;
                    opp0.opposite = he0;
                }

                if (opp1 == null) edgemap.Add(he1.GetEdgeHashCode(), he1);
                else
                {
                    he1.opposite = opp1;
                    opp1.opposite = he1;
                }

                if (opp2 == null) edgemap.Add(he2.GetEdgeHashCode(), he2);
                else
                {
                    he2.opposite = opp2;
                    opp2.opposite = he2;
                }


            }

        }


        public bool AddFace(Vertex vnew, IEnumerable<HalfEdge> edges)
        {
            return false;
        }


        public bool AddFace(HalfEdge e0, HalfEdge e1)
        {
            //       ........
            //      /\  f   /\
            //     / e0    e1 \
            //    /    \  /    \
            //   /______\/______\

            if (e0.opposite == null && e1.opposite==null)
            {
                Face face = new Face();

                HalfEdge he0 = face.he;
                HalfEdge he1 = he0.next;
                HalfEdge he2 = he1.next;

                he0.head = e0.next.next.head;
                he1.head = e0.head;
                he2.head = e1.head;

                e0.opposite = he0;
                e1.opposite = he1;

                return true;
            }
            else
            {
                return false;
            }
        }

        public bool AddFace(HalfEdge e, Vertex vnew)
        {
            //      ......... vnew
            //     / \   f  ;
            //    /   e    ;
            //   /     \  ;
            //  /_______\;

            if (e.opposite == null)
            {
                Face face = new Face();
                
                HalfEdge he0 = face.he;
                HalfEdge he1 = he0.next;
                HalfEdge he2 = he1.next;

                he0.head = e.next.next.head;
                he1.head = e.head;
                he2.head = vnew;

                e.opposite = he0;

                FaceList.Add(face);

                vnew.he = he1;
                vnew.idx = VertsList.Count;

                VertsList.Add(vnew);

                return true;

            }
            else
            {
                return false;
            }
        }
    }
}
