﻿using Engine.Graphics;
using Engine.Maths;

using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace BoundingVolumeHierarchy
{
    public class BVHObject
    {
        public BoxAA volume;
        public float area;
        public Vector3 centroid;
        public int id;

        public Vector3 Center
        {
            get { return volume.Center; }
        }

        public override string ToString()
        {
            return "primitive " + id;
        }
    }

    public class BVHNode
    {
        public BVHNode parent, left, right;
        public BoxAA bound;
        public List<BVHObject> objects = new List<BVHObject>();
        public int level = -1;
        public int id = -1;
        public string info = "<none>";


        public BVHNode()
        {
            parent = null;
            left = null;
            right = null;
            id = -1;
        }

        public TreeNode GetTreeNode()
        {
            TreeNode node = new TreeNode();
            string str =  "";
            foreach(BVHObject obj in objects) str += obj.id + " ";
            node.Text = "ID:" + id + " = " + str + " info: " + info;

            if (left != null)
            {
                node.Nodes.Add(left.GetTreeNode());
                node.Nodes.Add(right.GetTreeNode());
            }

            return node;
        }

    }

    // TODO : optimize internal primitives sorting using one list class instad building many instances
    public class BVHTree
    {
        const int MAXOBJ = 6;
        const int MAXDEPTH = 10;

        public BVHNode root;
        public BoxAA rootvolume;
        public List<BVHObject> primitives;
        public int idcounter = 0;

        public BVHTree(IList<Vector3> vertices, IList<Face16> triangles)
        {
            Vector3[] tri = new Vector3[3];

            primitives = new List<BVHObject>(triangles.Count);
            
            BoxAA rootvolume = BoxAA.NaN;

            for (int i = 0; i < triangles.Count; i++)
            {
                BVHObject obj = new BVHObject();
                Face16 face = triangles[i];

                tri[0] = vertices[triangles[i].I];
                tri[1] = vertices[triangles[i].J];
                tri[2] = vertices[triangles[i].K];

                obj.volume = BoxAA.FromData(tri);
                obj.area = Vector3.Cross(tri[1] - tri[0], tri[2] - tri[0]).Length * 0.5f;
                obj.centroid = (tri[0] + tri[1] + tri[2]) / 3.0f;
                obj.id = i;

                primitives.Add(obj);

                rootvolume += obj.volume;
            }

            root = new BVHNode();
            root.id = idcounter = 0;
            root.objects = primitives;
            root.level = MAXDEPTH - 1;

            BuildSubTree(root);
        }

        public void BuildSubTree(BVHNode node)
        {
            Vector3 mean = Vector3.Zero;
            int N = node.objects.Count;

            node.id = idcounter++;

            // is leaf case , no more splitting
            if (N <= MAXOBJ)
            {
                node.info = "leaf node ok";
                return; 
            }
            else if (node.level == 0)
            {
                node.info = "left node reach max depth";
                return;
            }


            foreach (BVHObject obj in node.objects)
                mean += obj.Center;
            mean /= N;

            Vector3 variance = Vector3.Zero;
            foreach (BVHObject obj in node.objects)
            {
                Vector3 center = obj.Center - mean;
                center = center * center;
                variance += center;
            }
            variance /= N;

            eAxis bestaxe = eAxis.None;
            
            if (variance.x > variance.y)
            {
                bestaxe = variance.x > variance.z ? eAxis.X : eAxis.Z;
            }
            else
            {
                bestaxe = variance.y > variance.z ? eAxis.Y : eAxis.Z;
            }

            if (node.id == 49)
            {
                Console.Write("");
                //bestaxe = eAxis.Z;
            }

            node.left = new BVHNode();
            node.right = new BVHNode();
            node.left.objects= new List<BVHObject>();
            node.right.objects = new List<BVHObject>();

            node.left.parent = node.right.parent = node;
            node.left.level = node.right.level = node.level - 1;

            float splitValue = mean[bestaxe];

            foreach (BVHObject obj in node.objects)
            {
                float value = obj.Center[bestaxe];
                if (value > splitValue)
                {
                    node.left.objects.Add(obj);
                }
                else
                {
                    node.right.objects.Add(obj);
                }
            }




            // the leaf condition mean node.objects.count > 6, check unbalanced case to avoid infinite splitting and force splitting
            // rangeBalancedIndices = 1 is too small and in worst case you can optain a tree with Depth = number of primitives.
            int rangeBalancedIndices = N / 3;
            
            bool unbalanced = node.right.objects.Count < rangeBalancedIndices || node.left.objects.Count < rangeBalancedIndices;


            if (unbalanced)
            {
                node.info = "unbalanced left = " + node.left.objects.Count + " right = " + node.right.objects.Count;

                List<BVHObject> presorted = new List<BVHObject>();
                presorted.AddRange(node.right.objects);
                presorted.AddRange(node.left.objects);

                node.left.objects.Clear();
                node.right.objects.Clear();

                int splitIndex = N / 2 ;

                for (int i = 0; i < splitIndex; i++)
                    node.left.objects.Add(presorted[i]);

                for (int i = splitIndex ; i < N; i++)
                    node.right.objects.Add(presorted[i]);

            }
            else
            {
                node.info = "ok";
            }

            node.objects.Clear();
            BuildSubTree(node.right);
            BuildSubTree(node.left);
        }

    }


}
