﻿using Engine.Graphics;
using Engine.Maths;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace BoundingVolumeHierarchy
{
    public partial class Form1 : Form
    {
        /// <summary>
        /// Punto di ingresso principale dell'applicazione.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }

        public Form1()
        {
            InitializeComponent();

            Vector3[] vertices = new Vector3[]
            {
               new Vector3(-1.90735 , 8.35571, -0.28038), 
                new Vector3(-3.72009 , 7.72705, -0.28038), 
                new Vector3(-5.34668 , 6.70166, -0.28038), 
                new Vector3(-6.70166 , 5.34668, -0.28038), 
                new Vector3(-7.72705 , 3.72009, -0.28038), 
                new Vector3(-8.35571 , 1.90735, -0.28038), 
                new Vector3(-8.57544 , 0.0, -0.28038), 
                new Vector3(-8.35571 , -1.90735, -0.28038), 
                new Vector3(-7.72705 , -3.72009, -0.28038), 
                new Vector3(-6.70166 , -5.34668, -0.28038), 
                new Vector3(-5.34668 , -6.70166, -0.28038), 
                new Vector3(-3.72009 , -7.72705, -0.28038), 
                new Vector3(-1.90735 , -8.35571, -0.28038), 
                new Vector3(-5.96046e-006 , -8.57544, -0.28038), 
                new Vector3(1.90735 , -8.35571, -0.28038), 
                new Vector3(3.72009 , -7.72705, -0.28038), 
                new Vector3(5.34668 , -6.70166, -0.28038), 
                new Vector3(6.70166 , -5.34668, -0.28038), 
                new Vector3(7.72705 , -3.72009, -0.28038), 
                new Vector3(8.35571 , -1.90735, -0.28038), 
                new Vector3(8.57544 , 0.0, -0.28038), 
                new Vector3(8.35571 , 1.90735, -0.28038), 
                new Vector3(7.72705 , 3.72009, -0.28038), 
                new Vector3(6.70166 , 5.34668, -0.28038), 
                new Vector3(5.34668 , 6.70166, -0.28038), 
                new Vector3(3.72009 , 7.72705, -0.28038), 
                new Vector3(1.90735 , 8.35571, -0.28038), 
                new Vector3(0.0 , 8.57544, -0.28038), 
                new Vector3(-8.57544 , 0.0, -0.28038), 
                new Vector3(-8.35571 , 1.90735, -0.28038), 
                new Vector3(-8.35571 , 1.90735, 0.28038), 
                new Vector3(-8.57544 , 0.0, 0.28038), 
                new Vector3(-7.72705 , 3.72009, -0.28038), 
                new Vector3(-7.72705 , 3.72009, 0.28038), 
                new Vector3(-6.70166 , 5.34668, -0.28038), 
                new Vector3(-6.70166 , 5.34668, 0.28038), 
                new Vector3(-5.34668 , 6.70166, -0.28038), 
                new Vector3(-5.34668 , 6.70166, 0.28038), 
                new Vector3(-3.72009 , 7.72705, -0.28038), 
                new Vector3(-3.72009 , 7.72705, 0.28038), 
                new Vector3(-1.90735 , 8.35571, -0.28038), 
                new Vector3(-1.90735 , 8.35571, 0.28038), 
                new Vector3(0.0 , 8.57544, -0.28038), 
                new Vector3(0.0 , 8.57544, 0.28038), 
                new Vector3(1.90735 , 8.35571, -0.28038), 
                new Vector3(1.90735 , 8.35571, 0.28038), 
                new Vector3(3.72009 , 7.72705, -0.28038), 
                new Vector3(3.72009 , 7.72705, 0.28038), 
                new Vector3(5.34668 , 6.70166, -0.28038), 
                new Vector3(5.34668 , 6.70166, 0.28038), 
                new Vector3(6.70166 , 5.34668, -0.28038), 
                new Vector3(6.70166 , 5.34668, 0.28038), 
                new Vector3(7.72705 , 3.72009, -0.28038), 
                new Vector3(7.72705 , 3.72009, 0.28038), 
                new Vector3(8.35571 , 1.90735, -0.28038), 
                new Vector3(8.35571 , 1.90735, 0.28038), 
                new Vector3(8.57544 , 0.0, -0.28038), 
                new Vector3(8.57544 , 0.0, 0.28038), 
                new Vector3(8.35571 , -1.90735, -0.28038), 
                new Vector3(8.35571 , -1.90735, 0.28038), 
                new Vector3(7.72705 , -3.72009, -0.28038), 
                new Vector3(7.72705 , -3.72009, 0.28038), 
                new Vector3(6.70166 , -5.34668, -0.28038), 
                new Vector3(6.70166 , -5.34668, 0.28038), 
                new Vector3(5.34668 , -6.70166, -0.28038), 
                new Vector3(5.34668 , -6.70166, 0.28038), 
                new Vector3(3.72009 , -7.72705, -0.28038), 
                new Vector3(3.72009 , -7.72705, 0.28038), 
                new Vector3(1.90735 , -8.35571, -0.28038), 
                new Vector3(1.90735 , -8.35571, 0.28038), 
                new Vector3(-5.96046e-006 , -8.57544, -0.28038), 
                new Vector3(-5.96046e-006 , -8.57544, 0.28038), 
                new Vector3(-5.96046e-006 , -8.57544, -0.28038), 
                new Vector3(-1.90735 , -8.35571, -0.28038), 
                new Vector3(-1.90735 , -8.35571, 0.28038), 
                new Vector3(-5.96046e-006 , -8.57544, 0.28038), 
                new Vector3(-3.72009 , -7.72705, -0.28038), 
                new Vector3(-3.72009 , -7.72705, 0.28038), 
                new Vector3(-5.34668 , -6.70166, -0.28038), 
                new Vector3(-5.34668 , -6.70166, 0.28038), 
                new Vector3(-6.70166 , -5.34668, -0.28038), 
                new Vector3(-6.70166 , -5.34668, 0.28038), 
                new Vector3(-7.72705 , -3.72009, -0.28038), 
                new Vector3(-7.72705 , -3.72009, 0.28038), 
                new Vector3(-8.35571 , -1.90735, -0.28038), 
                new Vector3(-8.35571 , -1.90735, 0.28038), 
                new Vector3(1.90735 , -8.35571, 0.28038), 
                new Vector3(-5.96046e-006 , -8.57544, 0.28038), 
                new Vector3(-1.90735 , -8.35571, 0.28038), 
                new Vector3(-3.72009 , -7.72705, 0.28038), 
                new Vector3(-5.34668 , -6.70166, 0.28038), 
                new Vector3(-6.70166 , -5.34668, 0.28038), 
                new Vector3(-7.72705 , -3.72009, 0.28038), 
                new Vector3(-8.35571 , -1.90735, 0.28038), 
                new Vector3(-8.57544 , 0.0, 0.28038), 
                new Vector3(-8.35571 , 1.90735, 0.28038), 
                new Vector3(-7.72705 , 3.72009, 0.28038), 
                new Vector3(-6.70166 , 5.34668, 0.28038), 
                new Vector3(-5.34668 , 6.70166, 0.28038), 
                new Vector3(-3.72009 , 7.72705, 0.28038), 
                new Vector3(-1.90735 , 8.35571, 0.28038), 
                new Vector3(0.0 , 8.57544, 0.28038), 
                new Vector3(1.90735 , 8.35571, 0.28038), 
                new Vector3(3.72009 , 7.72705, 0.28038), 
                new Vector3(5.34668 , 6.70166, 0.28038), 
                new Vector3(6.70166 , 5.34668, 0.28038), 
                new Vector3(7.72705 , 3.72009, 0.28038), 
                new Vector3(8.35571 , 1.90735, 0.28038), 
                new Vector3(8.57544 , 0.0, 0.28038), 
                new Vector3(8.35571 , -1.90735, 0.28038), 
                new Vector3(7.72705 , -3.72009, 0.28038), 
                new Vector3(6.70166 , -5.34668, 0.28038), 
                new Vector3(5.34668 , -6.70166, 0.28038), 
                new Vector3(3.72009 , -7.72705, 0.28038)
            };

            for (int i = 0; i < vertices.Length; i++)
            {
                //ToGameCoord(ref vertices[i]);
            }
            
            Face16[] triangles = new Face16[]
            {
                new Face16(2 , 1, 0), 
                new Face16(4 , 3, 2), 
                new Face16(6 , 5, 4), 
                new Face16(6 , 4, 2), 
                new Face16(8 , 7, 6), 
                new Face16(10 , 9, 8), 
                new Face16(10 , 8, 6), 
                new Face16(12 , 11, 10), 
                new Face16(14 , 13, 12), 
                new Face16(14 , 12, 10), 
                new Face16(14 , 10, 6), 
                new Face16(16 , 15, 14), 
                new Face16(18 , 17, 16), 
                new Face16(18 , 16, 14), 
                new Face16(20 , 19, 18), 
                new Face16(22 , 21, 20), 
                new Face16(22 , 20, 18), 
                new Face16(22 , 18, 14), 
                new Face16(22 , 14, 6), 
                new Face16(22 , 6, 2), 
                new Face16(24 , 23, 22), 
                new Face16(26 , 25, 24), 
                new Face16(26 , 24, 22), 
                new Face16(26 , 22, 2), 
                new Face16(26 , 2, 0), 
                new Face16(27 , 26, 0), 
                new Face16(30 , 29, 28), 
                new Face16(28 , 31, 30), 
                new Face16(33 , 32, 29), 
                new Face16(29 , 30, 33), 
                new Face16(35 , 34, 32), 
                new Face16(32 , 33, 35), 
                new Face16(37 , 36, 34), 
                new Face16(34 , 35, 37), 
                new Face16(39 , 38, 36), 
                new Face16(36 , 37, 39), 
                new Face16(41 , 40, 38), 
                new Face16(38 , 39, 41), 
                new Face16(43 , 42, 40), 
                new Face16(40 , 41, 43), 
                new Face16(45 , 44, 42), 
                new Face16(42 , 43, 45), 
                new Face16(47 , 46, 44), 
                new Face16(44 , 45, 47), 
                new Face16(49 , 48, 46), 
                new Face16(46 , 47, 49), 
                new Face16(51 , 50, 48), 
                new Face16(48 , 49, 51), 
                new Face16(53 , 52, 50), 
                new Face16(50 , 51, 53), 
                new Face16(55 , 54, 52), 
                new Face16(52 , 53, 55), 
                new Face16(57 , 56, 54), 
                new Face16(54 , 55, 57), 
                new Face16(59 , 58, 56), 
                new Face16(56 , 57, 59), 
                new Face16(61 , 60, 58), 
                new Face16(58 , 59, 61), 
                new Face16(63 , 62, 60), 
                new Face16(60 , 61, 63), 
                new Face16(65 , 64, 62), 
                new Face16(62 , 63, 65), 
                new Face16(67 , 66, 64), 
                new Face16(64 , 65, 67), 
                new Face16(69 , 68, 66), 
                new Face16(66 , 67, 69), 
                new Face16(71 , 70, 68), 
                new Face16(68 , 69, 71), 
                new Face16(74 , 73, 72), 
                new Face16(72 , 75, 74), 
                new Face16(77 , 76, 73), 
                new Face16(73 , 74, 77), 
                new Face16(79 , 78, 76), 
                new Face16(76 , 77, 79), 
                new Face16(81 , 80, 78), 
                new Face16(78 , 79, 81), 
                new Face16(83 , 82, 80), 
                new Face16(80 , 81, 83), 
                new Face16(85 , 84, 82), 
                new Face16(82 , 83, 85), 
                new Face16(31 , 28, 84), 
                new Face16(84 , 85, 31), 
                new Face16(88 , 87, 86), 
                new Face16(90 , 89, 88), 
                new Face16(92 , 91, 90), 
                new Face16(92 , 90, 88), 
                new Face16(94 , 93, 92), 
                new Face16(96 , 95, 94), 
                new Face16(96 , 94, 92), 
                new Face16(98 , 97, 96), 
                new Face16(100 , 99, 98), 
                new Face16(100 , 98, 96), 
                new Face16(100 , 96, 92), 
                new Face16(102 , 101, 100), 
                new Face16(104 , 103, 102), 
                new Face16(104 , 102, 100), 
                new Face16(106 , 105, 104), 
                new Face16(108 , 107, 106), 
                new Face16(108 , 106, 104), 
                new Face16(108 , 104, 100), 
                new Face16(108 , 100, 92), 
                new Face16(108 , 92, 88), 
                new Face16(110 , 109, 108), 
                new Face16(112 , 111, 110), 
                new Face16(112 , 110, 108), 
                new Face16(112 , 108, 88), 
                new Face16(112 , 88, 86), 
                new Face16(113 , 112, 86)
            };

            BVHTree tree = new BVHTree(vertices, triangles);

            TreeNode node = tree.root.GetTreeNode();
            this.treeBVH.Nodes.Add(node);

            this.treeBVH.ExpandAll();
        }


        public void ToGameCoord(ref Vector3 v)
        {
            v = new Vector3(-v.x, v.z, -v.y);
        }
    }
}
