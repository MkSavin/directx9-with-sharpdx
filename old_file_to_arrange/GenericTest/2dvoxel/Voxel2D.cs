﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;

using Engine.Tools;
using Engine.Maths;
using Engine.Partitions;

namespace voxel2d
{
    public class Voxel2D
    {
        Random rnd = new Random();
        
        public VoxelTree2d voxeltree;
        public List<List<VoxelPoint>> polygons;
        public BitArray2D density;


        Bitmap density_bmp;
        Brush fullbrush = new HatchBrush(HatchStyle.DarkUpwardDiagonal, Color.FromArgb(100, 255, 0, 0), Color.FromArgb(100, 255, 255, 255));
        Brush semibrush = new HatchBrush(HatchStyle.DarkDownwardDiagonal, Color.FromArgb(100, 0, 0, 255), Color.FromArgb(100, 255, 255, 255));
        Pen bigblackline = new Pen(Color.Black, 4);
        
        /// <summary>
        /// initialize the density field, is a table of '1' or '0' bit
        /// </summary>
        /// <param name="Width"></param>
        /// <param name="Height"></param>
        public Voxel2D(Bitmap map)
        {
            density_bmp = map;
            density = GetDensityTable(map);

            // find maximum suddivision, until obtain 2x2pixel
            int size = MathsUtils.MIN(map.Width, map.Height);
            int depth = GetDepth(size);

            // number of row and colums of density grid
            size = (int)Math.Pow(2, depth - 1) + 1;

            BuildTree();
        }


        public void BuildTree()
        {
            voxeltree = new VoxelTree2d(density, new RectangleAA2(50, 50, 50, 50));
            
            /*
            VoxelInfo densitycase;
            VoxelInfo result = quadtree.RecursiveBuilder2(quadtree.depth - 1, -1, 0, 0, 0, density.Width - 1, density.Heigth - 1, quadtree.bound, out quadtree.root, out densitycase);

            quadtree.GetNodeBound(quadtree.root.child[3]);
            */

            // link leaf nodes
            List<KeyValuePair<UInt32, VoxelNode2d>> leafnodes = new List<KeyValuePair<UInt32, VoxelNode2d>>(voxeltree.leveladdress[0]);

            int LeftPos = Voxel2DData.LeftPos;
            int TopPos = Voxel2DData.TopPos;
            int RightPos = Voxel2DData.RightPos;
            int BottomPos = Voxel2DData.BottomPos;

            foreach (KeyValuePair<UInt32, VoxelNode2d> pair in leafnodes)
            {
                sbyte[,] indices = Voxel2DData.edgetable;

                VoxelNode2d current = pair.Value;
                Vector2 center = current.Center;
                Vector2 halfsize = current.HalfSize;

                int vxl = (int)current.voxelcase;

                VoxelBorder brd = current.bordercase;

                // the special map border cases
                bool hasLeft = (brd & VoxelBorder.LEFT) != 0 && indices[vxl, LeftPos] < 8;
                bool hasTop = (brd & VoxelBorder.TOP) != 0 && indices[vxl, TopPos] < 8;
                bool hasRight = (brd & VoxelBorder.RIGHT) != 0 && indices[vxl, RightPos] < 8;
                bool hasBottom = (brd & VoxelBorder.BOTTOM) != 0 && indices[vxl, BottomPos] < 8;

                // link the neighbour cells
                voxeltree.leveladdress[0].TryGetValue(voxeltree.quadtree.GetTileHash(current.tilecoord.x, current.tilecoord.y + 1), out current.Top);
                voxeltree.leveladdress[0].TryGetValue(voxeltree.quadtree.GetTileHash(current.tilecoord.x, current.tilecoord.y - 1), out current.Bottom);
                voxeltree.leveladdress[0].TryGetValue(voxeltree.quadtree.GetTileHash(current.tilecoord.x - 1, current.tilecoord.y), out current.Left);
                voxeltree.leveladdress[0].TryGetValue(voxeltree.quadtree.GetTileHash(current.tilecoord.x + 1, current.tilecoord.y), out current.Right);

                /*
                quadtree.leveladdress[0].TryGetValue(quadtree.GetVoxelNodeKey(center.coord.x, center.coord.y + 1), out center.Top);
                quadtree.leveladdress[0].TryGetValue(quadtree.GetVoxelNodeKey(center.coord.x, center.coord.y - 1), out center.Bottom);
                quadtree.leveladdress[0].TryGetValue(quadtree.GetVoxelNodeKey(center.coord.x - 1, center.coord.y), out center.Left);
                quadtree.leveladdress[0].TryGetValue(quadtree.GetVoxelNodeKey(center.coord.x + 1, center.coord.y), out center.Right);
                */
                // get a list of only necessary points to use in polygons
                VoxelVertex flag = 0;

                for (int i = 0; i < 4; i++)
                    if (indices[vxl, i] < 8)
                        flag |= (VoxelVertex)(1 << indices[vxl, i]);

                if (hasLeft)
                    for (int i = 4; i < 6; i++)
                        if (indices[vxl, i] < 8)
                            flag |= (VoxelVertex)(1 << indices[vxl, i]);
                if (hasTop)
                    for (int i = 6; i < 8; i++)
                        if (indices[vxl, i] < 8)
                            flag |= (VoxelVertex)(1 << indices[vxl, i]);
                if (hasRight)
                    for (int i = 8; i < 10; i++)
                        if (indices[vxl, i] < 8)
                            flag |= (VoxelVertex)(1 << indices[vxl, i]);
                if (hasBottom)
                    for (int i = 10; i < 12; i++)
                        if (indices[vxl, i] < 8)
                            flag |= (VoxelVertex)(1 << indices[vxl, i]);

                // link found vertices with neightbours and initializate the class if not already done to generate a reference
                if (current.Top != null)
                {
                    current.v[1] = current.Top.v[3];
                    current.v[6] = current.Top.v[4];
                    current.v[7] = current.Top.v[5];
                    if (current.v[1] == null && (flag & VoxelVertex.V1) != 0) current.Top.v[3] = current.v[1] = new VoxelPoint(center + halfsize * Voxel2DData.verts[1]);
                    if (current.v[6] == null && (flag & VoxelVertex.V6) != 0) current.Top.v[4] = current.v[6] = new VoxelPoint(center + halfsize * Voxel2DData.verts[6]);
                    if (current.v[7] == null && (flag & VoxelVertex.V7) != 0) current.Top.v[5] = current.v[7] = new VoxelPoint(center + halfsize * Voxel2DData.verts[7]);
                }
                if (current.Bottom != null)
                {
                    current.v[3] = current.Bottom.v[1];
                    current.v[4] = current.Bottom.v[6];
                    current.v[5] = current.Bottom.v[7];
                    if (current.v[3] == null && (flag & VoxelVertex.V3) != 0) current.Bottom.v[1] = current.v[3] = new VoxelPoint(center + halfsize * Voxel2DData.verts[3]);
                    if (current.v[4] == null && (flag & VoxelVertex.V4) != 0) current.Bottom.v[6] = current.v[4] = new VoxelPoint(center + halfsize * Voxel2DData.verts[4]);
                    if (current.v[5] == null && (flag & VoxelVertex.V5) != 0) current.Bottom.v[7] = current.v[5] = new VoxelPoint(center + halfsize * Voxel2DData.verts[5]);
                }
                if (current.Left != null)
                {
                    current.v[0] = current.Left.v[2];
                    current.v[6] = current.Left.v[7];
                    current.v[4] = current.Left.v[5];
                    if (current.v[0] == null && (flag & VoxelVertex.V0) != 0) current.Left.v[2] = current.v[0] = new VoxelPoint(center + halfsize * Voxel2DData.verts[0]);
                    if (current.v[6] == null && (flag & VoxelVertex.V6) != 0) current.Left.v[7] = current.v[6] = new VoxelPoint(center + halfsize * Voxel2DData.verts[6]);
                    if (current.v[4] == null && (flag & VoxelVertex.V4) != 0) current.Left.v[5] = current.v[4] = new VoxelPoint(center + halfsize * Voxel2DData.verts[7]);
                }
                if (current.Right != null)
                {
                    current.v[2] = current.Right.v[0];
                    current.v[7] = current.Right.v[6];
                    current.v[5] = current.Right.v[4];
                    if (current.v[2] == null && (flag & VoxelVertex.V2) != 0) current.Right.v[0] = current.v[2] = new VoxelPoint(center + halfsize * Voxel2DData.verts[2]);
                    if (current.v[5] == null && (flag & VoxelVertex.V5) != 0) current.Right.v[6] = current.v[5] = new VoxelPoint(center + halfsize * Voxel2DData.verts[5]);
                    if (current.v[7] == null && (flag & VoxelVertex.V7) != 0) current.Right.v[4] = current.v[7] = new VoxelPoint(center + halfsize * Voxel2DData.verts[7]);
                }

                // link the default voxel lines
                if (indices[vxl, 0] < 8) makeEdge(current, indices[vxl, 0], indices[vxl, 1]);
                if (indices[vxl, 2] < 8) makeEdge(current, indices[vxl, 2], indices[vxl, 3]);

                // link the two border vertices
                if (hasLeft)
                {
                    makeEdge(current, indices[vxl, LeftPos], indices[vxl, LeftPos + 1]);
                }
                if (hasTop)
                {
                    makeEdge(current, indices[vxl, TopPos], indices[vxl, TopPos + 1]);
                }
                if (hasRight)
                {
                    makeEdge(current, indices[vxl, RightPos], indices[vxl, RightPos + 1]);
                }
                if (hasBottom)
                {
                    makeEdge(current, indices[vxl, BottomPos], indices[vxl, BottomPos + 1]);
                }
            }

            // Now all polygons have a continuos chain of vertices. Some vertices are created in neigbourg node linking but not used,
            // so check if 'next' value is not null.
            // Extract them.
            polygons = new List<List<VoxelPoint>>();

            int count = 0;
            for (int i = 0; i < leafnodes.Count; i++)
            {
                VoxelNode2d node = leafnodes[i].Value;

                for (int v = 0; v < 8; v++)
                {
                    VoxelPoint start = node.v[v];

                    if (start != null && !start.processed && start.next != null)
                    {
                        List<VoxelPoint> poly = new List<VoxelPoint>();
                        VoxelPoint curr = start;
                        do
                        {
                            if (curr.processed) throw new Exception("possible intersection ?");
                            curr.processed = true;
                            poly.Add(curr);
                            curr = curr.next;
                        }
                        while (curr != null && curr != start);
                        //poly.Add(start);

                        count += poly.Count;
                        polygons.Add(poly);
                    }
                }
            }

            Console.WriteLine("Point Instances Created : " + VoxelPoint.idcounter);
            Console.WriteLine("Point Instances Needed : " + count);
        }

        void makeEdge(VoxelNode2d node, int curr, int next)
        {
            // for borders case is possible that some points are not initialized because there aren't a neighbours
            if (node.v[curr] == null) node.v[curr] = new VoxelPoint(node.Center + node.HalfSize * Voxel2DData.verts[curr]);
            if (node.v[next] == null) node.v[next] = new VoxelPoint(node.Center + node.HalfSize * Voxel2DData.verts[next]);
            node.v[curr].next = node.v[next];
        }


        public BitArray2D GetDensityTable(Bitmap map)
        {
            BitArray2D densitymap = new BitArray2D(map.Width, map.Height);

            int w = map.Width;
            int h = map.Height;

            for (int y = 0; y < h; y++)
                for (int x = 0; x < w; x++)
                {
                    Color p = map.GetPixel(x, h - y - 1);

                    //Console.WriteLine(string.Format("get {0} {1} {2}", x, y,p));

                    if (p.GetBrightness() < 0.5f)
                    {
                        //Console.WriteLine(string.Format("Set {0} {1}",x,y));
                        densitymap[x, y] = true;
                    }
                }
            return densitymap;
        }


        /// <summary>
        /// </summary>
        /// <remarks>
        /// S = min(W,H)
        /// D0 : s 0 1,
        /// D1 : s 2,
        /// D2 : s 3 4,
        /// D3 : s 5 6 7 8,
        /// D4 : s 9...
        /// </remarks>
        public int GetDepth(int size)
        {
            int d = 0;
            while (size > 1) { size = (size + 1) / 2; d++; }
            return d;
        }


        int X0, Y0, X1, Y1;
        float deltax, deltay;
        float xmin, ymin;

        int X2Screen(float x)
        {
            return (int)(X0 + (x - xmin) / deltax);
        }
        int Y2Screen(float y)
        {
            return (int)(Y1 - (y - ymin) / deltay);
        }

        void SetBounds(System.Drawing.Rectangle size)
        {
            X0 = size.X;
            Y0 = size.Y;
            X1 = X0 + size.Width;
            Y1 = Y0 + size.Height;
            deltax = (voxeltree.quadtree.root.HalfSize.x * 2) / (X1 - X0);
            deltay = (voxeltree.quadtree.root.HalfSize.y * 2) / (Y1 - Y0);
            xmin = voxeltree.quadtree.root.Min.x;
            ymin = voxeltree.quadtree.root.Min.y;
        }

        Point[][] PolygonByCase(VoxelNode2d node)
        {
            int idx = (int)(node.voxelcase);

            byte[][] indices = Voxel2DData.indices_old[idx];
            Point[][] list = new Point[indices.Length][];

            for (int i = 0; i < indices.Length; i++)
            {
                Point[] poly = new Point[indices[i].Length];

                for (int j = 0; j < indices[i].Length; j++)
                {
                    Vector2 coord = Voxel2DData.points_old[indices[i][j]];
                    float x = node.Min.x + (coord.x + 1) * (node.HalfSize.x);
                    float y = node.Min.y + (coord.y + 1) * (node.HalfSize.y);
                    poly[j] = new Point(X2Screen(x), Y2Screen(y));
                }
                list[i] = poly;
            }

            return list;
        }


        public void DrawPolygon(Graphics g, System.Drawing.Rectangle size)
        {
            System.Drawing.Rectangle source = new System.Drawing.Rectangle(0, 0, density_bmp.Width, density_bmp.Height);

            SetBounds(size);
            SetBackground(size);

            g.InterpolationMode = InterpolationMode.NearestNeighbor;
            g.DrawImage(density_bmp, size);

            QuadNodesEnumerator<VoxelNode2d> collector = new QuadNodesEnumerator<VoxelNode2d>(voxeltree.quadtree.root);

            foreach (VoxelNode2d node in collector)
            {
                int x0 = X2Screen(node.Min.x);
                int x1 = X2Screen(node.Max.x);
                int y0 = Y2Screen(node.Min.y);
                int y1 = Y2Screen(node.Max.y);

                if (node.IsLeaf)
                {
                    Point[][] polygons = PolygonByCase(node);

                    //if (node.voxelcase == QuadIdx.FULL) throw new Exception("A full density node must not exist");
                    if (node.voxelcase == QuadIndex.None) throw new Exception("A empty density node must not exist");

                    g.DrawString(node.nodeid.ToString(), SystemFonts.DefaultFont, Brushes.Black, (x0 + x1) / 2, (y0 + y1) / 2);
                    foreach (Point[] poly in polygons)
                        g.FillPolygon(semibrush, poly);

                    g.DrawRectangle(Pens.Blue, x0, y1, x1 - x0, y0 - y1);
                }
                else
                {
                    g.DrawRectangle(Pens.Gray, x0, y1, x1 - x0, y0 - y1);
                }

            }
        }

        public void DrawSimple(Graphics g, System.Drawing.Rectangle size)
        {
            System.Drawing.Rectangle source = new System.Drawing.Rectangle(0, 0, density_bmp.Width, density_bmp.Height);

            SetBounds(size);
            SetBackground(size);

            g.InterpolationMode = InterpolationMode.NearestNeighbor;
            g.DrawImage(density_bmp, size);

            QuadNodesEnumerator<VoxelNode2d> collector = new QuadNodesEnumerator<VoxelNode2d>(voxeltree.quadtree.root);
            
            foreach (VoxelNode2d node in collector)
            {
                int x0 = X2Screen(node.Min.x);
                int x1 = X2Screen(node.Max.x);
                int y0 = Y2Screen(node.Min.y);
                int y1 = Y2Screen(node.Max.y);

                if (node.IsLeaf)
                {
                    //g.DrawString('n' + node.nodeid.ToString(), SystemFonts.DefaultFont, Brushes.Black, (x0 + x1) / 2, (y0 + y1) / 2);
                    //g.DrawRectangle(Pens.Blue, x0, y1, x1 - x0, y0 - y1);
                }
                else
                {
                    //g.DrawRectangle(Pens.Gray, x0, y1, x1 - x0, y0 - y1);
                }

            }



            foreach (List<VoxelPoint> poly in polygons)
            {
                Point[] list = new Point[poly.Count];
                Pen bigline = new Pen(Color.FromArgb(rnd.Next(25,255), rnd.Next(25,255), rnd.Next(25,255)), 4);
                Pen smalline = new Pen(bigline.Color, 2);

                for (int i = 0; i < list.Length; i++)
                {
                    Vector2 v = poly[i].value;
                    int x = X2Screen(v.x);
                    int y = Y2Screen(v.y);
                    list[i] = new Point(x, y);

                    x += rnd.Next() % 8;
                    y += rnd.Next() % 8;

                    //g.DrawString("P" + poly[i].ID.ToString(), SystemFonts.DefaultFont, Brushes.Black, x, y);
                }

                g.DrawLines(bigline, list);
                g.DrawLines(smalline, new Point[] { list[list.Length - 1], list[0] });
            }


        }


        public void SetBackground(System.Drawing.Rectangle size)
        {
            int W = size.Width;
            int H = size.Height;
            int w = density.Width;
            int h = density.Heigth;

            float pw = (float)size.Width / (density.Width - 1);
            float ph = (float)size.Height / (density.Heigth - 1);
            int semiw = (int)(pw / 2);
            int semih = (int)(ph / 2);


            density_bmp = new Bitmap(size.Width, size.Height);

            Graphics g = Graphics.FromImage(density_bmp);

            for (int y = 0; y < h; y++)
                g.DrawLine(Pens.LightGray, 0, y * ph - semih, W - 1, y * ph - semih);
            for (int x = 0; x < w; x++)
                g.DrawLine(Pens.LightGray, x * pw - semiw, 0, x * pw - semiw, H - 1);

            SolidBrush bush = new SolidBrush(Color.FromArgb(50, 255, 0, 0));

            for (int y = 0; y < h; y++)
                for (int x = 0; x < w; x++)
                {
                    if (density[x, y])
                    {
                        float xi = x * pw - semiw;
                        float yi = (h - y - 1) * ph - semih;

                        g.FillRectangle(bush, xi, yi, pw, ph);
                        //g.FillEllipse(bush, xi + semiw*0.5f, yi + semih*0.5f, semiw, semih);
                    }
                }

        }

        int clamp(float value, int max)
        {
            return value < 0 ? 0 : value > max ? max : (int)value;
        }


        public List<Point[]> GetPolygons(VoxelTree2d tree)
        {
            List<Point[]> polygons = new List<Point[]>();


            QuadNodesEnumerator<VoxelNode2d> collector = new QuadNodesEnumerator<VoxelNode2d>(voxeltree.quadtree.root);

            foreach (VoxelNode2d node in collector)
            {
                int x0 = X2Screen(node.Min.x);
                int x1 = X2Screen(node.Max.x);
                int y0 = Y2Screen(node.Min.y);
                int y1 = Y2Screen(node.Max.y);

                if (node.child == null)
                {
                    if (node.voxelcase == QuadIndex.All) throw new Exception("A full density node must not exist");
                    if (node.voxelcase == QuadIndex.None) throw new Exception("A empty density node must not exist");
                }

            }

            return polygons;
        }
    }


    public static class Voxel2DData
    {
        //   2------3
        //   |      |
        //   |      |
        //   0------1
        //
        //   v6--v1--v7
        //   |        |
        //   v0      v2
        //   |        |
        //   v4--v3--v5

        public static byte[][][] indices_old;

        public static Vector2[] points_old = new Vector2[]
        {
            new Vector2(-1,-1),
            new Vector2(-1, 0),
            new Vector2(-1, 1),
            new Vector2( 0, 1),
            new Vector2( 1, 1),
            new Vector2( 1, 0),
            new Vector2( 1,-1),
            new Vector2( 0,-1)
        };

        /// <summary>
        /// index -1 = none
        /// [0,1,2,3] are the standard voxel case line
        /// [4,5,6,7,8,9,10,11] are respectivly the left,top,right,bottom pair for border case
        /// 
        /// IMPORTANT, all edge or pair of indices, are oriented in direction of density
        /// </summary>
        public static sbyte[,] edgetable;
        public const int LeftPos = 4;
        public const int TopPos = 6;
        public const int RightPos = 8;
        public const int BottomPos = 10;


        public static Vector2[] verts = new Vector2[] 
        { 
            // middle edge points
            -Vector2.UnitX, //v0
            Vector2.UnitY,  //v1
            Vector2.UnitX,  //v2
            -Vector2.UnitY, //v3
            // corners
            new Vector2(-1,-1),//p0
            new Vector2(1,-1), //p1
            new Vector2(-1,1), //p2
            new Vector2(1,1),  //p3
        };

        static Voxel2DData()
        {
            indices_old = new byte[16][][];
            indices_old[0] = new byte[][] { };
            indices_old[1] = new byte[][] { new byte[] { 0, 1, 7 } };
            indices_old[2] = new byte[][] { new byte[] { 5, 6, 7 } };
            indices_old[3] = new byte[][] { new byte[] { 0, 1, 5, 6 } };
            indices_old[4] = new byte[][] { new byte[] { 1, 2, 3 } };
            indices_old[5] = new byte[][] { new byte[] { 7, 0, 2, 3 } };
            indices_old[6] = new byte[][] { new byte[] { 1, 2, 3 }, new byte[] { 5, 6, 7 } };
            indices_old[7] = new byte[][] { new byte[] { 0, 1, 2, 3, 5, 6, 7 } };
            indices_old[8] = new byte[][] { new byte[] { 3, 4, 5 } };
            indices_old[9] = new byte[][] { new byte[] { 0, 1, 7 }, new byte[] { 3, 4, 5 } };
            indices_old[10] = new byte[][] { new byte[] { 3, 4, 6, 7 } };
            indices_old[11] = new byte[][] { new byte[] { 0, 1, 3, 4, 6 } };
            indices_old[12] = new byte[][] { new byte[] { 1, 2, 4, 5 } };
            indices_old[13] = new byte[][] { new byte[] { 0, 2, 4, 5, 7 } };
            indices_old[14] = new byte[][] { new byte[] { 1, 2, 4, 6, 7 } };
            indices_old[15] = new byte[][] { new byte[] { 0, 2, 4, 6 } };

            edgetable = new sbyte[,] 
            { 
                { 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8 }, 
                { 0, 3, 8, 8, 4, 0, 8, 8, 8, 8, 3, 4 },
                { 3, 2, 8, 8, 8, 8, 8, 8, 2, 5, 5, 3 }, 
                { 0, 2, 8, 8, 4, 0, 8, 8, 2, 5, 5, 4 }, 
                { 1, 0, 8, 8, 0, 6, 6, 1, 8, 8, 8, 8 }, 
                { 1, 3, 8, 8, 4, 6, 6, 1, 8, 8, 3, 4 }, 
                { 3, 2, 1, 0, 0, 6, 6, 1, 2, 5, 5, 3 }, 
                { 1, 2, 8, 8, 4, 6, 6, 1, 2, 5, 5, 4 }, 
                { 2, 1, 8, 8, 8, 8, 1, 7, 7, 2, 8, 8 },
                { 0, 3, 2, 1, 4, 0, 1, 7, 7, 2, 3, 4 },
                { 3, 1, 8, 8, 8, 8, 1, 7, 7, 5, 5, 3 },
                { 0, 1, 8, 8, 4, 0, 1, 7, 7, 5, 5, 4 }, 
                { 2, 0, 8, 8, 0, 6, 6, 7, 7, 2, 8, 8 }, 
                { 2, 3, 8, 8, 4, 6, 6, 7, 7, 2, 3, 4 }, 
                { 3, 0, 8, 8, 0, 6, 6, 7, 7, 5, 5, 3 }, 
                { 8, 8, 8, 8, 4, 6, 6, 7, 7, 5, 5, 4 } 
            };
        }
    }
}
