﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Text;
using System.Windows.Forms;

using Engine.Maths;

namespace GraphControl
{
    public partial class Form1 : Form
    {
        Matrix matrix, matrix3;
        PointF[] bound;
        Pen pen;



        Matrix Make(RectangleF coord,PointF TopLeft, PointF TopRight, PointF BottomLeft)
        {
            float m11 = (TopRight.X - TopLeft.X) / coord.Width;
            float m21 = (BottomLeft.X - TopLeft.X) / coord.Height;
            float dx = TopLeft.X - m11 * coord.X - m21 * coord.Y;

            float m12 = (TopRight.Y - TopLeft.Y) / coord.Width;
            float m22 = (BottomLeft.Y - TopLeft.Y) / coord.Height;
            float dy = TopLeft.Y - m12 * coord.X - m22 * coord.Y;

            return new Matrix(m11, m12, m21, m22, dx, dy);
        }


        public Form1()
        {
            InitializeComponent();


            Vector2 min = new Vector2(-1, -2);
Vector2 max = new Vector2(10, 5);


            RectangleF coord = new RectangleF(-1, 5, 11, -7);
            PointF[] corner = new PointF[3];
            corner[0] = new PointF(1, 1);
            corner[1] = new PointF(201, 1);
            corner[2] = new PointF(1, 301);

            Rectangle graphic = new Rectangle(1,1,200,300);

            matrix = new Matrix(coord, corner);

            bound = new PointF[5];
            bound[0] = new PointF(-1, 5);
            bound[1] = new PointF(10, 5);
            bound[2] = new PointF(10, -2);
            bound[3] = new PointF(-1, -2);
            bound[4] = bound[0];

            pen = new Pen(Color.Black, 1.0f/100.0f);


            Matrix matrix2 = Make(coord, corner[0], corner[1], corner[2]);

            matrix3 = new Matrix();

            UtilsMath.MakeScreenTransform(ref min, ref max, ref graphic, ref matrix3);


            //matrix.TransformPoints(bound);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            e.Graphics.Transform = matrix3;
            e.Graphics.DrawPolygon(pen, bound);
        }

        public static void MakeScreenTransform(ref Vector2 MinCoord, ref Vector2 MaxCoord, ref Rectangle GraphicRect, ref Matrix CoordToScreen)
        {
            // the custom graph coordinate bound, where Y will be inverted to match with windows coordinate system
            float CoordX = MinCoord.x;
            float CoordY = MaxCoord.y;
            float Width = MaxCoord.x - MinCoord.x;
            float Height = MinCoord.y - MaxCoord.y;

            // the three screen coorners, notice that matrix will be not Sheared because is a rectangle
            float TopLeftX = GraphicRect.X;
            float TopLeftY = GraphicRect.Y;

            float TopRightX = GraphicRect.X + GraphicRect.Width;
            float TopRightY = GraphicRect.Y;

            float BottomLeftX = GraphicRect.X;
            float BottomLeftY = GraphicRect.Y + GraphicRect.Height;


            float m11 = (TopRightX - TopLeftX) / Width;
            float m21 = (BottomLeftX - TopLeftX) / Height;
            float dx = TopLeftX - m11 * CoordX - m21 * CoordY;

            float m12 = (TopRightY - TopLeftY) / Width;
            float m22 = (BottomLeftY - TopLeftY) / Height;
            float dy = TopLeftY - m12 * CoordX - m22 * CoordY;


            CoordToScreen.Reset();
            CoordToScreen.Shear(m12, m21, MatrixOrder.Append);
            CoordToScreen.Scale(m11, m22, MatrixOrder.Append);
            CoordToScreen.Translate(dx, dy, MatrixOrder.Append);
        }

    }
}
