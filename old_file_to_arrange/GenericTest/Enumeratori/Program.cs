﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace Enumeratori
{
    public class Program
    {
        public struct Info
        {
            public int count;
            public double mem;
             public long    time;

            public void Add(Info sum)
            {
                this.count = sum.count;
                this.mem += sum.mem;
                this.time += sum.time;
            }
        }

        public static Info RecursiveImpl(Tree tree)
        {
            int count = 0;
            Stopwatch timer = new Stopwatch();
            GC.Collect();
            GC.WaitForPendingFinalizers();
            long m0 = GC.GetTotalMemory(false);
            timer.Start();
            foreach (Node node in tree.NodeList_recursivenum)
            {
                //for (int i = 0; i < tree.root.level - node.level; i++) Console.Write("  ");
                //Console.WriteLine(node.ToString());
                count++;
            }
            timer.Stop();
            long m1 = GC.GetTotalMemory(false);
            GC.Collect();
            GC.WaitForPendingFinalizers();           
            
           // Console.WriteLine(string.Format("RECENUM nodes : {0} time : {1} mem : {2}", count , timer.ElapsedTicks,(m1-m0)/1024.0/1024));

            return new Info { count = count, mem = (m1 - m0) / 1024.0, time = timer.ElapsedTicks };
        }

        public static Info StackImpl(Tree tree)
        {
            int count = 0;
            Stopwatch timer = new Stopwatch();
            GC.Collect();
            GC.WaitForPendingFinalizers();
            long m0 = GC.GetTotalMemory(false);
            timer.Reset();
            timer.Start();
            foreach (Node node in tree.NodeList_stack)
            {
                //for (int i = 0; i < tree.root.level - node.level; i++) Console.Write("  ");
                //Console.WriteLine(node.ToString());
                count++;
            }
            timer.Stop();
            long m1 = GC.GetTotalMemory(false);
            GC.Collect();
            GC.WaitForPendingFinalizers();

            //Console.WriteLine(string.Format("NOTRECUR nodes : {0} time : {1} mem : {2}", count, timer.ElapsedTicks, (m1 - m0) / 1024.0 / 1024));

            return new Info { count = count, mem = (m1 - m0) / 1024.0, time = timer.ElapsedTicks };
        }

        /// <summary>
        /// Too many memory for big tree
        /// </summary>
        public static Info RecursiveImpl(Node root)
        {
            int count = 0;
            Stopwatch timer = new Stopwatch();
            GC.Collect();
            GC.WaitForPendingFinalizers();
            long m0 = GC.GetTotalMemory(false);
            timer.Start();
            foreach (Node node in root.NodeList_recursivenum)
            {
                //for (int i = 0; i < tree.root.level - node.level; i++) Console.Write("  ");
                //Console.WriteLine(node.ToString());
                count++;
            }
            timer.Stop();
            long m1 = GC.GetTotalMemory(false);
            GC.Collect();
            GC.WaitForPendingFinalizers();

            // Console.WriteLine(string.Format("RECENUM nodes : {0} time : {1} mem : {2}", count , timer.ElapsedTicks,(m1-m0)/1024.0/1024));

            return new Info { count = count, mem = (m1 - m0) / 1024.0, time = timer.ElapsedTicks };
        }

        /// <summary>
        /// little slow than StackEtorImpl
        /// </summary>
        public static Info StackImpl(Node root)
        {
            int count = 0;
            Stopwatch timer = new Stopwatch();
            GC.Collect();
            GC.WaitForPendingFinalizers();
            long m0 = GC.GetTotalMemory(false);
            timer.Reset();
            timer.Start();
            foreach (Node node in root.NodeList_stack)
            {
                //for (int i = 0; i < tree.root.level - node.level; i++) Console.Write("  ");
                //Console.WriteLine(node.ToString());
                count++;
            }
            timer.Stop();
            long m1 = GC.GetTotalMemory(false);
            GC.Collect();
            GC.WaitForPendingFinalizers();

            //Console.WriteLine(string.Format("NOTRECUR nodes : {0} time : {1} mem : {2}", count, timer.ElapsedTicks, (m1 - m0) / 1024.0 / 1024));

            return new Info { count = count, mem = (m1 - m0) / 1024.0, time = timer.ElapsedTicks };
        }

        /// <summary>
        /// fast because no yield function, new() slow a little
        /// </summary>
        public static Info StackEtorImpl(Node root)
        {
            int count = 0;
            Stopwatch timer = new Stopwatch();
            GC.Collect();
            GC.WaitForPendingFinalizers();
            long m0 = GC.GetTotalMemory(false);
            timer.Reset();
            timer.Start();

            NodeEnumerator enumerator = new NodeEnumerator(root);
            while (enumerator.MoveNext())
            {
                Node node = enumerator.Current;
                count++;
                //for (int i = 0; i < root.level - node.level; i++) Console.Write("  ");
                //Console.WriteLine(node.ToString());
            }
            timer.Stop();
            long m1 = GC.GetTotalMemory(false);
            GC.Collect();
            GC.WaitForPendingFinalizers();

            //Console.WriteLine(string.Format("NOTRECUR nodes : {0} time : {1} mem : {2}", count, timer.ElapsedTicks, (m1 - m0) / 1024.0 / 1024));

            return new Info { count = count, mem = (m1 - m0) / 1024.0, time = timer.ElapsedTicks };
        }
        /// <summary>
        /// yield function
        /// </summary>
        public static Info StackEtorYieldImpl(Node root)
        {
            int count = 0;
            Stopwatch timer = new Stopwatch();
            GC.Collect();
            GC.WaitForPendingFinalizers();
            long m0 = GC.GetTotalMemory(false);
            timer.Reset();
            timer.Start();

            NodeEnumerator enumerator = new NodeEnumerator(root);
            foreach (Node node in enumerator)
            {
                count++;
                //for (int i = 0; i < root.level - node.level; i++) Console.Write("  ");
                //Console.WriteLine(node.ToString());
            }
            timer.Stop();
            long m1 = GC.GetTotalMemory(false);
            GC.Collect();
            GC.WaitForPendingFinalizers();

            //Console.WriteLine(string.Format("NOTRECUR nodes : {0} time : {1} mem : {2}", count, timer.ElapsedTicks, (m1 - m0) / 1024.0 / 1024));

            return new Info { count = count, mem = (m1 - m0) / 1024.0, time = timer.ElapsedTicks };
        }

        static void Main(string[] args)
        {
            Tree tree = new Tree(8);

            Info treerecurs = new Info();
            Info treestack = new Info();
            Info noderecurs = new Info();
            Info nodestack = new Info();
            Info stacketor = new Info();
            Info stackyieldetor = new Info();

            for (int i = 0; i < 10; i++)
            {
                //treerecurs.Add(RecursiveImpl(tree));
                treestack.Add(StackImpl(tree));
                //noderecurs.Add(RecursiveImpl(tree.root));
                nodestack.Add(StackImpl(tree.root));
                stacketor.Add(StackEtorImpl(tree.root));
                //stackyieldetor.Add(StackEtorYieldImpl(tree.root));
            }

            Console.WriteLine(string.Format("RECURTREE nodes : {0} time : {1} mem : {2}", treerecurs.count, treerecurs.time, treerecurs.mem));
            Console.WriteLine(string.Format("STACKTREE nodes : {0} time : {1} mem : {2}", treestack.count, treestack.time, treestack.mem));
            Console.WriteLine(string.Format("RECURNODE nodes : {0} time : {1} mem : {2}", noderecurs.count, noderecurs.time, noderecurs.mem));
            Console.WriteLine(string.Format("STACKNODE nodes : {0} time : {1} mem : {2}", nodestack.count, nodestack.time, nodestack.mem));
            Console.WriteLine(string.Format("STACKETOR nodes : {0} time : {1} mem : {2}", stacketor.count, stacketor.time, stacketor.mem));
            Console.WriteLine(string.Format("STACKETORY nodes : {0} time : {1} mem : {2}", stackyieldetor.count, stackyieldetor.time, stackyieldetor.mem));

            Console.ReadKey();
        }
    }
}
