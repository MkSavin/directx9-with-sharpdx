﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GenericClass
{




    public class Program
    {
        static int[] A;
        static int I = 0;

        static void ByRef(ref int i)
        {
            i = A[0];
        }

        static unsafe void ByOut(out int i)
        {
            fixed (int* Aptr = &A[0])
            {
                i = *Aptr;
            }

        }
        static unsafe int* ByPtr()
        {
            fixed (int* Iptr = &A[0])
            {
                return Iptr;
            }
        }

        static unsafe IntPtr ByIntPtr()
        {
            fixed (int* Iptr = &I)
            {
                return new IntPtr(Iptr);
            }
        }

        public static void Main(string[] args)
        {
            A = new int[] { 11, 12, 13, 14 };

            int i = default(int);
            ByRef(ref i);
            i = 1;
            Console.WriteLine(A[0]);
            
            int j = default(int);
            ByOut(out j);
            j = 1;
            Console.WriteLine(A[0]);
            
            unsafe
            {
                int* kPtr = ByPtr();
                *kPtr = 1;
            }

            VertNode v0 = new VertNode(1.0f, 0);
            VertNode v1 = new VertNode(2.0f, 1);
            VertNode v2 = new VertNode(3.0f, 2);
            EdgeNode e0 = new EdgeNode(v0, v1);
            EdgeNode e1 = new EdgeNode(v1, v2);
            EdgeNode e2 = new EdgeNode(v2, v0);
            FaceNode f0 = new FaceNode(v0, v1, v2);

            
            Console.ReadKey();
        }
    }
}
