﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace Collisions
{
    public interface ICollisionMesh
    {
        void Draw(Graphics graphics);
    }


    public class RectangleObj : ICollisionMesh
    {
        private static Random rnd = new Random();
        private Brush riempimento;


        public Point Min, Max;


        private RectangleObj()
        {

        }

        public RectangleObj(Point Min, Point Max) : this()
        {
            riempimento = new SolidBrush(Color.FromArgb(rnd.Next(0, 255), rnd.Next(0, 255), rnd.Next(0, 255)));

        }

        public RectangleObj(Point Center, int HalfWidth,int HalfHeight) : this()
        {


        }

        /// <summary>
        /// Disegna sullo schermo
        /// </summary>
        public void Draw(Graphics graphics)
        {
            graphics.FillRectangle(riempimento, Min.X, Min.Y, Max.X - Min.X, Max.Y - Min.Y);
        }
    }



    public class Oggetto
    {
        public enum CollisionType
        {
            Sphere,
            Rectangle
        }

        ICollisionMesh collision;



        public Oggetto(CollisionType collisiontype, Point Center, int Width, int Height , float Rotation)
        {
            switch (collisiontype)
            {
                case CollisionType.Rectangle: collision = new RectangleObj(Center, Width / 2, Height / 2); break;

            }


        }


        /// <summary>
        /// Disegna sullo schermo
        /// </summary>
        public void Draw(Graphics graphics)
        {
            collision.Draw(graphics);
        }
    }
}
