﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Timer = System.Windows.Forms.Timer;
namespace Collisions
{
    public partial class MyForm : Form
    {
        private BufferedGraphicsContext context;
        private BufferedGraphics grafx;
        private Timer timer;
        private Random rnd = new Random();
        private int FPS = 0;
        private byte count;
        private byte bufferingMode;
        private string[] bufferingModeStrings = { "Draw to Form without OptimizedDoubleBufferring control style",
                                                   "Draw to Form using OptimizedDoubleBuffering control style",
                                                   "Draw to HDC for form" };


        List<Canvas> canvas = new List<Canvas>();


        public MyForm()
        {
            InitializeComponent();

            Point p = Point.Empty;

            canvas.Add(new Axis());



            // Configure the Form for this example.
            this.Text = "User double buffering";
            this.MouseDown += new MouseEventHandler(this.MouseDownHandler);
            this.panel1.Resize += new EventHandler(this.OnResize);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint, true);

            // Configure a timer to draw graphics updates.
            timer = new System.Windows.Forms.Timer();
            timer.Interval = 1000;
            timer.Tick += new EventHandler(this.OnTimer);

            bufferingMode = 2;
            count = 0;

            // Retrieves the BufferedGraphicsContext for the 
            // current application domain.
            context = BufferedGraphicsManager.Current;

            // Sets the maximum size for the primary graphics buffer
            // of the buffered graphics context for the application
            // domain.  Any allocation requests for a buffer larger 
            // than this will create a temporary buffered graphics 
            // context to host the graphics buffer.
            context.MaximumBuffer = new Size(this.panel1.Width + 1, this.panel1.Height + 1);

            // Allocates a graphics buffer the size of this form
            // using the pixel format of the Graphics created by 
            // the Form.CreateGraphics() method, which returns a 
            // Graphics object that matches the pixel format of the form.
            grafx = context.Allocate(this.CreateGraphics(), new Rectangle(0, 0, this.panel1.Width, this.panel1.Height));

            // Draw the first frame to the buffer.
            DrawToBuffer(grafx.Graphics);

            timer.Start();
        }



        public void Render()
        {
            FPS++;

            // Draw randomly positioned ellipses to the buffer.
            DrawToBuffer(grafx.Graphics);

            // If in bufferingMode 2, draw to the form's HDC.
            if (bufferingMode == 2)
                // Render the graphics buffer to the form's HDC.
                grafx.Render(Graphics.FromHwnd(this.panel1.Handle));
            // If in bufferingMode 0 or 1, draw in the paint method.
            else
                this.Refresh();
        }


        private void MouseDownHandler(object sender, MouseEventArgs e)
        {
            /*
            if (e.Button == MouseButtons.Right)
            {
                // Cycle the buffering mode.
                if (++bufferingMode > 2)
                    bufferingMode = 0;

                // If the previous buffering mode used 
                // the OptimizedDoubleBuffering ControlStyle,
                // disable the control style.
                if (bufferingMode == 1)
                    this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);

                // If the current buffering mode uses
                // the OptimizedDoubleBuffering ControlStyle,
                // enabke the control style.
                if (bufferingMode == 2)
                    this.SetStyle(ControlStyles.OptimizedDoubleBuffer, false);

                // Cause the background to be cleared and redraw.
                count = 6;
                DrawToBuffer(grafx.Graphics);
                this.Refresh();
            }
            */
        }

        private void OnTimer(object sender, EventArgs e)
        {
            Text = FPS.ToString() + " FPS";
            FPS = 0;
        }

        private void OnResize(object sender, EventArgs e)
        {
            // Re-create the graphics buffer for a new window size.
            context.MaximumBuffer = new Size(this.Width + 1, this.Height + 1);
            if (grafx != null)
            {
                grafx.Dispose();
                grafx = null;
            }
            grafx = context.Allocate(this.CreateGraphics(), new Rectangle(0, 0, this.Width, this.Height));

            // Cause the background to be cleared and redraw.
            count = 6;
            DrawToBuffer(grafx.Graphics);
            this.Refresh();
        }

        private void DrawToBuffer(Graphics g)
        {
            grafx.Graphics.Clear(Color.CornflowerBlue);
            foreach (Canvas canv in canvas) canv.Draw(g, this.panel1.ClientRectangle);
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            return;
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            grafx.Render(e.Graphics);
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            grafx.Dispose();
            context.Dispose();
            base.OnClosing(e);
        }


        /// <summary>
        /// Punto di ingresso principale dell'applicazione.
        /// </summary>
        [STAThread]
        static void Main()
        {
           Stopwatch timer = new Stopwatch();
            timer.Start();

            long prev = timer.ElapsedMilliseconds;

            using (MyForm form = new MyForm())
            {
                form.Show();

                while (form.Created)
                {
                    long elapsed = timer.ElapsedMilliseconds - prev;

                    if ((form.Visible == false) || (form.ContainsFocus == false) || (form.Enabled == false))
                    {
                        System.Threading.Thread.Sleep(100);                 
                    }
                    else
                    {
                        // 60 fps = 16,666 ms per frame
                        if (elapsed > 10 )
                        {
                            form.Render();
                            prev = timer.ElapsedMilliseconds;
                        }
                    }
                    Application.DoEvents();
                }
            }

        }
    }
}
