﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;


namespace Collisions
{
    public abstract class Canvas
    {
        /// <summary>
        /// </summary>
        public abstract void Draw(Graphics gfx, Rectangle clientsize);
    }


    public class Axis : Canvas
    {

        public override void Draw(Graphics g, Rectangle clientsize)
        {
            g.DrawLine(Pens.DarkGreen, clientsize.X + 10, clientsize.Height - 10, clientsize.X + 10, clientsize.Y + 10);
            g.DrawLine(Pens.DarkRed, clientsize.X + 10, clientsize.Height - 10, clientsize.Width - 10, clientsize.Height - 10);
        }


    }

}
