﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;


namespace MemoryPool
{
    public class PoolManager2<T>
    {
        Stack<T> pool;

        public PoolManager2()
        {
        }

    }

    public abstract class PoolClass2<T>
    {

        private PoolClass2()
        {

        }


        ~PoolClass2()
        {
            // Resurrecting the object
            HandleReAddingToPool(true);
        }

        /// <summary>
        /// </summary>
        internal bool Disposed { get; set; }

        private void HandleReAddingToPool(bool reRegisterForFinalization)
        {
            if (!Disposed)
            {
                // If there is any case that the re-adding to the pool failes, release the resources and set the internal Disposed flag to true
                try
                {
                    // Notifying the pool that this object is ready for re-adding to the pool.
                    ReturnToPool(reRegisterForFinalization);
                }
                catch (Exception)
                {
                    Disposed = true;
                }
            }
        }

        protected abstract void ReturnToPool(bool reRegisterForFinalization);

        /// <summary>
        /// Reset the object state to allow this object to be re-used by other parts of the application.
        /// </summary>
        protected virtual void OnResetState()
        {

        }
    }




}
