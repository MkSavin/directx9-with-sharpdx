﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace MemoryPool
{
    public class PoolClass<T> : IDisposable where T : class
    {
        protected bool disposed = false;
        public Action<T> RePoolingFunction { get; internal set; }

        /// <summary>
        /// Initialize a new Class, assign a RePoolingFunction to work with manager
        /// </summary>
        public PoolClass()
        {

            disposed = false;
        }
        /// <summary>
        /// Finalizer
        /// </summary>
        ~PoolClass()
        {
            Dispose();

        }

        public void ResursizeFunction()
        {
            disposed = false;
        }

        public bool IsDisposed
        {
            get { return disposed; }
        }

        public virtual void Dispose()
        {
            if (!disposed && RePoolingFunction != null)
            {
                disposed = true;
                RePoolingFunction(this as T);
            }
        }


    }

    public class CustomClass : PoolClass<CustomClass>, IUniqueInstanced
    {
        public long UniqueID { get; set; }

        public byte[] somedata;

        static int counter = 0;
        int instance = 0;

        /// <summary>
        /// Constructor
        /// </summary>
        public CustomClass()
        {
            UniqueID = -1;
            somedata = new byte[1000];
            instance = counter++;
#if DEBUG
            Console.WriteLine("Construttore " + this.ToString());
#endif
        }

        /// <summary>
        /// Finalizer
        /// </summary>
        ~CustomClass()
        {
#if DEBUG
            Console.WriteLine("Distruttore " + this.ToString());
#endif
        }

        public override string ToString()
        {
            return string.Format("CustomClass_{0}_ID{1}, disposed:{2}", instance, UniqueID, IsDisposed);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    public interface IUniqueInstanced
    {
        int UniqueID { get; set; }
    }


    /// <summary>
    /// Manager for classes used in small numbers but very frequently, pre-allocating some classes is not
    /// required initializating new classes but simply reuse some discarded classes. 
    /// </summary>
    public class PoolClassManager<T> : IDisposable where T : PoolClass<T>
    {
        protected bool disposed = false;
        Stack<T> pool;
        Type type;

        public readonly int Capacity = 0;

        public int InstancesGenerated = 0;

        public PoolClassManager(int Capacity)
        {
            this.Capacity = Capacity;
            disposed = false;
            type = typeof(T);
            pool = new Stack<T>(10);

        }

        ~PoolClassManager()
        {
            Dispose();
        }

        /// <summary>
        /// Generate a class, if required initialize some new classes
        /// </summary>
        /// <returns></returns>
        public T New()
        {
            T obj;

            if (pool.Count == 0) IncreaseInstances(Capacity / 10);
            obj = pool.Pop();

            //obj = pool.Count == 0 ? generateInstance() : pool.Pop();

            obj.RePoolingFunction = RePoolingObject;
            obj.ResursizeFunction();
            return obj;
        }

        public void RePoolingObject(T obj)
        {
            if (!disposed)
            {
                pool.Push(obj);
                obj.RePoolingFunction = null;
            }
        }

        public void IncreaseInstances(int num)
        {
            if (InstancesGenerated + num > Capacity) num = Capacity - InstancesGenerated;

            if (num <= 0) throw new OutOfMemoryException();

            for (int i = 0; i < num; i++)
            {
                T obj = generateInstance();
                pool.Push(obj);
            }
        }

        protected virtual T generateInstance()
        {
            T obj = (T)Activator.CreateInstance(type);
            InstancesGenerated++;
            return obj;
        }

        public void Dispose()
        {
            if (!disposed)
            {
                disposed = true;
                foreach (T obj in pool)
                {
                    obj.RePoolingFunction = null;
                    obj.Dispose();
                }
                pool.Clear();
            }
        }

        public override string ToString()
        {
            return "PoolClassManager<" + type + ">";
        }
    }

    public class PoolClassIndexedManager<T> : PoolClassManager<T> where T : PoolClass<T>, IUniqueInstanced
    {
        public UniqueIndexManager indexmanager;

        public PoolClassIndexedManager(int Capacity)
            : base(Capacity)
        {
            indexmanager = new UniqueIndexManager(Capacity,false);
        }

        protected override T generateInstance()
        {
            T obj = base.generateInstance();
            int idx = indexmanager.GetNextAvailableIndex(0);
            obj.UniqueID = idx;
            indexmanager.SetIndex(idx, true);
            return obj;
        }

    }

}
