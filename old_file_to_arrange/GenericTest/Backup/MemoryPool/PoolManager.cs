﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace MemoryPool
{
    /// <summary>
    /// 
    /// </summary>
    public interface IUniqueInstanced
    {
        int UniqueID { get; set; }
    }


    /// <summary>
    /// Manager for classes used in small numbers but very frequently, pre-allocating some classes is not
    /// required initializating new classes but simply reuse some discarded classes. 
    /// </summary>
    public class PoolClassManager<T> : IDisposable where T : PoolClass<T>
    {
        protected bool disposed = false;
        Stack<T> pool;
        Type type;

        public readonly int Capacity = 0;

        public int InstancesGenerated = 0;

        public PoolClassManager(int Capacity)
        {
            this.Capacity = Capacity;
            disposed = false;
            type = typeof(T);
            pool = new Stack<T>(10);

        }

        ~PoolClassManager()
        {
            Dispose();
        }

        /// <summary>
        /// Generate a class, if required initialize some new classes
        /// </summary>
        /// <returns></returns>
        public T New()
        {
            T obj;

            if (pool.Count == 0) IncreaseInstances(Capacity / 10);
            obj = pool.Pop();

            //obj = pool.Count == 0 ? generateInstance() : pool.Pop();

            obj.RePoolingFunction = RePoolingObject;
            obj.ResursizeFunction();
            return obj;
        }

        public void RePoolingObject(T obj)
        {
            if (!disposed)
            {
                pool.Push(obj);
                obj.RePoolingFunction = null;
            }
        }

        public void IncreaseInstances(int num)
        {
            if (InstancesGenerated + num > Capacity) num = Capacity - InstancesGenerated;

            if (num <= 0) throw new OutOfMemoryException();

            for (int i = 0; i < num; i++)
            {
                T obj = generateInstance();
                pool.Push(obj);
            }
        }

        protected virtual T generateInstance()
        {
            T obj = (T)Activator.CreateInstance(type);
            InstancesGenerated++;
            return obj;
        }

        public void Dispose()
        {
            if (!disposed)
            {
                disposed = true;
                foreach (T obj in pool)
                {
                    obj.RePoolingFunction = null;
                    obj.Dispose();
                }
                pool.Clear();
            }
        }

        public override string ToString()
        {
            return "PoolClassManager<" + type + ">";
        }
    }

    public class PoolClassIndexedManager<T> : PoolClassManager<T> where T : PoolClass<T>, IUniqueInstanced
    {
        public UniqueIndexManager indexmanager;

        public PoolClassIndexedManager(int Capacity)
            : base(Capacity)
        {
            indexmanager = new UniqueIndexManager(Capacity,false);
        }

        protected override T generateInstance()
        {
            T obj = base.generateInstance();
            int idx = indexmanager.GetNextAvailableIndex(0);
            obj.UniqueID = idx;
            indexmanager.SetIndex(idx, true);
            return obj;
        }

    }

}
