﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace MemoryPool
{
    public static class Tool
    {
        public static string GetBinaryString(UInt32 value)
        {
            char[] bits = new char[32];
            for (int i = 0; i < 32; i++) bits[31 - i] = (value & (1 << i)) != 0 ? '1' : '0';

            string str = "";
            for (int i = 0; i < 4; i++) str += new string(bits, i * 8, 8) + " ";
            return str;
        }
    }


    /// <summary>
    /// Generate a unique int32 id
    /// </summary>
    public class UniqueIndexManager
    {
        /// <summary>
        /// Maximum indices count
        /// </summary>
        public readonly int Capacity = 0;

        BitArray1D indexfield;

        /// <summary>
        /// Using a bitarray with integers, the size in byte of this class is a multiple of 32 indices, example all initialization
        /// from maximum index = 0 to maximum index = 31 have the same size in bytes.
        /// </summary>
        public UniqueIndexManager(int Capacity, bool initial)
        {
            this.Capacity = Capacity;
            indexfield = new BitArray1D(Capacity, initial);
        }

        /// <summary>
        /// The current maximum index used, can be used example to optimize the searching of new available index
        /// </summary>
        public int MaxIndexUsed { get; private set; }

        /// <summary>
        /// Return the position of first not-set index, it's the "0" in bitarray
        /// Return -1 if there aren't available indices
        /// </summary>
        /// <param name="istart">the first index to test</param>
        public int GetNextAvailableIndex(int istart = 0)
        {
            return indexfield.SearchNext(istart, false);
            //return indexfield.SearchNext_old(istart, false);
        }
        /// <summary>
        /// Return the position of first used index, it's the "1" in bitarray
        /// Return -1 if there aren't available indices
        /// </summary>
        /// <param name="istart">the first index to test</param>
        public int GetNextInUseIndex(int istart = 0)
        {
            return indexfield.SearchNext(istart, true);
        }

        /// <summary>
        /// Set the index value to USED or NOT-USED
        /// </summary>
        public void SetIndex(int index, bool inUse)
        {
            indexfield[index] = inUse;

            if (inUse && MaxIndexUsed < index) MaxIndexUsed = index;
        }

        /// <summary>
        /// Return true if index are used, false if is avaiable
        /// </summary>
        public bool GetIndex(int index)
        {
            return indexfield[index];
        }

        /// <summary>
        /// Find the corsize "MaxIndeUsed" when you have removed a lot of indices at the end
        /// </summary>
        public void TrimIndices()
        {
            int i = MaxIndexUsed;
            while (!indexfield[i] && i >= 0) { i--; MaxIndexUsed--; }
        }

        /// <summary>
        /// print to string the bitarray in compact mode, show all indices used
        /// </summary>
        public override string ToString()
        {
            return indexfield.ToCompactString();
        }
    }

    /// <summary>
    /// Base bit storage class
    /// </summary>
    public abstract class BitField : IEnumerable<bool>
    {
        const int FULL = unchecked((int)0xffffffff);

        // i noticed that using int value is a little fast than byte because there aren't cast operators
        protected int[] field;
        // lenght of field array
        protected int length;
        // length of bits list, always less of equal than capacity (length * 32)
        protected int size;

        protected BitField(int size, bool initialvalue)
        {
            this.size = size;
            this.length = (size - 1) / 32 + 1;
            
            if (length < 1 || length > int.MaxValue) throw new ArgumentOutOfRangeException("wrong size");
            //0 = false, 1 = true
            field = new int[length];

            if (initialvalue)
            {
                int value = unchecked((int)0xffffffff);
                for (int i = 0; i < length; field[i++] = value) ;
            }
        }

        /// <summary>
        /// The size in bytes of internal field array
        /// </summary>
        public int BytesSize
        { 
            get { return sizeof(int) * (length + 2); }
        }

        /// <summary>
        /// The size of bit's field, can be any number but BytesSize was always a multiple of 4 bytes
        /// </summary>
        public int Count
        {
            get { return size; }
        }

        /// <summary>
        /// return true for '1' or false for '0'
        /// </summary>
        public bool this[int i]
        {
            get { return getBit(i / 32, i % 32); }
            set { setBit(i / 32, i % 32, value); }
        }

        /// <summary>
        /// Do AND operator for all bits, if bitarray is smaller the missing value are considered '1'
        /// </summary>
        /// <param name="bitarray"></param>
        public void AND(BitField bitarray)
        {
            int min = length;
            if (min > bitarray.length) min = bitarray.length;
            for (int i = 0; i < min; i++) field[i] = field[i] & bitarray.field[i];
        }
        /// <summary>
        /// Do OR operator for all bits, if bitarray is smaller the missing value are considered '0'
        /// </summary>
        /// <param name="bitarray"></param>
        public void OR(BitField bitarray)
        {
            int min = length;
            if (min > bitarray.length) min = bitarray.length;
            for (int i = 0; i < min; i++) field[i] = field[i] | bitarray.field[i];
        }
        /// <summary>
        /// Do NOT operator for all bits
        /// </summary>
        /// <param name="bitarray"></param>
        public void NOT()
        {
            for (int i = 0; i < length; i++) field[i] = ~field[i];
        }



        protected void setBit(int ichunk, int ioffset, bool value)
        {
            if (value) field[ichunk] |= (1 << ioffset);
            else field[ichunk] &= ~(1 << ioffset);
        }

        protected bool getBit(int ichunk, int ioffset)
        {
            return (field[ichunk] & (1 << ioffset)) != 0;
        }

        /// <summary>
        /// Find in the integer's chunk the "value" from "ioffset"
        /// </summary>
        protected int search(int ichunk, int ioffset, bool value)
        {
            while (ioffset < 32 && getBit(ichunk, ioffset) != value) { ioffset++; }
            return ioffset >= 32 ? -1 : ioffset + ichunk * 32;
        }
        /// <summary>
        /// Find in the field the "value" from "istart".
        /// Code optimized
        /// </summary>
        protected int search(int istart, bool value)
        {
            // the integer's block containing the istart's index
            int ichunkStart = istart / 32;
            // the bits offset of integer's bits
            int ioffset = istart % 32;
            // the index of integer's array
            int ichunk = ichunkStart;
            // the all '1' bits after ioffset
            int maskR, empty;

            if (value)
            {
                // the all '1' bits after ioffset
                maskR = FULL << ioffset;
                empty = 0;
            }
            else
            {
                maskR = FULL << (ioffset);
                empty = FULL;
            }
            
            string test0 = Tool.GetBinaryString((uint)field[ichunk]);
            string test2 = Tool.GetBinaryString((uint)maskR);


            //--------------------------------------
            // test the list from "start" to "end"
            //--------------------------------------


            // begining integer contain a '1' after offset (valid for case '1' or '0')
            int chunkInteger = value ? field[ichunk] : ~field[ichunk];

            string test = Tool.GetBinaryString((uint)(chunkInteger & maskR));

            if ( (chunkInteger & maskR) != 0)
            {
                return search(ichunk, ioffset, value);
            }
            ichunk++;

            // filter 32 by 32 indices the list for a fast search
            while (ichunk < length && field[ichunk] == empty) { ichunk++;}

            //--------------------------------------
            // if not found test from "0" to "start"
            //--------------------------------------
            if (ichunk >= length)
            {
                // filter 32 by 32 indices the list for a fast search
                ichunk = 0;
                while (ichunk < ichunkStart && field[ichunk] == empty) { ichunk++; }

                if (ichunk >= ichunkStart)
                {
                    // begining integer contain a '1' before offset
                    chunkInteger = value ? field[ichunk] : ~field[ichunk];
                    if ((chunkInteger & (~maskR)) != 0)
                        return search(ichunk, 0, value);
                }
                else
                {
                    return search(ichunk, 0, value);
                }
                //--------------------------------------
                // if all chunk block are empty return not-found
                //--------------------------------------
                return -1;
            }
            else
            {
                return search(ichunk, 0, value);
            }
        }
        /// <summary>
        /// Find in the field the "value" from "istart".
        /// code use the standard access function
        /// </summary>
        protected int search_old(int istart, bool value)
        {
            int i = istart;
            // first test the list from "istart" to "end"
            while (i < size && this[i]!=value) { i++; }

            // if not found test from "0" to "istart"
            if (i >= size)
            {
                i = 0;
                while (i < istart && this[i] != value) { i++; }
                if (i >= istart) return -1;
            }

            return i;
        }

        /// <summary>
        /// Convert bitarray in string with '1' or '0'
        /// </summary>
        /// <param name="compactmode">the 3dstudio maxscript bitarray is the best way to rappresent a random sequence</param>
        protected string getbitstring(int Start, int Size, bool compactmode)
        {
            int End = Start + Size;

            // convert to a string like 3dstudio maxscript bitarray : {1...3,5} where show only true values
            if (compactmode)
            {
                StringBuilder str = new StringBuilder();

                int istart = Start;
                int iend = Start;
                bool firstgroup = true;
                str.Append('{');

                do
                {
                    // jump to next value '1'
                    // remark : the first boolean test is necessary to exit if you reach the last value
                    while (istart < End && !this[istart]) istart++;
                    iend = istart + 1;

                    // jump to next value '0'
                    while (iend < End && this[iend]) iend++;

                    if (istart < End)
                    {
                        if (istart > Start && !firstgroup)
                            str.Append(',');

                        if (istart + 1 < iend)
                            str.Append(string.Format("{0}..{1}", istart - Start, iend - Start - 1));
                        else
                            str.Append(string.Format("{0}", istart - Start));

                        istart = iend;
                        firstgroup = false;
                    }
                    else
                    {
                        break;
                    }
                }
                while (iend < End);

                str.Append('}');

                return str.ToString();
            }
            // write bitsxbits
            else
            {
                char[] chars = new char[Size];
                for (int i = 0; i < Size; i++) chars[i] = this[Start + i] ? '1' : '0';
                return new string(chars);
            }
        }


        public IEnumerator<bool> GetEnumerator()
        {
            int i, j;

            for (i = 0; i < length; i++)
            {
                int remain = i < length - 1 ? 32 : size % 32;
                int value = field[i];
                for (j = 0; j < remain; j++)
                    yield return (value & (1 << j)) != 0;
            }
            yield break;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        public abstract string ToExtendedString();

        public abstract string ToCompactString();

    }

    /// <summary>
    /// One dimension Bit array
    /// </summary>
    public class BitArray1D : BitField
    {
        public BitArray1D(int size, bool initialvalue = false) : base(size, initialvalue) { }

        /// <summary>
        /// Search the position of a index by boolean value, return -1 if not found
        /// </summary>
        public int SearchNext(int istart, bool value)
        {
            return base.search(istart,value);
        }
        /// <summary>
        /// Search the position of a index by boolean value, return -1 if not found
        /// </summary>
        public int SearchNext_old(int istart,bool value)
        {
            return base.search_old(istart, value);
        }

        public override string ToExtendedString()
        {
            return base.getbitstring(0, length * 32, false);
        }

        public override string ToCompactString()
        {
            return base.getbitstring(0, size, true);
        }
    }
}
