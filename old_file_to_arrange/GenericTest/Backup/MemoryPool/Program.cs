﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MemoryPool
{
    class Program
    {
        #region GC

        static void GCtest()
        {
            CustomClass c1, c2, c3;

            using (PoolClassManager<CustomClass> pool = new PoolClassManager<CustomClass>(10))
            {
                using (CustomClass c0 = pool.New())
                {
                    Console.WriteLine(string.Format("{0}  Disposing...", c0.ToString()));
                }
                c1 = pool.New();
                c2 = pool.New();
                c3 = pool.New();
                      
               Console.WriteLine("Pool Disposing...");
            }

            Console.WriteLine("*Classi non chiuse*");
            Console.WriteLine(c1.ToString());
            Console.WriteLine(c2.ToString());
            Console.WriteLine(c3.ToString());


            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        #endregion


        #region Instance Test

        [Flags]
        enum Mode : byte
        {
            None = 0,
            Polled = 1,
            UnPolled = 2,
            Both = Polled | UnPolled
        }

        static CustomClass GetNewInstance()
        {
            //Console.WriteLine(string.Format("get new : {0}", item.ToString()));
            //return item;
            return null;
        }

        static void DeleteInstance(CustomClass item)
        {
            item.Dispose();
            Console.WriteLine(string.Format("release : {0}", item.ToString()));
        }

        static int INSTMANAGED = 100;

        static CustomClass[] temp = new CustomClass[INSTMANAGED];

        static void TestA1(int loop)
        {
            for (int i = 0; i < loop; i++)
            {
                for (int j = 0; j < INSTMANAGED; j++)
                {
                    //temp[j] = pool
                }
                for (int j = 0; j < INSTMANAGED; j++)
                {
                    temp[j].Dispose();
                    temp[j] = null;
                }
            }
        }

        static void TestA2(int loop)
        {
            for (int i = 0; i < loop; i++)
            {
                for (int j = 0; j < INSTMANAGED; j++)
                {
                    temp[j] = new CustomClass();
                }
                for (int j = 0; j < INSTMANAGED; j++)
                {
                    temp[j].Dispose();
                    temp[j] = null;
                }
            }

        }

        static void TestB1(PoolClassManager<CustomClass> pool)
        {
            Random rnd = new Random();
            
            for (int i = 0; i < 1000000; i++)
            {
                int j = rnd.Next(INSTMANAGED);
                if (temp[j] != null) temp[j].Dispose();
                temp[j] = pool.New();
            }
        }

        static void TestB2()
        {
            Random rnd = new Random();

            for (int i = 0; i < 1000000; i++)
            {
                int j = rnd.Next(INSTMANAGED);
                temp[j] = new CustomClass();
            }
        }

        static void InstanceTest(Mode mode)
        {
            Console.WriteLine("Any key to start...");
            //Console.ReadKey();

            GC.Collect();
            GC.WaitForPendingFinalizers();
            long m0 = GC.GetTotalMemory(false);

            PoolClassManager<CustomClass> pool = new PoolClassManager<CustomClass>(1000000);
            //pool.IncreaseInstances(pool.Capacity);

            HighTimer timer = new HighTimer();

            if ((mode & Mode.Polled) != 0)
            {
                timer.Start();
                TestB1(pool);
                timer.Stop();

                double t0 = timer.DurationMiliseconds;
                long m1 = GC.GetTotalMemory(false);

                GC.Collect();
                GC.WaitForPendingFinalizers();
                long m2 = GC.GetTotalMemory(false);

                Console.WriteLine(string.Format("Polled:\nTime : {0}\nMemory : {1} MB\nAfter Collect : {2}\nPool Instances : {3}", t0, (m1 - m0) / 1024.0 / 1024.0, (m2 - m1) / 1024.0 / 1024.0, pool.InstancesGenerated));
                Console.WriteLine("");
                pool = null;
            }
            if ((mode & Mode.UnPolled) != 0)
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                long m2 = GC.GetTotalMemory(false);

                timer.Start();
                TestB2();
                timer.Stop();

                double t1 = timer.DurationMiliseconds;
                long m3 = GC.GetTotalMemory(false);
                GC.Collect();
                GC.WaitForPendingFinalizers();
                long m4 = GC.GetTotalMemory(false);

                Console.WriteLine(string.Format("Unpolled:\nTime : {0}\nMemory : {1} MB\nAfter Collect : {2}", t1, (m3 - m2) / 1024.0 / 1024.0, (m4 - m3) / 1024.0 / 1024.0));
                Console.WriteLine("");
            }

            Console.WriteLine("Any key to exit...");
            Console.ReadKey();
        }

        static void InstanceIndexTest(Mode mode)
        {
            HighTimer timer = new HighTimer();
            temp = null;

            PoolClassManager<CustomClass> pool = new PoolClassManager<CustomClass>(100);
            //PoolClassIndexedManager<CustomClass> pool = new PoolClassIndexedManager<CustomClass>(1000);

            GC.Collect();
            GC.WaitForPendingFinalizers();
            long m0 = GC.GetTotalMemory(false);

            //--------------------------------- WITH POOL
            
            timer.Start();
            for (int i = 0; i < 100000; i++)
            {
                CustomClass c0 = pool.New();
                CustomClass c1 = pool.New();
                CustomClass c2 = pool.New();
                CustomClass c3 = pool.New();
                CustomClass c4 = pool.New();
                CustomClass c5 = pool.New();
                CustomClass c6 = pool.New();
                CustomClass c7 = pool.New();
                CustomClass c8 = pool.New();
                CustomClass c9 = pool.New();
                c0.Dispose();
                c1.Dispose();
                c2.Dispose();
                c3.Dispose();
                c4.Dispose();
                c5.Dispose();
                c6.Dispose();
                c7.Dispose();
                c8.Dispose();
                c9.Dispose();
            }
            timer.Stop();

            double t0 = timer.DurationMiliseconds;
            long m1 = GC.GetTotalMemory(false);

            Console.WriteLine(string.Format("Polled:\nTime : {0}\nMemory : {1} MB\nPool Instances : {2}", t0, (m1 - m0) / 1024.0 / 1024.0, pool.InstancesGenerated));
            Console.WriteLine("");

            pool.Dispose();
            pool = null;

            //--------------------------------- WITHOUT POOL
            GC.Collect();
            GC.WaitForPendingFinalizers();
            long m2 = GC.GetTotalMemory(false);
            
            timer.Start();
            for (int i = 0; i < 100000; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    CustomClass c = new CustomClass();
                }
            }
            timer.Stop();
            double t1 = timer.DurationMiliseconds;

            long m3 = GC.GetTotalMemory(false);

            Console.WriteLine(string.Format("UnPolled:\nTime : {0}\nMemory : {1} MB", t1, (m3 - m2) / 1024.0 / 1024.0));
            Console.WriteLine("");

        }
        #endregion


        #region BitField Test

        static void BitTest2()
        {
            UniqueIndexManager indices = new UniqueIndexManager(INSTMANAGED, true);
            HighTimer timer = new HighTimer();
            timer.Start();

            //for (int i = 0;i<INSTMANAGED - 1; i++)
            for (int i = INSTMANAGED - 2; i >= 0 ; i--)
            {
                //indices.SetIndex(i, false);

                int j = indices.GetNextAvailableIndex(i + 1);
                
                if (i != j) throw new Exception();

                indices.SetIndex(i, true);

                if (i % 1000 == 0)
                {
                    timer.Stop();
                    Console.WriteLine(i + " " + timer.DurationMiliseconds);
                    timer.Start();
                }
            }

        }
        
        static void BitTest3()
        {
            UniqueIndexManager indices = new UniqueIndexManager(32 * 1000 - 1, false);
            indices.SetIndex(indices.Capacity - 10, true);

            
            HighTimer timer = new HighTimer();

            timer.Start();
            for (int i = 0; i < 1000000; i++)
            {
                int j = indices.GetNextAvailableIndex(indices.Capacity - 9);
            }
            timer.Stop();
            double t0 = timer.DurationMiliseconds;

            indices.SetIndex(indices.Capacity - 10, false);
            indices.SetIndex(0, true);

            timer.Start();
            for (int i = 0; i < 1000000; i++)
            {
                int j = indices.GetNextAvailableIndex(1);
            }
            timer.Stop();
            double t1 = timer.DurationMiliseconds;

            Console.WriteLine(string.Format("Time last : {0}\nTime first : {1}", t0, t1)); 
        }
        
        static void BitTest()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
            long m0 = GC.GetTotalMemory(false);

            UniqueIndexManager indices = new UniqueIndexManager(1000000, false);
            int idx;
            long m1 = GC.GetTotalMemory(false);

            for (int i = 0; i < 1000000; i++)
            {
                if (i % 128 == 0) indices.SetIndex(i, true);
            }

            //Console.WriteLine(indices.ToString());


            HighTimer timer = new HighTimer();
            timer.Start();

            idx = 0;
            for (int i = 0; i < 1000000; i++)
            {
                idx = indices.GetNextAvailableIndex(idx) + 1;
            }
            timer.Stop();
            double t0 = timer.DurationMiliseconds;

            timer.Start();
            idx = 0;
            for (int i = 0; i < 1000000; i++)
            {
                idx = indices.GetNextAvailableIndex(idx) + 1;
            }
            timer.Stop();
            double t1 = timer.DurationMiliseconds;

            Console.WriteLine(string.Format("Memory : {0}\nTime : {1}\nOptimized : {2}", m1 - m0, t0, t1));


            Console.ReadKey();
        }

        static void BitTest4()
        {
            UniqueIndexManager indices = new UniqueIndexManager(32 * 3 - 1, true);

            indices.SetIndex(62, false);

            string test = indices.ToString();

            int idx = indices.GetNextAvailableIndex(63);


        }
        static void BitTest5()
        {
            BitArray1D array1 = new BitArray1D(64, false);
            BitArray1D array2 = new BitArray1D(32, true);
            
            array1.OR(array2);

            string test = array1.ToCompactString();
        }

        #endregion


        static void Main(string[] args)
        {
            CustomClass c = new CustomClass();

            int h0 = c.GetHashCode();

            c.somedata = new byte[10];

            if (h0 != c.GetHashCode()) throw new Exception();

            InstanceIndexTest(Mode.Both);
            Console.WriteLine("App Close");
        }
    }
}
