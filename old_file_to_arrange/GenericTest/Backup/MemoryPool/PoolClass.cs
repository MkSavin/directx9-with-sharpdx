﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;


namespace MemoryPool
{

    public class PoolClass<T> : IDisposable  where T : class 
    {
        protected bool disposed = false;
        public Action<T> RePoolingFunction { get; internal set; }

        /// <summary>
        /// Initialize a new Class, assign a RePoolingFunction to work with manager
        /// </summary>
        public PoolClass()
        {
            
            disposed = false; 
        }
        /// <summary>
        /// Finalizer
        /// </summary>
        ~PoolClass()
        {
            Dispose();
           
        }

        public void ResursizeFunction()
        {
            disposed = false;
        }

        public bool IsDisposed
        {
            get { return disposed; }
        }

        public virtual void Dispose()
        {
            if (!disposed && RePoolingFunction!=null)
            {
                disposed = true;
                RePoolingFunction(this as T);
            }
        }


    }

    public class CustomClass : PoolClass<CustomClass>, IUniqueInstanced
    {
        public long UniqueID { get; set; }

        public byte[] somedata;

        static int counter = 0;
        int instance = 0;

        /// <summary>
        /// Constructor
        /// </summary>
        public CustomClass()
        {
            UniqueID = -1;
            somedata = new byte[1000];
            instance = counter++;
#if DEBUG
            Console.WriteLine("Construttore " + this.ToString());
#endif
        }

        /// <summary>
        /// Finalizer
        /// </summary>
        ~CustomClass()
        {
#if DEBUG
            Console.WriteLine("Distruttore " + this.ToString());
#endif
        }

        public override string ToString()
        {
            return string.Format("CustomClass_{0}_ID{1}, disposed:{2}", instance, UniqueID, IsDisposed);
        }
    }

}
