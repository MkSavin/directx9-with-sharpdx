﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace Quadtree
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            Quadtree<sizeangleNode> tree = new Quadtree<sizeangleNode>(3);
            sizeangleNode root = new sizeangleNode(tree, new sizeangle(0, 0, 10, 10));
            tree.Populate(root);
            */
            Quadtree world = new Quadtree(3);

            world.root = new QuadNode(world, new Rectangle(0, 0, 10, 10));
            world.root.RecurseSplit();
        }
    }
}
