﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace Quadtree
{
    public class QuadNode
    {
        const byte Q0 = 1 << 0;
        const byte Q1 = 1 << 1;
        const byte Q2 = 1 << 2;
        const byte Q3 = 1 << 3;
        const byte FULL = Q0 | Q1 | Q2 | Q3;
        const byte EMPTY = 0;

        public QuadNode parent;
        public QuadNode[] child;
        public Quadtree main;
        public int index;
        public int level;
        public int nodeid;
        public Rectangle size;
        public byte childused = EMPTY;

        public QuadNode(Quadtree main, Rectangle size)
        {
            this.main = main;
            this.parent = null;
            this.index = -1;
            this.level = main.depth - 1;
            this.nodeid = 0;
            this.size = size;
        }

        public QuadNode(QuadNode parent, int index, Rectangle size)
        {
            this.main = parent.main;
            this.parent = parent;
            this.index = index;
            this.level = parent.level - 1;
            this.nodeid = (parent.nodeid << 2) + index + 1;
            this.size = size;
            this.parent.childused |= (byte)(1 << index);
        }


        public void RecurseSplit()
        {
            if (level > 0)
            {
                child = new QuadNode[4];
                child[0] = new QuadNode(this, 0, new Rectangle(size.x0, size.y0, size.xm, size.ym));
                child[1] = new QuadNode(this, 1, new Rectangle(size.x0, size.ym, size.xm, size.y1));
                child[2] = new QuadNode(this, 2, new Rectangle(size.xm, size.y0, size.x1, size.ym));
                child[3] = new QuadNode(this, 3, new Rectangle(size.xm, size.ym, size.x1, size.y1));

                for (int i = 0; i < 4; i++)
                    child[i].RecurseSplit();
            }
        }

        public override string ToString()
        {
            return string.Format("Node id:{0}", nodeid);
        }

    }

    public class Quadtree
    {
        public QuadNode root;
        public int depth;

        public Quadtree(int depth)
        {
            this.depth = depth;
        }
    }

    public struct Rectangle
    {
        public float x0, y0, x1, y1;
        public float xm { get { return (x0 + y0) * 0.5f; } }
        public float ym { get { return (x1 + y1) * 0.5f; } }

        public Rectangle(float x0, float y0, float x1, float y1)
        {
            this.x0 = x0;
            this.x1 = x1;
            this.y0 = y0;
            this.y1 = y1;
        }

        public override string ToString()
        {
            return string.Format("{0} {1} - {2} {3}", x0, y0, x1, y1);
        }
    }
}
