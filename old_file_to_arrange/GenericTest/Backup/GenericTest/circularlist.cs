﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericTest
{
    /// <summary>
    /// To use c# version of pointers i need a class, not a struct
    /// </summary>
    public interface ILink<T> where T : class
    {
        /// <summary>
        /// Pointers to next item
        /// </summary>
        T Next { get; set; }
        /// <summary>
        /// Pointers to previous item
        /// </summary>
        T Prev { get; set; }
        /// <summary>
        /// A boolean value use when you must remove all
        /// </summary>
        bool marked2remove { get; }
    }

    /// <summary>
    /// Menager class of ILink nodes
    /// </summary>
    [DebuggerDisplay("Count = {Count}")]
    public class CircularLinkedList<T> : IEnumerable<T> where T : class, ILink<T>
    {
        T head = null;
        int count = 0;

        public CircularLinkedList()
        {
        }
        public CircularLinkedList(IEnumerable<T> collection)
        {
            IEnumerator<T> enumerator = collection.GetEnumerator();
            while (enumerator.MoveNext()) this.Add(enumerator.Current);
        }

        /// <summary>
        /// Get or Set the first element in the circular link. No check are made.
        /// </summary>
        public T Head
        {
            get { return head; }
            set { head = value; }
        }
        /// <summary>
        /// Get number of elements in the list
        /// </summary>
        public int Count
        {
            get { return count; }
        }
        /// <summary>
        /// Add node before head or as last element
        /// </summary>
        public void Add(T node)
        {
            if (head == null)
            {
                head = node;
                head.Next = head.Prev = head;
            }
            // example   +- N0 - N1 - N2 -+     where N0 == head and you add N3  
            //           |________________|
            else
            {
                //get temp value;
                T n0 = head;
                T n2 = head.Prev;

                //set n3 link
                node.Next = n0;
                node.Prev = n2;

                //update old link
                n2.Next = node;
                n0.Prev = node;
            }
            count++;
        }
        /// <summary>
        /// Remove a node added to this list, no safety test was made so ensure that node is its.
        /// All reference "Next" and "Prev" will set to null
        /// If you remove the head, the new head is set as Head.Next
        /// </summary>
        public void Remove(T node)
        {
            T Np = node.Prev;
            T Nn = node.Next;

            Np.Next = Nn;
            Nn.Prev = Np;

            // if you delete head, assign a new head
            if (head == node)
            {
                // if next == head mean are you deleting last node       
                head = (Nn == head) ? null : Nn;
            }
            node.Next = node.Prev = null;

            count--;
        }
        /// <summary>
        /// Set to null all Prev and Next reference, remove head reference and set count=0
        /// </summary>
        public void RemoveAll()
        {
            count = 0;
            if (head == null) return;

            T node = head.Next;
            while (node != head)
            {
                node.Prev = null;
                node = node.Next;
                node.Prev.Next = null;
                node.Prev = null;
            }
            head.Prev = head.Next = null;
            head = null;
        }

        /// <summary>
        /// Remove all node with a compare function, provide a internal implementation and avoid user 
        /// to do a complicate procedure. If the head are deleted, the new head is head.Next
        /// </summary>
        public void Clear()
        {
            // first step remove all head untill get a non removed item
            while (head != null && head.marked2remove)
            {
                Remove(head);
            }
            if (head == null) return;

            // start from this head and do untill tail
            T node = head.Next;
            while (node != head)
            {
                T nextnode = node.Next;
                if (node.marked2remove) Remove(node);
                node = nextnode;
            }
        }

        /// <summary>
        /// Gets the item at the current index O(n) complexity
        /// </summary>
        public T this[int index]
        {
            get
            {
                if (index >= count || index < 0)
                {
                    throw new ArgumentOutOfRangeException("index");
                }
                else
                {
                    T node = head;
                    for (int i = 0; i < index; i++) node = node.Next;
                    return node;
                }
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            T current = head;
            if (current != null)
            {
                do
                {
                    yield return current;
                    current = current.Next;
                }
                while (current != head);
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public List<T> List
        {
            get
            {
                List<T> list = new List<T>(count);
                if (head != null)
                {
                    T node = head;
                    do
                    {
                        list.Add(node);
                        node = node.Next;
                    }
                    while (node != head);
                }
                return list;
            }
        }
        bool Search(T start, T node)
        {
            if (node == start) return true;
            if (start.Next != head) return Search(start.Next, node);
            return false;
        }
        public bool Contains(T node)
        {
            return head != null ? Search(head, node) : false;
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }

}
