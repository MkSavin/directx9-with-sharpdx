﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace GenericTest
{
    public class Node : ILink<Node>
    {
        public Node Next { get; set; }
        public Node Prev { get; set; }
        public bool marked2remove { get { throw new NotImplementedException(); } }

        int i = 0;

        public Node(int i)
        {
            this.i = i;
        }

        public override string ToString()
        {
            return "N" + i;
        }
    }



    class Program
    {
        static void TestEnumerable(BitArray1D bitarray)
        {
            foreach (bool item in bitarray)
            {
                //Console.Write(item);
                //Console.Write(" ");
            }

        }

        static void TestEnumerator(BitArray1D bitarray)
        {
            foreach (bool item in bitarray.BooleanList)
            {
                //Console.Write(item);
                //Console.Write(" ");
            }
            //foreach (int item in bitarray.IndicesList)
            {
                //Console.Write(item);
                //Console.Write(" ");
            }
        }
        static void TestEnumeratorIdx(BitArray1D bitarray)
        {
            foreach (int item in bitarray.IndicesList)
            {
                //Console.Write(item);
                //Console.Write(" ");
            }

        }

        public static void Main(string[] args)
        {
            HighTimer timer = new HighTimer();
            Random rnd = new Random();
            BitArray1D bitarray = new BitArray1D(100000, true);
            for (int i = 0; i < 100000; i++) bitarray[i] = rnd.Next(1000) > 500;

            timer.Start();

            TestEnumerable(bitarray);
            timer.Stop();
            double t = timer.DurationMiliseconds;
            Console.WriteLine(string.Format("Enumerable : time {0}", t));
            

            timer.Start();
            TestEnumerator(bitarray);
            timer.Stop();
            t = timer.DurationMiliseconds;
            Console.WriteLine(string.Format("Enumerator : time {0}", t));

            timer.Start();
            TestEnumeratorIdx(bitarray);
            timer.Stop();
            t = timer.DurationMiliseconds;
            Console.WriteLine(string.Format("EnumeratorIDX : time {0}", t));


            //Console.WriteLine(bitarray.ToCompactString());

            Console.ReadKey();
        }
    }
}
