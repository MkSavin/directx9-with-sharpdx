﻿
using System;
using System.Collections.Generic;
using System.Text;

using Engine.Tools;

namespace voxel2d
{
    [Flags]
    public enum QuadIdx : byte
    {
        EMPTY = 0,
        Q0 = 1 << 0,
        Q1 = 1 << 1,
        Q2 = 1 << 2,
        Q3 = 1 << 3,
        FULL = Q0 | Q1 | Q2 | Q3
    }

    [Flags]
    public enum VoxelCase : byte
    {
        EMPTY = 0,
        P0 = 1 << 0,
        P1 = 1 << 2,
        P2 = 1 << 3,
        P3 = 1 << 1,
        FULL = P0 | P1 | P2 | P3
    }

    public class QuadNode : IEnumerable<QuadNode>
    {
        public QuadNode parent;
        public QuadNode[] child;
        public Quadtree main;
        public int index;
        public int level;
        public int nodeid;
        public Rectangle size;
        public QuadIdx childused = QuadIdx.EMPTY;

        public QuadNode()
        {

        }
        public QuadNode(Quadtree main, Rectangle size)
        {
            SetAsRoot(main, size);
        }
        public QuadNode(QuadNode parent, int index, Rectangle size)
        {
            SetAsNode(parent, index, size);
        }
       
        public void SetAsRoot(Quadtree main, Rectangle size)
        {
            this.main = main;
            this.parent = null;
            this.index = -1;
            this.level = main.depth - 1;
            this.nodeid = 0;
            this.size = size;
        }
        public void SetAsNode(QuadNode parent, int index, Rectangle size)
        {
            this.main = parent.main;
            this.parent = parent;
            this.index = index;
            this.level = parent.level - 1;
            this.nodeid = (parent.nodeid << 2) + index + 1;
            this.size = size;
            this.parent.childused |= (QuadIdx)(1 << index);
        }

        public bool ExistChild(int index)
        {
            return child!=null && child[index]!=null;
        }

        /// <summary>
        /// This formula return the nodeid of parent
        /// </summary>
        public static int GetParentById(int nodeid)
        {
            return (nodeid - 1) >> 2;
        }
        /// <summary>
        /// This formula return the index of nodeid
        /// </summary>
        public static int GetIndexById(int nodeid)
        {
            return (nodeid - 1) & 3;
        }

        public void Split()
        {
            this.childused = QuadIdx.EMPTY;
            child = new QuadNode[4];
            child[0] = new QuadNode(this, 0, new Rectangle(size.x0, size.y0, size.xm, size.ym));
            child[1] = new QuadNode(this, 1, new Rectangle(size.xm, size.y0, size.x1, size.ym));
            child[2] = new QuadNode(this, 2, new Rectangle(size.x0, size.ym, size.xm, size.y1));
            child[3] = new QuadNode(this, 3, new Rectangle(size.xm, size.ym, size.x1, size.y1));
        }

        public void CreateChild<T>(int index) where T : QuadNode , new()
        {
            if (child == null) child = new QuadNode[4];
            Rectangle childsize;
            switch (index)
            {
                case 0: childsize = new Rectangle(size.x0, size.y0, size.xm, size.ym); break;
                case 1: childsize = new Rectangle(size.xm, size.y0, size.x1, size.ym); break;
                case 2: childsize = new Rectangle(size.x0, size.ym, size.xm, size.y1); break;
                case 3: childsize = new Rectangle(size.xm, size.ym, size.x1, size.y1); break;
                default: throw new IndexOutOfRangeException();
            }
            child[index] = new T();
            child[index].SetAsNode(this, index, childsize);
        }

        public void RecurseSplit()
        {
            if (level > 0)
            {
                Split();
                for (int i = 0; i < 4; i++)
                    child[i].RecurseSplit();
            }
        }

        public override string ToString()
        {
            return string.Format("Node id:{0}", nodeid);
        }


        public IEnumerator<QuadNode> GetEnumerator()
        {
            if (child == null) yield break;

            for (int i = 0; i < 4; i++)
            {
                if (child[i] != null)
                {
                    yield return child[i];
                    foreach (QuadNode node in child[i])
                        yield return node;
                }
            }
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }


    public class VoxelNode : QuadNode
    {
        public QuadIdx voxelcase;

        public VoxelNode()
            : base()
        {
        }

        public VoxelNode(Quadtree main, Rectangle size)
            : base(main, size)
        {
        }

        public VoxelNode(QuadNode parent, int index, Rectangle size)
            : base(parent, index, size)
        {
        }

    }

    public class Quadtree : IEnumerable<QuadNode>
    {
        public BitArray2D density;
        public QuadNode root;
        public int depth;
        int[] tree;


        public Quadtree(BitArray2D density  , int depth)
        {
            this.depth = depth;
            this.density = density;
            tree = new int[depth];
        }

        public void Build()
        {
            if (root == null) return;
            Recurse(root.level, 0, root.nodeid, 0, 0, density.Width - 1, density.Heigth - 1);
        }

        public T CreateNodeByID<T>(int nodeid) where T : QuadNode, new()
        {
            int i = 0;
            int par = nodeid;
            while (par > 0)
            {
                tree[i++] = QuadNode.GetIndexById(par);
                par = QuadNode.GetParentById(par);
            }
            if (i == 0) return (T)root;

            QuadNode curr = root;

            for (i = i - 1; i >= 0; i--)
            {
                if (curr.child == null) curr.child = new QuadNode[4];

                int index = tree[i];

                if (curr.child[index] == null) curr.CreateChild<T>(index);

                curr = curr.child[index];
            }

            if (curr.nodeid != nodeid) throw new Exception("Algorithm wrong");

            return (T)curr;

        }

        public int Recurse(int level, int index, int nodeid, int x0, int y0, int x1, int y1)
        {
            int sum = 0;

            // if is the corner of leaf node, calculate number of pixels
            if (level < 0)
            {
                sum = 0;
                for (int y = y0; y <= y1; y++)
                    for (int x = x0; x <= x1; x++)
                        if (density[x, y]) sum++;
                return sum;
            }
            // if is a node create the tree untill it only in voxelcase mixed
            else
            {
                int xm = (x0 + x1) / 2;
                int ym = (y0 + y1) / 2;
                int threasold = (x1 - x0 + 1) * (y1 - y0 + 1) / 4;
                QuadIdx voxelcase = 0;

                sum = Recurse(level - 1, 0, (nodeid << 2) + 1, x0, y0, xm, ym);
                if (sum >= threasold) voxelcase |= QuadIdx.Q0;

                sum = Recurse(level - 1, 1, (nodeid << 2) + 2, xm + 1, y0, x1, ym);
                if (sum >= threasold) voxelcase |= QuadIdx.Q1;

                sum = Recurse(level - 1, 2, (nodeid << 2) + 3, x0, ym + 1, xm, y1);
                if (sum >= threasold) voxelcase |= QuadIdx.Q2;

                sum = Recurse(level - 1, 3, (nodeid << 2) + 4, xm + 1, ym + 1, x1, y1);
                if (sum >= threasold) voxelcase |= QuadIdx.Q3;

                switch (voxelcase)
                {
                    case QuadIdx.EMPTY:
                        return 0;
                    case QuadIdx.FULL:
                        return threasold;
                    default:
                        VoxelNode node = CreateNodeByID<VoxelNode>(nodeid);
                        node.voxelcase = voxelcase;

                        if (level > 0)
                        {
                            // ensure exist children with VoxelCase.Full
                            int vcase = (int)voxelcase;
                            for (int i = 0 , mask = 1 ; i < 4; i++ , mask <<= 1)
                            {
                                if ((vcase & mask) != 0)
                                {
                                    if (!node.ExistChild(i)) node.CreateChild<VoxelNode>((nodeid << 2) + i + 1);
                                    (node.child[i] as VoxelNode).voxelcase = QuadIdx.FULL;
                                }
                            }
                        }

                        return threasold;

                }
            }
        }
        public IEnumerator<QuadNode> GetEnumerator()
        {
            if (root == null) yield break;

            yield return root; 

            foreach (QuadNode node in root)
                yield return node;
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }

    public struct Rectangle
    {
        public float x0, y0, x1, y1;
        public float xm { get { return (x0 + x1) * 0.5f; } }
        public float ym { get { return (y0 + y1) * 0.5f; } }

        public Rectangle(float x0, float y0, float x1, float y1)
        {
            this.x0 = x0;
            this.x1 = x1;
            this.y0 = y0;
            this.y1 = y1;
        }

        public override string ToString()
        {
            return string.Format("{0} {1} - {2} {3}", x0, y0, x1, y1);
        }
    }
}
