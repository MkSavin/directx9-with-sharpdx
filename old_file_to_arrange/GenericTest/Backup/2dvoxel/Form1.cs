﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace voxel2d
{
    public partial class Form1 : Form
    {
        Voxel2D world;

        public Form1()
        {
            //SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint, true);
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            world = new Voxel2D(Properties.Resources.density);

            base.OnLoad(e);
        }


        protected override void OnPaintBackground(PaintEventArgs e)
        {
            if (DesignMode) base.OnPaintBackground(e);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            e.Graphics.Clear(BackColor);

            System.Drawing.Rectangle zone = ClientRectangle;

            if (zone.Width > 20) { zone.X += 10; zone.Width -= 20; }
            if (zone.Height > 20) { zone.Y += 10; zone.Height -= 20; }

            e.Graphics.FillRectangle(Brushes.White, zone);

            world.Draw(e.Graphics, zone);
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            this.Invalidate();
        }
    }
}
