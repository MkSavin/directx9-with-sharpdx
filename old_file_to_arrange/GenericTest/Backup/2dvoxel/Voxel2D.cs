﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;

using Engine.Tools;
using Engine.Maths;

namespace voxel2d
{
    public class Voxel2D
    {
        BitArray2D density;

        Bitmap density_bmp;

        Quadtree quadtree;

        Brush poligonbrush = new HatchBrush(HatchStyle.LightDownwardDiagonal, Color.Blue, Color.Blue);
        Brush semibrush = new HatchBrush(HatchStyle.LightDownwardDiagonal, Color.Aqua, Color.Aqua);
        Point[] polygon;


        /// <summary>
        /// initialize the density field, is a table of '1' or '0' bit
        /// </summary>
        /// <param name="Width"></param>
        /// <param name="Height"></param>
        public Voxel2D(Bitmap map)
        {
            // find maximum suddivision, until obtain 2x2pixel
            int size = MathsUtils.MIN(map.Width, map.Height);
            int minsize = size;
            int depth = 0;
            while (minsize >= 2) { minsize /= 2; depth++; }

            depth = 3;

            density = new BitArray2D(map.Width, map.Height);


            BitmapLock bmplock = new BitmapLock(map);
            bmplock.LockBits();

            int w = map.Width;
            int h = map.Height;

            for (int y = 0; y < h; y++ )
                for (int x = 0; x < w; x++)
                {
                    Color p = bmplock.GetPixel(x, h - y - 1);
                    if (p.GetBrightness() < 0.5f) density[x, y] = true;
                }
            bmplock.UnlockBits();


            density_bmp = new Bitmap(density.Width, density.Heigth);

            bmplock = new BitmapLock(density_bmp);
            bmplock.LockBits();
            for (int y = 0; y<h; y++)
                for (int x = 0; x < w; x++)
                {
                    if (density[x, y]) bmplock.SetPixel(x, h - y -1, Color.Yellow);
                }

            bmplock.UnlockBits();


            quadtree = new Quadtree(density, depth);
            quadtree.root = new VoxelNode(quadtree, new Rectangle(0, 0, 100, 100));

            //quadtree.CreateNodeByID<VoxelNode>(9);
            //quadtree.CreateNodeByID<VoxelNode>(10);
            //quadtree.CreateNodeByID<VoxelNode>(11);
            //quadtree.CreateNodeByID<VoxelNode>(12);
            quadtree.Build();
        }


        public void Draw(Graphics g, System.Drawing.Rectangle size)
        {
            g.DrawImage(density_bmp, size, new System.Drawing.Rectangle(0, 0, density_bmp.Width, density_bmp.Height),GraphicsUnit.Pixel);

            int X0 = size.X;
            int Y0 = size.Y;
            int X1 = X0 + size.Width;
            int Y1 = Y0 + size.Height;

            float deltax = (quadtree.root.size.x1 - quadtree.root.size.x0) / (X1 - X0);
            float deltay = (quadtree.root.size.y1 - quadtree.root.size.y0) / (Y1 - Y0);

            foreach (VoxelNode node in quadtree)
            {
                int x0 = (int)(X0 + (node.size.x0 - quadtree.root.size.x0) / deltax);
                int x1 = (int)(X0 + (node.size.x1 - quadtree.root.size.x0) / deltax);
                
                int y0 = (int)(Y1 - (node.size.y0 - quadtree.root.size.y0) / deltay); 
                int y1 = (int)(Y1 - (node.size.y1 - quadtree.root.size.y0) / deltay);

                switch(node.voxelcase)
                {
                    case QuadIdx.FULL:
                        g.FillRectangle(poligonbrush, x0, y1, x1 - x0, y0 - y1);    
                        break;
                    case QuadIdx.EMPTY:
                        g.DrawRectangle(Pens.Gray, x0, y1, x1 - x0, y0 - y1); 
                        break;
                    default:
                        g.FillRectangle(semibrush, x0, y1, x1 - x0, y0 - y1);
                        break;
                }

               //g.DrawString(node.nodeid.ToString(), SystemFonts.DefaultFont, Brushes.Black, (x0 + x1) / 2, (y0 + y1) / 2);
            }
        }

    }

    public class Point2
    {
        public int id;
        public float x, y;

        public Point2(float x, float y)
        {
            this.id = -1;
            this.x = x;
            this.y = y;
        }
    }

    public static class Voxel2DData
    {
        //
        //   1------2
        //   |      |
        //   |      |
        //   0------3
        //

        public static Point2[] points = new Point2[]
        {
            new Point2(-1,-1),
            new Point2(-1, 0),
            new Point2(-1, 1),
            new Point2( 0, 1),
            new Point2( 1, 1),
            new Point2( 1, 0),
            new Point2( 1,-1),
            new Point2( 0,-1)
        };

        public static byte[][] indices;

        static Voxel2DData()
        {
            indices = new byte[15][];
            indices[0] = new byte[] { };
            indices[1] = new byte[] { 0, 1, 7 };
            indices[2] = new byte[] { 1, 2, 3 };
            indices[3] = new byte[] { 7, 0, 2, 3 };
            indices[4] = new byte[] { 3, 4, 5 };
            indices[5] = new byte[] { 0, 1, 7, 3, 4, 5 };
            indices[6] = new byte[] { 1, 2, 4, 5 };
            indices[7] = new byte[] { 0, 2, 4, 5, 7 };
            indices[8] = new byte[] { 5, 6, 7 };
            indices[9] = new byte[] { 0, 1, 5, 6 };
            indices[10] = new byte[] { 1, 2, 3, 5, 6, 7 };
            indices[11] = new byte[] { 0, 1, 2, 3, 5, 6, 7 };
            indices[12] = new byte[] { 3, 4, 6, 7 };
            indices[13] = new byte[] { 0, 1, 3, 4, 6 };
            indices[14] = new byte[] { 1, 2, 4, 6, 7 };
            indices[15] = new byte[] { 0, 2, 4, 6 };
        }
    }

}
