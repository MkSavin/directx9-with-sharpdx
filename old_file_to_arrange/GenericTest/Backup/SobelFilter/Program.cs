﻿using System;
using System.IO;
using System.Drawing;
using System.Text;

namespace SobelFilter
{
    class Program
    {
        static void Main(string[] args)
        {
            string filename = Path.GetFullPath(@"..\..\debug.bmp");
            if (File.Exists(filename))
            {
                string savename = Path.GetDisizeoryName(filename) + "\\result.bmp";


                BoundingBox mapsize = new BoundingBox(0, 0, 0, 10, 10, 10);


                HeightMap map = HeightMap.FromBitmap(new Bitmap(filename), mapsize);
                Vector3B[,] colors = map.GetNormalMap();

                Bitmap normals = new Bitmap(map.SizeX, map.SizeY, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

                for (int i = 0; i < map.SizeX; i++)
                    for (int j = 1; j < map.SizeY; j++)
                    {
                        Color color = Color.FromArgb(255, colors[i, j].r, colors[i, j].g, colors[i, j].b);
                        // swap Y
                        normals.SetPixel(i, map.SizeY - j - 1, color);
                    }

                normals.Save(savename, System.Drawing.Imaging.ImageFormat.Bmp);
            }
        }
    }


    public struct Vector3
    {
        public float x, y, z;

        public Vector3(float x, float y, float z)
        {
            this.x = x; this.y = y; this.z = z;
        }
        public static Vector3 operator +(Vector3 left, Vector3 right)
        {
            return new Vector3(left.x + right.x, left.y + right.y, left.z + right.z);
        }
        public static Vector3 operator *(Vector3 left, float scalar)
        {
            return new Vector3(left.x * scalar, left.y * scalar, left.z * scalar);
        }
    }

    public struct BoundingBox
    {
        public Vector3 max, min;
        public Vector3 center { get { return (max + min) * 0.5f; } }

        float minf(float a, float b) { return a < b ? a : b; }
        float maxf(float a, float b) { return a > b ? a : b; }
        void swap(ref float a, ref float b) { float t = a; a = b; b = t; }

        public BoundingBox(Vector3 p0, Vector3 p1)
        {
            max = p0;
            min = p1;
            if (max.x < min.x) swap(ref max.x, ref min.x);
            if (max.y < min.y) swap(ref max.y, ref min.y);
            if (max.z < min.z) swap(ref max.z, ref min.z);
        }
        public BoundingBox(float p0x, float p0y, float p0z, float p1x, float p1y, float p1z) :
            this(new Vector3(p0x, p0y, p0z), new Vector3(p1x, p1y, p1z)) { }

    }
    /// <summary>
    /// Encode normals to 3 bytes, float will have +-0.0078125f precision
    /// </summary>
    public struct Vector3B
    {
        // are coverted from [-1,1] range to [0,255]
        public byte r, g, b;

        public float nx { get { return decode(r); } set { r = encode(value); } }
        public float ny { get { return decode(g); } set { g = encode(value); } }
        public float nz { get { return decode(b); } set { b = encode(value); } }

        /// <summary>
        /// From normalize coordinates in [-1,1] range
        /// </summary>
        public Vector3B(float nx, float ny, float nz)
        {
            r = g = b = 0;
            r = encode(nx);
            g = encode(ny);
            b = encode(nz);
        }

        byte encode(float i)
        {
            i = (i + 1.0f) * 128.0f;
            if (i > 255) return 255;
            else if (i < 0) return 0;
            else return (byte)i;
        }
        float decode(byte i)
        {
            return (i - 128) / 128.0f;
        }
    }

    public class HeightMap
    {
        public int SizeX { get; private set; }
        public int SizeY { get; private set; }
        public float SizeH { get; private set; }
        
        byte[,] map;
        BoundingBox bounds;
        float hsize;
        float hsize255;

        public HeightMap(int sizeX, int sizeY, BoundingBox size)
        {
            this.SizeX = sizeX;
            this.SizeY = sizeY;
            this.bounds = size;
            this.hsize = size.max.y - size.min.y;
            this.hsize255 = hsize * 255.0f;
            this.map = new byte[sizeX, sizeY];
        }

        float clamp(float f) { return f < 0 ? 0 : f > 1 ? 1 : f; }

        /// <summary>
        /// use [0.0 - 1.0] range to extract height
        /// </summary>
        public float this[float x, float z]
        {
            get { return map[(int)(clamp(x) * SizeX), (int)(clamp(z) * SizeY)] * hsize255; }
            set { map[(int)(clamp(x) * SizeX), (int)(clamp(z) * SizeY)] = (byte)(value * hsize255); }
        }
        /// <summary>
        /// use [0 - SizeX-1] range to extract height
        /// </summary>
        public float this[int x, int y]
        {
            get { return map[x, y] * hsize255; }
            set { map[x, y] = (byte)(value * hsize255); }
        }

        /// <summary>
        /// </summary>
        public void GetMinMax(int startx, int starty, int endx, int endy, out float Min, out float Max)
        {
            byte min = 255;
            byte max = 0;

            for (int x = startx; x < endx; x += 1)
                for (int y = starty; y < endy; y += 1)
                {
                    byte b = map[x, y];
                    if (b < min) min = b;
                    if (b > max) max = b;
                }
            Min = min / 255.0f;
            Max = max / 255.0f;
        }

        /// <summary>
        /// My custom implementation to generate normals data from height map
        /// </summary>
        public Vector3B[,] GetNormalMap()
        {
            float strength = hsize / 1000;

            Vector3B[,] normals = new Vector3B[SizeX, SizeY];

            for (int i = 0; i < SizeX; i++) normals[i, 0] = normals[i, SizeX - 1] = new Vector3B(0, 0, 0);
            for (int j = 0; j < SizeY; j++) normals[0, j] = normals[SizeY - 1, j] = new Vector3B(0, 0, 0);

            for (int i = 1; i < SizeX - 1; i++)
                for (int j = 1; j < SizeY - 1; j++)
                {
                    byte m = map[i, j];
                    //top , topright , right , bottomright , bottom, bottomleft, left, topleft
                    byte t = map[i, j + 1];
                    byte tr = map[i + 1, j + 1];
                    byte r = map[i + 1, j];
                    byte br = map[i + 1, j - 1];
                    byte b = map[i - 1, j - 1];
                    byte bl = map[i - 1, j - 1];
                    byte l = map[i - 1, j];
                    byte tl = map[i - 1, j + 1];

                    // sobel filter
                    float dx = (tr + 2.0f * r + br) - (tl + 2.0f * l + bl);
                    float dy = (bl + 2.0f * b + br) - (tl + 2.0f * t + tr);
                    float dz = 1.0f / strength;

                    float mag = (float)Math.Sqrt(dx * dx + dy * dy + dz * dz);

                    normals[i, j] = new Vector3B(dx / mag, dy / mag, dz / mag);
                }


            return normals;
        }


        public static HeightMap FromBitmap(Bitmap bmp,BoundingBox size)
        {

            HeightMap tab = new HeightMap(bmp.Width, bmp.Height, size);
            // remember to swap V
            for (int x = 0; x < bmp.Width; x++)
                for (int y = 0; y < bmp.Height; y++)
                    tab.map[x, y] = (byte)(bmp.GetPixel(x, bmp.Height - y - 1).GetBrightness() * 255.0f);
            return tab;
        }

        /// <summary>
        /// To debug
        /// </summary>
        public static HeightMap WaveMap(int sizeX, int sizeY, BoundingBox size)
        {
            HeightMap tab = new HeightMap(sizeX, sizeY, size);

            float dx = (float)Math.PI * 2 / sizeX;
            float dy = (float)Math.PI * 2 / sizeY;

            for (int x = 0; x < sizeX; x++)
                for (int y = 0; y < sizeX; y++)
                {
                    float s = (float)Math.Sin(x * dx);
                    float c = (float)Math.Cos(y * dy);
                    tab[x, y] = s + c + 0.5f;
                }
            return tab;
        }
    }
}
