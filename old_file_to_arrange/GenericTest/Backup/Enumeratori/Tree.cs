﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Enumeratori
{
    public class Tree
    {
        public Stack<Node> stack = new Stack<Node>(20);
        public Node root;

        public Tree(int depth)
        {
            depth--;
            root = new Node(this, depth, -1, 0);
            root.RecursiveSplit();
        }

        /// <summary>
        /// A lot of memory usage, slow
        /// </summary>
        public IEnumerable<Node> NodeList_recursivenum
        {
            get
            {
                if (root == null) yield break;
                yield return root;         
                foreach (Node node in root.NodeList_recursivenum)
                    yield return node;
            }
        }
        /// <summary>
        /// No memory usage, x10 faster
        /// </summary>
        public IEnumerable<Node> NodeList_stack
        {
            get
            {
                if (root == null) yield break;
                stack.Clear();
                Node current = root;
                stack.Push(current);
               
                while (stack.Count > 0)
                {
                    current = stack.Pop();
                    
                    yield return current;

                    if (current.level > 0)
                    {
                        stack.Push(current.child[3]);
                        stack.Push(current.child[2]);
                        stack.Push(current.child[1]);
                        stack.Push(current.child[0]);
                    }
                }
            }
        }

    }

    public class Node
    {
        public Tree main;
        public int level, index;
        public Node[] child;
        public int nodeid;

        public Node(Tree tree, int level, int index, int nodeid)
        {
            this.main = tree;
            this.level = level;
            this.index = index;
            this.nodeid = nodeid;
        }

        public void RecursiveSplit()
        {
            if (level > 0)
            {
                child = new Node[4];
                for (int i = 0; i < 4; i++)
                {
                    child[i] = new Node(main, level - 1, i, (nodeid << 2) + (i + 1));
                    child[i].RecursiveSplit();
                }
            }
        }

        public override string ToString()
        {
            return string.Format("level {0} index {1} nodeid {2}", level, index, nodeid);
        }


        public IEnumerable<Node> NodeList_recursivenum
        {
            get
            {
                if (level == 0) yield break;

                    for (int i = 0; i < 4; i++)
                    {
                        yield return child[i];
                        foreach (Node node in child[i].NodeList_recursivenum)
                            yield return node;
                    }
            }
        }

        public IEnumerable<Node> NodeList_stack
        {
            get
            {
                if (level == 0) yield break;
                
                main.stack.Clear();

                main.stack.Push(child[3]);
                main.stack.Push(child[2]);
                main.stack.Push(child[1]);
                main.stack.Push(child[0]);

                Node current;

                while (main.stack.Count > 0)
                {
                    current = main.stack.Pop();

                    yield return current;

                    if (current.level > 0)
                    {
                        main.stack.Push(current.child[3]);
                        main.stack.Push(current.child[2]);
                        main.stack.Push(current.child[1]);
                        main.stack.Push(current.child[0]);
                    }
                }
            }
        }
    }



    public class NodeEnumerator : IEnumerator<Node> , IEnumerable<Node>
    {
        Node root;
        Node current;
        Stack<Node> stack;
        public int InteretorCounter { get; private set; }
        
        /// <summary>
        /// Root node will be not returned by iterator, only children, because you already know the root node
        /// </summary>
        public NodeEnumerator(Node root)
        {
            this.root = root;
            stack = new Stack<Node>(10);
            current = root;
        }

        public Node Current
        {
            get { return current; }
        }

        public bool MoveNext()
        {
            if (current.level > 0)
            {
                stack.Push(current.child[3]);
                stack.Push(current.child[2]);
                stack.Push(current.child[1]);
                stack.Push(current.child[0]);
            }

            // no child or current node is last, exit
            if (stack.Count == 0) return false;

            current = stack.Pop();
            InteretorCounter++;
            return true;
        }

        public void Reset()
        {
            current = root;
            stack.Clear();
        }

        public void Dispose()
        {
            Reset();
            stack = null;
        }

        object IEnumerator.Current
        {
            get { return Current; }
        }

        public IEnumerator<Node> GetEnumerator()
        {
            while (this.MoveNext())
            {
                yield return current;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this;
        }
    }
}
