﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MeshTool
{
    public struct Triangle
    {
        public int[] edge;

        public Triangle(int i, int j, int k)
        {
            edge = new int[] { i, j, k };
        }

    }

    public struct EdgeTab
    {
        public Triangle edge;

        public override int this[int i]
        {
            get
            {
                return edge.edge[i];
            }
            set
            {
                edge.edge[i] = value;
            }
        }

        public EdgeTab(int x, int y, int z)
        {
            edge = new Triangle(x, y, z);
        }

    }


    public class Class1
    {
        enum Axe
        {
            X = 0,
            Y = 1,
            Z = 2,
        }


        Point3d[] P;
        EdgeTab[] E;

        public Class1()
        {
            // corners of cube
            P = new Point3d[8];

            P[0] = new Point3d(-1, -1, -1);
            P[1] = new Point3d(-1, 1, -1);
            P[2] = new Point3d(1, 1, -1);
            P[3] = new Point3d(1, -1, -1);
            P[4] = new Point3d(-1, -1, 1);
            P[5] = new Point3d(1, -1, 1);
            P[6] = new Point3d(-1, 1, 1);
            P[7] = new Point3d(1, 1, 1);

            E = new EdgeTab[8];
            E[0] = new EdgeTab(3, 0, 8);
            E[1] = new EdgeTab(1, 0, 9);
            E[2] = new EdgeTab(1, 2, 10);
            E[3] = new EdgeTab(3, 2, 11);
            E[4] = new EdgeTab(7, 4, 8);
            E[5] = new EdgeTab(5, 4, 9);
            E[6] = new EdgeTab(5, 6, 10);
            E[7] = new EdgeTab(7, 6, 11);

        }


        public bool FindEdgeToCollapse(out Axe edge,params int[] p)
        {
            edge = Axe.X;
            int count = p.Length;

            for (int i = 0; i < count; i++)
            {
                for (int j = i + 1; j < count; j++)
                {
                    if (E[p[i]][0] == E[p[j]][0]) { edge = Axe.X; return true; }
                    if (E[p[i]][1] == E[p[j]][1]) { edge = Axe.Y; return true; }
                    if (E[p[i]][2] == E[p[j]][2]) { edge = Axe.Z; return true; }
                }
            }
            return false;
        }

        public Triangle GetCase(int pi)
        {
            return E[pi].edge;
        }



        public IList<Triangle> GetCase(int pi, int pj)
        {
            Axe edge;
            List<Triangle> triangles = new List<Triangle>();

            if (FindEdgeToCollapse(out edge, pi, pj))
            {
                int axe = (int)edge;

                Triangle ti = new Triangle(pi

            }
            return triangles;
        }


    }
}
