﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GenericClass
{
    public abstract class HalfVertex
    {
        public int ID = -1;

        public HalfVertex()
        {

        }
    }

    public abstract class HalfEdge<V, E, F>
        where V : HalfVertex, new()
        where E : HalfEdge<V, E, F>, new()
        where F : HalfFace<V, E, F>, new()
    {
        public int ID = -1;

        public V[] v = new V[2];
        public F[] f = new F[2];

        public HalfEdge()
        {
        }

        public override string ToString()
        {
            return string.Format("e{0}({1},{2}) f{3} f{4}", ID, (v[0]).ID, (v[1]).ID, (f[0] != null) ? f[0].ID.ToString() : "-", (f[1] != null) ? f[1].ID.ToString() : "-");
        }
    }

    public abstract class HalfFace<V, E, F>
        where V : HalfVertex, new()
        where E : HalfEdge<V, E, F>, new()
        where F : HalfFace<V, E, F>, new()
    {
        public int ID = -1;
        public E[] e = new E[3];
        public V[] v = new V[3];

        public HalfFace()
            : this(-1)
        { }

        public HalfFace(int id)
        {
            this.ID = id;
        }


        public override string ToString()
        {
            return string.Format("f{0} [{1} {2} {3}] -> {4} {5} {6}", ID, (v[0]).ID, (v[1]).ID, (v[2]).ID, e[0], e[1], e[2]);
        }
    }


    public interface ILink<T> where T : class
    {
        T Next { get; set; }
        T Prev { get; set; }
        bool marked2remove { get; }
    }

    public class VertNode : HalfVertex, ILink<VertNode>
    {
        public VertNode Next { get; set; }
        public VertNode Prev { get; set; }
        public bool onhull = false;
        public bool processed = false;
        public bool marked2remove
        {
            get { throw new NotImplementedException(); }
        }

        public VertNode()
            : base()
        {
        }

        public VertNode(float vector, int idx)
            : base()
        {
            base.ID = idx;
        }
    }
    public class EdgeNode : HalfEdge<VertNode, EdgeNode, FaceNode>, ILink<EdgeNode>
    {
        public EdgeNode Next { get; set; }
        public EdgeNode Prev { get; set; }
        public bool marked2remove
        {
            get { throw new NotImplementedException(); }
        }


        public EdgeNode()
            : base()
        {

        }
        public EdgeNode(VertNode a, VertNode b)
            : base()
        {
            v[0] = a;
            v[1] = b;
        }

    }
    public class FaceNode : HalfFace<VertNode, EdgeNode, FaceNode>, ILink<FaceNode>
    {
        public FaceNode Next { get; set; }
        public FaceNode Prev { get; set; }
        public bool marked2remove
        {
            get { throw new NotImplementedException(); }
        }


        public FaceNode()
            : base()
        {
        }

        public FaceNode(VertNode a, VertNode b, VertNode c)
            : base()
        {
            v[0] = a;
            v[1] = b;
            v[2] = c;
            e[0] = new EdgeNode(a, b);
            e[1] = new EdgeNode(b, c);
            e[2] = new EdgeNode(c, a);
        }
    }
}
