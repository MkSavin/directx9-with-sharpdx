﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GenericClass
{



    public class Program
    {
        public static void Main(string[] args)
        {
            VertNode v0 = new VertNode(1.0f, 0);
            VertNode v1 = new VertNode(2.0f, 1);
            VertNode v2 = new VertNode(3.0f, 2);
            EdgeNode e0 = new EdgeNode(v0, v1);
            EdgeNode e1 = new EdgeNode(v1, v2);
            EdgeNode e2 = new EdgeNode(v2, v0);
            FaceNode f0 = new FaceNode(v0, v1, v2);

            
            Console.ReadKey();
        }
    }
}
