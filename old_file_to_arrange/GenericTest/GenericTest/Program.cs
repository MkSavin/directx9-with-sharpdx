﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Text;
using System.Collections.Generic;

using Engine.Tools;

namespace GenericTest
{

    public class Node3class
    {
        public readonly Node3 Next ;
        public readonly Node3 Prev ;
        public int id;

        public Node3class(int id)
        {
            this.id = id;
        }
        public override string ToString()
        {
            return id < 0 ? "-1" : id.ToString();
        }
    }

    public struct Node3
    {
        public int id;
    }




    public class Node2
    {
        public readonly Node2 Next ;
        public readonly Node2 Prev ;

        public int id = 0;

        public Node2(int id, bool preinitialize = false)
        {
            this.id = id;
            if (preinitialize)
            {
                this.Prev = new Node2(id + 1, false);
                this.Next = new Node2(id + 2, false);
            }
        }
        public override string ToString()
        {
            return id < 0 ? "-1" : id.ToString();
        }
    }





    public class Node : ILink<Node>
    {
        public Node Next { get; set; }
        public Node Prev { get; set; }

        public int id = 0;

        public Node(int id)
        {
            this.id = id;
        }
        public override string ToString()
        {
            return id < 0 ? "-1" : id.ToString();
        }
    }




    class Program
    {
        static void TestEnumerable(BitArray1D bitarray)
        {
            foreach (bool item in bitarray)
            {
                //Console.Write(item);
                //Console.Write(" ");
            }

        }

        static void TestEnumerator(BitArray1D bitarray)
        {
            foreach (bool item in bitarray.BooleanList)
            {
                //Console.Write(item);
                //Console.Write(" ");
            }
            //foreach (int item in bitarray.IndicesList)
            {
                //Console.Write(item);
                //Console.Write(" ");
            }
        }

        static void TestEnumeratorIdx(BitArray1D bitarray)
        {
            foreach (int item in bitarray.IndicesList)
            {
                //Console.Write(item);
                //Console.Write(" ");
            }

        }


        static void TestCircular()
        {       
            CircularLinkedList2<Node> list = new CircularLinkedList2<Node>();

            Node n0 = new Node(0); list.AddLast(n0);
            Node n1 = new Node(1); list.AddLast(n1);
            Node n2 = new Node(2); list.AddLast(n2);
            Node n3 = new Node(3); list.AddLast(n3);

            list.Invert();

            Console.WriteLine("Enumerable:");

            foreach(Node node in list)
            {
                Console.WriteLine(node);
            }

            CircularEnumerator<Node> enumerator1 = new CircularEnumerator<Node>(n0);
            Console.WriteLine("enumerator1:");
            foreach (Node node in enumerator1)
            {
                Console.WriteLine(node);
            }
            
            CircularEnumerator<Node> enumerator2 = new CircularEnumerator<Node>(n2);
            Console.WriteLine("enumerator2:");
            foreach (Node node in enumerator2)
            {
                Console.WriteLine(node);
            }
            
            Node[] array = list.ToArray();

            Console.ReadKey();        
        }


        static void TestProperties()
        {
            Stopwatch sw = new Stopwatch();
            
            Node2 node0 = new Node2(0,true);
            
            Node node1 = new Node(0);
            node1.Next = new Node(1);
            node1.Prev = new Node(2);

            Node3class node3 = new Node3class(0);

            sw.Start();
            for (int i = 1; i < 1000000; i++)
            {
                node0.Next.id = -1;
            }
            sw.Stop();
            Console.WriteLine("Readonly = {0}", sw.ElapsedTicks);
            
            sw.Reset();
            sw.Start();
            for (int i = 1; i < 1000000; i++)
            {
                node1.Next.id = -1;
            }
            sw.Stop();
            Console.WriteLine("Get = {0}", sw.ElapsedTicks);


            sw.Reset();
            sw.Start();
            for (int i = 1; i < 1000000; i++)
            {
                Node3 node = node3.Next;
                node.id = -1;

            }
            sw.Stop();
            Console.WriteLine("Readonly struct = {0}", sw.ElapsedTicks);
        }



        public static void Main(string[] args)
        {

            TestProperties();


        }
    }
}
