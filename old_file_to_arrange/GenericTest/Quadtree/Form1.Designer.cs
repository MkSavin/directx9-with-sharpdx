﻿namespace QuadtreeSpace
{
    partial class Form1
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Liberare le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.trackBarYoffset = new System.Windows.Forms.TrackBar();
            this.label1 = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.trackBarXoffset = new System.Windows.Forms.TrackBar();
            this.label3 = new System.Windows.Forms.Label();
            this.trackBarZoffset = new System.Windows.Forms.TrackBar();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarYoffset)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarXoffset)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarZoffset)).BeginInit();
            this.SuspendLayout();
            // 
            // treeView1
            // 
            this.treeView1.Dock = System.Windows.Forms.DockStyle.Left;
            this.treeView1.Location = new System.Drawing.Point(0, 0);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(205, 681);
            this.treeView1.TabIndex = 0;
            this.treeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterSelect);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(205, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(865, 535);
            this.panel1.TabIndex = 1;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            this.panel1.Resize += new System.EventHandler(this.panel1_Resize);
            // 
            // trackBarYoffset
            // 
            this.trackBarYoffset.LargeChange = 10;
            this.trackBarYoffset.Location = new System.Drawing.Point(60, 3);
            this.trackBarYoffset.Maximum = 100;
            this.trackBarYoffset.Name = "trackBarYoffset";
            this.trackBarYoffset.Size = new System.Drawing.Size(793, 45);
            this.trackBarYoffset.TabIndex = 0;
            this.trackBarYoffset.TickFrequency = 10;
            this.trackBarYoffset.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.trackBarYoffset.ValueChanged += new System.EventHandler(this.trackBarYoffset_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Elevation";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.label1);
            this.flowLayoutPanel1.Controls.Add(this.trackBarYoffset);
            this.flowLayoutPanel1.Controls.Add(this.label2);
            this.flowLayoutPanel1.Controls.Add(this.trackBarXoffset);
            this.flowLayoutPanel1.Controls.Add(this.label3);
            this.flowLayoutPanel1.Controls.Add(this.trackBarZoffset);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(205, 535);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(865, 146);
            this.flowLayoutPanel1.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Xoffset";
            // 
            // trackBarXoffset
            // 
            this.trackBarXoffset.LargeChange = 10;
            this.trackBarXoffset.Location = new System.Drawing.Point(49, 54);
            this.trackBarXoffset.Maximum = 100;
            this.trackBarXoffset.Name = "trackBarXoffset";
            this.trackBarXoffset.Size = new System.Drawing.Size(804, 45);
            this.trackBarXoffset.TabIndex = 2;
            this.trackBarXoffset.TickFrequency = 10;
            this.trackBarXoffset.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.trackBarXoffset.Value = 50;
            this.trackBarXoffset.ValueChanged += new System.EventHandler(this.trackBarXoffset_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 102);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Zoffset";
            // 
            // trackBarZoffset
            // 
            this.trackBarZoffset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.trackBarZoffset.LargeChange = 10;
            this.trackBarZoffset.Location = new System.Drawing.Point(49, 105);
            this.trackBarZoffset.Maximum = 100;
            this.trackBarZoffset.Name = "trackBarZoffset";
            this.trackBarZoffset.Size = new System.Drawing.Size(804, 45);
            this.trackBarZoffset.TabIndex = 4;
            this.trackBarZoffset.TickFrequency = 10;
            this.trackBarZoffset.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.trackBarZoffset.Value = 50;
            this.trackBarZoffset.ValueChanged += new System.EventHandler(this.trackBarZoffset_ValueChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1070, 681);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.treeView1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.trackBarYoffset)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarXoffset)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarZoffset)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.TrackBar trackBarYoffset;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.TrackBar trackBarXoffset;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.TrackBar trackBarZoffset;
    }
}

