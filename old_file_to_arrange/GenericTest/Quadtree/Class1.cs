﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Text;

using Engine.Maths;
using Engine.Partitions;

namespace QuadtreeSpace
{
    public class MyQuadNode : QuadNode<MyQuadNode>
    {
        public bool selected = false;
        public bool treeviewselection = false;
        public bool degenerate = false;

        public MyQuadNode()
            : base() { }

        public MyQuadNode(Quadtree<MyQuadNode> main)
            : base(main) { }

        public MyQuadNode(MyQuadNode parent, int index)
            : base(parent, index) { }
    }


    public class MySelectSpheres
    {
        public float morth = 0.2f;

        public Vector3 center;
        public float[] radius;
        public float[] morthradius;
        public Pen[] pens;

        float[] dashValues = { 0.01f,0.01f};


        /// <summary>
        /// </summary>
        /// <param name="position"> where plane is XZ and height is Y</param>
        public MySelectSpheres(Vector3 position, float[] radius, Pen[] pens , bool tratteggio)
        {
            this.radius = radius;
            this.center = position;
            this.pens = (Pen[])pens.Clone();

            morthradius = new float[radius.Length];

            float prevradius = 0.0f;
            for (int level = 0; level < radius.Length; level++)
            {
                morthradius[level] = prevradius + (radius[level] - prevradius) * (1 - morth);
                prevradius = radius[level];
            }

        }

        public Circle GetSphereProjeted(int level, float elevation)
        {
            float h = (center.y - elevation) * (center.y - elevation);
            float r = radius[level] * radius[level];

            if (h <= r)
            {
                r = (float)Math.Sqrt(r - h);
                return new Circle(center.x, center.z, r);
            }
            return Circle.Empty;
        }

        /// <summary>
        /// Draw the sphere projection to screen plane by its height
        /// </summary>
        public void Draw(Graphics g, float elevation , float penwidth)
        {
            /*
            Color[] colors = { Color.Transparent, Color.Black, Color.Black, Color.Black };
            float[] relativePositions = { 0, morth ,morth, 1 };
            
            ColorBlend colorBlend = new ColorBlend();
            colorBlend.Positions = relativePositions;
            */

            for (int l = radius.Length-1; l >=0; l--)
            {
                Circle c = GetSphereProjeted(l, elevation);

                if (c.radius>0)
                {
                    /*
                    GraphicsPath path = new GraphicsPath();
                    path.AddEllipse(c.center.x - c.radius, c.center.y - c.radius, c.radius * 2, c.radius * 2);


                    Color col = Color.FromArgb(100, pens[l].Color.R, pens[l].Color.G, pens[l].Color.B);

                    colors[1] = colors[2] = colors[3] = col;
                    colorBlend.Colors = colors;
                                 
                    PathGradientBrush pthGrBrush = new PathGradientBrush(path);  
                    pthGrBrush.InterpolationColors = colorBlend;
                    */
                    pens[l].Width = penwidth;

                    g.DrawEllipse(pens[l], c.center.x - c.radius, c.center.y - c.radius, c.radius * 2, c.radius * 2);
                    
                    //g.FillRectangle(pthGrBrush, c.center.x - c.radius, c.center.y - c.radius, c.radius * 2, c.radius * 2);

                }
            }
        }
    }

    public class CdlodSelector : QuadNodesEnumerator<MyQuadNode>
    {
        MySelectSpheres range;
        public List<MyQuadNode> selected = new List<MyQuadNode>();

        float elevation;

        public CdlodSelector(MyQuadNode root, MySelectSpheres range, float elevation)
            : base(root)
        {
            UpdateSpheres(range, elevation);
        }


        public void UpdateSpheres(MySelectSpheres range, float elevation)
        {
            this.range = range;
            this.elevation = elevation;
            this.selected.Clear();

            stack.Push(root);

            while (stack.Count > 0)
            {
                current = stack.Pop();
                current.selected = false;

                Circle circle = range.GetSphereProjeted(current.level, elevation);
                bool intersect_parent = PrimitiveIntersections.IntersectAABRCircle(current.Min, current.Max, circle.center, circle.radius);

                if (intersect_parent)
                {
                    // check if children are selected
                    if (current.level > 0)
                    {
                        bool intersect_child = false;

                        for (int i = 3; i >= 0; i--)
                        {
                            MyQuadNode child = current.child[i];
                            child.selected = false;
                            circle = range.GetSphereProjeted(current.level - 1, elevation);

                            bool check = PrimitiveIntersections.IntersectAABRCircle(child.Min, child.Max, circle.center, circle.radius);

                            intersect_child |= check;

                            if (!check)
                            {
                                selected.Add(child);
                                child.selected = true;
                            }
                            else stack.Push(current.child[i]);
                        }

                    }
                    else
                    {
                        current.selected = true;
                        selected.Add(current);
                    }
                }
            }

        }

        public override bool MoveNext()
        {
            if (stack.Count == 0) return false;

            current = stack.Pop();

            if (!current.IsLeaf)
            {
                for (int i = 3; i >= 0; i--)
                    if (current.child[i] != null)
                        stack.Push(current.child[i]);
            }

            count++;
            return true;
        }

    }
}
