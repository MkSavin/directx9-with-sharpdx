﻿using Engine.Maths;
using Engine.Partitions;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace QuadtreeSpace
{
    static class Program
    {
        /// <summary>
        /// Punto di ingresso principale dell'applicazione.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }


}
