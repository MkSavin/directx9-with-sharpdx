﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;


using Engine.Partitions;
using Engine.Maths;

namespace QuadtreeSpace
{

    public partial class Form1 : Form
    {
        const int Depth = 6;

        Quadtree<MyQuadNode> quadtree;
        QuadNodesEnumerator<MyQuadNode> enumerator;
        CdlodSelector selector;

        Pen[] levelcolor;

        /// <summary>
        /// f0 is the maximum range and is used to select
        /// f1 is the minimum range where start morthing
        /// </summary>
        MySelectSpheres spheres_f0, spheres_f1;

        HatchBrush selectcolor = new HatchBrush(HatchStyle.ForwardDiagonal, Color.Red, Color.Transparent);

        SolidBrush complete_f1 = new SolidBrush(Color.FromArgb(100, 255, 0, 0));
        SolidBrush complete_f0 = new SolidBrush(Color.FromArgb(100, 0, 0, 255));
        SolidBrush transition_f1_f0 = new SolidBrush(Color.FromArgb(100, 0, 255, 0));
 
        
        MyQuadNode current = null;

        // plot coordinate
        float mMinX = 0.0f;
        float mMinY = 0.0f;
        float mMaxY = 1.0f;
        float mMaxX  = 1.0f;
        float mFunctionPenSize = 1.0f;
        PointF[] controlCorner = new PointF[3];
        RectangleF rectanglePlot = new RectangleF();

        public Form1()
        {
            InitializeComponent();

            // generate quadtree
            quadtree = new Quadtree<MyQuadNode>(Depth,new RectangleAA(mMinX, mMinY, mMaxX, mMaxY));
            MyQuadNode root = new MyQuadNode(quadtree);
            quadtree.root = root;
            root.RecursiveSplit();

            enumerator = new QuadNodesEnumerator<MyQuadNode>(root);

            levelcolor = new Pen[Depth];

            for (int i = Depth - 1; i >= 0; i--)
                levelcolor[i] = new Pen(Color32.Rainbow(i / ((float)Depth - 1)));

            MakeRange();

            selector = new CdlodSelector(root, spheres_f0,Elevation);

            // generate tree list
            this.treeView1.TabStop = false;
            this.treeView1.Nodes.Add(root.GetDebugView());
            
        }

        void MakeRange()
        {
            float[] range_f0 = new float[Depth];
            float[] range_f1 = new float[Depth];

            float radius_min = (float)Math.Sqrt((mMaxX - mMinX) * (mMaxX - mMinX) + (mMaxY - mMinY) * (mMaxY - mMinY));
            float radius_max = (mMaxX - mMinX) * 2f;

            radius_min *= 0.999f;
            radius_max *= 0.999f;

            for (int i = Depth - 1; i >= 0; i--)
            {

                float hue = i / ((float)Depth - 1);
                Color color = Color32.Rainbow(hue);
                levelcolor[i] = new Pen(color);

                range_f0[i] = radius_max;
                range_f1[i] = radius_min;


                radius_max /= 2f;
                radius_min /= 2f;

            }


            spheres_f0 = new MySelectSpheres(Vector3.zero, range_f0,levelcolor,false);
            spheres_f1 = new MySelectSpheres(Vector3.zero, range_f1,levelcolor,true);
        }


        float Xoffset { get { return (trackBarXoffset.Value ) / (float)this.trackBarXoffset.Maximum; } }
        float Zoffset { get { return (trackBarZoffset.Value ) / (float)this.trackBarZoffset.Maximum; } }
        float Elevation { get { return trackBarYoffset.Value / (float)this.trackBarYoffset.Maximum; } }


        // generate a transform matrix to mach my coordinate system with windows bitmap draw
        void ChangeTranformMatrix(Graphics g , Rectangle rect)
        {
            float Xmargin = 20;
            float Ymargin = 20;


            rectanglePlot.x = mMinX;
            rectanglePlot.y = mMaxY;
            rectanglePlot.Width = mMaxX - mMinX;
            rectanglePlot.Height = mMinY - mMaxY;

            controlCorner[0] = new PointF(Xmargin, Ymargin);
            controlCorner[1] = new PointF(rect.Width-Xmargin, Ymargin);
            controlCorner[2] = new PointF(Xmargin, rect.Height - Ymargin);

            float mXAxisPensize = (mMaxX - mMinX) / rect.Width ;
            float mYAxisPensize = (mMaxY - mMinY) / rect.Height ;

            mFunctionPenSize = Math.Min(mXAxisPensize, mYAxisPensize);

            foreach (Pen pen in levelcolor) pen.Width = mFunctionPenSize;
            
            g.Transform = new Matrix(rectanglePlot, controlCorner);

            g.TranslateTransform(mFunctionPenSize , -mFunctionPenSize );
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            Panel p = (Panel)sender;
            Graphics g = e.Graphics;
            SolidBrush brush;

            g.Clear(this.panel1.BackColor);
            g.SmoothingMode = SmoothingMode.HighSpeed;

            if (p.ClientSize.Width <= 0 && p.ClientSize.Height <= 0) return;
            ChangeTranformMatrix(g, p.ClientRectangle);

            RectangleF quadrect = new RectangleF();


            spheres_f0.center.x = Xoffset;
            spheres_f0.center.z = Zoffset;
            spheres_f1.center.x = Xoffset;
            spheres_f1.center.z = Zoffset;


            selector.UpdateSpheres(spheres_f0, Elevation);

            foreach (MyQuadNode node in selector.selected)
            {
                quadrect.x = node.Min.x;
                quadrect.y = node.Min.y;
                quadrect.Width = node.HalfSize.x * 2;
                quadrect.Height = node.HalfSize.y * 2;

                if (node.level ==0)
                {

                    Circle circle_f1 = spheres_f1.GetSphereProjeted(node.level+1 , Elevation);
                    Circle circle_f0 = spheres_f0.GetSphereProjeted(node.level+1, Elevation);

                    PrimitiveIntersections.OverlapType type0, type1;

                    bool test0 = PrimitiveIntersections.IntersectAABRCircle(node.Min, node.Max, circle_f1.center, circle_f0.radius, out type0);
                    bool test1 = PrimitiveIntersections.IntersectAABRCircle(node.Min, node.Max, circle_f1.center, circle_f1.radius, out type1);

                    if (type0 == PrimitiveIntersections.OverlapType.CompleteOutsize)
                        brush = complete_f0;
                    else if (type1 == PrimitiveIntersections.OverlapType.CompleteInside)
                        brush = complete_f1;
                    else
                        brush = transition_f1_f0;
                    g.FillRectangle(brush, quadrect.x, quadrect.y, quadrect.Width, quadrect.Height);
                }
                g.DrawRectangle(levelcolor[node.level], quadrect.x, quadrect.y, quadrect.Width, quadrect.Height);
                
            }

            TreeNode selectnode = treeView1.SelectedNode;

            if (selectnode != null)
            {
                MyQuadNode node = (selectnode.Tag as MyQuadNode);
                quadrect.x = node.Min.x;
                quadrect.y = node.Min.y;
                quadrect.Width = node.HalfSize.x * 2;
                quadrect.Height = node.HalfSize.y * 2;
                g.FillRectangle(selectcolor, quadrect);
            }

            spheres_f0.Draw(g, Elevation, mFunctionPenSize);
            spheres_f1.Draw(g, Elevation, mFunctionPenSize);
        }

        private void panel1_Resize(object sender, EventArgs e)
        {
            ((Panel)sender).Invalidate();
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            try
            {
                if (current != null) current.selected = false;
                current = (MyQuadNode)(e.Node.Tag);
                if (current != null) current.selected = true;
                this.panel1.Invalidate();

            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.ToString());
            }
        }

        private void trackBarYoffset_ValueChanged(object sender, EventArgs e)
        {
            this.panel1.Invalidate();
        }

        private void trackBarXoffset_ValueChanged(object sender, EventArgs e)
        {
            this.panel1.Invalidate();
        }

        private void trackBarZoffset_ValueChanged(object sender, EventArgs e)
        {
            this.panel1.Invalidate();
        }
    }
}
