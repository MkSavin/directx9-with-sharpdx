//-----------------------------------------------------------------------------
// Globals.
//-----------------------------------------------------------------------------
float4x4 WorldViewProj : WORLDVIEWPROJ;
float2   xLevelOrigin;
float2   xPatchOrigin;
float2   xPatchScaleF;
float4   xPatchColor;
float4   xTerrainBound; //min-max
float3   xLightDir;
float3   xEye;

Texture  xHeightMap;

sampler textureSampler = sampler_state 
{ 
	texture = <xHeightMap>;
	magfilter = LINEAR;
	minfilter = LINEAR;
	mipfilter = LINEAR;
	AddressU  = CLAMP; 
	AddressV  = WRAP; //CLAMP;
};

//-----------------------------------------------------------------------------
// Vertex Shaders.
//-----------------------------------------------------------------------------
struct VS_INPUT
{
    float2  Position  : POSITION;
};

struct VS_OUTPUT
{   
	float4 Position : POSITION;
    float4 Color    : COLOR0;
	float2 TexCoord : TEXCOORD0;
	float3 Normal   : TEXCOORD1;
	float3 View     : TEXCOORD2;
};


VS_OUTPUT VShader(VS_INPUT input)
{
	VS_OUTPUT output = (VS_OUTPUT)0; 

	// convert unit vertex in global position
	output.Position.x = (input.Position.x + xPatchOrigin.x) * xPatchScaleF.x + xLevelOrigin.x;
	output.Position.z = (input.Position.y + xPatchOrigin.y) * xPatchScaleF.y + xLevelOrigin.y;
	output.Position.w = 1;


	// find texcoord in the range [0,1]
	output.TexCoord.x = (output.Position.x + xEye.x - xTerrainBound.x) / (xTerrainBound.z - xTerrainBound.x);
	output.TexCoord.y = (output.Position.z + xEye.z - xTerrainBound.x) / (xTerrainBound.z - xTerrainBound.x);

	// get height from map
	output.Position.y = tex2Dlod(textureSampler, float4(output.TexCoord,0,1)).a * 10.0f;
	output.Normal = tex2Dlod(textureSampler, float4(output.TexCoord,0,1)).xyz * 2.0f - 1.0f;
	// trasform to screen
	output.Position = mul(output.Position, WorldViewProj);
	output.View = xEye - output.Position.xyz;
	return output;
}

//-----------------------------------------------------------------------------
// Pixel Shaders.
//-----------------------------------------------------------------------------

float4 PShader(VS_OUTPUT input) : COLOR0
{
	//float3 normal = normalize(tex2D(textureSampler, input.TexCoord).xyz * 2.0f - 1.0f);
	float3 normal = input.Normal;
	
	float4 diff   = saturate(dot(normal, -xLightDir));
	//float3 Reflect = normalize(2 * diff * input.Normal - xLightDir); 
	//float4 specular = pow(saturate(dot(Reflect, input.View)), 2); // R.V^n
	
	return xPatchColor * diff;// + specular;
}
float4 PShader_black(VS_OUTPUT input) : COLOR0
{
	return float4(0,0,0,0);
}
float4 PShader_texture(VS_OUTPUT input) : COLOR0
{
	 return tex2D(textureSampler, input.TexCoord);
}
//-----------------------------------------------------------------------------
// Techniques.
//-----------------------------------------------------------------------------
technique DefaultTechique
{
    pass p0
    {
		CullMode = CCW;
		FillMode = Solid;
		DepthBias = 0;
        VertexShader = compile vs_3_0 VShader();
		PixelShader =  compile ps_3_0 PShader();
    }
}
technique WireframeTechique2
{
    pass p0
    {
		CullMode = CCW;
		FillMode = Solid;
		DepthBias = 0;
        VertexShader = compile vs_3_0 VShader();
		PixelShader =  compile ps_3_0 PShader();
    }
	pass p1
    {
		CullMode = CCW;
		FillMode = Wireframe;
		DepthBias = -0.00001f;
        VertexShader = compile vs_3_0 VShader();
		PixelShader =  compile ps_3_0 PShader_black();
    }
}