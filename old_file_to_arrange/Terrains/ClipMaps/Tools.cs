﻿using System;
using System.Collections.Generic;
using System.Drawing;

using Engine.Graphics;
using Engine.Maths;
using Engine.Tools;
using Engine.Renderer;

namespace Engine.ClipMaps
{
    public class AxisObj : IDrawable
    {
        MaterialFX_VCOLOR material;
        GraphicDevice device;
        VertexBuffer axis;
        VertexDeclaration cvertex_decl;

        public AxisObj(GraphicDevice device)
        {
            this.material = new MaterialFX_VCOLOR(device);
            this.device = device;
            this.cvertex_decl = new VertexDeclaration(device, CVERTEX.m_elements);

            CVERTEX[] verts = new CVERTEX[]
            {
                new CVERTEX(0,0,0, Color.Red),
                new CVERTEX(50,0,0, Color.Red),
                new CVERTEX(0,0,0, Color.Green),
                new CVERTEX(0,50,0, Color.Green),
                new CVERTEX(0,0,0, Color.Blue),
                new CVERTEX(0,0,50, Color.Blue)
            };
            axis = new VertexBuffer(device, cvertex_decl.Format, verts.Length, false, true);

            VertexStream vdata = axis.OpenStream();
            vdata.WriteCollection<CVERTEX>(verts, 0, verts.Length, 0);
            axis.CloseStream();
            axis.Count = verts.Length;
        }

        public Matrix4 transform
        {
            get { return Matrix4.Identity; }
            set { }
        }
        public void SetCamera(ICamera camera)
        {
            throw new NotImplementedException();
        }

        public void Draw(ICamera camera)
        {
            material.ProjViewWorld = camera.Projection * camera.View;
            cvertex_decl.SetToDevice();
            axis.SetToDevice();

            int count = material.Begin();
            for (int pass = 0; pass < count; pass++)
            {
                material.BeginPass(pass);
                device.DrawPrimitives(PrimitiveType.LineList, 0, axis.Count / 2);
                material.EndPass();
            }
            material.End();
        }

        public void Draw()
        {
            throw new NotImplementedException();
        }

        public void Draw(MaterialBase material)
        {
            throw new NotImplementedException();
        }
        public void SetToDevice()
        {
            throw new NotImplementedException();
        }
        public void Render(MaterialBase material)
        {
            throw new NotImplementedException();
        }
    }

    public class FrustumObj
    {
        GraphicDevice m_device;
        VertexDeclaration cvertex_decl;
        VertexBuffer frustumV;
        IndexBuffer frustumE;
        Frustum m_frustum;
        

        public Frustum frustum
        {
            get { return m_frustum; }
            set { m_frustum = value; RenderFrustum(); }
        }

        public FrustumObj(GraphicDevice device)
        {
            m_device = device;
            cvertex_decl = new VertexDeclaration(device, CVERTEX.m_elements);
            frustumV = new VertexBuffer(device, cvertex_decl.Format, 10, false, true);
            frustumE = new IndexBuffer(device, IndexInfo.IndexEdge16, 13, false, true);
        }


        void RenderFrustum()
        {
            Edge16[] edges;
            CVERTEX[] verts;
            m_frustum.Render(out verts, out edges);

            VertexStream vdata = frustumV.OpenStream();
            vdata.WriteCollection<CVERTEX>(verts, 0, verts.Length, 0);
            frustumV.CloseStream();
            frustumV.Count = verts.Length;

            IndexStream idata = frustumE.OpenStream();
            idata.WriteCollection<Edge16>(edges, 0, edges.Length, 0);
            frustumE.CloseStream();
            frustumE.Count = edges.Length;
        }

        public void Draw()
        {
            cvertex_decl.SetToDevice();
            m_device.renderstates.lightEnable = false;
            if (frustumV.Count > 0)
            {
                cvertex_decl.SetToDevice();
                frustumV.SetToDevice();
                frustumE.SetToDevice();
                m_device.DrawIndexedPrimitives(PrimitiveType.LineList, 0, 0, frustumV.Count, 0, frustumE.Count);
            }
        }
    }

    public class Player
    {
        public float velocity = 0.5f; //units/seconds
        public Vector3 walkdirection = new Vector3(1, 0, 0);
        public Vector3 walkposition = new Vector3(0, 0, 0);
        public Vector3 walklookdirection = Vector3.GetNormal(new Vector3(1, -0.1f, 0));

        public float yangle = 0;
        
        Terrain m_terrain;

        public Player(Terrain walkterrain)
        {
            m_terrain = walkterrain;
            //walkposition.y = m_terrain.GetHeight(walkposition.x, walkposition.z) + 30;
            Move(0);
            yangle = 0;
        }
        public void Move(float dt)
        {
            if (yangle >= Math.PI * 2)
            {
                walklookdirection = Vector3.GetNormal((walklookdirection - Vector3.UnitY*velocity*0.1f));
                walkposition += Vector3.UnitY * velocity;
            }
            else
            {
                walklookdirection.x = (float)Math.Sin(yangle);
                walklookdirection.z = (float)Math.Cos(yangle);
                yangle += dt * velocity;
            }
        }
    }

}
