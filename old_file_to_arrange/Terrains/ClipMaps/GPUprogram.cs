﻿using System;
using System.Collections.Generic;
using System.Text;

using Engine.Graphics;
using Engine.Maths;

namespace Engine.ClipMaps
{
    public class ClipMapsMaterial : MaterialFX
    {
        public ClipMapsMaterial(GraphicDevice device)
            : base(device, Engine.ClipMaps.Properties.Resources.Clipmap)
        {
            effect.Parameters.Add("Technique", new EffectParamTechnique("DefaultTechique"));

            effect.Parameters.Add("Transform", new EffectParamMatrix(Matrix4.Identity, "WorldViewProj"));
            effect.Parameters.Add("LevelOrigin", new EffectParamVector2("xLevelOrigin"));
            effect.Parameters.Add("PatchOrigin", new EffectParamVector2("xPatchOrigin"));
            effect.Parameters.Add("PatchScaleF", new EffectParamVector2("xPatchScaleF"));
            effect.Parameters.Add("PatchColor", new EffectParamColor(Color32.Write, "xPatchColor"));
            effect.Parameters.Add("HeightMap", new EffectParamTexture(null, "xHeightMap"));
            effect.Parameters.Add("TerrainBound", new EffectAttributeVector4("xTerrainBound"));
            effect.Parameters.Add("LightDirection", new EffectParamVector3("xLightDir"));
            effect.Parameters.Add("CameraEye", new EffectParamVector3("xEye"));
        }

        public bool WireframeMode
        {
            get { return (string)effect.Parameters["Technique"] == "WireframeTechique2" ; }
            set { effect.Parameters["Technique"] = value ? "WireframeTechique2" : "DefaultTechique"; }
        }

        public Matrix4 ProjViewWorld
        {
            get { return (Matrix4)effect.Parameters["Transform"]; }
            set { effect.Parameters["Transform"] = (Matrix4)value; }
        }
        public Texture HeightMap
        {
            get { return (Texture)effect.Parameters["HeightMap"]; }
            set { effect.Parameters["HeightMap"] = (Texture)value; }
        }
        public Vector4 TerrainBound
        {
            get { return (Vector4)effect.Parameters["TerrainBound"]; }
            set { effect.Parameters["TerrainBound"] = (Vector4)value; }
        }
        public Vector2 LevelOrigin
        {
            get { return (Vector2)effect.Parameters["LevelOrigin"]; }
            set { effect.Parameters["LevelOrigin"] = (Vector2)value; }
        }
        public Vector2 PatchOrigin
        {
            get { return (Vector2)effect.Parameters["PatchOrigin"]; }
            set { effect.Parameters["PatchOrigin"] = (Vector2)value; }
        }
        public Vector2 PatchScaleFactor
        {
            get { return (Vector2)effect.Parameters["PatchScaleF"]; }
            set { effect.Parameters["PatchScaleF"] = (Vector2)value; }
        }
        public Vector3 LightDirection
        {
            get { return (Vector3)effect.Parameters["LightDirection"]; }
            set { effect.Parameters["LightDirection"] = (Vector3)value; }
        }
        public Vector3 CameraEye
        {
            get { return (Vector3)effect.Parameters["CameraEye"]; }
            set { effect.Parameters["CameraEye"] = (Vector3)value; }
        }
        public Color32 PatchColor
        {
            get { return (Color32)effect.Parameters["PatchColor"]; }
            set { effect.Parameters["PatchColor"] = (Color32)value; }
        }
        public override void ApplyParams()
        {
            effect.Parameters.UpdateParams(true);
        }

        public override void BeginPass(int pass)
        {
            effect.BeginPass(pass);
        }
    }
}
