﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Engine.ClipMaps
{
    public class ViewPanel : Panel
    {
        System.ComponentModel.IContainer components = null;
        Font drawFont = new Font("Arial", 20);
        SolidBrush drawBrush = new SolidBrush(Color.Black);

        public ViewPanel()
        {
            this.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint | ControlStyles.Opaque, true);
            InitializeComponent();
        }

        public delegate void OnRenderDelegate(ViewPanel panel);
        public OnRenderDelegate RenderFunction;


        protected override void OnPaint(PaintEventArgs e)
        {
            if (DesignMode)
            {
                base.OnPaint(e);
                e.Graphics.Clear(Color.White);
                e.Graphics.DrawString("OnPaint event override by directx", drawFont, drawBrush, 10, ClientSize.Height * 0.5f);       
            }
            else if (RenderFunction != null)
            {
                RenderFunction(this);
            }
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            // Don't call base to avoid fliker paint issue!
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            this.ResumeLayout(false);
        }
    }
}
