﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;

using Engine.Tools;
using Engine.Maths;
using Engine.Graphics;

namespace Engine.ClipMaps
{
    public class Terrain
    {
        Random rnd = new Random();
        ClipMapsMaterial material;
        GraphicDevice m_device;
        Texture heightmap;

        public ClipmapLevel[] Levels { get; private set; }

        public bool WireframeMode
        {
            get { return material.WireframeMode; }
            set { material.WireframeMode = value; }
        }

        public Terrain(GraphicDevice device)
        {
            MakeStaticPatch(device, 102);

            heightmap = new Texture(device, @"..\..\..\terrain.dds", true, 0);
            material = new ClipMapsMaterial(device);
            material.WireframeMode = false;
            material.HeightMap = heightmap;
            material.LightDirection = Vector3.GetNormal(new Vector3(-1, -1, -1));
            m_device = device;


            Levels = new ClipmapLevel[3];

            for (int i = 0; i < Levels.Length; i++)
            {
                Levels[i] = new ClipmapLevel(i);
            }
            // connectivity
            int maxlevel = Levels.Length - 1;
            for (int i = 0; i < Levels.Length; i++)
            {
                Levels[i].CoarserLevel = Levels[i <= 0 ? i : i - 1];
                Levels[i].FinerLevel = Levels[i >= maxlevel ? i : i + 1];
            }

            // bounding rettangle
            Vector2 max = new Vector2(100.0, 100.0);
            Vector2 min = -max;

            material.TerrainBound = new Vector4(min.x, min.y, max.x, max.y);

            float n = Terrain.N - 1;
            float m = Terrain.M - 1;

            Levels[0].levelScale = (max - min) / n;
            Levels[0].levelOrigin = min;

            for (int i = 1; i < Levels.Length; i++)
            {
                Levels[i].levelOrigin = Levels[i - 1].levelOrigin + Levels[i - 1].levelScale * m;
                Levels[i].levelScale = Levels[i - 1].levelScale * 0.5f;
            }

        }

        public void Draw(ICamera camera)
        {
            StaticPatchMesh.DrawCount = 0;
            StaticPatchMesh.TrianglesCount = 0;

            material.ProjViewWorld = camera.Projection * camera.View;
            material.HeightMap = heightmap;
            material.CameraEye = camera.Eye;

            int maxDepth = 0;

            if (camera.Eye.y >= 20) maxDepth = 0;
            if (camera.Eye.y > 10 && camera.Eye.y < 20) maxDepth = 1;
            if (camera.Eye.y <= 10) maxDepth = 2;


            int numpass = material.Begin();
            for (int pass = 0; pass < numpass; pass++)
            {
                material.BeginPass(pass);

                for (int l = 0; l <= maxDepth; l++)
                    Levels[l].DrawLevel(material, camera, l == maxDepth);

                material.EndPass();
            }
            material.End();
        }

        #region ripetitive properties
        /// <summary>
        /// Build all ripetitive properties
        /// </summary>
        public static void MakeStaticPatch(GraphicDevice device, int MapSize)
        {
            int n = MapSize;
            int m = n / 4;
            int k = n % 4;
            int g = k;
            N = n + 1;
            M = m + 1;
            K = k + 1;
            G = g + 1;

            // build 4 static grid geometries
            if (m > 0)
            {
                Terrain.MxM = new StaticPatchMesh(device, m, m);
                if (k > 0)
                {
                    Terrain.MxK = new StaticPatchMesh(device, m, k);
                    Terrain.KxM = new StaticPatchMesh(device, k, m);
                    Terrain.KxK = new StaticPatchMesh(device, k, k);

                    if (g > 0)
                    {
                        Terrain.Or = new StaticPatchMesh(device, g, n + g);
                        Terrain.Ve = new StaticPatchMesh(device, n, g);
                    }
                }
            }
            Console.WriteLine(string.Format("N{0} M{1} K{2}", N, M, K));

            //build the 16 or 25 ripetitive level's patch
            int m2 = m * 2;

            // first build all MM patch
            // left down corner
            DL0 = new StaticPatch(Terrain.MxM, 0, 0);
            DL1 = new StaticPatch(Terrain.MxM, m, 0);
            DL2 = new StaticPatch(Terrain.MxM, 0, m);
            DL = new StaticPatch(Terrain.MxM, m, m);
            DL.color = DL0.color = DL1.color = DL2.color = Color32.Yellow;
            DL0.name = "DL0"; DL1.name = "DL1"; DL2.name = "DL2"; DL.name = "DL";

            // left top corner
            TL0 = new StaticPatch(Terrain.MxM, 0, n - m);
            TL1 = new StaticPatch(Terrain.MxM, m, n - m);
            TL2 = new StaticPatch(Terrain.MxM, 0, n - m2);
            TL = new StaticPatch(Terrain.MxM, m, n - m2);
            TL.color = TL0.color = TL1.color = TL2.color = Color32.Yellow;
            TL0.name = "TL0"; TL1.name = "TL1"; TL2.name = "TL2"; TL.name = "TL";

            // right down corner
            DR0 = new StaticPatch(Terrain.MxM, n - m, 0);
            DR1 = new StaticPatch(Terrain.MxM, n - m2, 0);
            DR2 = new StaticPatch(Terrain.MxM, n - m, m);
            DR = new StaticPatch(Terrain.MxM, n - m2, m);
            DR.color = DR0.color = DR1.color = DR2.color = Color32.Yellow;
            DR.name = "DR"; DR2.name = "DR2"; DR1.name = "DR1"; DR0.name = "DR0";

            // right top corner
            TR0 = new StaticPatch(Terrain.MxM, n - m, n - m);
            TR1 = new StaticPatch(Terrain.MxM, n - m2, n - m);
            TR2 = new StaticPatch(Terrain.MxM, n - m, n - m2);
            TR = new StaticPatch(Terrain.MxM, n - m2, n - m2);
            TR.color = TR0.color = TR1.color = TR2.color = Color32.Yellow;
            TR.name = "TR"; TR0.name = "TR0"; TR1.name = "TR1"; TR2.name = "TR2";

            // if exist conectivities blocks
            if (k > 0)
            {
                //vertical patch
                TT = new StaticPatch(Terrain.MxK, m2, n - m);
                T = new StaticPatch(Terrain.MxK, m2, n - m2);
                D = new StaticPatch(Terrain.MxK, m2, m);
                DD = new StaticPatch(Terrain.MxK, m2, 0);
                TT.color = T.color = DD.color = D.color = Color32.Cyano;
                TT.name = "TT"; T.name = "T"; D.name = "D"; DD.name = "DD";

                //orizontal patch
                LL = new StaticPatch(Terrain.KxM, 0, m2);
                L = new StaticPatch(Terrain.KxM, m, m2);
                R = new StaticPatch(Terrain.KxM, n - m2, m2);
                RR = new StaticPatch(Terrain.KxM, n - m, m2);
                LL.color = L.color = R.color = RR.color = Color32.Cyano;
                LL.name = "LL"; L.name = "L"; R.name = "R"; RR.name = "RR";

                //center patch
                C = new StaticPatch(Terrain.KxK, m2, m2);
                C.color = Color32.Blue;
                C.name = "C";

                //top gap (orizontal)
                O = new StaticPatch(Terrain.Or, 0, n);
                O.color = Color32.Red;
                O.name = "Orizontal";
                //right gap (vertical)
                V = new StaticPatch(Terrain.Ve, n, 0);
                V.color = Color32.Mangenta;
                V.name = "Vertical";
            }

        }

        /// <summary>
        /// N : Texture height map size of NxN
        /// </summary>
        public static int N, M, K, G;
        /// <summary> the static geometries </summary>
        public static StaticPatchMesh MxM { get; private set; }
        public static StaticPatchMesh MxK { get; private set; }
        public static StaticPatchMesh KxM { get; private set; }
        public static StaticPatchMesh KxK { get; private set; }
        /// <summary>orizontal gap 1xN </summary>
        public static StaticPatchMesh Or { get; private set; }
        /// <summary>vertical gap (N-1)x1 </summary>
        public static StaticPatchMesh Ve { get; private set; }
        // MM
        //top left corner
        public static StaticPatch TL0 { get; private set; }
        public static StaticPatch TL1 { get; private set; }
        public static StaticPatch TL2 { get; private set; }

        //top right corner
        public static StaticPatch TR0 { get; private set; }
        public static StaticPatch TR1 { get; private set; }
        public static StaticPatch TR2 { get; private set; }
        //down left corner
        public static StaticPatch DL0 { get; private set; }
        public static StaticPatch DL1 { get; private set; }
        public static StaticPatch DL2 { get; private set; }
        //down right corner
        public static StaticPatch DR0 { get; private set; }
        public static StaticPatch DR1 { get; private set; }
        public static StaticPatch DR2 { get; private set; }
        //interior ring
        public static StaticPatch TL { get; private set; }
        public static StaticPatch TR { get; private set; }
        public static StaticPatch DL { get; private set; }
        public static StaticPatch DR { get; private set; }
        // MK
        // top , down , interior top , interior down
        public static StaticPatch TT { get; private set; }
        public static StaticPatch DD { get; private set; }
        public static StaticPatch T { get; private set; }
        public static StaticPatch D { get; private set; }
        // KM
        // top , down , interior top , interior down
        public static StaticPatch LL { get; private set; }
        public static StaticPatch RR { get; private set; }
        public static StaticPatch L { get; private set; }
        public static StaticPatch R { get; private set; }
        // KK
        public static StaticPatch C { get; private set; }
        // 1xN
        public static StaticPatch O { get; private set; }
        // (N-1)x1
        public static StaticPatch V { get; private set; }
        #endregion
    }

    /// <summary>
    /// An unique patch in one levels
    /// </summary>
    public class ClipmapPatch
    {
        StaticPatch patch;

        //only for debug;
        public Color32 color;
        public string name;

        public ClipmapPatch(StaticPatch patch)
        {
            this.patch = patch;
        }

        public override string ToString()
        {
            return name + patch.ToString();
        }
    }

    /// <summary>
    /// An unique level in terrain
    /// </summary>
    public class ClipmapLevel
    {
        /// <summary> more detailed l+1 and less detailed l-1 </summary>
        public ClipmapLevel FinerLevel, CoarserLevel;
        /// <summary>
        /// the minx,minz origin of this level, all patch will be derived from this coordinate
        /// </summary>
        public Vector2 levelOrigin;
        /// <summary>
        /// the unit to float conversion of static patch's mesh
        /// </summary>
        public Vector2 levelScale;

        public ClipmapLevel(int level)
        {

        }

        bool isPatchVisible(StaticPatch patch,ICamera camera)
        {
            return true;
        }

        /// <summary>
        /// Draw all block of current level
        /// </summary>
        public void DrawLevel(ClipMapsMaterial material, ICamera camera, bool fillring)
        {
            material.LevelOrigin = levelOrigin;
            material.PatchScaleFactor = levelScale;

            // all MM patches
            if (isPatchVisible(Terrain.TL0, camera)) Terrain.TL0.DrawPatch(material);
            if (isPatchVisible(Terrain.TL1, camera)) Terrain.TL1.DrawPatch(material);
            if (isPatchVisible(Terrain.TL2, camera)) Terrain.TL2.DrawPatch(material);

            if (isPatchVisible(Terrain.DL0, camera)) Terrain.DL0.DrawPatch(material);
            if (isPatchVisible(Terrain.DL1, camera)) Terrain.DL1.DrawPatch(material);
            if (isPatchVisible(Terrain.DL2, camera)) Terrain.DL2.DrawPatch(material);

            if (isPatchVisible(Terrain.TR0, camera)) Terrain.TR0.DrawPatch(material);
            if (isPatchVisible(Terrain.TR1, camera)) Terrain.TR1.DrawPatch(material);
            if (isPatchVisible(Terrain.TR2, camera)) Terrain.TR2.DrawPatch(material);

            if (isPatchVisible(Terrain.DR0, camera)) Terrain.DR0.DrawPatch(material);
            if (isPatchVisible(Terrain.DR1, camera)) Terrain.DR1.DrawPatch(material);
            if (isPatchVisible(Terrain.DR2, camera)) Terrain.DR2.DrawPatch(material);

            // all conectivity block
            if (Terrain.K > 1)
            {
                Terrain.TT.DrawPatch(material);
                Terrain.RR.DrawPatch(material);
                Terrain.DD.DrawPatch(material);
                Terrain.LL.DrawPatch(material);

                // if not top level, draw also 2 gaps
                if (CoarserLevel != this && Terrain.G > 1)
                {
                    Terrain.V.DrawPatch(material);
                    Terrain.O.DrawPatch(material);
                }
            }


            // if last level, complete it with interiors
            if (fillring)
            {
                // interior
                Terrain.TL.DrawPatch(material);
                Terrain.DL.DrawPatch(material);
                Terrain.TR.DrawPatch(material);
                Terrain.DR.DrawPatch(material);
                // interior conectivity
                if (Terrain.K > 1)
                {
                    Terrain.T.DrawPatch(material);
                    Terrain.R.DrawPatch(material);
                    Terrain.D.DrawPatch(material);
                    Terrain.L.DrawPatch(material);
                    Terrain.C.DrawPatch(material);
                }

            }
        }
    }
}
