﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Runtime.InteropServices;
using System.Windows.Forms;

using Engine.Graphics;
using Engine.Maths;
using Engine.Tools;
using Engine.Renderer;

using Font = Engine.Graphics.Font;

namespace Engine.ClipMaps
{
    public partial class TerrainForm : Form
    {
        /// <summary>
        /// Punto di ingresso principale dell'applicazione.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //Application.EnableVisualStyles();
            //Application.SetCompatibleTextRenderingDefault(false);
            TerrainForm form = new TerrainForm();
            Application.Idle += new EventHandler(form.Application_Idle);
            Application.Run(form);
        }

        GraphicDevice device;
        TrackBallCamera_new camera;
        Terrain terrain;
        AxisObj axis;
        Font font;

        float yangle = 0;
        float l;
        int frames = 0;
        int LastTickCount = 0;
        int lastFramescount = 0;
        int StartTime = 0;
        Point start = new Point(0, 0);
        Point end = new Point(0, 0);

        void swap(ref int a, ref int b)
        {
            int t = a;
            a = b;
            b = t;
        }
        public TerrainForm()
        {
            this.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint | ControlStyles.Opaque, true);
            InitializeComponent();

            device = new GraphicDevice(this);
            font = new Font(device, Engine.Graphics.Font.FontName.Arial,8); 
            axis = new AxisObj(device);

            camera = new TrackBallCamera_new(this, new Vector3(-20, 20, -20), Vector3.Zero, Vector3.UnitY, 0.1f, 1000.0f);

            device.lights.Add(Light.Sun);
            
            terrain = new Terrain(device);

            StartTime = Environment.TickCount; 

        }

        protected override void OnPaint(PaintEventArgs e)
        {
            frames++;

            if (Math.Abs(Environment.TickCount - LastTickCount) > 1000)
            {
                lastFramescount = frames;
                LastTickCount = Environment.TickCount;
                frames = 0;
            }
            
            if (device.BeginDraw())
            {
                device.SetRenderTarghet();
                device.Clear(Color.CornflowerBlue);

                axis.Draw(camera);

                terrain.Draw(camera);

                font.Draw("FPS   : " + lastFramescount.ToString(), 10, 10, Color.Black);
                font.Draw("Draws : " + StaticPatchMesh.DrawCount, 10, 20, Color.Black);
                font.Draw("Tris  : " + StaticPatchMesh.TrianglesCount, 10, 30, Color.Black);
                font.Draw("Key to Wireframe", 10, 40, Color.Black);
                device.EndDraw();
                device.PresentDraw();
            }

        }
        protected override void OnKeyPress(KeyPressEventArgs e)
        {
            terrain.WireframeMode = !terrain.WireframeMode;
        }
    
        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            if (device == null) return;
            device.Resize(this.ClientSize);
        }

        #region GAME LOOP
        bool draw = true;
        protected void Application_Idle(object sender, EventArgs e)
        {
            while (AppStillIdle && draw)
            {
                System.Threading.Thread.Sleep(1); //reduce CPU usage 
                this.Invalidate();
            }
        }
        public bool AppStillIdle
        {
            get
            {
                PeekMsg msg;
                return !PeekMessage(out msg, IntPtr.Zero, 0, 0, 0);
            }
        }
        [System.Security.SuppressUnmanagedCodeSecurity]
        [DllImport("User32.dll", CharSet = CharSet.Auto)]
        private static extern bool PeekMessage(out PeekMsg msg, IntPtr hWnd,
                 uint messageFilterMin, uint messageFilterMax, uint flags);

        [StructLayout(LayoutKind.Sequential)]
        public struct PeekMsg
        {
            public IntPtr hWnd;
            public Message msg;
            public IntPtr wParam;
            public IntPtr lParam;
            public uint time;
            public System.Drawing.Point p;
        }
        #endregion
    }
}
