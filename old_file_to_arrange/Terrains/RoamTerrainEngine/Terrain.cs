﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

using Engine.Maths;
using Engine.Graphics;

namespace Engine.Roam
{
    public class Terrain
    {
        public static Random rnd = new Random();
        
        IMap m_map;
        Vector3 m_min;
        Vector3 m_max;
        Vector3 m_size;
        Frustum m_frustum;

        VertexDeclaration m_decl;
        VertexBuffer m_vertexbuff;
        IndexBuffer m_facebuff;
        
        // used because i don't know the side before render recursion
        internal List<NCVERTEX> m_verts; 
        internal List<Face> m_faces;
        internal float[] m_planesin;

        public NCVERTEX[] vertices { get; private set; }
        public Face[] triangles { get; private set; }

        public Patch[,] m_patchs { get; private set; }
        public int n_xpatch { get; private set; }
        public int n_zpatch { get; private set; }
        public int TotTriangles { get; private set; }
        

        public Terrain(Bitmap image, Vector3 mincorner, Vector3 maxcorner, int nXpatch, int nZpatch)
        {
            m_min = mincorner;
            m_max = maxcorner;
            m_size = new Vector3(
                Math.Abs(m_max.x - m_min.x),
                Math.Abs(m_max.y - m_min.y),
                Math.Abs(m_max.z - m_min.z));

            m_map = new BmpHeightMap(image);
            n_xpatch = nXpatch;
            n_zpatch = nZpatch;

            m_planesin = new float[6];
            m_verts = new List<NCVERTEX>();
            m_faces = new List<Face>();
            Init();
        }
        /// <summary>
        /// Initialize patchs array
        /// </summary>
        public void Init()
        {
            // build patchs
            m_patchs = new Patch[n_zpatch, n_xpatch];

            for (int j = 0; j < n_zpatch; j++)
                for (int i = 0; i < n_xpatch; i++)
                {
                    m_patchs[i, j] = new Patch(this, ref m_verts, ref m_faces,
                        m_min.x + i * m_size.x / n_xpatch,
                        m_min.z + j * m_size.z / n_xpatch,
                        m_size.x / n_xpatch,
                        m_size.z / n_zpatch);

                    m_patchs[i, j].name = "Patch." + i.ToString() + "." + j.ToString();
                    m_patchs[i, j].splitall = m_maxsplit;
                }

            // do connectivity
            for (int j = 0; j < n_zpatch; j++)
                for (int i = 0; i < n_xpatch; i++)
                {
                    m_patchs[i, j].m_Pdown = j > 0 ? m_patchs[i, j - 1] : null;
                    m_patchs[i, j].m_Pup = (j < n_zpatch - 1) ? m_patchs[i, j + 1] : null;
                    m_patchs[i, j].m_Pleft = (i > 0) ? m_patchs[i - 1, j] : null;
                    m_patchs[i, j].m_Pright = (i < n_xpatch - 1) ? m_patchs[i + 1, j] : null;
                    m_patchs[i, j].Reset();
                }
        }

        /// <summary>
        /// Get of Set the new frustum values, when set the sin() factor will be recalculated
        /// </summary>
        public Frustum frustum
        {
            get { return m_frustum; }
            set 
            {
                m_frustum = value;
                //precompute the sin() factor to adjust radius
                for (int i = 0; i < 6; i++)
                {
                    // notice that cos = Dot(Plane.Normal , CylinderY.Direction) with Direction = 0,1,0 the dot is simply Normal.y*1
                    float cos = m_frustum.m_plane[i].norm.y;
                    m_planesin[i] = (float)Math.Sqrt(1 - cos * cos);
                }
            }
        }
        /// <summary>
        /// Resets for new tesselation
        /// </summary>
        public void Reset()
        {
            for (int j = 0; j < n_zpatch; j++)
                for (int i = 0; i < n_xpatch; i++)
                    m_patchs[i, j].Reset();
        }
        /// <summary>
        /// tesselates all the patches. Using current frustum, build trinode's binary tree structure.
        /// After this call Render() to compute the geometry
        /// </summary>
        public void Tesselate()
        {
            for (int j = 0; j < n_zpatch; j++)
                for (int i = 0; i < n_xpatch; i++)
                    m_patchs[i, j].ClearFov();

            for (int j = 0; j < n_zpatch; j++)
                for (int i = 0; i < n_xpatch; i++)
                    m_patchs[i, j].Tesselate();
        }
        /// <summary>
        /// Return the height using world coodinates, remember that bitmap was center in
        /// 0,0,0 point. No interpolations was made for moment
        /// </summary>
        public float GetHeight(float x, float z)
        {
            x = (x - m_min.x) * m_map.Width / m_size.x;
            z = (z - m_min.z) * m_map.Lenght / m_size.z;
            int i, j;

            if (x < 0) i = 0;
            else if (x >= m_map.Width) i = m_map.Width - 1;
            else i = (int)x;

            if (z < 0) j = 0;
            else if (z >= m_map.Lenght) j = m_map.Lenght - 1;
            else j = (int)z;

            return m_min.y + m_map.GetH(i, j) * m_size.y;
        }      
        /// <summary>
        /// Clear all vertice and indices array , recalculate it. A tesselation must be made before it
        /// </summary>
        public void Render()
        {
            m_verts.Clear();
            m_faces.Clear();
            TotTriangles = 0;

            for (int j = 0; j < n_zpatch; j++)
                for (int i = 0; i < n_xpatch; i++)
                    m_patchs[i, j].ClearIndices();

            for (int j = 0; j < n_zpatch; j++)
                for (int i = 0; i < n_xpatch; i++)
                {
                    m_patchs[i, j].Render();
                    TotTriangles += m_patchs[i, j].tri_count;
                }

            vertices = m_verts.ToArray();
            triangles = m_faces.ToArray();

            for (int t = 0; t < triangles.Length; t++)
            {
                Face face = m_faces[t];
                Vector3 v0 = vertices[face.I].position;
                Vector3 v1 = vertices[face.J].position;
                Vector3 v2 = vertices[face.K].position;

                Vector3 e0 = v1 - v0;
                Vector3 e1 = v2 - v0;
                Vector3 e2 = v2 - v1;

                Vector3 n = Vector3.Cross(e0, e1);
                float dot0 = e0.LengthSq;
                float dot1 = e1.LengthSq;
                float dot2 = e2.LengthSq;

                if (dot0 < 0.0001) dot0 = 1.0f;
                if (dot1 < 0.0001) dot1 = 1.0f;
                if (dot2 < 0.0001) dot2 = 1.0f;

                vertices[face.I].normal += n * (1.0f / (dot0 * dot1));
                vertices[face.J].normal += n * (1.0f / (dot2 * dot0));
                vertices[face.K].normal += n * (1.0f / (dot1 * dot2));
            }
            for (int i = 0; i < vertices.Length; i++)
            {
                vertices[i].normal.Normalize();
            }
        }

        bool m_maxsplit = false;
        bool m_usefrustum = true;    
        /// <summary>
        /// If true all visible patch are splitted to maximum depth, used to debug
        /// </summary>
        public bool ForceMaximumSplitting
        {
            get
            { 
                return m_maxsplit;
            }
            set
            {
                if (value != m_maxsplit && m_patchs != null)
                        for (int j = 0; j < n_zpatch; j++)
                            for (int i = 0; i < n_xpatch; i++)
                                m_patchs[i, j].splitall = value;
                m_maxsplit = value;
            }
        }
        /// <summary>
        /// If false all triangle are visible, used to debug example when create a wireframe terrain
        /// </summary>
        public bool UseFrustumOcclusion
        {
            get
            {
                return m_usefrustum;
            }
            set
            {
                if (value != m_usefrustum && m_patchs != null)
                    for (int j = 0; j < n_zpatch; j++)
                        for (int i = 0; i < n_xpatch; i++)
                            m_patchs[i, j].showall = !value;
                m_usefrustum = value;
            }
        }

        #region Device job
        
        public void UpdateBuffers(GraphicDevice device)
        {
            if (m_verts.Count == 0 || m_faces.Count == 0) return;

            if (m_decl == null)
                m_decl = new VertexDeclaration(device, typeof(NCVERTEX), NCVERTEX.elements);

            if (m_vertexbuff==null || m_vertexbuff.Capacity < m_verts.Count)
                m_vertexbuff = new VertexBuffer(device, m_decl, m_verts.Count, true, true);

            if (m_facebuff == null || m_facebuff.Capacity < m_faces.Count)
                m_facebuff = new IndexBuffer(device, typeof(Face), m_faces.Count, true, true);

            m_vertexbuff.Open(0, m_verts.Count);
            m_vertexbuff.Write(m_verts.ToArray());
            m_vertexbuff.Close();
            m_vertexbuff.Count = m_verts.Count;

            // indexbuffer is a buffer of Face
            Type itype = m_facebuff.Type;
            m_facebuff.Open(0, m_faces.Count);
            m_facebuff.Write(m_faces.ToArray());
            m_facebuff.Close();
            m_facebuff.Count = m_faces.Count;
        }
        
        public void Draw(GraphicDevice device)
        {
            if (TotTriangles == 0) return;
            m_decl.SetToDevice();
            m_vertexbuff.SetToDevice();
            m_facebuff.SetToDevice();
            device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, m_vertexbuff.Count, 0, m_facebuff.Count);

            //device.renderstates.fillMode = FillMode.WireFrame;
            //device.renderstates.lightEnable = true;
            //device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, m_vertexbuff.Count, 0, m_facebuff.Count);
        }
        #endregion
    }
}
