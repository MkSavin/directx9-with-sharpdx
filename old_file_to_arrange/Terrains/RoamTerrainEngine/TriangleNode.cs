﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Maths;

namespace Engine.Roam
{
    /// <summary>
    /// the first bit are for visibility, last 6 bit for six frustum planes
    /// </summary>
    [Flags]
    public enum eFov : byte
    {
        None = 0,
        Visible = 1,
        Left = 4,
        Right = 8,
        Top = 16,
        Bottom = 32,
        Near = 64,
        Far = 128,
        /// <summary>
        /// set all to true
        /// </summary>
        Default = Visible | Left | Right | Top | Bottom | Near | Far
    }
    /// <summary>
    /// Triangle node, the base of triangle tree structure
    /// </summary>
    [DebuggerDisplay("TriNode{name}")]
    public class TriNode
    {
#if DEBUG
        public string name = "Tri";
        public Vector3 center = Vector3.Zero;
        public TriNode(string name) : this()
        {
            this.name = name;
            this.center = Vector3.Zero;
        }
#endif
        /// <summary>
        /// conectivity information
        /// </summary>
        public TriNode LChild, RChild;
        /// <summary>
        /// base neighbor
        /// </summary>
        public TriNode BNeighbor;
        /// <summary>
        /// Left-Rigth neighbor
        /// </summary>
        public TriNode LNeighbor, RNeighbor;
        /// <summary>
        /// Parent node
        /// </summary>
        public TriNode Parent;
        /// <summary>
        /// Fov flags and vivibility
        /// </summary>
        public eFov fov;
        /// <summary>
        /// center vertex index, -1 to invalidate, are used in the Render() function to memorize index in the child-parent recursion
        /// </summary>
        public int ic;
        /// <summary>
        /// is the null vertex index
        /// </summary>
        public const int NULLIDX = -1;


        public float defferal = -1;
        public float lastvar = 0;


        public TriNode()
        {
            ic = NULLIDX;
            fov = eFov.Default;
        }
        /// <summary>
        /// Return if this triangle is in a diamond structure
        /// </summary>
        public bool IsInDiamond
        {
            get { return (BNeighbor == null || BNeighbor.BNeighbor == this); }
        }
        /// <summary>
        /// Return is this triangle is splitted in Left and Rigth children
        /// </summary>
        public bool IsSplitted
        {
            get 
            { 
#if DEBUG
                if ((LChild != null && RChild == null) || (LChild == null && RChild != null))
                    throw new Exception("Uncorrect splitting");
#endif
                return LChild != null; 
            }
        }

        /// <summary>
        /// Get or Set the Fov visibility flag
        /// </summary>
        public bool IsVisible
        {
            get { return (fov & eFov.Visible) != 0; }
            set { if (value) fov |= eFov.Visible; else fov &= ~eFov.Visible; }
        }

        /// <summary>
        /// Clear all indices of tree structure
        /// </summary>
        public void ClearIndices()
        {
            ic = NULLIDX;
            if (IsSplitted)
            {
                LChild.ClearIndices();
                RChild.ClearIndices();
            }
        }
        
        /// <summary>
        /// Clear all fov information of tree structure
        /// </summary>
        public void ClearFov()
        {
            fov = eFov.Default;
            if (IsSplitted)
            {
                LChild.ClearFov();
                RChild.ClearFov();
            }
        }
        
        public int node = 0;

        public override string ToString()
        {
            return name;
        }
    }
}
