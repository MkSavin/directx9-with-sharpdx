﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Runtime.InteropServices;
using System.Windows.Forms;

using Engine.Graphics;
using Engine.Maths;
using Engine.Tools;

using Font = Engine.Graphics.Font;

namespace Engine.Roam
{
    public partial class TerrainForm : Form
    {
        /// <summary>
        /// Punto di ingresso principale dell'applicazione.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //Application.EnableVisualStyles();
            //Application.SetCompatibleTextRenderingDefault(false);
            TerrainForm form = new TerrainForm();
            Application.Idle += new EventHandler(form.Application_Idle);
            Application.Run(form);
        }

        GraphicDevice device;
        SwapChain swapchain;
        CameraValues topCam;
        CameraValues playerCam;

        Player player;
        AxisObj axis;
        FrustumObj frust;
        Vector3 eye , look;
        Terrain terrain;

        #region Wireframe terrain
        VertexDeclaration cvertex_decl;
        VertexBuffer wireframeV;
        IndexBuffer wireframeF;
        #endregion

        Font font;

        float yangle = 0;
        float l;
        int frames = 0;
        int LastTickCount = 0;
        int lastFramescount = 0;
        int StartTime = 0;
        Point start = new Point(0, 0);
        Point end = new Point(0, 0);

        void swap(ref int a, ref int b)
        {
            int t = a;
            a = b;
            b = t;
        }
        public TerrainForm()
        {
            //this.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint | ControlStyles.Opaque, true);
            InitializeComponent();

            device = new GraphicDevice(TopView);
            font = new Font(device,FontName.Arial,8); 
            axis = new AxisObj(device);
            frust = new FrustumObj(device);

            swapchain = new SwapChain(device, CameraView);

            device.lights.Add(Light.Sun);

            eye = new Vector3(-190, 200,-170);
            look = new Vector3(0,0,0);
            l = eye.Length;
            topCam = new CameraValues();
            playerCam = new CameraValues();

            terrain = new Terrain(Properties.Resources.terr2, new Vector3(-100, 0, -100), new Vector3(100, 20, 100), 5, 5);
            player = new Player(terrain);
            UpdateCamera();

            // INITIALIZE TERRAIN
            terrain.ForceMaximumSplitting = true;
            terrain.UseFrustumOcclusion = false;
            terrain.Reset();
            terrain.Tesselate();
            terrain.Render();
            terrain.UpdateBuffers(device);
            
            // build the wireframe terrain
            /*
            cvertex_decl = new VertexDeclaration(device, typeof(NCVERTEX), NCVERTEX.elements);
            wireframeV = new VertexBuffer(device, cvertex_decl, terrain.vertices.Length, true, true);
            wireframeF = new IndexBuffer(device, typeof(Face), terrain.triangles.Length, true, true);

            wireframeV.Open(0, terrain.vertices.Length);
            wireframeV.Write(terrain.vertices);
            wireframeV.Close();
            wireframeV.Count = terrain.vertices.Length;

            wireframeF.Open(0, terrain.triangles.Length);
            wireframeF.Write(terrain.triangles);
            wireframeF.Close();
            wireframeF.Count = terrain.triangles.Length;
            */

            //UpdateTreeView();

            StartTime = Environment.TickCount; 
            TopView.RenderFunction = new ViewPanel.OnRenderDelegate(TopView_Paint);
            CameraView.RenderFunction = new ViewPanel.OnRenderDelegate(CameraView_Paint);
        }

        private void TopView_Paint(ViewPanel panel)
        {
            frames++;
            //frameMove = (Environment.TickCount - LastTickCount) > 1000;
            if (Math.Abs(Environment.TickCount - LastTickCount) > 1000)
            {
                lastFramescount = frames;
                LastTickCount = Environment.TickCount;
                frames = 0;
            }
            

            UpdateCamera();

            if (device.BeginDraw())
            {
                device.SetRenderTarghet();
                device.Clear(Color.CornflowerBlue);
                device.viewport = topCam.viewport;
                device.renderstates.world = topCam.world;
                device.renderstates.projection = topCam.projection;
                device.renderstates.view = topCam.view;
                device.renderstates.cull = Cull.CounterClockwise;

                

                axis.Draw();

                device.renderstates.lightEnable = false;
                device.renderstates.fillMode = FillMode.Solid;
                terrain.Draw(device);
                device.renderstates.lightEnable = true;
                device.renderstates.fillMode = FillMode.WireFrame;
                terrain.Draw(device);


                if (wireframeV!=null && wireframeV.Count > 0)
                {
                    device.renderstates.lightEnable = true;
                    device.renderstates.fillMode = FillMode.WireFrame;
                    cvertex_decl.SetToDevice();
                    wireframeV.SetToDevice();
                    wireframeF.SetToDevice();
                    device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, wireframeV.Count, 0, wireframeF.Count);
                }
                frust.Draw();

                /*
                if (terrain.triangles != null)
                    for (int i = 0; i < terrain.triangles.Length; i++)
                    {
                        Vector3 screen = Vector3.Project(terrain.triangles[i].center, topCam.viewport, topCam.projection, topCam.view, topCam.world);
                        font.Draw(terrain.triangles[i].name, (int)screen.x, (int)screen.y, Color.Black);
                    }
                */
                font.Draw("fps: " + lastFramescount.ToString(), 10, 10, Color.Black);
                font.Draw("tri: " + terrain.TotTriangles.ToString(), 10, 20, Color.Black);

                device.EndDraw();
                device.PresentDraw();
            }

        }      
        private void CameraView_Paint(ViewPanel panel)
        {
            UpdateCamera();
            
            if (device.BeginDraw())
            {
                swapchain.SetRenderTarget();
                swapchain.Clear(Color.Blue);
                device.viewport = playerCam.viewport;
                device.renderstates.world = playerCam.world;
                device.renderstates.projection = playerCam.projection;
                device.renderstates.view = playerCam.view;

                device.renderstates.cull = Cull.CounterClockwise;
                device.renderstates.lightEnable = false;
                device.renderstates.fillMode = FillMode.Solid;
                terrain.Draw(device);
                device.renderstates.lightEnable = true;
                device.renderstates.fillMode = FillMode.WireFrame;
                terrain.Draw(device);

                device.EndDraw();
                swapchain.Present();
            }
        }
        
        private void TopView_MouseDown(object sender, MouseEventArgs e)
        {
            start.X = e.X;
            start.Y = e.Y;
        }
        private void TopView_MouseUp(object sender, MouseEventArgs e)
        {
            end.X = e.X;
            end.Y = e.Y;
            //DoFrustumArea(start, end);
        }
        private void TopView_MouseClick(object sender, MouseEventArgs e)
        {
            terrain.ForceMaximumSplitting = false;
            terrain.UseFrustumOcclusion = true;
            //terrain.Reset();
            terrain.frustum = frust.frustum;
            terrain.Tesselate();
            terrain.Render();
            terrain.UpdateBuffers(device);
        }
        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            if (device == null) return;
            UpdateCamera();
            swapchain.Dispose();
            device.Resize(TopView.ClientSize);
            swapchain.Resize(CameraView.ClientSize);
        }


        void UpdateCamera()
        {
            yangle += 0.01f;

            topCam.nearZ = 20f;
            topCam.farZ = 500f;
            topCam.viewport = new Viewport(TopView.ClientSize.Width,TopView.ClientSize.Height, 0, 0);
            topCam.projection = Matrix4.MakeProjectionLH(MathUtils.DegreeToRadian(45), topCam.viewport, 100f, 500f);
            topCam.view = Matrix4.MakeViewLH(eye, look, Vector3.UnitY);
            topCam.inview = Matrix4.Inverse(topCam.view);
            topCam.world = Matrix4.Identity;

            if (player != null)
            {
                playerCam.nearZ = 20f;
                playerCam.farZ = 500f;
                playerCam.viewport = new Viewport(CameraView.ClientSize.Width, CameraView.ClientSize.Height, 0, 0);
                playerCam.projection = Matrix4.MakeProjectionLH(MathUtils.DegreeToRadian(45), playerCam.viewport, 10f, 100f);
                playerCam.view = Matrix4.MakeViewLH(player.walkposition, player.walkposition + player.walklookdirection, Vector3.UnitY);
                playerCam.inview = Matrix4.Inverse(playerCam.view);
                playerCam.world = Matrix4.Identity;
            }
            else
            {
                playerCam = topCam;
            }
            frust.frustum = new Frustum(playerCam.projection, playerCam.view, playerCam.world);
        }
        void DoFrustumArea(Point ps, Point pe)
        {
            int xmin = ps.X;
            int ymin = ps.Y;
            int xmax = pe.X;
            int ymax = pe.Y;

            if (xmin > xmax) swap(ref xmin, ref xmax);
            if (ymin > ymax) swap(ref ymin, ref ymax);

            Viewport area = new Viewport(xmax - xmin, ymax - ymin, xmin, ymin);
            Frustum main = new Frustum(topCam.projection, topCam.view, topCam.world);
            Frustum child = main.GetDerivedArea(topCam.viewport, area, topCam.projection, topCam.view, topCam.world);

            terrain.Init();
            terrain.frustum = child;
            terrain.Tesselate();
            terrain.Render();
            terrain.UpdateBuffers(device);

            Frustum.ePlane planes = Frustum.ePlane.ALL;

            Vector3 bot = Vector3.Zero;
            Vector3 top = new Vector3(0, 100, 0);
            Vector3 center = (top + bot) * 0.5f;
            Vector3 dir = top - bot;
            float midheith = dir.Normalize() * 0.5f;


            bool test = terrain.frustum.GetCylindrerSide(center, dir, midheith, 200, ref planes);
            
            Console.WriteLine("cilinder visible " + test + " intersect plane " + planes.ToString());

            frust.frustum = terrain.frustum;
        }
        void StampList(IEnumerable collection)
        {
            foreach(object obj in collection)
            {
                 Console.WriteLine(obj.ToString());
            }
        }
        void UpdateTreeView()
        {
            treeView.Nodes.Clear();

            TreeNode root = new TreeNode("Terrain");

            for (int j = 0; j < terrain.n_zpatch; j++)
                for (int i = 0; i < terrain.n_xpatch; i++)
                {
                    Patch p = terrain.m_patchs[i, j];
                    TreeNode path = p.GetTreeView("Patch" + i + "," + j);

                    if (i == 0 && j == 0) path.ExpandAll();
                    else path.Collapse();
                    root.Nodes.Add(path);
                }

            root.Expand();
            treeView.Nodes.Add(root);
            treeView.Invalidate();
        }
        void MoveFrame()
        {

            float ms = Environment.TickCount - StartTime;
            if (ms > 100)
            {
                terrain.ForceMaximumSplitting = false;
                terrain.UseFrustumOcclusion = true;
                player.Move(ms / 1000f);
                terrain.frustum = frust.frustum;
                terrain.Tesselate();
                terrain.Render();
                terrain.UpdateBuffers(device);
                StartTime = Environment.TickCount;
            }
        }

        #region GAME LOOP
        bool draw = true;
        protected void Application_Idle(object sender, EventArgs e)
        {
            while (AppStillIdle && draw)
            {
                System.Threading.Thread.Sleep(10); //reduce CPU usage 
                UpdateCamera();
                MoveFrame();
                TopView.Invalidate();
                CameraView.Invalidate();
            }
        }
        public bool AppStillIdle
        {
            get
            {
                PeekMsg msg;
                return !PeekMessage(out msg, IntPtr.Zero, 0, 0, 0);
            }
        }
        [System.Security.SuppressUnmanagedCodeSecurity]
        [DllImport("User32.dll", CharSet = CharSet.Auto)]
        private static extern bool PeekMessage(out PeekMsg msg, IntPtr hWnd,
                 uint messageFilterMin, uint messageFilterMax, uint flags);

        [StructLayout(LayoutKind.Sequential)]
        public struct PeekMsg
        {
            public IntPtr hWnd;
            public Message msg;
            public IntPtr wParam;
            public IntPtr lParam;
            public uint time;
            public System.Drawing.Point p;
        }
        #endregion
    }
}
