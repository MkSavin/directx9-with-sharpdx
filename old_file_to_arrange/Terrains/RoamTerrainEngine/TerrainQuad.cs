﻿using System;
using System.Collections.Generic;
using System.Text;

using Engine.Graphics;
using Engine.Maths;

namespace Engine.Roam
{
    /// <summary>
    /// A Path group, used to optimize the vertexbuffer size, to hide quick in frustum culling a entire buffer without
    /// recalculate
    /// </summary>
    public class TerrainQuad
    {
        BoundaryAABB m_size;
        public List<Patch> m_patchs;

        VertexBuffer m_Vb;
        IndexBuffer m_Ib;

        List<NCVERTEX> m_vertices;
        List<Face> m_triangles;

        public TerrainQuad()
        {
            m_patchs = new List<Patch>();
        }

        public void Initialize(GraphicDevice device)
        {
            m_size = BoundaryAABB.NaN;

            m_vertices = new List<NCVERTEX>();
            m_triangles = new List<Face>();

            foreach (Patch patch in m_patchs)
            {
                m_size += patch.m_box;
                patch.pindices = m_triangles;
                patch.pvertices = m_vertices;
            }
        }

        public void Draw(Frustum frustum)
        {
            if (!frustum.isBoxVisible(m_size)) return;
        }

    }
}
