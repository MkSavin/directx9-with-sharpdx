﻿using System;
using System.Collections.Generic;
using System.Drawing;


using Engine.Graphics;
using Engine.Maths;
using Engine.Tools;

namespace Engine.Roam
{
    public class AxisObj
    {
        VertexDeclaration cvertex_decl;
        VertexBuffer axis;
        GraphicDevice m_device;

        public AxisObj(GraphicDevice device)
        {
            m_device = device;

            CVERTEX[] verts = new CVERTEX[]
            {
                new CVERTEX(0,0,0, Color.Red),
                new CVERTEX(200,0,0, Color.Red),
                new CVERTEX(0,0,0, Color.Green),
                new CVERTEX(0,200,0, Color.Green),
                new CVERTEX(0,0,0, Color.Blue),
                new CVERTEX(0,0,200, Color.Blue)
            };
            cvertex_decl = new VertexDeclaration(device, typeof(CVERTEX), CVERTEX.elements);

            axis = new VertexBuffer(device, cvertex_decl, verts.Length, false, true);
            axis.Open();
            axis.Write(verts);
            axis.Close();
            axis.Count = verts.Length;
        }

        public void Draw()
        {
            cvertex_decl.SetToDevice();
            m_device.renderstates.lightEnable = false;
            if (axis != null)
            {
                axis.SetToDevice();
                m_device.DrawPrimitives(PrimitiveType.LineList, 0, axis.Count / 2);
            }
        }
    }

    public class FrustumObj
    {
        GraphicDevice m_device;
        VertexDeclaration cvertex_decl;
        VertexBuffer frustumV;
        IndexBuffer frustumE;
        Frustum m_frustum;
        

        public Frustum frustum
        {
            get { return m_frustum; }
            set { m_frustum = value; RenderFrustum(); }
        }

        public FrustumObj(GraphicDevice device)
        {
            m_device = device;
            cvertex_decl = new VertexDeclaration(device, typeof(CVERTEX), CVERTEX.elements);
            frustumV = new VertexBuffer(device, cvertex_decl, 10, false, true);
            frustumE = new IndexBuffer(device, typeof(Edge), 13, false, true);
        }


        void RenderFrustum()
        {
            Edge[] edges;
            CVERTEX[] verts;
            m_frustum.Render(out verts, out edges);

            frustumV.Open();
            frustumV.Write(verts);
            frustumV.Count = verts.Length;

            frustumE.Open();
            frustumE.Write(edges);
            frustumE.Count = edges.Length;
        }

        public void Draw()
        {
            cvertex_decl.SetToDevice();
            m_device.renderstates.lightEnable = false;
            if (frustumV.Count > 0)
            {
                cvertex_decl.SetToDevice();
                frustumV.SetToDevice();
                frustumE.SetToDevice();
                m_device.DrawIndexedPrimitives(PrimitiveType.LineList, 0, 0, frustumV.Count, 0, frustumE.Count);
            }
        }
    }


    public class Player
    {
        public float velocity = 0.5f; //units/seconds
        public Vector3 walkdirection = new Vector3(1, 0, 0);
        public Vector3 walkposition = new Vector3(0, 0, 0);
        public Vector3 walklookdirection = Vector3.GetNormal(new Vector3(1, -0.1f, 0));

        public float yangle = 0;
        
        Terrain m_terrain;

        public Player(Terrain walkterrain)
        {
            m_terrain = walkterrain;
            walkposition.y = m_terrain.GetHeight(walkposition.x, walkposition.z) + 30;
            Move(0);
            yangle = 0;
        }
        public void Move(float dt)
        {
            if (yangle >= Math.PI * 2)
            {
                walklookdirection = Vector3.GetNormal((walklookdirection - Vector3.UnitY*velocity*0.1f));
                walkposition += Vector3.UnitY * velocity;
            }
            else
            {
                walklookdirection.x = (float)Math.Sin(yangle);
                walklookdirection.z = (float)Math.Cos(yangle);
                yangle += dt * velocity;
            }
        }
    }

}
