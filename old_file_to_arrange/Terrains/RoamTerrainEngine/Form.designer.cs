﻿namespace Engine.Roam
{
    partial class TerrainForm
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Liberare le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.treeView = new System.Windows.Forms.TreeView();
            this.TopView = new Engine.Roam.ViewPanel();
            this.CameraView = new Engine.Roam.ViewPanel();
            this.SuspendLayout();
            // 
            // treeView
            // 
            this.treeView.Dock = System.Windows.Forms.DockStyle.Right;
            this.treeView.Location = new System.Drawing.Point(867, 0);
            this.treeView.Name = "treeView";
            this.treeView.Size = new System.Drawing.Size(194, 470);
            this.treeView.TabIndex = 1;
            // 
            // TopView
            // 
            this.TopView.BackColor = System.Drawing.SystemColors.ControlDark;
            this.TopView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TopView.Dock = System.Windows.Forms.DockStyle.Left;
            this.TopView.Location = new System.Drawing.Point(0, 0);
            this.TopView.Name = "TopView";
            this.TopView.Size = new System.Drawing.Size(430, 470);
            this.TopView.TabIndex = 0;
            this.TopView.MouseClick += new System.Windows.Forms.MouseEventHandler(this.TopView_MouseClick);
            this.TopView.MouseDown += new System.Windows.Forms.MouseEventHandler(this.TopView_MouseDown);
            this.TopView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.TopView_MouseUp);
            // 
            // CameraView
            // 
            this.CameraView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CameraView.Location = new System.Drawing.Point(430, 0);
            this.CameraView.Name = "CameraView";
            this.CameraView.Size = new System.Drawing.Size(437, 470);
            this.CameraView.TabIndex = 2;
            // 
            // TerrainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1061, 470);
            this.Controls.Add(this.CameraView);
            this.Controls.Add(this.treeView);
            this.Controls.Add(this.TopView);
            this.Name = "TerrainForm";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private ViewPanel TopView;
        private System.Windows.Forms.TreeView treeView;
        private ViewPanel CameraView;
    }
}

