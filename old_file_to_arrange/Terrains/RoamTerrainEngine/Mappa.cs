﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;

using Engine.Maths;

namespace Engine.Roam
{
    /// <summary>
    /// Each custom height's map need these implementatio
    /// </summary>
    public interface IMap
    {
        /// <summary>
        /// num of x coordinates
        /// </summary>
        int Width{get;}
        /// <summary>
        /// num of z coordinates
        /// </summary>
        int Lenght { get; }
        /// <summary>
        /// the height (in y coordinate) in percentage, 1.0 = max , 0.0 = min
        /// </summary>
        /// <param name="i">in x coordinate</param>
        /// <param name="j">in y coordinate</param>
        float GetH(int i, int j);
    }

    /// <summary>
    /// Height map get from a bitmap image
    /// </summary>
    public class BmpHeightMap : IMap
    {
        int m_width;
        int m_lenght;
        byte[] m_map; // encode height as 0 - 255 range

        public int Width { get { return m_width; } }
        public int Lenght { get { return m_lenght; } }

        /// <summary>
        /// </summary>
        public BmpHeightMap(Bitmap bmp)
        {
            //    lenght(H in bitmap)
            //    |
            //    j
            //    |
            //    |
            //    |_____ i ____ width
            //
            m_width = bmp.Width;
            m_lenght = bmp.Height;
            m_map = new byte[m_width * m_lenght];
            
            //  invert j coordinate of bitmap class to match with min x - min z point in the world
            for (int j = m_lenght - 1; j >= 0; j--)
            {
                for (int i = 0; i < m_width; i++)
                {
                    // a pixel color is in byte so there are only 255 possible heights
                    byte val = (byte)(bmp.GetPixel(i, j).GetBrightness() * byte.MaxValue);
                    int idx = i + (m_lenght - j - 1) * m_width;
                    m_map[idx] = val;
                }
            }
        }
        /// <summary>
        /// Return the stored height value
        /// </summary>
        /// <param name="i">x component</param>
        /// <param name="j">z component</param>
        public float GetH(int i, int j)
        {
            if (i < 0) i = 0;
            else if (i > m_width) i = m_width;

            if (j < 0) j = 0;
            else if (j > m_lenght) j = m_lenght;

            return (float)m_map[i + j * m_width] / byte.MaxValue;
        }
    }
}
