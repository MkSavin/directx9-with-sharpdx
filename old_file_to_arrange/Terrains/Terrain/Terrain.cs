﻿using System;
using System.Collections.Generic;
using System.Drawing;

using Engine.Graphics;
using Engine.Maths;
using Engine.Tools;
using Engine.Renderers;
using Engine;
using System.IO;

namespace TerrainRenderingTest
{
    public class TerrainPatchHLSL : Effect
    {
        int current = 0;

        public readonly EffectParamMatrix WorldViewProj;
        public EffectParamVector3 PatchOrigin;
        public EffectParamVector3 PatchScale;
        public EffectParamTexture2D HeightMap;
       
        public EffectParamTexture2D RockMap;
        public EffectParamTexture2D GrassMap;
        public EffectParamTexture2D SandMap;
        public EffectParamTexture2D BlendMap;
        public EffectParamTexture2D SlopeMap;

        public EffectParamVector3 LightDirection;
        public EffectParamVector3 CameraEye;
        public EffectParamVector3 Selector;


        public EffectTechnique TechTextured;
        public EffectTechnique TechHeight;
        public EffectTechnique TechLight;
        public EffectTechnique TechWireframe;
        public EffectTechnique TechDepthPaint;
        public EffectTechnique TechSlopePaint;

        public List<EffectTechnique> AllTechniques;

        public TerrainPatchHLSL(Device device)
            : base(device, TerrainRenderingTest.Properties.Resources.Path)
        {
            WorldViewProj = new EffectParamMatrix(this, "WorldViewProj");
            PatchOrigin = new EffectParamVector3(this, "PatchOrigin");
            PatchScale = new EffectParamVector3(this, "PatchScale");
            
            HeightMap = new EffectParamTexture2D(this, "HeightMap");
            RockMap = new EffectParamTexture2D(this, "RockMap");
            GrassMap = new EffectParamTexture2D(this, "GrassMap");
            SandMap = new EffectParamTexture2D(this, "SandMap");
            BlendMap = new EffectParamTexture2D(this, "BlendMap");
            SlopeMap = new EffectParamTexture2D(this, "SlopeMap");

            LightDirection = new EffectParamVector3(this, "LightDir");
            CameraEye = new EffectParamVector3(this, "Eye");
            Selector = new EffectParamVector3(this, "Selector");

            TechTextured = new EffectTechnique(this, "Textured");
            TechHeight = new EffectTechnique(this, "Height");
            TechLight = new EffectTechnique(this, "Light");
            TechWireframe = new EffectTechnique(this, "Wireframe");
            TechDepthPaint = new EffectTechnique(this, "DepthPaint");
            TechSlopePaint = new EffectTechnique(this, "SlopePaint");

            AllTechniques = new List<EffectTechnique>();
            AllTechniques.Add(TechSlopePaint);
            AllTechniques.Add(TechTextured);
            AllTechniques.Add(TechHeight);
            AllTechniques.Add(TechLight);
            AllTechniques.Add(TechWireframe);
            AllTechniques.Add(TechDepthPaint);
            
        }

    }

    public class TerrainPatch
    {    
        const int col = 63;
        const int row = 63;
        int technique = 0;

        Device device;
        VertexBuffer vertexBuffer;
        IndexBuffer facesBuffer;
        VertexDeclaration vertex2declaration;

        public BoxAA mapsize;
        public TerrainPatchHLSL effect;


        public TerrainPatch(Device device, string contentfolder)
        {
            this.device = device;

            mapsize = new BoxAA(new Vector3(-50, 0, -50),new Vector3(50, 20, 50));

            effect = new TerrainPatchHLSL(device);

            effect.HeightMap.Value = Texture2D.FromFilename(device, TextureUsage.Managed, contentfolder + "Heights.tga");
            effect.RockMap.Value = Texture2D.FromFilename(device, TextureUsage.Managed, contentfolder + "Rock.jpg", true);
            effect.GrassMap.Value = Texture2D.FromFilename(device, TextureUsage.Managed, contentfolder + "Grass.jpg", true);
            effect.SandMap.Value = Texture2D.FromFilename(device, TextureUsage.Managed, contentfolder + "Sand.jpg", true);
            effect.BlendMap.Value = Texture2D.FromFilename(device, TextureUsage.Managed, contentfolder + "Blend.jpg");
            effect.SlopeMap.Value = Texture2D.FromFilename(device, TextureUsage.Managed, contentfolder + "SandGrass.jpg", true);

            BuildGrid();
        }

        public void MoveNextTecnique()
        {
            technique = (technique + 1) % effect.AllTechniques.Count;
        }

        public string CurrentTecnique
        {
            get { return effect.AllTechniques[technique].Name; }
        }

        public Vector2 TexFromWorldPos(Vector3 worldpos)
        {
            Vector2 tex = new Vector2();
            tex.u = (worldpos.x - mapsize.min.x) / (mapsize.max.x - mapsize.min.x);
            tex.v = (worldpos.z - mapsize.min.z) / (mapsize.max.z - mapsize.min.z);

            tex.v = tex.v;

            if (tex.u > 1) tex.u = 1;
            if (tex.v > 1) tex.v = 1;
            if (tex.u < 0) tex.u = 0;
            if (tex.v < 0) tex.v = 0;
       
            return tex;
        }



        public void Draw(ICamera camera)
        {
            device.SetVertexDeclaration(vertex2declaration);
            device.SetVertexStream(vertexBuffer, 0);
            device.SetIndexStream(facesBuffer);
            device.renderstates.cullMode = Cull.CounterClockwise;

            effect.WorldViewProj.Value = camera.Projection * camera.View;
            effect.CameraEye.Value = camera.Eye;
            effect.PatchOrigin.Value = mapsize.min;
            effect.PatchScale.Value = mapsize.max - mapsize.min;
            effect.LightDirection.Value = new Vector3(-1, -1, -1);

            foreach (Pass pass in effect.AllTechniques[technique])
            {
                device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, vertexBuffer.Count, 0, facesBuffer.Count);
            }
        }

        public void BuildGrid()
        {
            Vector2[] vertices;
            Face16[] indices;

            getIndexedGrid(col - 1, row - 1, out vertices, out indices);

            VertexLayout vertexdescr = new VertexLayout();
            vertexdescr.Add(0, DeclarationType.Float2, DeclarationUsage.Position);

            vertex2declaration = new VertexDeclaration(device, vertexdescr);

            //facesBuffer = new IndexBuffer(IndexInfo.IndexFace16, indices.Length, true);
            facesBuffer = new IndexBuffer(device, IndexLayout.Face16, BufferUsage.Managed, indices.Length);
            IndexStream idata = facesBuffer.OpenStream();
            idata.WriteCollection<Face16>(indices, IndexLayout.Face16, 0, indices.Length, 0, 0);
            facesBuffer.CloseStream();
            facesBuffer.Count = indices.Length;

            //vertexBuffer = new VertexBuffer(typeof(Vector2), vertex2declaration.Format, vertices.Length, true);
            vertexBuffer = new VertexBuffer(device, vertexdescr, BufferUsage.Managed, vertices.Length);
            VertexStream vdata = vertexBuffer.OpenStream();
            vdata.WriteCollection<Vector2>(vertices, 0, vertices.Length, 0);
            vertexBuffer.CloseStream();
            vertexBuffer.Count = vertices.Length;
        }

        int vertex(int i, int j) { return i + j * col; }

        /// <summary>
        /// <para>xsudd = ysudd = 2</para>
        /// <para>6---7---8</para>
        /// <para>|   |   |</para>
        /// <para>3---4---5</para>
        /// <para>|   |   |</para>
        /// <para>0---1---2</para>
        /// </summary>
        void getIndexedGrid(int xsudd, int ysudd, out Vector2[] vertices, out Face16[] indices)
        {
            int numverts = (xsudd + 1) * (ysudd + 1);
            int numtris = xsudd * ysudd * 2;
            if (numverts > ushort.MaxValue - 1) throw new OverflowException("too many vertices for 16bit indices");

            vertices = new Vector2[numverts];

            for (int n = 0, j = 0; j <= ysudd; j++)
                for (int i = 0; i <= xsudd; i++)
                    vertices[n++] = new Vector2((float)i / xsudd, (float)j / ysudd);

            indices = new Face16[numtris];
            for (int n = 0, j = 0; j < ysudd; j++)
                for (int i = 0; i < xsudd; i++)
                {
                    indices[n++] = new Face16(vertex(i, j), vertex(i, j + 1), vertex(i + 1, j + 1));
                    indices[n++] = new Face16(vertex(i, j), vertex(i + 1, j + 1), vertex(i + 1, j));
                }
        }
    }
}
