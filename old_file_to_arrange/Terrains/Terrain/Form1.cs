﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

using Engine.Graphics;
using Engine.Maths;
using Engine.Tools;
using Engine.Forms;

using Font = Engine.Graphics.Font;
using Engine;
using System.IO;
using Engine.Content;

namespace TerrainRenderingTest
{
    public partial class Form1 : Form
    {
        /// <summary>
        /// Punto di ingresso principale dell'applicazione.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //Application.EnableVisualStyles();
            //Application.SetCompatibleTextRenderingDefault(false);
            Form1 form = new Form1();
            Application.Run(form);
        }

        Viewport viewport;
        RenderWindow render;
        TrackBallCamera_new camera;

        Axis axis;
        Font font;
        EffectSimple effect;
        TerrainPatch terrain;
        SkyBox sky;
        
        
        bool usepen = false;
        Color32 pencolor;
        Vector3 penposition;
        Vector3[] corners;
        GameLoop gameloop;


        void swap(ref int a, ref int b)
        {
            int t = a;
            a = b;
            b = t;
        }
        public Form1()
        {
            EngineResources.ContentFolder = @"C:\Users\john\PROJECTS\3dEngine\3dengineSharpDx\Engine\Resources\Content\";

            this.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint | ControlStyles.Opaque, true);
            InitializeComponent();
            panel1.Focus();
            InitGraphics();

            gameloop = new GameLoop(Render, 60);
            gameloop.StartLooping();

            viewport = new Viewport(panel1.ClientSize);
        }

        void InitGraphics()
        {
            string contentfolder = Path.GetFullPath(@"..\..\..\Content\");


            if (!DesignMode)
            {
                render = new RenderWindow(new FrameSetting(true, false, new Viewport(ClientSize), this.panel1.Handle, true));

                font = new Font(render.Device, Engine.Graphics.Font.FontName.Arial, 8);
                axis = new Axis(render.Device, 10, Vector3.Zero);
                camera = new TrackBallCamera_new(this.panel1, new Vector3(-50, 20, -50), Vector3.Zero, Vector3.UnitY, 0.001f, 1000.0f);
                effect = new EffectSimple(render.Device);
                terrain = new TerrainPatch(render.Device, contentfolder);
                sky = new SkyBox(render.Device, EngineResources.SkyBoxTextureCube);
                corners = new Vector3[]
                {
                    terrain.mapsize.Corner0,
                    terrain.mapsize.Corner1,
                    terrain.mapsize.Corner2,
                    terrain.mapsize.Corner3,
                    terrain.mapsize.Corner4,
                    terrain.mapsize.Corner5,
                    terrain.mapsize.Corner6,
                    terrain.mapsize.Corner7
                };

                this.KeyPreview = true;
            }
        }


        void Render(double alpha)
        {
            this.Invalidate();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            e.Graphics.Clear(this.BackColor);

            if (render != null && render.Device.CanDraw())
            {
                render.FrameViewport = viewport;
                render.ClearDraw(BackColor);
                render.BeginDraw();

                axis.Draw(camera, effect);
                terrain.Draw(camera);

                Matrix4 PVW = camera.Projection * camera.View;

                for (int i = 0; i < 8; i++)
                {
                    Vector4 screen = PVW * (Vector4)corners[i];
                    Vector4 screenlog = PVW * (Vector4)corners[i];
                    LogarithmicDepth(ref screenlog, 0.001f, 1000.0f);


                    float inv_w = screen.w * screen.w > float.Epsilon ? 1.0f / screen.w : 1.0f;
                    screen *= inv_w;
                    ToViewport(ref screen, camera.Viewport);
                    //font.Draw(screen.z.ToString(), (int)screen.x, (int)screen.y, Color32.Blue);

                    inv_w = screenlog.w * screenlog.w > float.Epsilon ? 1.0f / screenlog.w : 1.0f;
                    screenlog *= inv_w;
                    ToViewport(ref screenlog, camera.Viewport);
                    //font.Draw(screenlog.z.ToString(), (int)screenlog.x, (int)screenlog.y + 10, Color32.Red);

                    Vector3 screenlog2 = Vector3.ProjectLogZ(corners[i], camera, 0.001f);
                    font.Draw(screenlog2.z.ToString(), (int)screenlog2.x, (int)screenlog2.y, Color32.White);


                }

                sky.DrawLast(camera.View, camera.Projection);

                render.EndDraw();
                render.Present();
            }
        }

        void ToViewport(ref Vector4 Transformed, Viewport viewport)
        {
            Transformed.x = (Transformed.x + 1) * 0.5f * viewport.Width + viewport.X;
            Transformed.y = (1 - Transformed.y) * 0.5f * viewport.Height + viewport.Y;

            if (Transformed.x < 0) Transformed.x = 0;
            if (Transformed.y < 0) Transformed.y = 0;
            if (Transformed.x > viewport.Width) Transformed.x = viewport.Width;
            if (Transformed.y > viewport.Height) Transformed.y = viewport.Height;
        }
        void LogarithmicDepth(ref Vector4 Transformed, float Resolution, float FarPlane)
        {
            Transformed.z = (float)(Math.Log10(Resolution * Transformed.z + 1) / Math.Log10(Resolution * FarPlane + 1) * Transformed.w);
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            return;
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            if (render == null) return;
            render.ResizeBackBuffer(panel1.ClientSize);
            viewport = new Viewport(panel1.ClientSize);
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);
            if (e.KeyCode == Keys.Space)
            {
                terrain.MoveNextTecnique();
                this.statusLabel.Text = "Tecnique : " + terrain.CurrentTecnique;
            };
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            base.OnMouseMove(e);

            if (!usepen)
            {
                //terrain.shader.Selector = new Vector3(0, 0, -1);
                return;
            }

            float t;
            Ray ray = new Ray(e.X, e.Y, camera.Viewport, camera.Projection, camera.View);

            bool intersect = PrimitiveIntersections.IntersectLinePlane(ray.Origin, ray.Direction, eAxis.XZ, out t);

            if (intersect)
            {
                Vector3 Int = penposition = ray[t];
                //terrain.shader.Selector = new Vector3(Int.x, Int.z, 10);
            }
            else
            {
                penposition = Vector3.Zero;
                //terrain.shader.Selector = new Vector3(0, 0, -1);
            }

            /*
            Vector3 p0 = Vector3.Unproject(e.X, e.Y, 0, camera.Viewport, camera.Projection, camera.View, Matrix4.Identity);
            Vector3 p1 = Vector3.Unproject(e.X, e.Y, 1, camera.Viewport, camera.Projection, camera.View, Matrix4.Identity);
            Vector3 dir = Vector3.GetNormal(p1 - p0);

            intersect = PrimitiveIntersections.IntersectLinePlane(p0, dir, eAxis.XZ, out t);


            terrain.shader.Selector = new Vector3(p0.x, p0.z, 10);
            */
            
        }

        void UpdateColor()
        {
            pencolor = Color32.White;
            if (this.radioButton1.Checked) pencolor = Color32.Red;
            if (this.radioButton2.Checked) pencolor = Color32.Green;
        }

        private void penMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            ToolStripMenuItem item = (ToolStripMenuItem)sender;
            
            camera.Enabled = !item.Checked;
            usepen = item.Checked;
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton btn = (RadioButton)sender;
            if (btn.Checked)
            {
                this.radioButton2.Checked = false;
            }
            UpdateColor();
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton btn = (RadioButton)sender;
            if (btn.Checked)
            {
                this.radioButton1.Checked = false;
            }
            UpdateColor();
        }

    }
}
