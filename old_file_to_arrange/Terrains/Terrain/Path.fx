//-----------------------------------------------------------------------------
// Globals.
//-----------------------------------------------------------------------------
#define WHITE float4(1,1,1,1)
#define BLACK float4(0,0,0,1)
#define RED   float4(1,0,0,1)
#define GREEN float4(0,1,0,1)
#define BLUE  float4(0,0,1,1)

#define SlopePlane 0.4f
#define SlopeHard  0.6f


float4x4 WorldViewProj : WORLDVIEWPROJ;
float3   PatchScale;
float3   PatchOrigin;
float3   LightDir;
float3   Eye;
float3   Selector; // x,y= position , z=radius

float  Resolution  = 0.001f;  // resolution constant
float  FarPlane    = 1000.0f; // far clip plane


Texture HeightMap;
Texture GrassMap;
Texture RockMap;
Texture SandMap;
Texture BlendMap;
Texture SlopeMap;


sampler heightSampler = sampler_state 
{ 
	texture = <HeightMap>;
	magfilter = LINEAR;
	minfilter = LINEAR;
	mipfilter = LINEAR;
	AddressU  = CLAMP; 
	AddressV  = CLAMP;
};
sampler slopeSampler = sampler_state 
{ 
	texture = <SlopeMap>;
	magfilter = LINEAR;
	minfilter = LINEAR;
	mipfilter = LINEAR;
	AddressU  = WRAP; 
	AddressV  = WRAP;
};
sampler grassSampler = sampler_state 
{ 
	texture = <GrassMap>;
	magfilter = LINEAR;
	minfilter = LINEAR;
	mipfilter = LINEAR;
	AddressU  = WRAP; 
	AddressV  = WRAP;
};
sampler rockSampler = sampler_state 
{ 
	texture = <RockMap>;
	magfilter = LINEAR;
	minfilter = LINEAR;
	mipfilter = LINEAR;
	AddressU  = WRAP; 
	AddressV  = WRAP;
};
sampler sandSampler = sampler_state 
{ 
	texture = <SandMap>;
	magfilter = LINEAR;
	minfilter = LINEAR;
	mipfilter = LINEAR;
	AddressU  = WRAP; 
	AddressV  = WRAP;
};
sampler blendSampler = sampler_state 
{ 
	texture = <BlendMap>;
	magfilter = LINEAR;
	minfilter = LINEAR;
	mipfilter = LINEAR;
	AddressU  = WRAP; 
	AddressV  = WRAP;
};



//-----------------------------------------------------------------------------
// Vertex Shaders.
//-----------------------------------------------------------------------------
struct VS_INPUT
{
    float2  Position  : POSITION;
};

struct VS_OUTPUT_DEPTH
{   
	float4 Position : POSITION;
	float4 WPosition: TEXCOORD0;  // un-transformed vertex position (real x,y,z)
	float2 DistanceHeight : TEXCOORD1;
};

struct VS_OUTPUT
{   
	float4 Position : POSITION;   // transformed vertex position (to x,y,depth,homogeneus)
	float4 WPosition: TEXCOORD0;  // un-transformed vertex position (real x,y,z)
	float2 DistanceHeight : TEXCOORD1;
	float2 TexCoord : TEXCOORD2;
	float3 Normal   : TEXCOORD3;
	float4 Color    : COLOR0;
};

// get height from map
float GetHeight(float2 texcoord)
{
	return tex2Dlod(heightSampler, float4(texcoord.xy, 0, 0)).a * PatchScale.y + PatchOrigin.y;
}
// get normal from map
float3 GetNormal(float2 texcoord)
{
	return tex2Dlod(heightSampler, float4(texcoord.xy, 0, 0)).rbg *2.0f - 1.0f;
}

// get normal from map using only height map
float3 GetSlope(float2 texcoord)
{
	return tex2Dlod(heightSampler, float4(texcoord.xy, 0, 0)).rbg *2.0f - 1.0f;
}

// convert unit vertex in global position
float4 GetGlobalPOS(float2 vertex)
{
	float4 pos = 0;
	pos.x = (vertex.x * PatchScale.x) + PatchOrigin.x;
	pos.z = (vertex.y * PatchScale.z) + PatchOrigin.z;
	pos.y = GetHeight(vertex);
	pos.w = 1;
	return pos;
}

// convert the liner z value in a logaritmic range
float LogarithmicDepth(float z , float w , float C , float Far)
{
	return log(C*z + 1) / log(C*Far + 1) * w;
}

VS_OUTPUT VShader(VS_INPUT input)
{
	VS_OUTPUT output = (VS_OUTPUT)0; 

	float2 texcoord = input.Position;

	output.TexCoord  = texcoord;
	output.WPosition = GetGlobalPOS(texcoord);
	output.Normal    = GetNormal(texcoord);
	output.DistanceHeight = float2( distance(output.WPosition.xyz , Eye) , (output.WPosition.y - PatchOrigin.y) / PatchScale.y);
	output.Position  = mul(output.WPosition, WorldViewProj);
	return output;
}

VS_OUTPUT VShader_slope(VS_INPUT input)
{
	VS_OUTPUT output = (VS_OUTPUT)0; 

	float2 texcoord = input.Position;

	output.TexCoord  = texcoord;
	output.WPosition = GetGlobalPOS(texcoord);
	output.Normal    = GetSlope(texcoord);
	output.DistanceHeight = float2( distance(output.WPosition.xyz , Eye) , (output.WPosition.y - PatchOrigin.y) / PatchScale.y);
	output.Position  = mul(output.WPosition, WorldViewProj);
	return output;
}



VS_OUTPUT_DEPTH VShader_zbuffer(VS_INPUT input)
{
	VS_OUTPUT_DEPTH output = (VS_OUTPUT_DEPTH)0; 

	float2 texcoord = input.Position;
	output.WPosition = GetGlobalPOS(texcoord);
	output.Position = mul(output.WPosition, WorldViewProj);

	output.Position.z = LogarithmicDepth(output.Position.z, output.Position.w, Resolution , FarPlane); 

	output.WPosition = output.Position;
	output.WPosition.z /= output.WPosition.w;
	output.DistanceHeight = float2( distance(output.WPosition.xyz , Eye) , (output.WPosition.y - PatchOrigin.y) / PatchScale.y);

	return output;
}


//-----------------------------------------------------------------------------
// Pixel Shaders.
//-----------------------------------------------------------------------------
float4 PShader_light(VS_OUTPUT input) : COLOR0
{
	float3 normal = tex2D(heightSampler, input.TexCoord.xy).rbg * 2.0f - 1.0f;

	return WHITE * saturate(max(0,dot(normal , -LightDir)));
}

float4 PShader_textured(VS_OUTPUT input) : COLOR0
{
	//if (frac (input.DistanceHeight.y / 0.25f) < 0.01f)

	float3 normal = tex2D(heightSampler, input.TexCoord).rbg * 2.0f - 1.0f;
	float4 tiled = float4(input.TexCoord * 50.0f , 0, input.DistanceHeight.x / 20);
	float4 indices = tex2D(blendSampler, input.TexCoord);
	float4 color0 = tex2Dlod(rockSampler, tiled) * indices.r;
	float4 color1 = tex2Dlod(grassSampler, tiled) * indices.g;
	float4 color2 = tex2Dlod(sandSampler, tiled) * indices.b;
	float lightintensity = saturate(max(0,dot(normal , -LightDir)));

	if (Selector.z > 0)
	{
		float2 delta = Selector.xy - input.WPosition.xz;
		float distsq = delta.x * delta.x + delta.y * delta.y;
		if(distsq < Selector.z * Selector.z)
		{
			color0 = lerp(color0,RED,0.2f); 
		}
	}

	return (color0 + color1 + color2) * lightintensity;
	
}

float4 PShader_height(VS_OUTPUT input) : COLOR0
{
	float a = tex2D(heightSampler, input.TexCoord).a;
	return float4(a,a,a,1);
}

float4 PShader_zbuffer(float4 inputPosition : TEXCOORD0) : COLOR0
{
	float a = 1 - inputPosition.z;

	return float4(a,a,a,1);
}

float4 PShader_white() : COLOR0
{
	return WHITE;
}

float4 PShader_black() : COLOR0
{
	return BLACK;
}

float4 PShader_slope(float4 inputPosition : TEXCOORD0 , float3 inputNormal : TEXCOORD3) : COLOR0
{
    float slope = 1.0f - inputNormal.y;
	float blendAmount = 1.0f;
	float4 textureColor = RED;

    if(slope < 0.2)
    {
        blendAmount = slope / 0.2f;
        textureColor = lerp(BLUE, GREEN, blendAmount);
    }
    else if((slope < 0.7) && (slope >= 0.2f))
    {
        blendAmount = (slope - 0.2f) * (1.0f / (0.7f - 0.2f));
        textureColor = lerp(GREEN, RED, blendAmount);
    }

	return textureColor;
}

float4 PShader_slopetextured(VS_OUTPUT input) : COLOR0
{
	float3 normal = tex2D(heightSampler, input.TexCoord.xy).rbg * 2.0f - 1.0f;

    float slope = 1.0f - normal.y;
	float blendAmount = 1.0f;

	float4 tiled = float4(input.TexCoord * 50.0f , 0, input.DistanceHeight.x / 50);

	float4 rockColor = tex2Dlod(rockSampler, tiled);
	float4 grasColor = tex2Dlod(grassSampler, tiled);
	float4 sandColor = tex2Dlod(slopeSampler, tiled);
	
	float pixelightintensity = saturate(max(0.1f,dot(input.Normal , -LightDir)));
	float vertlightintensity = saturate(max(0.1f,dot(normal , -LightDir)));

	float intensity = saturate(pixelightintensity + vertlightintensity);

	// orizontal
    if(slope <= SlopePlane)
    {
        blendAmount = slope / SlopePlane;
        return lerp(grasColor, sandColor, blendAmount) * intensity;
    }
	// small slope
    else if((slope < SlopeHard) && (slope > SlopePlane))
    {
        blendAmount = (slope - SlopePlane) * (1.0f / (SlopeHard - SlopePlane));
        return lerp(sandColor, rockColor, blendAmount) * intensity;
    }
	// hard slope
	return rockColor * intensity;
}


//-----------------------------------------------------------------------------
// Techniques.
//-----------------------------------------------------------------------------
technique Textured
{
    pass p0
    {
        VertexShader = compile vs_3_0 VShader();
		PixelShader =  compile ps_3_0 PShader_textured();
		CullMode = CCW;
		FillMode = Solid;
    }
}

technique Height
{
    pass p0
    {
        VertexShader = compile vs_3_0 VShader();
		PixelShader =  compile ps_3_0 PShader_height();
		CullMode = CCW;
		FillMode = Solid;
    }
}
technique Light
{
    pass p0
    {
        VertexShader = compile vs_3_0 VShader();
		PixelShader =  compile ps_3_0 PShader_light();
		CullMode = CCW;
		FillMode = Solid;
    }
}
technique DepthPaint
{
    pass p0
    {
        VertexShader = compile vs_3_0 VShader_zbuffer();
		PixelShader =  compile ps_3_0 PShader_zbuffer();
		CullMode = CCW;
		FillMode = Solid;
    }
}
technique SlopePaint
{
    pass p0
    {
        VertexShader = compile vs_3_0 VShader_slope();
		PixelShader =  compile ps_3_0 PShader_slopetextured();
		CullMode = CCW;
		FillMode = Solid;
    }
}
technique Wireframe
{
    pass p0
    {

        VertexShader = compile vs_3_0 VShader();
		PixelShader =  compile ps_3_0 PShader_white();
		CullMode = CCW;
		FillMode = Solid;
    }
	pass p1
    {

        VertexShader = compile vs_3_0 VShader();
		PixelShader =  compile ps_3_0 PShader_black();
		CullMode = CCW;
		FillMode = Wireframe;
    }
}