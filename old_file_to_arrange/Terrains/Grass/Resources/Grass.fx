//-----------------------------------------------------------------------------
// Globals.
//-----------------------------------------------------------------------------
#define WHITE float4(1,1,1,1)
#define BLACK float4(0,0,0,1)
#define RED   float4(1,0,0,1)
#define GREEN float4(0,1,0,1)
#define BLUE  float4(0,0,1,1)

float4x4 ViewProj : VIEWPROJ;
float4x4 World;
float4x4 View;
float4x4 Proj;

Texture  HeightMap;
Texture  DiffuseMap;

sampler heightSampler = sampler_state 
{ 
	texture = <HeightMap>;
	magfilter = LINEAR;
	minfilter = LINEAR;
	mipfilter = LINEAR;
	AddressU  = CLAMP; 
	AddressV  = CLAMP;
};
sampler diffuseSampler = sampler_state 
{ 
	texture = <DiffuseMap>;
	magfilter = LINEAR;
	minfilter = LINEAR;
	mipfilter = LINEAR;
	AddressU  = CLAMP; 
	AddressV  = CLAMP;
};

struct VS_INPUT_CLOSEUP
{
    float2  Vertex    : POSITION0; // the quad xy
	float2  TexCoord  : TEXCOORD0; // the quad uv
};

struct VS_INPUT_INSTANCE
{
    float2  Vertex    : POSITION0; // the quad xy
	float2  TexCoord  : TEXCOORD0; // the quad uv
	float4  Position  : POSITION1; // the quad position on terrain
};

struct VS_OUTPUT
{   
	float4 Position : POSITION;   // transformed vertex position (to x,y,depth,homogeneus)
	float2 TexCoord : TEXCOORD0;
	float4 Color    : COLOR0;
};


//-----------------------------------------------------------------------------
// Vertex Shaders.
//-----------------------------------------------------------------------------

VS_OUTPUT VShader(VS_INPUT_INSTANCE input)
{
	VS_OUTPUT output = (VS_OUTPUT)0; 

	output.Position = float4( input.Vertex.xy, 0 , 1);
	output.Position.xyz	+= input.Position;
	output.Position.w = 1.0f;
	output.Position = mul(output.Position, ViewProj);
	output.TexCoord = input.TexCoord;
	output.Color = WHITE;
	return output;
}

VS_OUTPUT VShader_sphericbillboard(VS_INPUT_INSTANCE input)
{
	VS_OUTPUT output = (VS_OUTPUT)0; 

	output.Position = input.Position;
	output.Position = mul(output.Position,View);
	output.Position.xy += input.Vertex.xy;
	//output.Position.w = 1.0f;
	
	output.Position = mul(output.Position, Proj);

	output.TexCoord = input.TexCoord;
	output.Color = WHITE;
	return output;
}

VS_OUTPUT VShader_Closeup(VS_INPUT_CLOSEUP input)
{
	VS_OUTPUT output = (VS_OUTPUT)0; 

	output.Position = float4(input.TexCoord.x * 2.0f - 1.0f, 1.0f - input.TexCoord.y * 2.0f, 0, 1);
	output.TexCoord = input.TexCoord;
	output.Color = WHITE;

	return output;
}

//-----------------------------------------------------------------------------
// Pixel Shaders.
//-----------------------------------------------------------------------------
float4 PShader(VS_OUTPUT input) : COLOR0
{
	return tex2D(diffuseSampler, input.TexCoord);
}

//-----------------------------------------------------------------------------
// Techniques.
//-----------------------------------------------------------------------------
technique GrassTechnique
{
    pass p0
    {
        VertexShader = compile vs_3_0 VShader();
		PixelShader =  compile ps_3_0 PShader();	
    }
}

technique BillboardTechnique
{
    pass p0
    {
        VertexShader = compile vs_3_0 VShader_sphericbillboard();
		PixelShader =  compile ps_3_0 PShader();
    }
}

technique CloseupTechnique
{
    pass p0
    {
        VertexShader = compile vs_3_0 VShader_Closeup();
		PixelShader =  compile ps_3_0 PShader();
    }
}