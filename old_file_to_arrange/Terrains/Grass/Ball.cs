﻿using System;
using System.Collections.Generic;
using System.Drawing;

using Engine.Graphics;
using Engine.Maths;
using Engine.Tools;
using Engine.Geometry;
using Engine;

namespace TestGrass
{
    public class Ball
    {
        Device device;
        VertexBuffer vb;
        IndexBuffer ib;
        VertexDeclaration vdecl;

        public Matrix4 transform = Matrix4.Identity;

        public Ball(Device device, Color32 color)
        {
            this.device = device;

            VertexLayout vlayout = new VertexLayout();
            vlayout.Add(0, DeclarationType.Float3, DeclarationUsage.Position , 0);
            vlayout.Add(0, DeclarationType.Color, DeclarationUsage.Color , 0);
            vdecl = new VertexDeclaration(device, vlayout);

            InitBox();
        }

        void InitBox()
        {  
            MeshListGeometry mesh = BaseTriGeometry.CubeOpen();

            vb = new VertexBuffer(device, vdecl.Format, BufferUsage.Managed, mesh.vertices.Count);
            VertexStream vstream = vb.OpenStream();
            vstream.WriteCollection<Vector3>(mesh.vertices.data, vdecl.Format.Elements[0], 0, mesh.vertices.Count, 0);
            vstream.WriteCollection<Color32>(mesh.colors.data, vdecl.Format.Elements[1], 0, mesh.vertices.Count, 0);
            vb.CloseStream();
            vb.Count = mesh.vertices.Count;

            ib = new IndexBuffer(device, IndexLayout.Face16, BufferUsage.Managed, mesh.indices.Count);
            IndexStream istream = ib.OpenStream();

            istream.WriteCollection<Face16>(mesh.indices.data, IndexLayout.Face16, 0, mesh.indices.Count, 0, 0);
            ib.CloseStream();
            ib.Count = mesh.indices.Count;
        }

        void InitQuad(Color32 color)
        {
            vb = new VertexBuffer(device, vdecl.Format, BufferUsage.Managed, Quad.NVERTS);
            VertexStream vstream = vb.OpenStream();

            Vector3[] vertices = new Vector3[]
            {
                new Vector3(0,0,0),
                new Vector3(1,0,0),
                new Vector3(1,1,0),
                new Vector3(0,1,0) 
            };
            Color32[] colors = new Color32[] { color, color, color, color };

            vstream.WriteCollection<Vector3>(vertices, vdecl.Format.Elements[0], 0, Quad.NVERTS, 0);
            vstream.WriteCollection<Color32>(colors, vdecl.Format.Elements[1], 0, Quad.NVERTS, 0);
            vb.CloseStream();
            vb.Count = Quad.NVERTS;

            ib = new IndexBuffer(device, IndexLayout.Face16, BufferUsage.Managed, Quad.NINDIS / 3);
            IndexStream istream = ib.OpenStream();
            istream.WriteCollection<byte>(Quad.indices, IndexLayout.One8, 0, Quad.NINDIS, 0, 0);
            ib.CloseStream();
            ib.Count = Quad.NINDIS / 3;

        }



        public void Draw(ICamera camera, EffectSimple effect)
        {
            effect.WorldViewProject = camera.Projection * camera.View * transform;
            effect.World = transform;
            effect.View = camera.View;
            effect.Project = camera.Projection;

            device.renderstates.cullMode = Cull.CounterClockwise;
            device.renderstates.fillMode = FillMode.Solid;

            device.SetVertexDeclaration(vdecl);
            device.SetVertexStream(vb, 0);
            device.SetIndexStream(ib);

            int count = effect.Begin();
            for (int i = 0; i < count; i++)
            {
                effect.BeginPass(i);
                device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, vb.Count, 0, ib.Count);
                effect.EndPass();
            }
            effect.End();

        }

    }
}
