﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Drawing;

using Engine.Graphics;
using Engine.Maths;
using Engine.Tools;
using Engine.Renderers;
using Engine;

namespace TestGrass
{
    /// <summary>
    /// The default quad geometry to plane XY (Z=0)
    /// </summary>
    /// <remarks>
    ///   v3---v2     y          ___u
    ///   | \  |      |__X       |
    ///   |  \ |                 v
    ///   v0---v1
    /// </remarks>
    public static class Quad
    {
        public const int NVERTS = 4;
        public const int NINDIS = 12;

        public static Vector2[] vertices = null;
        public static Vector2[] texcoords = null;
        public static byte[] indices = null; // notice that can be use also byte

        static Quad()
        {
            vertices = new Vector2[]
            {
                new Vector2(0,0),
                new Vector2(1,0),
                new Vector2(1,1),
                new Vector2(0,1) 
            };

            texcoords = new Vector2[]
            {
                new Vector2(0,1),
                new Vector2(1,1),
                new Vector2(1,0),
                new Vector2(0,0) 
            };

            indices = new byte[] 
            { 
                0, 3, 1, 3, 2, 1 ,
                0, 1, 3, 3, 1, 2
            };

        }

    }

    /// <summary>
    /// The GPU programm
    /// </summary>
    public class GrassEffect : Effect
    {
        public EffectTechnique instancetech, closeuptech, billboard;
        public EffectParamTexture2D DiffuseMap;
        public EffectParamMatrix ViewProj, View, Proj;

        public GrassEffect(Device device)
            : base(device, Properties.Resources.Grass)
        {

            DiffuseMap = new EffectParamTexture2D(this, "DiffuseMap");
            ViewProj = new EffectParamMatrix(this, "ViewProj");
            View = new EffectParamMatrix(this, "View");
            Proj = new EffectParamMatrix(this, "Proj");

            instancetech = new EffectTechnique(this, "GrassTechnique");
            closeuptech = new EffectTechnique(this, "CloseupTechnique");
            billboard = new EffectTechnique(this, "BillboardTechnique");
            Technique = billboard;


            string grass01dds = Path.GetFullPath(@"..\..\..\grass01.dds");
            if (File.Exists(grass01dds))
            {
                DiffuseMap.Value = Texture2D.FromFilename(device, TextureUsage.Managed, grass01dds, true);
            }
        }
    }

    /// <summary>
    /// The CPU class
    /// </summary>
    public class Grass
    {
        public enum Method : int
        {
            HardwareInstancing = 0,
            Ripetitive = 1,
            CloseUp = 2,
            HardwareBillboard = 3
        }

        Random rnd = new Random();
        Method current = Method.HardwareBillboard;
        Device device;
        HWInstancedGeometry InstaceGrass;
        List<Vector3> Positions;
        VertexDeclaration geometryDecl;

        public GrassEffect effect;


        public Grass(Device device)
        {
            this.device = device;

            effect = new GrassEffect(device);

            InstaceGrass = new HWInstancedGeometry(device);

            VertexLayout geometryLayout = new VertexLayout();
            geometryLayout.Add(0, DeclarationType.Float2, DeclarationUsage.Position, 0);
            geometryLayout.Add(0, DeclarationType.Float2, DeclarationUsage.TexCoord);

            VertexBuffer geometryVB = new VertexBuffer(device, geometryLayout, BufferUsage.Managed, Quad.NVERTS);
            VertexStream vstream = geometryVB.OpenStream();

            vstream.WriteCollection<Vector2>(Quad.vertices, geometryLayout.Elements[0], 0, Quad.NVERTS, 0);
            vstream.WriteCollection<Vector2>(Quad.texcoords, geometryLayout.Elements[1], 0, Quad.NVERTS, 0);
            geometryVB.CloseStream();
            geometryVB.Count = Quad.NVERTS;

            IndexBuffer geometryIB = new IndexBuffer(device, IndexLayout.One16, BufferUsage.Managed, Quad.NINDIS);
            IndexStream istream = geometryIB.OpenStream();
            istream.WriteCollection<byte>(Quad.indices, IndexLayout.One8, 0, Quad.NINDIS, 0, 0);
            geometryIB.CloseStream();
            geometryIB.Count = Quad.NINDIS;

            Positions = new List<Vector3>();
            for (int j = 50; j > -50; j -= 2)
                for (int i = -50; i < 50; i += 2)
                    Positions.Add(new Vector3(i, 0, j));

            VertexLayout instanceLayout = new VertexLayout();
            instanceLayout.Add(1, DeclarationType.Float3, DeclarationUsage.Position, 1);

            VertexBuffer instanceVB = new VertexBuffer(device, instanceLayout, BufferUsage.Managed, Positions.Count);
            vstream = instanceVB.OpenStream();
            vstream.WriteCollection<Vector3>(Positions, 0, Positions.Count, 0);
            instanceVB.CloseStream();
            instanceVB.Count = Positions.Count;

            VertexDeclaration declaration = new VertexDeclaration(device, geometryLayout, instanceLayout);
            geometryDecl = new VertexDeclaration(device, geometryLayout);

            InstaceGrass.geomtrybuffer = geometryVB;
            InstaceGrass.indexbuffer = geometryIB;
            InstaceGrass.instancebuffer = instanceVB;
            InstaceGrass.vertexdeclaration = declaration;
            InstaceGrass.NumGeometryPrimitives = Quad.NINDIS / 3;
            InstaceGrass.NumGeometryVertices = Quad.NVERTS;
            InstaceGrass.NumInstances = Positions.Count;
            InstaceGrass.PrimitiveType = PrimitiveType.TriangleList;
            InstaceGrass.shadercode = effect;


        }


        public void Draw(ICamera camera)
        {
            device.renderstates.fillMode = FillMode.Solid;
            device.renderstates.cullMode = Cull.CounterClockwise;
            device.renderstates.AlphaTestEnable = true;
            device.renderstates.AlphaFunction = Compare.GreaterEqual;
            device.renderstates.AlphaRef = 125;

            effect.ViewProj.Value = camera.Projection * camera.View;
            int passcount = 0;

            switch (current)
            {
                case Method.Ripetitive:
                    device.SetVertexStream(InstaceGrass.geomtrybuffer);
                    device.SetIndexStream(InstaceGrass.indexbuffer);
                    device.SetVertexDeclaration(geometryDecl);


                    foreach (Pass pass in effect.instancetech)
                    {
                        for (int n = 0; n < Positions.Count; n++)
                        {
                            Vector3 pos = Positions[n];
                            pos.x += (float)(2 * rnd.NextDouble() - 1) * 0.1f;
                            effect.ViewProj.Value = camera.Projection * camera.View * Matrix4.Translating(pos);
                            effect.Update();

                            device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, Quad.NVERTS, 0, Quad.NINDIS / 3);
                        }
                    }
                    break;

                case Method.HardwareBillboard:
                    effect.View.Value = camera.View;
                    effect.Proj.Value = camera.Projection;
                    InstaceGrass.Draw();
                    break;

                case Method.HardwareInstancing:
                    InstaceGrass.Draw();
                    break;

                case Method.CloseUp:

                    device.SetVertexStream(InstaceGrass.geomtrybuffer);
                    device.SetIndexStream(InstaceGrass.indexbuffer);
                    device.SetVertexDeclaration(geometryDecl);

                    passcount = effect.Begin();
                    for (int i = 0; i < passcount; i++)
                    {
                        effect.BeginPass(i);
                        device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, Quad.NVERTS, 0, Quad.NINDIS / 3);
                        effect.EndPass();
                    }
                    effect.End();
                    break;
            }

        }


        public void SetNextTechnique()
        {
            int curr = ((int)current + 1) % 4;
            current = (Method)curr;

            switch (current)
            {
                case Method.HardwareInstancing:
                    effect.Technique = effect.instancetech; break;
                case Method.Ripetitive:
                    effect.Technique = effect.instancetech; break;
                case Method.CloseUp:
                    effect.Technique = effect.closeuptech; break;
                case Method.HardwareBillboard:
                    effect.Technique = effect.billboard; break;
            }

            Console.WriteLine("Set " + current);
        }


    }
}
