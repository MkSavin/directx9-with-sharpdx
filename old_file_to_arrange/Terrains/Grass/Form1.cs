﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Engine.Graphics;
using Engine.Tools;
using Engine.Maths;
using Engine;
using Engine.Content;

namespace TestGrass
{
    public partial class Form1 : Form
    {
        RenderWindow renderer;
        TrackBallCamera_new camera;

        Grass grass;
        Axis axis;
        Ball ball0, ball1;
        EffectSimple effect;

        float angleY = 0.0f;

        public Form1()
        {
            EngineResources.ContentFolder = @"C:\Users\john\PROJECTS\3dEngine\3dengineSharpDx\Engine\Resources\Content\";


            InitializeComponent();

            renderer = new RenderWindow(new FrameSetting(true, false, new Viewport(ClientSize), this.Handle, true));
           
            camera = new TrackBallCamera_new(this, new Vector3(-10,5,-10), Vector3.Zero, Vector3.UnitY, 0, 1000.0f);

            grass = new Grass(renderer.Device);

            axis = new Axis(renderer.Device, 10.0f, Vector3.Zero);

            ball0 = new Ball(renderer.Device, Color32.Red);
            ball1 = new Ball(renderer.Device, Color32.Green);

            effect = new EffectSimple(renderer.Device);


        }


        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            System.Threading.Thread.Sleep(5);


            if (renderer == null || !renderer.CanDraw()) return;

            renderer.BeginDraw();

            axis.Draw(camera, effect);

            angleY += 0.02f;

            ball1.transform = Matrix4.RotationY(angleY) * Matrix4.Translating(0,1, 1);

            grass.Draw(camera);

            ball1.Draw(camera, effect);
            ball0.Draw(camera, effect);


            renderer.EndDraw();
            renderer.Present();

            this.Invalidate();
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Space)
            {
                grass.SetNextTechnique();
            }
        }
        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
            this.Invalidate();
        }
        protected override void OnMouseWheel(MouseEventArgs e)
        {
            base.OnMouseWheel(e);
            this.Invalidate();
        }
        protected override void OnPaintBackground(PaintEventArgs e)
        {
            return;
        }
        protected override void OnResizeBegin(EventArgs e)
        {
            base.OnResizeBegin(e);
        }
        protected override void OnResizeEnd(EventArgs e)
        {
            base.OnResizeEnd(e);
            if (renderer != null) renderer.ResizeBackBuffer(ClientSize);
        }
    }
}
