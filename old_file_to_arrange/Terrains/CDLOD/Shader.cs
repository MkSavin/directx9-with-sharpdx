﻿using System;
using System.Collections.Generic;
using System.Text;

using Engine.Graphics;
using Engine.Maths;
using Engine.Renderers;

namespace Engine.Cdlod
{
    /// <summary>
    /// Is the shader code used to render the cdlod algorithm
    /// </summary>
    public class CdlodShader : MaterialFX
    {
        public enum TechniqueMode
        {
            /// <summary>
            /// Using index texture for a custom layers mask
            /// </summary>
            Default = 0,
            /// <summary>
            /// Using some function to find layers mask
            /// </summary>
            Default_Procedural = 1,
            /// <summary>
            /// Test slope function
            /// </summary>
            Slope=2,
            /// <summary>
            /// Test quadtree algorithm
            /// </summary>
            Wireframe=3,
            /// <summary>
            /// Test normal mapping
            /// </summary>
            NormalX=4,
            NormalY=5,
            NormalZ=6
        }
        TechniqueMode current = TechniqueMode.Default;

        public CdlodShader(RenderDevice render)
            : base(render, Engine.Cdlod.Properties.Resources.QuadMap)
        {
            /*
            float3   xPatchOrigin; // the min(x,y,z) position in global coord system of current patch
            float2   xPatchScale;  // the scale factor of patch's vertices in (dx,dy) format
            float4   xPatchColor;  // debug patch with a custum color if diffuse texture aren't available
            float4   xTerrainSize  // global size of terrain in (minx,minz,maxx,maxz) format
            */
            effect.Parameters.Add("Technique", new EffectParamTechnique(current.ToString()));
            effect.Parameters.Add("Transform", new EffectParamMatrix(Matrix4.Identity, "WorldViewProj"));
            effect.Parameters.Add("PatchOrigin", new EffectParamVector3("xPatchOrigin"));
            effect.Parameters.Add("PatchScale", new EffectParamVector3("xPatchScale"));
            effect.Parameters.Add("PatchDimension", new EffectParamVector2("xPatchDim"));
            effect.Parameters.Add("PatchColor", new EffectParamColor(Color32.White, "xPatchColor"));
            
            effect.Parameters.Add("HeightMap", new EffectParamTexture(null, "xHeightMap"));

            effect.Parameters.Add("IndexMap", new EffectParamTexture(null, "xIndicesMap"));
            effect.Parameters.Add("GrassMap", new EffectParamTexture(null, "xGrassMap"));
            effect.Parameters.Add("ForestMap", new EffectParamTexture(null, "xForestMap"));
            effect.Parameters.Add("RockMap", new EffectParamTexture(null, "xRockMap"));

            effect.Parameters.Add("TerrainMin", new EffectParamVector3("xTerrainMin"));
            effect.Parameters.Add("TerrainMax", new EffectParamVector3("xTerrainMax"));
            effect.Parameters.Add("LightDirection", new EffectParamVector3("xLightDir"));
            effect.Parameters.Add("CameraEye", new EffectParamVector3("xEye"));

            effect.Parameters.Add("Morph", new EffectParamVector2("xMorph"));
            
        }

        public int RenderModeIndex
        {
            get { return (int)RenderMode; }
            set { RenderMode = (TechniqueMode)(value % 7); }
        }

        public TechniqueMode RenderMode
        {
            get { return current; }
            set { effect.Parameters["Technique"] = value.ToString(); current = value; }
        }

        public Matrix4 ProjViewWorld
        {
            get { return (Matrix4)effect.Parameters["Transform"]; }
            set { effect.Parameters["Transform"] = (Matrix4)value; }
        }
        public Texture2D HeightMap
        {
            get { return (Texture2D)effect.Parameters["HeightMap"]; }
            set { effect.Parameters["HeightMap"] = (Texture2D)value; }
        }
        public Texture2D IndexMap
        {
            get { return (Texture2D)effect.Parameters["IndexMap"]; }
            set { effect.Parameters["IndexMap"] = (Texture2D)value; }
        }

        public Texture2D GrassMap
        {
            get { return (Texture2D)effect.Parameters["GrassMap"]; }
            set { effect.Parameters["GrassMap"] = (Texture2D)value; }
        }
        public Texture2D ForestMap
        {
            get { return (Texture2D)effect.Parameters["ForestMap"]; }
            set { effect.Parameters["ForestMap"] = (Texture2D)value; }
        }
        public Texture2D RockMap
        {
            get { return (Texture2D)effect.Parameters["RockMap"]; }
            set { effect.Parameters["RockMap"] = (Texture2D)value; }
        }

        public Vector3 TerrainMin
        {
            get { return (Vector3)effect.Parameters["TerrainMin"]; }
            set { effect.Parameters["TerrainMin"] = (Vector3)value; }
        }
        public Vector3 TerrainMax
        {
            get { return (Vector3)effect.Parameters["TerrainMax"]; }
            set { effect.Parameters["TerrainMax"] = (Vector3)value; }
        }



        public Vector2 Morph
        {
            get { return (Vector2)effect.Parameters["Morph"]; }
            set { effect.Parameters["Morph"] = (Vector2)value; }
        }

        /// <summary>
        /// In [minX,minY,minZ] format
        /// </summary>
        public Vector3 PatchOrigin
        {
            get { return (Vector3)effect.Parameters["PatchOrigin"]; }
            set { effect.Parameters["PatchOrigin"] = (Vector3)value; }
        }
        public Vector2 PatchDimension
        {
            get { return (Vector2)effect.Parameters["PatchDimension"]; }
            set { effect.Parameters["PatchDimension"] = (Vector2)value; }
        }
        public Vector3 PatchScale
        {
            get { return (Vector3)effect.Parameters["PatchScale"]; }
            set { effect.Parameters["PatchScale"] = (Vector3)value; }
        }
        public Vector3 LightDirection
        {
            get { return (Vector3)effect.Parameters["LightDirection"]; }
            set { effect.Parameters["LightDirection"] = (Vector3)value; }
        }
        public Vector3 CameraEye
        {
            get { return (Vector3)effect.Parameters["CameraEye"]; }
            set { effect.Parameters["CameraEye"] = (Vector3)value; }
        }
        public Color32 PatchColor
        {
            get { return (Color32)effect.Parameters["PatchColor"]; }
            set { effect.Parameters["PatchColor"] = (Color32)value; }
        }
        
        public override void ApplyParams()
        {
            effect.Parameters.UpdateParams(true);
        }

        public override void BeginPass(int pass)
        {
            effect.BeginPass(pass);
        }
    }

    /// <summary>
    /// Is the shader code used to render the cdlod algorithm
    /// </summary>
    public class CdlodNormalShader : MaterialFX
    {
        public CdlodNormalShader(RenderDevice render)
            : base(render, Engine.Cdlod.Properties.Resources.QuadNorm)
        {
            effect.Parameters.Add("Technique", new EffectParamTechnique("DefaultTechique"));
            effect.Parameters.Add("Transform", new EffectParamMatrix(Matrix4.Identity, "WorldViewProj"));
            effect.Parameters.Add("PatchOrigin", new EffectParamVector3("xPatchOrigin"));
            effect.Parameters.Add("PatchScale", new EffectParamVector3("xPatchScale"));
            effect.Parameters.Add("PatchDimension", new EffectParamVector2("xPatchDim"));
            effect.Parameters.Add("PatchColor", new EffectParamColor(Color32.White, "xPatchColor"));
            effect.Parameters.Add("HeightMap", new EffectParamTexture(null, "xHeightMap"));
           
            effect.Parameters.Add("TerrainMin", new EffectParamVector3("xTerrainMin"));
            effect.Parameters.Add("TerrainMax", new EffectParamVector3("xTerrainMax"));
            effect.Parameters.Add("CameraEye", new EffectParamVector3("xEye"));
            effect.Parameters.Add("Morph", new EffectParamVector2("xMorph"));
        }

        public Matrix4 ProjViewWorld
        {
            get { return (Matrix4)effect.Parameters["Transform"]; }
            set { effect.Parameters["Transform"] = (Matrix4)value; }
        }
        public Texture2D HeightMap
        {
            get { return (Texture2D)effect.Parameters["HeightMap"]; }
            set { effect.Parameters["HeightMap"] = (Texture2D)value; }
        }
        public Vector3 TerrainMin
        {
            get { return (Vector3)effect.Parameters["TerrainMin"]; }
            set { effect.Parameters["TerrainMin"] = (Vector3)value; }
        }
        public Vector3 TerrainMax
        {
            get { return (Vector3)effect.Parameters["TerrainMax"]; }
            set { effect.Parameters["TerrainMax"] = (Vector3)value; }
        }
        public Vector2 Morph
        {
            get { return (Vector2)effect.Parameters["Morph"]; }
            set { effect.Parameters["Morph"] = (Vector2)value; }
        }

        /// <summary>
        /// In [minX,minY,minZ] format
        /// </summary>
        public Vector3 PatchOrigin
        {
            get { return (Vector3)effect.Parameters["PatchOrigin"]; }
            set { effect.Parameters["PatchOrigin"] = (Vector3)value; }
        }
        public Vector2 PatchDimension
        {
            get { return (Vector2)effect.Parameters["PatchDimension"]; }
            set { effect.Parameters["PatchDimension"] = (Vector2)value; }
        }
        public Vector3 PatchScale
        {
            get { return (Vector3)effect.Parameters["PatchScale"]; }
            set { effect.Parameters["PatchScale"] = (Vector3)value; }
        }
        public Vector3 CameraEye
        {
            get { return (Vector3)effect.Parameters["CameraEye"]; }
            set { effect.Parameters["CameraEye"] = (Vector3)value; }
        }
        public Color32 PatchColor
        {
            get { return (Color32)effect.Parameters["PatchColor"]; }
            set { effect.Parameters["PatchColor"] = (Color32)value; }
        }

        public override void ApplyParams()
        {
            effect.Parameters.UpdateParams(true);
        }

        public override void BeginPass(int pass)
        {
            effect.BeginPass(pass);
        }
    }


    public class WaterShader : MaterialFX
    {
        public WaterShader(RenderDevice render)
            : base(render, Engine.Cdlod.Properties.Resources.Water)
        {

            effect.Parameters.Add("Technique", new EffectParamTechnique("DefaultTechique"));
            effect.Parameters.Add("Transform", new EffectParamMatrix(Matrix4.Identity, "WorldViewProj"));
            effect.Parameters.Add("LightDirection", new EffectParamVector3("xLightDir"));
            effect.Parameters.Add("CameraEye", new EffectParamVector3("xEye"));
        }

        public bool WireframeMode
        {
            get { return (string)effect.Parameters["Technique"] == "WireframeTechique2" ; }
            set { effect.Parameters["Technique"] = value ? "WireframeTechique2" : "DefaultTechique"; }
        }
        public Matrix4 ProjViewWorld
        {
            get { return (Matrix4)effect.Parameters["Transform"]; }
            set { effect.Parameters["Transform"] = (Matrix4)value; }
        }
        public Vector3 LightDirection
        {
            get { return (Vector3)effect.Parameters["LightDirection"]; }
            set { effect.Parameters["LightDirection"] = (Vector3)value; }
        }
        public Vector3 CameraEye
        {
            get { return (Vector3)effect.Parameters["CameraEye"]; }
            set { effect.Parameters["CameraEye"] = (Vector3)value; }
        }

        public override void ApplyParams()
        {
            effect.Parameters.UpdateParams(true);
        }

        public override void BeginPass(int pass)
        {
            effect.BeginPass(pass);
        }
    }
}
