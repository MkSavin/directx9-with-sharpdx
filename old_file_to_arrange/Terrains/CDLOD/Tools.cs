﻿using System;
using System.Collections.Generic;
using System.Drawing;

using Engine.Graphics;
using Engine.Maths;
using Engine.Tools;
using Engine.Renderers;
using Engine.Geometry;

namespace Engine.Cdlod
{
    public class AxisObj
    {
        public MaterialFX_VCOLOR material = null;
        RenderDevice render;
        VertexBuffer axis;
        VertexDeclaration cvertex_decl;

        public Vector3[] axe = new Vector3[3];

        public AxisObj(RenderDevice render)
        {
            this.render = render;
            this.cvertex_decl = new VertexDeclaration(render, CVERTEX.m_elements);

            CVERTEX[] verts = new CVERTEX[]
            {
                new CVERTEX(0,0,0, Color.Red),
                new CVERTEX(50,0,0, Color.Red),
                new CVERTEX(0,0,0, Color.Green),
                new CVERTEX(0,20,0, Color.Green),
                new CVERTEX(0,0,0, Color.Blue),
                new CVERTEX(0,0,50, Color.Blue)
            };

            axe[0] = verts[1].position;
            axe[1] = verts[3].position;
            axe[2] = verts[5].position;

            axis = new VertexBuffer(render, cvertex_decl.Format, BufferUsage.Managed, verts.Length);

            VertexStream vdata = axis.OpenStream();
            vdata.WriteCollection<CVERTEX>(verts, 0, verts.Length, 0);
            axis.CloseStream();
            axis.Count = verts.Length;
        }

        public void Draw(ICamera camera)
        {
            render.Device.SetVertexDeclaration(cvertex_decl);
            render.Device.SetVertexStream(axis, 0);

            if (material != null)
            {
                material.ProjViewWorld = camera.Projection * camera.View * Matrix4.Identity;
                int count = material.Begin();
                for (int pass = 0; pass < count; pass++)
                {
                    material.BeginPass(pass);
                    render.Device.DrawPrimitives(PrimitiveType.LineList, 0, axis.Count / 2);
                    material.EndPass();
                }
                material.End();
            }
            render.Device.renderstates.projection = camera.Projection;
            render.Device.renderstates.view = camera.View;
            render.Device.renderstates.world = Matrix4.Identity;
            render.Device.DrawPrimitives(PrimitiveType.LineList, 0, axis.Count / 2);
        }
    }

    public class FrustumObj
    {
        RenderDevice render;
        VertexDeclaration cvertex_decl;
        VertexBuffer frustumV;
        IndexBuffer frustumE;
        Frustum m_frustum;


        public Frustum frustum
        {
            get { return m_frustum; }
            set { m_frustum = value; RenderFrustum(); }
        }

        public FrustumObj(RenderDevice render)
        {
            this.render = render;
            cvertex_decl = new VertexDeclaration(render, CVERTEX.m_elements);
            frustumV = new VertexBuffer(render, cvertex_decl.Format, BufferUsage.Managed, 10);
            frustumE = new IndexBuffer(render, IndexInfo.Edge16, BufferUsage.Managed, 13);
        }


        void RenderFrustum()
        {
            Edge16[] edges;
            CVERTEX[] verts;
            m_frustum.Render(out verts, out edges);

            VertexStream vdata = frustumV.OpenStream();
            vdata.WriteCollection<CVERTEX>(verts, 0, verts.Length, 0);
            frustumV.CloseStream();
            frustumV.Count = verts.Length;

            IndexStream idata = frustumE.OpenStream();
            idata.WriteCollection<Edge16>(edges, 0, edges.Length, 0);
            frustumE.CloseStream();
            frustumE.Count = edges.Length;
        }

        public void Draw()
        {
            render.Device.renderstates.lightEnable = false;
            if (frustumV.Count > 0)
            {
                render.Device.SetVertexStream(frustumV, 0);
                render.Device.SetIndexStream(frustumE);
                render.Device.DrawIndexedPrimitives(PrimitiveType.LineList, 0, 0, frustumV.Count, 0, frustumE.Count);
            }
        }
    }


    public class PlayerObj
    {
        RenderDevice render;
        public MaterialFX_VCOLOR material = null;
        VertexBuffer axis;
        VertexDeclaration cvertex_decl;
        Matrix4 transform;


        public PlayerObj(RenderDevice render)
        {
            this.render = render;
            this.cvertex_decl = new VertexDeclaration(render, CVERTEX.m_elements);

            CVERTEX[] verts = new CVERTEX[]
            {
                new CVERTEX(0,0,0, Color.Black),
                new CVERTEX(1,-2,1, Color.Green),
                new CVERTEX(0,0,0, Color.Black),
                new CVERTEX(1,-2,-1, Color.Green),
                new CVERTEX(0,0,0, Color.Black),
                new CVERTEX(-1,-2,-1, Color.Green),
                new CVERTEX(0,0,0, Color.Black),
                new CVERTEX(-1,-2,1, Color.Green),
            };
            axis = new VertexBuffer(render, cvertex_decl.Format, BufferUsage.Managed, verts.Length);

            VertexStream vdata = axis.OpenStream();
            vdata.WriteCollection<CVERTEX>(verts, 0, verts.Length, 0);
            axis.CloseStream();
            axis.Count = verts.Length;

            transform = Matrix4.Identity;
        }

        public Vector3 PlayerPos
        {
            get { return transform.Position; }
            set { transform.Position = value; }
        }

        public void Draw(ICamera camera)
        {
            render.Device.SetVertexDeclaration(cvertex_decl);
            render.Device.SetVertexStream(axis, 0);

            if (material != null)
            {
                material.ProjViewWorld = camera.Projection * camera.View * transform;
                int count = material.Begin();
                for (int pass = 0; pass < count; pass++)
                {
                    material.BeginPass(pass);
                    render.Device.DrawPrimitives(PrimitiveType.LineList, 0, axis.Count / 2);
                    material.EndPass();
                }
                material.End();
            }
            else
            {
                render.Device.renderstates.projection = camera.Projection;
                render.Device.renderstates.view = camera.View;
                render.Device.renderstates.world = transform;
                render.Device.DrawPrimitives(PrimitiveType.LineList, 0, axis.Count / 2);
            }
        }

    }


    /// <summary>
    /// Drawable box in units size
    /// </summary>
    public class BoxHelper
    {
        RenderDevice render;
        VertexBuffer vb;
        VertexDeclaration decl;
        LineListGeometry box = (LineListGeometry)BaseLineGeometry.BoxGizmo(1, 1, 1);
        Matrix4 Proj;
        Matrix4 View;

        public MaterialFX_POSITION material = null;
        public Engine.Graphics.Font font = null;

        public BoxHelper(RenderDevice render)
        {
            this.render = render;
            decl = new VertexDeclaration(render, VERTEX.m_elements);

            vb = new VertexBuffer(render, decl.Format, BufferUsage.Managed, box.numVertices);
            VertexStream data = vb.OpenStream();
            data.WriteCollection<Vector3>(box.vertices, 0, box.numVertices, 0);
            vb.CloseStream();
        }

        public void SetToDevice(ICamera camera)
        {
            if (material != null)
            {
                // precompute a part of WVP matrix
                Proj = camera.Projection;
                View = camera.View;
            }
            else
            {
                render.Device.renderstates.projection = camera.Projection;
                render.Device.renderstates.view = camera.View;
            }
            render.Device.SetVertexDeclaration(decl);
            render.Device.SetVertexStream(vb, 0);
        }
        public void Draw(Matrix4 world, Color32 vcolor)
        {
            if (material != null)
            {
                material.ProjViewWorld = Proj * View * world;
                material.Diffuse = vcolor;
            }
            else
            {
                render.Device.renderstates.world = world;
            }
            render.Device.DrawPrimitives(box.primitive, 0, box.numPrimitives);
        }
    }


    public class MaterialFX_trasparent : MaterialFX
    {
        public MaterialFX_trasparent(RenderDevice render)
            : base(render, Engine.Cdlod.Properties.Resources.QuadMap)
        {
            effect.Parameters.Add("Technique", new EffectParamTechnique("WireframeTechique2"));
            effect.Parameters.Add("Transform", new EffectParamMatrix(Matrix4.Identity, "WorldViewProj"));
        }

        public Matrix4 ProjViewWorld
        {
            get { return (Matrix4)effect.Parameters["Transform"]; }
            set { effect.Parameters["Transform"] = (Matrix4)value; }
        }

        public override void ApplyParams()
        {
            effect.Parameters.UpdateParams(true);
        }

        public override void BeginPass(int pass)
        {
            effect.BeginPass(pass);
        }
    }

    public class CameraHelper
    {
        public MaterialFX_POSITION shader;
        RenderDevice render;
        VertexBuffer vb;
        VertexDeclaration decl;
        IndexBuffer ib;

        float[] range;
        Color32[] colors;

        public CameraHelper( RenderDevice render, float[] lodranges)
        {
            this.render = render;

            //MeshListGeometry usphere = BaseTriGeometry.Icosahedron(1);
            MeshListGeometry usphere = BaseTriGeometry.GlobeSphere(10, 10, 1);
            this.range = lodranges;

            decl = new VertexDeclaration(render, VERTEX.m_elements);

            vb = new VertexBuffer(render, decl.Format, BufferUsage.Managed, usphere.numVertices);
            VertexStream data = vb.OpenStream();
            data.WriteCollection<Vector3>(usphere.vertices, 0, usphere.numVertices, 0);
            vb.CloseStream();
            vb.Count = usphere.numVertices;

            ib = new IndexBuffer(render, IndexInfo.Face16, BufferUsage.Managed, usphere.numPrimitives);
            IndexStream idata = ib.OpenStream();
            idata.WriteCollection<Face16>(usphere.indices, 0, 0);
            ib.CloseStream();
            ib.Count = usphere.numPrimitives;

            this.colors = new Color32[range.Length];
            for (int level = 0; level < range.Length; level++)
                colors[level] = Color32.Rainbow((float)level / range.Length);
        }

        public void Draw(ICamera camera, Vector3 position)
        {
            render.Device.SetVertexDeclaration(decl);
            render.Device.SetVertexStream(vb, 0);
            render.Device.SetIndexStream(ib);

            render.Device.renderstates.cullMode = Cull.Clockwise;

            int numpass = shader.Begin();

            for (int pass = 0; pass < numpass; pass++)
            {
                shader.BeginPass(pass);


                for (int level = 0 ; level<range.Length;level++)
                {
                    //if (level != range.Length-2) continue;

                    float r = range[level];

                    Matrix4 transform = new Matrix4(
                    r, 0, 0, position.x,
                    0, r, 0, position.y,
                    0, 0, r, position.z,
                    0, 0, 0, 1);
                    shader.ProjViewWorld = camera.Projection * camera.View * transform;
                    shader.Diffuse = colors[level];
                    render.Device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, vb.Count, 0, ib.Count);
                }
                shader.EndPass();
            }
            shader.End();
            render.Device.renderstates.cullMode = Cull.CounterClockwise;
        }
    }
}
