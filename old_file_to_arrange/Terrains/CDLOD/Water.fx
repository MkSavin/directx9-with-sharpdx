//-----------------------------------------------------------------------------
// Globals.
//-----------------------------------------------------------------------------
float4x4 WorldViewProj : WORLDVIEWPROJ;

float3   xLightDir;
float3   xEye;

Texture  xColorMap;

sampler colorSampler = sampler_state 
{ 
	texture = <xColorMap>;
	magfilter = LINEAR;
	minfilter = LINEAR;
	mipfilter = LINEAR;
	AddressU  = CLAMP; 
	AddressV  = CLAMP;
};

//-----------------------------------------------------------------------------
// Vertex Shaders.
//-----------------------------------------------------------------------------
struct VS_INPUT
{
    float4  Position  : POSITION;
};

struct VS_OUTPUT
{   
	float4 Position : POSITION;
	float3 Normal   : NORMAL;
    float4 Color    : COLOR0;
	float2 TexCoord : TEXCOORD0;
};

VS_OUTPUT VShader(VS_INPUT input)
{
	VS_OUTPUT output = (VS_OUTPUT)0; 

	output.Position = mul(input.Position, WorldViewProj);
	output.Normal = float3(0,1,0);
	return output;
}

//-----------------------------------------------------------------------------
// Pixel Shaders.
//-----------------------------------------------------------------------------
float4 PShader_diffuse(VS_OUTPUT input) : COLOR0
{
	float3 normal = input.Normal;
	float dt = max(0, dot(normal, -xLightDir));
	
	float4 color = float4(0,0,1,1) * dt;
	color.a = 0.5f;
	return color;
}

float4 PShader_black(VS_OUTPUT input) : COLOR0
{
	return float4(0,0,0,1);
}


//-----------------------------------------------------------------------------
// Techniques.
//-----------------------------------------------------------------------------
technique DefaultTechique
{
    pass p0
    {
		CullMode = CCW;
		FillMode = Solid;

		ZWriteEnable    = FALSE;
		AlphaBlendEnable = TRUE;

        VertexShader = compile vs_3_0 VShader();
		PixelShader =  compile ps_3_0 PShader_diffuse();
    }
}
technique WireframeTechique2
{
    pass p0
    {
		CullMode = CCW;
		FillMode = Solid;
		DepthBias = 0;
        VertexShader = compile vs_3_0 VShader();
		PixelShader =  compile ps_3_0 PShader_diffuse();
    }
	pass p1
    {
		CullMode = CCW;
		FillMode = Wireframe;
		DepthBias = -0.00001f;
        VertexShader = compile vs_3_0 VShader();
		PixelShader =  compile ps_3_0 PShader_black();
    }
}