﻿using System;
using System.Collections.Generic;
using System.Text;

using Engine.Maths;
using Engine.Graphics;
using Engine.Tools;

namespace Engine.Cdlod
{
    /// <summary>
    /// Most problematic values, i don't understand why my implementation have a lot of issue in the lod transitions, sometime
    /// the neighbour QuadNode are 2 level greater and this generate a missing morth transition. To resolve a little i add the
    /// semipatch.
    /// Morth end-start isen't interpolated instad i need to resolve the transition issue
    /// </summary>
    public struct LodRange
    {
        public float radius;
        public Vector2 morth;

        public LodRange(float lodradius, float endmorth, float starmorth)
        {
            radius = lodradius;
            morth = new Vector2(endmorth, starmorth);
        }
    }

    /// <summary>
    /// A class only for custom setting
    /// </summary>
    public class QuadTreeSetting
    {
        public const int MAXLEVELS = 10; //used in shader constant

        int NUMLEVELS;
        int NUMNODES;
        int patchrow;
        int patchcol;
        bool ForceLod0;
        bool showBox;
        bool showPatch;
        bool showNormal;
        float RootRadius;
        bool computeBound;
        bool usemorth;

        public int NumLevels
        {
            get { return NUMLEVELS; }
            set 
            { 
                NUMLEVELS = value > MAXLEVELS ? MAXLEVELS : value;
                NUMNODES = ((1 << (NUMLEVELS * 2)) - 1) / 3;
            }
        }
        /// <summary> maximum number of total node what level generate</summary>
        public int NumNodes
        {
            get { return NUMNODES; }
        }
        /// <summary> maximum lod0 count (leaf nodes) what level generate</summary>
        public int NumLeafNodes
        {
            get { return 1 << NUMLEVELS; }
        }

        /// <summary> force draw untill lod0 details</summary>
        public bool ForceDrawLeaf
        {
            get { return ForceLod0; }
            set { ForceLod0 = value; }
        }
        /// <summary> compute the min max y value in the quad node tree</summary>
        public bool OptimalPatchBoundary
        {
            get { return computeBound; }
            set { computeBound = value; }
        }
        /// <summary> enable or disable blend transition</summary>
        public bool MorthTransition
        {
            get { return usemorth; }
            set { usemorth = value; }
        }      
        /// <summary> show help box to check if bounding volume are correct</summary>
        public bool ShowHelpBox
        {
            get { return showBox; }
            set { showBox = value; }
        }
        /// <summary> show the quad patches</summary>
        public bool ShowQuadPatch
        {
            get { return showPatch; }
            set { showPatch = value; }
        }
        /// <summary> show the quad normals</summary>
        public bool ShowQuadNormals
        {
            get { return showNormal; }
            set { showNormal = value; }
        }
        /// <summary> show the quad patches</summary>
        public float RootLevelRadius
        {
            get { return RootRadius; }
            set { RootRadius = value; }
        }
        /// <summary> num of rows for a static patch</summary>
        public int PatchRow
        {
            get { return patchrow; }
            set
            { 
                patchrow = value;
                System.Diagnostics.Debug.Assert(usemorth && patchrow % 2 != 0, "even size with morth transition generate a crack in the transition zone");
            }
        }
        /// <summary> num of colums for a static patch</summary>
        public int PatchCol
        {
            get { return patchcol; }
            set 
            {
                patchcol = value;
                System.Diagnostics.Debug.Assert(usemorth && patchcol % 2 != 0, "even size with morth transition generate a crack in the transition zone");
            }
        }
        public QuadTreeSetting()
        {
            NUMLEVELS = 4;
            NUMNODES = ((1 << (NUMLEVELS * 2)) - 1) / 3;
            ForceLod0 = false;
            showBox = true;
            showPatch = true;
            RootRadius = 40.0f;
            patchrow = 33;
            patchcol = 33;
            OptimalPatchBoundary = true;
            MorthTransition = true;
        }
    }


    public class QuadTree
    {
        public QuadTreeSetting setting { get; private set; }

        /// <summary> debug </summary>
        public static int QuadNodeDrawCount = 0;
        public static int TotalTrianglesCount = 0;

        public Terrain terrain;
        public QuadNode root;

        /// <summary>
        /// X : Contain the precomputed radius(distance from camera eye) that activate the level's lod
        /// Y : Contain the radius where morth end (1)
        /// Z : Contain the radius where morth start (0) start is smaller than end
        /// </summary>
        public LodRange[] LodMorthRadius;
        /// <summary>
        /// Contain the precomputed min and max heights value for all possible QuadNode to fix node's bounding box height
        /// </summary>
        public Vector2[] Heights;

        public CdlodShader shader;
        public CdlodNormalShader normalshader;

        /// <summary>
        /// the semipatch is used when a patch is morphed to maximum, you can halve the vertices and eliminate degenerate triangles
        /// </summary>
        public QuadMesh patch, semipatch;
        public QuadNormal normalpatch, normalsemipatch;

        public BoxHelper boxSpline = null;
        public CameraHelper cameraSphere = null;
        Vector3 Eye;
        float angle = 0;

        float minf(float a, float b) { return a < b ? a : b; }
        float maxf(float a, float b) { return a > b ? a : b; }
        float maxf(float a, float b, float c) { return a > b ? maxf(a, c) : maxf(b, c); }
        float clamp(float f,float min,float max) { return f > max ? max : f < min ? min : f; }

        /// <summary>
        /// </summary>
        /// <param name="heightssample">the height map, can be null</param>
        /// <remarks>
        /// heightssample : is not necessary a finner map, just an approximation to precompute fast the height size for each quad node
        /// </remarks>
        public QuadTree(Terrain terrain , QuadTreeSetting setting)
        {
            this.setting = setting;
            this.terrain = terrain;

            if (terrain.elevationsMap != null)
            {
                int pixelsW = terrain.elevationsMap.SizeX / setting.NumLevels;
                int pixelsH = terrain.elevationsMap.SizeY / setting.NumLevels;

                if (pixelsW < setting.PatchCol - 1) Console.WriteLine("suddivision x too big for heighmap size");
                if (pixelsH < setting.PatchRow - 1) Console.WriteLine("suddivision y too big for heighmap size");
            }
            // calculate the ranges of distance that activate a quadnode for each level

            LodMorthRadius = new LodRange[setting.NumLevels];

            for (int level = 0; level < setting.NumLevels; level++)
            {
                LodMorthRadius[level] = new LodRange(0,100000,100000);

                Vector3 d = (terrain.worldsize.max - terrain.worldsize.min) / (1 << level);

                float r = (float)Math.Sqrt(d.x * d.x + d.y * d.y + d.z * d.z);

                LodMorthRadius[level].radius = r*1.1f + (level == setting.NumLevels - 1 ? 0 : LodMorthRadius[level + 1].radius );

                if (setting.MorthTransition)
                {
                    //r = level < 1 ? LodMorthRadius[level].radius * 2 : LodMorthRadius[level - 1].radius;
                    r *= 2;
                    LodMorthRadius[level].morth.x = r * 0.99f;
                    LodMorthRadius[level].morth.y = r * 0.70f;
                }
            }

            // calculate the heights for all nodes
            Heights = new Vector2[setting.NumNodes];

            if (setting.OptimalPatchBoundary)
            {
                if (terrain.elevationsMap != null)
                {
                    // initialize an infinite values
                    for (int node = 0; node < setting.NumNodes; node++)
                    {
                        Heights[node] = new Vector2(
                            float.PositiveInfinity,
                            float.NegativeInfinity);
                    }
                    RecurseMinMaxHeight(0, 0, 0, 0, terrain.elevationsMap.SizeX, terrain.elevationsMap.SizeY);
                }
                else
                {
                    for (int node = 0; node < setting.NumNodes; node++)
                        Heights[node] = new Vector2(
                            terrain.worldsize.min.y,
                            (terrain.worldsize.max.y - terrain.worldsize.min.y) * 0.1f);
                }
            }
            else
            {
                for (int node = 0; node < setting.NumNodes; node++)
                    Heights[node] = new Vector2(terrain.worldsize.min.y, terrain.worldsize.max.y);
            }

            // initialize root node
            root = new QuadNode(this, terrain.worldsize, 0, 0, null);
            root.name = "Root";

            //root.RecursiveSplit();
        }

        /// <summary>
        /// Recursive draw, set once the vertexbuffer and draw all box... only for debug becuase 
        /// drawcall increase exponential.
        /// TODO : use harware instancing to avoid this performance issue
        /// </summary>
        public void Draw(ICamera camera)
        {
            if (setting.ShowHelpBox)
            {
                if (cameraSphere != null) 
                    cameraSphere.Draw(camera, Eye);

                if (boxSpline != null)
                {
                    boxSpline.SetToDevice(camera);
                    if (boxSpline.material != null)
                    {
                        // draw all box in one Begin() End(), commit new properties is more fast
                        int numpass = boxSpline.material.Begin();
                        for (int pass = 0; pass < numpass; pass++)
                        {
                            boxSpline.material.BeginPass(pass);
                            root.DrawHelpBox();
                            boxSpline.material.EndPass();
                        }
                        boxSpline.material.End();
                    }
                    else
                    {
                        root.DrawHelpBox();
                    }
                    if (boxSpline.font != null)
                    {
                        root.DrawHelpBoxName(boxSpline.font, camera);
                    }
                }
            }
            QuadNodeDrawCount = 0;
            TotalTrianglesCount = 0;

            if (setting.ShowQuadPatch && shader != null)
            {
                angle += 0.01f;

                shader.CameraEye = Eye;
                shader.LightDirection = Vector3.UnitY;
                //shader.LightDirection = Vector3.GetNormal(new Vector3(Math.Sin(angle), -1, Math.Cos(angle)));
                shader.ProjViewWorld = camera.Projection * camera.View * Matrix4.Identity;
                shader.TerrainMax = terrain.worldsize.max;
                shader.TerrainMin = terrain.worldsize.min;
                shader.HeightMap = terrain.heightmap;
                shader.IndexMap = terrain.layersidx;
                shader.GrassMap = terrain.grass;
                shader.ForestMap = terrain.forest;
                shader.RockMap = terrain.rock;

                int numpass = shader.Begin();

                if (patch != null)
                {
                    // Draw the default-patch
                    patch.SetToDevice();
                    for (int pass = 0; pass < numpass; pass++)
                    {
                        shader.BeginPass(pass);
                        root.Draw(false);
                        shader.EndPass();
                    }
                }

                if (semipatch != null)
                {
                    // Draw the semi-patch
                    semipatch.SetToDevice();
                    for (int pass = 0; pass < numpass; pass++)
                    {
                        shader.BeginPass(pass);
                        root.Draw(true);
                        shader.EndPass();
                    }
                }
                shader.End();
                QuadNodeDrawCount /= numpass;
                TotalTrianglesCount /= numpass;
            }

            if (setting.ShowQuadNormals && normalshader != null)
            {
                normalshader.CameraEye = Eye;
                normalshader.ProjViewWorld = camera.Projection * camera.View * Matrix4.Identity;
                normalshader.TerrainMax = terrain.worldsize.max;
                normalshader.TerrainMin = terrain.worldsize.min;
                normalshader.HeightMap = terrain.heightmap;

                int numpass = normalshader.Begin();
                if (normalpatch != null)
                {
                    // Draw the default-patch
                    normalpatch.SetToDevice();
                    for (int pass = 0; pass < numpass; pass++)
                    {
                        normalshader.BeginPass(pass);
                        root.DrawNormal(false);
                        normalshader.EndPass();
                    }
                }

                if (normalsemipatch != null)
                {
                    // Draw the semi-patch
                    normalsemipatch.SetToDevice();
                    for (int pass = 0; pass < numpass; pass++)
                    {
                        normalshader.BeginPass(pass);
                        root.DrawNormal(true);
                        normalshader.EndPass();
                    }
                }
                normalshader.End();
            }
        }

        /// <summary>
        /// Calculate the node to render 
        /// </summary>
        public void UpdateTree(Vector3 eye , Frustum frustum)
        {
            if (Vector3.Distance(Eye , eye) > 0.001f)
            {
                Eye = eye;
                root.UpdateQuadTree(eye, frustum);
                //root.RecursiveSelect(eye, frustum);
            }
        }

        /// <summary>
        /// Recursive function, update min-max from leaf node to parent node;
        /// TODO : the  heights.GetMinMax() use pixel by pixel... add true precision using patch suddivision
        /// </summary>
        void RecurseMinMaxHeight(int node, int level, int minx, int minz, int maxx, int maxz)
        {
            // if is leaf calculate min-max
            bool isleaf = level >= setting.NumLevels - 1;
            //bool isleaf = (node << 2) >= NUMNODES - 1;
            if (isleaf)
            {
                if (minx > maxx || minz > maxz) throw new ArgumentException();

                int dx = setting.PatchCol;
                int dy = setting.PatchRow;
                float miny, maxy;

                terrain.elevationsMap.GetMinMax(minx, minz, maxx, maxz, out miny, out maxy);

                //miny = miny * 0.9f;
                //maxy = clamp(maxy * 1.1f, 0, 1);

                miny = terrain.worldsize.min.y + miny * (terrain.worldsize.max.y - terrain.worldsize.min.y);
                maxy = terrain.worldsize.min.y + maxy * (terrain.worldsize.max.y - terrain.worldsize.min.y);

                //if you want set to all node the same size of world, simply pre-set a value to min and maxHeights
                Heights[node].x = miny;
                Heights[node].y = maxy;
                //minHeights[node] = min(minHeights[node], miny);
                //maxHeights[node] = max(maxHeights[node], maxy);
            }
            else
            {
                int down = node << 2;
                int hx = (maxx - minx) / 2;
                int hz = (maxz - minz) / 2;

                // LD LR TL TR
                RecurseMinMaxHeight(down + 1, level + 1, minx, minz, minx + hx, minz + hz);
                RecurseMinMaxHeight(down + 2, level + 1, minx + hx, minz, maxx, minz + hz);
                RecurseMinMaxHeight(down + 3, level + 1, minx, minz + hz, minx + hx, maxz);
                RecurseMinMaxHeight(down + 4, level + 1, minx + hx, minz + hz, maxx, maxz);
            }

            // update parent
            int par = node > 0 ? (node - 1) >> 2 : 0;
            Heights[par].x = minf(Heights[par].x, Heights[node].x);
            Heights[par].y = maxf(Heights[par].y, Heights[node].y);
        }
    }

    /// <summary>
    /// The core of algorithm, TODO : for a better memory usage, you can implement a pool class to avoid continuos Garbage Collection when node
    /// are splitted or merged
    /// </summary>
    public class QuadNode
    {     
        const int DL = 0;
        const int DR = 1;
        const int TL = 2;
        const int TR = 3;

        BoxAA bound;
        int level = 0;
        int node = 0;
        bool selected = false;
        bool degenerate = false;
        /// <summary>
        /// conectivity with top node , if root is null
        /// </summary>
        QuadNode parent = null;
        QuadTree terrain = null;
        /// <summary>
        /// conectivity with down nodes, if leaf are all null
        /// </summary>
        QuadNode[] child = null;
        /// <summary>
        /// convert a units box in this bound size
        /// </summary>
        Matrix4 boxtransform;
        Color32 boxcolor;

        public string name = "";

        /// <summary>
        /// THe used quad counter
        /// </summary>
        public static int NodeCount { get; private set; }
        /// <summary>
        /// The quad counter stored in memory, NodeCount &lt;= GarbageCount, depend garbage management
        /// </summary>
        public static int GarbageCount { get; private set; }
        /// <summary>
        /// </summary>
        /// <param name="size">the box volume of real world</param>
        public QuadNode(QuadTree terrain, BoxAA size, int level, int node, QuadNode parent)
        {
            if (terrain == null) throw new ArgumentNullException();

            this.terrain = terrain;
            this.level = level;
            this.parent = parent;
            this.node = node;
            this.boxcolor = Color32.Rainbow((float)level / terrain.setting.NumLevels);
            this.bound = size;

            Vector3 scale = (bound.max - bound.min) * 0.499f;
            Vector3 traslate = bound.Center;
            this.boxtransform = new Matrix4(
                scale.x, 0, 0, traslate.x,
                0, scale.y, 0, traslate.y,
                0, 0, scale.z, traslate.z,
                0, 0, 0, 1);

            if (NodeCount++ >= terrain.setting.NumNodes)
                throw new StackOverflowException("Warning, too many node created");

            GarbageCount++;
        }

        ~QuadNode()
        {
            merge();
            GarbageCount--;
        }

        int max(int i, int j) { return i > j ? i : j; }

        /// <summary>
        /// Return true if it's the last in the tree, so it will be rendered
        /// </summary>
        bool islast
        {
            get
            {
                if (child == null) return true;
                bool advance = false;
                for (int i = 0; i < 4; i++) advance |= child[i].selected;
                return !advance;
            }
        }

        /// <summary>
        /// Recursive drawing
        /// </summary>
        public void Draw(bool drawSemiPatch)
        {
            if (islast)
            {
                if (drawSemiPatch == degenerate)
                {
                    QuadMesh currPatch = degenerate ? terrain.semipatch : terrain.patch;

                    if (currPatch != null)
                    {
                        terrain.shader.Morph = (degenerate && level > 0) ? terrain.LodMorthRadius[level - 1].morth : terrain.LodMorthRadius[level].morth;
                        terrain.shader.PatchColor = degenerate ? Color32.Red : boxcolor;
                        terrain.shader.PatchOrigin = bound.min;
                        terrain.shader.PatchScale = bound.max - bound.min;
                        terrain.shader.PatchDimension = new Vector2(currPatch.Col - 1, currPatch.Row - 1);
                        currPatch.DrawMesh();
                        QuadTree.TotalTrianglesCount += currPatch.NumTriangles;
                    }
                    QuadTree.QuadNodeDrawCount++;
                }
            }
            else
            {
                for (int i = 0; i < 4; i++)
                    if (child[i] != null) child[i].Draw(drawSemiPatch);
            }

        }
        
        public void DrawNormal(bool drawSemiPatch)
        {
            if (islast)
            {
                if (drawSemiPatch == degenerate)
                {
                    QuadNormal currPatch = degenerate ? terrain.normalsemipatch : terrain.normalpatch;

                    if (currPatch != null)
                    {
                        terrain.normalshader.Morph = (degenerate && level > 0) ? terrain.LodMorthRadius[level - 1].morth : terrain.LodMorthRadius[level].morth;
                        terrain.normalshader.PatchColor = degenerate ? Color32.Red : Color32.Blue;
                        terrain.normalshader.PatchOrigin = bound.min;
                        terrain.normalshader.PatchScale = bound.max - bound.min;
                        terrain.normalshader.PatchDimension = new Vector2(currPatch.Col - 1, currPatch.Row - 1);
                        currPatch.DrawMesh();
                    }
                }
            }
            else
            {
                for (int i = 0; i < 4; i++)
                    if (child[i] != null) child[i].DrawNormal(drawSemiPatch);
            }

        }
  
        public void DrawHelpBox()
        {
            if (islast)
            {
                Color32 color = degenerate ?Color32.Black : boxcolor;
                terrain.boxSpline.Draw(boxtransform, color);
                QuadTree.QuadNodeDrawCount++;
            }
            else
            {
                for (int i = 0; i < 4; i++) child[i].DrawHelpBox();
            }
        }
        
        public void DrawHelpBoxName(Font font, ICamera camera)
        {
            if (islast)
            {
                Vector3 screen = Vector3.Project(boxtransform.Position, camera.Viewport, camera.Projection, camera.View, Matrix4.Identity);
                font.Draw(node + "_" + name, (int)screen.x, (int)screen.y, (System.Drawing.Color)boxcolor);
            }
            else
            {
                for (int i = 0; i < 4; i++) child[i].DrawHelpBoxName(font,camera);
            }
        }

        /// <summary>
        /// Generate all nodes
        /// </summary>
        public void RecursiveSplit()
        {
            // if it's last node in the tree can't advance
            if (level >= terrain.setting.NumLevels - 1) return;
            split();
            for (int i = 0; i < 4; i++)
                child[i].RecursiveSplit();
        }

        public void RecursiveSelect(Vector3 eye, Frustum frustum)
        {
            // if it's last node in the tree can't advance
            if (level >= terrain.setting.NumLevels - 1) return;
            
            if (selected =  PrimitiveIntersections.IntersectAABBSphere(bound.min,bound.max,eye, terrain.LodMorthRadius[level].radius))
            {
                if (child != null)
                    for (int i = 0; i < 4; i++)
                        child[i].RecursiveSelect(eye, frustum);
            }
            else
            {
                RecursiveUnSelect();
            }

        }
        
        public void RecursiveUnSelect()
        {
            if (child != null)
                for (int i = 0; i < 4; i++)
                {
                    child[i].selected = false;
                    child[i].RecursiveUnSelect();
                }
        }

        /// <summary>
        /// TODO : need a continuity lod transiction, each lod(i) must have a lod(i) or lod(i-1) neighbours 
        /// Recursive selection , return true if is splitted in 4 child
        /// </summary>
        public bool UpdateQuadTree(Vector3 eye, Frustum frustum)
        {
            selected = true;
            degenerate = false;
            if (terrain.setting.ForceDrawLeaf)
            {
                if (level >= terrain.setting.NumLevels - 1)
                {
                }
                else
                {
                    if (child==null) split();
                    for (int i = 0; i < 4; i++)
                        child[i].UpdateQuadTree(eye, frustum);
                    return true;
                }
            }
            if (!PrimitiveIntersections.IntersectAABBSphere(bound.min, bound.max, eye, terrain.LodMorthRadius[level].radius))
            {
                merge();
                return false;
            }
            else
            {
                // if last node in the tree can't advance
                if (level >= terrain.setting.NumLevels - 1)
                {
                    return true;
                }
                else
                {
                    if (child == null) split();

                    for (int i = 0; i < 4; i++)
                    {
                        child[i].UpdateQuadTree(eye, frustum);
                        child[i].degenerate = !PrimitiveIntersections.IntersectAABBSphere(bound.min, bound.max, eye, terrain.LodMorthRadius[level].radius);
                    }

                }
            }
            return true;
        }

        void split()
        {
            int down = node << 2;
            BoxAA sizeDL = new BoxAA(
                    new Vector3(bound.Center.x, terrain.Heights[down + 1].y, bound.Center.z),
                    new Vector3(bound.min.x, terrain.Heights[down + 1].x, bound.min.z));

            BoxAA sizeDR = new BoxAA(
                    new Vector3(bound.max.x, terrain.Heights[down + 2].y, bound.Center.z),
                    new Vector3(bound.Center.x, terrain.Heights[down + 2].x, bound.min.z));

            BoxAA sizeTL = new BoxAA(
                    new Vector3(bound.Center.x, terrain.Heights[down + 3].y, bound.max.z),
                    new Vector3(bound.min.x, terrain.Heights[down + 3].x, bound.Center.z));

            BoxAA sizeTR = new BoxAA(
                    new Vector3(bound.max.x, terrain.Heights[down + 4].y, bound.max.z),
                    new Vector3(bound.Center.x, terrain.Heights[down + 4].x, bound.Center.z));

            split(sizeDL, sizeDR, sizeTL, sizeTR);
        }
        /// <summary>
        /// Split this Quad into 4 children, pass a precomputer boundary
        /// </summary>
        void split(BoxAA sizeDL, BoxAA sizeDR, BoxAA sizeTL, BoxAA sizeTR)
        {
            int down = node << 2;
            
            if (child != null) merge();

            child = new QuadNode[4];
            child[DL] = new QuadNode(terrain, sizeDL, level + 1, down + 1, this);
            child[DR] = new QuadNode(terrain, sizeDR, level + 1, down + 2, this);
            child[TL] = new QuadNode(terrain, sizeTL, level + 1, down + 3, this);
            child[TR] = new QuadNode(terrain, sizeTR, level + 1, down + 4, this);
            child[DL].name = "downLeft";
            child[DR].name = "downRight";
            child[TL].name = "topLeft";
            child[TR].name = "topRight";
        }
        /// <summary>
        /// Remove the 4 children and free memory
        /// </summary>
        void merge()
        {
            if (child != null)
                for (int i = 0; i < 4; i++)
                    if (child[i]!=null)
                    {
                        child[i].merge();
                        child[i] = null;
                        NodeCount--;
                    }
            child = null;
            //GC.Collect();
        }

        public override string ToString()
        {
            return string.Format("LOD{0} {1} : size[{2}]", terrain.setting.NumLevels - level, name, bound);
        }
    }
}
