﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;

using Engine.Tools;
using Engine.Maths;
using Engine.Graphics;

namespace Engine.Cdlod
{
    public class Terrain
    {
        Random rnd = new Random();
        RenderDevice render;

        // true size of fvg.tga in meters
        public BoxAA worldsize = new BoxAA( new Vector3(-35000, 13, -35000),new Vector3(35000, 1437, 35000)); 
        public QuadTree quadTree;

        string resourcedir;       
        string heightname = "fvg2.tga";
        // i use same texture as layers mask of overmap... work in progress
        string layersname = "fvg_overlay.jpg";//"fvg_indices.tga";
        string grassname = "Grass.jpg";
        string rockname = "Rock.jpg";
        string forestname = "Forest.jpg";

        public Texture2D heightmap;
        public HeightMap elevationsMap;
        public Texture2D layersidx;
        public Texture2D grass, rock, forest;
        public Texture2D WhiteTexture;

        public Terrain(RenderDevice render)
        {
            this.render = render;
           
            // Set options
            worldsize = new BoxAA(worldsize.min / 100, worldsize.max / 100); // scale a little

            QuadTreeSetting setting = new QuadTreeSetting();
            setting.NumLevels = 6;
            setting.OptimalPatchBoundary = false;
            setting.MorthTransition = true;
            setting.PatchCol = 9;
            setting.PatchRow = 9;

            // load textures
            resourcedir = System.IO.Path.GetFullPath(@"..\..\..\..\Resources");

            heightmap = Texture2D.FromFilename(render, TextureUsage.Managed, Format.A8R8G8B8, resourcedir + "\\" + heightname, 3601, 3601);
            layersidx = Texture2D.FromFilename(render, TextureUsage.Managed, Format.A8R8G8B8, resourcedir + "\\" + layersname, 3601, 3601);
            grass = Texture2D.FromFilename(render, TextureUsage.Managed, Format.A8R8G8B8, resourcedir + "\\" + grassname);
            rock = Texture2D.FromFilename(render, TextureUsage.Managed, Format.A8R8G8B8, resourcedir + "\\" + rockname);
            forest = Texture2D.FromFilename(render, TextureUsage.Managed, Format.A8R8G8B8, resourcedir + "\\" + forestname);
            WhiteTexture = new Texture2D(render, TextureUsage.Managed, Format.A8R8G8B8, 1, 1, false);

            heightmap = layersidx = grass = rock = forest = WhiteTexture;

            // generate a height table for CPU access
            elevationsMap = HeightMap.FromTexture(heightmap);

            // initialize rootnode
            quadTree = new QuadTree(this, setting);


            // initialize the static geometries
            CdlodShader cdlodshader = new CdlodShader(render);
            quadTree.shader = cdlodshader;

            quadTree.patch = new QuadMesh(render, setting.PatchCol - 1, setting.PatchRow - 1);
            quadTree.semipatch = new QuadMesh(render, (setting.PatchCol - 1) / 2, (setting.PatchRow - 1) / 2);

            quadTree.boxSpline = new BoxHelper(render);
            quadTree.boxSpline.material = CDLODForm.materialP;
            //quadTree.boxSpline.font = font;


            CdlodNormalShader cdlodnormalshader = new CdlodNormalShader(render);
            quadTree.normalpatch = new QuadNormal(render, setting.PatchCol - 1, setting.PatchRow - 1, 1f);
            quadTree.normalsemipatch = new QuadNormal(render, (setting.PatchCol - 1) / 2, (setting.PatchRow - 1) / 2, 1f);
            quadTree.normalshader = cdlodnormalshader;


            float[] radius = new float[quadTree.LodMorthRadius.Length];
            for (int i = 0; i < radius.Length; i++) radius[i] = quadTree.LodMorthRadius[i].radius;

            quadTree.cameraSphere = new CameraHelper(render, radius);
            quadTree.cameraSphere.shader = CDLODForm.materialP;
        }

        public void Update(Vector3 eye, Frustum frustum)
        {
            quadTree.UpdateTree(eye, frustum);
        }

        public void Draw(ICamera camera)
        {
            quadTree.setting.ShowHelpBox = quadTree.shader.RenderMode != CdlodShader.TechniqueMode.Default;
            quadTree.setting.ShowQuadNormals = quadTree.shader.RenderMode == CdlodShader.TechniqueMode.Slope;
            
            quadTree.Draw(camera);
        }
    }

    public class Water
    {
        RenderDevice render;
        BoxAA TerrainSize;
        WaterShader shader;

        VertexBuffer vertexbuffer;
        VertexDeclaration declaration;

        public Water(RenderDevice render)
        {
            BoxAA size = new BoxAA();
            this.render = render;
            this.TerrainSize = size;
            this.shader = new WaterShader(render);
            this.shader.WireframeMode = false;

            declaration = new VertexDeclaration(render, VERTEX.m_elements);

            float h = size.min.y + (size.max.y - size.min.y) * 0.1f;

            VERTEX[] vertices = new VERTEX[]
            {
                new VERTEX(size.min.x,h,size.max.z),
                new VERTEX(size.max.x,h,size.max.z),
                new VERTEX(size.min.x,h,size.min.z),

                new VERTEX(size.min.x,h,size.min.z),
                new VERTEX(size.max.x,h,size.max.z),
                new VERTEX(size.max.x,h,size.min.z),
            };


            vertexbuffer = new VertexBuffer(render, declaration.Format, BufferUsage.Managed, 6);
            VertexStream data = vertexbuffer.OpenStream();
            data.WriteCollection<VERTEX>(vertices, 0, 6, 0);
            vertexbuffer.CloseStream();
        }

        public void Update(Vector3 eye, Frustum frustum)
        {

        }
        public void Draw(ICamera camera)
        {
            render.Device.SetVertexDeclaration(declaration);
            render.Device.SetVertexStream(vertexbuffer, 0);

            render.Device.renderstates.AlphaBlendEnable = true;
            render.Device.renderstates.ZBufferWriteEnable = false;

            int numpass = shader.Begin();
            for (int pass = 0; pass < numpass; pass++)
            {
                shader.BeginPass(pass);
                shader.ProjViewWorld = camera.Projection * camera.View;
                shader.CameraEye = camera.Eye;
                shader.LightDirection = Vector3.GetNormal(new Vector3(-1, -1, -1));
                render.Device.DrawPrimitives(PrimitiveType.TriangleList, 0, 2);
                shader.EndPass();
            }
            shader.End();

            render.Device.renderstates.AlphaBlendEnable = false;
            render.Device.renderstates.ZBufferWriteEnable = true;

        }
    }
}
