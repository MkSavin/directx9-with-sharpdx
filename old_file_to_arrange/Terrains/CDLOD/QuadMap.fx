//-----------------------------------------------------------------------------
// Globals.
//-----------------------------------------------------------------------------
#define MAXLEVELS 10
#define WHITE  float4(1,1,1,1)
#define BLACK  float4(0,0,0,1)
#define RED    float4(1,0,0,1)
#define YELLOW float4(1,1,0,1)
#define GREEN  float4(0,1,0,1)
#define CYAN   float4(0,1,1,1)
#define BLUE   float4(0,0,1,1)
#define MAGENT float4(1,0,1,1)


float4x4 WorldViewProj : WORLDVIEWPROJ;

float3  xPatchOrigin; // the min(x,y,z) position in global coord system of current patch
float3  xPatchScale;  // the scale factor of patch's vertices in (dx,dy,dz) format
float2  xPatchDim;    // the patch suddivision
float4  xPatchColor;  // debug patch with a custum color if diffuse texture aren't available
float2  xMorph;       // radiusmin - radiusmax = morph 0.0 - 1.0
float3  xTerrainMin;  // terrain bound min used to scale world
float3  xTerrainMax;  // terrain bound max used to scale world
float3  xLightDir;
float3  xEye;

Texture xHeightMap;

Texture xIndicesMap;

Texture xGrassMap;
Texture xRockMap;
Texture xForestMap;

// summary
// MAGFILTER default(POINT) : the texels are larger than screen pixels, so one texel covers multiple pixels
// MINFILTER default(POINT) : the texel is smaller than a pixel, so one pixel covers multiple texels
// MIPFILTER default(NONE)



sampler heightSampler = sampler_state 
{ 
	texture = <xHeightMap>;
	magfilter = LINEAR;
	minfilter = LINEAR;
	AddressU  = CLAMP; 
	AddressV  = CLAMP;
};

sampler indexSampler = sampler_state 
{ 
	texture = <xIndicesMap>;
	magfilter = LINEAR;
	minfilter = LINEAR;
	mipfilter = LINEAR;
	AddressU  = CLAMP; 
	AddressV  = CLAMP;
};

sampler grassSampler = sampler_state 
{ 
	texture = <xGrassMap>;
	magfilter = LINEAR;
	minfilter = LINEAR;
	mipfilter = LINEAR;
	AddressU  = WRAP; 
	AddressV  = WRAP;
};
sampler forestSampler = sampler_state 
{ 
	texture = <xForestMap>;
	magfilter = LINEAR;
	minfilter = LINEAR;
	mipfilter = LINEAR;
	AddressU  = WRAP; 
	AddressV  = WRAP;
};
sampler rockSampler = sampler_state 
{ 
	texture = <xRockMap>;
	magfilter = LINEAR;
	minfilter = LINEAR;
	mipfilter = LINEAR;
	AddressU  = WRAP; 
	AddressV  = WRAP;
};



//-----------------------------------------------------------------------------
// Vertex Shaders.
//-----------------------------------------------------------------------------
struct VS_INPUT
{
    float2  Position  : POSITION;
};

struct VS_OUTPUT
{   
	float4 Position : POSITION;
    float4 Color    : COLOR0;
	float2 TexCoord : TEXCOORD0;
	float3 Normal   : TEXCOORD1;
	float3 View     : TEXCOORD2;
	float  Height   : TEXCOORD3;
};


// convert unit vertex in global position
float4 GetGlobalPOS(float2 vertex)
{
	float4 pos = 0;
	pos.x = (vertex.x * xPatchScale.x) + xPatchOrigin.x;
	pos.z = (vertex.y * xPatchScale.z) + xPatchOrigin.z;
	pos.w = 1;
	return pos;
}

// find texcoord in the range [0,1] and swap V to orient correctly to XZ plane
// -----> u         z
// |                |
// |                |____x
// v
float2 GetGlobalUV(float4 vertex)
{
	float2 tex = 0;
	tex.x = (vertex.x - xTerrainMin.x) / (xTerrainMax.x - xTerrainMin.x);
	tex.y = 1.0f - (vertex.z - xTerrainMin.z) / (xTerrainMax.z - xTerrainMin.z);
	return tex;
}

// get height from map in 0.0f - 1.0f range
float GetHeight(float2 texcoord)
{
	return tex2Dlod(heightSampler, float4(texcoord.xy, 0, 0)).a;
}

void GetFourCornerHeight(float4 pos , out float n , out float s , out float e , out float w , out float dz , out float dx)
{
    dz = xPatchScale.x / xPatchDim.y;
    dx = xPatchScale.z / xPatchDim.x;
    n = GetHeight(GetGlobalUV(pos + float4(0,0,dz,0)));
    s = GetHeight(GetGlobalUV(pos - float4(0,0,dz,0)));
    e = GetHeight(GetGlobalUV(pos + float4(dx,0,0,0)));
    w = GetHeight(GetGlobalUV(pos - float4(dx,0,0,0)));
}

// get normal from height with 4 points
float3 GetNormalfromHeight4b(float4 pos,float h)
{
	float n, s, e, w, dz, dx;
	GetFourCornerHeight(pos , n , s , e , w , dz , dx);

    float3 ns = float3(0, n-s , dz);
    float3 ew = float3(dx, e-w , 0);

	float3 norm = cross(ns, ew);
	norm.y=norm.y*0.5f;

    return float3(normalize(norm));
}


// position = global position XZ
// vertex   = grid vertex in [0,1] range
float2 MorphVertex(float2 vertex, float2 position, float morphLerpK)
{
	float2 f = frac(vertex * xPatchDim.xy * 0.5f);
	return position - f * (xPatchScale.xz / xPatchDim.xy) * (1-morphLerpK) * 2;
}


VS_OUTPUT VShader(VS_INPUT input)
{
	VS_OUTPUT output = (VS_OUTPUT)0; 

	output.Position = GetGlobalPOS(input.Position);

	float dist = distance(output.Position.xyz, xEye);
	float morphLerpK = (dist - xMorph.x) / (xMorph.y - xMorph.x);
	morphLerpK = saturate(morphLerpK);
	output.Position.xz = MorphVertex(input.Position, output.Position.xz, morphLerpK);
	
	float2 texcoord = GetGlobalUV(output.Position);

	output.TexCoord = texcoord;

	// use argb encoded map
	float4 data = tex2Dlod(heightSampler, float4(texcoord, 0, 0));
	float3 normal = data.xyz * 2.0f - 1.0f;
	normal.y = 2;
	normal = normalize(normal);

	//normal = GetNormalfromHeight4b(output.Position , h);
	//output.Normal = GetNormalfromMap(output.TexCoord);
	
	output.Position.y = xTerrainMin.y + data.a * (xTerrainMax.y - xTerrainMin.y);
	output.Height = data.a;
	output.Normal = normal;
	output.View = normalize(output.Position.xyz);
	output.Color = saturate(xPatchColor * morphLerpK + (xPatchColor * 0.5f + WHITE * 0.5f) * (1 - morphLerpK));

	output.Position = mul(output.Position, WorldViewProj);
	return output;
}

//-----------------------------------------------------------------------------
// Pixel Shaders.
//-----------------------------------------------------------------------------

// Using procedural a indices texture as layers mask
float4 PShader_layers(VS_OUTPUT input) : COLOR0
{
	float4 color;
	// normal
	float3 normal = tex2D(heightSampler, input.TexCoord).xzy * 2.0f - 1.0f;
	// layers mask controller
	float4 indices = tex2D(indexSampler, input.TexCoord);
	// layers
	float4 grass = tex2D(grassSampler, input.TexCoord * 500.0f);
    float4 rocks = tex2D(rockSampler, input.TexCoord * 500.0f);	
	float4 trees = tex2D(forestSampler, input.TexCoord * 500.0f);	

	color = lerp(grass, rocks, indices.r);
	color = lerp(color, trees, indices.b);

	float dt = max(0.2f , dot(normal, -xLightDir));

	return saturate(color * dt);
}

// Using a big overlayed maps
float4 PShader_textured(VS_OUTPUT input) : COLOR0
{
	float4 color;
	// normal
	float3 normal = tex2D(heightSampler, input.TexCoord).xzy * 2.0f - 1.0f;
	float4 overmap = tex2D(indexSampler, input.TexCoord);
	float dt = max(0.2f , dot(normal, -xLightDir));

	return saturate(overmap * dt);
}

// Using procedural function instead use a indices texture as layers mask
float4 PShader_textured_proc(VS_OUTPUT input) : COLOR0
{
	float4 color;
	// normal
	float3 normal = tex2D(heightSampler, input.TexCoord).xzy * 2.0f - 1.0f;
	// layers
	float4 grass = tex2D(grassSampler, input.TexCoord * 50.0f);
    float4 rocks = tex2D(rockSampler, input.TexCoord * 500.0f);	
	float4 trees = tex2D(forestSampler, input.TexCoord * 500.0f);	

	float slope = normal.y;
	float height = input.Height;
	float dt = max(0.2f , dot(normal, -xLightDir));
	

	if (normal.y > 0.99f)
		color = grass;
	else if (normal.y > 0.9f)
		color = trees;
	else if (normal.y > 0.8f)
		lerp(trees , rocks , 0.5f);
	else 
		color = rocks;

	return saturate(color * dt);
}

// Show only grass layer
float4 PShader_textured_grass(VS_OUTPUT input) : COLOR0
{
	float3 normal = tex2D(heightSampler, input.TexCoord).xzy * 2.0f - 1.0f;
	float3 indices = tex2D(indexSampler, input.TexCoord).rgb;

	float4 color = tex2D(grassSampler, input.TexCoord * 10.0f);
	color = lerp(BLUE, color, indices.g);

	float dt = max(0.2f , dot(normal, -xLightDir));
	return saturate(color * dt);
}
float4 PShader_textured_rock(VS_OUTPUT input) : COLOR0
{
	float3 normal = tex2D(heightSampler, input.TexCoord).xzy * 2.0f - 1.0f;
	float3 indices = tex2D(indexSampler, input.TexCoord).rgb;

	float4 color = tex2D(rockSampler, input.TexCoord * 10.0f);
	color = lerp(BLUE, color, indices.r);

	float dt = max(0.2f , dot(normal, -xLightDir));


	return saturate(color * dt);
}
float4 PShader_textured_forest(VS_OUTPUT input) : COLOR0
{
	float3 normal = tex2D(heightSampler, input.TexCoord).xzy * 2.0f - 1.0f;
	float3 indices = tex2D(indexSampler, input.TexCoord).rgb;

	float4 color = tex2D(forestSampler, input.TexCoord * 10.0f);
	color = lerp(BLUE, color, indices.b);

	float dt = max(0.2f , dot(normal, -xLightDir));
	return saturate(color * dt);
}

// show the procedural slope claculation
float4 PShader_slope(VS_OUTPUT input) : COLOR0
{
	//float3 normal = input.Normal;
	float3 normal = tex2D(heightSampler, input.TexCoord).xzy * 2.0f - 1.0f;	
	// dot(normal , unity) = normal.y
	float slope = clamp((normal.y - 0.9f)/(1.0f - 0.9f), 0 , 1);
	
	float4 color;//BLUE * (1-slope) + WHITE * slope;

	if (normal.y > 0.99f)
		color = BLACK;
	else if (normal.y > 0.9f)
		color = BLUE;
	else if (normal.y > 0.8f)
		color = GREEN;
	else if (normal.y > 0.7f)
		color = YELLOW;
	else if (normal.y > 0.6f)
		color = RED;
	else 
		color = WHITE;

	return color;
}

float4 PShader_white(VS_OUTPUT input) : COLOR0
{
	float3 normal = tex2D(heightSampler, input.TexCoord).xzy * 2.0f - 1.0f;	
	float dt = dot(normal, -xLightDir);
	return WHITE * dt *0.5f + xPatchColor * dt;
}


// with nvidea plugin for photoshop i optain:
// tex2D.x == X direction
// tex2D.y == Z direction
// tex2D.z == Y direction
float4 PShader_normalX(VS_OUTPUT input) : COLOR0
{
	float3 normal = tex2D(heightSampler, input.TexCoord).xzy * 2.0f - 1.0f;
	return WHITE * (1-normal.x) + RED * normal.x;
}
float4 PShader_normalY(VS_OUTPUT input) : COLOR0
{
	float3 normal = tex2D(heightSampler, input.TexCoord).xzy * 2.0f - 1.0f;
	return WHITE * (1-normal.y) + GREEN * normal.y;
}
float4 PShader_normalZ(VS_OUTPUT input) : COLOR0
{
	float3 normal = tex2D(heightSampler, input.TexCoord).xzy * 2.0f - 1.0f;
	return WHITE * (1-normal.z) + BLUE * normal.z;
}


float4 PShader_black(VS_OUTPUT input) : COLOR0
{
	return BLACK;
}
//-----------------------------------------------------------------------------
// Techniques.
//-----------------------------------------------------------------------------
technique Default
{
    pass p0
    {
		CullMode = CCW;
		FillMode = Solid;
		DepthBias = 0;
        VertexShader = compile vs_3_0 VShader();
		PixelShader =  compile ps_3_0 PShader_textured();
    }
}
technique Default_Procedural
{
    pass p0
    {
		CullMode = CCW;
		FillMode = Solid;
		DepthBias = 0;
        VertexShader = compile vs_3_0 VShader();
		PixelShader =  compile ps_3_0 PShader_textured_proc();
    }
}
technique Slope
{
    pass p0
    {
		CullMode = CCW;
		FillMode = Solid;
        VertexShader = compile vs_3_0 VShader();
		PixelShader =  compile ps_3_0 PShader_slope();
    }
	
	pass p1
    {
		CullMode = CCW;
		FillMode = Wireframe;
        VertexShader = compile vs_3_0 VShader();
		PixelShader =  compile ps_3_0 PShader_black();
    }
}
technique NormalX
{
    pass p0
    {
		CullMode = CCW;
		FillMode = Solid;
        VertexShader = compile vs_3_0 VShader();
		PixelShader =  compile ps_3_0 PShader_normalX();
    }
}
technique NormalY
{
    pass p0
    {
		CullMode = CCW;
		FillMode = Solid;
        VertexShader = compile vs_3_0 VShader();
		PixelShader =  compile ps_3_0 PShader_normalY();
    }
}
technique NormalZ
{
    pass p0
    {
		CullMode = CCW;
		FillMode = Solid;
        VertexShader = compile vs_3_0 VShader();
		PixelShader =  compile ps_3_0 PShader_normalZ();
    }
}
technique Wireframe
{
    pass p0
    {
		CullMode = CCW;
		FillMode = Solid;
		DepthBias = 0;
        VertexShader = compile vs_3_0 VShader();
		PixelShader =  compile ps_3_0 PShader_white();
    }
	pass p1
    {
		CullMode = CCW;
		FillMode = Wireframe;
		//DepthBias = -0.00001f;

        VertexShader = compile vs_3_0 VShader();
		PixelShader =  compile ps_3_0 PShader_black();
    }
}