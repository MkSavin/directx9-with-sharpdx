//-----------------------------------------------------------------------------
// Globals.
//-----------------------------------------------------------------------------
#define MAXLEVELS 10
#define WHITE float4(1,1,1,1)
#define BLACK float4(0,0,0,1)
#define RED   float4(1,0,0,1)
#define GREEN float4(0,1,0,1)
#define BLUE  float4(0,0,1,1)


float4x4 WorldViewProj : WORLDVIEWPROJ;

float3  xPatchOrigin; // the min(x,y,z) position in global coord system of current patch
float3  xPatchScale;  // the scale factor of patch's vertices in (dx,dy,dz) format
float2  xPatchDim;    // the patch suddivision
float4  xPatchColor;  // debug patch with a custum color if diffuse texture aren't available
float2  xMorph;       // radiusmin - radiusmax = morph 0.0 - 1.0
float3  xTerrainMin;  // terrain bound min used to scale world
float3  xTerrainMax;  // terrain bound max used to scale world

float3  xEye;
Texture xHeightMap;

sampler heightSampler = sampler_state 
{ 
	texture = <xHeightMap>;
	magfilter = LINEAR;
	minfilter = LINEAR;
	mipfilter = LINEAR;
	AddressU  = CLAMP; 
	AddressV  = CLAMP;
};
//-----------------------------------------------------------------------------
// Vertex Shaders.
//-----------------------------------------------------------------------------
struct VS_INPUT
{
    float3  Position  : POSITION;
};

struct VS_OUTPUT
{   
	float4 Position : POSITION;
    float4 Color    : COLOR0;
};


// convert unit vertex in global position
float4 GetGlobalPOS(float2 vertex)
{
	float4 pos = 0;
	pos.x = (vertex.x * xPatchScale.x) + xPatchOrigin.x;
	pos.z = (vertex.y * xPatchScale.z) + xPatchOrigin.z;
	pos.w = 1;
	return pos;
}

// find texcoord in the range [0,1] and swap V to orient correctly to XZ plane
// -----> u         z
// |                |
// |                |____x
// v
float2 GetGlobalUV(float4 vertex)
{
	float2 tex = 0;
	tex.x = (vertex.x - xTerrainMin.x) / (xTerrainMax.x - xTerrainMin.x);
	tex.y = 1.0f - (vertex.z - xTerrainMin.z) / (xTerrainMax.z - xTerrainMin.z);
	return tex;
}

// get height from map in 0.0f - 1.0f range
float GetHeight(float2 texcoord)
{
	return tex2Dlod(heightSampler, float4(texcoord.xy, 0, 0)).a;
}

void GetFourCornerHeight(float4 pos , out float n , out float s , out float e , out float w , out float dz , out float dx)
{
    dz = (xPatchScale.x / xPatchDim.y);
    dx = (xPatchScale.z / xPatchDim.x) ;
    n = GetHeight(GetGlobalUV(pos + float4(0,0,dz,0)));
    s = GetHeight(GetGlobalUV(pos - float4(0,0,dz,0)));
    e = GetHeight(GetGlobalUV(pos + float4(dx,0,0,0)));
    w = GetHeight(GetGlobalUV(pos - float4(dx,0,0,0)));
}

// get normal from height with 4 points
float3 GetNormalfromHeight4a(float4 pos,float h)
{	

	float n, s, e, w, dz, dx;
	GetFourCornerHeight(pos , n , s , e , w , dz , dx);
	float3 norm = float3(w-e, 2 , s-n);
	norm = normalize(norm);
    return norm;

}

// get normal from height with 4 points
float3 GetNormalfromHeight4b(float4 pos,float h)
{
	float n, s, e, w, dz, dx;
	GetFourCornerHeight(pos , n , s , e , w , dz , dx);

    float3 ns = float3(0, n-s , dz);
    float3 ew = float3(dx, e-w , 0);

	float3 norm = cross(ns, ew);
	norm.y=norm.y*0.5f;

    return float3(normalize(norm));
}


// position = global position XZ
// vertex   = grid vertex in [0,1] range
float2 MorphVertex(float2 vertex, float2 position, float morphLerpK)
{
	float2 f = frac(vertex * xPatchDim.xy * 0.5f);
	return position - f * (xPatchScale.xz / xPatchDim.xy) * (1-morphLerpK) * 2;
}


VS_OUTPUT VShader(VS_INPUT input)
{
	VS_OUTPUT output = (VS_OUTPUT)0; 

	output.Position = GetGlobalPOS(input.Position.xy);

	float dist = distance(output.Position.xyz, xEye);
	float morphLerpK = (dist - xMorph.x) / (xMorph.y - xMorph.x);
	morphLerpK = saturate(morphLerpK);
	output.Position.xz = MorphVertex(input.Position.xy, output.Position.xz,morphLerpK);
	
	float2 texcoord = GetGlobalUV(output.Position);

	// use argb encoded map
	float4 data = tex2Dlod(heightSampler, float4(texcoord, 0, 0));
	float3 normal = data.xzy * 2.0f - 1.0f;
	normal.y = 2;
	normal = normalize(normal);

	//normal = GetNormalfromHeight4a(output.Position , data.a);

	output.Position.y = xTerrainMin.y + data.a * (xTerrainMax.y - xTerrainMin.y);

	output.Position.xyz += normal * input.Position.z;

	output.Position = mul(output.Position, WorldViewProj);
	output.Color = BLUE * input.Position.z + GREEN * (1 - input.Position.z);
	return output;
}

//-----------------------------------------------------------------------------
// Pixel Shaders.
//-----------------------------------------------------------------------------

float4 PShader_black(VS_OUTPUT input) : COLOR0
{
	return input.Color;
}

//-----------------------------------------------------------------------------
// Techniques.
//-----------------------------------------------------------------------------
technique DefaultTechique
{
    pass p0
    {
        VertexShader = compile vs_3_0 VShader();
		PixelShader =  compile ps_3_0 PShader_black();
    }
}