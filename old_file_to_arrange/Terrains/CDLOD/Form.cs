﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Diagnostics;

using Engine.Graphics;
using Engine.Maths;
using Engine.Tools;
using Engine.Renderers;
using Engine.Inputs;

using Font = Engine.Graphics.Font;


namespace Engine.Cdlod
{
    public partial class CDLODForm : Form
    {
        /// <summary>
        /// Punto di ingresso principale dell'applicazione.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //Application.EnableVisualStyles();
            //Application.SetCompatibleTextRenderingDefault(false);
            CDLODForm form = new CDLODForm();
            Application.Idle += new EventHandler(form.Application_Idle);
            Application.Run(form);
        }

        RenderDevice render;
        KeyboardDevice keyboard;

        public static MaterialFX_VCOLOR materialVC;
        public static MaterialFX_POSITION materialP;

        TrackBallCamera_new camera;
        Terrain terrain;
        Water water;
        AxisObj axis;
        Font font;
        PlayerObj player;
        Stopwatch stopWatch = new Stopwatch();
        

        int frames = 0;
        int LastTickCount = 0;
        int lastFramescount = 0;
        int StartTime = 0;
        

        void swap(ref int a, ref int b)
        {
            int t = a;
            a = b;
            b = t;
        }
        public CDLODForm()
        {
            this.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint | ControlStyles.Opaque, true);
            InitializeComponent();

            this.richTextBox.Enabled = false; // there are a little issue with MouseWheel Focus event

            render = new RenderDevice(this);
            keyboard = new KeyboardDevice(this);

            font = new Font(render, Engine.Graphics.Font.FontName.Arial, 8);
            materialVC = new MaterialFX_VCOLOR(render);
            materialP = new MaterialFX_POSITION(render);

            axis = new AxisObj(render);
            axis.material = materialVC;

            player = new PlayerObj(render);
            player.material = materialVC;

            player.PlayerPos = new Vector3(-5, 0, -5);
            camera = new TrackBallCamera_new(this, new Vector3(-7, 4, -7), Vector3.Zero, Vector3.UnitY, 0.1f, 2000.0f);

            terrain = new Terrain(render);
            water = new Water(render);

            StartTime = Environment.TickCount; 
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            frames++;

            if (Math.Abs(Environment.TickCount - LastTickCount) > 1000)
            {
                lastFramescount = frames;
                LastTickCount = Environment.TickCount;
                frames = 0;
            }

            if (render.CheckDeviceState()== DeviceState.OK)
            {
                render.BeginScene();
                Frustum frustum = new Frustum();

                render.Clear(Color.CornflowerBlue);

                axis.Draw(camera);
                player.Draw(camera);

                stopWatch.Start();
                //terrain.Update(camera.Eye, frustum);
                terrain.Update(player.PlayerPos, frustum);
                TimeSpan ts0 = stopWatch.Elapsed;
                terrain.Draw(camera);
                TimeSpan ts1 = stopWatch.Elapsed;
                stopWatch.Stop();
                stopWatch.Reset();

                //water.Update(player.PlayerPos, frustum);
                //water.Draw(camera);

                Vector3 pos;
                pos = Vector3.Project(axis.axe[0], camera.Viewport, camera.Projection, camera.View, Matrix4.Identity);
                font.Draw("X", (int)pos.x, (int)pos.y, Color.Red);
                pos = Vector3.Project(axis.axe[1], camera.Viewport, camera.Projection, camera.View, Matrix4.Identity);
                font.Draw("Y", (int)pos.x, (int)pos.y, Color.Green);
                pos = Vector3.Project(axis.axe[2], camera.Viewport, camera.Projection, camera.View, Matrix4.Identity);
                font.Draw("Z", (int)pos.x, (int)pos.y, Color.Blue);


                // theorical maximum triangle : if draw untill leaf node and there aren't semipatch because morph always 0
                int maxtriangles = terrain.quadTree.patch.NumTriangles * terrain.quadTree.setting.NumLeafNodes;

                int triangles = QuadTree.TotalTrianglesCount;
                int drawscall = QuadTree.QuadNodeDrawCount;
                string misura = "";
                if (triangles > 1000) { triangles /= 1000; misura = "K"; }
                if (triangles > 1000) { triangles /= 1000; misura = "M"; }

                font.Draw("FPS   : " + lastFramescount.ToString(), 10, 10, Color.Black);
                font.Draw(string.Format("Nodes : {0} / max{1}", QuadNode.NodeCount, terrain.quadTree.setting.NumNodes), 10, 20, Color.Black);
                font.Draw(string.Format("Triangles : {0} / max{1}", triangles, maxtriangles), 10, 30, Color.Black);
                font.Draw("Updating in : " + ts0.Ticks, 10, 40, Color.Black);
                font.Draw("Drawing in  : " + (ts1 - ts0).Ticks, 10, 50, Color.Black);

                this.richTextBox.Text = string.Format("Mode{0}\t: {1}\nDrawsCall\t: {2}\nTriangles\t: {3}{4}", 
                    (int)terrain.quadTree.shader.RenderMode,
                    terrain.quadTree.shader.RenderMode.ToString(),
                    drawscall, 
                    triangles,
                    misura);

                render.EndScene();
                render.Present();
            }

        }

        float velocity = 0.2f;
        bool spacedown = false;
        /// <summary>
        /// Very efficent than OnKeyDown event
        /// </summary>
        void UpdateMovement()
        {
            if (keyboard != null)
            {
                keyboard.ReadStates();

                if (keyboard.IsKeyDown(Key.Add)) velocity *= 1.05f;
                if (keyboard.IsKeyDown(Key.Subtract)) velocity *= 0.95f;

                if (keyboard.IsKeyDown(Key.W) || keyboard.IsKeyDown(Key.Up)) player.PlayerPos += Vector3.UnitX * velocity;
                if (keyboard.IsKeyDown(Key.S) || keyboard.IsKeyDown(Key.Down)) player.PlayerPos -= Vector3.UnitX * velocity;
                if (keyboard.IsKeyDown(Key.A) || keyboard.IsKeyDown(Key.Left)) player.PlayerPos += Vector3.UnitZ * velocity;
                if (keyboard.IsKeyDown(Key.D) || keyboard.IsKeyDown(Key.Right)) player.PlayerPos -= Vector3.UnitZ * velocity;
                if (keyboard.IsKeyDown(Key.Q)) player.PlayerPos += Vector3.UnitY * velocity;
                if (keyboard.IsKeyDown(Key.Z)) player.PlayerPos -= Vector3.UnitY * velocity;

                if (keyboard.IsKeyDown(Key.Space))
                {
                    if (!spacedown)
                    {
                        terrain.quadTree.shader.RenderModeIndex++;
                        terrain.quadTree.setting.ShowHelpBox = terrain.quadTree.shader.RenderMode == CdlodShader.TechniqueMode.Wireframe;
                        spacedown = true;
                    }
                    else
                    {

                    }
                }
                else
                {
                    spacedown = false;
                }


            }
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            if (render == null) return;
            render.Resize(new Viewport(ClientSize));
        }

        #region GAME LOOP
        bool draw = true;
        protected void Application_Idle(object sender, EventArgs e)
        {
            while (AppStillIdle && draw)
            {
                System.Threading.Thread.Sleep(10); //reduce CPU usage 
                UpdateMovement();
                this.Invalidate();
            }
        }
        public bool AppStillIdle
        {
            get
            {
                PeekMsg msg;
                return !PeekMessage(out msg, IntPtr.Zero, 0, 0, 0);
            }
        }
        [System.Security.SuppressUnmanagedCodeSecurity]
        [DllImport("User32.dll", CharSet = CharSet.Auto)]
        private static extern bool PeekMessage(out PeekMsg msg, IntPtr hWnd,
                 uint messageFilterMin, uint messageFilterMax, uint flags);

        [StructLayout(LayoutKind.Sequential)]
        public struct PeekMsg
        {
            public IntPtr hWnd;
            public Message msg;
            public IntPtr wParam;
            public IntPtr lParam;
            public uint time;
            public System.Drawing.Point p;
        }
        #endregion

    }
}
