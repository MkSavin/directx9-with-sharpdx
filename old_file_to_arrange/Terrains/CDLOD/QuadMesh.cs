﻿using System;
using System.Collections.Generic;
using System.Text;

using Engine.Graphics;
using Engine.Tools;
using Engine.Maths;

namespace Engine.Cdlod
{
    /// <summary>
    /// The static mesh geometry, a fixed grid in 2D. The size must be a 2^n
    /// All vertices in [0.0 - 1.0] range
    /// </summary>
    public class QuadMesh : DisposableResource
    {
        public static int DrawCount = 0;

        RenderDevice render;
        VertexDeclaration m_decl;
        VertexBuffer m_vb;
        IndexBuffer m_ib;
        PrimitiveType m_type;

        public int Row { get; private set; }
        public int Col { get; private set; }
        public int NumTriangles { get; private set; }

        /// <summary>
        /// </summary>
        /// <param name="xsuddivision">remember that num of columns = xsudd + 1</param>
        /// <param name="ysuddivision">remember that num of rows = ysudd + 1</param>
        /// <param name="useStrip">can triangleStrip primitive improve performances ?</param>
        public QuadMesh(RenderDevice render, int xsuddivision, int ysuddivision)
        {
            this.render = render;
            Row = ysuddivision+1;
            Col = xsuddivision+1;
            NumTriangles = 0;

            if (Row < 1 || Col < 1 || Row * Col > ushort.MaxValue - 1)
                throw new ArgumentOutOfRangeException(string.Format("Grid size {0}x{1} too big", Row, Col));

            m_type = PrimitiveType.TriangleList;

            Restore();
        }

        public void SetToDevice()
        {
            render.Device.SetVertexDeclaration(m_decl);
            render.Device.SetVertexStream(m_vb, 0);
            render.Device.SetIndexStream(m_ib);
        }

        public void DrawMesh()
        {
            render.Device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, m_vb.Count, 0, m_ib.Count);
            DrawCount++;
        }

        int vertex(int i, int j) { return i + j * Col; }

        /// <summary>
        /// <para>xsudd = ysudd = 2</para>
        /// <para>6---7---8</para>
        /// <para>|   |   |</para>
        /// <para>3---4---5</para>
        /// <para>|   |   |</para>
        /// <para>0---1---2</para>
        /// </summary>
        void getIndexedGrid(int xsudd, int ysudd, out Vector2[] vertices, out Face16[] indices)
        {
            int numverts = (xsudd + 1) * (ysudd + 1);
            int numtris = xsudd * ysudd * 2;
            if (numverts > ushort.MaxValue - 1) throw new OverflowException("too many vertices for 16bit indices");

            vertices = new Vector2[numverts];

            for (int n = 0, j = 0; j <= ysudd; j++)
                for (int i = 0; i <= xsudd; i++)
                    vertices[n++] = new Vector2((float)i / xsudd, (float)j / ysudd);

            indices = new Face16[numtris];
            for (int n = 0,j = 0; j < ysudd; j++)
                for (int i = 0; i < xsudd; i++)
                {
                    indices[n++] = new Face16(vertex(i, j), vertex(i, j + 1), vertex(i + 1, j + 1));
                    indices[n++] = new Face16(vertex(i, j), vertex(i + 1, j + 1), vertex(i + 1, j));
                }
        }

        public void Restore()
        {
            Vector2[] vertices;

            if (m_type == PrimitiveType.TriangleList)
            {
                Face16[] indices;
                getIndexedGrid(Col-1, Row-1, out vertices, out indices);

                List<VertexElement> m_elements = new List<VertexElement>();
                m_elements.Add(new VertexElement(0, 0, DeclarationType.Float2, DeclarationMethod.Default, DeclarationUsage.Position, 0));
                m_decl = new VertexDeclaration(render, m_elements);


                m_ib = new IndexBuffer(render, IndexInfo.Face16, BufferUsage.Managed, indices.Length);
                IndexStream idata = m_ib.OpenStream();
                idata.WriteCollection<Face16>(indices, IndexInfo.Face16, 0, indices.Length, 0, 0);
                m_ib.CloseStream();
                m_ib.Count = indices.Length;
                NumTriangles = m_ib.Count;
            }
            else
            {
                throw new NotImplementedException();
            }
            m_vb = new VertexBuffer(render, m_decl.Format, BufferUsage.Managed, vertices.Length);
            VertexStream vdata = m_vb.OpenStream();
            vdata.WriteCollection<Vector2>(vertices, 0, vertices.Length, 0);
            m_vb.CloseStream();
            m_vb.Count = vertices.Length;

            
        }

        protected override void Dispose(bool disposing)
        {
            m_vb.Dispose();
            m_ib.Dispose();
        }

        public override string ToString()
        {
            return string.Format("{0}x{1}", Row, Col);
        }

    }

    /// <summary>
    /// the static lines geometry to show normal
    /// </summary>
    public class QuadNormal : DisposableResource
    {
        public static int DrawCount = 0;
        RenderDevice render;
        VertexDeclaration m_decl;
        VertexBuffer m_vb;
        PrimitiveType m_type;
        float lenght;

        public int Row { get; private set; }
        public int Col { get; private set; }
        public int NumSegments { get; private set; }

        /// <summary>
        /// </summary>
        /// <param name="xsuddivision">remember that num of columns = xsudd + 1</param>
        /// <param name="ysuddivision">remember that num of rows = ysudd + 1</param>
        /// <param name="useStrip">can triangleStrip primitive improve performances ?</param>
        public QuadNormal(RenderDevice render, int xsuddivision, int ysuddivision, float normalLength)
        {
            this.render = render;
            Row = ysuddivision + 1;
            Col = xsuddivision + 1;
            NumSegments = 0;
            lenght = normalLength;

            if (Row < 1 || Col < 1 || Row * Col > ushort.MaxValue - 1)
                throw new ArgumentOutOfRangeException(string.Format("Grid size {0}x{1} too big", Row, Col));

            m_type = PrimitiveType.LineList;

            Restore();
        }

        public void SetToDevice()
        {
            render.Device.SetVertexDeclaration(m_decl);
            render.Device.SetVertexStream(m_vb, 0);
        }

        public void DrawMesh()
        {
            render.Device.DrawPrimitives(PrimitiveType.LineList, 0, m_vb.Count / 2);
            DrawCount++;
        }

        int vertex(int i, int j) { return i + j * Col; }


        void getIndexedGrid(int xsudd, int ysudd, out Vector3[] vertices)
        {
            int numverts = (xsudd + 1) * (ysudd + 1);
            int numtris = xsudd * ysudd * 2;
            if (numverts > ushort.MaxValue - 1) throw new OverflowException("too many vertices for 16bit indices");

            vertices = new Vector3[numverts*2];

            for (int n = 0, j = 0; j <= ysudd; j++)
                for (int i = 0; i <= xsudd; i++)
                {
                    vertices[n++] = new Vector3((float)i / xsudd, (float)j / ysudd, 0);
                    vertices[n++] = new Vector3((float)i / xsudd, (float)j / ysudd, lenght);
                }
        }

        public void Restore()
        {
            Vector3[] vertices;

            getIndexedGrid(Col - 1, Row - 1, out vertices);

            List<VertexElement> m_elements = new List<VertexElement>();
            m_elements.Add(new VertexElement(0, 0, DeclarationType.Float3, DeclarationMethod.Default, DeclarationUsage.Position, 0));
            m_decl = new VertexDeclaration(render, m_elements);

            m_vb = new VertexBuffer(render, m_decl.Format, BufferUsage.Managed, vertices.Length);
            VertexStream vdata = m_vb.OpenStream();
            vdata.WriteCollection<Vector3>(vertices, 0, vertices.Length, 0);
            m_vb.CloseStream();
            m_vb.Count = vertices.Length;
        }

        protected override void Dispose(bool disposing)
        {
            m_vb.Dispose();
        }

        public override string ToString()
        {
            return string.Format("{0}x{1}", Row, Col);
        }

    }
}
