﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

using Engine.Maths;
using Engine.Tools;
using Engine.Graphics;

namespace Engine.Cdlod
{
    /// <summary>
    /// </summary>
    public interface IHeightMap
    {
        /// <summary> the Width of image</summary>
        int SizeX { get; }
        /// <summary> the Height of image</summary>
        int SizeY { get; }
        /// <summary>
        /// Get the height in [0.0 - 1.0] range
        /// </summary>
        /// <remarks>
        /// the image are oriented to match with XZ plane
        /// <para>[-x,+z]---[+x,+z]</para>
        /// <para>   |         |</para>
        /// <para>   |         |</para>
        /// <para>[-x.-z]---[+x,-z]</para>
        /// </remarks>
        float this[float x, float z] { get; set; }
        /// <summary>
        /// i'm using <code>for (int x = startx; x &lt; endx; x++)</code>
        /// </summary>
        void GetMinMax(int startx, int starty, int endx, int endy, out float Min, out float Max);
    }

    /// <summary>
    /// </summary>
    public class HeightMap : IHeightMap
    {
        public int SizeX { get; private set; }
        public int SizeY { get; private set; }

        byte[,] map;
        

        private HeightMap(int sizeX, int sizeY)
        {
            this.SizeX = sizeX;
            this.SizeY = sizeY;
            map = new byte[sizeX, sizeY];
        }

        float clamp(float f) { return f < 0 ? 0 : f > 1 ? 1 : f; }

        /// <summary>
        /// use [0.0 - 1.0] range to extract height
        /// </summary>
        public float this[float x, float z]
        {
            get { return map[(int)(clamp(x) * SizeX), (int)(clamp(z) * SizeY)] * 255.0f; }
            set { map[(int)(clamp(x) * SizeX), (int)(clamp(z) * SizeY)] = (byte)(value * 255.0f); }
        }
        /// <summary>
        /// use [0 - SizeX-1] range to extract height
        /// </summary>
        public float this[int x, int y]
        {
            get { return map[x, y] * 255.0f; }
            set { map[x, y] = (byte)(value * 255.0f); }
        }

        /// <summary>
        /// </summary>
        public void GetMinMax(int startx, int starty, int endx, int endy, out float Min, out float Max)
        {
            byte min = 255;
            byte max = 0;

            if (endx >= SizeX) endx = SizeX - 1;
            if (endy >= SizeY) endy = SizeY - 1;

            for (int x = startx; x <= endx; x++)
                for (int y = starty; y <= endy; y++)
                {
                    byte b = map[x, y];
                    if (b < min) min = b;
                    if (b > max) max = b;
                }
            Min = min / 255.0f;
            Max = max / 255.0f;
        }

        /// <summary>
        /// My custom implementation to generate normals data from height map
        /// Like nvidea plugin the y and z are swapped
        /// </summary>
        public Normal24[,] GetNormalMap(float pStrength)
        {
            int count = 0;
            Normal24[,] normals = new Normal24[SizeX, SizeY];

            for (int i = 1; i < SizeX - 1; i++)
                for (int j = 1; j < SizeY - 1; j++)
                {
                    byte m = map[i, j];
                    //top , topright , right , bottomright , bottom, bottomleft, left, topleft
                    byte t = map[i, j + 1];
                    byte tr = map[i + 1, j + 1];
                    byte r = map[i + 1, j];
                    byte br = map[i + 1, j - 1];
                    byte b = map[i - 1, j - 1];
                    byte bl = map[i - 1, j - 1];
                    byte l = map[i - 1, j];
                    byte tl = map[i - 1, j + 1];

                    // sobel filter
                    float dx = (tr + 2.0f * r + br) - (tl + 2.0f * l + bl);
                    float dz = (bl + 2.0f * b + br) - (tl + 2.0f * t + tr);
                    float dy = -1.0f / pStrength;

                    float lenght = (float)Math.Sqrt(dx * dx + dy * dy + dz * dz);
                    Normal24 nrgb = new Normal24(dx / lenght, dy / lenght, dz / lenght);

                    normals[i, j] = nrgb;
                    count++;
                }

            for (int i = 0; i < SizeX; i++, count+=2)
                normals[i, 0] = normals[i, SizeY - 1] = new Normal24(0, 0, 1);

            for (int j = 1; j < SizeY-1; j++ , count+=2)
                normals[0, j] = normals[SizeX - 1, j] = new Normal24(0, 0, 1);

            //System.Diagnostics.Debug.Assert(count == normals.Length, ""); 

            return normals;
        }

        /// <summary>
        /// return the bitmap data in a8r8g8b8 than rappresent the terrain texture used in shader.
        /// The normals are calculate with sobel filter. The Y coordinate are swapped to respect bitmap format
        /// </summary>
        /// <returns></returns>
        public Bitmap GetEncodeTerrain()
        {
            Bitmap bmp = new Bitmap(SizeX, SizeY, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            Normal24[,] normals = GetNormalMap(10.0f);

            // remember to swap V
            for (int x = 0; x < SizeX; x++)
                for (int y = 0; y < SizeY; y++)
                    bmp.SetPixel(x, SizeY - y - 1, Color.FromArgb(map[x, y], normals[x, y].r, normals[x, y].g, normals[x, y].b));

            return bmp;
        }


        public static HeightMap FromTexture(Texture2D tex)
        {
            if (tex != null)
            {
                HeightMap tab = new HeightMap(tex.Width, tex.Height);

                int W = tex.Width;
                int H = tex.Height;

                if (tex.PixelFormat == Format.A8R8G8B8)
                {
                    TextureStream stream = tex.OpenStream(0, true);
                    Color32[,] data = stream.GetData();
                    tex.CloseStream();
                    for (int y = 0; y < H; y++)
                        for (int x = 0; x < W; x++)
                            tab.map[x, H - y - 1] = (byte)data[x, y].A;
                }
                else
                {
                }
                return tab;
            }
            else
            {
                HeightMap tab = new HeightMap(1, 1);
                tab.map[0, 0] = 1;
                return tab;
            }
        }

        public static HeightMap FromBitmap(Bitmap bmp)
        {
            Stopwatch watch = new Stopwatch();        
            watch.Start();

            HeightMap tab = new HeightMap(bmp.Width, bmp.Height);

            BitmapLock tool = new BitmapLock(bmp);
            tool.LockBits();
                
            // remember to swap V
            for (int x = 0; x < bmp.Width; x++)
                for (int y = 0; y < bmp.Height; y++)
                    tab.map[x, y] = (byte)(tool.GetPixel(x, bmp.Height - y - 1).GetBrightness() * 255.0f);
            tool.UnlockBits();

            watch.Stop();

            Console.WriteLine(string.Format("load map in {0}s:{1}ms", watch.Elapsed.Seconds, watch.Elapsed.Milliseconds));

            return tab;
        }

        /// <summary>
        /// To debug
        /// </summary>
        public static HeightMap WaveMap(int sizeX, int sizeY)
        {
            HeightMap tab = new HeightMap(sizeX, sizeY);

            float dx = (float)Math.PI * 2 / sizeX;
            float dy = (float)Math.PI * 2 / sizeY;

            for (int x = 0; x < sizeX; x++)
                for (int y = 0; y < sizeX; y++)
                {
                    float s = (float)Math.Sin(x * dx);
                    float c = (float)Math.Cos(y * dy);
                    tab[x, y] = s + c + 0.5f;
                }
            return tab;
        }

        public static HeightMap White(int sizeX, int sizeY)
        {
            HeightMap tab = new HeightMap(sizeX, sizeY);
            return tab;
        }
    }
}
