﻿using System;
using System.IO;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using Engine.Maths;
using Engine.Tools;
using Engine.Graphics;
using Engine.Inputs;

using Font = Engine.Graphics.Font;
using Engine.Geometry;
using Engine;

namespace Test
{
    public class NormalHLSL : Effect
    {
        public EffectParamMatrix VP , W ,WIT ;
        public EffectParamColor Diffuse ;
        public EffectParamFloat Lenght;

        public EffectTechnique DefaultTechnique;

        public NormalHLSL(Device device)
            : base(device, "NormalShader.fx")
        {

            VP = new EffectParamMatrix(this,"ViewProj");
            W = new EffectParamMatrix(this, "World");
            WIT = new EffectParamMatrix(this, "WorldInvTraspose");
            Diffuse = new EffectParamColor(this, "Diffuse");
            Lenght = new EffectParamFloat(this, "Lenght");
            DefaultTechnique = new EffectTechnique(this, "NormalLineTechnique");

            Technique = DefaultTechnique;
        }
    }



    public class NormalObj
    {
        Device device;

        // each buffer require a VertexInfo declaration to know its structure
        VertexBuffer vertexbuffer;
        VertexLayout vertexlayout;
        VertexDeclaration vertexdeclar;
        Matrix4 world, worldInvTrasp ;

        NormalHLSL effect;

        public NormalObj(Device device , IList<Vector3> vertices , IList<Vector3> normals , Matrix4 world)
        {
            this.device = device;
            this.world = world;
            this.worldInvTrasp = Matrix4.Inverse(world);
            this.worldInvTrasp.Traspose();


            vertexlayout = new VertexLayout();
            vertexlayout.Add(0, DeclarationType.Float3, DeclarationUsage.Position);
            vertexlayout.Add(0, DeclarationType.Float3, DeclarationUsage.Normal);
            vertexlayout.Add(0, DeclarationType.Float1, DeclarationUsage.TexCoord);
            vertexdeclar = new VertexDeclaration(device, vertexlayout);

            int numvertices = vertices.Count * 2;

            Vertex[] array = new Vertex[numvertices];

            for (int i = 0; i < numvertices; i++)
            {
                array[i] = new Vertex
                {
                    Pos = vertices[i / 2],
                    Norm = normals[i / 2],
                    Scalar = i % 2
                };
            }

            vertexbuffer = new VertexBuffer(device, vertexlayout, BufferUsage.Managed, numvertices);
            VertexStream stream = vertexbuffer.OpenStream();
            stream.WriteCollection<Vertex>(array, 0, numvertices, 0);
            stream.Close();
            vertexbuffer.Count = numvertices;

            this.effect = new NormalHLSL(device);
        }

        public void Draw(Matrix4 projection, Matrix4 view, Matrix4 world)
        {
            this.world = world;
            this.worldInvTrasp = Matrix4.Inverse(world);
            worldInvTrasp.Traspose();

            if (vertexbuffer != null)
            {
               // effect.Technique = effect.DefaultTechnique;
                effect.Diffuse.Value = Color32.Green;
                effect.VP.Value = projection * view;
                effect.W.Value = world;
                effect.WIT.Value = worldInvTrasp;
                effect.Lenght.Value = 1.0f;

                device.SetVertexDeclaration(vertexdeclar);
                device.SetVertexStream(vertexbuffer, 0);

                foreach (Pass pass in effect.DefaultTechnique)
                {
                    device.DrawPrimitives(PrimitiveType.LineList, 0, vertexbuffer.Count / 2);
                }

                /*
                int count = effect.Begin();

                for (int i = 0; i < count; i++)
                {
                    effect.BeginPass(i);
                    render.Device.DrawPrimitives(PrimitiveType.LineList, 0, vertexbuffer.Count / 2);
                    effect.EndPass();
                }
                effect.End();
                */
            }
        }


        [StructLayout(LayoutKind.Sequential, Pack = 4, Size = sizeof(float) * 7)]
        struct Vertex
        {
            public Vector3 Pos;
            public Vector3 Norm;
            public float Scalar;
            public override string ToString()
            {
                return string.Format("{0} [{1}] [{2}]", Scalar, Pos, Norm);
            }
        }
    }
}
