﻿using System;
using System.IO;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using Engine.Maths;
using Engine.Tools;
using Engine.Graphics;
using Engine.Inputs;

using Font = Engine.Graphics.Font;
using Engine;
using Engine.Content;
using Engine.Forms;

namespace Test
{
    public class TestForm : Form
    {
        Random rnd = new Random();

        RenderWindow render;

        Font font;
        PreciseTimer timer;

        EffectLine effectline;
        EffectLight effectlight;

        Axis axis0, axis1;

        MeshObj mesh;
        NormalObj norm;
        GameLoop gameLoop;

        SkyBox sky;

        bool loop = true;
        Light light = new Light();
        Vector3 scale = Vector3.One;


        RenderControl renderControl1;
        ComboBox comboBox1;
        Button buttonReset;

        public TestForm()
        {
            EngineResources.ContentFolder = @"C:\Users\john\PROJECTS\3dEngine\3dengineSharpDx\Engine\Resources\Content\";

            timer = new PreciseTimer();
            InitializeComponent();

            this.renderControl1.InitGraphic(null);
            this.renderControl1.DrawFunction = RenderWorld;
            render = this.renderControl1.render;

            axis0 = new Axis(render.Device, 10, Vector3.Zero);
            axis1 = new Axis(render.Device, 5, Vector3.UnitY);

            mesh = new MeshObj(render.Device);
            norm = new NormalObj(render.Device, mesh.Vertices, mesh.Normals,mesh.world);

            font = new Font(render.Device, Engine.Graphics.Font.FontName.Arial, 10);

            effectline = new EffectLine(render.Device);
            effectlight = new EffectLight(render.Device);

            foreach (EffectTechnique tech in effectlight.Techniques.Values)
            {
                comboBox1.Items.Add(tech.Name);
            }
            comboBox1.SelectedIndex = 0;


            //Console.WriteLine(effect.ToString());

            sky = new SkyBox(render.Device, EngineResources.SunsetTextureCube);

            gameLoop = new GameLoop(UpdateWorld, 10, Render, 0);
            gameLoop.StartLooping();
        }

        float yrotation = 0.0f;

        private void UpdateWorld(double dt)
        {
            // calculate 10°/second
            if (dt > 0)
                yrotation += (float)(Math.PI * 2.0 * 10.0 / 360.0 * dt);

            this.statusLabel.Text = gameLoop.ToString();

            float max = (float)(Math.Sin(yrotation)*10 + 10.0);
            float min = max / 2.0f;
            int delay = rnd.Next((int)min, (int)max);
            //Console.SetCursorPosition(0, 1);
            //Console.WriteLine("Update delay " + delay);
            //System.Threading.Thread.Sleep(delay);


            float x = (float)Math.Sin(yrotation * 2);
            float y = (float)Math.Sin(yrotation * 4);
            float z = (float)Math.Sin(yrotation * 8);


            scale.x = 2.0f * (float)Math.Pow(2, x);
            scale.y = 2.0f * (float)Math.Pow(2, y);
            scale.z = 2.0f * (float)Math.Pow(2, z);

        }

        int draw = 0;

        private void RenderWorld(RenderControl sender, ICamera camera)
        {
            try
            {

                EffectTechnique technique = effectlight.GetTechniqueByName(comboBox1.SelectedItem.ToString());


                Matrix4 projection = camera.Projection;
                Matrix4 view = camera.View;
                Matrix4 iview = camera.CameraView;

                effectlight.LightDirection.Value = light.LDirection;

                effectlight.Light.Value = (Vector4)light.LColor * light.LIntensity;
                effectlight.Ambient.Value = Vector4.Zero;
                effectlight.Diffuse.Value = (Vector4)Color.Blue * light.LIntensity;

                if (render.Device.CanDraw())
                {
                    render.ClearDraw(Color.CornflowerBlue);
                    render.BeginDraw();
                    draw++;

                    Matrix4 scalemat = Matrix4.Scaling(scale);

                    //render.Device.ClearShaderCode();
                    mesh.Draw(projection, view, scalemat, iview, effectlight, technique);
                    norm.Draw(projection, view, scalemat);
                    axis1.Draw(projection * view, effectline);
                    //axis0.Draw(projection * view, effect);

                    sky.DrawLast(view, projection);

                    render.EndDraw();
                    render.Present();

                    //Console.WriteLine(string.Format("Num SetValue calls = {0}", EffectParam.SetValueCalls));

                }


            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void Render(double alpha)
        {
            this.renderControl1.Invalidate();
        }

        private void buttonReset_Click(object sender, EventArgs e)
        {
            if (loop)
            {
                gameLoop.StopLooping();
                buttonReset.Text = "Resume";
            }
            else
            {
                gameLoop.StartLooping();
                buttonReset.Text = "Stop";
            }
            loop = !loop;
        }

        private StatusStrip statusStrip;
        private ToolStripStatusLabel statusLabel;

        private void InitializeComponent()
        {
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.buttonReset = new System.Windows.Forms.Button();
            this.renderControl1 = new Engine.Forms.RenderControl();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.statusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusLabel});
            this.statusStrip.Location = new System.Drawing.Point(0, 378);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(800, 22);
            this.statusStrip.TabIndex = 0;
            this.statusStrip.Text = "statusStrip";
            // 
            // statusLabel
            // 
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(43, 17);
            this.statusLabel.Text = "mouse";
            // 
            // buttonReset
            // 
            this.buttonReset.Location = new System.Drawing.Point(12, 12);
            this.buttonReset.Name = "buttonReset";
            this.buttonReset.Size = new System.Drawing.Size(64, 35);
            this.buttonReset.TabIndex = 1;
            this.buttonReset.Text = "Stop";
            this.buttonReset.UseVisualStyleBackColor = true;
            this.buttonReset.Click += new System.EventHandler(this.buttonReset_Click);
            // 
            // renderControl1
            // 
            this.renderControl1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(189)))), ((int)(((byte)(230)))), ((int)(((byte)(196)))));
            this.renderControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.renderControl1.Location = new System.Drawing.Point(0, 0);
            this.renderControl1.Name = "renderControl1";
            this.renderControl1.Size = new System.Drawing.Size(800, 400);
            this.renderControl1.TabIndex = 2;
            // 
            // comboBox1
            // 
            this.comboBox1.Dock = System.Windows.Forms.DockStyle.Right;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(609, 0);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(191, 21);
            this.comboBox1.TabIndex = 3;
            // 
            // TestForm
            // 
            this.ClientSize = new System.Drawing.Size(800, 400);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.buttonReset);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.renderControl1);
            this.Name = "TestForm";
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }


    }
    
    public class Light
    {
        public Vector3 LDirection = (Vector3.One).Normal;
        public Color32 LColor = Color32.White;
        public float LIntensity = 1;
    }

    static class Program
    {
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new TestForm());
        }
    }


}
