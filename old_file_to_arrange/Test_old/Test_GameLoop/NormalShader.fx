//-----------------------------------------------------------------------------
// Basic Shader program
//-----------------------------------------------------------------------------
float4x4 ViewProj;
float4x4 World;
float4x4 WorldInvTraspose;
float4   Diffuse;
float    Lenght;

struct VS_INPUT
{
    float4  Position : POSITION;
	float3  Normal   : NORMAL;
	float   Scalar   : TEXCOORD;
};

struct VS_OUTPUT
{   
    float4  Position : POSITION;
};


//-----------------------------------------------------------------------------
// Vertex Shaders.
//-----------------------------------------------------------------------------

VS_OUTPUT VSVertex(VS_INPUT input)
{
	VS_OUTPUT output = (VS_OUTPUT)0; 

	float3 normal = mul(input.Normal, (float3x3)WorldInvTraspose);

	output.Position = mul(input.Position, World);
	output.Position.xyz += normalize(normal) * input.Scalar * Lenght;
	output.Position = mul(output.Position, ViewProj);

	return output;
}

//-----------------------------------------------------------------------------
// Pixel Shaders.
//-----------------------------------------------------------------------------

float4 PSDiffuse(VS_OUTPUT input) : COLOR0
{
	return Diffuse;
}

//-----------------------------------------------------------------------------
// Techniques.
//-----------------------------------------------------------------------------
technique NormalLineTechnique
{
    pass passone
    {
        VertexShader = compile vs_2_0 VSVertex();
		PixelShader =  compile ps_2_0 PSDiffuse();
    } 
    pass passtwo
    {
        VertexShader = compile vs_2_0 VSVertex();
		PixelShader =  compile ps_2_0 PSDiffuse();
    } 

}