﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using Engine.Maths;
using Engine.Tools;
using Engine.Graphics;
using Engine.Geometry;
using Engine.Physics;


namespace Engine.Test
{

    public class BodyShape : RigidBody2D
    {
        static Point[] polygon = new Point[5];

        public RectangleO rect;
        public Circle circle;


        public BodyShape(RectangleO obj, Vector2 velocity) : base(obj.Center,velocity)
        {
            this.rect = obj;
            this.circle = new Circle(obj.Center, (float)Math.Sqrt(obj.halfsize.x * obj.halfsize.x + obj.halfsize.y * obj.halfsize.y));
            this.Mass = circle.Area;
        }

        public void Draw(System.Drawing.Graphics g, Rectangle client, IRectangleAA world)
        {
            polygon[0] = MathsUtils.TransformCoordinateToScreen(rect.Corner0, client, world);
            polygon[1] = MathsUtils.TransformCoordinateToScreen(rect.Corner1, client, world);
            polygon[2] = MathsUtils.TransformCoordinateToScreen(rect.Corner2, client, world);
            polygon[3] = MathsUtils.TransformCoordinateToScreen(rect.Corner3, client, world);
            polygon[4] = polygon[0];

            for (int i = 0; i < 4; i++)
            {
                g.FillRectangle(Brushes.Blue, polygon[i].X - 1, polygon[i].Y - 1, 3, 3);
                g.DrawString("C" + i.ToString(), SystemFonts.CaptionFont, Brushes.Black, polygon[i].X, polygon[i].Y);
            }
            g.DrawPolygon(Pens.DarkGray, polygon);

            Vector2 min = circle.center;
            Vector2 max = circle.center;
            min.x -= circle.radius;
            min.y -= circle.radius;
            max.x += circle.radius;
            max.y += circle.radius;
            polygon[0] = MathsUtils.TransformCoordinateToScreen(min, client, world);
            polygon[1] = MathsUtils.TransformCoordinateToScreen(max, client, world);

            if ( polygon[0].X>  polygon[1].X)
            {
                 int tmp = polygon[0].X;
                 polygon[0].X =  polygon[1].X;
                 polygon[1].X  = tmp;
            }
            if ( polygon[0].Y>  polygon[1].Y)
            {
                int tmp = polygon[0].Y;
                 polygon[0].Y =  polygon[1].Y;
                 polygon[1].Y  = tmp;
            }


            g.DrawEllipse(Pens.DarkGray, polygon[0].X, polygon[0].Y,  polygon[1].X -  polygon[0].X, polygon[1].Y- polygon[0].Y);

        }

    }



    public partial class RectangleRenderPanel : Panel
    {
        Vector2 O = new Vector2(0, 0);
        Vector2 c0 =  new Vector2(-1, -1);
        Vector2 c1 =  new Vector2(1, -1);
        Vector2 c2 =  new Vector2(1, 1);
        Vector2 c3 =  new Vector2(-1, 1);
        Point[] polygon;

        RectangleAA world;
        PreciseTimer timer;
        Random rnd;


        List<BodyShape> objects = new List<BodyShape>();


        public RectangleRenderPanel()
        {
            rnd = new Random();
            timer = new PreciseTimer();
            world = new RectangleAA(-5, -5, 20, 20);


            objects.Add(new BodyShape(new RectangleO(1, 1, 1.5f, 0.5f, MathsUtils.DegreeToRadian(10)), Vector2.One* 1));
            objects.Add(new BodyShape(new RectangleO(10, 2, 2.0f, 1.0f, MathsUtils.DegreeToRadian(80)),-Vector2.UnitX*2));

            polygon = new Point[5];
        }

        public void UpdatePhysic(double elapsed)
        {

        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            /*
            switch (keyData)
            {
                case Keys.Left: rectA.origin.x -= 0.1f; break;
                case Keys.Right: rectA.origin.x += 0.1f; break;
                case Keys.Up: rectA.origin.y += 0.1f; break;
                case Keys.Down: rectA.origin.y -= 0.1f; break;

                case Keys.PageUp: rectA.Angle = rectA.Angle + 0.1f; break;
                case Keys.PageDown: rectA.Angle = rectA.Angle - 0.1f; break;

                case Keys.Delete: rectA.halfsize.x *= 1.1f; break;
                case Keys.End: rectA.halfsize.x *= 0.9f; break;
            }
            */

            this.Invalidate();
            return base.ProcessCmdKey(ref msg, keyData);
        }



        protected override void OnPaintBackground(PaintEventArgs e)
        {
            // override with nothing
        }


        protected override void OnPaint(PaintEventArgs e)
        {
            System.Drawing.Graphics g = e.Graphics;

            Rectangle ClientOffsetRect = ClientRectangle;
            ClientOffsetRect.X += 10;
            ClientOffsetRect.Y += 10;
            ClientOffsetRect.Width -= 20;
            ClientOffsetRect.Height -= 20;
            Point p0 = MathsUtils.TransformCoordinateToScreen(Vector2.Zero, ClientOffsetRect, world);
            Point px = MathsUtils.TransformCoordinateToScreen(new Vector2(world.max.x, 0), ClientOffsetRect, world);
            Point py = MathsUtils.TransformCoordinateToScreen(new Vector2(0, world.max.y), ClientOffsetRect, world);
            g.Clear(BackColor);
            g.DrawLine(Pens.Red, p0, px);
            g.DrawLine(Pens.Green, p0, py);

            foreach (BodyShape obj in objects) obj.Draw(g, ClientOffsetRect, world);

            base.OnPaint(e);
        }

        protected override void OnResize(EventArgs eventargs)
        {
            base.OnResize(eventargs);
            this.Invalidate();
        }

    }
}
