﻿using System;
using System.IO;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using Engine.Maths;
using Engine.Tools;
using Engine.Graphics;
using Engine.Inputs;
using Engine.Partitions;
using Engine.Geometry;
using Engine.Collections;

namespace Test
{
    public struct MyStruct
    {
        public static int count = 0;
        public int value;
        public MyStruct(int index)
        {
            value = index;
        }
        public override string ToString()
        {
            return value.ToString();
        }
    }

    public class TestForm : Form
    {
        Engine.Test.RectangleRenderPanel rectangleRenderPanel1;

        GameLoop gameloop;

        public TestForm()
        {
            InitializeComponent();


            Matrix4 m = new Matrix4(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16);
            Matrix4 n = new Matrix4(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16);

            m.Multiply(n);

            Console.WriteLine(m);



            SArray<MyStruct> array = new SArray<MyStruct>(10);

            array.Add(new MyStruct(0));
            array.Add(new MyStruct(1));
            array.Add(new MyStruct(2));
            array.Add(new MyStruct(3));

            IntPtr ptr = array.GetPointer();

            unsafe
            {
                MyStruct* myptr = (MyStruct*)ptr.ToPointer();
                myptr[2].value = 99;
            }

            array.ReleasePointer();



            this.KeyPreview = true;
            this.rectangleRenderPanel1.Focus();
            this.rectangleRenderPanel1.Select();

            gameloop = new GameLoop(Update, 10.0, Render, 30);
            gameloop.StartLooping();
        }

        double maxdt = 0.0;

        private void Update(double elapsedtime)
        {
            Console.SetCursorPosition(0, 0);
            Console.WriteLine(gameloop);

            rectangleRenderPanel1.UpdatePhysic(elapsedtime);
        }

        private void Render(double alpha)
        {
            rectangleRenderPanel1.Invalidate();
        }


        private void InitializeComponent()
        {
            this.rectangleRenderPanel1 = new Engine.Test.RectangleRenderPanel();
            this.SuspendLayout();
            // 
            // rectangleRenderPanel1
            // 
            this.rectangleRenderPanel1.Location = new System.Drawing.Point(0, 0);
            this.rectangleRenderPanel1.Name = "rectangleRenderPanel1";
            this.rectangleRenderPanel1.Size = new System.Drawing.Size(700, 700);
            this.rectangleRenderPanel1.TabIndex = 1;
            // 
            // TestForm
            // 
            this.ClientSize = new System.Drawing.Size(792, 769);
            this.Controls.Add(this.rectangleRenderPanel1);
            this.Name = "TestForm";
            this.ResumeLayout(false);

        }

        /// <summary>
        /// Punto di ingresso principale dell'applicazione.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new TestForm { ResizeRedraw = true });
        }
    }

}
