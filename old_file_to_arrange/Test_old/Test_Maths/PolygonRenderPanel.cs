﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using Engine.Maths;
using Engine.Tools;
using Engine.Graphics;
using Engine.Geometry;

namespace Engine.Test
{
    public partial class PolygonRenderPanel : Panel
    {
        Point[] polygon;
        List<Vector2> vertices;
        List<Face16> indices;
        RectangleAA area;
        PreciseTimer timer;
        Random rnd;

        public PolygonRenderPanel()
        {
            rnd = new Random();
            timer = new PreciseTimer();
            area = new RectangleAA(0, 0, 10, 10);
            vertices = new List<Vector2>();
            indices = new List<Face16>();

            Polygon1();
            MakeTriangulation();
        }


        protected override void OnPaintBackground(PaintEventArgs e)
        {
            // override with nothing
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            System.Drawing.Graphics g = e.Graphics;

            g.Clear(BackColor);

            if (vertices.Count > 0)
            {
                int count = 8;
                while (count < vertices.Count + 1) count *= 2;

                if (count > polygon.Length) polygon = new Point[count];

                int i = 0;

                for (; i < vertices.Count; i++) polygon[i] = MathsUtils.TransformCoordinateToScreen(vertices[i], ClientRectangle, area);
                polygon[i++] = polygon[0];
                for (; i < polygon.Length; i++) polygon[i] = Point.Empty;

                if (indices.Count > 0)
                {
                    Point[] triangle = new Point[4];

                    foreach (Face16 f in indices)
                    {
                        triangle[0] = polygon[f.I];
                        triangle[1] = polygon[f.J];
                        triangle[2] = polygon[f.K];
                        triangle[3] = triangle[0];
                        g.FillPolygon(new SolidBrush(Color.FromArgb(200, rnd.Next(125, 255), rnd.Next(125, 255), rnd.Next(125, 255))), triangle);
                        
                    }

                }
                for (i = 0; i < vertices.Count; i++)
                    g.DrawLine(Pens.Black, polygon[i], polygon[i + 1]);

                for ( i = 0; i < vertices.Count; i++)
                    g.DrawString("P" + i.ToString(), SystemFonts.CaptionFont, Brushes.Black, polygon[i]);
            }
            base.OnPaint(e);
        }

        protected override void OnResize(EventArgs eventargs)
        {
            base.OnResize(eventargs);
            this.Invalidate();
        }

        protected override void OnMouseClick(MouseEventArgs e)
        {
            Console.Clear();
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                vertices.Add(MathsUtils.TransformScreenToCoordinate(e.Location, ClientRectangle, area));

                if (vertices.Count>2)
                indices = PolygonEarCuter.GetPolygon(vertices);
                foreach (Face16 f in indices)
                    Console.WriteLine(f.ToString());
            }
            else
            {
                vertices.Clear();
                indices.Clear();
            }
            this.Invalidate();
        }


        void MakeTriangulation()
        {
            long start = timer.TicksAccumulated;
            indices = PolygonEarCuter.GetPolygon(vertices);
            long end = timer.TicksAccumulated;

            Console.WriteLine(string.Format("done in {0}", end - start));

        }

        void Polygon1()
        {
            Vector2[] v = new Vector2[8];
            v[0] = new Vector2(2, 3);
            v[1] = new Vector2(3, 7);
            v[2] = new Vector2(5, 5);
            v[3] = new Vector2(8, 6);
            v[4] = new Vector2(6, 3);
            v[5] = new Vector2(5, 1);
            v[6] = new Vector2(4, 5);
            v[7] = new Vector2(3, 1);
            this.vertices = new List<Vector2>(v);
            polygon = new Point[8];
        }

        void Polygon2()
        {
            Vector2[] v = new Vector2[20];
            v[0] = new Vector2(1.757143, 5.75);
            v[1] = new Vector2(4.042857, 8.85);
            v[2] = new Vector2(6.357143, 4.9);
            v[3] = new Vector2(4.8, 1.2);
            v[4] = new Vector2(4.685714, 4.15);
            v[5] = new Vector2(3.585714, 6.625);
            v[6] = new Vector2(4.057143, 1.375);
            v[7] = new Vector2(1.728571, 2.075);
            v[8] = new Vector2(0.5857143, 5.3);
            v[9] = new Vector2(0.5714286, 8.025);
            v[10] = new Vector2(2, 9.025);
            v[11] = new Vector2(2.8, 9.05);
            v[12] = new Vector2(1.714286, 6.8);
            v[13] = new Vector2(1.328571, 5.725);
            v[14] = new Vector2(1.528571, 4.85);
            v[15] = new Vector2(1.985714, 2.875);
            v[16] = new Vector2(3.042857, 2.65);
            v[17] = new Vector2(3.085714, 3.975);
            v[18] = new Vector2(3.085714, 5.3);
            v[19] = new Vector2(2.054054, 4.50805);
            this.vertices = new List<Vector2>(v);
            polygon = new Point[20];
        }
    }
}
