﻿namespace Engine.Test
{
    partial class RenderForm
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Liberare le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.renderctrl = new Engine.Forms.RenderControl();
            this.SuspendLayout();
            // 
            // renderControlLeft
            // 
            this.renderctrl.BackColor = System.Drawing.Color.Red;
            this.renderctrl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.renderctrl.Location = new System.Drawing.Point(0, 0);
            this.renderctrl.Name = "renderControlLeft";
            this.renderctrl.Size = new System.Drawing.Size(437, 354);
            this.renderctrl.TabIndex = 5;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(437, 354);
            this.Controls.Add(this.renderctrl);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        public Forms.RenderControl renderctrl;

    }
}

