﻿using System;
using System.Text;
using System.Windows.Forms;

using Engine.Graphics;
using Engine.Maths;
using Engine.Tools;
using Engine.Forms;
using System.Collections.Generic;

namespace Engine.Test
{
    public class Programm
    {
        Axis axis;
        MeshObj mesh0;
        EffectLight effect;

        public RenderForm form0;
        public List<RenderForm> forms;



        /// <summary>
        /// Punto di ingresso principale dell'applicazione.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Programm programm = new Programm();
            programm.InitForm0();
            programm.forms = new List<RenderForm>();

            for (int i = 0; i < 4; i++)
            {
                RenderForm form = new RenderForm(programm);
                form.renderctrl.InitGraphic(programm.form0.renderctrl.render);
                form.renderctrl.DrawFunction = programm.DrawFunction;
                form.Show();
                programm.forms.Add(form);
            }

            Application.Run(programm.form0);

        }

        void InitResources(Device device)
        {
            axis = new Axis(device, 10.0f);
            Engine.Content.EngineResources.ContentFolder = @"C:\Users\john\PROJECTS\3dEngine\3dengineSharpDx\Engine\Resources\Content\";
            effect = new EffectLight(device);
            effect.LightDirection.Value = new Vector3(1, 1, 1);
            mesh0 = new MeshObj(device);
        }


        void InitForm0()
        {
            form0 = new RenderForm(this);
            form0.renderctrl.InitGraphic(null);       
            InitResources(form0.renderctrl.render.Device);
            form0.renderctrl.DrawFunction = DrawFunction;
        }

        void DrawFunction(RenderControl sender, ICamera camera)
        {
            System.Threading.Thread.Sleep(1);
            RenderWindow render = sender.render;

            if (render.Device.CanDraw())
            {
                render.BeginDraw();

                axis.Draw(camera, effect);

                mesh0.Draw(camera.Projection, camera.View, Matrix4.Identity, effect);
                mesh0.Draw(camera.Projection, camera.View, Matrix4.Translating(1, 0, 0), effect);

                render.EndDraw();
                render.Present();
            }

        }

    }
}
