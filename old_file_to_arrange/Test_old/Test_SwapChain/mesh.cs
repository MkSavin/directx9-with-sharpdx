﻿using System;
using System.IO;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using Engine.Maths;
using Engine.Tools;
using Engine.Graphics;
using Engine.Inputs;

using Font = Engine.Graphics.Font;
using Engine.Geometry;
using Engine;

namespace Engine.Test
{
    public class MeshObj
    {
        Device device;
        IndexBuffer indexbuffer;

        // each buffer require a VertexInfo declaration to know its structure
        VertexBuffer vertexbuffer;
        VertexLayout vertexdecl;
        // each buffer require a VertexInfo declaration to know its structure
        VertexBuffer colorbuffer;
        VertexLayout colordecl;
        
        // the render require a VertexDeclaration, can be sum of more vertexinfo
        VertexLayout shaderdecl;
        VertexDeclaration shaderVertexDecl;

        public Matrix4 world = Matrix4.Scaling(1, 2, 3);
        Matrix4 worldInvTrasp = Matrix4.Traspose(Matrix4.Inverse(Matrix4.Scaling(1, 2, 3)));


       public Vector3[] Vertices, Normals;

        
        
        public MeshObj(Device device)
        {
            this.device = device;

            VertexElement[] vertexElements = new VertexElement[]
            {
                new VertexElement(0,0,DeclarationType.Float3,DeclarationMethod.Default,DeclarationUsage.Position,0),
                new VertexElement(0,12,DeclarationType.Float3,DeclarationMethod.Default,DeclarationUsage.Normal,0)
                
            };
            VertexElement[] colorElements = new VertexElement[]
            {
                new VertexElement(0,0,DeclarationType.Color,DeclarationMethod.Default,DeclarationUsage.Color,0)
            };

            vertexdecl = new VertexLayout(vertexElements);
            colordecl = new VertexLayout(colorElements);
            
            // make a sum of 2 stream
            shaderdecl = new VertexLayout(vertexElements, colorElements);

            shaderVertexDecl = new VertexDeclaration(device, shaderdecl.Elements);


            MeshListGeometry mesh = BaseTriGeometry.GeoSphere(1);

            mesh.CalculateNormal();

            vertexbuffer = new VertexBuffer(device, vertexdecl, BufferUsage.Managed, mesh.numVertices);
            colorbuffer = new VertexBuffer(device, colordecl, BufferUsage.Managed, mesh.numVertices);
            indexbuffer = new IndexBuffer(device, IndexLayout.Face16, BufferUsage.Managed, mesh.numPrimitives);

            VertexStream stream = vertexbuffer.OpenStream();
            // this is possible becase i use DeclarationUsage semantic to match buffer position
            stream.WriteCollection<Vector3>(mesh.vertices, 0, mesh.vertices.Count, 0);
            stream.WriteCollection<Vector3>(mesh.normals, 0, mesh.normals.Count, 0);
            stream.Close();
            vertexbuffer.Count = mesh.vertices.Count;


            stream = colorbuffer.OpenStream();
            Color32[] colors = new Color32[mesh.vertices.Count];
            for (int i = 0; i < mesh.vertices.Count; i++) colors[i] = Color32.Cyano;
            stream.WriteCollection<Color32>(colors, colorElements[0], 0, mesh.vertices.Count, 0);
            stream.Close();
            colorbuffer.Count = mesh.vertices.Count;

            IndexStream istream = indexbuffer.OpenStream();
            istream.WriteCollection<Face16>(mesh.indices, 0, 0);
            istream.Close();
            indexbuffer.Count = mesh.indices.Count;

            Vertices = new Vector3[ mesh.vertices.Count];
            Normals = new Vector3[mesh.normals.Count];

            mesh.vertices.data.CopyTo(Vertices, 0);
            mesh.normals.data.CopyTo(Normals, 0);
        }



        public void Draw(Matrix4 projection, Matrix4 view , Matrix4 world, EffectLight effect)
        {
            this.world = world;

            if (vertexbuffer != null)
            {
                effect.Technique = effect.TechDiffuseLighting;

                effect.WorldViewProj.Value = projection * view * world;
                effect.WorldInvTraspose.Value = Matrix4.Traspose(Matrix4.Inverse(world));


                device.SetVertexDeclaration(shaderVertexDecl);
                device.SetVertexStream(vertexbuffer, 0);
                device.SetVertexStream(colorbuffer, 1);
                device.SetIndexStream(indexbuffer);


                foreach (Pass pass in effect.TechDiffuseLightingPerPixel)
                {
                    device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, vertexbuffer.Count, 0, indexbuffer.Count);
                }
            }
        }
    }
}
