﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

using Engine.Graphics;
using Engine.Maths;
using Engine.Tools;
using Engine.Renderers;
using Engine.Forms;

namespace Engine.Test
{
    public partial class RenderForm : Form
    {
        Programm owner;

        public RenderForm(Programm owner)
        {
            this.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint, true);
            InitializeComponent();
            this.owner = owner;
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);
            this.renderctrl.Destroy();
        }
    }

}
