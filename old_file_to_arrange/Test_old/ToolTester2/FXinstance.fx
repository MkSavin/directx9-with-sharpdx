//-----------------------------------------------------------------------------
// Globals.
//-----------------------------------------------------------------------------
float4x4 xWorldViewProj : WORLDVIEWPROJ;


//-----------------------------------------------------------------------------
// Vertex Shaders.
//-----------------------------------------------------------------------------
struct VS_INPUT
{
    float4  Traslate : POSITION;
	float4  BoxSize  : TEXCOORD0;
	float4  BoxMin   : TEXCOORD1;
	float4  BoxColor : COLOR0;
};
struct VS_OUTPUT
{   
	float4  Position  : POSITION;
    float4  Color     : COLOR0;
};

VS_OUTPUT VShader(VS_INPUT input)
{
	VS_OUTPUT output = (VS_OUTPUT)0;
	output.Position = input.BoxMin + input.BoxSize * input.Traslate;
	output.Position.w = 1;
	output.Position = mul(output.Position, xWorldViewProj);
	output.Color = input.BoxColor;
	return output;
}


//-----------------------------------------------------------------------------
// Pixel Shaders.
//-----------------------------------------------------------------------------
float4 PShader(VS_OUTPUT Input) : COLOR0
{
	return Input.Color;
}
//-----------------------------------------------------------------------------
// Techniques.
//-----------------------------------------------------------------------------
technique InstancingTechique
{
    pass p0
    {
        VertexShader = compile vs_3_0 VShader();
		PixelShader =  compile ps_3_0 PShader();
    }
}