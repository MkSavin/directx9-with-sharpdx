﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

using Engine.Maths;
using Engine.Tools;
using Engine.Graphics;
using Engine.Forms;
using Engine.Partitions;

namespace ToolTester
{
    public partial class TestForm2 : Form
    {
        RenderWindow render;
        AxisObj axis;
        LineObj line;

        OctreeObj octreescene;
        OctTree octree;
        OctRayTraceEnumerator octreeraytracer;


        long memorySize;
        Stopwatch timer = new Stopwatch();

        float xangle = 0;
        float yangle = 0;
        float increment = 0.02f;
        Vector3 p0, dir;

        GameLoop gameloop;


        public TestForm2()
        {
            InitializeComponent();

            InitializeGraphics();
            InitTree();

            gameloop = new GameLoop(SceneDraw);
        }

        void InitializeGraphics()
        {
            this.renderPanel1.InitGraphics();
            this.renderPanel1.cameraController = new TrackBallCamera_new(this.renderPanel1, new Vector3(10, 10, 10), new Vector3(5, 0, 0), Vector3.UnitY, 0.1f, 1000.0f);
            this.renderPanel1.AssignCallbacks(Render, UpdateFunc);


            render = this.renderPanel1.mainTarghet;

            render.Device.renderstates.cullMode = Cull.None;
            render.Device.renderstates.fillMode = FillMode.Solid;

            axis = new AxisObj(render);
            line = new LineObj(render);
            octreescene = new OctreeObj(render);

            dir = new Vector3(1,1,1.5);
            dir.Normalize();

            p0 = new Vector3(-1, 5, -1);
        }


        void SceneDraw(double elapsed)
        {
            this.renderPanel1.Invalidate();
        }

        void UpdateFunc(float elapsed, ICamera camera)
        {
            //xangle += 0.01f;
            yangle += increment;
            Matrix4 rotation = Matrix4.RotationYawPitchRoll(0, yangle, xangle);         
            Vector3 rotdir = Vector3.TransformCoordinate(dir, rotation);

            line.Update(p0, p0 + rotdir * 100);

            if (octreeraytracer == null)
            {
                octreeraytracer = new OctRayTraceEnumerator(octree.Root, new Ray(p0, rotdir));
            }
            else
            {
                octreeraytracer.SetNewParameters(new Ray(p0, rotdir));
            }
            octreescene.SetInstance(octree, octreeraytracer);
        }


        void Render(RenderPanel panel, ICamera camera)
        {
            render.Device.renderstates.world = Matrix4.Identity;
            render.Device.renderstates.projection = camera.Projection;
            render.Device.renderstates.view = camera.View;

            octreescene.Draw(camera);
            render.Device.ClearShaderCode();
            axis.Draw(camera);
            line.Draw(camera);
        }

        void InitTree()
        {
            int depth = 6;

            memorySize = GC.GetTotalMemory(false);
            octree = new OctTree(depth);
            octree.Root = new OctNode(octree, new BoxAA2( 0, 0, 0, 10, 10, 10));
            octree.Root.RecursiveSplit();
            memorySize = GC.GetTotalMemory(false) - memorySize;

            OctNodesEnumerator collection = new OctNodesEnumerator(octree.Root);

            timer.Reset();
            timer.Start();
            int count = 0;
            foreach (OctNode node in collection) count++;
            timer.Stop();
            Console.WriteLine("+---------------------NOT-RECURSIVE FUNC PERFORMANCE 1-------");
            Console.WriteLine("| total quads counts : " + octree.NodeCount);
            Console.WriteLine(string.Format("| estimation : {0}B avarage : {1}B total : {2}MB", OctNode.AvarageSizeInByte, memorySize / octree.NodeCount, memorySize / 1024 / 1024));
            Console.WriteLine("| algorithm elapsed  : " + timer.ElapsedMilliseconds);
            Console.WriteLine("+---------------------------------------------------------");

            octreescene.SetInstance(octree, collection);
        }


    }







  
}
