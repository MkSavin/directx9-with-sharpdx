﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

using Engine.Renderers;
using Engine.Graphics;
using Engine.Maths;
using Engine.Tools;
using Engine.Partitions;



namespace ToolTester
{
    public class InstanceProgram : MaterialFX
    {
        public InstanceProgram(RenderWindow render)
            : base(render, ToolTester.Properties.Resources.FXinstance)
        {
            if (DX9Enumerations.VertexShaderVersion.Major < 3) throw new Exception("This graphic card don't support shader 3.0");
            effect.Parameters.Add("Technique", new EffectTechnique("InstancingTechique"));
            effect.Parameters.Add("Transform", new EffectParamMatrix(Matrix4.Identity, "xWorldViewProj"));
        }

        public Matrix4 ProjViewWorld
        {
            get { return (Matrix4)effect.Parameters["Transform"]; }
            set { effect.Parameters["Transform"] = value; }
        }
    }

    public class OctreeObj
    {
        RenderWindow render;
        List<BoxInstance> instances = new List<BoxInstance>();

        // common geometries value for all box
        BoxGeometry[] geometryV;
        ushort[] geometryI;

        InstanceProgram shader;
        VertexLayout geometrydef;
        VertexLayout instancedef;
        VertexDeclaration declaration;
        VertexBuffer geometryVertexBuffer;
        VertexBuffer instanceVertexBuffer;
        IndexBuffer indexBuffer;

        public OctreeObj(RenderWindow render)
        {
            this.render = render;
            this.shader = new InstanceProgram(render);

            // build static geometry of box instance
            geometryV = new BoxGeometry[8];
            geometryV[0] = new BoxGeometry(0, 0, 0);
            geometryV[1] = new BoxGeometry(1, 0, 0);
            geometryV[2] = new BoxGeometry(0, 1, 0);
            geometryV[3] = new BoxGeometry(1, 1, 0);
            geometryV[4] = new BoxGeometry(0, 0, 1);
            geometryV[5] = new BoxGeometry(1, 0, 1);
            geometryV[6] = new BoxGeometry(0, 1, 1);
            geometryV[7] = new BoxGeometry(1, 1, 1);

            geometryI = new ushort[]
            {
                0, 1, 1, 3, 3, 2, 2, 0, // front
                4, 5, 5, 7, 7, 6, 6, 4, // back
                0, 4, 2, 6, 3, 7, 1, 5  // connect
            };


            // define vertex declaration
            List<VertexElement> geometryElements = new List<VertexElement>();
            List<VertexElement> instanceElements = new List<VertexElement>();

            geometryElements.Add(new VertexElement(0, 0, DeclarationType.Float3, DeclarationMethod.Default, DeclarationUsage.Position, 0));
            instanceElements.Add(new VertexElement(1, 0, DeclarationType.Float3, DeclarationMethod.Default, DeclarationUsage.TexCoord, 0));
            instanceElements.Add(new VertexElement(1, 12, DeclarationType.Float3, DeclarationMethod.Default, DeclarationUsage.TexCoord, 1));
            instanceElements.Add(new VertexElement(1, 24, DeclarationType.Color, DeclarationMethod.Default, DeclarationUsage.Color, 0));

            geometrydef = new VertexLayout(geometryElements);
            instancedef = new VertexLayout(instanceElements);

            // sum the two stream and build the declaration
            List<VertexElement> allElements = new List<VertexElement>(geometryElements);
            allElements.AddRange(instanceElements);
            declaration = new VertexDeclaration(render, allElements);

            // create static geometry
            geometryVertexBuffer = new VertexBuffer(render, geometrydef, BufferUsage.Managed, geometryV.Length);
            VertexStream stream = geometryVertexBuffer.OpenStream();
            stream.WriteCollection<BoxGeometry>(geometryV, 0, geometryV.Length, 0);
            geometryVertexBuffer.CloseStream();
            geometryVertexBuffer.Count = geometryV.Length;

            indexBuffer = new IndexBuffer(render, IndexLayout.One16, BufferUsage.Managed, geometryI.Length);
            IndexStream istream = indexBuffer.OpenStream();
            istream.WriteCollection<ushort>(geometryI, 0, geometryI.Length, 0);
            indexBuffer.CloseStream();
            indexBuffer.Count = geometryI.Length;

        }

        public void SetInstance(OctTree octree, OctEnumerator collection)
        {
            // to reduce a little we can avoid draw the parent node that have 8 children because
            // the mesh's lines are overlapping

            instances.Clear();

            foreach (OctNode node in collection)
            {
                if (node.level == 0)
                    instances.Add(new BoxInstance(node.size.Min, node.size.Max, Color32.Red));
            }

            // if vertexbuffer is smaller, the only way is recreating 
            if (instanceVertexBuffer == null || instances.Count > instanceVertexBuffer.Count)
            {
                instanceVertexBuffer = new VertexBuffer(render, instancedef, BufferUsage.Managed, instances.Count);
            }

            if (instances.Count > 0)
            {
                VertexStream stream = instanceVertexBuffer.OpenStream(0, instances.Count);
                stream.WriteCollection<BoxInstance>(instances, 0, instances.Count, 0);
                instanceVertexBuffer.CloseStream();
                instanceVertexBuffer.Count = instances.Count;
            }
        }


        public void Draw(ICamera camera)
        {
            if (instances.Count > 0)
            {
                shader.ProjViewWorld = camera.Projection * camera.View;
                
                render.Device.SetStreamFrequency(instances.Count, StreamSource.IndexedData, 0);
                render.Device.SetStreamFrequency(1, StreamSource.InstanceData, 1);
                
                render.Device.SetVertexDeclaration(declaration);

                render.Device.SetIndexStream(indexBuffer);
               
                render.Device.SetVertexStream(geometryVertexBuffer, 0);
                render.Device.SetVertexStream(instanceVertexBuffer, 1);

                int numpass = shader.Begin();
                for (int pass = 0; pass < numpass; pass++)
                {
                    shader.BeginPass(pass);
                    render.Device.DrawIndexedPrimitives(PrimitiveType.LineList, 0, 0, geometryV.Length ,0, geometryI.Length/2);
                    shader.EndPass();
                }
                shader.End();

                render.Device.SetStreamFrequency(1, StreamSource.Reset, 0);
                render.Device.SetStreamFrequency(1, StreamSource.Reset, 1);
            }
        }




        /// <summary>
        /// The static geometry vertex
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 4)]
        public struct BoxGeometry
        {
            public Vector3 traslation;
            public BoxGeometry(float tx,float ty,float tz)
            {
                this.traslation = new Vector3(tx,ty,tz);
            }
        }
        /// <summary>
        /// The instance vertex
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 4)]
        public struct BoxInstance
        {
            public Vector3 size;
            public Vector3 min;
            public Color32 color;

            public BoxInstance(Vector3 min, Vector3 max,Color32 color)
            {
                this.min = min;
                this.size = max - min;
                this.color = color;
            }
        }
    }

}
