﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using Engine.Maths;
using Engine.Tools;
using Engine.Graphics;
using Engine.Forms;

namespace ToolTester
{
    public class AxisObj
    {
        RenderWindow render;
        VertexBuffer axis;
        VertexDeclaration cvertex_decl;

        public AxisObj(RenderWindow render)
        {
            this.render = render;
            this.cvertex_decl = new VertexDeclaration(render, CVERTEX.m_elements);

            CVERTEX[] verts = new CVERTEX[]
            {
                new CVERTEX(0,0,0, Color.Red),
                new CVERTEX(10,0,0, Color.Red),
                new CVERTEX(0,0,0, Color.Green),
                new CVERTEX(0,10,0, Color.Green),
                new CVERTEX(0,0,0, Color.Blue),
                new CVERTEX(0,0,10, Color.Blue)
            };
            axis = new VertexBuffer(render, cvertex_decl.Format, BufferUsage.Managed, verts.Length);
            VertexStream stream = axis.OpenStream();
            stream.WriteCollection<CVERTEX>(verts, 0, verts.Length, 0);
            axis.CloseStream();
            axis.Count = verts.Length;
        }

        public void Draw(ICamera camera)
        {
            render.Device.renderstates.projection = camera.Projection;
            render.Device.renderstates.view = camera.View;
            render.Device.renderstates.world = Matrix4.Identity;
            render.Device.SetVertexDeclaration(cvertex_decl);
            render.Device.SetVertexStream(axis, 0);
            render.Device.renderstates.lightEnable = false;
            render.Device.DrawPrimitives(PrimitiveType.LineList, 0, axis.Count / 2);
        }
    }

    public class LineObj
    {
        RenderWindow render;
        VertexBuffer axis;
        VertexDeclaration cvertex_decl;
        
        CVERTEX[] verts = new CVERTEX[]
        {
            new CVERTEX(0,0,0, Color.Black),
            new CVERTEX(10,5,9, Color.Red)
        };


        public LineObj(RenderWindow render)
        {
            this.render = render;
            this.cvertex_decl = new VertexDeclaration(render, CVERTEX.m_elements);

            axis = new VertexBuffer(render, cvertex_decl.Format, BufferUsage.Managed, verts.Length);
            VertexStream stream = axis.OpenStream();
            stream.WriteCollection<CVERTEX>(verts, 0, verts.Length, 0);
            axis.CloseStream();
            axis.Count = verts.Length;
        }

        public void Update(Vector3 p0, Vector3 p1)
        {
            verts[0].m_pos = p0;
            verts[1].m_pos = p1;

            VertexStream stream = axis.OpenStream();
            stream.WriteCollection<CVERTEX>(verts, 0, verts.Length, 0);
            axis.CloseStream();
            axis.Count = verts.Length;
        }

        public void Draw(ICamera camera)
        {
            render.Device.renderstates.projection = camera.Projection;
            render.Device.renderstates.view = camera.View;
            render.Device.renderstates.world = Matrix4.Identity;
            render.Device.SetVertexDeclaration(cvertex_decl);
            render.Device.SetVertexStream(axis, 0);
            render.Device.renderstates.lightEnable = false;
            render.Device.DrawPrimitives(PrimitiveType.LineList, 0, 1);
        }
    }

    
}
