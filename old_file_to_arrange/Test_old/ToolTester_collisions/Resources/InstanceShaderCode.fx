//-----------------------------------------------------------------------------
// Globals.
//-----------------------------------------------------------------------------
float4x4 xWorldViewProj : WORLDVIEWPROJ;


//-----------------------------------------------------------------------------
// Vertex Shaders.
//-----------------------------------------------------------------------------
struct VS_INPUT
{
    float4  Position : POSITION;
	float4  Offset   : TEXCOORD0;
	float4  Color    : COLOR0;
};

struct VS_OUTPUT
{   
	float4  Position : POSITION;
    float4  Color    : COLOR0;
};

VS_OUTPUT VShader(VS_INPUT input)
{
	VS_OUTPUT output = (VS_OUTPUT)0;
	output.Position = input.Position + input.Offset;
	output.Position.w = 1;
	output.Position = mul(output.Position, xWorldViewProj);
	output.Color = input.Color;
	
	return output;
}


//-----------------------------------------------------------------------------
// Pixel Shaders.
//-----------------------------------------------------------------------------
float4 PShader(VS_OUTPUT Input) : COLOR0
{
	return Input.Color;
}
float4 PShader_black(VS_OUTPUT Input) : COLOR0
{
	return float4(0,0,0,0);
}
//-----------------------------------------------------------------------------
// Techniques.
//-----------------------------------------------------------------------------
technique InstancingTechique
{
    pass p0
    {
        VertexShader = compile vs_3_0 VShader();
		PixelShader =  compile ps_3_0 PShader();
    }
}
technique InstancingTechique_Wireframe
{
    pass p0
    {
        VertexShader = compile vs_3_0 VShader();
		PixelShader =  compile ps_3_0 PShader();
		FillMode = Solid;
    }

	pass p1
    {
        VertexShader = compile vs_3_0 VShader();
		PixelShader =  compile ps_3_0 PShader_black();
		FillMode = Wireframe;
    }
}