﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Engine.Graphics;
using Engine.Tools;
using Engine.Maths;
using Engine.Inputs;
using System.IO;
using Engine.Partitions;

using Font = Engine.Graphics.Font;
using Engine;
using Engine.Geometry;

namespace ToolTester
{
    public partial class Form1 : Form
    {
        RenderWindow render;
        Font font;
        AxisObj axis;
        GameLoop gameloop;
        TrackBallCamera_new ballcamera;
        KeyboardDevice keyboard;

        BoxObj box0, box1;
        SphereObj sphere0,sphere1;
        PlaneObj plane0;
        TriangleObj tri0;
        WavefrontObj obj0;
        WavefrontObject objfile;
        //MeshObj mesh0;

        MeshObj movingobject;

        BoxAA worldsize;
        PreciseTimer timer = new PreciseTimer();
        OctreeObj octree0;

        SkyBox sky;
        HWInstancedGeometry InstanceMesh;
        
        EffectParamMatrix WVP = new EffectParamMatrix("xWorldViewProj");

        public Form1()
        {
            this.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint | ControlStyles.Opaque, true);
            InitializeComponent();
            InitializeGraphics();
        }

        void InitializeGraphics()
        {
            render = new RenderWindow(this);

            render.Device.renderstates.cullMode = Cull.CounterClockwise;
            render.Device.renderstates.fillMode = FillMode.Solid;

            font = new Font(render, Engine.Graphics.Font.FontName.Arial, 8);

            axis = new AxisObj(render,10);
            box0 = new BoxObj(render, Matrix4.Scaling(1, 1, 1) * Matrix4.Translating(1, 0, 1));
            box1 = new BoxObj(render, Matrix4.Scaling(2, 2, 2));
            sphere0 = new SphereObj(render, new Vector3(2.5, 0, 2.5), 0.5f);
            sphere1 = new SphereObj(render, new Vector3(5, 0, 0), 2);
            plane0 = new PlaneObj(render, Vector3.Zero, new Vector3(1,2,-1));
            tri0 = new TriangleObj(render, Vector3.UnitX*2, Vector3.UnitY*2, Vector3.UnitZ*2);
            worldsize = new BoxAA(new Vector3(20, 20, 20), new Vector3(-20, -20, -20));

            ballcamera = new TrackBallCamera_new(this, new Vector3(20,20,20), Vector3.Zero, Vector3.UnitY, 0.1f, 1000.0f);

            gameloop = new GameLoop(Render, 60);
            gameloop.StartLooping();

            keyboard = new KeyboardDevice(this);

            movingobject = box0;

            sky = new SkyBox(render, @"C:\Users\john\PROJECTS\3dEngine\3dengineSharpDx\Engine\Content\Skybox.dds");


            InstanceMesh =  InitInstanceBox();
        }

        private HWInstancedGeometry InitInstanceLine()
        {
            // Build Geometry buffers
            VertexLayout geometryDescription = new VertexLayout();
            geometryDescription.Add(0, DeclarationType.Float3, DeclarationUsage.Position);

            VertexBuffer geometryVB = new VertexBuffer(render, geometryDescription, BufferUsage.Managed, 2);
            VertexStream stream = geometryVB.OpenStream();
            stream.WriteAndIncrement(Vector3.Zero);
            stream.WriteAndIncrement(Vector3.UnitY);
            geometryVB.CloseStream();
            geometryVB.Count = 2;

            IndexBuffer geometryIB = new IndexBuffer(render, IndexLayout.Edge16, BufferUsage.Managed, 1);
            IndexStream istream = geometryIB.OpenStream();
            istream.WriteAndIncrement((ushort)0);
            istream.WriteAndIncrement((ushort)1);
            geometryIB.Count = 2;

            // Build Instance buffers
            int NumInstances = 1000;
            Vector3[] offset = new Vector3[NumInstances];
            Color32[] colors = new Color32[NumInstances];
            Random rnd = new Random();

            for (int i = 0; i < NumInstances; i++)
            {
                offset[i] = new Vector3(rnd.NextDouble(), rnd.NextDouble(), rnd.NextDouble());
                colors[i] = new Color32(offset[i].x, offset[i].y, offset[i].z, 1.0f);
                offset[i] = (2.0f * offset[i] - 1.0f) * 20.0f;
            }

            VertexLayout instanceDecription = new VertexLayout();
            instanceDecription.Add(1, DeclarationType.Color, DeclarationUsage.Color);
            instanceDecription.Add(1, DeclarationType.Float3, DeclarationUsage.TexCoord);


            VertexBuffer instanceVB = new VertexBuffer(render, instanceDecription, BufferUsage.Managed, NumInstances);
            stream = instanceVB.OpenStream();
            stream.WriteCollection<Color32>(colors, instanceDecription.Elements[0], 0, NumInstances, 0);
            stream.WriteCollection<Vector3>(offset, instanceDecription.Elements[1], 0, NumInstances, 0);
            instanceVB.CloseStream();
            instanceVB.Count = NumInstances;


            // sum the two stream declaration to build the main vertex declaration
            VertexDeclaration bufferDeclaration = new VertexDeclaration(render, geometryDescription, instanceDecription);

            // compile the relative shader code
            Effect InstanceEffect = new Effect(render, Properties.Resources.InstanceShaderCode);
            InstanceEffect.ParameterCollector.AddUsed(WVP);
            EffectTechnique InstanceTechnique = new EffectTechnique("InstancingTechique");
            InstanceEffect.TechniqueCollector.AddUsed(InstanceTechnique);
            InstanceEffect.Technique = InstanceTechnique;


            HWInstancedGeometry instance = new HWInstancedGeometry(render);
            instance.vertexdeclaration = bufferDeclaration;
            instance.indexbuffer = geometryIB;
            instance.geomtrybuffer = geometryVB;
            instance.instancebuffer = instanceVB;
            instance.shadercode = InstanceEffect;
            instance.NumGeometryVertices = 2;
            instance.NumGeometryPrimitives = 1;
            instance.NumInstances = NumInstances;
            instance.PrimitiveType = PrimitiveType.LineList;
            return instance;
        }

        private HWInstancedGeometry InitInstanceBox()
        {
            // Build Geometry buffers
            
            List<VertexElement> geometryElements = new List<VertexElement>();
            geometryElements.Add(new VertexElement(0, 0, DeclarationType.Float3, DeclarationMethod.Default, DeclarationUsage.Position, 0));
            VertexLayout geometryDescription = new VertexLayout(geometryElements);

            MeshListGeometry box = BaseTriGeometry.BoxClose(5, 5, 5);

            VertexBuffer geometryVB = new VertexBuffer(render, geometryDescription, BufferUsage.Managed, box.numVertices);
            VertexStream stream = geometryVB.OpenStream();
            stream.WriteCollection<Vector3>(box.vertices, 0, box.numVertices, 0);
            geometryVB.CloseStream();
            geometryVB.Count = box.numVertices;

            IndexBuffer geometryIB = new IndexBuffer(render, IndexLayout.Face16, BufferUsage.Managed, box.numPrimitives);
            IndexStream istream = geometryIB.OpenStream();
            istream.WriteCollection<Face16>(box.indices, 0, 0);
            geometryIB.Count = box.numPrimitives;

            // Build Instance buffers
            int NumInstances = 100;
            Vector3[] offset = new Vector3[NumInstances];
            Color32[] colors = new Color32[NumInstances];
            Random rnd = new Random();
            
            for (int i = 0; i < NumInstances; i++)
            {
                offset[i] = new Vector3(rnd.NextDouble(), rnd.NextDouble(), rnd.NextDouble());
                colors[i] = new Color32(offset[i].x, offset[i].y, offset[i].z, 1.0f);
                offset[i] = (2.0f * offset[i] - 1.0f) * 20.0f;
            }

            List<VertexElement> instanceElements = new List<VertexElement>();
            instanceElements.Add(new VertexElement(1, 0, DeclarationType.Color, DeclarationMethod.Default, DeclarationUsage.Color, 0));
            instanceElements.Add(new VertexElement(1, 4, DeclarationType.Float3, DeclarationMethod.Default, DeclarationUsage.TexCoord, 0));
            VertexLayout instanceDecription = new VertexLayout(instanceElements);


            VertexBuffer instanceVB = new VertexBuffer(render, instanceDecription, BufferUsage.Managed, NumInstances);
            stream = instanceVB.OpenStream();
            stream.WriteCollection<Color32>(colors, instanceElements[0], 0, NumInstances, 0);
            stream.WriteCollection<Vector3>(offset, instanceElements[1], 0, NumInstances, 0); 
            instanceVB.CloseStream();
            instanceVB.Count = NumInstances;


            // sum the two stream declaration to build the main vertex declaration
            List<VertexElement> allElements = new List<VertexElement>();
            allElements.AddRange(geometryElements);
            allElements.AddRange(instanceElements);
            VertexDeclaration bufferDeclaration = new VertexDeclaration(render, allElements);

            // compile the relative shader code
            Effect InstanceEffect = new Effect(render,Properties.Resources.InstanceShaderCode);
            InstanceEffect.ParameterCollector.AddUsed(WVP);
            EffectTechnique InstanceTechnique = new EffectTechnique("InstancingTechique");
            InstanceEffect.TechniqueCollector.AddUsed(InstanceTechnique);
            InstanceEffect.Technique = InstanceTechnique;

            HWInstancedGeometry instance = new HWInstancedGeometry(render);
            instance.vertexdeclaration = bufferDeclaration;
            instance.indexbuffer = geometryIB;
            instance.geomtrybuffer = geometryVB;
            instance.instancebuffer = instanceVB;
            instance.shadercode = InstanceEffect;
            instance.NumGeometryVertices = geometryVB.Count;
            instance.NumGeometryPrimitives = geometryIB.Count;
            instance.NumInstances = NumInstances;
            instance.PrimitiveType = PrimitiveType.TriangleList;


            return instance;
        }



        int count = 0;
        bool prevcollision = false;
        bool needupdate = false;

        void UpdateInput()
        {
            if (keyboard == null) return;

            keyboard.ReadStates();
            
            Vector3 move = Vector3.Zero;
            float delta = 0.2f;

            //if (keyboard.KeyPressed.Count != 0) Console.WriteLine("press : " + keyboard.KeyPressed[0]);

            if (keyboard.IsKeyDown(Key.A)) move.x += delta;
            if (keyboard.IsKeyDown(Key.D)) move.x -= delta;
            if (keyboard.IsKeyDown(Key.W)) move.z += delta;
            if (keyboard.IsKeyDown(Key.S)) move.z -= delta;
            if (keyboard.IsKeyDown(Key.Space)) move.y += delta;
            if (keyboard.IsKeyDown(Key.C)) move.y -= delta;

            needupdate = move != Vector3.Zero;

            if (movingobject != null)
                movingobject.Position = movingobject.Position + move;

            BoxAA aabb0 = box0.Box;
            BoxAA aabb1 = box1.Box;
            Sphere bs0 = sphere0.Sphere;
            Sphere bs1 = sphere1.Sphere;
            Triangle tri = tri0.Triangle;


            if (needupdate && objfile!=null)
            {
                double t0 = timer.SecondsAccumulated;
                Voxelizer voxel = new Voxelizer(worldsize, objfile.vertices, objfile.faces, 6, Matrix4.Translating(obj0.Position));
                double t1 = timer.SecondsAccumulated;
                if (octree0==null) octree0 = new OctreeObj(render);
                List<OctBoxNode> collection = new List<OctBoxNode>(new OctNodesEnumerator<OctBoxNode>(voxel.octree.root));
                octree0.SetInstance(voxel.octree, collection);
                double t2 = timer.SecondsAccumulated;

                toolStripStatusLabel1.Text = "COMPUTATIONS " + voxel.ComputationsCounter + " in " + (t1 - t0) + ", update vbuffer in " + (t2 - t1); 
            }


            bool collision = PrimitiveIntersections.IntersectAABBTriangle(aabb0.min, aabb0.max, tri.P0, tri.P1, tri.P2);
            //bool collision = PrimitiveIntersections.IntersectSphereTriangle(bs0.center, bs0.radius, tri.P0, tri.P1, tri.P2);
            //bool collision = PrimitiveIntersections.IntersectAABBPlane(aabb0.min, aabb0.max, plane0.Plane);
            //bool collision = PrimitiveIntersections.IntersectAABBAABB(aabb0.min, aabb0.max, aabb1.min, aabb1.max);
            //bool collision = PrimitiveIntersections.IntersectAABBSphere(aabb0.min, aabb0.max, bs0.center, bs0.radius);
            //bool collision = PrimitiveIntersections.IntersectSphereSphere(bs0.center, bs0.radius, bs1.center, bs1.radius);

            if (count == 0) prevcollision = !collision;

            if (collision && !prevcollision)
            {
                tri0.SetColor(Color32.Red);
                toolStripStatusLabel1.Text = "COLLISION";
                toolStripStatusLabel1.Invalidate();
            }
            if (!collision && prevcollision)
            {
                tri0.SetColor(Color32.Blue);
                toolStripStatusLabel1.Text = "NOT COLLISION";
                toolStripStatusLabel1.Invalidate();
            }
            prevcollision = collision;
            count++;
        }

        void Render(double elapsedTime)
        {
            if (render.CheckDeviceState() == DeviceState.OK)
            {
                UpdateInput();

                render.SetTarghet();

                render.Clear(Color.CornflowerBlue);
                render.BeginScene();
                ICamera values = ballcamera;


                render.Device.ClearShaderCode();
                render.Device.renderstates.lightEnable = false;
                render.Device.renderstates.cullMode = Cull.CounterClockwise;
                render.Device.renderstates.material = Material.Default;
                render.Device.renderstates.fillMode = FillMode.Solid;
                render.Device.renderstates.ZBufferWriteEnable = true;
                render.Device.renderstates.ZBufferEnable = true;


                movingobject.Draw(ballcamera);
                axis.Draw(ballcamera);

                render.Device.renderstates.material = new Material { diffuse = Color32.Black };
                render.Device.renderstates.lightEnable = true;
                render.Device.renderstates.fillMode = FillMode.WireFrame;
                movingobject.Draw(ballcamera);

                Matrix4 wvp = ballcamera.Projection * ballcamera.View;

                render.Device.renderstates.fillMode = FillMode.Solid;

                WVP.Value = wvp;

                InstanceMesh.Draw();

                wvp *= movingobject.transform;


                int i=0;
                foreach (Vector3 v in movingobject.mesh.vertices)
                {
                    Vector3 pos = Vector3.Project(v, ballcamera.Viewport, wvp);
                    
                    if (pos.z > 0 && pos.z < 1)
                        font.Draw(i.ToString(), (int)pos.x, (int)pos.y, Color32.Black);
                    
                    //Console.WriteLine(pos);
                    i++;
                }

                sky.DrawLast(ballcamera.View, ballcamera.Projection);

                render.EndScene();
                render.Present();

                this.Invalidate();
            }
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            //render.Destroy();
            keyboard.Dispose();
            keyboard = null;

        }
        protected override void OnResizeBegin(EventArgs e)
        {
            base.OnResizeBegin(e);
            gameloop.StartLooping();
        }
        protected override void OnResizeEnd(EventArgs e)
        {
            base.OnResizeEnd(e);
            render.Resize(new Viewport(ClientSize));
            gameloop.StartLooping();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileForm open = new OpenFileForm();
            open.Filters.Clear();
            open.Filters.Add(new OpenFileFilters("obj", "obj"));

            string filename = open.GetFilePatch();
            
            if (!string.IsNullOrEmpty(filename))
            {
                objfile = new WavefrontObject();
                objfile.OpenFile(filename);
                obj0 = new WavefrontObj(render, objfile);

            }
            
        }
    }
}
