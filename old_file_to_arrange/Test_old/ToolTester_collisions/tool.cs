﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using Engine.Maths;
using Engine.Tools;
using Engine.Graphics;
using Engine.Forms;
using System.Runtime.InteropServices;
using Engine.Renderers;
using Engine.Geometry;
using Engine.Partitions;
using Engine;


namespace ToolTester
{
    public class MeshObj
    {
        public RenderWindow render;
        public VertexBuffer vbuffer, cbuffer;
        public IndexBuffer ibuffer;
        public VertexDeclaration declaration;
        public Matrix4 transform;
        public MeshListGeometry mesh;

        public MeshObj(RenderWindow render)
        {
            this.render = render;
        }
        public void Initialize(Matrix4 transform, MeshListGeometry mesh)
        {
            Initialize(transform, mesh, new Color32(0, 0, 255, 125));
        }
        public void Initialize(Matrix4 transform, MeshListGeometry mesh, Color32 meshcolor)
        {
            this.transform = transform;
            this.mesh = mesh;

            // define vertex declaration
            List<VertexElement> positionElements = new List<VertexElement>();
            List<VertexElement> colorElements = new List<VertexElement>();

            positionElements.Add(new VertexElement(0, 0, DeclarationType.Float3, DeclarationMethod.Default, DeclarationUsage.Position, 0));
            colorElements.Add(new VertexElement(1, 0, DeclarationType.Color, DeclarationMethod.Default, DeclarationUsage.Color, 0));

            VertexLayout positiondef = new VertexLayout(positionElements);
            VertexLayout colordef = new VertexLayout(colorElements);

            // sum the two stream and build the declaration
            List<VertexElement> allElements = new List<VertexElement>(positionElements);
            allElements.AddRange(colorElements);
            declaration = new VertexDeclaration(render, allElements);


            vbuffer = new VertexBuffer(render, positiondef, BufferUsage.Managed, mesh.numVertices);
            VertexStream stream = vbuffer.OpenStream();
            stream.WriteCollection<Vector3>(mesh.vertices, 0, mesh.numVertices, 0);
            vbuffer.CloseStream();
            vbuffer.Count = mesh.numVertices;

            cbuffer = new VertexBuffer(render, colordef, BufferUsage.Managed, mesh.numVertices);
            SetColor(meshcolor);
            cbuffer.Count = vbuffer.Count;

            ibuffer = new IndexBuffer(render, IndexLayout.Face16, BufferUsage.Managed, mesh.numPrimitives);
            IndexStream istream = ibuffer.OpenStream();
            istream.WriteCollection<Face16>(mesh.indices, 0, 0);
            ibuffer.Count = mesh.numPrimitives;
        }

        public void SetColor(Color32 color)
        {
            Color32[] colors = new Color32[vbuffer.Count];
            for (int i = 0; i < vbuffer.Count; i++) colors[i] = color;

            VertexStream stream = cbuffer.OpenStream();
            stream.WriteCollection<Color32>(colors, 0, colors.Length, 0);
            cbuffer.CloseStream();
        }

        public void Draw(ICamera camera)
        {
            render.Device.renderstates.projection = camera.Projection;
            render.Device.renderstates.view = camera.View;
            render.Device.renderstates.world = transform;
            render.Device.SetVertexDeclaration(declaration);
            render.Device.SetVertexStream(vbuffer, 0);
            render.Device.SetVertexStream(cbuffer, 1);
            render.Device.SetIndexStream(ibuffer);
            render.Device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, vbuffer.Count, 0, ibuffer.Count);
        }

        public Vector3 Position
        {
            get { return transform.Position; }
            set { transform.Position = value; }
        }

    }

    public class BoxObj : MeshObj
    {
        public BoxAA Box
        {
            get { return new BoxAA(transform); }
        }

        public BoxObj(RenderWindow render, Matrix4 transform) : base(render)
        {
            base.Initialize(transform, BaseTriGeometry.BoxClose(1, 1, 1));
            base.transform = transform;
        }
    }

    public class SphereObj : MeshObj
    {
        float radius;

        public Sphere Sphere
        {
            get { return new Sphere(base.Position, radius); }
        }

        public SphereObj(RenderWindow render, Vector3 center, float radius): base(render)
        {
            this.radius = radius;
            base.Initialize(Matrix4.Identity, BaseTriGeometry.GlobeSphere(10, 10, radius));
            base.Position = center;
        }
    }

    public class PlaneObj : MeshObj
    {
        Vector3 direction;

        public Plane Plane
        {
            get { return new Plane(direction, Position); }
        }

        public PlaneObj(RenderWindow render, Vector3 point, Vector3 direction)
            : base(render)
        {
            this.direction = direction.Normal;
            base.Initialize(Matrix4.Scaling(10, 10, 10) * Matrix4.Orienting(this.direction), BaseTriGeometry.TessellatedPlane(-1, -1, 1, 1, 10, 10));
            base.Position = point;
        }
    }
    
    public class TriangleObj : MeshObj
    {
       public Vector3 p0, p1, p2;

       public Triangle Triangle
       {
           get { return new Triangle(p0, p1, p2); }
       }

        public TriangleObj(RenderWindow render, Vector3 p0,Vector3 p1,Vector3 p2)
            : base(render)
        {
            this.p0 = p0;
            this.p1 = p1;
            this.p2 = p2;
            base.Initialize(Matrix4.Identity, BaseTriGeometry.Triangle(p0, p1, p2));
        }
    }


    public class WavefrontObj : MeshObj
    {
        public readonly BoxAA size;

        public WavefrontObj(RenderWindow render, WavefrontObject obj)
            : base(render)
        {
            MeshListGeometry mesh = new MeshListGeometry();
            mesh.vertices = new VertexAttribute<Vector3>(DeclarationUsage.Position, obj.vertices);
            mesh.colors = new VertexAttribute<Color32>(DeclarationUsage.Color, mesh.vertices.Count, Color32.Green);
            mesh.indices = new IndexAttribute<Face16>(obj.faces);

            size = obj.bounding;

            base.Initialize(Matrix4.Identity, mesh);
        }
    }

    public abstract class LineObj
    {
        protected RenderWindow render;
        protected VertexBuffer vbuffer;
        protected VertexDeclaration cvertex_decl;
        
        public LineObj(RenderWindow render)
        {
            this.render = render;
        }


        protected void Initialize(CVERTEX[] vertices)
        {          
            this.cvertex_decl = new VertexDeclaration(render, CVERTEX.m_elements);
            vbuffer = new VertexBuffer(render, cvertex_decl.Format, BufferUsage.Managed, vertices.Length);
            VertexStream stream = vbuffer.OpenStream();
            stream.WriteCollection<CVERTEX>(vertices, 0, vertices.Length, 0);
            vbuffer.CloseStream();
            vbuffer.Count = vertices.Length;
        }

        public void Draw(ICamera camera)
        {
            render.Device.renderstates.projection = camera.Projection;
            render.Device.renderstates.view = camera.View;
            render.Device.renderstates.world = Matrix4.Identity;
            render.Device.SetVertexDeclaration(cvertex_decl);
            render.Device.SetVertexStream(vbuffer, 0);
            render.Device.DrawPrimitives(PrimitiveType.LineList, 0, vbuffer.Count / 2);
        }
    }

    public class AxisObj : LineObj
    {
        public AxisObj(RenderWindow render,float length):base(render)
        {
            CVERTEX[] verts = new CVERTEX[]
            {
                new CVERTEX(0,0,0, Color.Red),
                new CVERTEX(length,0,0, Color.Red),
                new CVERTEX(0,0,0, Color.Green),
                new CVERTEX(0,length,0, Color.Green),
                new CVERTEX(0,0,0, Color.Blue),
                new CVERTEX(0,0,length, Color.Blue)
            };

            base.Initialize(verts);
        }

    }

    public class SegmentObj : LineObj
    {
            CVERTEX[] verts = new CVERTEX[]
            {
                new CVERTEX(0,0,0, Color.Black),
                new CVERTEX(10,5,9, Color.Red)
            };

        public SegmentObj(RenderWindow render):base(render)
        {

            base.Initialize(verts);
        }


        public void Update(Vector3 p0, Vector3 p1)
        {
            verts[0].m_pos = p0;
            verts[1].m_pos = p1;

            VertexStream stream = vbuffer.OpenStream();
            stream.WriteCollection<CVERTEX>(verts, 0, verts.Length, 0);
            vbuffer.CloseStream();
            vbuffer.Count = verts.Length;
        }

    }


    public class OctreeObj
    {
        RenderWindow render;
        List<BoxInstance> instances = new List<BoxInstance>();

        // common geometries value for all box
        BoxGeometry[] geometryV;
        ushort[] geometryI;

        Effect shader;
        HWInstancedGeometry instanceclass;
        VertexLayout instancedef;

        EffectParamMatrix WVP = new EffectParamMatrix("xWorldViewProj");

        public OctreeObj(RenderWindow render)
        {
            this.render = render;

            // build static geometry of box instance
            geometryV = new BoxGeometry[8];
            geometryV[0] = new BoxGeometry(0, 0, 0);
            geometryV[1] = new BoxGeometry(1, 0, 0);
            geometryV[2] = new BoxGeometry(0, 1, 0);
            geometryV[3] = new BoxGeometry(1, 1, 0);
            geometryV[4] = new BoxGeometry(0, 0, 1);
            geometryV[5] = new BoxGeometry(1, 0, 1);
            geometryV[6] = new BoxGeometry(0, 1, 1);
            geometryV[7] = new BoxGeometry(1, 1, 1);

            geometryI = new ushort[]
            {
                0, 1, 1, 3, 3, 2, 2, 0, // front
                4, 5, 5, 7, 7, 6, 6, 4, // back
                0, 4, 2, 6, 3, 7, 1, 5  // connect
            };


            // define vertex declaration
            List<VertexElement> geometryElements = new List<VertexElement>();
            List<VertexElement> instanceElements = new List<VertexElement>();

            geometryElements.Add(new VertexElement(0, 0, DeclarationType.Float3, DeclarationMethod.Default, DeclarationUsage.Position, 0));
            instanceElements.Add(new VertexElement(1, 0, DeclarationType.Float3, DeclarationMethod.Default, DeclarationUsage.TexCoord, 0));
            instanceElements.Add(new VertexElement(1, 12, DeclarationType.Float3, DeclarationMethod.Default, DeclarationUsage.TexCoord, 1));
            instanceElements.Add(new VertexElement(1, 24, DeclarationType.Color, DeclarationMethod.Default, DeclarationUsage.Color, 0));

            VertexLayout geometrydef = new VertexLayout(geometryElements);
            instancedef = new VertexLayout(instanceElements);

            // sum the two stream and build the declaration
            List<VertexElement> allElements = new List<VertexElement>(geometryElements);
            allElements.AddRange(instanceElements);
            VertexDeclaration declaration = new VertexDeclaration(render, allElements);

            // create static geometry
            VertexBuffer geometryVertexBuffer = new VertexBuffer(render, geometrydef, BufferUsage.Managed, geometryV.Length);
            VertexStream stream = geometryVertexBuffer.OpenStream();
            stream.WriteCollection<BoxGeometry>(geometryV, 0, geometryV.Length, 0);
            geometryVertexBuffer.CloseStream();
            geometryVertexBuffer.Count = geometryV.Length;

            IndexBuffer  indexBuffer = new IndexBuffer(render, IndexLayout.One16, BufferUsage.Managed, geometryI.Length);
            IndexStream istream = indexBuffer.OpenStream();
            istream.WriteCollection<ushort>(geometryI, 0, geometryI.Length, 0);
            indexBuffer.CloseStream();
            indexBuffer.Count = geometryI.Length;


            // compile the relative shader code
            shader = new Effect(render, Properties.Resources.InstanceShaderCode);
            shader.ParameterCollector.AddUsed(WVP);
            EffectTechnique tech = new EffectTechnique("InstancingTechique_Wireframe");
            shader.TechniqueCollector.AddUsed(tech);
            shader.Technique = tech;


            instanceclass = new HWInstancedGeometry(render);
            instanceclass.vertexdeclaration = declaration;
            instanceclass.indexbuffer = indexBuffer;
            instanceclass.geomtrybuffer = geometryVertexBuffer;
            instanceclass.NumGeometryVertices = geometryVertexBuffer.Count;
            instanceclass.NumGeometryPrimitives = indexBuffer.Count / 2;
            instanceclass.PrimitiveType = PrimitiveType.LineList;
            instanceclass.shadercode = shader;

        }

        public void SetInstance(Octree<OctBoxNode> octree, IEnumerable<OctBoxNode> collection)
        {
            // to reduce a little we can avoid draw the parent node that have 8 children because
            // the mesh's lines are overlapping

            instances.Clear();

            foreach (OctBoxNode node in collection)
            {
                if (node.childused != OctIdx.All || node.level == octree.Depth - 1)
                    instances.Add(new BoxInstance(node.Min, node.Max, Color32.Red));
            }

            if (instanceclass.instancebuffer == null || instanceclass.instancebuffer.Capacity < instances.Count)
            {
                instanceclass.instancebuffer = new VertexBuffer(render, instancedef, BufferUsage.Managed, instances.Count);
            }

            if (instances.Count > 0)
            {
                VertexStream stream = instanceclass.instancebuffer.OpenStream();
                stream.WriteCollection<BoxInstance>(instances, 0, instances.Count, 0);
                instanceclass.instancebuffer.CloseStream();
                instanceclass.instancebuffer.Count = instances.Count;
                instanceclass.NumInstances = instances.Count;
            }
        }


        public void Draw(ICamera camera)
        {
            if (instances.Count > 0)
            {
                WVP.Value = camera.Projection * camera.View;
                instanceclass.Draw();        
            }
        }


        /// <summary>
        /// The static geometry vertex
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 4)]
        public struct BoxGeometry
        {
            public Vector3 traslation;
            public BoxGeometry(float tx, float ty, float tz)
            {
                this.traslation = new Vector3(tx, ty, tz);
            }
        }
        /// <summary>
        /// The instance vertex
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 4)]
        public struct BoxInstance
        {
            public Vector3 size;
            public Vector3 min;
            public Color32 color;

            public BoxInstance(Vector3 min, Vector3 max, Color32 color)
            {
                this.min = min;
                this.size = max - min;
                this.color = color;
            }
        }
    }
}
