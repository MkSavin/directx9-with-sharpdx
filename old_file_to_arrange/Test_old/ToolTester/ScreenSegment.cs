﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.Diagnostics;

using Engine.Maths;
using Engine.Tools;
using Engine.Partitions;

namespace ToolTester
{
    enum TestMode : int
    {
        NONE = -1,
        Segment = 0,
        Ray = 1,
        Line = 2,
        Rectangle = 3,
        Circle = 4
    }

    class ScreenSegment
    {
        // some offsets just to center quadtree on the control panel
        int L = 20; // left
        int R = 50; // right
        int T = 20; // top
        int B = 50; // bottom
        int W { get { return ctrl.ClientSize.Width; } }
        int H { get { return ctrl.ClientSize.Height; } }

        TestForm form;
        Control ctrl;
        bool mousing = false;
        Stopwatch timer;
        Pen[] pens;
        Quadtree<MyQuadNode> quadtree;
        public TestMode mode = TestMode.NONE;

        public Point P0, P1;

        public bool IsEmpty { get { return P0.IsEmpty || P1.IsEmpty; } }

        float minf(float a, float b) { return a < b ? a : b; }

        public ScreenSegment(TestForm form, Control ctrl, Quadtree<MyQuadNode> quadtree)
        {
            this.ctrl = ctrl;
            this.form = form;
            this.ctrl.MouseDown += new MouseEventHandler(this.panel1_MouseDown);
            this.ctrl.MouseUp += new MouseEventHandler(this.panel1_MouseUp);
            this.ctrl.MouseMove += new MouseEventHandler(this.panel1_MouseMove);
            this.ctrl.Paint += new PaintEventHandler(this.panel1_Paint);
            this.ctrl.Resize += new EventHandler(this.panel1_Resize);
            this.ctrl.Focus();
            timer = new Stopwatch();

            this.quadtree = quadtree;

            P0 = new Point(0, 0);
            P1 = new Point(10, 10);


            pens = new Pen[quadtree.Depth];
            for (int i = 0; i < pens.Length; i++)
            {
                Color color;
                switch (i)
                {
                    case 0: color = Color.Red; break;
                    case 1: color = Color.Green; break;
                    case 2: color = Color.Blue; break;
                    case 3: color = Color.Magenta; break;
                    default: color = Color.Black; break;

                }
                pens[i] = new Pen(color);
            }
        }

        #region math
        public static int MAX(int a, int b) { return a > b ? a : b; }
        public static int MIN(int a, int b) { return a < b ? a : b; }
        public static void SWAP(ref int a, ref int b) { int t = a; a = b; b = t; }
        public static int ABS(int a) { return a > 0 ? a : -a; }

        public void WorldToScreen(float x, float y, out int X, out int Y)
        {
            Vector2 min = ((MyQuadNode)quadtree.root).Min;
            Vector2 max = ((MyQuadNode)quadtree.root).Max;


            X = (int)((x - min.x) / (max.x - min.x) * (W - R - L) + L);
            //Y = (int)((y - min.y) / (max.y - min.y) * (H - B - T) + T);
            //swap y
            Y = (int)((y - min.y) / (max.y - min.y) * (T - H + B) + H - B);

            if (X < 0) X = 0;
            if (X >= W) X = W - 1;
            if (Y < 0) Y = 0;
            if (Y >= H) Y = H - 1;
        }
        public void ScreenToWorld(int X, int Y, out float x, out float y)
        {
            Vector2 min = ((MyQuadNode)quadtree.root).Min;
            Vector2 max = ((MyQuadNode)quadtree.root).Max;

            x = (X - L) * (max.x - min.x) / (W - R - L) + min.x;
            //y =  (Y - T) * (max.y - min.y) / (H - B - T) + min.y;
            y = (Y - H + B) * (max.y - min.y) / (T - H + B) + min.y;
        }


        public Point ProjectP1
        {
            get
            {
                float Dx = P1.X - P0.X;
                float Dy = P1.Y - P0.Y;
                float lenght = (float)Math.Sqrt(Dx * Dx + Dy * Dy);
                Dx /= lenght;
                Dy /= lenght;

                float Divx = 1.0f / Dx;
                float Divy = 1.0f / Dy;

                int W = Divx > 0 ? ctrl.ClientSize.Width : 0;
                int H = Divy > 0 ? ctrl.ClientSize.Height : 0;

                float tmin = minf((W - P0.X) * Divx, (H - P0.Y) * Divy);

                return new Point((int)(P0.X + tmin * Dx), (int)(P0.Y + tmin * Dy));
            }
        }
        public Point ProjectP0
        {
            get
            {
                float Dx = P0.X - P1.X;
                float Dy = P0.Y - P1.Y;
                float lenght = (float)Math.Sqrt(Dx * Dx + Dy * Dy);
                Dx /= lenght;
                Dy /= lenght;

                float Divx = 1.0f / Dx;
                float Divy = 1.0f / Dy;

                int W = Divx > 0 ? ctrl.ClientSize.Width : 0;
                int H = Divy > 0 ? ctrl.ClientSize.Height : 0;

                float tmin = minf((W - P1.X) * Divx, (H - P1.Y) * Divy);

                return new Point((int)(P1.X + tmin * Dx), (int)(P1.Y + tmin * Dy));
            }
        }

        public Ray2D ray
        {
            get
            {
                Vector2 p0, p1;
                ScreenToWorld(P0.X, P0.Y, out p0.x, out p0.y);
                ScreenToWorld(P1.X, P1.Y, out p1.x, out p1.y);
                return new Ray2D(p0, p1 - p0);
            }
        }
        public Segment2D seg
        {
            get
            {
                Vector2 p0, p1;
                ScreenToWorld(P0.X, P0.Y, out p0.x, out p0.y);
                ScreenToWorld(P1.X, P1.Y, out p1.x, out p1.y);
                return new Segment2D(p0, p1);
            }
        }
        public RectangleAA area
        {
            get { return new RectangleAA(this.seg); }
        }
        public Circle circle
        {
            get
            {
                Vector2 p0, p1;
                ScreenToWorld(P0.X, P0.Y, out p0.x, out p0.y);
                ScreenToWorld(P1.X, P1.Y, out p1.x, out p1.y);
                return new Circle(p0, Vector2.GetLength(p1 - p0));
            }
        }
        #endregion


        public void DrawCircle(PaintEventArgs e)
        {
            Circle c = this.circle;
            int p0x, p0y, p1x, p1y;
            WorldToScreen(c.center.x - c.radius, c.center.y - c.radius, out p0x, out p0y);
            WorldToScreen(c.center.x + c.radius, c.center.y + c.radius, out p1x, out p1y);
            e.Graphics.DrawEllipse(Pens.Black, MIN(p0x, p1x), MIN(p0y, p1y), ABS(p0x - p1x), ABS(p0y - p1y));
        }
        public void DrawSegment(PaintEventArgs e)
        {
            e.Graphics.DrawLine(Pens.Red, P0, P1);
        }
        public void DrawRay(PaintEventArgs e)
        {
            e.Graphics.DrawLine(Pens.Red, P0, ProjectP1);
        }
        public void DrawLine(PaintEventArgs e)
        {
            e.Graphics.DrawLine(Pens.Red, ProjectP0, ProjectP1);
        }
        public void DrawRectangle(PaintEventArgs e)
        {
            e.Graphics.DrawRectangle(Pens.Black, MIN(P0.X, P1.X), MIN(P0.Y, P1.Y), ABS(P0.X - P1.X), ABS(P0.Y - P1.Y));
        }


        void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            mousing = false;
            form.Invalidate();
            ctrl.Invalidate();
        }

        void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            float x, y;
            ScreenToWorld(e.X, e.Y, out x, out y);
            form.statusLabel1.Text = string.Format("{0,3};{1,3}      x: {2} y: {3}", e.X, e.Y, x, y);

            if (mousing)
            {
                P1 = new Point(e.X, e.Y);
                form.Invalidate();
                ctrl.Invalidate();
            }

        }

        void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            P0 = new Point(e.X, e.Y);
            mousing = true;
        }

        void panel1_Resize(object sender, EventArgs e)
        {
            form.Invalidate();
            ctrl.Invalidate();
        }

        void panel1_Paint(object sender, PaintEventArgs e)
        {
            QuadEnumerator<MyQuadNode> collection = null;

            switch (mode)
            {
                case TestMode.Segment:
                    this.DrawSegment(e);
                    collection = new QuadSegTraceEnumerator<MyQuadNode>((MyQuadNode)quadtree.root, this.seg);
                    break;
                case TestMode.Ray:
                    this.DrawRay(e);
                    collection = new QuadRayTraceEnumerator<MyQuadNode>((MyQuadNode)quadtree.root, this.ray);
                    break;
                case TestMode.Line:
                    this.DrawLine(e);
                    collection = new QuadLineTraceEnumerator<MyQuadNode>((MyQuadNode)quadtree.root, (Line2D)this.ray);
                    break;
                case TestMode.Rectangle:
                    this.DrawRectangle(e);
                    collection = new QuadRectangleEnumerator<MyQuadNode>((MyQuadNode)quadtree.root, this.area);
                    break;
                case TestMode.Circle:
                    this.DrawCircle(e);
                    Circle c = this.circle;
                    collection = new QuadCircleEnumerator<MyQuadNode>((MyQuadNode)quadtree.root, c);
                    form.richTextBox1.Text = c.ToString();
                    break;
                default:
                    collection = new QuadNodesEnumerator<MyQuadNode>((MyQuadNode)quadtree.root);
                    break;
            }

            // 2 MS FOR 1.000.000 INSTANCES !!! WHAT AT FUCK !!!
            timer.Reset();
            timer.Start();
            int count = 0;
            foreach (QuadNode node in collection) count++;
            timer.Stop();
            Console.WriteLine("* intersections test : " + count);
            Console.WriteLine("* algorithm elapsed  : " + timer.ElapsedMilliseconds + " ms");
            Console.WriteLine("* algorithm elapsed  : " + timer.ElapsedTicks + " ticks");
            Console.WriteLine("* stack count used   : " + collection.MaxStackSizeUsed);
            Console.WriteLine("");

            //collection.DebugCheck();



            foreach (MyQuadNode node in collection)
            {
                int level = node.level;

                //Console.WriteLine(node.ToString());

                // painting is too slow
                if (level >= quadtree.Depth - 3)
                {
                    int x0, y0, x1, y1;

                    Vector2 min = node.Min;
                    Vector2 max = node.Max;

                    WorldToScreen(min.x, min.y, out x0, out y0);
                    WorldToScreen(max.x, max.y, out x1, out y1);

                    if (x0 > x1) SWAP(ref x0, ref x1);
                    if (y0 > y1) SWAP(ref y0, ref y1);

                    if (node.childused != QuadIndex.None)
                    {
                        //e.Graphics.DrawRectangle(Pens.Gray, x0 + level * 2, y0 + level * 2, x1 - x0, y1 - y0);
                    }
                    else
                    {
                        e.Graphics.DrawRectangle(pens[level], x0, y0, x1 - x0, y1 - y0);
                        e.Graphics.DrawString(node.ToString(), SystemFonts.DefaultFont, pens[level].Brush, (x0 + x1) / 2.0f, (y0 + y1) / 2.0f);
                    }
                    if (level >= quadtree.Depth - 3)
                    {
                        //e.Graphics.DrawString(node.ToString(), SystemFonts.DefaultFont, pens[level].Brush, (x0 + x1) / 2.0f, (y0 + y1) / 2.0f);
                    }
                }
            }
        }



    }
}
