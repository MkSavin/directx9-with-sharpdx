﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.Diagnostics;

using Engine.Maths;
using Engine.Tools;
using Engine.Partitions;

namespace ToolTester
{
    public class MyQuadNode : QuadNode<MyQuadNode>
    {
        bool selected = false;

        public MyQuadNode()
            : base() {}

        public MyQuadNode(Quadtree<MyQuadNode> main)
            : base(main) {}

        public MyQuadNode(MyQuadNode parent, int index)
            : base(parent,index) {}
    }
}
