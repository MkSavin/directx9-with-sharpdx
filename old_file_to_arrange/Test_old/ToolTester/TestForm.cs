﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

using Engine.Tools;
using Engine.Maths;

using Engine.Partitions;

namespace ToolTester
{
    public partial class TestForm : Form
    {
        Quadtree<MyQuadNode> quadtree;
        QuadEnumerator<MyQuadNode> collection;
        
        ScreenSegment screenSegment;
        Stopwatch timer = new Stopwatch();
        long quadMemorySize = 0;


        public TestForm()
        {
            //this.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint | ControlStyles.Opaque, true);
            InitializeComponent();

            //MakeIntersectionTest();

            InitTree();

            screenSegment = new ScreenSegment(this, this.panel1, quadtree);
            screenSegment.mode = (TestMode)comboBox1.SelectedIndex;

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            screenSegment.mode = (TestMode)(((ComboBox)sender).SelectedIndex);
        }

        void MakeQuadTest()
        {
        }


        void MakeIntersectionTest()
        { 
            
            Vector2 min = Vector2.Zero;
            Vector2 max = new Vector2(1,1);
            Vector2 mid = (max + min) * 0.5f;
            Vector2 semisize = (max - min) * 0.5f;

            Vector2 center = Vector2.Zero;
            float radius = 0.5f;

            int count = 1000000;
            timer.Reset();
            timer.Start();
            for (int i=0;i<count;i++)
            PrimitiveIntersections.IntersectAABRCircle(min, max, center, radius);
            timer.Stop();
            Console.WriteLine("IntersectAABRCircle elapsed  : " + timer.ElapsedMilliseconds);

            timer.Reset();
            timer.Start();
            for (int i = 0; i < count; i++)
                PrimitiveIntersections.Intersect_AABR_Circle_v1(mid, semisize, center, radius);
            timer.Stop();
            Console.WriteLine("IntersectAABRCircle1 elapsed  : " + timer.ElapsedMilliseconds);


            timer.Reset();
            timer.Start();
            for (int i = 0; i < count; i++)
                PrimitiveIntersections.Intersect_AABR_Circle_v2(min, max, center, radius);
            timer.Stop();
            Console.WriteLine("IntersectAABRCircle2 elapsed  : " + timer.ElapsedMilliseconds);

        }

        void InitTree()
        {
            int depth = 3;

            PerformanceCounter ramCounter = new PerformanceCounter("Memory", "Available MBytes", true);

            int availableMB = Convert.ToInt32(ramCounter.NextValue());
            int nodescount = Quadtree<MyQuadNode>.MaximumNodes(depth);
            int bytespernode = QuadNode.AvarageSizeInByte;
            int requireMB = nodescount * bytespernode / 1024 / 1024;

            Debug.Assert(availableMB > requireMB, "ATTENTION , require " + requireMB + " to store quadtree");

            quadMemorySize = GC.GetTotalMemory(false);

            quadtree = new Quadtree<MyQuadNode>(depth, RectangleAA2.FromMinMax(Vector2.One * -10.0f, Vector2.Zero));
            quadtree.root = new MyQuadNode(quadtree);

            ((MyQuadNode)quadtree.root).RecursiveSplit();

            quadMemorySize = GC.GetTotalMemory(false) - quadMemorySize;

            collection = new QuadNodesEnumerator<MyQuadNode>((MyQuadNode)quadtree.root);

            timer.Reset();
            timer.Start();
            

            int count = 0;

            foreach (MyQuadNode node in collection)
            {
                count++;
            }

            Debug.Assert(count == Quadtree.MaximumNodes(quadtree.Depth), string.Format("ERROR count {0} different from calculated {1}", count, Quadtree.MaximumNodes(quadtree.Depth)));

            timer.Stop();
            Console.WriteLine("+---------------------NOT-RECURSIVE FUNC PERFORMANCE 1-------");
            Console.WriteLine("| total quads counts : " + quadtree.NodeCount);
            Console.WriteLine(string.Format("| estimation : {0}B avarage : {1}B total : {2}MB", QuadNode.AvarageSizeInByte, quadMemorySize / quadtree.NodeCount, quadMemorySize / 1024 / 1024));
            Console.WriteLine("| algorithm elapsed  : " + timer.ElapsedMilliseconds);
            Console.WriteLine("+---------------------------------------------------------");
        }
    }
}
