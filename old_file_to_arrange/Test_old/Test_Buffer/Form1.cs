﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using System.IO;

using Engine.Graphics;
using Engine.Tools;
using Engine.Maths;
using Engine.Partitions;

namespace Test_Buffer
{
    public partial class Form1 : Form
    {
        RenderWindow render;
        AxisLines axis;

        List<VoxelCell> voxels = new List<VoxelCell>();


        public Form1()
        {
            InitializeComponent();

            renderPanel.InitGraphics();
            renderPanel.cameraController = new TrackBallCamera_new(renderPanel, new Vector3(3, 3, 4), Vector3.Zero, Vector3.UnitY, 0.1f, 1000.0f);
            renderPanel.DrawFunction+= new Engine.Forms.RenderPanel.RenderPanelDrawCallback(Render);

            render = renderPanel.mainTarghet;

            axis = new AxisLines(render);

            VoxelOctree octree = new VoxelOctree(@"C:\Users\john\Desktop\bits.Bits");
            octree.InitTree();
        }


        void Render(Engine.Forms.RenderPanel panel, ICamera camera)
        {
            render.Device.renderstates.projection = camera.Projection;
            render.Device.renderstates.view = camera.View;
            render.Device.renderstates.cullMode = Cull.None;
            render.Device.renderstates.fillMode = FillMode.Solid;
            render.Device.renderstates.lightEnable = false;

            axis.Draw(camera);

            foreach (VoxelCell cell in voxels) cell.Draw(camera);
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            if (voxels.Count > 0)
            {
                foreach (VoxelCell cell in voxels)
                    cell.Destroy();
                voxels.Clear();
            }

            VoxelCase cellcase = (VoxelCase)((NumericUpDown)sender).Value;

            //voxels.Add(new VoxelCell(cellcase, render, 0, 0, 0));

            for (int x = -1; x < 2; x++)
                for (int y = -1; y < 2; y++)
                    for (int z = -1; z < 2; z++)
                    {
                        /*
                        VoxelCase neighboarcase = VoxelConst2d.NeighboarConverter(VoxelConst2d.FilterUsedByNeighboar(x, y, z, cellcase), x, y, z);

                        if (neighboarcase != VoxelCase.EMPTY && neighboarcase != VoxelCase.FULL)
                        {
                            voxels.Add(new VoxelCell(neighboarcase, render, x, y, z));
                        }
                         */
                    }
            renderPanel.Invalidate();

        }


        /// <summary>
        /// Punto di ingresso principale dell'applicazione.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Form1 form = new Form1();
            Application.Idle += new EventHandler(form.Application_Idle);
            Application.Run(form);
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            renderPanel.DrawFunction = null;
            render.Dispose();

            base.OnClosing(e);
        }

        #region GAME LOOP
        protected void Application_Idle(object sender, EventArgs e)
        {
            while (AppStillIdle)
            {
                System.Threading.Thread.Sleep(10);
                this.Invalidate();
            }
        }
        public bool AppStillIdle
        {
            get
            {
                PeekMsg msg;
                return !PeekMessage(out msg, IntPtr.Zero, 0, 0, 0);
            }
        }
        [System.Security.SuppressUnmanagedCodeSecurity]
        [DllImport("User32.dll", CharSet = CharSet.Auto)]
        private static extern bool PeekMessage(out PeekMsg msg, IntPtr hWnd,
                 uint messageFilterMin, uint messageFilterMax, uint flags);

        [StructLayout(LayoutKind.Sequential)]
        public struct PeekMsg
        {
            public IntPtr hWnd;
            public Message msg;
            public IntPtr wParam;
            public IntPtr lParam;
            public uint time;
            public System.Drawing.Point p;
        }
        #endregion
    }
}
