﻿using System;
using System.Collections.Generic;
using System.Text;

using Engine.Graphics;
using Engine.Maths;
using Engine.Geometry;
using Engine.Geometry.HalfEdge;
using Engine.Tools;

namespace Test_Buffer
{
    public abstract class MeshResource : IResource
    {
        protected RenderWindow render;
        protected VertexBuffer vbuffer;
        protected IndexBuffer ibuffer;
        protected VertexDeclaration declaration;

        public Device device
        {
            get { return render.Device; }
        }
        public bool Managed
        {
            get;
            private set;
        }
        public bool IsEnabled { get; set; }
        public bool IsDisposed { get; protected set; }

        public MeshResource(RenderWindow render, bool managed)
        {
            this.render = render;
            Managed = managed;
            if (!Managed) render.UnManagedResources.Add(this);
            IsEnabled = true;
            IsDisposed = false;
        }

        ~MeshResource()
        {
            Destroy();
        }

        public void DeviceRestore()
        {
            if (!Managed && IsEnabled) InitBuffers();
        }
        public void DeviceLost()
        {
            if (!Managed) Dispose();

        }
        public void Dispose()
        {
            if (vbuffer != null) vbuffer.Dispose();
            if (ibuffer != null) ibuffer.Dispose();
            if (declaration != null) declaration.Dispose();
            IsDisposed = true;
        }

        public void Destroy()
        {
            if (!Managed && render.UnManagedResources.Contains(this)) render.UnManagedResources.Remove(this);
            Dispose();
        }

        public abstract void InitBuffers();

        public abstract void Draw(ICamera camera);
    }

    public class AxisLines : MeshResource
    {
        public AxisLines(RenderWindow render)
            : base(render, true)
        {
            InitBuffers();
        }

        public override void InitBuffers()
        {
            declaration = new VertexDeclaration(render, CVERTEX.m_elements);

            CVERTEX[] vertices = new CVERTEX[]
            {
                new CVERTEX(1,0,0,Color32.Red),
                new CVERTEX(0,0,0,Color32.Red),
                new CVERTEX(0,1,0,Color32.Green),
                new CVERTEX(0,0,0,Color32.Green),
                new CVERTEX(0,0,1,Color32.Blue),
                new CVERTEX(0,0,0,Color32.Blue)
            };

            vbuffer = new VertexBuffer(render, declaration.Format, BufferUsage.Managed, vertices.Length);
            VertexStream vstream = vbuffer.OpenStream();
            vstream.WriteCollection<CVERTEX>(vertices,0,vertices.Length,0);
            vbuffer.CloseStream();

            IsEnabled = true;
            IsDisposed = false;
        }


        public override void Draw(ICamera camera)
        {
            device.SetVertexDeclaration(declaration);
            device.SetVertexStream(vbuffer, 0);
            device.DrawPrimitives(PrimitiveType.LineList, 0, 3);
        }
    }


    public class MeshObj : MeshResource
    {
        public MeshObj(RenderWindow render)
            : base(render, true)
        {
            InitBuffers();
        }

        public override void InitBuffers()
        {
            declaration = new VertexDeclaration(render, CVERTEX.m_elements);


            MeshListGeometry mesh =  Engine.Geometry.BaseTriGeometry.BoxClose(5, 5, 5);


            Vector3[] vertices = new Vector3[]{
                new Vector3(0,0,0),
                new Vector3(0,5,0),
                new Vector3(5,0,0),
                new Vector3(5,5,0)};

            Face16[] faces = new Face16[]{
                new Face16(0,1,2),
                new Face16(2,1,3)};


            Mesh3D hmesh = new Mesh3D(vertices, faces);

            Edge16[] edges = hmesh.GetEdgeList();


            mesh = hmesh.TriMesh;

            vbuffer = new VertexBuffer(render, declaration.Format, BufferUsage.Managed, mesh.numVertices);
            vbuffer.Count = mesh.numVertices;
            VertexStream vstream = vbuffer.OpenStream();
            mesh.WriteAttibutes(vstream);
            vbuffer.CloseStream();

            ibuffer = new IndexBuffer(render, IndexLayout.Face16, BufferUsage.Managed, mesh.numPrimitives);
             IndexStream istream = ibuffer.OpenStream();
             mesh.WriteAttributesIndex(istream);
            ibuffer.CloseStream();
            ibuffer.Count = mesh.numPrimitives;

            IsEnabled = true;
            IsDisposed = false;
        }


        public override void Draw(ICamera camera)
        {     
            device.SetVertexDeclaration(declaration);
            device.SetVertexStream(vbuffer, 0);
            device.SetIndexStream(ibuffer);

            device.renderstates.lightEnable = false;
            device.renderstates.fillMode = FillMode.Solid;
            device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, vbuffer.Count, 0, ibuffer.Count);

            device.renderstates.lightEnable = true;
            device.renderstates.fillMode = FillMode.WireFrame;
            device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, vbuffer.Count, 0, ibuffer.Count);
        }
    }


}
