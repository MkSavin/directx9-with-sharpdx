﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

using Engine.Graphics;
using Engine.Tools;
using Engine.Maths;
using Engine.Geometry;
using Engine.Partitions;

namespace Test_Buffer
{  

    public class VoxelCell : MeshResource
    {
        bool isNeighboar = false;
        public VoxelCase cellcase = VoxelCase.EMPTY;
        public int edgescase = 0;

        Matrix4 transform;
        
        Font font;

        public VoxelCell(VoxelCase cellcase, RenderWindow render, int xoffset, int yoffset, int zoffset)
            : base(render, true)
        {
            this.cellcase = cellcase;
            transform = Matrix4.Translating(xoffset * 2, yoffset * 2, zoffset * 2);
            isNeighboar = xoffset != 0 || yoffset != 0 || zoffset != 0;
            font = new Font(render, Font.FontName.Calibri, 12);
            
            
            InitBuffers();
        }

        
        public override void InitBuffers()
        {
            declaration = new VertexDeclaration(render, CVERTEX.m_elements);


            byte[] indices = VoxelConst2d.GetTriangles(cellcase);

            foreach (byte b in indices)
            {
                edgescase |= 1 << (b + 1);
            }


            Color32[] colors = new Color32[VoxelConst2d.edgevertex.Length];

            for (int i = 0; i < VoxelConst2d.edgevertex.Length; i++) colors[i] = isNeighboar ? Color32.Cyano : Color32.Blue;



            vbuffer = new VertexBuffer(render, declaration.Format, BufferUsage.Managed, VoxelConst2d.edgevertex.Length);
            vbuffer.Count = VoxelConst2d.edgevertex.Length;
            VertexStream vstream = vbuffer.OpenStream();
            vstream.WriteCollection<Vector3>(VoxelConst2d.edgevertex, CVERTEX.m_elements[0], 0, VoxelConst2d.edgevertex.Length, 0);
            vstream.WriteCollection<Color32>(colors, CVERTEX.m_elements[1], 0, VoxelConst2d.edgevertex.Length, 0);
            vbuffer.CloseStream();

            ibuffer = new IndexBuffer(render, IndexLayout.Face16, BufferUsage.Managed, indices.Length / 3);
            IndexStream istream = ibuffer.OpenStream();
            istream.WriteCollection<byte>(indices, IndexLayout.One8, 0, indices.Length, 0, 0);
            ibuffer.CloseStream();
            ibuffer.Count = indices.Length / 3;

            IsEnabled = true;
            IsDisposed = false;
        }


        public override void Draw(ICamera camera)
        {
            if (IsDisposed) return;

            device.SetVertexDeclaration(declaration);
            device.SetVertexStream(vbuffer, 0);
            device.SetIndexStream(ibuffer);

            device.renderstates.cullMode = Cull.CounterClockwise;
            device.renderstates.world = transform;

            device.renderstates.lightEnable = false;
            device.renderstates.fillMode = FillMode.Solid;
            device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, vbuffer.Count, 0, ibuffer.Count);

            device.renderstates.cullMode = Cull.None;

            device.renderstates.lightEnable = true;
            device.renderstates.fillMode = FillMode.WireFrame;
            device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, vbuffer.Count, 0, ibuffer.Count);



            if (!isNeighboar)
            {
                for (int i = 0; i < 8; i++)
                {
                    VoxelCase mask = (VoxelCase)(1 << i);

                    if ((cellcase & mask) != 0)
                    {
                        Vector3 point = Vector3.Project(VoxelConst2d.cubecorner[i], camera);
                        font.Draw("X", (int)point.x, (int)point.y, Color32.Black);
                    }
                }
                for (int i = 0; i < VoxelConst2d.edgevertex.Length; i++)
                {
                    if ((edgescase & (1 << (i + 1))) != 0)
                    {
                        Vector3 point = Vector3.Project(VoxelConst2d.edgevertex[i], camera);
                        font.Draw("e" + i.ToString(), (int)point.x, (int)point.y, Color32.Red);
                    }
                }
            }
        }
    }

    public class VoxelBox : MeshResource
    {
        private Font font;

        public VoxelBox(RenderWindow render)
            : base(render, true)
        {

            font = new Font(render, Font.FontName.Arial, 8);
            InitBuffers();

        }


        public override void InitBuffers()
        {
            declaration = new VertexDeclaration(render, CVERTEX.m_elements);

            ushort[] indices = VoxelConst2d.cubeedges;

            Color32[] colors = new Color32[VoxelConst2d.cubecorner.Length];
            for (int i = 0; i < VoxelConst2d.cubecorner.Length; i++) colors[i] = Color32.Green;

            vbuffer = new VertexBuffer(render, declaration.Format, BufferUsage.Managed, VoxelConst2d.cubecorner.Length);
            vbuffer.Count = VoxelConst2d.cubecorner.Length;
            VertexStream vstream = vbuffer.OpenStream();
            vstream.WriteCollection<Vector3>(VoxelConst2d.cubecorner, CVERTEX.m_elements[0], 0, VoxelConst2d.cubecorner.Length, 0);
            vstream.WriteCollection<Color32>(colors, CVERTEX.m_elements[1], 0, VoxelConst2d.cubecorner.Length, 0);
            vbuffer.CloseStream();

            ibuffer = new IndexBuffer(render, IndexLayout.Edge16, BufferUsage.Managed, indices.Length / 2);
            IndexStream istream = ibuffer.OpenStream();
            istream.WriteCollection<ushort>(indices, IndexLayout.One16, 0, indices.Length, 0, 0);
            ibuffer.CloseStream();
            ibuffer.Count = indices.Length / 2;

            IsEnabled = true;
            IsDisposed = false;
        }


        public override void Draw(ICamera camera)
        {
            if (IsDisposed) return;

            device.SetVertexDeclaration(declaration);
            device.SetVertexStream(vbuffer, 0);
            device.SetIndexStream(ibuffer);

            device.renderstates.lightEnable = false;
            device.DrawIndexedPrimitives(PrimitiveType.LineList, 0, 0, vbuffer.Count, 0, ibuffer.Count);

            for (int i = 0; i < VoxelConst2d.cubecorner.Length; i++)
            {
                Vector3 point = Vector3.Project(VoxelConst2d.cubecorner[i], camera);
                font.Draw(i.ToString(), (int)point.x, (int)point.y, Color32.Red);
            }

        }
    }
}
