﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

using Engine.Graphics;
using Engine.Tools;
using Engine.Maths;
using Engine.Renderers;

using Font = Engine.Graphics.Font;

namespace Engine.Test
{
    public partial class Form1 : Form
    {
        RenderWindow render;
        Font font;

        MeshObj mesh;
        BillboardObj billboards;
        //InstanceObj instances;
        AxisObj axis;

        bool draw = true;
        int drawcall = 0;
        
        TrackBallCamera_new ballcamera;

        Vector3 eye, look;
        float yangle = MathsUtils.DegreeToRadian(135);
        float l;
        Point start = new Point(0, 0);
        Point end = new Point(0, 0);

        /// <summary>
        /// Punto di ingresso principale dell'applicazione.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Form1 form = new Form1();
            Application.Idle += new EventHandler(form.Application_Idle);
            Application.Run(form);
        }

        public Form1()
        {
            this.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint | ControlStyles.Opaque, true);
            InitializeComponent();
            InitializeGraphics();

            Console.WriteLine(Marshal.SizeOf(typeof(Quaternion)));
        }

        void InitializeGraphics()
        {
            render = new RenderWindow(this);

            font = new Font(render, Engine.Graphics.Font.FontName.Arial, 12);


            //Light sun = Light.Sun;
            //sun.Position = new Vector3(-100, 100, -100);
            //sun.Direction = Vector3.GetNormal(-sun.Position);
           // device.lights.Add(sun);


            ////////////////////// mesh
            billboards = new BillboardObj(render);
            //instances = new InstanceObj(device, 100);
            axis = new AxisObj(render);
            mesh = new MeshObj(render);

            eye = new Vector3(-20, 20, -20);
            l = eye.Length;
            look = Vector3.Zero;

            ballcamera = new TrackBallCamera_new(this, eye, look,Vector3.UnitY, 0.1f, 1000.0f);

        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            render.Destroy();
        }
        protected override void OnResizeBegin(EventArgs e)
        {
            base.OnResizeBegin(e);
            draw = false;
        }
        protected override void OnResizeEnd(EventArgs e)
        {
            base.OnResizeEnd(e);
            draw = true;
            render.Resize(new Viewport(ClientSize));
            this.Invalidate();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            if (!draw) return;
            unchecked { drawcall++; }

            if (render.CheckDeviceState() == DeviceState.OK)
            {
                render.SetTarghet();
                
                render.Clear(Color.CornflowerBlue);
                render.BeginScene();
                ICamera values = ballcamera;

                axis.SetCamera(values);
                axis.SetToDevice();
                axis.Draw();

                billboards.SetCamera(values);
                billboards.SetToDevice();
                billboards.Draw();

                mesh.SetCamera(values);
                mesh.SetToDevice();
                mesh.Draw();

                //instances.SetCamera(camera);
                //instances.SetToDevice();
                //instances.Draw();

                /*
                if (billboards != null)
                {
                    for (int i = 0; i < billboards.instance.Length; i++)
                    {
                        Vector3 basepos = billboards.instance[i].traslation;
                        Vector3 s = Vector3.Project(basepos, values.Viewport, values.Projection, values.View, values.World);
                        font.Draw(i.ToString(), (int)s.x, (int)s.y, Color.Black);
                    }
                }
                */
                render.EndScene();
                render.Present();
            }
        }

        #region GAME LOOP
        protected void Application_Idle(object sender, EventArgs e)
        {
            while (AppStillIdle)
            {
                System.Threading.Thread.Sleep(10);
                this.Invalidate();
            }
        }
        public bool AppStillIdle
        {
            get
            {
                PeekMsg msg;
                return !PeekMessage(out msg, IntPtr.Zero, 0, 0, 0);
            }
        }
        [System.Security.SuppressUnmanagedCodeSecurity]
        [DllImport("User32.dll", CharSet = CharSet.Auto)]
        private static extern bool PeekMessage(out PeekMsg msg, IntPtr hWnd,
                 uint messageFilterMin, uint messageFilterMax, uint flags);

        [StructLayout(LayoutKind.Sequential)]
        public struct PeekMsg
        {
            public IntPtr hWnd;
            public Message msg;
            public IntPtr wParam;
            public IntPtr lParam;
            public uint time;
            public System.Drawing.Point p;
        }
        #endregion
    }
}
