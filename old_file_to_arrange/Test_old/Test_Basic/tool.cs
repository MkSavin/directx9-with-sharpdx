﻿using System;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Collections.Generic;
using System.Text;

using Engine.Graphics;
using Engine.Tools;
using Engine.Maths;
using Engine.Renderers;
using Engine.Geometry;


namespace Engine.Test
{
    public class MaterialFX_BILLBOARD : Effect
    {
        Techniques current;
        public enum Techniques
        {
            ExperimentBillboardTechique,

            CylinderBillboard,
            ParallelBillboard,
            SphericBillboard,

            CylinderBillboardWithTexture,
            ParallelBillboardWithTexture,
            SphericBillboardWithTexture,

            ParallelBillboardWithWireframeTexture
        }

        public MaterialFX_BILLBOARD(RenderWindow render)
            : base(render, Engine.Test.Properties.Resources.FxBillboard)
        {
            if (DX9Enumerations.VertexShaderVersion.Major < 1) throw new Exception("This graphic card don't support shader 3.0");

            base.Parameters.Add("Transform", new EffectParamMatrix("xWorldViewProj"));
            base.Parameters.Add("WorldTransform", new EffectParamMatrix("xWorld"));
            base.Parameters.Add("ViewTransform", new EffectParamMatrix("xView"));
            base.Parameters.Add("ProjTransform", new EffectParamMatrix("xProj"));
            base.Parameters.Add("CameraPos", new EffectParamVector3("xEye"));
            base.Parameters.Add("CameraTarghet", new EffectParamVector3("xTarghet"));
            base.Parameters.Add("WindXZ", new EffectParamVector2(new Vector2(1, 1), "xWind"));
            base.Parameters.Add("AllowedDir", new EffectParamVector3(Vector3.UnitY, "xAllowedRotDir"));
            base.Parameters.Add("Texture", new EffectParamTexture(null, "xBillboardTexture"));
            base.Technique = new EffectTechnique("CylBillboardTechique");
            Method = Techniques.ParallelBillboard;
        }

        public Techniques Method
        {
            get { return current; }
            set
            {
                string tech = "CylBillboardTechique";
                switch (value)
                {
                    case Techniques.ExperimentBillboardTechique: tech = "ExperimentBillboardTechique"; break;

                    case Techniques.CylinderBillboard: tech = "CylBillboardTechique"; break;
                    case Techniques.ParallelBillboard: tech = "ParBillboardTechique"; break;
                    case Techniques.SphericBillboard: tech = "SphBillboardTechique"; break;

                    case Techniques.CylinderBillboardWithTexture: tech = "CylBillboardTextureTechique"; break;
                    case Techniques.ParallelBillboardWithTexture: tech = "ParBillboardTextureTechique"; break;
                    case Techniques.SphericBillboardWithTexture: tech = "SphBillboardTextureTechique"; break;

                    case Techniques.ParallelBillboardWithWireframeTexture: tech = "ParBillboardTextureWireTechique"; break;
                    default: throw new ArgumentException();
                }
                base.Technique = new EffectTechnique(tech);
                current = value;
            }
        }
        
        public Matrix4 Proj
        {
            get { return (Matrix4)effect.Parameters["ProjTransform"]; }
            set { effect.Parameters["ProjTransform"] = (Matrix4)value; }
        }
         public Matrix4 World
        {
            get { return (Matrix4)effect.Parameters["WorldTransform"]; }
            set { effect.Parameters["WorldTransform"] = (Matrix4)value; }
        }
        
        public Matrix4 View
        {
            get { return (Matrix4)effect.Parameters["ViewTransform"]; }
            set { effect.Parameters["ViewTransform"] = (Matrix4)value; }
        }

        public Matrix4 ProjViewWorld
        {
            get { return (Matrix4)effect.Parameters["Transform"]; }
            set { effect.Parameters["Transform"] = (Matrix4)value; }
        }
        public Vector3 Eye
        {
            get { return (Vector3)effect.Parameters["CameraPos"]; }
            set { effect.Parameters["CameraPos"] = (Vector3)value; }
        }
        public Vector3 Targhet
        {
            get { return (Vector3)effect.Parameters["CameraTarghet"]; }
            set { effect.Parameters["CameraTarghet"] = (Vector3)value; }
        }
        public Vector3 AllowedDir
        {
            get { return (Vector3)effect.Parameters["AllowedDir"]; }
            set { effect.Parameters["AllowedDir"] = (Vector3)value; }
        }
        public Texture2D DiffuseMap
        {
            get { return (Texture2D)effect.Parameters["Texture"]; }
            set { effect.Parameters["Texture"] = (Texture2D)value; }
        }
        public Vector2 WindXZ
        {
            get { return (Vector2)effect.Parameters["WindXZ"]; }
            set { effect.Parameters["WindXZ"] = (Vector2)value; }
        }
    }

    /// <summary>
    /// Require POSITION and TEXCOORD(float3) for traslation semantic in vertex buffer
    /// </summary>
    public class MaterialFX_INSTANCING : Effect
    {
        public MaterialFX_INSTANCING(RenderWindow render)
            : base(render, Engine.Test.Properties.Resources.FxInstancing)
        {
            if (DX9Enumerations.VertexShaderVersion.Major < 3) throw new Exception("This graphic card don't support shader 3.0");

            base.Parameters.Add("Diffuse", new EffectParamColor((Color32)Color.Blue, "material.ambient"));
            base.Parameters.Add("Wireframe", new EffectParamColor((Color32)Color.Black, "material.diffuse"));
            base.Parameters.Add("Transform", new EffectParamMatrix(Matrix4.Identity, "WorldViewProj"));
            base.Technique = new EffectTechnique("InstancingTechique");
            SolidWireframe = false;
        }

        public Color32 WireframeColor
        {
            get { return (Color32)base.Parameters["Wireframe"]; }
            set { base.Parameters["Wireframe"] = value; }
        }
        public Color32 Diffuse
        {
            get { return (Color32)base.Parameters["Diffuse"]; }
            set { base.Parameters["Diffuse"] = value; }
        }
        public bool SolidWireframe
        {
            get { return base.Technique.Name == "WireframeTechique2"; }
            set {  base.Technique = new EffectTechnique( value ? "WireframeTechique2" : "InstancingTechique"); }
        }
        public Matrix4 ProjViewWorld
        {
            get { return (Matrix4)base.Parameters["Transform"]; }
            set { base.Parameters["Transform"] = value; }
        }
    }
    




    public class BillboardObj : IDrawable
    {
        public BillBoardVertex[] geometry;
        public BillBoardInstance[] instance;

        MaterialFX_BILLBOARD.Techniques method = MaterialFX_BILLBOARD.Techniques.ExperimentBillboardTechique;
        Random rnd = new Random();
        Texture2D diffusemap;
        bool INSTANCING = true;
        bool INDEXED = true;

        /// <summary>
        /// the billboard vertex definition
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 4)]
        public struct BillBoardVertex
        {
            public Vector2 texcoord;
            public Vector2 scalar;

            public BillBoardVertex(float scalarx, float scalary, float texx, float texy)
            {
                scalar = new Vector2(scalarx, scalary);
                texcoord = new Vector2(texx, texy);         
            }
            public override string ToString()
            {
                return texcoord.ToString() + " ; " + scalar.ToString();
            }
        }
        /// <summary>
        /// the billboard vertex definition
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 4)]
        public struct BillBoardInstance
        {
            public Vector3 traslation;
            public Color32 color;
            public Vector2 windf;

            public BillBoardInstance(float x, float y, float z, Color32 color, float windx, float windy)
            {
                this.traslation = new Vector3(x, y, z);
                this.color = color;
                this.windf = new Vector2(windx, windy);
            }
            public override string ToString()
            {
                return traslation.ToString() + " ; " + color.ToString();
            }
        }


        MaterialFX_BILLBOARD material;

        int numinstances = 0;
        VertexLayout d_geometry, d_instance;
        VertexDeclaration instance_decl;
        RenderWindow render;
        VertexBuffer gVB, iVB;
        IndexBuffer iB;

        Vector2 Wind = new Vector2(1,1);
        /// <summary>
        /// </summary>
        /// <param name="iIndex">instance index</param>
        /// <param name="gIndex">geometry's vertex index</param>
        /// <returns></returns>
        public Vector3 encode(int iIndex, int gIndex)
        {
            switch (method)
            {
                case MaterialFX_BILLBOARD.Techniques.CylinderBillboard:
                case MaterialFX_BILLBOARD.Techniques.CylinderBillboardWithTexture:
                    return Vector3.Zero;
                case MaterialFX_BILLBOARD.Techniques.ParallelBillboard:
                case MaterialFX_BILLBOARD.Techniques.ParallelBillboardWithTexture:
                case MaterialFX_BILLBOARD.Techniques.ParallelBillboardWithWireframeTexture:
                    return Vector3.Zero;
                case MaterialFX_BILLBOARD.Techniques.SphericBillboard:
                case MaterialFX_BILLBOARD.Techniques.SphericBillboardWithTexture:
                    return Vector3.Zero;
            }
            return Vector3.Zero;
        }



        public BillboardObj(RenderWindow render)
        {
            //     0-------1
            //     | \     |         vup
            //     |  \    |         |
            //     |   \   |         |___ vright
            //     |    \  |
            //     |     \ |
            //     3---T---2
            //
            //  p0 = T + s0.x * vup + s0.y * vright

            this.render = render;

            if (INDEXED)
            {
                geometry = new BillBoardVertex[]{
                    new BillBoardVertex( 1,-0.5f, 0,0),
                    new BillBoardVertex( 1, 0.5f, 1,0),
                    new BillBoardVertex( 0, 0.5f, 1,1),
                    new BillBoardVertex( 0,-0.5f, 0,1)};
            }
            else
            {
                geometry = new BillBoardVertex[]{
                    new BillBoardVertex( 0.5f,-0.5f, 0,0),
                    new BillBoardVertex( 0.5f, 0.5f, 1,0),
                    new BillBoardVertex(-0.5f, 0.5f, 1,1),

                    new BillBoardVertex( 0.5f,-0.5f, 0,0),
                    new BillBoardVertex(-0.5f, 0.5f, 1,1),
                    new BillBoardVertex(-0.5f,-0.5f, 0,1)};
            }


            if (INSTANCING)
            {
                List<BillBoardInstance> list = new List<BillBoardInstance>();

                int n = 10;
                int m = 0;
                for (int x = -n; x <= n; x += 1)
                    for (int y = -m; y <= m; y += 1)
                        for (int z = -n; z <= n; z += 1)
                            list.Add(new BillBoardInstance(x * rnd.Next(50, 100) / 100.0f, y, z * rnd.Next(50, 100) / 100.0f, Color.White, (float)rnd.NextDouble() * 2 - 1, (float)rnd.NextDouble() * 2 - 1));

                Console.WriteLine("Num Billboard : " + list.Count);

                /*
                list.Clear();
                for (int i = 0; i < 1000; i++)
                    list.Add(new BillBoardInstance(
                        ((float)rnd.NextDouble() - 0.5f) * 8,
                        (float)rnd.NextDouble(),
                        ((float)rnd.NextDouble() - 0.5f) * 8,
                        Color.White));
                */


                instance = list.ToArray();

                numinstances = instance.Length;
                for (int i = 0; i < numinstances; i++)
                {
                    float f = (float)i / numinstances;
                    instance[i].color = new Color32(f, 1 - f, f, 1);
                }
            }
            else
            {
                instance = new BillBoardInstance[6];
                instance[0].color = Color.Red;
                instance[1].color = Color.Green;
                instance[2].color = Color.Blue;

                instance[3].color = Color.Red;
                instance[4].color = Color.Blue;
                instance[5].color = Color.White;
            }

            List<VertexElement> e_geometry = new List<VertexElement>();
            List<VertexElement> e_traslation = new List<VertexElement>();

            int c = 4;
            int f2 = 8;
            int f3 = 12;
            e_geometry.Add(new VertexElement(0, 0, DeclarationType.Float2, DeclarationMethod.Default, DeclarationUsage.TexCoord, 0));
            e_geometry.Add(new VertexElement(0,f2, DeclarationType.Float2, DeclarationMethod.Default, DeclarationUsage.TexCoord, 1));

            e_traslation.Add(new VertexElement(1,   0, DeclarationType.Float3, DeclarationMethod.Default, DeclarationUsage.Position, 0));
            e_traslation.Add(new VertexElement(1,  f3, DeclarationType.Color, DeclarationMethod.Default, DeclarationUsage.Color, 0));
            e_traslation.Add(new VertexElement(1,f3+c, DeclarationType.Float2, DeclarationMethod.Default, DeclarationUsage.TexCoord, 2));


            d_geometry = new VertexLayout(e_geometry);
            d_instance = new VertexLayout(e_traslation);


            List<VertexElement> e_global = new List<VertexElement>(e_geometry);
            e_global.AddRange(e_traslation);
            instance_decl = new VertexDeclaration(render, e_global);

            gVB = new VertexBuffer(render, d_geometry, BufferUsage.Managed, geometry.Length);
            iVB = new VertexBuffer(render, d_instance, BufferUsage.Managed, instance.Length);
            //gVB = new VertexBuffer(typeof(BillBoardVertex), d_geometry, geometry.Length, false);
            //iVB = new VertexBuffer(typeof(VERTEX), d_instance, positions.Length, false);

            VertexStream stream = gVB.OpenStream();
            stream.WriteCollection<BillBoardVertex>(geometry, 0, geometry.Length, 0);
            gVB.CloseStream();
            gVB.Count = geometry.Length;

            stream = iVB.OpenStream();
            stream.WriteCollection<BillBoardInstance>(instance, 0, instance.Length, 0);
            iVB.CloseStream();
            iVB.Count = instance.Length;


            if (INDEXED)
            {
                iB = new IndexBuffer(render, IndexLayout.One16, BufferUsage.Managed, 6);
                IndexStream istream = iB.OpenStream();
                istream.WriteCollection<ushort>(new ushort[] { 0, 1, 2, 0, 2, 3 }, 0, 6, 0);
                iB.CloseStream();
                iB.Count = 6;
            }
            else
            {
                iB = new IndexBuffer(render, IndexLayout.One16, BufferUsage.Managed, 6);
                IndexStream istream = iB.OpenStream();
                istream.WriteCollection<ushort>(new ushort[] { 0, 1, 2, 3, 4, 5 }, 0, 6, 0);
                iB.CloseStream();
                iB.Count = 6;
            }


            this.diffusemap = Texture2D.FromFilename(render, TextureUsage.Managed, @"..\..\treealpha.png");

            this.material = new MaterialFX_BILLBOARD(render);
            this.material.Method = method;
            this.material.AllowedDir = Vector3.UnitY;
            this.material.DiffuseMap = diffusemap;
        }


        float angle = 0;

        public void Draw()
        {

            material.WindXZ = new Vector2(
                (Math.Sin(angle) * 0.01 + Math.Cos(angle) * 0.005),
                (Math.Cos(angle) * 0.01 + Math.Sin(angle) * 0.005));
            angle += 0.01f;


            render.Device.renderstates.fillMode = FillMode.Solid;
            render.Device.renderstates.cullMode = Cull.None;
            // if alpha >= 200 write to depth value, else skip
            render.Device.renderstates.ZBufferWriteEnable = true;
            render.Device.renderstates.AlphaTestEnable = true;
            render.Device.renderstates.AlphaFunction = Compare.GreaterEqual;
            render.Device.renderstates.AlphaRef = 200;


            int numpass = material.Begin();

            numpass = 1;

            for (int pass = 0; pass < numpass; pass++)
            {
                material.BeginPass(pass);

                if (INDEXED)
                    render.Device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, 6, 0, 2);
                else
                    render.Device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, 4, 0, 2);
                    //device.DrawPrimitives(PrimitiveType.TriangleList, 0, 2);

                material.EndPass();
            }
            material.End();

            if (INSTANCING)
            {
                render.Device.SetStreamFrequency(1, StreamSource.Reset, 0);
                render.Device.SetStreamFrequency(1, StreamSource.Reset, 1);
            }
            render.Device.renderstates.ZBufferEnable = true;
            render.Device.renderstates.ZBufferWriteEnable = true;

            render.Device.renderstates.AlphaBlendEnable = false;
            render.Device.renderstates.AlphaTestEnable = false; 
        }

        public void Render(MaterialBase material)
        {
            throw new NotImplementedException();
        }

        public void SetToDevice()
        {
            render.Device.SetVertexDeclaration(instance_decl);

            if (INSTANCING)
            {
                if (INDEXED)
                {
                    render.Device.SetStreamFrequency(numinstances, StreamSource.IndexedData, 0);
                    render.Device.SetStreamFrequency(1, StreamSource.InstanceData, 1);

                }
                else
                {
                    render.Device.SetStreamFrequency(numinstances, StreamSource.IndexedData, 0);
                    render.Device.SetStreamFrequency(1, StreamSource.InstanceData, 1);
                    //gVB.SetFrequency(0, StreamSource.None);
                    //iVB.SetFrequency(6, StreamSource.None);
                }
            }
            // if (INDEXED)
            render.Device.SetIndexStream(iB);
            render.Device.SetVertexStream(gVB, 0);
            render.Device.SetVertexStream(iVB, 1);
        }
        public void SetCamera(ICamera camera)
        {
            material.ProjViewWorld = camera.Projection * camera.View;
            material.Eye = camera.Eye;
            material.Targhet = camera.Targhet;
            material.World = Matrix4.Identity;
            material.View = camera.View;
            material.Proj = camera.Projection;
        }

        public Matrix4 transform
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
    }

    public class MeshObj : IDrawable
    {
        MaterialFX_POSITION material;
        VertexLayout d_geometry;
        VertexDeclaration instance_decl;
        RenderWindow render;
        VertexBuffer gVB;
        IndexBuffer iB;

        public MeshObj(RenderWindow render)
        {
            this.material = new MaterialFX_POSITION(render);
            this.material.SolidWireframe = true;
            this.material.WireframeColor = Color.Black;
            this.material.Diffuse = Color.Blue;
            this.render = render;

            MeshListGeometry obj = BaseTriGeometry.CubeOpen();

            d_geometry = new VertexLayout(NTVERTEX.m_elements);
            instance_decl = new VertexDeclaration(render, NTVERTEX.m_elements);
            
            int nverts = obj.vertices.Count;
            NTVERTEX[] vertices = new NTVERTEX[nverts];
            
            for (int i = 0; i < nverts; i++)
            {
                vertices[i] = new NTVERTEX();
                vertices[i].m_pos = obj.vertices[i] + new Vector3(0, 0, 0);
                if (obj.texcoords.Count == nverts) vertices[i].m_uv = obj.texcoords[i];
                if (obj.normals.Count == nverts) vertices[i].m_norm = obj.normals[i];
            }

            gVB = new VertexBuffer(render, d_geometry, BufferUsage.Managed, nverts);
            gVB.OpenStream().WriteCollection<NTVERTEX>(vertices, 0, nverts, 0);
            gVB.CloseStream();
            gVB.Count = nverts;


            Face16[] faces = obj.indices.data;
            iB = new IndexBuffer(render, IndexLayout.One16, BufferUsage.Managed, faces.Length * 3);
            iB.OpenStream().WriteCollection<Face16>(faces, 0, faces.Length, 0);
            iB.CloseStream();
            iB.Count = faces.Length * 3;
       
        }

        public void Draw()
        {
            int numpass = material.Begin();
            for (int pass = 0; pass < numpass; pass++)
            {
                material.BeginPass(pass);
                render.Device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, gVB.Count, 0, iB.Count / 3);
                material.EndPass();
            }
            material.End();
        }


        public void SetToDevice()
        {
            render.Device.SetVertexDeclaration(instance_decl);
            render.Device.SetIndexStream(iB);
            render.Device.SetVertexStream(gVB, 0);
        }
        public void SetCamera(ICamera camera)
        {
            material.ProjViewWorld = camera.Projection * camera.View;
            material.Diffuse = Color.Blue;
            material.ApplyParams();
        }
        
    }

    public class InstanceObj : IDrawable
    {
        bool INSTANCE = true;
        MaterialFX_INSTANCING material;

        int numinstances = 0;
        MeshListGeometry billboard = BaseTriGeometry.Billboard(eAxis.X);
        VertexLayout d_geometry, d_instance;
        VertexDeclaration instance_decl;
        RenderWindow render;

        VertexBuffer gVB, iVB;
        IndexBuffer iB;

        public int NumTotalVertices
        {
            get { return billboard.numVertices * numinstances; }
        }


        public InstanceObj(RenderWindow render, int numinstances)
        {
            this.material = new MaterialFX_INSTANCING(render);
            this.render = render;
            this.numinstances = numinstances;

            Vector3[] positions = null;

            if (INSTANCE)
            {
                positions = new Vector3[numinstances];
                int i = 0;
                for (float x = 0; x < 100 && i < numinstances; x += 2)
                    for (float y = 0; y < 100 && i < numinstances; y += 2)
                        for (float z = 0; z < 100 && i < numinstances; z += 2)
                            positions[i++] = new Vector3(x * 0.25f, y * 0.25f, z * 0.25f);
            }
            else
            {
                positions = new Vector3[billboard.numVertices];
                for (int i = 0; i < billboard.numVertices; i++)
                    positions[i] = new Vector3(0, 0, 0);
            }

            List<VertexElement> e_geometry = new List<VertexElement>();
            List<VertexElement> e_traslation = new List<VertexElement>();
            e_geometry.Add(new VertexElement(0, 0, DeclarationType.Float3, DeclarationMethod.Default, DeclarationUsage.Position, 0));
            e_geometry.Add(new VertexElement(0, 12, DeclarationType.Color, DeclarationMethod.Default, DeclarationUsage.Color, 0));
            e_traslation.Add(new VertexElement(1, 0, DeclarationType.Float3, DeclarationMethod.Default, DeclarationUsage.TexCoord, 0));

            d_geometry = new VertexLayout(e_geometry);
            d_instance = new VertexLayout(e_traslation);


            List<VertexElement> e_global = new List<VertexElement>(e_geometry);
            e_global.AddRange(e_traslation);
            instance_decl = new VertexDeclaration(render, e_global);

            gVB = new VertexBuffer(render, d_geometry, BufferUsage.Managed, billboard.numVertices);
            iVB = new VertexBuffer(render, d_instance, BufferUsage.Managed, positions.Length);
            //gVB = new VertexBuffer(typeof(CVERTEX), d_geometry, billboard.numVertices, false);
            //iVB = new VertexBuffer(typeof(VERTEX), d_instance, positions.Length, false);
            

            VertexElement element;

            if (!instance_decl.GetElement(DeclarationUsage.Position, 0, out element)) throw new ArgumentException("Notfound");
            if (!instance_decl.GetElement(DeclarationUsage.Color, 0, out element)) throw new ArgumentException("Notfound");

            VertexStream vstream = gVB.OpenStream();
            vstream.WriteCollection<Vector3>(billboard.vertices.data, element, 0, billboard.numVertices, 0);
            vstream.WriteCollection<Color32>(billboard.colors.data, element, 0, billboard.numVertices, 0);
            gVB.CloseStream();
            gVB.Count = billboard.numVertices;


            iVB.OpenStream().WriteCollection<Vector3>(positions, 0, positions.Length, 0);
            iVB.CloseStream();
            iVB.Count = positions.Length;


            iB = new IndexBuffer(render, IndexLayout.One16, BufferUsage.Managed, billboard.numIndices);
            //iB = new IndexBuffer(IndexInfo.IndexOne16, billboard.numIndices, false);

            iB.OpenStream().WriteCollection<Face16>(billboard.indices.data, 0, billboard.numIndices / 3, 0);
            iB.CloseStream();
            iB.Count = billboard.numIndices;
        }


        public void Draw()
        {
            if (INSTANCE)
            {
                render.Device.SetStreamFrequency(numinstances, StreamSource.IndexedData, 0);
                render.Device.SetStreamFrequency(1, StreamSource.InstanceData, 1);
            }
            int numpass = material.Begin();
            for (int pass = 0; pass < numpass; pass++)
            {
                material.BeginPass(pass);
                render.Device.DrawIndexedPrimitives(billboard.primitive, 0, 0, billboard.numVertices, 0, billboard.numPrimitives);
                material.EndPass();
            }
            material.End();

            if (INSTANCE)
            {
                render.Device.SetStreamFrequency(1, StreamSource.Reset, 0);
                render.Device.SetStreamFrequency(1, StreamSource.Reset, 1);
            }
        }

        public void Render(MaterialBase material)
        {
            throw new NotImplementedException();
        }

        public void SetToDevice()
        {
            render.Device.SetVertexDeclaration(instance_decl);
            render.Device.SetVertexStream(gVB, 0);
            render.Device.SetVertexStream(iVB, 1);
            render.Device.SetIndexStream(iB);
        }
        public void SetCamera(ICamera camera)
        {
            material.ProjViewWorld = camera.Projection * camera.View; 
        }
        
        public Matrix4 transform
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
    }
    
    public class AxisObj : IDrawable
    {
        MaterialFX_VCOLOR material;
        RenderWindow render;
        VertexBuffer axis;
        VertexDeclaration cvertex_decl;

        public AxisObj(RenderWindow render)
        {
            this.render = render;
            this.material = new MaterialFX_VCOLOR(render);
            this.cvertex_decl = new VertexDeclaration(render, CVERTEX.m_elements);

            CVERTEX[] verts = new CVERTEX[]
            {
                new CVERTEX(0,0,0, Color.Red),
                new CVERTEX(10,0,0, Color.Red),
                new CVERTEX(0,0,0, Color.Green),
                new CVERTEX(0,10,0, Color.Green),
                new CVERTEX(0,0,0, Color.Blue),
                new CVERTEX(0,0,10, Color.Blue)
            };
            axis = new VertexBuffer(render, cvertex_decl.Format, BufferUsage.Static, verts.Length);
            VertexStream stream = axis.OpenStream();
            stream.WriteCollection<CVERTEX>(verts, 0, verts.Length, 0);
            axis.CloseStream();
            axis.Count = verts.Length;
        }

        public Matrix4 transform
        {
            get { return Matrix4.Identity; }
            set { }
        }
        public void SetCamera(ICamera camera)
        {
            material.ProjViewWorld = camera.Projection * camera.View;
        }

        public void Draw()
        {
            int count = material.Begin();
            for (int pass = 0; pass < count; pass++)
            {
                material.BeginPass(pass);
                render.Device.DrawPrimitives(PrimitiveType.LineList, 0, axis.Count / 2);
                material.EndPass();
            }
            material.End();
        }

        public void Draw(MaterialBase material)
        {
            throw new NotImplementedException();
        }
        public void SetToDevice()
        {
            render.Device.SetVertexDeclaration(cvertex_decl);
            render.Device.SetVertexStream(axis, 0);
        }
        public void Render(MaterialBase material)
        {
            throw new NotImplementedException();
        }
    }
}
