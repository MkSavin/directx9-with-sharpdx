struct Material
{
	float4 ambient;
	float4 diffuse;
	float4 emissive;
	float4 specular;
	float shininess;
};
//-----------------------------------------------------------------------------
// Globals.
//-----------------------------------------------------------------------------
float4x4 WorldViewProj : WORLDVIEWPROJ;
Material material;


//-----------------------------------------------------------------------------
// Vertex Shaders.
//-----------------------------------------------------------------------------
struct VS_INPUT
{
    float4  Position   : POSITION;
	float4  Color      : COLOR0;
	float4  Traslation : TEXCOORD;
};
struct VS_OUTPUT
{   
	float4  Position  : POSITION;
    float4  Color     : COLOR0;
};

VS_OUTPUT VShaderA(VS_INPUT input)
{
	VS_OUTPUT output = (VS_OUTPUT)0;
	output.Position = input.Position + input.Traslation;
	output.Position.w = 1;
	output.Position = mul(output.Position, WorldViewProj);
	output.Color = input.Color;
	return output;
}
VS_OUTPUT VShaderB(VS_INPUT input)
{
	VS_OUTPUT output = (VS_OUTPUT)0;
	output.Position = input.Position + input.Traslation;
	output.Position.w = 1;
	output.Position = mul(output.Position, WorldViewProj);
	output.Color = material.ambient;
	return output;
}


//-----------------------------------------------------------------------------
// Pixel Shaders.
//-----------------------------------------------------------------------------
float4 PShader(VS_OUTPUT Input) : COLOR0
{
	return Input.Color;
}
//-----------------------------------------------------------------------------
// Techniques.
//-----------------------------------------------------------------------------
technique InstancingTechique
{
    pass p0
    {
        VertexShader = compile vs_3_0 VShaderA();
		PixelShader =  compile ps_3_0 PShader();
    }
}
technique WireframeTechique1
{
	// color used from material
    pass p0
    {
        VertexShader = compile vs_3_0 VShaderA();
		PixelShader =  compile ps_3_0 PShader();
    }
	// for wireframe color
	pass p1
    {
        VertexShader = compile vs_3_0 VShaderB();
        PixelShader =  compile ps_3_0 PShader();
	}
}
technique WireframeTechique2
{
    pass p0
    {
		FillMode = Solid;
		DepthBias = 0.0f;
        VertexShader = compile vs_3_0 VShaderA();
		PixelShader =  compile ps_3_0 PShader();
    }
	pass p1
    {
		FillMode = Wireframe;
		DepthBias = -0.001f;
        VertexShader = compile vs_3_0 VShaderB();
		PixelShader =  compile ps_3_0 PShader();
		DepthBias = 0.0f;
    }
}