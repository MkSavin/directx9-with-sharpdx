﻿using System;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Collections.Generic;
using System.Text;

using Engine.Graphics;
using Engine.Tools;
using Engine.Maths;
using Engine.Renderers;
using Engine.Geometry;


namespace Engine.Test
{
    public class BillboardHLSL : Effect
    {
        private EffectParamMatrix W, V, P;

        public BillboardHLSL(RenderWindow render)
            : base(render, Engine.Test.Properties.Resources.Effect)
        {
            if (DX9Enumerations.VertexShaderVersion.Major < 1) throw new Exception("This graphic card don't support shader 3.0");

            ParameterCollector.AddUsed (W = new EffectParamMatrix("xWorld"));
            ParameterCollector.AddUsed(V = new EffectParamMatrix("xView"));
            ParameterCollector.AddUsed(P = new EffectParamMatrix("xProj"));

            EffectTechnique DefaultTechnique = new EffectTechnique("Default");
            TechniqueCollector.AddUsed(DefaultTechnique);
            Technique = DefaultTechnique;
        }

        public Matrix4 Proj
        {
            get { return P.Value; }
            set { P.Value = value; }
        }
         public Matrix4 World
        {
            get { return W.Value; }
            set { W.Value = value; }
        }
        
        public Matrix4 View
        {
            get { return V.Value; }
            set { V.Value = value; }
        }

    }

    public class BillboardObj 
    {
        BillboardHLSL effect;

        VertexLayout vertexStructure;
        VertexDeclaration vertexDeclaration;
        RenderWindow render;
        VertexBuffer vertexBuffer;
        IndexBuffer faceBufer;

        public BillboardObj(RenderWindow render)
        {
            this.render = render;

            MeshListGeometry obj = BaseTriGeometry.CubeOpen();

            vertexStructure = new VertexLayout(NTVERTEX.m_elements);
            vertexDeclaration = new VertexDeclaration(render, NTVERTEX.m_elements);

            int count = obj.vertices.Count;
            int nverts = count + 4;
            
            NTVERTEX[] vertices = new NTVERTEX[nverts];

            for (int i = 0; i < count; i++)
                vertices[i] = new NTVERTEX(obj.vertices[i], Vector3.Zero, Vector2.Zero);


            vertices[0 + count] = new NTVERTEX(new Vector3(0, 0, 3), Vector3.Zero, new Vector2(0, 0) + Vector2.One);
            vertices[1 + count] = new NTVERTEX(new Vector3(0, 0, 3), Vector3.Zero, new Vector2(1, 0) + Vector2.One);
            vertices[2 + count] = new NTVERTEX(new Vector3(0, 0, 3), Vector3.Zero, new Vector2(1, 1) + Vector2.One);
            vertices[3 + count] = new NTVERTEX(new Vector3(0, 0, 3), Vector3.Zero, new Vector2(0, 1) + Vector2.One);


            vertexBuffer = new VertexBuffer(render, vertexStructure, BufferUsage.Managed, nverts);       
            vertexBuffer.OpenStream().WriteCollection<NTVERTEX>(vertices, 0, nverts, 0);
            vertexBuffer.CloseStream();
            vertexBuffer.Count = nverts;


            Face16[] quad = new Face16[]
            {
                new Face16(count,count+1,count+2),
                new Face16(count,count+2,count+3)
            };


            faceBufer = new IndexBuffer(render, IndexLayout.Face16, BufferUsage.Managed, obj.indices.Count + 2);
            faceBufer.OpenStream().WriteCollection<Face16>(obj.indices, 0, 0);
            faceBufer.OpenStream().WriteCollection<Face16>(quad, 0,quad.Length,obj.indices.Count);
            faceBufer.CloseStream();
            faceBufer.Count = obj.indices.Count + 2;

            this.effect = new BillboardHLSL(render);
        }


        public void Draw(ICamera camera , Matrix4 world)
        {
            effect.World = world;
            effect.View = camera.View;
            effect.Proj = camera.Projection;

            render.Device.renderstates.fillMode = FillMode.Solid;
            render.Device.renderstates.cullMode = Cull.None;

            render.Device.SetVertexDeclaration(vertexDeclaration);
            render.Device.SetIndexStream(faceBufer);
            render.Device.SetVertexStream(vertexBuffer, 0);

            int numpass = effect.Begin();
            for (int pass = 0; pass < numpass; pass++)
            {
                effect.BeginPass(pass);
                render.Device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, vertexBuffer.Count, 0, faceBufer.Count);
                effect.EndPass();
            }
            effect.End();
        }
    }

}
