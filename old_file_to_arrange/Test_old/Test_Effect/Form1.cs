﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

using Engine.Graphics;
using Engine.Renderers;
using Engine.Tools;
using Engine.Maths;
using Font = Engine.Graphics.Font;
using Engine;

namespace Engine.Test
{
    public partial class Form1 : Form
    {
        RenderWindow render;
        bool draw = true;

        Axis axis;
        EffectLight effect;

        BillboardObj billboards;
        TrackBallCamera_new cameractrl;
        PreciseTimer timer;
        int frames = 0;
        double renderTime = 0;

        /// <summary>
        /// Punto di ingresso principale dell'applicazione.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }

        public Form1()
        {
            this.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint | ControlStyles.Opaque, true);
            InitializeComponent();

            cameractrl = new TrackBallCamera_new(this, new Vector3(-10, 5, -10), Vector3.Zero, Vector3.UnitZ, 0.1f, 1000.0f);
            render = new RenderWindow(this);

            effect = new EffectLight(render);
            axis = new Axis(render, 10.0f, Vector3.Zero);
            billboards = new BillboardObj(render);

            timer = new PreciseTimer();
        }


        protected override void OnClosing(CancelEventArgs e)
        {
            render.Dispose();
            base.OnClosing(e);
        }

        protected override void OnResizeBegin(EventArgs e)
        {
            base.OnResizeBegin(e);
            draw = false;
        }
        
        protected override void OnResizeEnd(EventArgs e)
        {
            base.OnResizeEnd(e);
            draw = true;
            render.Resize(new Viewport(ClientSize));
            this.Invalidate();
        }


        protected override void OnPaint(PaintEventArgs e)
        {
            if (!draw) return;

            System.Threading.Thread.Sleep(10);

            double currTime = timer.SecondsAccumulated;

            if (render.CheckDeviceState() == DeviceState.OK)
            {
                render.SetTarghet();

                render.Clear(Color.CornflowerBlue);
                render.BeginScene();

                axis.Draw(cameractrl,effect);
                billboards.Draw(cameractrl, Matrix4.Translating(0, 0, 5));
                
                billboards.Draw(cameractrl, Matrix4.Identity);


                render.EndScene();
                render.Present();

                frames++;

                renderTime += timer.SecondsAccumulated - currTime;

                if (renderTime > 1)
                {

                    Console.SetCursorPosition(0, 0);
                    Console.WriteLine(string.Format("FPS {0}", frames / renderTime));
                    Console.WriteLine(string.Format("avarage render time {0}", renderTime / frames));
                    frames = 0;
                    renderTime = 0;
                }

            }
            this.Invalidate();

        }
    }
}
