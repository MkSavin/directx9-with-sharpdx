float4x4 xWorld;
float4x4 xView;
float4x4 xProj;



struct VS_INPUT
{
    float4  Position  : POSITION;
	float4  Normal    : TEXCOORD1;
	float2  TexCoord  : TEXCOORD0;
};

struct VS_OUTPUT
{
    float4  Position  : POSITION;    
    float4  Color     : COLOR0;
};



VS_OUTPUT VShaderBillboard(VS_INPUT input)
{
	VS_OUTPUT output = (VS_OUTPUT)0; 
	

	output.Position = input.Position;
	input.TexCoord  = (input.TexCoord - 1.0f) * 2.0f - 1.0f;

	output.Position = mul(output.Position, xWorld);
	output.Position = mul(output.Position, xView);
	
	output.Position.xy += input.TexCoord;

	
	output.Position = mul(output.Position, xProj);
	output.Color = float4(0,1,0,1);
	return output;
}

VS_OUTPUT VShader(VS_INPUT input)
{
	VS_OUTPUT output = (VS_OUTPUT)0; 
	
	if (input.TexCoord.x > 0.99f && input.TexCoord.y > 0.99f)
	{
		return VShaderBillboard(input);
	}

	output.Position = input.Position;
	output.Position = mul(output.Position, xWorld);
	output.Position = mul(output.Position, xView);
	output.Position = mul(output.Position, xProj);
	output.Color = float4(1,0,0,1);
	return output;
}


float4 PShader(VS_OUTPUT input): COLOR0
{
	return input.Color;
}
float4 PShader_black(VS_OUTPUT input): COLOR0
{
	return float4(0,0,0,1);
}
//////////////////////////////////////
// Techniques specs follow
//////////////////////////////////////
technique Default
{
    pass solidpass
    {
		FillMode = Solid;
        VertexShader = compile vs_3_0 VShader();
        PixelShader =  compile ps_3_0 PShader();
    }
	pass wireframepass
    {
		FillMode = Wireframe;
        VertexShader = compile vs_3_0 VShader();
        PixelShader =  compile ps_3_0 PShader_black();
    }
}