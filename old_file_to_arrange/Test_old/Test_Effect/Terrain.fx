float4x4 transform;
float4x4 world;
float3  LightDirection;
float3  ViewDirection;
float4  Specular     : Specular;
float4  Diffuse      : Diffuse;
float   SpecularPower : Power;

texture base_Tex   : BaseTex;
texture bump_Tex   : NormalTex;
sampler2D baseMap = sampler_state
{
   Texture = (base_Tex);
   ADDRESSU = WRAP;
   ADDRESSV = WRAP;
   MINFILTER = ANISOTROPIC;
   MAGFILTER = ANISOTROPIC;
   MIPFILTER = ANISOTROPIC;
};


sampler2D bumpMap = sampler_state
{
   Texture = (bump_Tex);
   ADDRESSU = WRAP;
   ADDRESSV = WRAP;
   MINFILTER = LINEAR;
   MAGFILTER = LINEAR;
   MIPFILTER = LINEAR;
};

struct VS_INPUT
{
    float4  Pos             : POSITION;
    float3  Normal          : NORMAL;
    float3  Tex0            : TEXCOORD0;
    float3 Binormal         : BINORMAL;
    float3 Tangent 	        : TANGENT;
};

struct VS_OUTPUT
{
    float4  Pos			  : POSITION;    
    float2  Tex0	      : TEXCOORD0;
    float3  viewDirection : TEXCOORD1;
    float3  lightDirection: TEXCOORD2;	
    float3  Normal	      : TEXCOORD3;
    
};

struct PS_INPUT
{
    float2 Tex0          : TEXCOORD0;
    float3 viewDirection : TEXCOORD1;
    float3 lightDirection: TEXCOORD2;	
    float3 Normal        : TEXCOORD3;
};


VS_OUTPUT VShader(VS_INPUT input)
{
    	VS_OUTPUT output = (VS_OUTPUT)0; 
    
    	output.Pos = mul(input.Pos, transform);
    

		float3      Normal =0;
		float3      Tangent =0;
		float3      Binormal =0;
    	
    	Normal         = mul(  input.Normal ,world);
       	Binormal       = mul(  input.Binormal,world );
       	Tangent        = mul(  input.Tangent ,world);
        
        /*
		output.ViewDirection.x  = dot(ViewDirection, Tangent  );
		output.ViewDirection.y  = dot(ViewDirection, Binormal  );
		output.ViewDirection.z  = dot(ViewDirection, Normal  );
	       
		output.LightDirection.x  = dot(LightDirection ,Tangent  );
		output.LightDirection.y  = dot(LightDirection, Binormal  );
   		output.LightDirection.z  = dot(LightDirection, Normal  );
		*/
            
       	output.viewDirection.x  = dot( Tangent, ViewDirection );
       	output.viewDirection.y  = dot( Binormal, ViewDirection );
       	output.viewDirection.z  = dot( Normal, ViewDirection );
       
       	output.lightDirection.x  = dot( Tangent, LightDirection );
       	output.lightDirection.y  = dot( Binormal, LightDirection );
		output.lightDirection.z  = dot( Normal, LightDirection );
    
        output.Normal=Normal;
    	output.Tex0  = input.Tex0.xy;
    	return output;
}


float4 PSShader(PS_INPUT Input) : COLOR0 {

	float3 fvLightDirection = normalize( Input.lightDirection );

   	float3 Normal         = normalize( ( tex2D( bumpMap, Input.Tex0 ).xyz * 2.0f ) - 1.0f );
   	float  fNDotL           = dot( Normal, fvLightDirection ); 
   	
   	float3 Reflection     = normalize( ( ( 2.0f * Normal ) * ( fNDotL ) ) - fvLightDirection ); 
   	float3 _ViewDirection  = normalize( Input.viewDirection );
   	float  fRDotV           = max( 0.0f, dot( Reflection, _ViewDirection ) );
   
   	float4 BaseColor      = tex2D( baseMap,Input.Tex0  );
  	
   	
   	float4 TotalDiffuse   = fNDotL * BaseColor*Diffuse; 
   	float4 TotalSpecular  = Specular * pow( fRDotV, SpecularPower );
   
   	return( saturate( TotalDiffuse + TotalSpecular ) );
}

float4 PSShaderWOSpecular(PS_INPUT Input) : COLOR0 {

	float3 fvLightDirection = normalize( Input.lightDirection );

   	float3 Normal         = normalize( ( tex2D( bumpMap, Input.Tex0 ).xyz * 2.0f ) - 1.0f );
   	float  fNDotL           = dot( Normal, fvLightDirection ); 
   	float4 BaseColor      = tex2D( baseMap,Input.Tex0  );
   	float4 TotalDiffuse   = fNDotL * BaseColor*Diffuse;    	
   
   	return( saturate( TotalDiffuse  ) );
}


float4 PSShaderPerPixel(PS_INPUT Input) : COLOR0 {

	float3 fvLightDirection = normalize( LightDirection );

   	float3 Normal         = normalize( Input.Normal) ;
   	float  fNDotL           = dot( Normal, fvLightDirection ); 
   	
   	float3 Reflection     = normalize( ( ( 2.0f * Normal ) * ( fNDotL ) ) - fvLightDirection ); 
	float3 _ViewDirection  = normalize(ViewDirection );
	float  fRDotV           = max( 0.0f, dot( Reflection, _ViewDirection ) );
	   
	float4 BaseColor      = tex2D( baseMap,Input.Tex0  );
	  	
	   	
	float4 TotalDiffuse   = fNDotL * BaseColor*Diffuse; 
	float4 TotalSpecular  = Specular * pow( fRDotV, SpecularPower );
	   
   	return( saturate( TotalDiffuse + TotalSpecular ) );
}

float4 PSShaderPerPixelWOSpecular(PS_INPUT Input) : COLOR0 {

	float3 fvLightDirection = normalize( LightDirection );

   	float3 Normal         = normalize( Input.Normal) ;
   	float  fNDotL           = dot( Normal, fvLightDirection ); 
   	float4 BaseColor      = tex2D( baseMap,Input.Tex0  );
   	float4 TotalDiffuse   = fNDotL * BaseColor*Diffuse;    	
   
   	return( saturate( TotalDiffuse  ) );
}

//////////////////////////////////////
// Techniques specs follow
//////////////////////////////////////
technique t0
{
    pass p0
    {
        VertexShader = compile vs_2_0 VShader();
        PixelShader =  compile ps_2_0 PSShader();
    }
    
    pass p1
    {
        VertexShader = compile vs_2_0 VShader();
        PixelShader =  compile ps_2_0 PSShaderWOSpecular();
    }
    
    pass p2
	{
        VertexShader = compile vs_2_0 VShader();
        PixelShader =  compile ps_2_0 PSShaderPerPixel();
    }
    pass p3        
	{
        VertexShader = compile vs_2_0 VShader();
        PixelShader =  compile ps_2_0 PSShaderPerPixelWOSpecular();
    }
}