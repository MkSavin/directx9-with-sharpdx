float4x4 WorldViewProj;
float xphase;
float zphase;
float4 watercolor;

struct VS_INPUT
{
    float4 position   : POSITION;
	float3 normal	  : NORMAL;
	float2 textcoord  : TEXCOORD0;
};

struct VS_OUTPUT 
{
	float4 position   : POSITION;
	float2 textcoord  : TEXCOORD0;
	float3 normal     : TEXCOORD1;
    float4 color      : COLOR;
};


VS_OUTPUT VertexShaderFunction(VS_INPUT Input)
{
    VS_OUTPUT Output = (VS_OUTPUT)0;

	Input.position.w = 1;
	Input.position.y = sin((Input.position.x + xphase)*1.0f)*0.1f + sin((Input.position.z + zphase)*4.0f)*0.1f;
 
	float4 p =  mul(Input.position, WorldViewProj);
	//Output.position = float4(p.x,p.y,0,p.z);
	Output.position = p;
	Output.color = watercolor;
	Output.textcoord = Input.textcoord;

	return Output;    
}

float4 PixelShaderFunction(VS_OUTPUT Input) : COLOR
{
    return Input.color;
}

technique Wave
{
    pass Pass0
    {        
        VertexShader = compile vs_2_0 VertexShaderFunction();
        PixelShader = compile ps_2_0 PixelShaderFunction();
    }
}

/*
Vertex shader support in directx 9

BINORMAL[n]	    Binormal					float4
BLENDINDICES[n]	Blend indices				uint
BLENDWEIGHT[n]	Blend weights				float
COLOR[n]	    Diffuse and specular color	float4
NORMAL[n]	    Normal vector				float4
POSITION[n]   	Vertex position in object space.	float4
POSITIONT	    Transformed vertex position.		float4
PSIZE[n]	    Point size					float
TANGENT[n]	    Tangent						float4
TEXCOORD[n]	    Texture coordinates			float4
Output	        Description	Type
COLOR[n]	    Diffuse or specular color	float4
FOG	            Vertex fog					float
POSITION[n]	    Position of a vertex in homogenous space. Compute position in screen-space by dividing (x,y,z) by w. Every vertex shader must write out a parameter with this semantic.	float4
PSIZE	        Point size					float
TESSFACTOR[n]	Tessellation factor			float
TEXCOORD[n]	    Texture coordinates			float4

Pixel shader suppor in directx9

COLOR[n]	Diffuse or specular color.	float4
TEXCOORD[n]	Texture coordinates	float4
VFACE	Floating-point scalar that indicates a back-facing primitive. A negative value faces backwards, while a positive value faces the camera.
Note  This semantic is available in Direct3D 9 Shader Model 3.0. For Direct3D 10 and later, use SV_IsFrontFace instead.
float
VPOS	The pixel location (x,y) in screen space. To convert a Direct3D 9 shader (that uses this semantic) to a Direct3D 10 and later shader, see Direct3D 9 VPOS and Direct3D 10 SV_Position)	float2
Output	Description	Type
COLOR[n]	Output color	float4
DEPTH[n]	Output depth



*/