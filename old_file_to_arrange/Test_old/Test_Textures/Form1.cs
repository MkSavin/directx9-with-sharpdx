﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

using Font = Engine.Graphics.Font;

using Engine.Graphics;
using Engine.Tools;
using Engine.Maths;
using Engine.Content;
using Engine.Utils;


namespace Engine.Test
{
    public partial class SampleForm : Form
    {
        bool first = true;
        RenderWindow render;
        Font font;
        Axis axis;
        Terrain mesh;
        QuadBuffer2D quad;

        EffectLight effect;
        EffectRenderTexture effectRenderToTarghet;

        Viewport[] views;
        Color[] colors;
        Texture2DTarghet[] texture_output;


        TrackBallCamera_new cameractrl;


        /// <summary>
        /// Punto di ingresso principale dell'applicazione.
        /// </summary>
        [STAThread]
        static void Main()
        {
            SampleForm form = new SampleForm();
            Application.Run(form);
        }

        public SampleForm()
        {
            this.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint | ControlStyles.Opaque, true);
            InitializeComponent();
            InitializeGraphics();
        }


        /// <summary>
        /// Liberare le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);

            render.Destroy();
        }

        void InitializeGraphics()
        {
            EngineResources.ContentFolder = @"C:\Users\john\PROJECTS\3dEngine\3dengineSharpDx\Engine\Resources\Content\";


            render = new RenderWindow(new FrameSetting(true, false, new Viewport(ClientSize), this.Handle, true));
            render.Device.renderstates.ShadeMode = ShadeMode.Flat;

            cameractrl = new TrackBallCamera_new(this, new Vector3(-5, 5, -5), Vector3.Zero, Vector3.UnitY, 0.1f, 100.0f);
            cameractrl.Enabled = true;

            font = new Font(render.Device, Engine.Graphics.Font.FontName.Arial, 8);
            axis = new Axis(render.Device, 10.0f);
            effect = new EffectLight(render.Device);
            
            effectRenderToTarghet = new EffectRenderTexture(render.Device);

            mesh = new Terrain(render.Device, Properties.Resources.heights);
            quad = new QuadBuffer2D(render.Device);


            texture_output = new Texture2DTarghet[4];
            
            for (int i = 0; i < 4; i++)
            {
                texture_output[i] = new Texture2DTarghet(render);
            }

            InitializeViewports();
        }

        void InitializeViewports()
        {
            views = new Viewport[5];
            colors = new Color[5];
            int w2 = ClientSize.Width / 2;
            int h2 = ClientSize.Height / 2;


            views[0] = new Viewport(ClientSize);
            views[1] = new Viewport(0, 0, w2,h2);
            views[2] = new Viewport(w2, 0, w2, h2);
            views[3] = new Viewport(0, h2, w2, h2);
            views[4] = new Viewport(w2, h2, w2, h2);

            colors[0] = Color.CornflowerBlue;
            colors[1] = Color.FromArgb(255, 0, 0);
            colors[2] = Color.FromArgb(255, 100, 100);
            colors[3] = Color.FromArgb(0,255,0);
            colors[4] = Color.FromArgb(100, 255, 100);


        }

        protected override void OnResizeBegin(EventArgs e)
        {
            base.OnResizeBegin(e);

        }

        protected override void OnResizeEnd(EventArgs e)
        {
            base.OnResizeEnd(e);
            render.ResizeBackBuffer(ClientSize);
            InitializeViewports();
            this.Invalidate();
        }

        void NormalRender()
        {
            System.Threading.Thread.Sleep(1);

            if (render.Device.CanDraw())
            {
                render.FrameViewport = views[0];
                render.SetOriginalTarghet();
                render.ClearDraw(Color.CornflowerBlue);

                render.BeginDraw();
                axis.Draw(cameractrl.Projection * cameractrl.View, effect);
                mesh.Draw(cameractrl,effect,effect.TechDiffuseLighting);
                render.EndDraw();
                render.Present();
                this.Invalidate();
            }
        }

        void RenderTarget0()
        {
            render.SetOriginalTarghet();
            render.FrameViewport = views[0];
            render.ClearDraw(Color.CornflowerBlue);
            
            render.BeginDraw();
            effectRenderToTarghet.DrawTextureToTarghet(quad,texture_output[1]);
            axis.Draw(cameractrl.Projection * cameractrl.View, effect);
            render.EndDraw();
        }

        void MultiRender()
        {
            render.SetOriginalTarghet();
            render.ClearDraw(Color.CornflowerBlue);
            for (int i = 0; i < 4; i++)
            {
                render.FrameViewport = views[i + 1];
                render.BeginDraw();

                effectRenderToTarghet.DrawTextureToTarghet(quad, texture_output[i]);
                axis.Draw(cameractrl.Projection * cameractrl.View, effect);

                render.EndDraw();
            }

        }
        protected override void OnPaint(PaintEventArgs e)
        {
            System.Threading.Thread.Sleep(1);

            if (render.Device.CanDraw())
            {

                render.FrameViewport = views[0];

                for (int i = 0; i < 4; i++)
                    render.Device.SetTextureTarghet(texture_output[i], i);

                render.ClearDraw(Color.Gray);
                render.BeginDraw();
                axis.Draw(cameractrl.Projection * cameractrl.View, effect);
                mesh.world = Matrix4.Translating(1, -0.5f, 1);
                mesh.DrawMRT(cameractrl);
                mesh.world = Matrix4.Identity;
                mesh.DrawMRT(cameractrl);
                render.EndDraw();

                if (first)
                {
                    for (int i = 0; i < 4; i++)
                        texture_output[i].Save("texture_output_" + i.ToString() + ".jpg", ImageFileFormat.JPG);
                }

                MultiRender();

                render.Present();

                first = false;
                this.Invalidate();

            }
        }
    }
}
