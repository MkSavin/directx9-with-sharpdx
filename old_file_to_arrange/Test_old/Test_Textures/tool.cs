﻿using System;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Collections.Generic;
using System.Text;


using Engine.Graphics;
using Engine.Tools;
using Engine.Maths;
using Engine.Renderers;
using Engine.Geometry;


namespace Engine.Test
{
    public class TerrainEffect : Effect
    {
        public EffectTechnique MultiRenderTarghetTechnique;
        public EffectParamMatrix World, View, Proj;
        public EffectParamColor MaterialDiffuse, LightColor;
        public EffectParamVector3 LightDirection;
        public EffectParamFloat LightIntensity;
        public EffectParamVector3 Eye;
        public TerrainEffect(Device device)
            : base(device, Properties.Resources.EffectMRT)
        {

            MultiRenderTarghetTechnique = new EffectTechnique(this, "MultiRenderTarghetTechnique");
            World = new EffectParamMatrix(this, "World");
            View = new EffectParamMatrix(this, "View");
            Proj = new EffectParamMatrix(this, "Proj");
            MaterialDiffuse = new EffectParamColor(this, "MaterialDiffuse");
            LightColor = new EffectParamColor(this, "LightColor");
            LightDirection = new EffectParamVector3(this, "LightDirection");
            LightIntensity = new EffectParamFloat(this, "LightIntensity");
            LightColor.Value = Color32.White;
            Eye = new EffectParamVector3(this, "Eye");
        }
    }


    public class Terrain
    {
        Device device;
        VertexBuffer vertexbuffer;
        IndexBuffer facebuffer;
        VertexDeclaration declaration;
        TerrainEffect effectMRT;

        TerrainVertex[] grid;
        Face16[] grid_index;
        public Matrix4 world;

        public Terrain(Device device , Bitmap heightmap)
        {
            InitGrid( heightmap);


            this.device = device;
 
            this.effectMRT = new TerrainEffect(device);

            VertexLayout layout = new VertexLayout();
            layout.Add(0, DeclarationType.Float3, DeclarationUsage.Position);
            layout.Add(0, DeclarationType.Float2, DeclarationUsage.TexCoord);
            layout.Add(0, DeclarationType.Float3, DeclarationUsage.Normal);
            layout.Add(0, DeclarationType.Color, DeclarationUsage.Color);

            declaration = new VertexDeclaration(device, layout);

            vertexbuffer = new VertexBuffer(device, layout, BufferUsage.Managed, grid.Length);
            VertexStream vstream = vertexbuffer.OpenStream();
            vstream.WriteCollection<TerrainVertex>(grid, 0, grid.Length, 0);
            vertexbuffer.CloseStream();
            vertexbuffer.Count = grid.Length;

            facebuffer = new IndexBuffer(device, IndexLayout.Face16, BufferUsage.Managed, grid_index.Length);
            IndexStream istream = facebuffer.OpenStream();
            istream.WriteCollection<Face16>(grid_index, 0, grid_index.Length, 0);
            facebuffer.CloseStream();
            facebuffer.Count = grid_index.Length;
        }

        public void Draw(ICamera camera, EffectLight simple , EffectTechnique usetechnique)
        {
            simple.WorldViewProj.Value = camera.Projection * camera.View;
            simple.View.Value = camera.View;
            simple.Proj.Value = camera.Projection;
            simple.World.Value = world;
            simple.WorldInvTraspose.Value = Matrix4.Inverse( Matrix4.Traspose(world));
            simple.LightDirection.Value = Vector3.GetNormal(Vector3.One);
            simple.LightIntensity.Value = 1.0f;

            device.SetVertexDeclaration(declaration);
            device.SetVertexStream(vertexbuffer);
            device.SetIndexStream(facebuffer);


            /*
            simple.Technique = simple.TechDiffuseLighting;
            simple.Begin();
            simple.BeginPass(0);
            device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, vertexbuffer.Count, 0, facebuffer.Count);
            simple.EndPass();
            simple.End();
            */

            foreach (Pass pass in usetechnique)
            {
                device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, vertexbuffer.Count, 0, facebuffer.Count);
            }
        }

        public void DrawMRT(ICamera camera)
        {
            effectMRT.View.Value = camera.View;
            effectMRT.Proj.Value = camera.Projection;
            effectMRT.World.Value = world;

            effectMRT.LightDirection.Value = Vector3.GetNormal(Vector3.One);
            effectMRT.LightIntensity.Value = 1.0f;
            effectMRT.Eye.Value = camera.CameraView.Position;

            device.SetVertexDeclaration(declaration);
            device.SetVertexStream(vertexbuffer);
            device.SetIndexStream(facebuffer);

            foreach (Pass pass in effectMRT.MultiRenderTarghetTechnique)

            device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, vertexbuffer.Count, 0, facebuffer.Count);


        }

        public void InitGrid(Bitmap heightmap)
        {
            // that mean x from 0 to 10
            int W = 11;
            int H = 11;


            BitmapLock bmplock = new BitmapLock(heightmap, System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor);
            bmplock.LockBits();


            int numVerts = W * H;
            grid = new TerrainVertex[numVerts];

            for (int y = 0; y < H; y++)
            {
                for (int x = 0; x < W; x++)
                {
                    int i = x + y * H;
                    float xx = x / (float)(W-1);
                    float zz = y / (float)(H-1);

                    float px = xx;
                    float py = 1 - zz;

                    grid[i] = new TerrainVertex();
                    grid[i].TexCoord = new Vector2(px, py);
                    grid[i].Normal = new Vector3(1, 0.5f, 1);
                    grid[i].Color = bmplock.GetPixel(px, py);

                    float h = grid[i].Color.Brightness ;

                    grid[i].Position = new Vector3(xx * 10, h * 4.0f, zz * 10);
                }
            }
            bmplock.UnlockBits();

            int numFaces = (W-1) * (H-1) * 2;

            grid_index = new Face16[numFaces];

            for (int n = 0, y = 0; y < H-1; y++)
                for (int x = 0; x < W-1; x++)
                {
                    int xx = x + 1;
                    int yy = y + 1;
                    grid_index[n++] = new Face16(x + y * W, x + yy * W, xx + yy * W);
                    grid_index[n++] = new Face16(x + y * W, xx + yy * W, xx + y * W);
                }
            
            GeometryTools.CalculateNormals<TerrainVertex>(grid, grid_index);

        }

        [Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 4, Size = sizeof(float) * 8 + sizeof(uint))]
        
        public struct TerrainVertex : IVertexPosition , IVertexNormal
        {
            public Vector3 Position;
            public Vector2 TexCoord;
            public Vector3 Normal;
            public Color32 Color;

            public Vector3 normal
            {
                get { return Normal; }
                set
                { 
                    Normal = value; 
                }
            }

            public Vector3 position
            {
                get { return Position; }
                set { Position = value; }
            }

            public override string ToString()
            {
                return string.Format("{0} {1}" , TexCoord , Position.y);
            }
        }
    }
}
