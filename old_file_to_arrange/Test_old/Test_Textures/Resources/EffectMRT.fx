// Multi Render Targhet sample

#define WHITE float4(1,1,1,1);
#define BLACK float4(0,0,0,1);
#define BLUE  float4(0,0,1,1);

float4x4 WorldViewProj;
float4x4 World;
float4x4 View;
float4x4 Proj;

float3 Eye;
float4 MaterialDiffuse = BLUE;

float4 LightColor = WHITE;
float3 LightDirection = float3(1,0,0);
float  LightIntensity = 1.0f;
float  SpecularPower = 2.0f;
float  SpecularIntensity = 1.0f;

//-----------------------------------------------------------------------------
// Basic Shader program
//-----------------------------------------------------------------------------
struct VS_INPUT
{
    float4  Position  : POSITION;
	float4  Color     : COLOR;
	float3  Normal    : NORMAL;
	float2  TexCoord  : TEXCOORD;
};

struct VS_OUTPUT
{   
    float4  Position  : POSITION;
	float4  WPosition : TEXCOORD0;
	float3  Normal    : TEXCOORD1;
};

struct PS_OUTPUT
{   
    float4  Targhet0 : COLOR0;
	float4  Targhet1 : COLOR1;
	float4  Targhet2 : COLOR2;
};


/*
float4 SpecularCalculation(float4 InNormal)
{
	// Calculate the ambient term: 
	float4 Ambient = AmbientIntensity * vecAmbient; 
	// Calculate the diffuse term: 
	InNormal = normalize(InNormal); 
	float4 Diffuse = DiffuseIntensity * SunColor * saturate(dot(-vecSunDir, InNormal)); 
	// Fetch the pixel color from the color map: 
	float4 Color = tex2D(ColorMapSampler, InTex); 
	// Calculate the reflection vector: 
	float3 R = normalize(2 * dot(InNormal, -vecSunDir) * InNormal + vecSunDir); 
	// Calculate the speculate component: 
	float Specular = pow(saturate(dot(R, normalize(InViewDir))), SpecularPower) * SpecularIntensity; 
	// Calculate final color: 
	return (Ambient + Diffuse + Specular) * Color; 
}
*/


//-----------------------------------------------------------------------------
// First Pass : draw model to 3 render targhet
//-----------------------------------------------------------------------------

VS_OUTPUT VShader(VS_INPUT input)
{
	VS_OUTPUT output = (VS_OUTPUT)0; 
	
	output.WPosition = mul(input.Position, World);
	output.WPosition = mul(output.WPosition, View);
	output.WPosition /= output.WPosition.w;

	output.Position = mul(output.WPosition, Proj);	
	output.Normal = input.Normal;

	return output;
}

PS_OUTPUT PShader(VS_OUTPUT input)
{
	PS_OUTPUT output = (PS_OUTPUT)0;

	float4 diffuse = (float4)BLUE;
	float3 N  = normalize(input.Normal);
	float3 D  = normalize(input.WPosition.xyz - Eye);
	float  NL = dot(N, LightDirection);
	float3 R  = normalize(2 * NL * N - LightDirection); 
	float  S  = pow(saturate(dot(R,D)), SpecularPower) * SpecularIntensity;
	float4 diffuseS = diffuse * S;


	output.Targhet0 = saturate(diffuseS);
	output.Targhet1 = float4(input.Normal, 1);
	output.Targhet2 = saturate(diffuse);

	return output;
}
PS_OUTPUT PShaderBlack(VS_OUTPUT input)
{
	PS_OUTPUT output = (PS_OUTPUT)0;

	output.Targhet0 = BLACK;
	output.Targhet1 = BLACK;
	output.Targhet2 = BLACK;

	return output;
}


//-----------------------------------------------------------------------------
// Second Pass draw one texture to backbuffer
//-----------------------------------------------------------------------------




//-----------------------------------------------------------------------------
// Techniques.
//-----------------------------------------------------------------------------
technique MultiRenderTarghetTechnique
{
    pass p0
    {
        VertexShader = compile vs_2_0 VShader();
		PixelShader =  compile ps_2_0 PShader();
		
		ShadeMode = FLAT;
		FillMode = Solid;
    }
	pass p1
    {
        VertexShader = compile vs_2_0 VShader();
		PixelShader =  compile ps_2_0 PShaderBlack();

		ShadeMode = FLAT;
		FillMode = Wireframe;
    }
}