﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace voxel2d
{
    public partial class Form1 : Form
    {
        Voxel2D world;
        int maxdepth = 0;

        public Form1()
        {
            //SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint, true);
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            world = new Voxel2D(Properties.Resources.density);
            treeView1.Nodes.Clear();

            if (world.voxeltree.quadtree.root!=null)
                treeView1.Nodes.Add(world.voxeltree.quadtree.root.GetDebugView());
            base.OnLoad(e);
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
           // if (DesignMode) 
                base.OnPaintBackground(e);
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            this.Invalidate();
        }

        protected override void OnResizeEnd(EventArgs e)
        {
            base.OnResizeEnd(e);
            world.SetBackground(panel1.ClientRectangle);
            this.panel1.Invalidate();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.Clear(BackColor);

            System.Drawing.Rectangle zone = panel1.ClientRectangle;

            if (zone.Width > 20) { zone.X += 10; zone.Width -= 20; }
            if (zone.Height > 20) { zone.Y += 10; zone.Height -= 20; }

            e.Graphics.FillRectangle(Brushes.White, zone);

            //world.DrawPolygon(e.Graphics, zone);
            world.DrawSimple(e.Graphics, zone);
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            maxdepth = (int)this.numericUpDown1.Value;
            this.panel1.Invalidate();
        }
    }
}
