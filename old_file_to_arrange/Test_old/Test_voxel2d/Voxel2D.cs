﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;

using Engine.Tools;
using Engine.Maths;
using Engine.Partitions;
using Engine.Geometry;

namespace voxel2d
{
    public class Voxel2D
    {
        Random rnd = new Random();
        
        public VoxelTree2d voxeltree;
        public List<Polygon> polygons;
        public BitArray2D density;


        Bitmap density_bmp;
        Brush fullbrush = new HatchBrush(HatchStyle.DarkUpwardDiagonal, Color.FromArgb(100, 255, 0, 0), Color.FromArgb(100, 255, 255, 255));
        Brush semibrush = new HatchBrush(HatchStyle.DarkDownwardDiagonal, Color.FromArgb(100, 0, 0, 255), Color.FromArgb(100, 255, 255, 255));
        Pen bigblackline = new Pen(Color.Black, 4);
        
        /// <summary>
        /// initialize the density field, is a table of '1' or '0' bit
        /// </summary>
        /// <param name="Width"></param>
        /// <param name="Height"></param>
        public Voxel2D(Bitmap map)
        {
            density_bmp = map;
            density = BitArray2D.LoadFromGrayBitmap(map);

            // find maximum subdivision, until obtain 2x2pixel
            int size = MathsUtils.MIN(map.Width, map.Height);
            int depth = GetDepth(size);

            // number of row and column of density grid
            size = (int)Math.Pow(2, depth - 1) + 1;

            BuildTree();
        }


        public void BuildTree()
        {
            voxeltree = new VoxelTree2d_v1(density);
            polygons = voxeltree.ExtractPolygons();
            return;

            //voxeltree.quadtree.Bound = new RectangleAA2(50, 50, 50, 50);

            int LeftPos = VoxelConst2d.LeftPos;
            int TopPos = VoxelConst2d.TopPos;
            int RightPos = VoxelConst2d.RightPos;
            int BottomPos = VoxelConst2d.BottomPos;

            foreach (KeyValuePair<UInt32, VoxelNode2d> pair in voxeltree.leveladdress[0])
            {
                sbyte[,] indices = VoxelConst2d.edgetable;

                VoxelNode2d current = pair.Value;
                Vector2 center = current.Center;
                Vector2 halfsize = current.HalfSize;

                int vxl = (int)current.voxelcase;

                VoxelBorderType brd = current.bordercase;

                // the special map border cases
                bool hasLeft = (brd & VoxelBorderType.LEFT) != 0 && indices[vxl, LeftPos] < 8;
                bool hasTop = (brd & VoxelBorderType.TOP) != 0 && indices[vxl, TopPos] < 8;
                bool hasRight = (brd & VoxelBorderType.RIGHT) != 0 && indices[vxl, RightPos] < 8;
                bool hasBottom = (brd & VoxelBorderType.BOTTOM) != 0 && indices[vxl, BottomPos] < 8;

                // link the neighbour cells
                voxeltree.leveladdress[0].TryGetValue(voxeltree.quadtree.GetTileHash(current.tilecoord.x, current.tilecoord.y + 1), out current.Top);
                voxeltree.leveladdress[0].TryGetValue(voxeltree.quadtree.GetTileHash(current.tilecoord.x, current.tilecoord.y - 1), out current.Bottom);
                voxeltree.leveladdress[0].TryGetValue(voxeltree.quadtree.GetTileHash(current.tilecoord.x - 1, current.tilecoord.y), out current.Left);
                voxeltree.leveladdress[0].TryGetValue(voxeltree.quadtree.GetTileHash(current.tilecoord.x + 1, current.tilecoord.y), out current.Right);

                // get a list of only necessary points to use in polygons
                VoxelVertexType flag = 0;

                for (int i = 0; i < 4; i++)
                    if (indices[vxl, i] < 8)
                        flag |= (VoxelVertexType)(1 << indices[vxl, i]);

                if (hasLeft)
                    for (int i = 4; i < 6; i++)
                        if (indices[vxl, i] < 8)
                            flag |= (VoxelVertexType)(1 << indices[vxl, i]);
                if (hasTop)
                    for (int i = 6; i < 8; i++)
                        if (indices[vxl, i] < 8)
                            flag |= (VoxelVertexType)(1 << indices[vxl, i]);
                if (hasRight)
                    for (int i = 8; i < 10; i++)
                        if (indices[vxl, i] < 8)
                            flag |= (VoxelVertexType)(1 << indices[vxl, i]);
                if (hasBottom)
                    for (int i = 10; i < 12; i++)
                        if (indices[vxl, i] < 8)
                            flag |= (VoxelVertexType)(1 << indices[vxl, i]);

                // link found vertices with neightbours and initializate the class if not already done to generate a reference
                if (current.Top != null)
                {
                    current.v[1] = current.Top.v[3];
                    current.v[6] = current.Top.v[4];
                    current.v[7] = current.Top.v[5];
                    if (current.v[1] == null && (flag & VoxelVertexType.V1) != 0) current.Top.v[3] = current.v[1] = new VoxelPoint(center + halfsize * VoxelConst2d.verts[1]);
                    if (current.v[6] == null && (flag & VoxelVertexType.V6) != 0) current.Top.v[4] = current.v[6] = new VoxelPoint(center + halfsize * VoxelConst2d.verts[6]);
                    if (current.v[7] == null && (flag & VoxelVertexType.V7) != 0) current.Top.v[5] = current.v[7] = new VoxelPoint(center + halfsize * VoxelConst2d.verts[7]);
                }
                if (current.Bottom != null)
                {
                    current.v[3] = current.Bottom.v[1];
                    current.v[4] = current.Bottom.v[6];
                    current.v[5] = current.Bottom.v[7];
                    if (current.v[3] == null && (flag & VoxelVertexType.V3) != 0) current.Bottom.v[1] = current.v[3] = new VoxelPoint(center + halfsize * VoxelConst2d.verts[3]);
                    if (current.v[4] == null && (flag & VoxelVertexType.V4) != 0) current.Bottom.v[6] = current.v[4] = new VoxelPoint(center + halfsize * VoxelConst2d.verts[4]);
                    if (current.v[5] == null && (flag & VoxelVertexType.V5) != 0) current.Bottom.v[7] = current.v[5] = new VoxelPoint(center + halfsize * VoxelConst2d.verts[5]);
                }
                if (current.Left != null)
                {
                    current.v[0] = current.Left.v[2];
                    current.v[6] = current.Left.v[7];
                    current.v[4] = current.Left.v[5];
                    if (current.v[0] == null && (flag & VoxelVertexType.V0) != 0) current.Left.v[2] = current.v[0] = new VoxelPoint(center + halfsize * VoxelConst2d.verts[0]);
                    if (current.v[6] == null && (flag & VoxelVertexType.V6) != 0) current.Left.v[7] = current.v[6] = new VoxelPoint(center + halfsize * VoxelConst2d.verts[6]);
                    if (current.v[4] == null && (flag & VoxelVertexType.V4) != 0) current.Left.v[5] = current.v[4] = new VoxelPoint(center + halfsize * VoxelConst2d.verts[7]);
                }
                if (current.Right != null)
                {
                    current.v[2] = current.Right.v[0];
                    current.v[7] = current.Right.v[6];
                    current.v[5] = current.Right.v[4];
                    if (current.v[2] == null && (flag & VoxelVertexType.V2) != 0) current.Right.v[0] = current.v[2] = new VoxelPoint(center + halfsize * VoxelConst2d.verts[2]);
                    if (current.v[5] == null && (flag & VoxelVertexType.V5) != 0) current.Right.v[6] = current.v[5] = new VoxelPoint(center + halfsize * VoxelConst2d.verts[5]);
                    if (current.v[7] == null && (flag & VoxelVertexType.V7) != 0) current.Right.v[4] = current.v[7] = new VoxelPoint(center + halfsize * VoxelConst2d.verts[7]);
                }

                // link the default voxel lines
                if (indices[vxl, 0] < 8) makeEdge(current, indices[vxl, 0], indices[vxl, 1]);
                if (indices[vxl, 2] < 8) makeEdge(current, indices[vxl, 2], indices[vxl, 3]);

                // link the two border vertices
                if (hasLeft)
                {
                    makeEdge(current, indices[vxl, LeftPos], indices[vxl, LeftPos + 1]);
                }
                if (hasTop)
                {
                    makeEdge(current, indices[vxl, TopPos], indices[vxl, TopPos + 1]);
                }
                if (hasRight)
                {
                    makeEdge(current, indices[vxl, RightPos], indices[vxl, RightPos + 1]);
                }
                if (hasBottom)
                {
                    makeEdge(current, indices[vxl, BottomPos], indices[vxl, BottomPos + 1]);
                }
            }

            // Now all polygons have a continuos chain of vertices. Some vertices are created in neigbourg node linking but not used,
            // so check if 'next' value is not null.
            // Extract them.
            polygons = new List<Polygon>();

            int count = 0;

             foreach (KeyValuePair<UInt32, VoxelNode2d> pair in voxeltree.leveladdress[0])
             {
                 VoxelNode2d node = pair.Value;
                for (int v = 0; v < 8; v++)
                {
                    VoxelPoint start = node.v[v];

                    if (start != null && !start.processed && start.next != null)
                    {
                        Polygon poly = new Polygon();
                        VoxelPoint curr = start;
                        do
                        {
                            if (curr.processed) throw new Exception("possible intersection ?");
                            curr.processed = true;
                            poly.vertices.Add(curr.value);
                            curr = curr.next;
                        }
                        while (curr != null && curr != start);
                        //poly.Add(start);

                        count += poly.vertices.Count;
                        polygons.Add(poly);
                    }
                }
            }

            Console.WriteLine("Point Instances Created : " + VoxelPoint.idcounter);
            Console.WriteLine("Point Instances Needed : " + count);
        }

        void makeEdge(VoxelNode2d node, int curr, int next)
        {
            // for borders case is possible that some points are not initialized because there aren't a neighbours
            if (node.v[curr] == null) node.v[curr] = new VoxelPoint(node.Center + node.HalfSize * VoxelConst2d.verts[curr]);
            if (node.v[next] == null) node.v[next] = new VoxelPoint(node.Center + node.HalfSize * VoxelConst2d.verts[next]);

            node.v[curr].next = node.v[next];
            node.v[next].prev = node.v[curr];
        }

        /// <summary>
        /// </summary>
        /// <remarks>
        /// S = min(W,H)
        /// D0 : s 0 1,
        /// D1 : s 2,
        /// D2 : s 3 4,
        /// D3 : s 5 6 7 8,
        /// D4 : s 9...
        /// </remarks>
        public int GetDepth(int size)
        {
            int d = 0;
            while (size > 1) { size = (size + 1) / 2; d++; }
            return d;
        }


        int X0, Y0, X1, Y1;
        float deltax, deltay;
        float xmin, ymin;

        int X2Screen(float x)
        {
            return (int)(X0 + (x - xmin) / deltax);
        }
        int Y2Screen(float y)
        {
            return (int)(Y1 - (y - ymin) / deltay);
        }

        void SetBounds(System.Drawing.Rectangle size)
        {
            X0 = size.X;
            Y0 = size.Y;
            X1 = X0 + size.Width;
            Y1 = Y0 + size.Height;
            deltax = (voxeltree.quadtree.root.HalfSize.x * 2) / (X1 - X0);
            deltay = (voxeltree.quadtree.root.HalfSize.y * 2) / (Y1 - Y0);
            xmin = voxeltree.quadtree.root.Min.x;
            ymin = voxeltree.quadtree.root.Min.y;
        }

        List<Point> PolygonByCase(VoxelNode2d node)
        {
            int vxl = (int)(node.voxelcase);

            List<Point> pointlist = new List<Point>();

            for (int i = 0; i < 4; i++)
            {
                int index = VoxelConst2d.edgetable[vxl, i];

                if (index < 8)
                {
                    Vector2 vertex = node.Center + node.HalfSize * VoxelConst2d.verts[index];
                    pointlist.Add(new Point(X2Screen(vertex.x), Y2Screen(vertex.y)));
                }
            }

            return pointlist;
        }


        public void DrawPolygon(Graphics g, System.Drawing.Rectangle size)
        {
            System.Drawing.Rectangle source = new System.Drawing.Rectangle(0, 0, density_bmp.Width, density_bmp.Height);

            SetBounds(size);
            SetBackground(size);

            g.InterpolationMode = InterpolationMode.NearestNeighbor;
            g.DrawImage(density_bmp, size);

            QuadNodesEnumerator<VoxelNode2d> collector = new QuadNodesEnumerator<VoxelNode2d>(voxeltree.quadtree.root);

            foreach (VoxelNode2d node in collector)
            {
                int x0 = X2Screen(node.Min.x);
                int x1 = X2Screen(node.Max.x);
                int y0 = Y2Screen(node.Min.y);
                int y1 = Y2Screen(node.Max.y);

                if (node.IsLeaf)
                {
                    List<Point> polygons = PolygonByCase(node);

                    //if (node.voxelcase == QuadIdx.FULL) throw new Exception("A full density node must not exist");
                    if (node.voxelcase == QuadIndex.None) throw new Exception("A empty density node must not exist");

                    g.DrawString(node.nodeid.ToString(), SystemFonts.DefaultFont, Brushes.Black, (x0 + x1) / 2, (y0 + y1) / 2);

                    g.FillPolygon(semibrush, polygons.ToArray());

                    g.DrawRectangle(Pens.Blue, x0, y1, x1 - x0, y0 - y1);
                }
                else
                {
                    g.DrawRectangle(Pens.Gray, x0, y1, x1 - x0, y0 - y1);
                }

            }
        }

        public void DrawSimple(Graphics g, System.Drawing.Rectangle size)
        {
            System.Drawing.Rectangle source = new System.Drawing.Rectangle(0, 0, density_bmp.Width, density_bmp.Height);

            SetBounds(size);
            SetBackground(size);

            g.InterpolationMode = InterpolationMode.NearestNeighbor;
            g.DrawImage(density_bmp, size);

            QuadNodesEnumerator<VoxelNode2d> collector = new QuadNodesEnumerator<VoxelNode2d>(voxeltree.quadtree.root);
            
            foreach (VoxelNode2d node in collector)
            {
                int x0 = X2Screen(node.Min.x);
                int x1 = X2Screen(node.Max.x);
                int y0 = Y2Screen(node.Min.y);
                int y1 = Y2Screen(node.Max.y);

                if (node.IsLeaf)
                {
                    g.DrawString('n' + node.nodeid.ToString(), SystemFonts.DefaultFont, Brushes.Black, (x0 + x1) / 2, (y0 + y1) / 2);
                    g.DrawRectangle(Pens.Blue, x0, y1, x1 - x0, y0 - y1);
                }
                else
                {
                    //g.DrawRectangle(Pens.Gray, x0, y1, x1 - x0, y0 - y1);
                }

            }

            foreach (Polygon poly in polygons)
            {
                Point[] list = new Point[poly.vertices.Count];
                Pen bigline = new Pen(Color.FromArgb(rnd.Next(25,255), rnd.Next(25,255), rnd.Next(25,255)), 4);
                Pen smalline = new Pen(bigline.Color, 2);

                for (int i = 0; i < list.Length; i++)
                {
                    Vector2 v = poly.vertices[i];
                    int x = X2Screen(v.x);
                    int y = Y2Screen(v.y);
                    list[i] = new Point(x, y);

                    x += rnd.Next() % 8;
                    y += rnd.Next() % 8;

                    g.DrawString("P" + i.ToString(), SystemFonts.DefaultFont, Brushes.Black, x, y);
                }

                g.DrawLines(bigline, list);
                g.DrawLines(smalline, new Point[] { list[list.Length - 1], list[0] });
            }


        }


        public void SetBackground(System.Drawing.Rectangle size)
        {
            int W = size.Width;
            int H = size.Height;
            int w = density.Width;
            int h = density.Heigth;

            float pw = (float)size.Width / (density.Width - 1);
            float ph = (float)size.Height / (density.Heigth - 1);
            int semiw = (int)(pw / 2);
            int semih = (int)(ph / 2);


            density_bmp = new Bitmap(size.Width, size.Height);

            Graphics g = Graphics.FromImage(density_bmp);

            for (int y = 0; y < h; y++)
                g.DrawLine(Pens.LightGray, 0, y * ph - semih, W - 1, y * ph - semih);
            for (int x = 0; x < w; x++)
                g.DrawLine(Pens.LightGray, x * pw - semiw, 0, x * pw - semiw, H - 1);

            SolidBrush bush = new SolidBrush(Color.FromArgb(50, 255, 0, 0));

            for (int y = 0; y < h; y++)
                for (int x = 0; x < w; x++)
                {
                    if (density[x, y])
                    {
                        float xi = x * pw - semiw;
                        float yi = (h - y - 1) * ph - semih;

                        g.FillRectangle(bush, xi, yi, pw, ph);
                        //g.FillEllipse(bush, xi + semiw*0.5f, yi + semih*0.5f, semiw, semih);
                    }
                }

        }

        int clamp(float value, int max)
        {
            return value < 0 ? 0 : value > max ? max : (int)value;
        }


        public List<Point[]> GetPolygons(VoxelTree2d tree)
        {
            List<Point[]> polygons = new List<Point[]>();


            QuadNodesEnumerator<VoxelNode2d> collector = new QuadNodesEnumerator<VoxelNode2d>(voxeltree.quadtree.root);

            foreach (VoxelNode2d node in collector)
            {
                int x0 = X2Screen(node.Min.x);
                int x1 = X2Screen(node.Max.x);
                int y0 = Y2Screen(node.Min.y);
                int y1 = Y2Screen(node.Max.y);

                if (node.child == null)
                {
                    if (node.voxelcase == QuadIndex.All) throw new Exception("A full density node must not exist");
                    if (node.voxelcase == QuadIndex.None) throw new Exception("A empty density node must not exist");
                }

            }

            return polygons;
        }
    }
}
