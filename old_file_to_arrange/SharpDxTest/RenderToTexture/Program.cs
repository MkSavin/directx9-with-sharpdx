using System;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.InteropServices;

using SharpDX;
using SharpDX.Direct3D9;

using Color = SharpDX.Color;



namespace DX9Sample
{
    public class DX9Form : Form
    {
        Device device;
        PresentParameters param;
        AxisLines axis;
        QuadMesh quad;

        TextureRender textureRenderer;

        static void Main()
        {
            using (DX9Form form = new DX9Form())
            {
                form.Show();
                form.Init();
                Application.Run(form);
            }
        }
        public DX9Form()
        {
            this.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.Opaque, true);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            System.Threading.Thread.Sleep(10);

            if (WindowState != FormWindowState.Minimized)
            {
                this.Render2Texture();
                this.Render();
                this.Invalidate();
            }
        }

        private void Init()
        {
            CreateFlags flags = CreateFlags.HardwareVertexProcessing;

            param = new PresentParameters();
            param.BackBufferFormat = Format.Unknown;
            param.SwapEffect = SwapEffect.Discard;
            param.Windowed = true;
            param.EnableAutoDepthStencil = true;
            param.AutoDepthStencilFormat = Format.D16;
            param.PresentationInterval = PresentInterval.Immediate;

            Direct3D d3d = new Direct3D();
            device = new Device(d3d, 0, DeviceType.Hardware, this.Handle, flags, param);

            device.Disposing += OnLostDevice;
            device.Disposed += OnResetDevice;

            axis = new AxisLines(device);
            quad = new QuadMesh(device, false);

            textureRenderer = new TextureRender(device, ClientSize.Width / 10, ClientSize.Height / 10, PixelFormat.X8R8G8B8, DepthFormat.D24X8);

            OnResetDevice(device, new DeviceEventArgs("manual"));
        }

        /// <summary>
        /// Render the axis object to texture
        /// </summary>
        void Render2Texture()
        {
            Result result = device.TestCooperativeLevel();
            if (!device.TestCooperativeLevel().Success) { Console.WriteLine("device not ready to draw " + result.ToString()); return; }

            textureRenderer.BeginScene();

            device.SetTexture(0, null);
            axis.Draw();// a random object

            textureRenderer.EndScene();
        }

        /// <summary>
        /// Render the previous scene to quad faces
        /// </summary>
        void Render()
        {
            Result result = device.TestCooperativeLevel();
            if (!device.TestCooperativeLevel().Success) { Console.WriteLine("device not ready to draw " + result.ToString()); return; }


            device.Clear(ClearFlags.Target | ClearFlags.ZBuffer, ColorBGRA.FromRgba(Color.CornflowerBlue.ToRgba()), 1.0f, 0);
            device.BeginScene();

            device.SetTexture(0, null);
            axis.Draw(); // just to see the coorndinate positions

            if (textureRenderer != null)
                device.SetTexture(0, textureRenderer.Targhet);
            quad.Draw();

            device.EndScene();
            device.Present();
        }


        /// <summary>
        /// Reset only when resize end
        /// </summary>
        protected override void OnResizeEnd(EventArgs e)
        {
            base.OnResizeEnd(e);
            //device.Dispose();
            ResetDevice();
        }

        void OnLostDevice(object sender, EventArgs e)
        {
            Device device = (Device)sender;
            Console.WriteLine("lost event: " + e.ToString());
        }

        void OnResetDevice(object sender, EventArgs e)
        {
            Device device = (Device)sender;
            Console.WriteLine("reset event : " + e.ToString());

            device.SetRenderState(RenderState.CullMode, Cull.None);
            device.SetTransform(TransformState.Projection, Matrix.PerspectiveFovLH((float)Math.PI / 4, (float)ClientSize.Width / ClientSize.Height, 0.1f, 100.0f));
            device.SetTransform(TransformState.View, Matrix.LookAtLH(new Vector3(2, 2, 2), new Vector3(0, 0, 0), new Vector3(0, 1, 0)));


            device.SetRenderState(RenderState.CullMode, Cull.None);
            device.SetRenderState(RenderState.ZEnable, true);
            device.SetRenderState(RenderState.Lighting, false);
        }

        void ResetDevice()
        {
            if (!quad.Managed) quad.Dispose();
            if (textureRenderer != null) textureRenderer.Dispose();

            OnLostDevice(device, new DeviceEventArgs("manual"));

            device.Viewport = new Viewport(0, 0, ClientSize.Width, ClientSize.Height);
            param.BackBufferWidth = ClientSize.Width;
            param.BackBufferHeight = ClientSize.Height;

            device.Reset(param);

            OnResetDevice(device, new DeviceEventArgs("manual"));

            if (!quad.Managed) quad.Restore();
            if (textureRenderer != null) textureRenderer.Restore();
        }

        protected override void Dispose(bool disposing)
        {
            //device.Dispose(); generate Lost+Reset ???
            base.Dispose(disposing);
        }

        class DeviceEventArgs : EventArgs
        {
            string value = "none";
            public DeviceEventArgs(string value)
            {
                this.value = value;
            }
            public override string ToString()
            {
                return value;
            }
        }
    }
}
