﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

using SharpDX;
using SharpDX.Direct3D9;

using Color = SharpDX.Color;


namespace DX9Sample
{
    public interface IRestorable
    {
        void Restore();
    }


    public class AxisLines : IDisposable
    {
        Device device;
        VertexBuffer vbuffer;
        VertexDeclaration cvertex;

        public AxisLines(Device device)
        {
            this.device = device;

            cvertex = new VertexDeclaration(device, CVertex.elements);

            CVertex[] vertices = new CVertex[]
            {
                new CVertex(1,0,0,Color.Red),
                new CVertex(0,0,0,Color.Red),
                new CVertex(0,1,0,Color.Green),
                new CVertex(0,0,0,Color.Green),
                new CVertex(0,0,1,Color.Blue),
                new CVertex(0,0,0,Color.Blue)
            };

            vbuffer = new VertexBuffer(device, vertices.Length * CVertex.stride, Usage.None, VertexFormat.None, Pool.Managed);
            DataStream stream = vbuffer.Lock(0, vertices.Length * CVertex.stride, LockFlags.None);
            stream.WriteRange<CVertex>(vertices);
            vbuffer.Unlock();
        }

        public void Draw()
        {
            device.VertexDeclaration = cvertex;
            device.SetStreamSource(0, vbuffer, 0, CVertex.stride);
            device.DrawPrimitives(PrimitiveType.LineList, 0, 3);
        }

        public void Dispose()
        {
            vbuffer.Dispose();
            cvertex.Dispose();
        }
    }

    public class QuadMesh : IDisposable, IRestorable
    {
        Device device;
        VertexBuffer vbuffer;
        IndexBuffer ibuffer;
        VertexDeclaration tvertex;

        public bool Managed { get; private set; }

        public QuadMesh(Device device , bool managed)
        {
            this.device = device;
            Managed = managed;
            InitBuffer();
        }

        public void Draw()
        {
            device.VertexDeclaration = tvertex;
            device.SetStreamSource(0, vbuffer, 0, TVertex.stride);
            device.Indices = ibuffer;
            device.DrawIndexedPrimitive(PrimitiveType.TriangleList, 0, 0, 4, 0, 2);
        }

        public void Dispose()
        {
            vbuffer.Dispose();
            ibuffer.Dispose();
            tvertex.Dispose();
        }


        void InitBuffer()
        {
            Usage usage = Usage.WriteOnly;
            Pool memory  = Pool.Default;

            if (Managed)
            {
                usage = Usage.None;
                memory = Pool.Managed;
            }

            tvertex = new VertexDeclaration(device, TVertex.elements);

            TVertex[] vertices = new TVertex[]
            {
                new TVertex(1,0,0, 1,0),
                new TVertex(1,1,0, 0,0),
                new TVertex(0,1,0, 0,1),
                new TVertex(0,0,0, 1,1)
            };

            ushort[] indices = new ushort[] { 0, 1, 2, 0, 2, 3 };

            vbuffer = new VertexBuffer(device, vertices.Length * TVertex.stride, usage, VertexFormat.None, memory);
            DataStream stream = vbuffer.Lock(0, vertices.Length * TVertex.stride, LockFlags.None);
            stream.WriteRange<TVertex>(vertices);
            vbuffer.Unlock();

            ibuffer = new IndexBuffer(device, sizeof(ushort) * indices.Length, usage, memory, true);
            DataStream istream = ibuffer.Lock(0, sizeof(ushort) * indices.Length, LockFlags.None);
            istream.WriteRange<ushort>(indices);
            ibuffer.Unlock();
        }

        public void Restore()
        {
            if (!Managed) InitBuffer();
        }
    }

    [StructLayout(LayoutKind.Sequential, Pack = 4, Size = 20)]
    struct TVertex
    {
        public Vector3 pos;
        public Vector2 tex;

        public TVertex(float x, float y, float z,float u,float v)
        {
            this.pos = new Vector3(x, y, z);
            this.tex = new Vector2(u, v);
        }

        public static int stride = Marshal.SizeOf(typeof(TVertex));

        public static readonly VertexElement[] elements = new VertexElement[]
        {
            new VertexElement(0,0,DeclarationType.Float3,DeclarationMethod.Default,DeclarationUsage.Position,0),
            new VertexElement(0,12,DeclarationType.Float2,DeclarationMethod.Default,DeclarationUsage.TextureCoordinate,0),
            VertexElement.VertexDeclarationEnd
        };
    }

    [StructLayout(LayoutKind.Sequential, Pack = 4, Size = 16)]
    struct CVertex
    {
        public Vector3 pos;
        public int color;

        public CVertex(float x, float y, float z, Color color)
        {
            this.pos = new Vector3(x, y, z);
            this.color = color.ToRgba();
        }

        public static int stride = Marshal.SizeOf(typeof(CVertex));

        public static readonly VertexElement[] elements = new VertexElement[]
        {
            new VertexElement(0,0,DeclarationType.Float3,DeclarationMethod.Default,DeclarationUsage.Position,0),
            new VertexElement(0,12,DeclarationType.Color,DeclarationMethod.Default,DeclarationUsage.Color,0),
            VertexElement.VertexDeclarationEnd
        };
    }
}
