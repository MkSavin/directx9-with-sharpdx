﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using SharpDX;
using SharpDX.Direct3D9;

namespace DX9Sample
{
    public partial class RenderPanel : Control
    {
        public int RenderCount = 0;
        /// <summary>
        /// The main device, same for all windows
        /// </summary>
        public MainRender renderDevice { get; private set; }
        /// <summary>
        /// If is a swapchain, this is the additional renderer
        /// </summary>
        SwapRender renderSwap = null;
        /// <summary>
        /// The generic render used to draw on this control
        /// </summary>
        public RenderTarghet render { get; private set; }
       
        public Device device
        {
            get { return render.device; }
        }

        public delegate void RenderPanelDrawFunction(RenderPanel sender);
        public RenderPanelDrawFunction DrawFunction = null;

        public RenderPanel()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initialize the panel graphics as main render windows, the device use this control's handle
        /// </summary>
        public void InitGraphics()
        {
            if (!DesignMode)
            {
                render = renderDevice = new MainRender(this);
                renderSwap = null;
                render.projection = Matrix.PerspectiveFovLH((float)Math.PI / 4, (float)ClientSize.Width / ClientSize.Height, 0.1f, 1000.0f);
            }

        }

        /// <summary>
        /// Initialize a device-dependent graphics as swapchain render windows, the device don't use this control's handle
        /// </summary>
        public void InitGraphics(MainRender main)
        {   
            // very important setting
            if (!DesignMode)
            {
                renderDevice = main;
                render = renderSwap = new SwapRender(this, main);
                render.projection = Matrix.PerspectiveFovLH((float)Math.PI / 4, (float)ClientSize.Width / ClientSize.Height, 0.1f, 1000.0f);
            }
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            return;
        }

        protected override void OnResize(EventArgs eventargs)
        {
            base.OnResize(eventargs);

            if (render != null && render.IsEnabled)
            {
                render.projection = Matrix.PerspectiveFovLH((float)Math.PI / 4, render.viewport.AspectRatio, 0.1f, 1000.0f);
                render.Resize(FormTool.GetViewport(ClientSize));
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            if (render != null && render.IsEnabled)
            {
                RenderCount++;
                if (DrawFunction != null)
                {
                    Result result = device.TestCooperativeLevel();
                    
                    if (result.Success)
                    {
                        render.SetTarghet();
                        render.Clear(this.BackColor);

                        render.BeginScene();
                        
                        DrawFunction(this);

                        render.EndScene();
                        render.Present();
                    }
                    else
                    {
                        throw new Exception(result.Code.ToString());
                    }
                }
            }
            else
            {
                e.Graphics.Clear(BackColor);
                e.Graphics.DrawString("DX9-render-panel", SystemFonts.CaptionFont, Brushes.Black, ClientSize.Width / 4, ClientSize.Height / 2);
            }
        }
    }
}
