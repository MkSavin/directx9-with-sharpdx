﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

using SharpDX;
using SharpDX.Direct3D9;

using Color = SharpDX.Color;

namespace DX9Sample
{
    public enum DepthFormat
    {
        Unknown = 0,
        D16 = 80,
        D24X8 = 77,
        D24S8 = 75,
        D32 = 71,
    }
    public enum PixelFormat
    {
        Unknown = 0,
        A8R8G8B8 = 21,
        X8R8G8B8 = 22,
    }


    public class TextureRender : IResourceManaged, IRenderer
    {
        MainRender render;
        Device device { get { return render.device; } }

        Viewport targhetViewport;
        Texture targhetTexture;
        Surface targhetSurface;
        Format targhetFormat;
        RenderToSurface renderSurface;

        Format depthFormat;
        bool useDepth;
        bool useStencil;

        int Width, Height;



        public TextureRender(MainRender render, int width, int height, PixelFormat textureformat, DepthFormat depthstencilformat)
        {
            this.render = render;
            this.targhetFormat = (Format)textureformat;
            this.depthFormat = (Format)depthstencilformat;
            this.useDepth = depthstencilformat != DepthFormat.Unknown;
            this.useStencil = depthstencilformat == DepthFormat.D24S8;
            this.Width = width < 0 ? 1 : width;
            this.Height = height < 0 ? 1 : height;

            this.targhetViewport = new Viewport(0, 0, Width, Height);

            render.unManagedResources.Add(this);

            InitSurfaces();
        }

        ~TextureRender()
        {
            Destroy();
        }

        public Texture Targhet 
        { 
            get { return targhetTexture; } 
        }
        

        public void BeginScene()
        {
            ClearFlags flag = ClearFlags.Target;
            if (useDepth) flag |= ClearFlags.ZBuffer;
            if (useStencil) flag |= ClearFlags.Stencil;

            renderSurface.BeginScene(targhetSurface, targhetViewport);         
            device.Clear(flag, ColorBGRA.FromRgba(Color.Gray.ToRgba()), 1.0f, 0);
        }

        public void EndScene()
        {
            renderSurface.EndScene(Filter.None);
        }

        void InitSurfaces()
        {
            targhetTexture = new Texture(device, Width, Height, 1, Usage.RenderTarget, targhetFormat, Pool.Default);
            targhetSurface = targhetTexture.GetSurfaceLevel(0);
            renderSurface = new RenderToSurface(device, Width, Height, targhetFormat, useDepth, depthFormat);
            IsDisposed = false;
        }


        #region IResourceManaged
        
        public void Dispose()
        {
            if (targhetTexture != null) targhetTexture.Dispose();
            if (targhetSurface != null) targhetSurface.Dispose();
            if (renderSurface != null) renderSurface.Dispose();
            targhetTexture = null;
            targhetSurface = null;
            renderSurface = null;
            IsDisposed = true;
        }

        public void DeviceLost()
        {
            Dispose();
        }

        public void DeviceRestore()
        {
            InitSurfaces();
        }

        public void Destroy()
        {
            Dispose();
            if (render.unManagedResources.Contains(this)) render.unManagedResources.Remove(this);
        }

        public bool IsEnabled { get; set; }

        public bool IsDisposed { get; private set; }
        #endregion
    }
}
