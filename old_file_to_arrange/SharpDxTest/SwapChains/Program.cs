using System;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Collections.Generic;

using SharpDX;
using SharpDX.Direct3D9;

namespace DX9Sample
{
    public partial class DX9Form : Form
    {
        Device device { get { return mainrender.device; } }
        MainRender mainrender;
        TextureRender texrender;
        AxisLines axis;
        QuadMesh quad;

        List<RenderPanel> renderPanels;

        float yangle = 0;

        static void Main()
        {
            using (DX9Form form = new DX9Form())
            {
                form.Show();
                form.Init();
                Application.Run(form);
            }
        }
        public DX9Form()
        {
            this.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.Opaque, true);
            this.InitializeComponent();

            renderPanels = new List<RenderPanel>();
            renderPanels.Add(renderPanel1);
            renderPanels.Add(renderPanel2);
            renderPanels.Add(renderPanel3);
            renderPanels.Add(renderPanel4);

        }
        private void Init()
        {
            if (DesignMode) return;

            //mainrender = new MainRender(this);
            //mainrender.view = Matrix.LookAtLH(new Vector3(2, 2, 2), new Vector3(0, 0, 0), new Vector3(0, 1, 0));
            //mainrender.projection = Matrix.PerspectiveFovLH((float)Math.PI / 4, mainrender.viewport.AspectRatio, 0.1f, 1000.0f);

            renderPanel1.InitGraphics();
            mainrender = renderPanel1.renderDevice;
            renderPanel2.InitGraphics(mainrender);
            renderPanel3.InitGraphics(mainrender);
            renderPanel4.InitGraphics(mainrender);

            renderPanel1.render.view = Matrix.LookAtLH(new Vector3(2, 2, 2), new Vector3(0, 0, 0), new Vector3(0, 1, 0));
            renderPanel2.render.view = Matrix.LookAtLH(new Vector3(-2, 2, 2), new Vector3(0, 0, 0), new Vector3(0, 1, 0));
            renderPanel3.render.view = Matrix.LookAtLH(new Vector3(2, 2, -2), new Vector3(0, 0, 0), new Vector3(0, 1, 0));
            renderPanel4.render.view = Matrix.LookAtLH(new Vector3(-2, 2, -2), new Vector3(0, 0, 0), new Vector3(0, 1, 0));

            renderPanel1.DrawFunction = new RenderPanel.RenderPanelDrawFunction(DrawScene);
            renderPanel2.DrawFunction = new RenderPanel.RenderPanelDrawFunction(DrawScene);
            renderPanel3.DrawFunction = new RenderPanel.RenderPanelDrawFunction(DrawScene); 
            renderPanel4.DrawFunction = new RenderPanel.RenderPanelDrawFunction(DrawScene);

            axis = new AxisLines(mainrender);
            quad = new QuadMesh(mainrender, false);
            texrender = new TextureRender(mainrender, 100, 100, PixelFormat.X8R8G8B8, DepthFormat.D16);

            //this.Invalidate();
        }

        void DrawScene(RenderPanel sender)
        {
            System.Threading.Thread.Sleep(10);

            sender.render.view = Matrix.RotationY(yangle += 0.001f) * sender.render.view;

            device.SetTransform(TransformState.View, sender.render.view);
            device.SetTransform(TransformState.Projection, sender.render.projection);
            device.SetRenderState(RenderState.CullMode, Cull.None);
            device.SetRenderState(RenderState.ZEnable, true);
            device.SetRenderState(RenderState.Lighting, false);

            axis.Draw();
        }

        protected override void OnResize(EventArgs eventargs)
        {
            base.OnResize(eventargs);
            return;

            if (mainrender != null && mainrender.IsEnabled)
            {
                mainrender.projection = Matrix.PerspectiveFovLH((float)Math.PI / 4, mainrender.viewport.AspectRatio, 0.1f, 1000.0f);
            }
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            return;
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            e.Graphics.Clear(BackColor);

            /*
            if (WindowState == FormWindowState.Minimized) return;

            if (mainrender != null && mainrender.IsEnabled)
            {
                System.Threading.Thread.Sleep(10);

                Result result = device.TestCooperativeLevel();

                if (result.Success)
                {
                    device.SetTransform(TransformState.View, mainrender.view);
                    device.SetTransform(TransformState.Projection, mainrender.projection);
                    device.SetRenderState(RenderState.CullMode, Cull.None);
                    device.SetRenderState(RenderState.ZEnable, true);
                    device.SetRenderState(RenderState.Lighting, false);

                    device.SetTexture(0, null);
                    
                    texrender.BeginScene();
                    axis.Draw();
                    texrender.EndScene();

                    mainrender.SetTarghet();
                    mainrender.Clear(this.BackColor);

                    mainrender.BeginScene();

                    device.SetTexture(0, texrender.Targhet);
                    quad.Draw();
                    
                    mainrender.EndScene();
                    mainrender.Present();
                }
                else
                {
                    Console.WriteLine(result.ToString());
                }
            }
            else
            {
                e.Graphics.Clear(BackColor);
                e.Graphics.DrawString("DX9-render-form", SystemFonts.CaptionFont, Brushes.Black, ClientSize.Width / 4, ClientSize.Height / 2);
            }
            */
        }

    }
}
