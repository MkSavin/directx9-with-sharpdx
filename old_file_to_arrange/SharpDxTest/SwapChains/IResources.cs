﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DX9Sample
{
    public interface IRenderer
    {
        void BeginScene();
        void EndScene();

    }
    public interface IResourceManaged : IDisposable
    {
        /// <summary>
        /// destructor implementation
        /// </summary>
        void Destroy();
        /// <summary>
        /// when device are lost do some disposes 
        /// </summary>
        void DeviceLost();
        /// <summary>
        /// after device was reseted, recreate if need the resources
        /// </summary>
        void DeviceRestore();
        /// <summary>
        /// Tell if resource are currently disabled or not, 
        /// </summary>
        bool IsEnabled { get; set; }
        /// <summary>
        /// Tell if resource are disposed, so you can check if need recreate them
        /// </summary>
        bool IsDisposed { get; }
    }
}
