﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Windows.Forms;

using SharpDX;
using SharpDX.Direct3D9;




namespace DX9Sample
{

    public abstract class RenderTarghet : IResourceManaged, IRenderer
    {
        public Device device { get; private set; }
        public Viewport viewport { get; protected set; }
        public Matrix projection;
        public Matrix view;

        protected Control control;
        protected PresentParameters param;
        protected Surface backBuffer;
        protected Surface depthBuffer;
        protected bool isSwapChain = false;
        protected SwapChain swapchain;
        protected string name = "dx9rendertarghet";

        /// <summary>
        /// </summary>
        /// <param name="device">if NULL are a swapchain</param>
        protected RenderTarghet(Control control, Viewport viewport, RenderTarghet main)
        {
            this.viewport = viewport;
            this.control = control;

            if (main == null)
            {
                this.device = null;
                this.isSwapChain = false;
            }
            else
            {
                this.device = main.device;
                this.isSwapChain = true;
            }

            this.Init();

            this.IsDisposed = false;
            this.IsEnabled = true;
        }

        ~RenderTarghet()
        {
            Destroy();
        }


        void Init()
        {
            int Width = viewport.Width;
            int Height = viewport.Height;
            if (Width == 0 || Height == 0) Width = Height = 1;

            CreateFlags flags = CreateFlags.HardwareVertexProcessing;

            param = new PresentParameters();
            param.BackBufferFormat = Format.X8R8G8B8;
            param.BackBufferWidth = Width;
            param.BackBufferHeight = Height;
            param.SwapEffect = SwapEffect.Discard;
            param.Windowed = true;
            param.EnableAutoDepthStencil = true;
            param.AutoDepthStencilFormat = Format.D16;
            param.PresentationInterval = PresentInterval.Immediate;

            viewport = new Viewport(0, 0, Width, Height);

            if (isSwapChain)
            {
                restoreSwapChainSurf();
            }
            else
            {
                param.DeviceWindowHandle = control.Handle;
                Direct3D d3d = new Direct3D();
                device = new Device(d3d, 0, DeviceType.Hardware, control.Handle, flags, param);
                // After CreateDevice, the first swap chain already exists, so just get it...
                restoreDeviceSurf();
            }
        }

        protected void restoreDeviceSurf()
        {
            swapchain = device.GetSwapChain(0);
            // Store references to buffers for convenience
            backBuffer = device.GetRenderTarget(0);
            depthBuffer = device.DepthStencilSurface;
        }

        protected void restoreSwapChainSurf()
        {
            swapchain = new SwapChain(device, param);
            backBuffer = swapchain.GetBackBuffer(0);
            // Additional swap chains need their own depth buffer to support resizing them
            depthBuffer = Surface.CreateDepthStencil(
                device, viewport.Width, viewport.Height,
                param.AutoDepthStencilFormat,
                param.MultiSampleType,
                param.MultiSampleQuality,
                (param.PresentFlags & PresentFlags.DiscardDepthStencil) != 0);
        }

        public void BeginScene()
        {
            device.BeginScene();
        }
        public void EndScene()
        {
            device.EndScene();
        }

        /// <summary>
        /// The SetTarghet process assign the current backbuffer and depthbuffer to device
        /// </summary>
        public void SetTarghet()
        {
            // backbuffer is same for all swapchains
            device.SetRenderTarget(0, backBuffer);
            // each swapchains have its depth buffer
            device.DepthStencilSurface = depthBuffer;

            device.Viewport = viewport;
        }

        /// <summary>
        /// The Clear process clear the current backbuffer and depthbuffer
        /// </summary>
        public void Clear(System.Drawing.Color backcolor)
        {
            device.Clear(ClearFlags.Target | ClearFlags.ZBuffer, ColorBGRA.FromRgba(backcolor.ToArgb()), 1.0f, 0);
        }

        /// <summary>
        /// The present process send the gpu backbuffer to frontbuffer (monitor)
        /// </summary>
        public abstract void Present();

        public void Resize(Viewport size)
        {
            if (IsEnabled)
            {
                Console.WriteLine(name + " : Resizing");

                if (size.Width < 1) size.Width = 1;
                if (size.Height < 1) size.Height = 1;

                viewport = size;

                param.BackBufferWidth = size.Width;
                param.BackBufferHeight = size.Height;

                DeviceLost();

                //foreach (IResourceManaged resource in unManagedResources) resource.ToString();
                if (!isSwapChain)
                    device.Reset(param);

                DeviceRestore();
            }
        }


        #region IResourceManaged
        public bool IsDisposed { get; protected set; }
        public bool IsEnabled { get; set; }

        public abstract void Destroy();

        public abstract void DeviceLost();

        public abstract void DeviceRestore();

        public void Dispose()
        {
            if (!IsDisposed)
            {
                Console.WriteLine(name + " : Disposing");

                if (swapchain != null) swapchain.Dispose();
                if (depthBuffer != null) depthBuffer.Dispose();
                if (backBuffer != null) backBuffer.Dispose();
                
                swapchain = null;
                depthBuffer = null;
                backBuffer = null;
                IsDisposed = true;
            }
        }
        #endregion


        public override string ToString()
        {
            return name;
        }
    }

    public class MainRender : RenderTarghet
    {
        /// <summary>
        /// The list of all non-managed resources, work when device reseting
        /// </summary>
        public List<IResourceManaged> unManagedResources = new List<IResourceManaged>();

        public MainRender(Control control)
            : base(control, new Viewport(0, 0, control.Width, control.Height), null)
        {
            name = "DX9MainRenderTarghet";
        }


        public override void DeviceLost()
        {
            Console.WriteLine(name + " : DeviceLost");

            foreach (IResourceManaged render in unManagedResources)
            {
                render.DeviceLost();
                // tell to children resources that can not be recreate untill device will be restored
                render.IsEnabled = false;
            }
            Dispose();
        }

        public override void DeviceRestore()
        {

            Console.WriteLine(name + " : DeviceRestore");

            base.restoreDeviceSurf();

            foreach (IResourceManaged render in unManagedResources)
            {
                // tell to children resources that can be recreate because device are restored
                render.IsEnabled = true;
                render.DeviceRestore();
            }

            IsDisposed = false;

        }

        public override void Present()
        {
            Console.WriteLine(name + " : Presenting");
            device.Present();
        }

        public override void Destroy()
        {
            IResourceManaged[] resources = unManagedResources.ToArray();
            foreach (IResourceManaged res in resources) res.Destroy();
            device.Dispose();
        }
    }

    public class SwapRender : RenderTarghet
    {
        MainRender main;

        static int Count = 0;

        public SwapRender(Control control, MainRender main)
            : base(control, new Viewport(0, 0, control.Width, control.Height), main)
        {
            name = "DX9SwapRenderTarghet" + Count++;
            this.main = main;
            main.unManagedResources.Add(this);
        }


        public override void DeviceLost()
        {
            Console.WriteLine(name + " : DeviceLost");
            Dispose();
        }
        
        public override void DeviceRestore()
        {
            Console.WriteLine(name + " : DeviceRestore");
            base.restoreSwapChainSurf();
            IsDisposed = false;
        }

        public override void Present()
        {

            Console.WriteLine(name + " : Presenting");
            // if windowsoverride is null, swapchain use param.DeviceWindowdsHandle
            // sourceRectangle : The area of the back buffer that should be presented. if null use entire region
            // destinationRectangle : The area of the front buffer that should receive the result of the presentation.
            swapchain.Present(SharpDX.Direct3D9.Present.None, Rectangle.Empty, Rectangle.Empty, control.Handle);
        }

        public override void Destroy()
        {
            Dispose();
            if (main.unManagedResources.Contains(this)) main.unManagedResources.Remove(this);
        }
    }

}
