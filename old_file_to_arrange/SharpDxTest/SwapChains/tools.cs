﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Windows.Forms;

using SharpDX;
using SharpDX.Direct3D9;

using Color = SharpDX.Color;
using System.Drawing;


namespace DX9Sample
{
    public abstract class MeshResource : IResourceManaged
    {
        protected MainRender render;
        protected VertexBuffer vbuffer;
        protected IndexBuffer ibuffer;
        protected VertexDeclaration declaration;
        
        public Device device
        {
            get { return render.device; }
        }
        public bool Managed 
        {
            get; private set;
        }
        public bool IsEnabled { get; set; }
        public bool IsDisposed { get; protected set; }

        public MeshResource(MainRender render, bool managed)
        {
            this.render = render;
            Managed = managed;
            if (!Managed) render.unManagedResources.Add(this);
            IsEnabled = true;
            IsDisposed = false;
        }

        ~MeshResource()
        {
            Destroy();
        }

        public void DeviceRestore()
        {
            if (!Managed && IsEnabled) InitBuffers();
        }
        public void DeviceLost()
        {
            if (!Managed) Dispose();

        }
        public void Dispose()
        {
            if (vbuffer!=null) vbuffer.Dispose();
            if (ibuffer!=null) ibuffer.Dispose();
            if (declaration!=null) declaration.Dispose();
            IsDisposed = true;
        }

        public void Destroy()
        {
            if (!Managed && render.unManagedResources.Contains(this)) render.unManagedResources.Remove(this);
            Dispose();
        }

        public abstract void InitBuffers();

        public abstract void Draw();
    }

    public class AxisLines : MeshResource
    {
        public AxisLines(MainRender render):base(render,true)
        {
            InitBuffers();
        }

        public override void InitBuffers()
        {
            declaration = new VertexDeclaration(device, CVertex.elements);

            CVertex[] vertices = new CVertex[]
            {
                new CVertex(1,0,0,Color.Red),
                new CVertex(0,0,0,Color.Red),
                new CVertex(0,1,0,Color.Green),
                new CVertex(0,0,0,Color.Green),
                new CVertex(0,0,1,Color.Blue),
                new CVertex(0,0,0,Color.Blue)
            };

            vbuffer = new VertexBuffer(device, vertices.Length * CVertex.stride, Usage.None, VertexFormat.None, Pool.Managed);
            DataStream stream = vbuffer.Lock(0, vertices.Length * CVertex.stride, LockFlags.None);
            stream.WriteRange<CVertex>(vertices);
            vbuffer.Unlock();
            
            IsEnabled = true;
            IsDisposed = false;
        }


        public override void Draw()
        {
            render.device.VertexDeclaration = declaration;
            render.device.SetStreamSource(0, vbuffer, 0, CVertex.stride);
            render.device.DrawPrimitives(PrimitiveType.LineList, 0, 3);
        }
    }

    public class QuadMesh : MeshResource
    {
        public QuadMesh(MainRender render, bool managed):base(render,managed)
        {
            InitBuffers();
        }

        public override void Draw()
        {
            if (IsEnabled)
            {
                device.VertexDeclaration = declaration;
                device.SetStreamSource(0, vbuffer, 0, TVertex.stride);
                device.Indices = ibuffer;
                device.DrawIndexedPrimitive(PrimitiveType.TriangleList, 0, 0, 4, 0, 2);
            }
        }
        public override void InitBuffers()
        {
            Usage usage = Usage.WriteOnly;
            Pool memory = Pool.Default;

            if (Managed)
            {
                usage = Usage.None;
                memory = Pool.Managed;
            }

            declaration = new VertexDeclaration(device, TVertex.elements);

            TVertex[] vertices = new TVertex[]
            {
                new TVertex(1,0,0, 1,0),
                new TVertex(1,1,0, 0,0),
                new TVertex(0,1,0, 0,1),
                new TVertex(0,0,0, 1,1)
            };

            ushort[] indices = new ushort[] { 0, 1, 2, 0, 2, 3 };

            vbuffer = new VertexBuffer(device, vertices.Length * TVertex.stride, usage, VertexFormat.None, memory);
            DataStream stream = vbuffer.Lock(0, vertices.Length * TVertex.stride, LockFlags.None);
            stream.WriteRange<TVertex>(vertices);
            vbuffer.Unlock();

            ibuffer = new IndexBuffer(device, sizeof(ushort) * indices.Length, usage, memory, true);
            DataStream istream = ibuffer.Lock(0, sizeof(ushort) * indices.Length, LockFlags.None);
            istream.WriteRange<ushort>(indices);
            ibuffer.Unlock();

            IsDisposed = false;
        }
    }

    public static class FormTool
    {
        public static Form GetParentForm(Control ctrl)
        {
            if (ctrl != null)
            {
                if (ctrl is Form)
                    return (Form)ctrl;
                else
                    return GetParentForm(ctrl.Parent);
            }
            return null;
        }
        public static Viewport GetViewport(Size size)
        {
            return new Viewport(0, 0, size.Width, size.Height);
        }
    }

    [StructLayout(LayoutKind.Sequential, Pack = 4, Size = 20)]
    struct TVertex
    {
        public Vector3 pos;
        public Vector2 tex;

        public TVertex(float x, float y, float z,float u,float v)
        {
            this.pos = new Vector3(x, y, z);
            this.tex = new Vector2(u, v);
        }

        public static int stride = Marshal.SizeOf(typeof(TVertex));

        public static readonly VertexElement[] elements = new VertexElement[]
        {
            new VertexElement(0,0,DeclarationType.Float3,DeclarationMethod.Default,DeclarationUsage.Position,0),
            new VertexElement(0,12,DeclarationType.Float2,DeclarationMethod.Default,DeclarationUsage.TextureCoordinate,0),
            VertexElement.VertexDeclarationEnd
        };
    }

    [StructLayout(LayoutKind.Sequential, Pack = 4, Size = 16)]
    struct CVertex
    {
        public Vector3 pos;
        public int color;

        public CVertex(float x, float y, float z, Color color)
        {
            this.pos = new Vector3(x, y, z);
            this.color = color.ToRgba();
        }

        public static int stride = Marshal.SizeOf(typeof(CVertex));

        public static readonly VertexElement[] elements = new VertexElement[]
        {
            new VertexElement(0,0,DeclarationType.Float3,DeclarationMethod.Default,DeclarationUsage.Position,0),
            new VertexElement(0,12,DeclarationType.Color,DeclarationMethod.Default,DeclarationUsage.Color,0),
            VertexElement.VertexDeclarationEnd
        };
    }
}
