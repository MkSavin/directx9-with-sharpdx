﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace DX9Sample
{
    partial class DX9Form
    {
        private void InitializeComponent()
        {
            this.tableLayout = new System.Windows.Forms.TableLayoutPanel();
            this.renderPanel4 = new DX9Sample.RenderPanel();
            this.renderPanel3 = new DX9Sample.RenderPanel();
            this.renderPanel2 = new DX9Sample.RenderPanel();
            this.renderPanel1 = new DX9Sample.RenderPanel();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.tableLayout.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayout
            // 
            this.tableLayout.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.tableLayout.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.tableLayout.ColumnCount = 2;
            this.tableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 46.76871F));
            this.tableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 53.23129F));
            this.tableLayout.Controls.Add(this.renderPanel4, 1, 1);
            this.tableLayout.Controls.Add(this.renderPanel3, 0, 1);
            this.tableLayout.Controls.Add(this.renderPanel2, 1, 0);
            this.tableLayout.Controls.Add(this.renderPanel1, 0, 0);
            this.tableLayout.Location = new System.Drawing.Point(12, 12);
            this.tableLayout.Name = "tableLayout";
            this.tableLayout.RowCount = 2;
            this.tableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 44.83986F));
            this.tableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 55.16014F));
            this.tableLayout.Size = new System.Drawing.Size(372, 480);
            this.tableLayout.TabIndex = 0;
            // 
            // renderPanel4
            // 
            this.renderPanel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(212)))), ((int)(((byte)(212)))));
            this.renderPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.renderPanel4.Location = new System.Drawing.Point(176, 218);
            this.renderPanel4.Name = "renderPanel4";
            this.renderPanel4.Size = new System.Drawing.Size(193, 259);
            this.renderPanel4.TabIndex = 2;
            // 
            // renderPanel3
            // 
            this.renderPanel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(255)))));
            this.renderPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.renderPanel3.Location = new System.Drawing.Point(3, 218);
            this.renderPanel3.Name = "renderPanel3";
            this.renderPanel3.Size = new System.Drawing.Size(167, 259);
            this.renderPanel3.TabIndex = 2;
            // 
            // renderPanel2
            // 
            this.renderPanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(255)))), ((int)(((byte)(170)))));
            this.renderPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.renderPanel2.Location = new System.Drawing.Point(176, 3);
            this.renderPanel2.Name = "renderPanel2";
            this.renderPanel2.Size = new System.Drawing.Size(193, 209);
            this.renderPanel2.TabIndex = 1;
            // 
            // renderPanel1
            // 
            this.renderPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.renderPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.renderPanel1.Location = new System.Drawing.Point(3, 3);
            this.renderPanel1.Name = "renderPanel1";
            this.renderPanel1.Size = new System.Drawing.Size(167, 209);
            this.renderPanel1.TabIndex = 0;
            // 
            // treeView1
            // 
            this.treeView1.Dock = System.Windows.Forms.DockStyle.Right;
            this.treeView1.Location = new System.Drawing.Point(666, 0);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(342, 504);
            this.treeView1.TabIndex = 1;
            // 
            // DX9Form
            // 
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1008, 504);
            this.Controls.Add(this.treeView1);
            this.Controls.Add(this.tableLayout);
            this.Name = "DX9Form";
            this.tableLayout.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        protected override void Dispose(bool disposing)
        {
            //device.Dispose(); generate Lost+Reset ???
            base.Dispose(disposing);
        }

        private TableLayoutPanel tableLayout;
        private TreeView treeView1;
        private RenderPanel renderPanel1;
        private RenderPanel renderPanel2;
        private RenderPanel renderPanel3;
        private RenderPanel renderPanel4;
    }
}
