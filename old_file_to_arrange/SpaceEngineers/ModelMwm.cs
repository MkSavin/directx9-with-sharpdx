﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Runtime.Serialization;

using Engine.Maths;

namespace SpaceEngineers
{
    public interface IBinaryReadWrite
    {
        bool ReadBin(BinaryReader file);
        bool WriteBin(BinaryWriter file);
    }

    public class ModelMwm : IBinaryReadWrite
    {
        string debug;

        /// <summary>
        ///  these functions return -1 if file was correctly processed, otherwise return the last file position for debugging.
        /// </summary>
        public Int64 OpenFile(string filepath)
        {
            Int64 filepos = -1;
            using (BinaryReader file = new BinaryReader(File.OpenRead(filepath)))
            {
                try
                {
                    ReadBin(file);
                    filepos = -1;
                }
                catch (Exception ex)
                {
                    filepos = file.BaseStream.Position;
                    debug = (string)ex.Message;
                    Console.WriteLine("!!!! ERROR !!!!\n" + debug);
                }
            }
            return filepos;
        }

        /// <summary>
        ///  these functions return -1 if file was correctly processed, otherwise return the last file position for debugging.
        /// </summary>
        public Int64 SaveFile(string filepath)
        {
            using (BinaryWriter file = new BinaryWriter(File.OpenWrite(filepath)))
                if (!WriteBin(file)) return file.BaseStream.Position;
            return -1;
        }

        /// <summary>
        /// </summary>
        public bool ReadBin(BinaryReader file)
        {
            file.BaseStream.Seek(0, SeekOrigin.Begin);
            return false;
        }

        /// <summary>
        /// </summary>
        public bool WriteBin(BinaryWriter file)
        {
            return false;
        }


    }


    public struct MwmString1 : IBinaryReadWrite 
    {
        byte length;
        string stringa ;

        public string Stringa
        {
            get { return stringa; }
            set
            {
                int size = value.Length;
                if (size >= byte.MaxValue) throw new ArgumentOutOfRangeException("string lenght max 255 chars");
                length = (byte)size;
                stringa = value;
            }
        }

        public bool ReadBin(BinaryReader file)
        {
            length = file.ReadByte();
            stringa = new string(file.ReadChars(length));
            return true;
        }

        public bool WriteBin(BinaryWriter file)
        {
            file.Write(length);
            file.Write(stringa.ToCharArray());
            return true;
        }

        public override string ToString()
        {
            return stringa;
        }
    }


    public struct MwmHeader : IBinaryReadWrite
    {
        public MwmString1 debug;
        public int debugnumber;
        public MwmString1 version;


        public bool ReadBin(BinaryReader file)
        {
            if (!debug.ReadBin(file)) return false;
            debugnumber = file.ReadInt32();
            if (!version.ReadBin(file)) return false;
            return true;
        }

        public bool WriteBin(BinaryWriter file)
        {
            throw new NotImplementedException();
        }
    }
}
