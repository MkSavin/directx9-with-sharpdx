#define WHITE float4(1,1,1,1);
#define BLACK float4(0,0,0,1);
#define BLUE  float4(0,1,0,1);

float4x4 World;
float4x4 Proj;
float4x4 View;

float4 VertexColor = BLACK;
float2 Origin;
float2 Scale;
float  Radius;

struct VS_OUTPUT
{   
    float4  Position : POSITION;
	float4  Color    : COLOR0;
};


VS_OUTPUT VShaderBox(float2 inPosition : POSITION )
{
	VS_OUTPUT output = (VS_OUTPUT)0; 
	output.Position = mul(float4(inPosition,0,1), World);
	output.Position = mul(output.Position, View);
	output.Position = mul(output.Position, Proj);
	output.Color = BLACK;
	return output;
}

VS_OUTPUT VShaderSphere(float2 inPosition : POSITION )
{
	VS_OUTPUT output = (VS_OUTPUT)0; 
	output.Position = mul(float4(inPosition,0,1), World);
	output.Position.xyz = normalize(output.Position.xyz) * Radius;
	output.Position = mul(output.Position, View);
	output.Position = mul(output.Position, Proj);
	output.Color = BLACK;
	return output;
}


float4 PShader(float4 Color : COLOR) : COLOR0
{
	return Color;
}



technique TechniqueBox
{
	pass p0
    {
        VertexShader = compile vs_2_0 VShaderBox();
		PixelShader =  compile ps_2_0 PShader();
		ZEnable = false;
    }
}
technique TechniqueSphere
{
	pass p0
    {
        VertexShader = compile vs_2_0 VShaderSphere();
		PixelShader =  compile ps_2_0 PShader();
		ZEnable = false;
    }
}