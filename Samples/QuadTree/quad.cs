﻿using System;
using System.Collections.Generic;
using System.Text;

using Engine.Quadtree;
using Engine.Maths;
using Engine;
using Engine.Tools;
using System.Windows.Forms;

namespace Test
{

    public class Quad : QuadNode<Quad,Tree> , IRectangleAA
    {
        public MapFace face;
        public Matrix4 transform;

        public Quad(Tree main, MapFace face)
            : base(main)
        {
            transform = GetTransform();
        }
        public Quad(Quad parent, int index)
            : base(parent, (byte)index)
        {
            transform = GetTransform();
        }

        /// <summary>
        /// Compute the transformation matrix to apply to a static grid to convert in world space.
        /// The static grid is a rectangle with min = (0,0) and max = (1,1) on XY plane
        /// </summary>
        /// <returns></returns>
        public Matrix4 GetTransform()
        {
            Vector2 origin = Origin;
            Vector2 scale = Size;
            Matrix4 QuadAlign = Matrix4.Translating(origin.x, origin.y, 0);
            Matrix4 QuadFit = Matrix4.Scaling(scale.x, scale.y, 1);
            Matrix4 QuadTreeWorld = tree.World;

            return QuadTreeWorld * QuadAlign * QuadFit;

        }

        public void Split()
        {
            if (level == 0) throw new ArgumentOutOfRangeException("Reach leaf node");
            childrenFlag = ALL;

            ushort x0 = (ushort)(tilecoord.x * 2);
            ushort y0 = (ushort)(tilecoord.y * 2);
            ushort x1 = (ushort)(x0 + 1);
            ushort y1 = (ushort)(y0 + 1);

            if (child == null) child = new Quad[4];
            
            child[0] = new Quad(this, 0);
            child[1] = new Quad(this, 1);
            child[2] = new Quad(this, 2);
            child[3] = new Quad(this, 3);

            /*
            child[0] = new Quad(this, 0, x0, y0);
            child[1] = new Quad(this, 1, x1, y0);
            child[2] = new Quad(this, 2, x0, y1);
            child[3] = new Quad(this, 3, x1, y1);
            */
        }

        public void RecursiveSplit()
        {
            if (level > 0)
            {
                Split();
                child[0].RecursiveSplit();
                child[1].RecursiveSplit();
                child[2].RecursiveSplit();
                child[3].RecursiveSplit();
            }
        }


        public Vector2 Size
        {
            get { return tree.Size / tree.pow2[Depth]; }
        }

        public Vector2 Origin
        {
            get 
            {
                Vector2 origin = tree.Origin;
                origin.x += tilecoord.x * (tree.Size.x / tree.pow2[Depth]);
                origin.y += tilecoord.y * (tree.Size.y / tree.pow2[Depth]);
                return origin;
            }
        }

        public Vector2 Max
        {
            get { return Origin + Size; }
        }

        public Vector2 Min
        {
            get { return Origin; }
        }

        public Vector2 Center
        {
            get
            {
                Vector2 origin = tree.Origin;
                origin.x += tilecoord.x * (tree.Size.x / tree.pow2[Depth] * 0.5f);
                origin.y += tilecoord.y * (tree.Size.y / tree.pow2[Depth] * 0.5f);
                return origin;
            }
        }

        public Vector2 HalfSize
        {
            get { return tree.Size / tree.pow2[Depth] * 0.5f; }
        }
    }
}
