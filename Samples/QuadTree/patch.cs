﻿
using System;
using System.Collections.Generic;
using System.Text;

using Engine;
using Engine.Maths;
 

namespace Test
{
    public class PatchProgram : Effect
    {
        public readonly EffectTechnique TechniqueBox, TechniqueSphere;
        public readonly EffectParamMatrix4 World;
        public readonly EffectParamMatrix4 Proj;
        public readonly EffectParamMatrix4 View;

        public readonly EffectParamVector2 Origin;
        public readonly EffectParamVector2 Scale;
        public readonly EffectParamFloat Radius;
        public readonly EffectParamColor VertexColor;


        public PatchProgram(Device device)
            : base(device, "patch.fx")
        {
            TechniqueBox = new EffectTechnique(this, "TechniqueBox");
            TechniqueSphere = new EffectTechnique(this, "TechniqueSphere");
            World = new EffectParamMatrix4(this, "World");
            Proj = new EffectParamMatrix4(this, "Proj");
            View = new EffectParamMatrix4(this, "View");
            Origin = new EffectParamVector2(this, "Origin");
            Scale = new EffectParamVector2(this, "Scale");
            Radius = new EffectParamFloat(this, "Radius");
            VertexColor = new EffectParamColor(this, "VertexColor");
        }


    }
    
    public class PatchGeometry
    {
        public VertexBuffer vertexbuffer;
        public VertexDeclaration declaration;
        public Vector2[] points;

        public PatchGeometry(Device device)
        {
            // create a simple static quad, can be managed because use a very small memory size
            VertexLayout layout = new VertexLayout();
            layout.Add(0, DeclarationType.Float2, DeclarationUsage.Position);


            points = new Vector2[6];
            points[0] = new Vector2(0, 0);
            points[1] = new Vector2(1, 0);
            points[2] = new Vector2(1, 1);
            points[3] = new Vector2(0, 0);
            points[4] = new Vector2(1, 1);
            points[5] = new Vector2(0, 1);
            

            vertexbuffer = new VertexBuffer(device, layout, BufferUsage.Managed, points.Length);
            VertexStream vstream = vertexbuffer.OpenStream();
            vstream.WriteCollection<Vector2>(points, 0, points.Length, 0);
            vertexbuffer.CloseStream();
            vertexbuffer.Count = points.Length;
            declaration = new VertexDeclaration(device, layout);
        }

        public void DrawQuad(Device device)
        {
            device.DrawPrimitives(PrimitiveType.TriangleList, 0, 2);
        }
    }
}
