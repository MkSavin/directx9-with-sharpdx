﻿
using System;
using System.Collections.Generic;
using System.Text;

using Engine;
using Engine.Tools;
 

namespace Test
{
    public class Planet
    {
        Device device;
        PatchProgram effect;
        PatchGeometry geometry;
        Font font;

        public CubeTree tree;
        public float Radius;

        public Planet(Device device, float Radius)
        {
            this.device = device;
            this.Radius = Radius;

            geometry = new PatchGeometry(device);
            effect = new PatchProgram(device);
            font = new Font(device, Font.FontName.Arial, 8);

            tree = new CubeTree(6);
            tree.Side[0].root.RecursiveSplit();
            /*
            tree.Faces[1].root.RecursiveSplit();
            tree.Faces[2].root.RecursiveSplit();
            tree.Faces[3].root.RecursiveSplit();
            tree.Faces[4].root.RecursiveSplit();
            tree.Faces[5].root.RecursiveSplit();
            */
        }

        public void Draw(ICamera camera, bool sphere = false)
        {
            effect.Radius.Value = Radius;
            effect.Proj.Value = camera.Projection;
            effect.View.Value = camera.View;

            device.SetVertexDeclaration(geometry.declaration);
            device.SetVertexStream(geometry.vertexbuffer);

            device.renderstates.fillMode = FillMode.WireFrame;
            device.renderstates.cullMode = Cull.CounterClockwise;


            EffectTechnique technique = sphere ? effect.TechniqueSphere : effect.TechniqueBox;

            foreach (Pass pass in technique)
            {
                for (int i = 0; i < 6; i++)
                {
                    foreach (Quad quad in tree.Side[i].enumerator)
                    {
                        if (quad.ChildrenFlag == 0)
                        {
                            effect.World.Value = quad.transform;
                            effect.Origin.Value = quad.Origin;
                            effect.Scale.Value = quad.Size;

                            // manual update of these value. this because is better don't exist 
                            // from effect.Begin() loop
                            effect.Origin.SetParam(effect);
                            effect.Scale.SetParam(effect);
                            effect.World.SetParam(effect);
                            effect.Commit();

                            geometry.DrawQuad(device);


                            //Vector3 proj = Vector3.Project(effect.World.Value.Position, camera);
                            //font.Draw(quad.ToString(), (int)proj.x, (int)proj.y, Color.Black);
                        }
                    }
                }
            }
        }

    }
}
