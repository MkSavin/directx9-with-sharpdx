﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Engine.Maths;
using Engine.Forms;
using Engine.Tools;
using Engine;

namespace Test
{
    public partial class MyForm : GameForm
    {
        Planet planet;
        Axis axis;
        Engine.Font font;

        InputManager input;
        TrackBallAroundPoint trackball;


        EffectLine lineffect;

        RectangleAA world = new RectangleAA(-10, -10, 10, 10);
        RectangleUV client = default(RectangleUV);
        RectangleAA rect = RectangleAA.Empty;
        Circle circle = Circle.Empty;
        Point start, end;

        public MyForm()
        {
            Engine.Content.EngineResources.ContentFolder = System.IO.Path.GetFullPath(@"..\..\..\..\..\Renderer\Resources\Content\");

            InitializeComponent();


        }

        /// <summary>
        /// Whait form initialization is complete before create device to avoid incorrect clientsize
        /// </summary>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            axis = new Axis(render.Device, 10.0f);
            lineffect = new EffectLine(render.Device);
            planet = new Planet(render.Device, 10.0f);
            font = new Engine.Font(render.Device, Engine.Font.FontName.Arial, 8);


            input = new InputManager(this);
            trackball = new TrackBallAroundPoint(this, input);
            trackball.Around = Vector3.UnitX * 5;

            TreeNode node = planet.tree.Side[0].root.GetDebugView();
            this.treeView1.Nodes.Add(node);
            this.treeView1.ExpandAll();
            this.treeView1.Enabled = false;
        }

        public override void Render(double alpha, GameLoopFunction info)
        {
            if (!render.Device.CanDraw()) return;

            render.ClearDraw(BackColor);
            render.BeginDraw();

            axis.Draw(trackball, lineffect);
            planet.Draw(trackball, true);

            Vector3 p = Vector3.Project(trackball.Around, render.FrameSetting.Viewport, trackball);

            font.Draw(".A", (int)p.x, (int)p.y, Color32.Black);

            render.EndDraw();
            render.Present();
        }

        public override void Update(double elapsed, GameLoopFunction info)
        {
            input.Update(elapsed);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
        }


        void MyForm_GamePaint(GameForm sender, PaintEventArgs e)
        {
            /*
            e.Graphics.Clear(BackColor);
            client = (RectangleUV)e.ClipRectangle;


            if (!rect.IsEmpty)
            {
                Rectangle rectangle = (Rectangle)rect.ConvertToScreen(client, world);

                //e.Graphics.DrawRectangle(Pens.Red, rectangle);
                e.Graphics.DrawEllipse(Pens.Red, rectangle);
            }
           
            foreach (Quad quad in tree.Faces[0].circleSelector)
            {
                if (quad.Depth < 3)
                {
                    
                    Vector2 Size = quad.Size;
                    Vector2 Origin = quad.Origin;


                    Rectangle uv = (Rectangle)RectangleAA.FromOriginSize(Origin, Size).ConvertToScreen(client, world);

                    e.Graphics.DrawRectangle(Pens.Black, uv);
                    e.Graphics.DrawString(quad.ToString(), font_design, Brushes.Black, (uv.Left + uv.Right) / 2, (uv.Bottom + uv.Top) / 2);

                }
            }
             */
        }
        

        protected override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                start = e.Location;
            }
        }
        protected override void OnMouseUp(MouseEventArgs e)
        {
            base.OnMouseUp(e);
            end = e.Location;
            UpdateSelection();
        }
        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);

            //Vector2 center = ((VectorInt2)e.Location).ConvertToWorld(client, world);

        }

        void UpdateSelection()
        {
            RectangleUV uv = RectangleUV.FromPoint(start.X, start.Y, end.X, end.Y, 0, 0);

            rect = (RectangleAA)uv.ConvertToWorld(client, world);

            //tree.Faces[0].rectangleSelector.rectangle = rect;
            //tree.Faces[0].circleSelector.circle = rect.Inscribed;
        }
    }
}
