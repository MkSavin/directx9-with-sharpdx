﻿using Engine.Maths;
using System;
using System.Collections.Generic;
using System.Text;

namespace Test
{
    public class Light
    {
        public Vector3 LDirection = (Vector3.One).Normal;
        public Color32 LColor = Color32.White;
        public float LIntensity = 1;
    }
}
