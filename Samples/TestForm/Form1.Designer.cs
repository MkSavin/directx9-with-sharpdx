﻿namespace TestForm
{
    partial class TestForm
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Liberare le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        protected override void InitializeComponent()
        {
            this.renderControl1 = new Engine.Forms.RenderControl();
            this.renderControl2 = new Engine.Forms.RenderControl();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.performaceGraphFPS = new Engine.Graph.PerformaceGraph();
            this.SuspendLayout();
            // 
            // renderControl1
            // 
            this.renderControl1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(246)))), ((int)(((byte)(250)))));
            this.renderControl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.renderControl1.Location = new System.Drawing.Point(12, 12);
            this.renderControl1.Name = "renderControl1";
            this.renderControl1.Size = new System.Drawing.Size(255, 213);
            this.renderControl1.TabIndex = 0;
            // 
            // renderControl2
            // 
            this.renderControl2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(246)))), ((int)(((byte)(250)))));
            this.renderControl2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.renderControl2.Location = new System.Drawing.Point(273, 12);
            this.renderControl2.Name = "renderControl2";
            this.renderControl2.Size = new System.Drawing.Size(255, 213);
            this.renderControl2.TabIndex = 1;
            // 
            // treeView1
            // 
            this.treeView1.Location = new System.Drawing.Point(675, 98);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(209, 271);
            this.treeView1.TabIndex = 2;
            // 
            // performaceGraph1
            // 
            this.performaceGraphFPS.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.performaceGraphFPS.Location = new System.Drawing.Point(12, 337);
            this.performaceGraphFPS.Name = "performaceGraph1";
            this.performaceGraphFPS.Size = new System.Drawing.Size(611, 168);
            this.performaceGraphFPS.TabIndex = 3;
            // 
            // TestForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1004, 517);
            this.Controls.Add(this.performaceGraphFPS);
            this.Controls.Add(this.treeView1);
            this.Controls.Add(this.renderControl2);
            this.Controls.Add(this.renderControl1);
            this.Name = "TestForm";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }
        #endregion

        private Engine.Forms.RenderControl renderControl1;
        private Engine.Forms.RenderControl renderControl2;
        private System.Windows.Forms.TreeView treeView1;
        private Engine.Graph.PerformaceGraph performaceGraphFPS;
    }
}

