﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Engine;
using Engine.Tools;
using Engine.Maths;

using GameLoop = Engine.Tools.GameLoopOld;


namespace TestForm
{
    public partial class TestForm : RenderForm
    {
        InputManager inputmanager;
        GameLoop gameLoop;
        float prev = 100;
        float x = 0;


        public TestForm()
            : base("test")
        {
            InitializeComponent();

            base.AppActivated += GameForm_AppActivated;
            base.AppDeactivated += GameForm_AppDeactivated;

        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            
            inputmanager = new InputManager(this);

            try
            {
                gameLoop = new GameLoop(this);
                gameLoop.AddLoopableFunction(RenderWorld, TimeStepMode.LimitFrameRate,  1000.0f/ 60);
                gameLoop.AddLoopableFunction(UpdateWorld, TimeStepMode.SemiFixedTimestep,  10.0f);
                gameLoop.Start();
            }
            catch(Exception exp)
            {
                Console.WriteLine("ERRRRRRRRRRRRROOOOOOOOOOOOORRRRRRR");
                Console.WriteLine(exp.ToString());
            }

            this.performaceGraphFPS.Plotter.AddSerie("unknow", 50, Color.Gray);
            this.performaceGraphFPS.Plotter.AddSerie("FPS", 50);
            this.performaceGraphFPS.Plotter.AddSerie("UPS", 50);
        }

        private void UpdateWorld(double dt, GameLoopFunction info)
        {
            inputmanager.Update(dt);

            Engine.Graph.ISerieCollector Plotter = performaceGraphFPS.Plotter;

            Plotter.AddValue(2, info.PrevDurationMS, (float)info.Timer.SecondsAccumulated);

            System.Threading.Thread.Sleep(MathUtils.GetRandomInt(0, 10));
        }

        void RenderWorld(double alpha, GameLoopFunction info)
        {
            try
            {
                System.Threading.Thread.Sleep(MathUtils.GetRandomInt(0, 10));

                int serie = MathUtils.GetRandomInt(1, 2);

                float sin = (float)Math.Sin(x);
                x += 0.5f;
                Engine.Graph.ISerieCollector Plotter = performaceGraphFPS.Plotter;

                Plotter.AddValue(1, info.PrevDurationMS,(float)info.Timer.SecondsAccumulated);

                //Vector2 p = new Vector2(info.Timer.SecondsAccumulated, 0);

                //this.performaceGraph1.Plotter.AddValue(serie, sin);
                //p.y = sin;
                //Plotter.AddValue(0, p);
                
                //p.y = sin * MathUtils.GetRandomFloat(-10, 10);
                //Plotter.AddValue(serie, p);

                performaceGraphFPS.Invalidate();
                Invalidate();
            }
            catch (Exception exp)
            {
                Console.WriteLine("ERRRRRRRRRRRRROOOOOOOOOOOOORRRRRRR");
                Console.WriteLine(exp.ToString());
            }
        }


        void GameForm_AppDeactivated(object sender, EventArgs e)
        {
            Console.WriteLine("Deactivate");
        }

        void GameForm_AppActivated(object sender, EventArgs e)
        {
            Console.WriteLine("Activate");
        }

    }
}
