﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using Engine.Partitions;
using Engine.Tools;
using Engine.Maths;
using Engine;
using System.Diagnostics;
using System.Drawing.Drawing2D;
using Engine.Time;
using System.Timers;

using GameLoop = Engine.Tools.GameLoopOld;


namespace MathTest
{
    class Program
    {
        static void Test2()
        {
            GameLoop loop = new GameLoop();
            loop.AddLoopableFunction(UpdateFn, TimeStepMode.SemiFixedTimestep, 10.0f);
            loop.AddLoopableFunction(RenderFn, TimeStepMode.VariableTimestep, 60);
            loop.Start();


        }
        static void TestB()
        {
            Line r1 = new Line(Vector3.Zero, new Vector3(2, 1, 0));
            Line r2 = new Line(new Vector3(5, 0, -1), Vector3.UnitZ);
            //float param1, param2;


            Vector3 a = Vector3.One;
            Vector3 b = Vector3.One;
            Matrix4 m = Matrix4.Zero;


            Stopwatch watch = new Stopwatch();
            watch.Start();

            for (int i = 0; i < 100000; i++)
            {
                float m33 = m.m33;
            }

            watch.Stop();
            long t0 = watch.ElapsedTicks;

            watch.Restart();
            for (int i = 0; i < 100000; i++)
            {
                unsafe
                {
                    float m33 = m.Dim[3 * 4 + 3];
                }
            }
            watch.Stop();
            long t1 = watch.ElapsedTicks;

            Console.WriteLine(string.Format("t0 {0}  t1 {1} rap {2}", t0, t1 , (double)t0/t1));
        }
        static void TestA()
        {
            Vector3 p0 = Vector3.Zero;
            Vector3 dir = (new Vector3(2,1,0)).Normal;
            Vector3 p = new Vector3(5, 0, 0);
            Line line = new Line(p0, dir);

            Stopwatch watch = new Stopwatch();
            watch.Start();

            for (int i = 0; i < 100000; i++)
            {
                //Line.GetDistance(ref p0, ref dir, p);
                //Line.GetDistance(ref line.orig, ref line.dir, p);
                line.GetPointDistance( p);
            }

            watch.Stop();
            long t0 = watch.ElapsedTicks;

            watch.Restart();
            for (int i = 0; i < 100000; i++)
            {
                Line.GetPointDistance( p0,  dir, p);
                //Line.GetDistance( p0,  dir, p);
            }
            watch.Stop();
            long t1 = watch.ElapsedTicks;

            Console.WriteLine(string.Format("t0 {0}  t1 {1}", t0, t1));

        }
        static void TestC()
        {
            QueueClass<int> queue = new QueueClass<int>(10);
            for (int i = 0; i < 10; i++) queue.AddTail(i+1);
            queue.RemoveHead();
            queue.RemoveHead();
            //queue.Enqueue(11);

            bool test = true;
            foreach (int val in queue.ItemList)
            {
                Console.WriteLine(val);
                if (test) queue.AddTail(99);
                    test = false;
            }
            Console.WriteLine("inverteds");
            foreach (int val in queue.ItemListInverted)
            {
                Console.WriteLine(val);
            }

 


            Stopwatch watch = new Stopwatch();
            watch.Start();

            for (int i = 0; i < 100000; i++)
            {

            }

            watch.Stop();
            long t0 = watch.ElapsedTicks;

            watch.Restart();
            for (int i = 0; i < 100000; i++)
            {

            }
            watch.Stop();
            long t1 = watch.ElapsedTicks;

            Console.WriteLine(string.Format("t0 {0}  t1 {1}", t0, t1));
        }

        static void UpdateFn(double elapsed, GameLoopFunction info)
        {
            System.Threading.Thread.Sleep(4);
        }
        static void RenderFn(double elapsed, GameLoopFunction info)
        {
            System.Threading.Thread.Sleep(4);
        }
        static void TestQueue()
        {
            QueueClass<string> queue = new QueueClass<string>(7);

            queue.AddTail("A");
            queue.AddTail("B");
            queue.AddTail("C");
            queue.AddTail("D");
            queue.AddTail("E");
            queue.AddTail("F");
            queue.AddTail("G");
            queue.RemoveHead();
            queue.AddTail("T");
            queue.RemoveTail();
            queue.RemoveTail();
            queue.RemoveTail();

            for (int i = 0; i < queue.Count; i++)
            {
                string s = queue.ItemListInverted[i];
                Console.WriteLine(s);
            }
            Console.WriteLine("***************");
            for (int i = 0; i < queue.Count; i++)
            {
                string s = queue.ItemList[i];
                Console.WriteLine(s);
            }
        }

        static void TesetMatrix()
        {
            RectangleF coord = new RectangleF(-1,5,11,7);
            PointF[] corner = new PointF[3];
            corner[0] = new PointF(1,1);
            corner[1] = new PointF(201, 1);
            corner[2] = new PointF(1, 301);

            Matrix matrix = new Matrix(coord, corner);

            Matrix matrix2 = new Matrix();
            matrix2.Reset();
        }

        static void TestSleepTime()
        {
            PreciseTimer timer = new PreciseTimer();


            int iterations = (int)(100000.0 * 10.0 / PreciseTimer.MS_For100000SpinIterations);


            // Test precise 10ms delay
            timer.Start();
            int N = 100;
            for (int i = 0; i < N; i++)
            {
                System.Threading.Thread.SpinWait(iterations);
            }
            timer.Stop();
            Console.WriteLine(string.Format(" Avarage  duration of precise delay 10 : {0} ms", timer.MilliSecondsAccumulated/N));
            Console.WriteLine(string.Format("Predicted duration of precise delay 10 : {0} ms", 10));

            // Test 1ms delay
            timer.Start();
            N = 100;
            for (int i = 0; i < N; i++)
            {
                System.Threading.Thread.Sleep(10);
            }
            timer.Stop();
            Console.WriteLine(string.Format(" Avarage  duration of delay 1 : {0} ms", timer.MilliSecondsAccumulated/N));
            Console.WriteLine(string.Format("Predicted duration of delay 1 : {0} ms", 1));

            // Test 100ms delay
            timer.Start();
            N = 10;
            for (int i = 0; i < N; i++)
            {
                System.Threading.Thread.Sleep(100);
            }
            timer.Stop();
            Console.WriteLine(string.Format(" Avarage  duration of delay 100 : {0} ms", timer.MilliSecondsAccumulated/N));
            Console.WriteLine(string.Format("Predicted duration of delay 100 : {0} ms", 100));


            // Test 1s delay
            timer.Start();
            N = 1;
            for (int i = 0; i < N; i++)
            {
                System.Threading.Thread.Sleep(1000);
            }
            timer.Stop();
            Console.WriteLine(string.Format(" Avarage  duration of delay 1000 : {0} ms", timer.MilliSecondsAccumulated/N));
            Console.WriteLine(string.Format("Predicted duration of delay 1000 : {0} ms", 1000 ));
        }

        static PerformanceCounter cpu;
        static PerformanceCounter ram;
        static Process process = Process.GetCurrentProcess();
        static CpuUsageCs.CpuUsage usage;
        
        static void TestCPUusage()
        {  
           usage = new CpuUsageCs.CpuUsage();

            cpu = new PerformanceCounter("Process", "% Processor Time", "_Total", true);
            ram = new PerformanceCounter("Memory", "Available MBytes");

            ApplicationLooper.OnLooping += TimerLooper_OnLooping;
        }

        static void TimerLooper_OnLooping(object sender, LooperEventArgs e)
        {
            TimeSpan tim = process.UserProcessorTime;
            string info = usage.GetUsage().ToString();

            Console.WriteLine(string.Format("cpu {0:####}  ram {1:####} elapsed {2} {3}", 
                cpu.NextValue() / Environment.ProcessorCount, 
                ram.NextValue(),
                e.ElapsedMS,
                info));
        }

        static Timer timer = new Timer(1);
       static string machineName;


        static void TimerFunction(double elapsed)
        {
            Console.WriteLine(string.Format("cpu {0}  ram {1}  dt {2}  duration {3}", cpu.NextValue() / Environment.ProcessorCount, ram.NextValue(), elapsed, null));
        }

        static void Main(string[] args)
        {
            float min = -100;
            float max = 0;
            float r, d;
            MathUtils.LooseLabel(min, max, 10, out min, out max, out r, out d);


            string[] cmdArgs = System.Environment.GetCommandLineArgs();
            machineName = cmdArgs[0];
            if ((cmdArgs != null) && (cmdArgs.Length > 1))
            { 
                machineName = cmdArgs[0]; 
            }

            TestCPUusage();

            Console.ReadKey();
        }
    }
}
