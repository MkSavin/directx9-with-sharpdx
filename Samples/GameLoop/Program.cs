﻿using System;
using System.IO;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using Engine.Maths;
using Engine.Tools;
 
using Engine.Inputs;
using Engine;
using Engine.Content;
using Engine.Forms;
using Engine.Geometry;


using Font = Engine.Font;

using GameLoop = Engine.Tools.GameLoopOld;

namespace Test
{
    public class TestForm : Form
    {
        Random rnd = new Random();

        MyTrackBallWASD trackball;
        Font font;

        EffectLine effectline;
        EffectLight effectlight;

        Axis axis0, axis1;

        MeshObj box;
        MeshObj mesh;
        NormalObj norm;      
        GameLoop gameLoop;

        SkyBox sky;

        bool loop = true;
        Light light = new Light();
        Vector3 scale = Vector3.One;
        float yrotation = 0.0f;
        int draw = 0;

        InputManager inputmanager;
        RenderControl myRenderControl;
        ComboBox comboBox1;
        Button buttonReset;

        float FPS = 0;
        float UPS = 0;
        float Fms = 0.0f;
        float Ums = 0.0f;
        private SplitContainer splitContainer1;
        private Panel panelRenderLight;
        float Udt = 0.0f;
        private Panel panelUpdateLight;

        bool renderLight = true;
        bool updateLight = true;

        float PhysicTimeInSeconds, RealTimeInSeconds;


        public TestForm()
        {

            InitializeComponent();

            inputmanager = new InputManager(this.myRenderControl);
            myRenderControl.InitGraphic(null);


            Device device = myRenderControl.render.Device;


            trackball = new MyTrackBallWASD(myRenderControl, inputmanager,
                Matrix4.MakeViewLH(new Vector3(10, 10, 10), Vector3.Zero, Vector3.UnitY),
                Matrix4.MakeProjectionAFovYLH(0.1f, 100.0f, myRenderControl.render.FrameSetting.AspectRation));

            axis0 = new Axis(device, 10, Vector3.Zero);
            axis1 = new Axis(device, 5, Vector3.UnitY);

            box = new MeshObj(device, BaseTriGeometry.BoxClose(1, 1, 1));
            mesh = new MeshObj(device, BaseTriGeometry.GeoSphere(1));
            norm = new NormalObj(device, mesh.Vertices, mesh.Normals, mesh.world);

            font = new Font(device, Engine.Font.FontName.Arial, 10);

            effectline = new EffectLine(device);
            effectlight = new EffectLight(device);

            foreach (EffectTechnique tech in effectlight.Techniques.Values)
            {
                comboBox1.Items.Add(tech.Name);
            }
            comboBox1.SelectedIndex = 0;

            //Console.WriteLine(effect.ToString());

            sky = new SkyBox(device, EngineResources.SunsetTextureCube);

            gameLoop = new GameLoop(this);

            //gameLoop.AddFunction(RenderWorld, TimeStepMode.LimitFrameRate, 1000.0f / 60.0f, GameLoopFakeFlags.Noise, 10);

            gameLoop.AddLoopableFunction(UpdateWorld, TimeStepMode.LimitFrameRate, 1000.0f / 60, GameLoopFakeFlags.Noise, 10);
            gameLoop.AddLoopableFunction(RenderWorld, TimeStepMode.LimitFrameRate, 1000.0f / 30, GameLoopFakeFlags.Noise, 10);
            gameLoop.AddLoopableFunction(RefreshScreen, TimeStepMode.LimitFrameRate, 1000.0f / 10);
            gameLoop.Start();

            //gameLoopsimple = new GameLoopSimple(RenderWorld2, UpdateWorld);
            //gameLoopsimple.StartLooping();
        }

        void GameForm_AppDeactivated(object sender, EventArgs e)
        {
            Console.WriteLine("Deactivate");
        }

        void GameForm_AppActivated(object sender, EventArgs e)
        {
            Console.WriteLine("Activate");
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            plotterFrames.Title = "FRAMES";
            plotterFrames.Plotter.AddSerie("FPS", 20, Color.Red);
            plotterFrames.Plotter.AddSerie("UPS", 20, Color.Green);

            plotterDuration.Title = "CALLBACK MS";
            plotterDuration.Plotter.AddSerie("Fret", 20, Color.Red);
            plotterDuration.Plotter.AddSerie("Uret", 20, Color.Green);

            //plotterDuration.Plotter.AddSerie("Udt", 20, Color.Blue);
            //plotterDuration.Plotter.AddSerie("Ums_media", 20, Color.DarkGreen);

            plotterCounter.Title = "COUNTER";
            plotterCounter.Plotter.AddSerie("nF", 20, Color.Red);
            plotterCounter.Plotter.AddSerie("nU", 20, Color.Green);
        }


        private void RefreshScreen(double dt, GameLoopFunction info)
        {
            doRefresh();
        }

        private void UpdateWorld(double dt, GameLoopFunction info)
        {
            Console.WriteLine("call update");

            updateLight = !updateLight;

            panelUpdateLight.BackColor = updateLight ? Color.White : Color.DarkGreen;
            panelUpdateLight.Invalidate();


            UPS = info.CallsPerSecond;
            Ums = info.PrevDurationMS;
            Udt = (float)dt;

            float seconds = (float)info.Timer.SecondsAccumulated;

            // update frame count for "UpdateWorld"
            plotterFrames.Plotter.AddValue(1, UPS, seconds);
            
            // update duration of "UpdateWorld"
            plotterDuration.Plotter.AddValue(1, (float)info.PrevDurationMS, seconds);
            //plotterDuration.Plotter.AddValue(2, Udt, seconds);
            //plotterDuration.Plotter.AddValue(3, info.CallsPerCicle > 0 ? Ums / info.CallsPerCicle : 0, seconds);

            plotterCounter.Plotter.AddValue(1, info.CallsPerCicle, seconds);

            doUpdate(dt, info);
        }

        private void RenderWorld(double alpha, GameLoopFunction info)
        {
            renderLight = !renderLight;

            panelRenderLight.BackColor = renderLight ? Color.White : Color.DarkRed;
            panelRenderLight.Invalidate();

            FPS = info.CallsPerSecond;
            Fms = info.PrevDurationMS;

            plotterFrames.Plotter.AddValue(0, info.CallsPerSecond, (float)info.Timer.SecondsAccumulated);

            plotterDuration.Plotter.AddValue(0, (float)info.PrevDurationMS, (float)info.Timer.SecondsAccumulated);
            //plotterDuration.Plotter.AddValue(1, (float)info.accumulated, (float)info.Timer.SecondsAccumulated);
            
            plotterCounter.Plotter.AddValue(0, info.CallsPerCicle, (float)info.Timer.SecondsAccumulated);

            doRender();
        }

        private void RenderWorld2(double alpha, GameLoopFunction info)
        {
            renderLight = !renderLight;

            panelRenderLight.BackColor = renderLight ? Color.White : Color.DarkRed;
            panelRenderLight.Invalidate();

            FPS = info.CallsPerSecond;
            Fms = info.PrevDurationMS;

            plotterFrames.Plotter.AddValue(0, FPS, (float)info.Timer.SecondsAccumulated);
            plotterDuration.Plotter.AddValue(0, (float)info.retardaccumulated, (float)info.Timer.SecondsAccumulated);
            plotterCounter.Plotter.AddValue(0, info.CallsPerCicle, (float)info.Timer.SecondsAccumulated);

            doRender();
            doRefresh();
        }


        void doUpdate(double dt, GameLoopFunction info)
        {
            PhysicTimeInSeconds += (float)(dt / 1000.0);
            
            double t0 = info.Timer.SecondsAccumulated;
            inputmanager.Update(dt);
            double t1 = info.Timer.SecondsAccumulated;

            //this.statusLabel.Text = string.Format("FPS:{0} ms:{1:0.00}   UPS:{2} ms:{3:0.00} dt:{4} calls:{5}   InputMS:{6:0.00}", FPS, Fms, (int)UPS, Ums, Udt, info.CallPerLoopcycle, (t1 - t0) * 1000.0);

            dt /= 1000.0f;

            // calculate 10°/second
            if (dt > 0)
                yrotation += (float)(Math.PI * 2.0 * 10.0 / 360.0 * dt);

            float max = (float)(Math.Sin(yrotation) * 10 + 10.0);
            float min = max / 2.0f;
            int delay = rnd.Next((int)min, (int)max);
            //Console.SetCursorPosition(0, 1);
            //Console.WriteLine("Update delay " + delay);
            //System.Threading.Thread.Sleep(delay);


            float x = (float)Math.Sin(yrotation * 2) * 2;
            float y = (float)Math.Sin(yrotation * 4);
            float z = (float)Math.Sin(yrotation * 8);


            scale.x = 3.0f * (float)Math.Pow(2, x);
            scale.y = 3.0f * (float)Math.Pow(2, y);
            scale.z = 3.0f * (float)Math.Pow(2, z);

        }

        void doRender()
        {
            RenderWindow render = myRenderControl.render;
            Device device = render.Device;

            try
            {
                if (render.Device.CanDraw())
                {
                    ICamera camera = trackball;
                    Matrix4 projection = camera.Projection;
                    Matrix4 view = camera.View;
                    Matrix4 iview = camera.CameraView;

                    render.ClearDraw(BackColor);

                    render.BeginDraw();

                    EffectTechnique technique = effectlight.GetTechniqueByName(comboBox1.SelectedItem.ToString());
                    effectlight.LightDirection.Value = light.LDirection;

                    effectlight.Light.Value = (Vector4)light.LColor * light.LIntensity;
                    effectlight.Ambient.Value = Vector4.Zero;
                    effectlight.Diffuse.Value = (Vector4)Color.Blue * light.LIntensity;


                    draw++;


                    Matrix4 scalemat = Matrix4.RotationX(yrotation) * Matrix4.RotationY(yrotation) * Matrix4.Scaling(ref scale);

                    //render.Device.ClearShaderCode();
                    mesh.Draw(projection, view, scalemat, iview, effectlight, technique);
                    norm.Draw(projection, view, scalemat);
                    axis1.Draw(projection * view, effectline);

                    //axis0.Draw(projection * view, effect);

                    OBBox obb = GetMinBox(mesh.Vertices, scalemat);

                    device.renderstates.fillMode = FillMode.WireFrame;
                    device.renderstates.ZBufferFunction = Compare.Always;
                    box.Draw(projection, view, obb.trs, iview, effectline);

                    device.renderstates.ZBufferFunction = Compare.LessEqual;
                    device.renderstates.fillMode = FillMode.Solid;


                    sky.DrawLast(view, projection);
                    render.EndDraw();
                    render.Present();
                }
                //Console.WriteLine(string.Format("Num SetValue calls = {0}", EffectParam.SetValueCalls));
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

        }

        void doRefresh()
        {
            Console.WriteLine("call refresh");
           
            plotterFrames.Invalidate();
            plotterDuration.Invalidate();
            plotterCounter.Invalidate();

            this.statusLabel.Text = string.Format("#PHY {6:0.000}s REAL {7:0.000}s DIF{8:0.000}  # FPS:{0} {1:0.00}ms  UPS:{2} {3:0.00}ms dt:{4}ms TOTAL {5}ms ",
              FPS, Fms, (int)UPS, Ums, Udt, gameLoop.PrevDurationMS, PhysicTimeInSeconds, gameLoop.Timer.SecondsAccumulated, PhysicTimeInSeconds-gameLoop.Timer.SecondsAccumulated);
        }


        private OBBox GetMinBox(Vector3[] vertices, Matrix4 mat)
        {
            List<Vector3> verts = new List<Vector3>(vertices);
            for (int i = 0; i < verts.Count; i++) verts[i] = Vector3.TransformCoordinate(verts[i], mat);
            return Engine.Tools.MinVolumeBoundingBox.GetMinimumEnclosedOBB(verts);
        }


        private void buttonReset_Click(object sender, EventArgs e)
        {
            if (loop)
            {
                gameLoop.Stop();
                buttonReset.Text = "Resume";
            }
            else
            {
                gameLoop.Start();
                buttonReset.Text = "Stop";
            }
            loop = !loop;
        }




        private StatusStrip statusStrip;
        private ToolStripStatusLabel statusLabel;
        private Engine.Graph.PerformaceGraph plotterFrames;
        private Engine.Graph.PerformaceGraph plotterDuration;
        private Engine.Graph.PerformaceGraph plotterCounter;
        

        protected void InitializeComponent()
        {
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.buttonReset = new System.Windows.Forms.Button();
            this.myRenderControl = new Engine.Forms.RenderControl();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.plotterFrames = new Engine.Graph.PerformaceGraph();
            this.plotterDuration = new Engine.Graph.PerformaceGraph();
            this.plotterCounter = new Engine.Graph.PerformaceGraph();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panelRenderLight = new System.Windows.Forms.Panel();
            this.panelUpdateLight = new System.Windows.Forms.Panel();
            this.statusStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusLabel});
            this.statusStrip.Location = new System.Drawing.Point(0, 474);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(1120, 22);
            this.statusStrip.TabIndex = 0;
            this.statusStrip.Text = "statusStrip";
            // 
            // statusLabel
            // 
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(43, 17);
            this.statusLabel.Text = "mouse";
            // 
            // buttonReset
            // 
            this.buttonReset.Location = new System.Drawing.Point(12, 12);
            this.buttonReset.Name = "buttonReset";
            this.buttonReset.Size = new System.Drawing.Size(64, 35);
            this.buttonReset.TabIndex = 1;
            this.buttonReset.Text = "Stop";
            this.buttonReset.UseVisualStyleBackColor = true;
            this.buttonReset.Click += new System.EventHandler(this.buttonReset_Click);
            // 
            // myRenderControl
            // 
            this.myRenderControl.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(189)))), ((int)(((byte)(230)))), ((int)(((byte)(196)))));
            this.myRenderControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myRenderControl.Location = new System.Drawing.Point(0, 0);
            this.myRenderControl.Name = "myRenderControl";
            this.myRenderControl.Size = new System.Drawing.Size(517, 474);
            this.myRenderControl.TabIndex = 2;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(911, 0);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(191, 21);
            this.comboBox1.TabIndex = 3;
            // 
            // plotterFrames
            // 
            this.plotterFrames.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.plotterFrames.Dock = System.Windows.Forms.DockStyle.Top;
            this.plotterFrames.Location = new System.Drawing.Point(0, 0);
            this.plotterFrames.Name = "plotterFrames";
            this.plotterFrames.Size = new System.Drawing.Size(599, 146);
            this.plotterFrames.TabIndex = 4;
            this.plotterFrames.Title = "MyPlotterControl";
            // 
            // plotterDuration
            // 
            this.plotterDuration.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.plotterDuration.Dock = System.Windows.Forms.DockStyle.Top;
            this.plotterDuration.Location = new System.Drawing.Point(0, 146);
            this.plotterDuration.Name = "plotterDuration";
            this.plotterDuration.Size = new System.Drawing.Size(599, 146);
            this.plotterDuration.TabIndex = 6;
            this.plotterDuration.Title = "MyPlotterControl";
            // 
            // plotterCounter
            // 
            this.plotterCounter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.plotterCounter.Dock = System.Windows.Forms.DockStyle.Top;
            this.plotterCounter.Location = new System.Drawing.Point(0, 292);
            this.plotterCounter.Name = "plotterCounter";
            this.plotterCounter.Size = new System.Drawing.Size(599, 146);
            this.plotterCounter.TabIndex = 5;
            this.plotterCounter.Title = "MyPlotterControl";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.panelUpdateLight);
            this.splitContainer1.Panel1.Controls.Add(this.panelRenderLight);
            this.splitContainer1.Panel1.Controls.Add(this.myRenderControl);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.plotterCounter);
            this.splitContainer1.Panel2.Controls.Add(this.plotterDuration);
            this.splitContainer1.Panel2.Controls.Add(this.plotterFrames);
            this.splitContainer1.Size = new System.Drawing.Size(1120, 474);
            this.splitContainer1.SplitterDistance = 517;
            this.splitContainer1.TabIndex = 7;
            // 
            // panelRenderLight
            // 
            this.panelRenderLight.Location = new System.Drawing.Point(12, 3);
            this.panelRenderLight.Name = "panelRenderLight";
            this.panelRenderLight.Size = new System.Drawing.Size(50, 50);
            this.panelRenderLight.TabIndex = 3;
            // 
            // panelUpdateLight
            // 
            this.panelUpdateLight.Location = new System.Drawing.Point(12, 421);
            this.panelUpdateLight.Name = "panelUpdateLight";
            this.panelUpdateLight.Size = new System.Drawing.Size(50, 50);
            this.panelUpdateLight.TabIndex = 4;
            // 
            // TestForm
            // 
            this.ClientSize = new System.Drawing.Size(1120, 496);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.buttonReset);
            this.Controls.Add(this.statusStrip);
            this.Name = "TestForm";
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
    }



    static class Program
    {
        [STAThread]
        static void Main()
        {
            Application.ThreadException += ThreadException;
            AppDomain.CurrentDomain.UnhandledException += UnhandledException;

            // temporaney solution, i store some file used by engine in this folder, so Renderer.dll require this location

            EngineResources.ContentFolder = System.IO.Path.GetFullPath(@"..\..\..\..\..\Renderer\Resources\Content\");

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            TestForm test = new TestForm();

            //MainLoop main = new MainLoop(test);

            Application.Run(test);

        }
        private static void ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            MessageBox.Show(e.Exception.ToString());
        }

        private static void UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            MessageBox.Show(e.ExceptionObject.ToString());
        }
    }


}
