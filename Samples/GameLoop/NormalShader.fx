//-----------------------------------------------------------------------------
// Basic Shader program
//-----------------------------------------------------------------------------

#define GREEN float4(0,1,0,1)
#define RED float4(1,0,0,1)

float4x4 ViewProj;
float4x4 World;
float3x3 WorldInvTraspose;
float4   Diffuse;
float    Lenght;

struct VS_INPUT
{
    float4  Position : POSITION;
	float3  Normal   : NORMAL;
	float   Scalar   : TEXCOORD;
};

struct VS_OUTPUT
{   
    float4  Position : POSITION;
	float4  Color    : COLOR0;
};


//-----------------------------------------------------------------------------
// Vertex Shaders.
//-----------------------------------------------------------------------------

VS_OUTPUT VSVertex(VS_INPUT input)
{
	VS_OUTPUT output = (VS_OUTPUT)0; 

	float3 normal = mul(input.Normal, WorldInvTraspose);

	output.Position = mul(input.Position, World);
	output.Position.xyz += normal * input.Scalar * Lenght;
	output.Position = mul(output.Position, ViewProj);

	output.Color = GREEN;

	// length can have a range of [0,2]
	float diff = length(normal);

	if (diff < 0.9f || diff > 1.1f) output.Color = RED;

	return output;
}

//-----------------------------------------------------------------------------
// Pixel Shaders.
//-----------------------------------------------------------------------------

float4 PSDiffuse(VS_OUTPUT input) : COLOR0
{
	return input.Color;
}

//-----------------------------------------------------------------------------
// Techniques.
//-----------------------------------------------------------------------------
technique NormalLineTechnique
{
    pass passone
    {
        VertexShader = compile vs_2_0 VSVertex();
		PixelShader =  compile ps_2_0 PSDiffuse();
    } 
    pass passtwo
    {
        VertexShader = compile vs_2_0 VSVertex();
		PixelShader =  compile ps_2_0 PSDiffuse();
    } 

}