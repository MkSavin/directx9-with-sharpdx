﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Engine.Forms;
using Engine;
using Engine.Tools;

namespace RenderMesh
{
    public partial class Form1 : GameForm
    {
        Axis axis;
        InputManager input;
        TrackBallAroundPoint trackball;

        public Form1()
        {
            Engine.Content.EngineResources.ContentFolder = System.IO.Path.GetFullPath(@"..\..\..\..\..\Renderer\Resources\Content\");
            InitializeComponent();

            axis = new Axis(this.render.Device, 10);
            input = new InputManager(this);
            trackball = new TrackBallAroundPoint(this, input);

        }

    }
}
