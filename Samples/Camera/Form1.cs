﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Engine;
using Engine.Tools;
using Engine.Forms;
using Engine.Maths;
using Engine.Geometry;
using Engine.Inputs;

using GameLoop = Engine.Tools.GameLoopOld;


namespace Camera
{
    public partial class MyForm : Form
    {
        RenderWindow render;
        Axis axis0;
        Engine.Font font;
        DrawMesh mesh;
        GameLoop gameLoop;
        MyTrackBallWASD trackball;
        EffectLine effectline;
        EffectLight effectlight;

        Matrix4 Proj, View, MyRot, MyTrasl;

        List<Matrix4> boxs;

        float angley = 0.0f;
        Vector3 Eye = new Vector3(10, 10, 20);
        Vector3 Targhet = Vector3.Zero;

        bool first = true;

        Matrix4 proj, view;
        Frustum CopyFrustum;
        FrustumMesh frustumbox;
        InfinitePlaneMesh planemesh;
        InputManager input;

        OBBox box;

        public MyForm()
        {
 
            Engine.Content.EngineResources.ContentFolder = System.IO.Path.GetFullPath(@"..\..\..\..\..\Renderer\Resources\Content\");
            InitializeComponent();
            Proj = View = MyRot = MyTrasl = Matrix4.Identity;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            input = new InputManager(this);

            myRenderControl.InitGraphic(null);

            render = myRenderControl.render;

            trackFOVX.Value = trackFOVY.Value = 45;
            Far = 100.0f;
            Near = 2.0f;

            Device device = this.myRenderControl.render.Device;

            Viewport viewport = render.FrameSetting.Viewport;
            
            font = new Engine.Font(device, Engine.Font.FontName.Calibri, 8);          
            axis0 = new Axis(device, 10, Vector3.Zero);
            effectline = new EffectLine(device);
            planemesh = new InfinitePlaneMesh(device);
            

            MeshListGeometry geom = MeshListGeometry.CubeOpen();
            geom.CalculateNormal();
            mesh = new DrawMesh(device,geom);


            proj = Matrix4.MakeProjectionAFovYLH(Near, Far, render.FrameSetting.Viewport.AspectRatio);
            view = Matrix4.MakeViewLH(new Vector3(0, 0, 5), new Vector3(0, 0, 10), Vector3.UnitY);


            Matrix4 proj2 = Matrix4.MakeProjectionAFovYLH(10.0f, 50.0f, render.FrameSetting.Viewport.AspectRatio);
            CopyFrustum = new Frustum(proj2 * Matrix4.Identity);
            trackball = new MyTrackBallWASD(this.myRenderControl, input.mouse, input.keyboard,view, proj);
            frustumbox = new FrustumMesh(device);
            effectlight = new EffectLight(device);

            gameLoop = new GameLoop(this);
            gameLoop.AddLoopableFunction(RenderWorld, TimeStepMode.LimitFrameRate, 1000.0f/60);
            gameLoop.AddLoopableFunction(UpdateWorld, TimeStepMode.SemiFixedTimestep ,10.0f);
            gameLoop.Start();



            boxs = new List<Matrix4>();
            for (float z = -50; z <= 50; z += 5)
                for (float x = -50; x <= 50; x += 5)
                {
                    if (MathUtils.ABS(x) > 5 || MathUtils.ABS(z) > 5)
                        boxs.Add(Matrix4.Translating(x, 0, z));
                }
            comboBoxProjection.SelectedIndex = 0;


            MyRot = Matrix4.Identity;


            OBBox box0 = new OBBox(MyTrasl*MyRot);

            List<Vector3> Point = new List<Vector3>();
            for (int i = 0; i < 8; i++) Point.Add(box0.Corner(i));

            box = MinVolumeBoundingBox.GetMinimumEnclosedOBB(Point);


            plotter1.Plotter.AddSerie("FPS", 50);
        }


        private void UpdateWorld(double dt, GameLoopFunction info)
        {
            //rot2 = Matrix4.RotationY((float)dt / 2);

           // rot = rot * rot2;

            //View = Matrix4.MakeViewLH(Eye, Targhet, Vector3.UnitY);

            float Velocity = MathUtils.Rad90 / 1000.0f;

            angley += Velocity * (float)dt;

            MyRot = Matrix4.RotateAxis(Vector3.NormalOne, angley);
            MyTrasl = Matrix4.Translating(angley, 0, 0) * Matrix4.RotateAxis(Vector3.NormalOne, 0.01f);
        }


        private void RenderWorld(double dt, GameLoopFunction info)
        {
            input.Update();
            if (render.Device.CanDraw())
            {
                ICamera camera = trackball;

                Frustum frustum = chkFixFrustum.Checked ? CopyFrustum : trackball.Frustum;


                render.ClearDraw(Color.CornflowerBlue);
                render.BeginDraw();

                axis0.Draw(camera.Projection * camera.View, effectline);

                effectlight.Diffuse.Value = Color32.Blue;
                effectlight.LightDirection.Value = new Vector3(10, 5, 1).Normal;

                render.Device.renderstates.cullMode = Engine.Cull.CounterClockwise;


                //this.camera.ApplyTransformation(rot2);
                //Frustum frustum = this.camera.Frustum;

                AABBox box = new AABBox(-Vector3.One,Vector3.One);
                
                Sphere sphere = Sphere.FromAABBox(box);


                Matrix4 myWorld = MyTrasl;
                effectlight.WorldViewProj.Value = camera.Projection * camera.View * myWorld;
                effectlight.WorldInvTraspose.Value = myWorld.WorldInverseTraspose();

                effectlight.Diffuse.Value = Color32.White;
                mesh.Draw(effectlight.TechDiffuseLightingPerPixel);

                foreach (Matrix4 world in boxs)
                {
                    Matrix4 transform = world * MyRot;
                    effectlight.WorldViewProj.Value = camera.Projection * camera.View * transform;
                    effectlight.WorldInvTraspose.Value = transform.WorldInverseTraspose();

                    box.Center = transform.Position;
                    sphere.center = transform.Position;
                    Color32 colore;

                    //Overlap test = frustum.Intersection_AABB(box.min, box.max);
                    //Overlap test = frustum.Intersection_Sphere(sphere.center, sphere.radius);
                    //Overlap test = frustum.Intersection_OBB(transform);
                    /*
                    switch (test)
                    {
                        case Overlap.INSIDE: colore = Color32.Red; break;
                        case Overlap.INTERSECT: colore = Color32.Green; break;
                        case Overlap.OUTSIDE: colore = Color32.Blue; break;
                        default: colore = Color32.Black; break;

                    }
                   */
                    if (PrimitiveIntersections.Intersect_OBB_OBB(transform, myWorld)) colore = Color32.Red; else colore = Color32.Blue;

                    effectlight.Diffuse.Value = colore;
                    mesh.Draw(effectlight.TechDiffuseLightingPerPixel);

                }



                effectline.WorldViewProj.Value = camera.Projection * camera.View * frustum.Transform;
                effectline.Color.Value = Color32.Red;

                frustumbox.DrawAsLine(effectline.TechColor);
                frustumbox.DrawAsLine(effectlight.TechDiffuseColor);

                //frustumbox.DrawPointsName(font, render.FrameSetting.Viewport, Proj * View, NewTransform);
                    DrawPlaneOfFrustum(frustum);


                render.EndDraw();
                render.Present();


                int x, y;
                input.mouse.MouseRelativePosition(this.myRenderControl, out x, out y);

                this.toolStripStatusLabel1.Text = string.Format("{0}  N:{1} F{2} M {3}x{4}", render.FrameSetting.ToString(), Near, Far, x, y);
            
                this.myRenderControl.Invalidate();

            }
            plotter1.Plotter.AddValue(0,info.CallsPerSecond);
            
            this.plotter1.Invalidate();

            //firstperson.Paint(this.renderControl1.CreateGraphics());
        }

        void UpdateProj()
        {
            /*
            if (this.camera != null)
            {
                this.camera.Far = Far;
                if (Near < 1e-6)
                    this.camera.Near = 0.001f;
                else
                    this.camera.Near = Near;

                this.camera.FovY = UtilsMath.DegreeToRadian(trackFOVY.Value);

            }
            */

            if (render != null)
            {

                float n = 0.1f;
                float f = 100.0f;
                float y = MathUtils.Rad45;// UtilsMath.DegreeToRadian(trackFOVY.Value);

                switch (comboBoxProjection.SelectedIndex)
                {
                    default:
                        Proj = Matrix4.MakeProjectionAFovYLH(n, f, render.FrameSetting.AspectRation, y);
                        break;

                    case 1:
                        Proj = Matrix4.MakeProjectionFovXYLH(n, f, y, MathUtils.DegreeToRadian(trackFOVX.Value));
                        break;

                    case 2:
                        Proj = Matrix4.MakeOrthoLH(n, f, -10, 10, 10, -10);
                        break;
                }

            }
        }


        void DrawPlaneOfFrustum(Frustum frustum)
        {
            Matrix4 PV = trackball.Projection * trackball.View;


            for (int i = 0; i < 6; i++)
            {
                Plane plane = frustum.m_plane[i];

                planemesh.DrawPlane(effectline, PV, plane, i);

                // this is the equivalent maths
                Vector3 p0 = Vector3.Project(plane.Origin, render.FrameSetting.Viewport, PV);

                Vector3 p1 = Vector3.Project(plane.Origin + plane.norm*2, render.FrameSetting.Viewport, PV);

                font.Draw(Frustum.m_planename[i].ToString(), (int)p0.x, (int)p0.y, Color32.Black);
                font.Draw(">" + i.ToString(), (int)p1.x, (int)p1.y, Color32.Black);
            }
  
            Vector3[] corner = frustum.m_corner;

            for (int i = 0; i < 8; i++)
            {
                Vector3 p2 = Vector3.Project(corner[i], render.FrameSetting.Viewport, PV);
                font.Draw(Frustum.m_cornername[i].ToString(), (int)p2.x, (int)p2.y, Color32.Green);
            }

        }


        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            UpdateProj();
        }

        private void trackFOVX_ValueChanged(object sender, EventArgs e)
        {
            labelFOVX.Text = "FOVx = "+ trackFOVX.Value.ToString();
            UpdateProj();
        }

        private void trackFOVY_ValueChanged(object sender, EventArgs e)
        {
            labelFOVY.Text = "FOVy = " + trackFOVY.Value.ToString();
            UpdateProj();
        }

        float Far
        {
            get{return (float)trackBarFar.Value;}
            set { trackBarFar.Value = (int)value; UpdateProj(); }
        }
        float Near
        {
            get { return (float)trackBarNear.Value /100.0f; }
            set { trackBarNear.Value = (int)(value*100); UpdateProj(); }
        }

        private void trackBarFar_ValueChanged(object sender, EventArgs e)
        {
            UpdateProj();
            labelFar.Text = "Far " + Far.ToString();

        }

        private void trackBarNear_ValueChanged(object sender, EventArgs e)
        {
            UpdateProj();
            labelNear.Text = "Near " +Near.ToString();
        }

        private void comboBoxProjection_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateProj();

            if (comboBoxProjection.SelectedIndex == 2)
            {
                Eye = new Vector3(5, 10, 0);
                Targhet = new Vector3(5, 0, 0);
            }
            else
            {
                Eye = new Vector3(10, 10, 20);
                Targhet = Vector3.Zero;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            trackball.View = Matrix4.Identity;
            Console.Clear();


            first = true;
        }


        private void chkFixFrustum_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox box = (CheckBox)sender;
            if (box.Checked)
            {
                trackball.Frustum.CloneTo(ref CopyFrustum);
            }
        }

    }
}
