﻿
using System;
using System.Collections.Generic;
using System.Text;

using Engine;
using Engine.Maths;
 

namespace Camera
{
    public class InfinitePlaneMesh
    {
        Device device;
        VertexBuffer vertexbuffer;
        VertexDeclaration vertexdecl;
        IndexBuffer trianglebuffer;

        public InfinitePlaneMesh(Device device)
        {
            this.device = device;

            VertexLayout layout = new VertexLayout();
            layout.Add(0, DeclarationType.Float3, DeclarationUsage.Position);

            vertexdecl = new VertexDeclaration(device, layout);

            vertexbuffer = new VertexBuffer(device, layout, BufferUsage.Managed, 5);
            VertexStream vstream = vertexbuffer.OpenStream();
            vstream.WriteAndIncrement(new Vector3(-1, -1, 0));
            vstream.WriteAndIncrement(new Vector3( 1, -1, 0));
            vstream.WriteAndIncrement(new Vector3( 1,  1, 0));
            vstream.WriteAndIncrement(new Vector3(-1,  1, 0));
            vstream.WriteAndIncrement(new Vector3(-1, -1, 0));

            vertexbuffer.CloseStream();
            vertexbuffer.Count = 5;


        }

        public void DrawPlane(EffectLine effect, Matrix4 ProjView, Plane plane , int planeidx)
        {

            Vector3 z = plane.norm;
            Vector3 y =  Vector3.UnitY;
            Vector3 x = Vector3.Cross(y, z);
            x.Normalize();
            y = Vector3.Cross(z, x);
            y.Normalize();

            Matrix4 matrix = new Matrix4(x, y, z);
            matrix.Position = plane.Origin;

            device.SetVertexDeclaration(vertexdecl);
            device.SetVertexStream(vertexbuffer);

            effect.WorldViewProj.Value = ProjView * matrix;

            foreach (Pass pass in effect.TechVertexColor)
            {
                device.DrawPrimitives(PrimitiveType.LineStrip, 0, 4);
            }
        }
    }
}
