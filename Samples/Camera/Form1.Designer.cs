﻿namespace Camera
{
    partial class MyForm
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Liberare le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.plotter1 = new Engine.Graph.PerformaceGraph();
            this.myRenderControl = new Engine.Forms.RenderControl();
            this.trackFOVX = new System.Windows.Forms.TrackBar();
            this.trackFOVY = new System.Windows.Forms.TrackBar();
            this.labelFOVX = new System.Windows.Forms.Label();
            this.labelFOVY = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.chkFixFrustum = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.comboBoxProjection = new System.Windows.Forms.ComboBox();
            this.labelNear = new System.Windows.Forms.Label();
            this.trackBarNear = new System.Windows.Forms.TrackBar();
            this.labelFar = new System.Windows.Forms.Label();
            this.trackBarFar = new System.Windows.Forms.TrackBar();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            ((System.ComponentModel.ISupportInitialize)(this.trackFOVX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackFOVY)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarNear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarFar)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // plotter1
            // 
            this.plotter1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.plotter1.Location = new System.Drawing.Point(0, 431);
            this.plotter1.Name = "plotter1";
            this.plotter1.Size = new System.Drawing.Size(362, 218);
            this.plotter1.TabIndex = 12;
            // 
            // myRenderControl
            // 
            this.myRenderControl.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(240)))), ((int)(((byte)(244)))));
            this.myRenderControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myRenderControl.Location = new System.Drawing.Point(0, 0);
            this.myRenderControl.Name = "myRenderControl";
            this.myRenderControl.Size = new System.Drawing.Size(537, 649);
            this.myRenderControl.TabIndex = 0;
            // 
            // trackFOVX
            // 
            this.trackFOVX.Location = new System.Drawing.Point(0, 24);
            this.trackFOVX.Maximum = 180;
            this.trackFOVX.Minimum = 1;
            this.trackFOVX.Name = "trackFOVX";
            this.trackFOVX.Size = new System.Drawing.Size(151, 45);
            this.trackFOVX.TabIndex = 1;
            this.trackFOVX.TickFrequency = 10;
            this.trackFOVX.Value = 1;
            this.trackFOVX.ValueChanged += new System.EventHandler(this.trackFOVX_ValueChanged);
            // 
            // trackFOVY
            // 
            this.trackFOVY.Location = new System.Drawing.Point(3, 64);
            this.trackFOVY.Maximum = 180;
            this.trackFOVY.Minimum = 1;
            this.trackFOVY.Name = "trackFOVY";
            this.trackFOVY.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trackFOVY.Size = new System.Drawing.Size(45, 75);
            this.trackFOVY.TabIndex = 2;
            this.trackFOVY.Value = 1;
            this.trackFOVY.ValueChanged += new System.EventHandler(this.trackFOVY_ValueChanged);
            // 
            // labelFOVX
            // 
            this.labelFOVX.AutoSize = true;
            this.labelFOVX.Location = new System.Drawing.Point(10, 8);
            this.labelFOVX.Name = "labelFOVX";
            this.labelFOVX.Size = new System.Drawing.Size(35, 13);
            this.labelFOVX.TabIndex = 3;
            this.labelFOVX.Text = "label1";
            // 
            // labelFOVY
            // 
            this.labelFOVY.AutoSize = true;
            this.labelFOVY.Location = new System.Drawing.Point(0, 142);
            this.labelFOVY.Name = "labelFOVY";
            this.labelFOVY.Size = new System.Drawing.Size(35, 13);
            this.labelFOVY.TabIndex = 4;
            this.labelFOVY.Text = "label1";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.plotter1);
            this.panel1.Controls.Add(this.chkFixFrustum);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.comboBoxProjection);
            this.panel1.Controls.Add(this.labelNear);
            this.panel1.Controls.Add(this.trackBarNear);
            this.panel1.Controls.Add(this.labelFar);
            this.panel1.Controls.Add(this.trackBarFar);
            this.panel1.Controls.Add(this.trackFOVY);
            this.panel1.Controls.Add(this.labelFOVX);
            this.panel1.Controls.Add(this.labelFOVY);
            this.panel1.Controls.Add(this.trackFOVX);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(537, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(362, 649);
            this.panel1.TabIndex = 5;
            // 
            // chkFixFrustum
            // 
            this.chkFixFrustum.AutoSize = true;
            this.chkFixFrustum.Location = new System.Drawing.Point(13, 361);
            this.chkFixFrustum.Name = "chkFixFrustum";
            this.chkFixFrustum.Size = new System.Drawing.Size(79, 17);
            this.chkFixFrustum.TabIndex = 11;
            this.chkFixFrustum.Text = "Fix Frustum";
            this.chkFixFrustum.UseVisualStyleBackColor = true;
            this.chkFixFrustum.CheckedChanged += new System.EventHandler(this.chkFixFrustum_CheckedChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(13, 332);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 10;
            this.button1.Text = "ResetView";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // comboBoxProjection
            // 
            this.comboBoxProjection.FormattingEnabled = true;
            this.comboBoxProjection.Items.AddRange(new object[] {
            "ProjectionFovY",
            "ProjectionFovXY",
            "Orthogonal"});
            this.comboBoxProjection.Location = new System.Drawing.Point(7, 275);
            this.comboBoxProjection.Name = "comboBoxProjection";
            this.comboBoxProjection.Size = new System.Drawing.Size(121, 21);
            this.comboBoxProjection.TabIndex = 9;
            this.comboBoxProjection.SelectedIndexChanged += new System.EventHandler(this.comboBoxProjection_SelectedIndexChanged);
            // 
            // labelNear
            // 
            this.labelNear.AutoSize = true;
            this.labelNear.Location = new System.Drawing.Point(117, 233);
            this.labelNear.Name = "labelNear";
            this.labelNear.Size = new System.Drawing.Size(35, 13);
            this.labelNear.TabIndex = 8;
            this.labelNear.Text = "label1";
            // 
            // trackBarNear
            // 
            this.trackBarNear.Location = new System.Drawing.Point(7, 223);
            this.trackBarNear.Maximum = 100000;
            this.trackBarNear.Name = "trackBarNear";
            this.trackBarNear.Size = new System.Drawing.Size(104, 45);
            this.trackBarNear.TabIndex = 7;
            this.trackBarNear.TickFrequency = 10000;
            this.trackBarNear.ValueChanged += new System.EventHandler(this.trackBarNear_ValueChanged);
            // 
            // labelFar
            // 
            this.labelFar.AutoSize = true;
            this.labelFar.Location = new System.Drawing.Point(117, 196);
            this.labelFar.Name = "labelFar";
            this.labelFar.Size = new System.Drawing.Size(35, 13);
            this.labelFar.TabIndex = 6;
            this.labelFar.Text = "label1";
            // 
            // trackBarFar
            // 
            this.trackBarFar.Location = new System.Drawing.Point(7, 186);
            this.trackBarFar.Maximum = 1000;
            this.trackBarFar.Name = "trackBarFar";
            this.trackBarFar.Size = new System.Drawing.Size(104, 45);
            this.trackBarFar.TabIndex = 5;
            this.trackBarFar.TickFrequency = 100;
            this.trackBarFar.ValueChanged += new System.EventHandler(this.trackBarFar_ValueChanged);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 649);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(899, 22);
            this.statusStrip1.TabIndex = 6;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(118, 17);
            this.toolStripStatusLabel1.Text = "toolStripStatusLabel1";
            // 
            // MyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(899, 671);
            this.Controls.Add(this.myRenderControl);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.statusStrip1);
            this.Name = "MyForm";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.trackFOVX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackFOVY)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarNear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarFar)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

  

        #endregion

        private Engine.Forms.RenderControl myRenderControl;
        private System.Windows.Forms.TrackBar trackFOVX;
        private System.Windows.Forms.TrackBar trackFOVY;
        private System.Windows.Forms.Label labelFOVX;
        private System.Windows.Forms.Label labelFOVY;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.Label labelNear;
        private System.Windows.Forms.TrackBar trackBarNear;
        private System.Windows.Forms.Label labelFar;
        private System.Windows.Forms.TrackBar trackBarFar;
        private System.Windows.Forms.ComboBox comboBoxProjection;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox chkFixFrustum;
        private Engine.Graph.PerformaceGraph plotter1;
    }
}

