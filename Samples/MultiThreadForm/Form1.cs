﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Engine;
using Engine.Tools;

//using GameLoop = Engine.Tools.GameLoopOld;


namespace MultiThreadForm
{
    public partial class ProblematicForm : Form
    {
        int i = 0;
        GameLoop loop;


        public ProblematicForm()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            loop = new GameLoop(this);
            loop.AddLoopableFunction(Render, TimeStepMode.Always);
            loop.Start();
        }

        void Render(double time, GameLoopFunction targhet)
        {
            if ((i++ % 10) == 0)
                Console.WriteLine(targhet.PrevDurationMS.ToString("G2") );
            else
                Console.Write(targhet.PrevDurationMS.ToString("G2")+ "\t");


            this.Invalidate();

        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            e.Graphics.Clear(Color.CornflowerBlue);
        }

    }
}
