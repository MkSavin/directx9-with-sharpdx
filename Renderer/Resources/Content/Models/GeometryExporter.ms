-----------------------------------------------------------------------
-- Autor:.......... JOHNWHILE
-- Version:........ Alpha
-- Revision:....... 25.06.2015
-- BugInfo:........ johnwhilemail@gmail.com
-- info:........... exporter to *.model format for my engine
-----------------------------------------------------------------------

fn GetHandleByObj obj = obj.handle
GetObjByHandle = maxops.getnodebyhandle -- fn GetObjByHandle handle= maxops.getnodebyhandle handle
fn sostituisciStringa stringa valore sostituto = (for i=1 to stringa.count do if stringa[i]==valore do stringa[i]=sostituto ; stringa )
fn readStringa fin =
(
	num = readLong fin
	if num==undefined or (abs num)>1000 then ( MessageBox "String lenght are \"undefined\" or > 1000 chars, the script will be stopped for safety" ; undefined )
	else if num!=0 then
	(
		local stringa = stringStream "", fmt = "%"
		for i=1 to num do
		(
			b = readByte fin
			if b==0 then format fmt "" to:stringa --"\n" isn't a good idea :]
			else format fmt (bit.intAsChar b ) to:stringa
		)
		stringa as string
	)
	else ""
)
fn writeStringa fout stringa =
(
	stringa = sostituisciStringa stringa "\\" "/"
	-- e se l'ultimo dato � una stringa ? scrive comunque il byte0
	--writeString fout stringa
	--if flag do fseek fout -1 #seek_cur
	writeLong fout stringa.count
	for i=1 to stringa.count do writeByte fout (bit.charAsInt stringa[i]) #unsigned
)



Struct MyModel
(
	FLOAT1 = 1,
	FLOAT2 = 2,
	FLOAT3 = 3,
    FLOAT4 = 4,
	FLOAT4x4 = 44,
	INDEX16 = 16,
	INDEX32 = 32,

	VERTEX = 0,
	TEXCOORD = 1,
	NORMALS = 2,
	INDEX = 3,

	MESH3D = 0,
	
	header = "Model_byjohnwhile",
	version = 0,
	geometries = #(),
	
	fn read fin =
	(
		format "> Reading model\n"
		header = readStringa fin
		version = readLong fin
		OK
	),
	fn write fout =
	(	
		format "> Writing % models\n" geometries.count
		writeStringa fout header
		writeLong fout version 
		
		writeLong fout geometries.count
		for handle in geometries do
		(
			obj = GetObjByHandle handle
			
			format "> Writing mesh %\n" obj.name
			
			writeByte fout MESH3D #unsigned
			writeStringa fout obj.name
			tmesh = snapshotAsMesh obj
			nverts = tmesh.numverts
			ntverts = tmesh.numtverts 
			nfaces = tmesh.numfaces
			
			writeByte fout VERTEX #unsigned
			writeByte fout FLOAT3 #unsigned
			writeLong fout nverts
			for i=1 to nverts do
			(
				v = getVert tmesh i
				writeFloat fout v.x
				writeFloat fout v.y
				writeFloat fout v.z
			)
			
			writeByte fout TEXCOORD #unsigned
			writeByte fout FLOAT2 #unsigned
			writeLong fout ntverts
			
			for i=1 to ntverts do
			(
				t = getTVert tmesh i
				writeFloat fout t.x
				writeFloat fout t.y
			)	
			
			writeByte fout NORMALS #unsigned
			writeByte fout FLOAT3 #unsigned
			writeLong fout nverts
			for i=1 to nverts do
			(
				n = getNormal tmesh i
				writeFloat fout n.x
				writeFloat fout n.y
				writeFloat fout n.z
			)
		
			writeByte fout INDEX #unsigned
			writeByte fout INDEX16 #unsigned
			writeLong fout nfaces		
			for i=1 to nfaces do
			(
				f = getFace tmesh i
				writeShort fout f.x #unsigned
				writeShort fout f.y #unsigned
				writeShort fout f.z #unsigned
			)
			writeStringa fout "no-texture"
		)
		
		OK
	),
	fn getscene =
	(
		geometries.count = 0
		for obj in Objects where SuperClassOf obj == GeometryClass do
		(
			append geometries (GetHandleByObj obj)
		)
		geometries.count
	),
	
	fn savefile =
	(
		filename = getSaveFileName types:"MyModel(*.model)|*.model" caption:"save a generic model"  filename:(sysInfo.currentdir + "/")
		fout = if filename!=undefined then fopen filename "wb" else undefined
		if fout!=undefined then
		(
			st = timestamp()
			print "======================================="
			print "               WRITING model (binary format)              "
			print "======================================="
			if (write fout)!= OK then MessageBox "/\"(T_T)\"\... Error writing !"
			fflush fout
			fclose fout
			format ">> complete in % ms\n" (timeStamp()-st)
		)
		OK
	)
)

clearListener()
model = MyModel()
if model.getscene() > 0 then model.savefile()
