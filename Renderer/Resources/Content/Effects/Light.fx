#include "baseshader.fx"



float3 LightDirection = float3(1,0,0);
float4 Ambient = BLACK; // AmbientColor(WHITE) * AmbientIntensity(0) = BLACK
float4 Light = WHITE;   // LightColor(WHITE)   * LightIntensity(1)
float4 Diffuse = BLUE;  // MaterialColor(BLUE) * LightIntensity(1)
float4 Specular = WHITE;// SpecularColor(WHITE) * SpecularIntensity(1) = WHITE
int    Power = 2.0f;

float3   Eye;
float4x4 World;
float4x4 View;
float4x4 Proj;
float4x4 Camera;            // or inverse of View, can be used to get camera components
float3x3 WorldInvTraspose;  // used for normal conversion when world matrix is a SRT (scale-rotation-traslation)


//-----------------------------------------------------------------------------
// Basic Shader program used for default rendering, the idea is to replace all
// fixed function pipeline with my shader implementetion
//-----------------------------------------------------------------------------
struct VS_INPUT
{
    float4  Position  : POSITION;
	float4  Color     : COLOR;
	float3  Normal    : NORMAL; // using float4 i notice a non-zero w component, why ???
	float2  TexCoord  : TEXCOORD;
};

struct VS_OUTPUT
{   
    float4  Position : POSITION;
	float4  Color    : COLOR0;
	float3  Normal   : TEXCOORD0;
	float3  ViewDir  : TEXCOORD1;
};


//-----------------------------------------------------------------------------
// Vertex Shaders.
//-----------------------------------------------------------------------------

VS_OUTPUT VSVertex(
	float4 inPosition : POSITION ,
	float4 inColor : COLOR)
{

	VS_OUTPUT output = (VS_OUTPUT)0; 
	output.Position = mul(inPosition, WorldViewProj);
	output.Color    = inColor;
	return output;
}

VS_OUTPUT VSVertexNormal(
	float4 inPosition : POSITION , 
	float3 inNormal : NORMAL ,
	float4 inColor : COLOR)
{

	VS_OUTPUT output = (VS_OUTPUT)0;
	output.Position = mul(inPosition, WorldViewProj);
	output.Normal   = GetTrueNormal(inNormal , WorldInvTraspose);
	output.Color    = inColor;
	return output;
}

VS_OUTPUT VSVertexNormalView(
	float4 inPosition : POSITION , 
	float3 inNormal : NORMAL ,
	float4 inColor : COLOR)
{

	VS_OUTPUT output = (VS_OUTPUT)0;
	
	float4 worldpos = mul(mul(inPosition, World), View);

	output.Position = mul(worldpos, Proj);
	output.Normal   = GetTrueNormal(inNormal , WorldInvTraspose);
	output.ViewDir  = normalize(worldpos.xyz - Eye);
	output.Color    = inColor;

	return output;
}


// (I = A + D * N.L + Specular)
VS_OUTPUT VSDiffuseLight(
	float4 inPosition : POSITION , 
	float3 inNormal : NORMAL)
{
	VS_OUTPUT output = (VS_OUTPUT)0; 

	float3 normal   = GetTrueNormal(inNormal , WorldInvTraspose);
	float intensity = dot(normal, LightDirection);

	output.Position = mul(inPosition, WorldViewProj);
	output.Color    = saturate(Ambient + Diffuse * intensity);
	output.Normal   = normal;

	return output;
}



//-----------------------------------------------------------------------------
// Pixel Shaders.
//-----------------------------------------------------------------------------
float4 PSBlack() : COLOR0
{
	return BLACK;
}

float4 PSDiffuseColor() : COLOR0
{
	return Diffuse;
}

float4 PSVertexColor(float4 inputColor : COLOR0) : COLOR0
{
	return inputColor;
}


float4 PSDiffuseLight(float3 inputNormal : TEXCOORD0) : COLOR0
{
	// I = A + D * N.L + (Specular)
    float intensity = dot(inputNormal, LightDirection);
	return saturate(Ambient + Diffuse * intensity);
}

float4 PSSpecularLight(float3 inputNormal : TEXCOORD0 , float3 inputView : TEXCOORD1) : COLOR0
{
	// R = 2 * (N.L) * N - L
	// I = A + D * N.L + S * (R.V)^n

	float intensity = dot(inputNormal, inputView);	

	return saturate(Ambient + Diffuse * intensity);


	//float intensity = dot(inputNormal, LightDirection);	
	//float3 reflection = (2 * intensity * inputNormal) - LightDirection;
	//reflection = pow(saturate(dot(reflection, inputView)), 8);

	//return saturate(Ambient + Diffuse * intensity + Specular * float4(reflection,1));
}


//-----------------------------------------------------------------------------
// Techniques.
//-----------------------------------------------------------------------------

// Use vertex color as output, used example for line rendering
technique VertexColorTechnique
{
    pass p0
    {
        VertexShader = compile vs_2_0 VSVertex();
		PixelShader =  compile ps_2_0 PSVertexColor();
    	
		FillMode = Solid;
    }

}
// Use MaterialDiffuse color as output, no light
technique VertexDiffuseTechnique
{
    pass p0
    {
        VertexShader = compile vs_2_0 VSVertex();
		PixelShader =  compile ps_2_0 PSDiffuseColor();
		FillMode = Solid;
    }

	pass p1
    {
        VertexShader = compile vs_2_0 VSVertex();
		PixelShader =  compile ps_2_0 PSBlack();
		FillMode = Wireframe;
    }
}
// Use DiffuseLight per vertex (I = A + D * N.L)
technique DiffuseLightPerVertex
{
    pass p0
    {
        VertexShader = compile vs_2_0 VSDiffuseLight();
		PixelShader =  compile ps_2_0 PSVertexColor();

		FillMode = Solid;
    }
	pass p1
    {
        VertexShader = compile vs_2_0 VSVertex();
		PixelShader =  compile ps_2_0 PSBlack();

		FillMode = Wireframe;
    }
}
// Use DiffuseLight per pixel (I = A + D * N.L)
technique DiffuseLight
{
    pass p0
    {
        VertexShader = compile vs_2_0 VSVertexNormal();
		PixelShader =  compile ps_2_0 PSDiffuseLight();

		FillMode = Solid;
    }
	pass p1
    {
        VertexShader = compile vs_2_0 VSVertex();
		PixelShader =  compile ps_2_0 PSBlack();

		FillMode = Wireframe;
    }
}
// Use SpecularLight per pixel  (I = A + D * N.L + S * (R.V)^n )
technique SpecularLight
{
	pass p0
    {
        VertexShader = compile vs_2_0 VSVertexNormalView();
		PixelShader =  compile ps_2_0 PSSpecularLight();

		FillMode = Solid;
    }
	pass p1
    {
        VertexShader = compile vs_2_0 VSVertex();
		PixelShader =  compile ps_2_0 PSBlack();

		FillMode = Wireframe;
    }
}