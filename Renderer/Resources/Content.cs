﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Reflection;

namespace Engine.Content
{
    /// <summary>
    /// Loader of Content folder
    /// </summary>
    public static class EngineResources
    {
        public static string AssemblyLocation = "";
        public static string ContentFolder = System.IO.Path.GetFullPath(@"\Content\");

        static string skybox = null;
        static string lighteffect = null;
        static string rendertex = null;
        static string line = null;


        static EngineResources()
        {
            Assembly executingAssembly = Assembly.GetExecutingAssembly();
            AssemblyLocation = Path.GetDirectoryName(executingAssembly.Location);
        }

        public static string LineHLSL
        {
            get { if (line == null)line = ContentFolder + @"\Effects\Line.fx"; return line; }
        }

        public static string SkyBoxHLSL
        {
            get { if (skybox == null)skybox = ContentFolder + @"\Effects\SkyBox.fx"; return skybox; }
        }
        public static string LightHLSL
        {
            get { if (lighteffect == null)lighteffect = ContentFolder + @"\Effects\Light.fx"; return lighteffect; }
        }
        public static string RenderTextureHLSL
        {
            get { if (rendertex == null)rendertex = ContentFolder + @"\Effects\RenderTexture.fx"; return rendertex; }
        }
        public static string SkyBoxTextureCube
        {
            get
            {
                string filename = ContentFolder + @"\Textures\SkyBox.dds";
                if (!File.Exists(filename)) throw new ArgumentNullException("Can't find file : " + filename.ToString());
                return filename;
            }
        }
        public static string SunsetTextureCube
        {
            get
            {
                string filename = ContentFolder + @"\Textures\Sunset.dds";
                if (!File.Exists(filename)) throw new ArgumentNullException("Can't find file : " + filename.ToString());
                return filename;
            }
        }
        public static string DefaultTexture
        {
            get
            {
                string filename = ContentFolder + @"\Textures\emptytexture0.bmp";
                if (!File.Exists(filename)) throw new ArgumentNullException("Can't find file : " + filename.ToString());
                return filename;
            }
        }
    }
}
