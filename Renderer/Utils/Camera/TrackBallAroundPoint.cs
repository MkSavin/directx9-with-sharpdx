﻿using Engine.Inputs;
using Engine.Maths;
using Engine.Tools;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Engine.Tools
{

    /// <summary>
    /// Work in progress
    /// </summary>
    public class TrackBallAroundPoint : Camera
    {
        enum Mousing
        {
            None = 0,
            Traslation,
            Rotating,
            Zooming
        }

        MouseDevice mouse;
        KeyboardDevice keyboard;
        Control control;
        Viewport viewport;
        float velocity = 1;
        //float zoom = 1;
        Mousing mousing = Mousing.None;


        public Vector3 Around { get; set; }

        public TrackBallAroundPoint(Control control, InputManager input)
            : this(
            control, input.mouse, input.keyboard, 
            Matrix4.MakeViewLH(new Vector3(10, 10, 10), Vector3.Zero, Vector3.UnitY),
            Matrix4.MakeProjectionAFovYLH(0.1f, 100.0f, (new Viewport(control.ClientRectangle)).AspectRatio))
        {

        }


        public TrackBallAroundPoint(Control control, InputManager input, Matrix4 view, Matrix4 proj)
            : this(control, input.mouse, input.keyboard, view, proj)
        { }


        public TrackBallAroundPoint(Control control, MouseDevice mouse, KeyboardDevice keyboard, Matrix4 view, Matrix4 proj)
            : base(proj, view)
        {
            Around = Vector3.Zero;

            this.control = control;
            this.mouse = mouse;
            this.keyboard = keyboard;

            keyboard.KeyPress += OnKeyPress;
            mouse.MouseMove += OnMouseMove;
            mouse.MouseDown += OnMouseDown;
            mouse.MouseUp += OnMouseUp;
            mouse.MouseWheel += OnMouseWheel;
            control.Resize += OnControlResize;

            this.viewport = new Viewport(control.ClientRectangle);
        }


        ~TrackBallAroundPoint()
        {
            keyboard.KeyPress -= OnKeyPress;
            mouse.MouseMove -= OnMouseMove;
            mouse.MouseDown -= OnMouseDown;
            mouse.MouseUp -= OnMouseUp;
            control.Resize -= OnControlResize;
        }

        void OnMouseWheel(MouseDevice sender)
        {
            int sign = 1;
            if (sender.MouseMovement.z < 0) sign=-1;

            mousing = Mousing.Zooming;

            CameraView = Matrix4.Translating(base.Forward * velocity * sign) * cameraview;

        }

        void OnMouseUp(MouseDevice sender, MouseButtonEventArg arg)
        {
            mousing = Mousing.None;
        }

        void OnMouseDown(MouseDevice sender, MouseButtonEventArg arg)
        {
            if (arg.button == MouseButton.Left)
                mousing = Mousing.Traslation;
            else if (arg.button == MouseButton.Right)
                mousing = Mousing.Rotating;
        }

        void OnMouseMove(MouseDevice sender)
        {
            if (mousing == Mousing.Rotating)
            {
                float dx = sender.MouseMovement.x * 0.004f;
                float dy = (1 - sender.MouseMovement.y) * 0.004f;

                //Matrix4 traslation = Matrix4.Translating(Around);
                //Matrix4 invtraslation = traslation; Matrix4.InvertTMatrix(ref invtraslation);

                Matrix4 rotation = Matrix4.RotationX(dy) * Matrix4.RotationY(dx) * Matrix4.RotationZ(dx * 0.2f);
                // Matrix4.InvertRMatrix(ref rotation);

                //CameraView = traslation * rotation * invtraslation * cameraview;
                CameraView = rotation * cameraview;


            }
            else if (mousing == Mousing.Traslation)
            {
                float dx = sender.MouseMovement.x * velocity * 0.05f;
                float dy = (1 - sender.MouseMovement.y) * velocity * 0.05f;

                Vector3 movement = Left * dx + Up * dy;
                Around = Around + movement;
                cameraview.Position -= movement;
                CameraView = cameraview;
            }
        }

        void OnKeyPress(KeyboardDevice sender, KeyboardButtonEventArg btn)
        {
            bool needupdate = false;
            switch (btn.button)
            {
                case KeyboardButton.W: cameraview.Position += Forward * velocity; needupdate = true; break;
                case KeyboardButton.A: cameraview.Position -= Left * velocity; needupdate = true; break;
                case KeyboardButton.S: cameraview.Position -= Forward * velocity; needupdate = true; break;
                case KeyboardButton.D: cameraview.Position += Left * velocity; needupdate = true; break;
                case KeyboardButton.C: cameraview.Position += Up * velocity; needupdate = true; break;
                case KeyboardButton.X: cameraview.Position -= Up * velocity; needupdate = true; break;


                case KeyboardButton.Q: cameraview = cameraview * Matrix4.RotationZ(MathUtils.DegreeToRadian(2 * velocity)); needupdate = true; break;
                case KeyboardButton.E: cameraview = cameraview * Matrix4.RotationZ(MathUtils.DegreeToRadian(-2 * velocity)); needupdate = true; break;


            }
            if (needupdate)
            {
                CameraView = cameraview;
            }
        }


        void OnControlResize(object sender, EventArgs e)
        {
            Control ctrl = (Control)sender;
            viewport = new Viewport(control.ClientRectangle);
            AspectRatio = viewport.AspectRatio;
        }
    }
}
