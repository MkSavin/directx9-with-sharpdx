﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;


using Engine.Tools;
using Engine.Maths;
using Engine.Inputs;

namespace Engine.Tools
{
    /// <summary>
    /// The Event's Controller of trackballcamera, need a MouseDevice and window control.
    /// </summary>
    public class MyTrackBall : TrackBallCameraMath
    {
        MouseDevice input;
        Control control;

        public MyTrackBall(Control control, MouseDevice input, Matrix4 view, Matrix4 proj)
            : base(new Viewport(control.ClientRectangle), view, proj)
        {
            InitEvents(control,input);
        }

        /// <summary>
        /// </summary>
        /// <param name="control">The ball require information about size of panel, at this moment viewport match with clientrectangle</param>
        public MyTrackBall(Control control, MouseDevice input, Vector3 Eye, Vector3 Targhet, float near, float far)
            : base(new Viewport(control.ClientRectangle), Eye, Targhet, near, far)
        {
            InitEvents(control,input);
        }

        ~MyTrackBall()
        {
            DeleteEvents();
        }

        void InitEvents(Control control , MouseDevice input)
        {
            this.input = input;
            input.MouseDown += OnMouseDown;
            input.MouseUp += OnMouseUp;
            input.MouseWheel += OnMouseWheel;
            input.MouseMove += OnMouseMove;

            this.control = control;
            control.Resize += OnControlResize;
        }

        void DeleteEvents()
        {
            input.MouseDown -= OnMouseDown;
            input.MouseUp -= OnMouseUp;
            input.MouseWheel -= OnMouseWheel;
            input.MouseMove -= OnMouseMove;
            control.Resize -= OnControlResize;
        }

        protected override void GetMouseCoord(out int X, out int Y)
        {
            input.MouseRelativePosition(control, out X, out Y);
        }

        void OnMouseDown(MouseDevice sender, MouseButtonEventArg button)
        {
            if (button.button == MouseButton.Left)
            {
                base.MouseTraslateDown();
            }
            else if (button.button == MouseButton.Right)
            {
                base.MouseRotateDown();
            }

        }

        void OnMouseUp(MouseDevice sender, MouseButtonEventArg button)
        {
            base.MouseUp();
        }

        void OnMouseWheel(MouseDevice sender)
        {
            base.MouseWheeling(sender.MouseMovement.z);
        }

        void OnMouseMove(MouseDevice sender)
        {
            if (mousing != Mousing.None)
            {
                base.MouseMoving();
            }
        }

        void OnControlResize(object sender, EventArgs e)
        {
            Control ctrl = (Control)sender;
            Viewport viewport = new Viewport(control.ClientRectangle);
            base.ViewportResize(viewport);
        }


    }
}
