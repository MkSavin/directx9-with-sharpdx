﻿

using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

using Engine.Tools;
using Engine.Inputs;
using Engine.Maths;
 


namespace Engine.Tools
{
    /// <summary>
    /// Work In Progress, using keyboard do add a more accurate control for a trackball camera
    /// <para> W/S = Forward/Backward </para>
    /// <para> A/D = Left/Right </para>
    /// <para> Q/R = Roll </para>
    /// <para> C/X = Up/Down </para>
    /// <para> MouseAxeY = Pitch</para>
    /// <para> MouseAxeX = Yaw </para>
    /// <para> MouseAxeZ = Velocity of WASD </para>
    /// </summary>
    public class MyTrackBallWASD : Camera
    {
        enum Mousing
        {
            None = 0,
            Traslation,
            Rotating
        }

        MouseDevice mouse;
        KeyboardDevice keyboard;
        Control control;
        Viewport viewport;
        float velocity=1;
        Mousing mousing =  Mousing.None;

        public MyTrackBallWASD(Control control, InputManager input)
            : this(
            control, input.mouse, input.keyboard, 
            Matrix4.MakeViewLH(new Vector3(10, 10, 10), Vector3.Zero, Vector3.UnitY),
            Matrix4.MakeProjectionAFovYLH(0.1f, 100.0f, (new Viewport(control.ClientRectangle)).AspectRatio))
        {

        }


        public MyTrackBallWASD(Control control, InputManager input, Matrix4 view, Matrix4 proj)
            : this(control, input.mouse, input.keyboard, view, proj)
        { }
        

        public MyTrackBallWASD(Control control, MouseDevice mouse, KeyboardDevice keyboard, Matrix4 view, Matrix4 proj)
            : base(proj, view)
        {
            this.control = control;
            this.mouse = mouse;
            this.keyboard = keyboard;

            keyboard.KeyPress += OnKeyPress;
            mouse.MouseMove += OnMouseMove;
            mouse.MouseDown += OnMouseDown;
            mouse.MouseUp += OnMouseUp;
            mouse.MouseWheel += OnMouseWheel;
            control.Resize += OnControlResize;

            this.viewport = new Viewport(control.ClientRectangle);
        }



        ~MyTrackBallWASD()
        {
            keyboard.KeyPress -= OnKeyPress;
            mouse.MouseMove -= OnMouseMove;
            mouse.MouseDown -= OnMouseDown;
            mouse.MouseUp -= OnMouseUp;
            control.Resize -= OnControlResize;
        }

        void OnMouseWheel(MouseDevice sender)
        {
            if (sender.MouseMovement.z > 0) velocity *= 1.2f;
            else velocity *= 0.8f;
        }

        void OnMouseUp(MouseDevice sender, MouseButtonEventArg arg)
        {
            mousing = Mousing.None;
        }

        void OnMouseDown(MouseDevice sender, MouseButtonEventArg arg)
        {
            if (arg.button == MouseButton.Left)
                mousing = Mousing.Traslation;
            else if (arg.button == MouseButton.Right)
                mousing = Mousing.Rotating;
        }

        void OnMouseMove(MouseDevice sender)
        {
            if (mousing == Mousing.Rotating)
            {
                float dx = sender.MouseMovement.x * 0.004f;
                float dy = (1 - sender.MouseMovement.y) * 0.004f;
                Matrix4 rotation = Matrix4.RotationX(dy) * Matrix4.RotationY(-dx) * Matrix4.RotationZ(dx * 0.2f);
                CameraView = cameraview * rotation;
            }
            else if (mousing == Mousing.Traslation)
            {
                float dx = sender.MouseMovement.x * velocity * 0.05f;
                float dy = (1 - sender.MouseMovement.y) * velocity * 0.05f;
                cameraview.Position -= Left * dx + Up * dy;
                CameraView = cameraview;
            }
        }

        void OnKeyPress(KeyboardDevice sender, KeyboardButtonEventArg btn)
        {
            bool needupdate = false;
            switch (btn.button)
            {
                case KeyboardButton.W: cameraview.Position += Forward * velocity; needupdate = true; break;
                case KeyboardButton.A: cameraview.Position -= Left * velocity; needupdate = true; break;
                case KeyboardButton.S: cameraview.Position -= Forward * velocity; needupdate = true; break;
                case KeyboardButton.D: cameraview.Position += Left * velocity; needupdate = true; break;
                case KeyboardButton.C: cameraview.Position += Up * velocity; needupdate = true; break;
                case KeyboardButton.X: cameraview.Position -= Up * velocity; needupdate = true; break;


                case KeyboardButton.Q: cameraview = cameraview * Matrix4.RotationZ(MathUtils.DegreeToRadian(2 * velocity)); needupdate = true; break;
                case KeyboardButton.E: cameraview = cameraview * Matrix4.RotationZ(MathUtils.DegreeToRadian(-2 * velocity)); needupdate = true; break;


            }
            if (needupdate)
            {
                CameraView = cameraview;
            }
        }


        void OnControlResize(object sender, EventArgs e)
        {
            Control ctrl = (Control)sender;
            viewport = new Viewport(control.ClientRectangle);
            AspectRatio = viewport.AspectRatio;
        }
    }

}
