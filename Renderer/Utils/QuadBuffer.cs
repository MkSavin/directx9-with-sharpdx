﻿using System;
using System.Collections.Generic;
using System.Text;

using DX = SharpDX;
using D3D = SharpDX.Direct3D9;

using Engine.Maths;

namespace Engine.Tools
{
    /// <summary>
    /// A simple static quad, can be managed because use a very small memory size.
    /// Used expecialy for "Deferred Shading" techniques or to Write a texture to a another set
    /// as first render targhet in device.
    /// </summary>
    /// <remarks>
    /// Why a class ? because it can be a singleton class, the static geometies can be initialized
    /// only once in your program and used for all cases.
    /// </remarks>
    public class QuadBuffer2D
    {
        bool asRectangle;
        public VertexBuffer vertexbuffer;
        public VertexDeclaration declaration;
        public Vector2[] points;


        public QuadBuffer2D(Device device , bool asRectangle = false)
        {
            this.asRectangle = asRectangle;

            // create a simple static quad, can be managed because use a very small memory size
            VertexLayout layout = new VertexLayout();
            layout.Add(0, DeclarationType.Float2, DeclarationUsage.Position);

          
            if (asRectangle)
            {
                points = new Vector2[5];
                points[0] = new Vector2(0, 0);
                points[1] = new Vector2(1, 0);
                points[2] = new Vector2(1, 1);
                points[3] = new Vector2(0, 1);
                points[4] = new Vector2(0, 0);
            }
            else
            {
                points = new Vector2[6];
                points[0] = new Vector2(0, 0);
                points[1] = new Vector2(1, 0);
                points[2] = new Vector2(1, 1);
                points[3] = new Vector2(0, 0);
                points[4] = new Vector2(1, 1);
                points[5] = new Vector2(0, 1);
            }

            vertexbuffer = new VertexBuffer(device, layout, BufferUsage.Managed, points.Length);
            VertexStream vstream = vertexbuffer.OpenStream();
            vstream.WriteCollection<Vector2>(points, 0, points.Length, 0);
            vertexbuffer.CloseStream();
            vertexbuffer.Count = points.Length;
            declaration = new VertexDeclaration(device, layout);
        }

        public void DrawQuad(Device device)
        {
            if (asRectangle) device.DrawPrimitives(PrimitiveType.LineStrip, 0, 4);
            else device.DrawPrimitives(PrimitiveType.TriangleList, 0, 2);
        }
    }
}
