﻿using System;
using System.Collections.Generic;
using System.Text;

 
using Engine.Maths;
using Engine.Tools;
using Engine.Content;

namespace Engine
{
    public class SkyBoxHLSL : Effect
    {
        EffectParamTextureCUBE skyboxtexture ;
        EffectParamMatrix4 viewproj;

        EffectTechnique skysolid;
        EffectTechnique skywireframe;

        public SkyBoxHLSL(Device device)
            : base(device, EngineResources.SkyBoxHLSL)
        {
            if (DX9Enumerations.PixelShaderVersion.Major < 2) throw new NotSupportedException("Require at least a graphic card with PixelShader2.0");

            skyboxtexture = new EffectParamTextureCUBE(this, "SkyBoxTexture");
            viewproj = new EffectParamMatrix4(this, "WorldViewProj");
            skysolid = new EffectTechnique(this, "SkyBoxSolid");
            skywireframe = new EffectTechnique(this, "SkyBoxSolidWireframe");
            Technique = skysolid;
        }

        public TextureCUBE SkyBoxTexture
        {
            get { return (TextureCUBE)skyboxtexture.Value; }
            set { skyboxtexture.Value = value; }
        }

        public Matrix4 ProjView
        {
            get { return viewproj.Value; }
            set { viewproj.Value = value; }
        }

        public bool WhiteWireframe
        {
            get { return Technique == skywireframe; }
            set { Technique = value ? skywireframe : skysolid; }
        }

    }


    public class SkyBox
    {
        /// <summary>
        /// The Cube Triangles, remember to Set CullMode.CW to see the interior instead exterior
        /// </summary>
        static byte[] indices = new byte[] 
        { 
            2,1,0, 2,3,1,
            3,5,1, 3,7,5,
            7,4,5, 7,6,4,
            6,0,4, 6,2,0,
            2,7,3, 2,6,7,
            1,4,0, 1,5,4      
        };

        /// <summary>
        /// The Cube vertices
        /// </summary>
        static Vector3[] corners = new Vector3[]
        {
            new Vector3(-1, -1, -1),
            new Vector3( 1, -1, -1),
            new Vector3(-1,  1, -1),
            new Vector3( 1,  1, -1),
            new Vector3(-1, -1,  1),
            new Vector3( 1, -1,  1),
            new Vector3(-1,  1,  1),
            new Vector3( 1,  1,  1)
        };
        /// <summary>
        /// Distance from center when lookat vector is in one of the corners
        /// </summary>
        static float MaxDistance = (float)Math.Sqrt(3);
        /// <summary>
        /// Depend by Fov of projection matrix, for safety set to camera eye
        /// </summary>
        static float MinDistance = 0f;

        static float M22 = MaxDistance / (MaxDistance - MinDistance);
        static float M23 = -M22 * MinDistance;


        Device device;
        VertexDeclaration vertexdeclaration;
        VertexBuffer vertexbuffer;
        IndexBuffer indexbuffer;
        SkyBoxHLSL effect;

        /// <summary>
        /// </summary>
        /// <param name="device"></param>
        /// <param name="filename">a dds cube texture</param>
        public SkyBox(Device device, string filename)
        {
            this.device = device;

            /////////////////////// SHADER ///////////////////////
            effect = new SkyBoxHLSL(device);

            /////////////////////// TEXTURE ///////////////////////
            effect.SkyBoxTexture = TextureCUBE.FromFilename(device, filename);

            /////////////////////// GEOMETRY ///////////////////////
            vertexdeclaration = new VertexDeclaration(device, VERTEX.m_elements);

            vertexbuffer = new VertexBuffer(device, vertexdeclaration.Format, BufferUsage.Managed, 8);
            VertexStream vstream = vertexbuffer.OpenStream();
            foreach (Vector3 v in corners) vstream.WriteAndIncrement(v);
            vertexbuffer.CloseStream();
            vertexbuffer.Count = 8;

            indexbuffer = new IndexBuffer(device, IndexLayout.Face16, BufferUsage.Managed, 12);
            IndexStream istream = indexbuffer.OpenStream();
            foreach (byte idx in indices) istream.WriteAndIncrement((ushort)idx);
            indexbuffer.CloseStream();
            indexbuffer.Count = 12;

        }


        void Draw(Matrix4 View, Matrix4 Proj)
        {
            // faster way to optain a matrix multiplication, the relative slow code is
            // View.Position = Vector3.Zero;
            // skymatrix = Projection * View;
            Matrix4 viewproj = View;
            viewproj.m03 = viewproj.m13 = viewproj.m23 = 0.0f;

            // to optimize the projection matrix, i re-setting the near and far clip plane, isn't very necessary
            // because i already force in vertexshader the depth to 1 for every case
            // Q = far / (far - near);
            // m22 = Q; m23 = -Q * near;
            Matrix4 proj = Proj;
            //proj.m22 = M22;
            //proj.m23 = M23;

            viewproj.PreMultiply(ref proj);

            effect.ProjView = viewproj;

            Cull prevcull = device.renderstates.cullMode;
            FillMode prevfill = device.renderstates.fillMode;

            device.renderstates.cullMode = Cull.Clockwise;
            device.renderstates.fillMode = FillMode.Solid;
            device.renderstates.lightEnable = false;

            int passcount = effect.Begin();
            for (int i = 0; i < passcount; i++)
            {
                //render.Device.renderstates.fillMode = i == 0 ? FillMode.Solid : FillMode.WireFrame;

                effect.BeginPass(i);
                device.SetVertexDeclaration(vertexdeclaration);
                device.SetVertexStream(vertexbuffer);
                device.SetIndexStream(indexbuffer);
                device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, vertexbuffer.Count, 0, indexbuffer.Count);
                effect.EndPass();
            }
            effect.End();

            device.renderstates.cullMode = prevcull;
            device.renderstates.fillMode = prevfill;
        }


        /// <summary>
        /// <para>RenderState changes:</para>
        /// <para>cullMode = CW</para>
        /// <para>FillMode = Solid</para>
        /// <para>ZBufferEnable = false</para>
        /// <para>LightEnable = false</para>
        /// <para>ZBufferWriteEnable = false</para>
        /// </summary>
        public void DrawFirst(Matrix4 View, Matrix4 Projection)
        {
            // Disable the z buffer, skybox don't need this calculation so can be omited to increase performance
            device.renderstates.ZBufferEnable = false;
            device.renderstates.ZBufferWriteEnable = false;
            Draw(View, Projection);

        }

        /// <summary>
        /// <para>RenderState changes:</para>
        /// <para>cullMode = CW</para>
        /// <para>FillMode = Solid</para>
        /// <para>ZBufferEnable = true</para>
        /// <para>ZBufferWriteEnable = false</para>
        /// <para>LightEnable = false</para>
        /// </summary>
        public void DrawLast(Matrix4 View, Matrix4 Projection)
        {
            // ZBufferEnable test is necessary otherwise overwrite all
            device.renderstates.ZBufferEnable = true;
            device.renderstates.ZBufferWriteEnable = true;
            Draw(View, Projection);
        }
    }
}
