﻿using System;
using System.Collections.Generic;
using System.Text;

using Engine.Maths;
using Engine.Tools;

namespace Engine
{

    public class Axis
    {
        Device device;
        VertexBuffer vb;
        IndexBuffer ib;
        VertexDeclaration vdecl;
        Matrix4 world;

        //bool first = true;
        public Axis(Device renderer, float scale)
            : this(renderer, scale, Vector3.Zero)
        { }

        public Axis(Device device, float scale, Vector3 pos)
        {
            this.world = Matrix4.Translating(pos) * Matrix4.Scaling(scale, scale, scale);
            this.device = device;

            VertexLayout vlayout = new VertexLayout();
            vlayout.Add(0, DeclarationType.Float3, DeclarationUsage.Position);
            vlayout.Add(0, DeclarationType.Color, DeclarationUsage.Color);

            Vector3[] v = new Vector3[] { Vector3.Zero, Vector3.UnitX, Vector3.Zero, Vector3.UnitY, Vector3.Zero, Vector3.UnitZ };
            Color32[] c = new Color32[] { Color32.Red, Color32.Red, Color32.Green, Color32.Green, Color32.Blue, Color32.Blue };

            vb = new VertexBuffer(device, vlayout, BufferUsage.Managed, 6);
            VertexStream vstream = vb.OpenStream();

            vstream.WriteCollection<Vector3>(v, vlayout.Elements[0], 0, 6, 0);
            vstream.WriteCollection<Color32>(c, vlayout.Elements[1], 0, 6, 0);

            /*
            vstream.WriteAndIncrement(Vector3.Zero);
            vstream.WriteAndIncrement(Color32.Red);

            vstream.WriteAndIncrement(Vector3.UnitX * scale);
            vstream.WriteAndIncrement(Color32.Red);

            vstream.WriteAndIncrement(Vector3.Zero);
            vstream.WriteAndIncrement(Color32.Green);

            vstream.WriteAndIncrement(Vector3.UnitY * scale);
            vstream.WriteAndIncrement(Color32.Green);

            vstream.WriteAndIncrement(Vector3.Zero);
            vstream.WriteAndIncrement(Color32.Blue);

            vstream.WriteAndIncrement(Vector3.UnitZ * scale);
            vstream.WriteAndIncrement(Color32.Blue);
            */

            vb.CloseStream();
            vb.Count = 6;

            ib = new IndexBuffer(device, IndexLayout.One16, BufferUsage.Managed, 3);
            IndexStream istream = ib.OpenStream();
            istream.WriteCollection<ushort>(new ushort[] { 0, 1, 2 }, 0, 3, 0);
            ib.CloseStream();

            vdecl = new VertexDeclaration(device, vlayout);

        }

        public void Draw(Matrix4 ProjView, EffectLine effect)
        {
            effect.Technique = effect.TechVertexColor;
            effect.WorldViewProj.Value = ProjView * world;
            
            device.SetVertexDeclaration(vdecl);
            device.SetVertexStream(vb, 0);

            int count = effect.Begin();

            for (int i = 0; i < count; i++)
            {
                effect.BeginPass(i);
                device.DrawPrimitives(PrimitiveType.LineList, 0, 3);
                effect.EndPass();
            }
            effect.End();
        }
        public void Draw(ICamera camera, EffectLine effect)
        {
            Draw(camera.Projection * camera.View, effect);
        }

    }
}
