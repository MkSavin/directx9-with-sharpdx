﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;

using DX = SharpDX;
using D3D = SharpDX.Direct3D9;
 

namespace Engine
{
    /// <summary>
    /// A list of all video adapters installed in you poor pc ! Two video card ? not make fun
    /// </summary>
    public static class VideoCards
    {
        public static readonly VideoAdapter Adapter0;
        public static readonly VideoAdapter Adapter1;

        static D3D.Direct3D d3d;

        static VideoCards()
        {
            d3d = new D3D.Direct3D();
            VideoCardCount = d3d.AdapterCount;
            Adapter0 = VideoCardCount > 0 ? new VideoAdapter(d3d, 0) : null;
            Adapter1 = VideoCardCount > 1 ? new VideoAdapter(d3d, 1) : null;
        }

        public static int VideoCardCount { get; private set; }

    }
    /// <summary>
    /// the graphic card
    /// </summary>
    public class VideoAdapter
    { 
        Version oneone;
        D3D.Capabilities caps;
        D3D.Direct3D d3d;

        internal VideoAdapter( D3D.Direct3D d3d , int adapter)
        {
            this.d3d = d3d;
            this.adapterIdx = adapter;
            oneone =  new Version(1, 1);
            caps = d3d.GetDeviceCaps(adapter, D3D.DeviceType.Hardware);
        }

        /// <summary>
        /// Index of current graphic card
        /// </summary>
        public int adapterIdx { get; private set; }

        public DisplayMode CurrentDisplayMode
        {
            get
            {
                D3D.DisplayMode d = d3d.GetAdapterDisplayMode(adapterIdx);
                return new DisplayMode { width = d.Width, height = d.Height, refreshRate = d.RefreshRate, format = (Format)d.Format };
            }
        }


        static bool isPowerOfTwo(int x)
        {
            return
                x == 1 || x == 2 || x == 4 || x == 8 || x == 16 || x == 32 || x == 64 ||
                x == 128 || x == 256 || x == 512 || x == 1024 || x == 2048 || x == 4096 ||
                x == 8192 || x == 16384 || x == 32768;
        }
    }

    /// <summary>
    /// SharpDx : Enumerate all graphic card capacities, only for First default adaptes
    /// </summary>
    public static class DX9Enumerations
    {
        const int adapter = 0;

        static Version oneone = new Version(1, 1);
        static D3D.Direct3D d3d = new D3D.Direct3D();
        static D3D.Capabilities caps = d3d.GetDeviceCaps(adapter, D3D.DeviceType.Hardware);


        // faster method because texture can't be greater than 8192
        static bool isPowerOfTwo(int x)
        {
            return
                x == 1 || x == 2 || x == 4 || x == 8 || x == 16 || x == 32 || x == 64 ||
                x == 128 || x == 256 || x == 512 || x == 1024 || x == 2048 || x == 4096 ||
                x == 8192 || x == 16384 || x == 32768;
        }


        /// <summary>
        /// Check the screen format, old card can support only 16bit pixels
        /// </summary>
        public static bool CheckDeviceType(Format DisplayFormat,Format BackBufferFormat,bool windowed)
        {
            DX.Result result;
            bool test = d3d.CheckDeviceType(adapter, D3D.DeviceType.Hardware, (D3D.Format)DisplayFormat, (D3D.Format)BackBufferFormat, windowed, out result);
            if (result.Failure) test = false;
            return test;
        }

        public static bool CheckDepthStencil(Format AdapterFormat, DepthFormat DepthStencilFormat)
        {
            return d3d.CheckDeviceFormat(0, D3D.DeviceType.Hardware, (D3D.Format)AdapterFormat, D3D.Usage.DepthStencil, D3D.ResourceType.Surface, (D3D.Format)DepthStencilFormat);
        }

        public static Version PixelShaderVersion
        {
            get { return caps.PixelShaderVersion; }
        }
        public static Version VertexShaderVersion
        {
            get { return caps.VertexShaderVersion; }
        }
        public static int MaxVertexShaderConst
        {
            get { return caps.MaxVertexShaderConst; }
        }
        /// <summary>
        /// check if a resolution is supported
        /// </summary>
        public static bool CheckDisplayMode(int width, int height, Format format)
        {
            D3D.AdapterCollection collection = d3d.Adapters;
            foreach (D3D.AdapterInformation info in collection)
            {
                D3D.DisplayMode d = info.CurrentDisplayMode;
                if (d.Width == width & d.Height == height & d.Format.Equals(format))
                    return true;
            }
            return false;
        }
        /// <summary>
        /// </summary>
        public static List<DisplayMode> GetViewsMode(Format format)
        {
            int count = d3d.GetAdapterModeCount(adapter, (D3D.Format)format);
            List<DisplayMode> list = new List<DisplayMode>(count);
            for (int i = 0; i < count; i++)
            {
                D3D.DisplayMode d = d3d.EnumAdapterModes(0, (D3D.Format)format, i);
                list.Add(new DisplayMode { width = d.Width, height = d.Height, refreshRate = d.RefreshRate, format = (Format)d.Format });
            }
            return list;
        }

        public static DisplayMode CurrentDisplayMode
        {
            get
            {
                D3D.DisplayMode d = d3d.GetAdapterDisplayMode(adapter);
                return new DisplayMode { width = d.Width, height = d.Height, refreshRate = d.RefreshRate, format = (Format)d.Format };
            }
        }


        public static bool SupportDynamicTexture
        {
            get { return (caps.Caps2 & D3D.Caps2.DynamicTextures) != 0; }
        }
        /// <summary>
        /// Check size
        /// </summary>
        /// <param name="sizes">{width,heigth} for texture2d ; {width,height,depth} for texture3d</param>
        public static bool SupportTexture(ResourceType textureType, out string message, params int[] sizes)
        {
            message = "ok";
            
            // is a problem when you load a texture without specified size because i don't implement a tested before load a file
            int width = 0;
            int height = 0;
            int depth = 0;

            if (sizes.Length > 0) width = sizes[0];
            if (sizes.Length > 1) height = sizes[1];
            if (sizes.Length > 2) depth = sizes[2];

            //for volume all size use MaxVolumeExtent
            if (textureType == ResourceType.VolumeTexture)
            {
                for (int i = 0; i < 3; i++)
                {
                    if (sizes[i] > 0 && (caps.MaxVolumeExtent < sizes[i] || (caps.TextureCaps & D3D.TextureCaps.Pow2) != 0 && (!isPowerOfTwo(sizes[i]))))
                    {
                        message = "size " + sizes[i] + " for volume texture not supported";
                        return false;
                    }
                }
            }
            else
            {
                
                if (width > 0 && (caps.MaxTextureWidth < width || (caps.TextureCaps & D3D.TextureCaps.Pow2) != 0 && (!isPowerOfTwo(width))))
                {
                    message = "width size not supported";
                    return false;
                }
                if (height > 0 && (caps.MaxTextureHeight < height || (caps.TextureCaps & D3D.TextureCaps.Pow2) != 0 && (!isPowerOfTwo(height))))
                {
                    message = "height size not supported";
                    return false;
                }
                // don't know if depth size require to be squared
                if (width > 0 && height > 0 && (caps.TextureCaps & D3D.TextureCaps.SquareOnly) != 0 && height != width)
                {
                    message = "Require square size";
                    return false;
                }
            }
            return true;
        }
        /// <summary>
        /// Check format
        /// </summary>
        public static bool SupportTexture(ResourceType textureType, Format pixelformat, out string message)
        {
            if (textureType != ResourceType.Texture && textureType != ResourceType.CubeTexture && textureType != ResourceType.VolumeTexture)
                throw new ArgumentException("please pass a valid resource type");

            message = "ok";
            // is a problem when you load a texture without specified format
            if (pixelformat == Format.Unknown) return true;

            if (!d3d.CheckDeviceFormat(0, D3D.DeviceType.Hardware, (D3D.Format)CurrentDisplayMode.format, 0, (D3D.ResourceType)textureType, (D3D.Format)pixelformat))
            {
                message = "Format " + pixelformat.ToString() + " not supported by device";
                return false;
            }
            return true;
        }

        /// <summary>
        /// Some check about using of tex2Dlod() in HSLS, not all texture are supported
        /// </summary>
        public static bool SupportVertexTextureFetch(Format pixelformat, out string message)
        {
            message = "ok";

            if (VertexShaderVersion.Major<3)
            {
                message = "Require ShaderModel3.0";
                return false;
            }
            if (!d3d.CheckDeviceFormat(0, D3D.DeviceType.Hardware, (D3D.Format)CurrentDisplayMode.format, D3D.Usage.QueryVertexTexture, D3D.ResourceType.Texture, (D3D.Format)pixelformat))
            {
                message = "Texture Format not supported, in vertex shader tex2Dlod(float2,0,mipmap) will return a float4(0,0,0,0)";
                return false;
            }
            return true;
        }

        /// <summary>
        /// Max Deph in Texture3D
        /// </summary>
        public static int MaxDepthVolumeTexture
        {
            get { return caps.MaxVolumeExtent; }
        }

        /// <summary>
        /// Max simultaneous textures, Directx9 can support max 8 textures
        /// </summary>
        public static int MaxActiveTexture
        {
            get { return caps.MaxSimultaneousTextures; }
        }
        /// <summary>
        /// Used to know if i can use uint instead ushort indices format, remember that 0xFFFF for ushort is used
        /// for reset primitives counter example in Triangle.Fan
        /// </summary>
        public static int MaxVertexIndex
        {
            get { return caps.MaxVertexIndex; }
        }

        public static int MaxPrimitiveCount
        {
            get { return caps.MaxPrimitiveCount; }
        }

        public static int MaxRenderTarghetCount
        {
            get { return caps.SimultaneousRTCount; }
        }

        /// <summary>
        /// Directx9 constant = 8
        /// </summary>
        public static int MaxLights
        {
            get { return caps.MaxActiveLights; }
        }
        public static bool SupportsHardwareTransformAndLight
        {
            get { return (caps.DeviceCaps & D3D.DeviceCaps.HWTransformAndLight) !=0 ; }
        }
        public static bool SupportsPureDevice
        {
            get { return (caps.DeviceCaps & D3D.DeviceCaps.PureDevice) != 0; }
        }

        /// <summary>
        /// To use Effect HLSL require a vs_1_1 and ps_1_1, the fixed function pipeline will be deprecated in my rendering engine
        /// </summary>
        public static bool SupportsShader
        {
            get { return caps.VertexShaderVersion >= oneone && caps.PixelShaderVersion >= oneone; }
        }


        public static void Check(Format format)
        {
            int count = d3d.GetAdapterModeCount(adapter, (D3D.Format)format);


            for (int i=0;i<count;i++)
            {
                D3D.DisplayMode d = d3d.EnumAdapterModes(0, (D3D.Format)format, i);
                Console.WriteLine(d);
            }

        }
    }
}
