﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Engine.Tools;
using Engine.Maths;

using GameLoop = Engine.Tools.GameLoopOld;


namespace Engine.Forms
{
    /// <summary>
    /// Semplified render windows.
    /// </summary>
    public partial class GameForm : Form
    {
        public Font renderFont;
        public FrameSetting setting;
        public RenderWindow render;
        public GameLoop loop;


        int TarghetFPS = 60;
        float UpdateMS = 10.0f;

        string text_design = "SharpDX9 GameForm";
        public System.Drawing.Font font_design;
        

        public GameForm()
            : this(60, 10.0f)
        {

        }
        public GameForm(int TarghetFPS , float UpdateMS)
        {
            this.TarghetFPS = TarghetFPS;
            this.UpdateMS = UpdateMS;

            SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.Opaque | ControlStyles.UserPaint, true);
            //UpdateStyles();

            InitializeComponent();

            this.Name = "GameForm";
            this.Text = "GameForm";

            font_design = new System.Drawing.Font("Calibri", 12, FontStyle.Bold);

            // ALWAYS AFTER InitializeComponent()
            setting = new FrameSetting(true, false, new Viewport(ClientSize), Handle, true);
            render = new RenderWindow(setting);

            loop = new GameLoop(this);   
            loop.AddLoopableFunction(UpdateWindows, TimeStepMode.LimitFrameRate, 1000.0f/ TarghetFPS);
            loop.AddLoopableFunction(Update,TimeStepMode.SemiFixedTimestep, UpdateMS);
            loop.Start();

            renderFont = new Font(render.Device, Engine.Font.FontName.Arial, 14);
        }

        /// <summary>
        /// When rendering control can reduce flikering, but not work for standard directx rendering
        /// </summary>
        public bool UseDoubleBuffer
        {
            get { return DoubleBuffered; }
            set 
            {
                DoubleBuffered = value;
                SetStyle(ControlStyles.OptimizedDoubleBuffer, value);
                UpdateStyles();
            }
        }


        private void UpdateWindows(double alpha, GameLoopFunction info)
        {
            this.Invalidate();
        }

        string str = "GameForm.Render(double, FunctionInfo) not overrided";

        public virtual void Render(double alpha, GameLoopFunction info)
        {
            renderFont.Draw(string.Format("{0}\nFrame: {1}\n{2}", str, render.FrameSetting.ToString(), info.ToString()),
                (Width - 400) / 2, (Height - 8) / 2, Color32.Black);
            
        }
        public virtual void Update(double elapsed, GameLoopFunction info)
        {
            //Console.Write(".");
        }

        protected override void WndProc(ref Message m)
        {
            base.WndProc(ref m);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            
            if (DesignMode)
            {
                e.Graphics.Clear(BackColor);
                base.OnPaint(e);
                var sizeText = e.Graphics.MeasureString(text_design, font_design);
                e.Graphics.DrawString(text_design, font_design, new SolidBrush(Color.Black), (Width - sizeText.Width) / 2, (Height - sizeText.Height) / 2);
            }
            else
            {
                e.Graphics.Clear(BackColor);
                var sizeText = e.Graphics.MeasureString(text_design, font_design);
                e.Graphics.DrawString(text_design, font_design, new SolidBrush(Color.Black), (Width - sizeText.Width) / 2, (Height - sizeText.Height) / 2); 
            }
        }
        protected override void OnPaintBackground(PaintEventArgs e)
        {
            // do nothing do avoid flikering
        }
        protected override void OnResizeBegin(EventArgs e)
        {
 	         base.OnResizeBegin(e);
        }
        protected override void OnResizeEnd(EventArgs e)
        {
            base.OnResizeEnd(e);
            if (render != null)
                render.ResizeBackBuffer(ClientSize);
        }
        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);
            render.Destroy();
            render = null;
        }  
    }
}
