﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Engine.Maths;
using Engine.Tools;

namespace Engine.Forms
{
    /// <summary>
    /// A Renderable UserControl.
    /// https://code.google.com/r/benjaminaautin-justadownload/source/browse/Source/SharpDX/Windows/RenderControl.cs?r=5de4dd66532b0012e6c3c05d7753bcf5ad7758f6
    /// </summary>
    public class RenderControl : UserControl
    {
        // need to used static because foreach new initialization you will optain the same seed
        private static Random rnd = new Random();
        private System.Drawing.Font fontForDesignMode;
        public RenderWindow render;

        /// <summary>
        /// Initializes a new instance of the <see cref="RenderForm"/> class.
        /// </summary>
        public RenderControl()
        {
            SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.Opaque | ControlStyles.UserPaint, true);
            UpdateStyles();
            double R = (rnd.NextDouble() + 2) / 3;
            double G = (rnd.NextDouble() + 2) / 3;
            double B = (rnd.NextDouble() + 2) / 3;

            this.BackColor = Color.FromArgb(clamp(R * 255.0), clamp(G * 255.0), clamp(B * 255.0));
        }
        /// <summary>
        /// Paints the background of the control, do nothing when rendering to avoid flickered
        /// </summary>
        protected override void OnPaintBackground(PaintEventArgs e)
        {
            if (DesignMode) base.OnPaintBackground(e);
        }

        /// <summary>
        /// </summary>
        protected override void OnPaint(PaintEventArgs e)
        {
            if (DesignMode)
            {
                base.OnPaint(e);

                if (fontForDesignMode == null)
                    fontForDesignMode = new System.Drawing.Font("Calibri", 12, FontStyle.Bold);

                e.Graphics.Clear(BackColor);

                string text = "SharpDX9 RenderControl";
                var sizeText = e.Graphics.MeasureString(text, fontForDesignMode);

                e.Graphics.DrawString(text, fontForDesignMode, new SolidBrush(Color.Black), (Width - sizeText.Width) / 2, (Height - sizeText.Height) / 2);
            }
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            if (render != null)
            {
                System.Threading.Thread.Sleep(100);
                render.ResizeBackBuffer(ClientSize);
            }
        }

        public void Destroy()
        {
            render.Destroy();
        }

        public void InitGraphic(RenderWindow mainwindow)
        {
            FrameSetting setting = new FrameSetting(true, false, new Viewport(ClientSize), Handle, true);

            if (mainwindow == null)
            {
                render = new RenderWindow(setting);
            }
            else
            {
                render = new RenderWindow(setting, mainwindow.Device);
            }
        }


        int clamp(double value) { return value > 255 ? 255 : value < 0 ? 0 : (int)value; }

    }
}
