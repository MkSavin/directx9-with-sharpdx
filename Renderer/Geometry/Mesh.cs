﻿using System;
using System.Collections.Generic;
using System.Text;

using Engine.Maths;
using Engine.Tools;
using Engine.Geometry;

namespace Engine
{
    public class Mesh
    {

        public Mesh()
        {

        }


        public static Mesh FromWavefrontObjFile(string filename)
        {
            Mesh mesh = new Mesh();


            return mesh;
        }


    }




    public class DrawMesh
    {
        MeshListGeometry mesh;
        Device device;
        VertexBuffer vertexbuffer;
        VertexDeclaration vertexdecl;
        IndexBuffer indexbuffer;


        public Matrix4 transform { get; set; }

        public DrawMesh(Device device, MeshListGeometry mesh)
        {
            this.device = device;
            this.mesh = mesh;
            this.transform = Matrix4.Identity;

            VertexLayout layout = new VertexLayout();
            layout.Add(0, DeclarationType.Float3, DeclarationUsage.Position);
            layout.Add(0, DeclarationType.Float3, DeclarationUsage.Normal);

            vertexdecl = new VertexDeclaration(device, layout);


            vertexbuffer = new VertexBuffer(device, layout, BufferUsage.Managed, mesh.numVertices);
            VertexStream vstream = vertexbuffer.OpenStream();
            vstream.WriteCollection<Vector3>(mesh.vertices, 0, mesh.numVertices, 0);
            vstream.WriteCollection<Vector3>(mesh.normals, 0, mesh.numVertices, 0);
            vertexbuffer.CloseStream();
            vertexbuffer.Count = mesh.numVertices;


            indexbuffer = new IndexBuffer(device, IndexLayout.Face16, BufferUsage.Managed, mesh.numPrimitives);
            IndexStream istream = indexbuffer.OpenStream();
            istream.WriteCollection<Face16>(mesh.indices, 0, 0);
            indexbuffer.CloseStream();
            indexbuffer.Count = mesh.numPrimitives;

        }


        public void Draw(EffectTechnique technique)
        {
            device.SetVertexDeclaration(vertexdecl);
            device.SetVertexStream(vertexbuffer, 0);
            device.SetIndexStream(indexbuffer);

            foreach (Pass pass in technique)
            {
                if (indexbuffer != null)
                    device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, mesh.numVertices, 0, mesh.numPrimitives);
                else
                    device.DrawPrimitives(PrimitiveType.TriangleList, 0, mesh.numPrimitives);
            }
        }

    }

    /// <summary>
    /// Diagnostic mesh to draw a frustum
    /// </summary>
    public class FrustumMesh
    {
        Device device;
        VertexBuffer vertexbuffer;
        VertexDeclaration vertexdecl;
        IndexBuffer trianglebuffer;
        IndexBuffer linebuffer;
        int numVerts;
        int numTris;
        int numLines;


        public FrustumMesh(Device device)
        {
            this.device = device;

            VertexLayout layout = new VertexLayout();
            layout.Add(0, DeclarationType.Float3, DeclarationUsage.Position);
            layout.Add(0, DeclarationType.Color, DeclarationUsage.Color);

            vertexdecl = new VertexDeclaration(device, layout);


            numVerts = FrustumGeometry.vertices.Length;
            numTris = FrustumGeometry.triangles.Length;
            numLines = FrustumGeometry.segments.Length;

            vertexbuffer = new VertexBuffer(device, layout, BufferUsage.Managed, numVerts);
            VertexStream vstream = vertexbuffer.OpenStream();
            vstream.WriteCollection<Vector3>(FrustumGeometry.vertices, layout.Elements[0], 0, numVerts, 0);
            vertexbuffer.CloseStream();
            vertexbuffer.Count = numVerts;


            trianglebuffer = new IndexBuffer(device, IndexLayout.Face16, BufferUsage.Managed, numTris);
            IndexStream istream = trianglebuffer.OpenStream();
            istream.WriteCollection<Face16>(FrustumGeometry.triangles, 0, numTris, 0);
            trianglebuffer.CloseStream();
            trianglebuffer.Count = numTris;

            linebuffer = new IndexBuffer(device, IndexLayout.Edge16, BufferUsage.Managed, numLines);
            istream = linebuffer.OpenStream();
            istream.WriteCollection<Edge16>(FrustumGeometry.segments, 0, numLines, 0);
            linebuffer.CloseStream();
            linebuffer.Count = numLines;

        }
        /// <summary>
        /// Have to set Proj * View * Transform matrix
        /// </summary>
        /// <remarks>
        /// To apply a movement to frustum just to
        /// Transform = movement * Transform
        /// </remarks>
        public void DrawAsMesh(EffectTechnique technique)
        {
            // 
            device.SetVertexDeclaration(vertexdecl);
            device.SetVertexStream(vertexbuffer, 0);
            device.SetIndexStream(trianglebuffer);

            foreach (Pass pass in technique)
            {
                device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, numVerts, 0, numTris);
            }
        }
        /// <summary>
        /// Have to set Proj * View * Transform matrix
        /// </summary>
        public void DrawAsLine(EffectTechnique technique)
        {
            device.SetVertexDeclaration(vertexdecl);
            device.SetVertexStream(vertexbuffer, 0);
            device.SetIndexStream(linebuffer);

            foreach (Pass pass in technique)
            {
                device.DrawIndexedPrimitives(PrimitiveType.LineList, 0, 0, numVerts, 0, numLines);
            }
        }

        /// <summary>
        /// Show how a point are converted to screen
        /// </summary>
        public void DrawPointsName(Font font, Viewport viewport, Matrix4 ProjView , Matrix4 Transform)
        {
            for (int i = 0; i < 8; i++)
            {
                // this is that graphic pipeline do to convert a point to virtual box and fit to viewport
                Vector3 P0 = Vector3.TransformCoordinate(FrustumGeometry.vertices[i], ProjView * Transform);
                P0.x = (P0.x + 1) * 0.5f * viewport.Width + viewport.X;
                P0.y = (1 - P0.y) * 0.5f * viewport.Height + viewport.Y;

                // this is the equivalent maths
                Vector3 P1 = Vector3.TransformCoordinate(FrustumGeometry.vertices[i], Transform);
                P1 = Vector3.Project(P1, viewport, ProjView);

                font.Draw(i.ToString(), (int)P1.x, (int)P1.y, Color32.Black);
            }
        }
    }
}
