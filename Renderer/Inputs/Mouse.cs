﻿//#define DEBUG_

using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

using DX = SharpDX;
using DI = SharpDX.DirectInput;

using Engine.Tools;
 
using Engine.Maths;

namespace Engine.Inputs
{
    /// <summary>
    /// I suppose is correct
    /// </summary>
    public enum MouseButton : byte
    {
        Left = 0,
        Right = 1,
        Middle = 2,
        Button3 = 3,
        Button4 = 4,
        Button5 = 5,
        Button6 = 6,
        Button7 = 7
    }

    /// <summary>
    /// The Mouse input manager, the acquiring is done at the first Update, you can also disable acquiring with
    /// EnableAcquiring = false BEFORE update
    /// </summary>
    public class MouseDevice : InputDevice
    {
        const int MAXBUTTONCOUNT = 8;
        const int BUTTONCOUNT = 3;

        int clamp(int value, int min, int max)
        {
            return value < min ? min : value > max ? max : value;
        }

        /// <summary>
        /// Occurs when mouse button is been pressed first time
        /// </summary>
        public event MouseButtonEventHandler MouseDown;
        /// <summary>
        /// Occurs when mouse button is been release after pressed.
        /// </summary>
        public event MouseButtonEventHandler MouseUp;
        /// <summary>
        /// Occurs when mouse move, also if Cursor is on the screen bound, so check always if cursor do a movement
        /// </summary>
        public event MouseEventHandler MouseMove;
        /// <summary>
        /// Occurs when mouse whell rotate
        /// </summary>
        public event MouseEventHandler MouseWheel;

        DI.Mouse mouse;
        DI.MouseState states;

        Point zero = new Point(0, 0);
        int titleBarHeight;
        int screenW = Screen.PrimaryScreen.Bounds.Width;
        int screenH = Screen.PrimaryScreen.Bounds.Height;


        // all possible key, will be used only first three
        MouseButtonEventArg[] buttonstates = new MouseButtonEventArg[BUTTONCOUNT];

        /// <summary>
        /// The data returned for the x-axis and y-axis of a mouse, indicates the movement of the mouse itself, not the cursor pixel position.
        /// The units of measurement are based on the values returned by the mouse hardware and have nothing to do with pixels
        /// or any other form of screen measurement, for a tipical computer mouse the movement==pixel, but can be different for other hardwares like jeypad
        /// </summary>
        public Vecto3I MouseMovement = new Vecto3I(0, 0, 0);
        /// <summary>
        /// Mouse position on screen in pixels at current Update()
        /// </summary>
        public Vector2I MouseAbsPosition = new Vector2I(Cursor.Position.X, Cursor.Position.Y);
        /// <summary>
        /// Mouse position on screen in pixels at previous Update()
        /// </summary>
        public Vector2I MousePrevAbsPosition = new Vector2I(Cursor.Position.X, Cursor.Position.Y);


        /// <summary>
        /// i don't know that is DI.DirectInput, but is always better initialize only once the same class
        /// </summary>
        /// <param name="mainwindow">SharpDX require Form handle otherwise crash</param>
        internal MouseDevice(Form mainwindow, DI.DirectInput dxi, InputUsage usage = InputUsage.DefaultMouse)
            : base(mainwindow, dxi, usage)
        {
            // preinitializing the unique class to avoid unnecessary new()
            for (int i = 0; i < BUTTONCOUNT; i++)
                buttonstates[i] = new MouseButtonEventArg(this, (MouseButton)i);

            mouse = new DI.Mouse(dxi);

            // just a memo: not valid for keyboard or mouse
            //if (defaultLevel == (DI.CooperativeLevel.Exclusive | DI.CooperativeLevel.Background)) throw new ArgumentException("Level not valid");

            // DISCL_EXCLUSIVE = not share with other programs
            // DISCL_NONEXCLUSIVE = However if you want other applications to have access to keyboard input while your program 
            // is running you can set it to non-exclusive.
            // Just remember that if it is non-exclusive and you are running in a windowed mode then you will need to check 
            // for when the device loses focus and when it re-gains that focus so it can re-acquire the device for use again.
            // This focus loss generally happens when other windows become the main focus over your window or your window is minimized.
            mouse.SetCooperativeLevel(mainwindow.Handle, (DI.CooperativeLevel)usage);
            // Because a mouse is a relative device - unlike a joystick, it does not have a home position - relative data is returned by default.
            mouse.Properties.AxisMode = DI.DeviceAxisMode.Relative;

            states = new DI.MouseState();

            MouseDown = new MouseButtonEventHandler(debugmousedown);
            MouseUp = new MouseButtonEventHandler(debugmouseup);
            MouseMove = new MouseEventHandler(debugmousemove);
            MouseWheel = new MouseEventHandler(debugmousewheel);

            Point pnt = new Point(0, 0);
            Point corner = mainwindow.PointToScreen(pnt);
            titleBarHeight = corner.Y - mainwindow.Location.Y;
        }

        /// <summary>
        /// MainFocusWindow define the focus state of main windows when use NonExclusive flag
        /// </summary>
        /// <param name="mainwindow">SharpDX require Form handle otherwise crash</param>
        public MouseDevice(Form mainwindow, InputUsage usage = InputUsage.DefaultMouse)
            : this(mainwindow, new DI.DirectInput(), usage) { }

        /// <summary>
        /// Read the mouse's device states, a elapsed time in ms was used to understand
        /// the time passed between two frames, example for get double-click event (work in progress)
        /// </summary>
        public void Update(double elapsed = 0.0)
        {
            if (!TryAcquireMouseState()) return;

            //mouse.Properties.Granularity;

            MouseMovement.x = states.X;
            MouseMovement.y = states.Y;
            MouseMovement.z = clamp(states.Z, -1, 1);// my mouse device use +120 -120 values
            MousePrevAbsPosition = MouseAbsPosition;
            MouseAbsPosition.x = Cursor.Position.X;
            MouseAbsPosition.y = Cursor.Position.Y;

            /* Found a Mouse Movement */
            if (MouseMovement.x != 0 || MouseMovement.y != 0) MouseMove(this);

            /* Found a Mouse Wheel Rotation */
            if (MouseMovement.z != 0) MouseWheel(this);

            for (int i = 0; i < BUTTONCOUNT; i++)
            {
                MouseButtonEventArg key = buttonstates[i];

                bool pressed = states.Buttons[i];
                bool prevpressed = key.pressed;

                key.prevstate = key.state;
                key.prevpressed = key.pressed;

                key.pressed = pressed;

                if (pressed)
                {
                    if (prevpressed)
                    {
                        key.state = ButtonState.Hold;
                    }
                    else
                    {
                        // pressed and prev not pressed
                        key.state = ButtonState.Pressed;
                        MouseDown(this, key);
                    }
                }
                else
                {
                    // not pressed and prev pressed
                    if (prevpressed)
                    {
                        key.state = ButtonState.Up;
                        MouseUp(this, key);
                    }
                }
            }
        }


        /// <summary>
        /// Return informations about mouse button at current state
        /// </summary>
        public MouseButtonEventArg GetButtonState(MouseButton button)
        {
            return buttonstates[(int)button];
        }

        /// <summary>
        /// Position of mouse on screen relative to a generic rectangle control
        /// </summary>
        public void MouseRelativePosition(Control control, out int pixelX, out int pixelY)
        {
            // control.Location is relative to parent, not to main form
            Point origin = control.PointToScreen(zero);
            pixelX = clamp(MouseAbsPosition.x - origin.X, 0, control.ClientSize.Width);
            pixelY = clamp(MouseAbsPosition.y - origin.Y, 0, control.ClientSize.Height);
        }


        protected override void Acquire()
        {
            mouse.Acquire();
        }
        protected override void GetCurrentState()
        {
            mouse.GetCurrentState(ref states);
        }
        protected override void Dispose(bool disposing)
        {
            if (!IsDisposed)
            {
                mouse.Unacquire();
                for (int i = 0; i < BUTTONCOUNT; i++)
                    buttonstates[i].Reset();
                if (!mouse.IsDisposed)
                    mouse.Dispose();
            }
        }

#if DEBUG_
        private void debugmousedown(MouseDevice sender, MouseButtonEventArg btn) { Console.WriteLine("OnMouseDown : " + btn.ToString()); }
        private void debugmouseup(MouseDevice sender, MouseButtonEventArg btn) { Console.WriteLine("OnMouseUp : " + btn.ToString()); }
        private void debugmousemove(MouseDevice sender) { Console.WriteLine(string.Format("OnMouseMove by {0}x {1}y ", sender.MouseMovement.x, sender.MouseMovement.y)); }
        private void debugmousewheel(MouseDevice sender) { Console.WriteLine(string.Format("OnMouseWhellRotate by {0}", sender.MouseMovement.z)); }
#else
        private void debugmousedown(MouseDevice sender, MouseButtonEventArg btn) { }
        private void debugmouseup(MouseDevice sender, MouseButtonEventArg btn) { }
        private void debugmousemove(MouseDevice sender) { }
        private void debugmousewheel(MouseDevice sender) { }
#endif


    }
}
