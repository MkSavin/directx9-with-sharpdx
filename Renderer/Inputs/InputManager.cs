﻿#define DEBUG_EXCEPTION


using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

using DX = SharpDX;
using DI = SharpDX.DirectInput;

using Engine.Inputs;
using Engine.Tools;
using System.Runtime.InteropServices;

namespace Engine
{
    [Flags]
    public enum InputUsage : byte
    {
        /// <summary>
        /// Disable acquiring when windows is in background or when focus lost, i don't want interferences with other applications
        /// </summary>
        DefaultKeyboard = Background | NonExclusive,

        /// <summary>
        /// Disable acquiring when windows is not focused, this generate a failure in Acquire()
        /// </summary>
        DefaultMouse = Foreground | NonExclusive,

        //     The application is requesting an exclusive access to the device. If the exclusive
        //     access is authorized, no other instance of the device can get an exclusive
        //     access to the device while it is acquired. Note that non-exclusive access
        //     to the input device is always authorized, even when another application has
        //     an exclusive access. In exclusive mode, an application that acquires the
        //     mouse or keyboard device must unacquire the input device when it receives
        //     a windows event message WM_ENTERSIZEMOVE or WM_ENTERMENULOOP. Otherwise,
        //     the user won't be able to access to the menu or move and resize the window.
        Exclusive = 1,
        //

        //     The application is requesting a non-exclusive access to the device. There
        //     is no interference even if another application is using the same device.
        NonExclusive = 2,


        //     The application is requesting a foreground access to the device. If the foreground
        //     access is authorized and the associated window moves to the background, the
        //     device is automatically unacquired.
        Foreground = 4,


        //     The application is requesting a background access to the device. If background
        //     access is authorized, the device can be acquired even when the associated
        //     window is not the active window.
        Background = 8,

        //     The application is requesting to disable the Windows logo key effect. When
        //     this flag is set, the user cannot perturbate the application. However, when
        //     the default action mapping UI is displayed, the Windows logo key is operating
        //     as long as that UI is opened. Consequently, this flag has no effect in this
        //     situation.
        NoWinKey = 16
    }



    public class InputManager
    {
        DI.DirectInput directinput;
        Form window;
        Control control;

        public readonly KeyboardDevice keyboard;
        public readonly MouseDevice mouse;

        /// <summary>
        /// The control must be Focused to output inputs
        /// </summary>
        public InputManager(Control control)
        {
            this.control = control;
            this.window = control.FindForm();
            directinput = new DI.DirectInput();
            keyboard = new KeyboardDevice(window, directinput);
            mouse = new MouseDevice(window, directinput);
        }

        public void Update(double elapsed = 0.0, bool forcefocused = false)
        {
            bool focused = forcefocused || (InputDevice.MainWindowsCanReceiveInput(window) && InputDevice.ControlCanReceiveInput(control));

            mouse.EnableAcquiring = focused;
            keyboard.EnableAcquiring = focused;

            if (focused)
            {
                keyboard.Update(elapsed);
                mouse.Update(elapsed);
            }
        }
    }


    /// <summary>
    /// The initial state of EnableAcquiring is false becuase acquireing will be done af first update
    /// </summary>
    public abstract class InputDevice : DisposableResource
    {
        protected InputUsage inputusage;
        protected Form windows;
        protected Control control;

        bool acquiring = false;
        bool tryacquirebefore = false;
   
        /// <summary>
        /// i don't know that is DI.DirectInput, but is always better initialize only once the same class
        /// </summary>
        internal InputDevice(Control control, DI.DirectInput dxi, InputUsage usage)
        {
            if (usage == (InputUsage.Background | InputUsage.Exclusive))
            {
                throw new NotImplementedException("Sharpdx don't want this flag");
            }

            windows = control.FindForm();
            inputusage = usage;
            EnableAcquiring = false;
        }

        /// <summary>
        /// </summary>
        ~InputDevice()
        {
            Dispose();
        }


        /// <summary>
        /// I found a way to understand if MainWindows can receive input when you want NotExclusive application,
        /// can be use to disagle acquiring inputs.
        /// </summary>
        public static bool MainWindowsCanReceiveInput(Form MainWindow)
        {
            return MainWindow == Form.ActiveForm;
            //return MainWindow.ActiveControl == null ? false : MainWindow.ActiveControl.Focused && MainWindow.WindowState != FormWindowState.Minimized;
        }

        /// <summary>
        /// Filtred the Focused cases (when control are selected by mouse)
        /// </summary>
        public static bool ControlCanReceiveInput(Control Control)
        {
            return Control.Focused;
        }


        /// <summary>
        /// If you find a way to know if windows lost/resume focus and you use NotExclusive application, didable acquireing manualy
        /// to avoid continuos try of acquire() function.
        /// </summary>
        public bool EnableAcquiring
        {
            get { return acquiring; }
            set
            {
                // only to avoid IsAcquired() exception, remember to do acquire before get device state
                tryacquirebefore = acquiring != value;
                acquiring = value;
            }
        }

        /// <summary>
        /// Call before Update, return false if can't read input device
        /// </summary>
        protected bool TryAcquireMouseState()
        {
            if (!acquiring) return false;

            if (tryacquirebefore || !IsAcquired())
            {
                if (!TryAcquire()) return false;
                else if (!IsAcquired()) return false;
            }
            tryacquirebefore = false;
            return true;
        }

        /// <summary>
        /// I can't find a way to know if mouse or keyboard is acquirable, just use try catch
        /// </summary>
        private bool TryAcquire()
        {
            try
            {
                Acquire();
                return true;
            }
#if DEBUG_EXCEPTION
            catch (DX.SharpDXException ex)
            {
                Console.WriteLine("Can't Mouse.Acquire " + ex.ResultCode);
                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Can't Mouse.Acquire "+ ex.Message);
                return false;
            }
#else
            catch
            {
                return false;
            }
#endif
        }
        
        /// <summary>
        /// try to get input device state, if can't, is possible you forget to check and to acquire with TryAcquire() function
        /// </summary>
        private bool IsAcquired()
        {
            try
            {
                GetCurrentState();
                return true;
            }
#if DEBUG_EXCEPTION
            catch (DX.SharpDXException ex)
            {
                Console.WriteLine("Can't Mouse.GetCurrentState " + ex.ResultCode);
                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Can't Mouse.GetCurrentState " + ex.Message);
                return false;
            }
#else
            catch
            {
                return false;
            }
#endif

        }

        protected abstract void Acquire();
        protected abstract void GetCurrentState();
    }
}
