﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace Engine.Inputs
{
    /// <summary>
    /// Used for both keyboard and mouse keys/buttons
    /// </summary>
    public enum ButtonState : byte
    {
        None = 0,
        Pressed = 1,
        Hold = 2,
        Up = 3
    }

    /// <summary>
    /// A button event contain also previous state for debug
    /// </summary>
    public abstract class ButtonEventArg
    {
        public ButtonState state;
        public ButtonState prevstate;
        public bool prevpressed;
        public bool pressed;

        public ButtonEventArg()
        {
            Reset();
        }

        public void Reset()
        {
            prevstate = state = ButtonState.None;
            pressed = prevpressed = false;
        }
        public override string ToString()
        {
            return string.Format("prev:{0} press:{1}", prevpressed, pressed);
        }
    }

    /// <summary>
    /// Contain the argument inerent to mouse button
    /// </summary>
    public class MouseButtonEventArg : ButtonEventArg
    {
        MouseDevice device;
        public readonly MouseButton button;

        public MouseButtonEventArg(MouseDevice Device, MouseButton Button):base()
        {
            device = Device;
            button = Button;
        }

        public override string ToString()
        {
            return string.Format("{0} movement:{1} {2}", button, device.MouseMovement, base.ToString());
        }
    }
   
    /// <summary>
    /// Contain the argument inerent to mouse button
    /// </summary>
    public class KeyboardButtonEventArg : ButtonEventArg
    {
        KeyboardDevice device;
        public readonly KeyboardButton button;

        public KeyboardButtonEventArg(KeyboardDevice Device, KeyboardButton Button):base()
        {
            device = Device;
            button = Button;
        }
        public override string ToString()
        {
            return string.Format("{0} {1}", button, base.ToString());
        }
    }

    /// <summary>
    /// My specified event for keyboard clicking
    /// </summary>
    public delegate void KeyboardButtonEventHandler(KeyboardDevice sender, KeyboardButtonEventArg arg);
    /// <summary>
    /// My specified event for mouse clicking
    /// </summary>
    public delegate void MouseButtonEventHandler(MouseDevice sender, MouseButtonEventArg arg);
    /// <summary>
    /// My specified event for mouse moving (not mouse button)
    /// </summary>
    public delegate void MouseEventHandler(MouseDevice sender);

}
