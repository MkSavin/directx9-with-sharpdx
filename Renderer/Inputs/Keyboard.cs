﻿//#define DEBUG_


using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Collections;

using DX = SharpDX;
using DI = SharpDX.DirectInput;

using Engine.Tools;

namespace Engine.Inputs
{
    /// <summary>
    /// different from windows form's keys
    /// </summary>
    public enum KeyboardButton : byte
    {
        Unknown = 0,
        Escape = 1,
        D1 = 2,
        D2 = 3,
        D3 = 4,
        D4 = 5,
        D5 = 6,
        D6 = 7,
        D7 = 8,
        D8 = 9,
        D9 = 10,
        D0 = 11,
        Minus = 12,
        Equals = 13,
        Back = 14,
        Tab = 15,
        Q = 16,
        W = 17,
        E = 18,
        R = 19,
        T = 20,
        Y = 21,
        U = 22,
        I = 23,
        O = 24,
        P = 25,
        LeftBracket = 26,
        RightBracket = 27,
        Return = 28,
        LeftControl = 29,
        A = 30,
        S = 31,
        D = 32,
        F = 33,
        G = 34,
        H = 35,
        J = 36,
        K = 37,
        L = 38,
        Semicolon = 39,
        Apostrophe = 40,
        Grave = 41,
        LeftShift = 42,
        Backslash = 43,
        Z = 44,
        X = 45,
        C = 46,
        V = 47,
        B = 48,
        N = 49,
        M = 50,
        Comma = 51,
        Period = 52,
        Slash = 53,
        RightShift = 54,
        Multiply = 55,
        LeftAlt = 56,
        Space = 57,
        Capital = 58,
        F1 = 59,
        F2 = 60,
        F3 = 61,
        F4 = 62,
        F5 = 63,
        F6 = 64,
        F7 = 65,
        F8 = 66,
        F9 = 67,
        F10 = 68,
        NumberLock = 69,
        ScrollLock = 70,
        NumberPad7 = 71,
        NumberPad8 = 72,
        NumberPad9 = 73,
        Subtract = 74,
        NumberPad4 = 75,
        NumberPad5 = 76,
        NumberPad6 = 77,
        Add = 78,
        NumberPad1 = 79,
        NumberPad2 = 80,
        NumberPad3 = 81,
        NumberPad0 = 82,
        Decimal = 83,
        Oem102 = 86,
        F11 = 87,
        F12 = 88,
        F13 = 100,
        F14 = 101,
        F15 = 102,
        Kana = 112,
        AbntC1 = 115,
        Convert = 121,
        NoConvert = 123,
        Yen = 125,
        AbntC2 = 126,
        NumberPadEquals = 141,
        PreviousTrack = 144,
        AT = 145,
        Colon = 146,
        Underline = 147,
        Kanji = 148,
        Stop = 149,
        AX = 150,
        Unlabeled = 151,
        NextTrack = 153,
        NumberPadEnter = 156,
        RightControl = 157,
        Mute = 160,
        Calculator = 161,
        PlayPause = 162,
        MediaStop = 164,
        VolumeDown = 174,
        VolumeUp = 176,
        WebHome = 178,
        NumberPadComma = 179,
        Divide = 181,
        PrintScreen = 183,
        RightAlt = 184,
        Pause = 197,
        Home = 199,
        Up = 200,
        PageUp = 201,
        Left = 203,
        Right = 205,
        End = 207,
        Down = 208,
        PageDown = 209,
        Insert = 210,
        Delete = 211,
        LeftWindowsKey = 219,
        RightWindowsKey = 220,
        Applications = 221,
        Power = 222,
        Sleep = 223,
        Wake = 227,
        WebSearch = 229,
        WebFavorites = 230,
        WebRefresh = 231,
        WebStop = 232,
        WebForward = 233,
        WebBack = 234,
        MyComputer = 235,
        Mail = 236,
        MediaSelect = 237,
    }
    /// <summary>
    /// Very efficent input than using windows.forms.onkeydown events
    /// </summary>
    public class KeyboardDevice : InputDevice
    {
        const int MAXBUTTONCOUNT = 256;
        const int BUTTONCOUNT = 237;

        // the keyboard device
        DI.Keyboard keyboard;
        DI.KeyboardState states;

        // all possible key, preinitialize to call new() only once
        KeyboardButtonEventArg[] buttonstates = new KeyboardButtonEventArg[MAXBUTTONCOUNT];


        /// <summary>
        /// All key pressed, no distinction between KeyDown or KeyHold 
        /// </summary>
        public event KeyboardButtonEventHandler KeyPress;
        /// <summary>
        /// Occurs when keyboard button is been pressed first time
        /// </summary>
        public event KeyboardButtonEventHandler KeyDown;
        /// <summary>
        /// Occurs when keyboard button is been release after pressed.
        /// </summary>
        public event KeyboardButtonEventHandler KeyUp;
        /// <summary>
        /// Occurs when keyboard button continue to be pressed.
        /// </summary>
        public event KeyboardButtonEventHandler KeyHold;


        /// <summary>
        /// i don't know that is DI.DirectInput, but is always better initialize only once the same class
        /// </summary>
        /// <param name="mainwindow">SharpDX require Form handle otherwise crash</param>
        internal KeyboardDevice(Form mainwindow, DI.DirectInput dxi, InputUsage usage = InputUsage.DefaultKeyboard)
            : base(mainwindow, dxi, usage)
        {
            // preinitializing the unique class to avoid unnecessary new()
            for (int i = 0; i < BUTTONCOUNT; i++)
                buttonstates[i] = new KeyboardButtonEventArg(this, (KeyboardButton)i);

            keyboard = new DI.Keyboard(dxi);
            keyboard.SetCooperativeLevel(mainwindow.Handle, (DI.CooperativeLevel)usage);
            states = new DI.KeyboardState();


            KeyPress = new KeyboardButtonEventHandler(debugkeydown);
            KeyDown = new KeyboardButtonEventHandler(debugkeydown);
            KeyUp = new KeyboardButtonEventHandler(debugkeyup);
            KeyHold = new KeyboardButtonEventHandler(debugkeypress);
        }

        /// <summary>
        /// MainFocusWindow define the focus state of main windows when use NonExclusive flag
        /// </summary>
        /// <param name="mainwindow">SharpDX require Form handle otherwise crash</param>
        public KeyboardDevice(Form mainwindow, InputUsage usage = InputUsage.DefaultKeyboard)
            : this(mainwindow, new DI.DirectInput(), usage) { }


        /// <summary>
        /// Read the keyboard's device states, a elapsed time in ms was used to understand
        /// the time passed between two frames, example to get multy-key press (work in progress)
        /// </summary>
        public void Update(double elapsed = 0.0)
        {
            if (!TryAcquireMouseState()) return;

            // copy previous state
            for (int i = 0; i < BUTTONCOUNT; i++)
            {
                buttonstates[i].prevpressed = buttonstates[i].pressed;
                buttonstates[i].pressed = false;
            }

            // update all pressed keys;
            foreach (DI.Key k in states.PressedKeys)
            {
                KeyboardButtonEventArg key = buttonstates[(int)k];
                key.pressed = true;

                // key previously not pressed
                if (!key.prevpressed)
                {
                    key.pressed = true;
                    KeyDown(this, key);
                }

                KeyPress(this, key);
            }
            // get all key not pressed but pressed previously
            for (int i = 0; i < BUTTONCOUNT; i++)
            {
                KeyboardButtonEventArg key = buttonstates[i];

                if (key.prevpressed)
                {
                    if (!key.pressed)
                    {
                        KeyUp(this, key);
                    }
                    else
                    {
                        KeyHold(this, key);
                    }
                }
            }
        }

        /// <summary>
        /// Return informations about keyboard button at current state
        /// </summary>
        public KeyboardButtonEventArg GetButtonState(KeyboardButton button)
        {
            return buttonstates[(int)button];
        }


        protected override void Acquire()
        {
            keyboard.Acquire();
        }

        protected override void GetCurrentState()
        {
            keyboard.GetCurrentState(ref states);
        }


        protected override void Dispose(bool disposing)
        {
            if (!IsDisposed)
            {
                keyboard.Unacquire();
                if (!keyboard.IsDisposed) keyboard.Dispose();
            }
        }

#if DEBUG_
        private void debugkeydown(KeyboardDevice sender, KeyboardButtonEventArg btn){Console.WriteLine("OnKeyDown : " + btn.ToString());}
        private void debugkeyup(KeyboardDevice sender, KeyboardButtonEventArg btn){Console.WriteLine("OnKeyUp : " + btn.ToString());}
        private void debugkeyhold(KeyboardDevice sender, KeyboardButtonEventArg btn) { Console.WriteLine("OnKeyHold : " + btn.ToString()); }
        private void debugkeypress(KeyboardDevice sender, KeyboardButtonEventArg btn) { Console.WriteLine("OnKeyPress : " + btn.ToString()); }
#else
        private void debugkeydown(KeyboardDevice sender, KeyboardButtonEventArg btn) {  }
        private void debugkeyup(KeyboardDevice sender, KeyboardButtonEventArg btn) {  }
        private void debugkeyhold(KeyboardDevice sender, KeyboardButtonEventArg btn) { }
        private void debugkeypress(KeyboardDevice sender, KeyboardButtonEventArg btn) { }

#endif


    }


}
