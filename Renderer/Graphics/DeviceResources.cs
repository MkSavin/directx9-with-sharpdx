﻿using System;
using System.Collections.Generic;
using System.Text;
using Engine.Tools;

namespace Engine
{
    /// <summary>
    /// All possible and tested combinations
    /// </summary>
    public enum ResourceUsage
    {
        /// <summary> 
        /// Auto-restorable when device reset.
        /// Full access.
        /// Fast but require a copy in the ram
        /// </summary>
        Managed,
        /// <summary>
        /// Not-restorable when device reset, only write, not optimized to be updated
        /// Best performance.
        /// NOT LOCKABLE IF TEXTURE
        /// </summary>
        Static,
        /// <summary> 
        /// Not-restorable when device reset, only write, optimized to be updated
        /// Better performance than DynamicReadable
        /// </summary>
        Dynamic,
        /// <summary>
        /// Not-restorable when device reset, full access, optimized to be updated
        /// Worst performance
        /// </summary>
        DynamicReadable
    }

    /// <summary>
    /// The common operation for all device's resources
    /// </summary>
    public interface IResource : IDisposable
    {
        /// <summary>
        /// When device are lost, disposes non-managed resources before device reseting
        /// </summary>
        void DeviceLost();
        /// <summary>
        /// After device was reseted and IsEnabled flag true, recreate the resources.
        /// For vertex, index and texture need a rewrite function
        /// </summary>
        void DeviceRestore();
        /// <summary>
        /// Tell if resource are currently enabled or not, if disabled the DeviceRestore don't make effect 
        /// </summary>
        bool IsEnabled { get; set; }
        /// <summary>
        /// Tell if resource are disposed, so you can check if need recreate them
        /// </summary>
        bool IsDisposed { get; }
        /// <summary>
        /// Destruction implementation, the .NET Framework garbage collector implicitly manages the allocation and release of memory
        /// so you can't know when desctructors was called
        /// </summary>
        void Destroy();
    }


    public abstract class DeviceResource : IResource
    {
        protected Device device;
        protected bool disposed = true;
        protected bool managed;


        /// <summary>
        /// for buffer i need to calculate the manage flag, so i need to implement after constructor
        /// </summary>
        /// <param name="device"></param>
        internal DeviceResource(Device device)
        {
            this.device = device;
        }
        internal void SetManagedFlag(bool managed)
        {
            this.managed = managed;
            device.resoucesmanager.Add(this);
        }


        public DeviceResource(Device device,  bool managed)
        {
            this.device = device;
            this.managed = managed;
            device.resoucesmanager.Add(this);
        }

        ~DeviceResource()
        {
            Destroy();
        }

        /// <summary>
        /// When resources are disabled, DeviceRestore don't recreate it
        /// </summary>
        public virtual bool IsEnabled { get; set; }

        public bool IsDisposed
        {
            get { return disposed; }
        }

        /// <summary>
        /// Auto-Managed by device, if resources create with Pool.Default isn't managed
        /// </summary>
        public bool IsManaged
        {
            get { return managed; }
        }

        /// <summary>
        /// Dispose not mean Destroy, dispose the device resouces but can be restored
        /// </summary>
        public abstract void Dispose();

        /// <summary>
        /// Dispose not mean Destroy, destroy remove phisicaly this resouces, force garbage collector to remove it
        /// TODO : is the time for suppress finealizer ???
        /// </summary>
        public virtual void Destroy()
        {
            if (!disposed) Dispose();
            device.resoucesmanager.Remove(this);
        }
        /// <summary>
        /// Call before device reseting, managed resources don't need this implementation
        /// DeviceLost require that all unmanaged resourcer must be disposed before reset, else crash
        /// </summary>
        public abstract void DeviceLost();
        /// <summary>
        /// Call after device reseting, managed resources don't need this implementation.
        /// Carefull for unmanaged resources, vertexbuffer, indexbuffer, texture etc.. must be re-initialized
        /// </summary>
        public abstract void DeviceRestore();

    }


    public class ResourceManager
    {
        protected List<DeviceResource> UnManaged;
        protected List<DeviceResource> Managed;
        
        public ResourceManager()
        {
            UnManaged = new List<DeviceResource>();
            Managed = new List<DeviceResource>();
        }

        public void Add(DeviceResource resource)
        {
            if (resource.IsManaged) Managed.Add(resource);
            else UnManaged.Add(resource);
        }
        public void Remove(DeviceResource resource)
        {
            if (resource.IsManaged) Managed.Remove(resource);
            else UnManaged.Remove(resource);

        }

        public void DeviceLost()
        {
            foreach (DeviceResource resource in UnManaged) resource.DeviceLost();
        }
        public void DeviceRestore()
        {
            foreach (DeviceResource resource in UnManaged) if (resource.IsEnabled) resource.DeviceRestore();
        }
    }
}
