﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using DX = SharpDX;
using D3D = SharpDX.Direct3D9;

using Engine.Tools;
using Engine.Maths;

namespace Engine
{
    /// <summary>
    /// A directx9 helper class to render to texture, deprecated for my MultipleRenderTarghetManager
    /// </summary>
    public class D3DRenderToSurface : TextureBase
    {
        //D3D.Surface _prevbackbuffer;

        D3D.Surface targhetSurface;
        D3D.Texture targhetTexture;
        D3D.RenderToSurface renderSurface;
        D3D.Surface tmp_surface;
        bool tmp_inuse = false;

        DepthFormat depthFormat;
        Viewport viewport;
        Matrix4 projection;
        bool useDepth;
        bool useStencil;

        internal override D3D.BaseTexture basetexture
        {
            get { return this.targhetTexture; }
        }

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="depth">if DepthFormat.Unknown it don't use Zbuffer (dephbuffer) and Stencil (depthstencilbuffer) </param>
        public D3DRenderToSurface(Device device, int width, int height, Format format, DepthFormat depth) :
            base(device, TextureUsage.RenderTarghet)
        {
            if (width < 1 || height < 1) throw new ArgumentOutOfRangeException("Size can't be zero");
            // the depth stencil surface must always be greater or equal to the size of the render target.
            // the format of the render target and the depth stencil surface must be compatible and the multisample type must be the same.
            useDepth = depth != DepthFormat.Unknown;
            useStencil = useDepth & haveStencil(depth);

            depthFormat = depth;
            PixelFormat = format;
            Width = width;
            Height = height;

            viewport = new Viewport();
            viewport.MinDepth = 0;
            viewport.MaxDepth = 1;
            viewport.Width = width;
            viewport.Height = height;

            projection = Matrix4.MakeProjectionAFovYLH(0.1f, 1000.0f, viewport.AspectRatio);

            CreateSurfaces();

          
        }

        bool haveStencil(DepthFormat depthformat)
        {
            return
                depthformat == DepthFormat.D15S1 ||
                depthformat == DepthFormat.D24S8 ||
                depthformat == DepthFormat.D24SingleS8 ||
                depthformat == DepthFormat.D24X4S4;
        }


        public void Resize(Size size)
        {
            Dispose();
            Width = size.Width;
            Height = size.Height;

            viewport = new Viewport();
            viewport.MinDepth = 0;
            viewport.MaxDepth = 1;
            viewport.Width = Width;
            viewport.Height = Height;

            projection = Matrix4.MakeProjectionAFovYLH(0.1f, 1000.0f, viewport.AspectRatio);

            CreateSurfaces();
        }

        void CreateSurfaces()
        {
            targhetTexture = new D3D.Texture(device.device, Width, Height, 1, (D3D.Usage)usage, (D3D.Format)PixelFormat, (D3D.Pool)memory);
            targhetSurface = targhetTexture.GetSurfaceLevel(0);
            renderSurface = new D3D.RenderToSurface(device.device, Width, Height, (D3D.Format)PixelFormat, useDepth, (D3D.Format)depthFormat);
        }

        /// <summary>
        /// Return a read-only texture data
        /// </summary>
        /// <returns></returns>
        public TextureStream OpenRenderTarghetStream()
        {
            // from Microsoft.com, the method fail if :
            // The render target is multisampled.
            // The source render target is a different size than the destination surface.
            // The source render target and destination surface formats do not match.

            D3D.Surface tmp_surface = D3D.Surface.CreateOffscreenPlain(base.device.device, Width, Height, (D3D.Format)PixelFormat, D3D.Pool.SystemMemory);
            base.device.device.GetRenderTargetData(targhetSurface, tmp_surface);

            DX.DataRectangle data = tmp_surface.LockRectangle(D3D.LockFlags.ReadOnly);

            IntPtr bufferPtr = data.DataPointer;
            tmp_inuse = bufferPtr != IntPtr.Zero;

            return new TextureStream(bufferPtr, 0, data.Pitch, PixelFormat, Width, Height);

        }
        
        public void CloseRenderTarghetStream()
        {
            if (tmp_surface != null) tmp_surface.Dispose();
            tmp_surface = null;
            tmp_inuse = false;
        }

        /// <summary>
        /// begin scene
        /// </summary>
        public void BeginScene()
        {
            if (tmp_inuse) throw new Exception("RenderTarghetStream in use, close it");

            renderSurface.BeginScene(targhetSurface, Helper.Convert(viewport));

            /*
            device.BeginDraw();
            _prevbackbuffer = device.m_device.GetRenderTarget(0);
            device.m_device.SetRenderTarget(0, targhetSurface);
            device.Clear(useDepth, useStencil, Color32.White);
            */

            D3D.ClearFlags flags = D3D.ClearFlags.Target;
            if (useDepth) flags |= D3D.ClearFlags.ZBuffer;
            if (useStencil) flags |= D3D.ClearFlags.Stencil;
            device.device.Clear(flags, new DX.ColorBGRA(255, 255, 255, 255), 1, 0);
        }

        /// <summary>
        /// termina la scena
        /// </summary>
        public void EndScene()
        {
            renderSurface.EndScene(D3D.Filter.None);
            //device.EndDraw();
            //device.m_device.SetRenderTarget(0, _prevbackbuffer);
        }

        public override void DeviceRestore()
        {
            if (IsEnabled)
            {
                CreateSurfaces();
            }
        }

        public override void Dispose()
        {
            if (renderSurface != null) renderSurface.Dispose();
            //if (_prevbackbuffer != null) _prevbackbuffer.Dispose();
            if (targhetSurface != null) targhetSurface.Dispose();          
            if (targhetTexture != null) targhetTexture.Dispose();
            if (tmp_surface != null) 
            {
                tmp_surface.Dispose();
                tmp_surface = null;
            }
        }
    }
}
