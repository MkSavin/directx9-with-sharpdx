﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Drawing.Imaging;
using System.Collections.Generic;
using System.Text;

using DX = SharpDX;
using D3D = SharpDX.Direct3D9;

using Engine.Tools;
using Engine.Maths;
 

namespace Engine
{

    /// <summary>
    /// </summary>
    /// <remarks>
    /// http://vincedx.altervista.org/Lessons.php?id=87&from=0
    /// 
    /// <para>           ----------                </para>
    /// <para>           |   2    |                </para>
    /// <para> -----------------------------------</para>
    /// <para> |   1     |   4    |   0   |   5   |</para>
    /// <para> -----------------------------------</para>
    /// <para>           |   3    |                </para>
    /// <para>           ----------                </para>
    /// where indices are MapFace
    /// </remarks>
    public class TextureCUBE : TextureBase
    {
        internal D3D.CubeTexture texture;
        internal override D3D.BaseTexture basetexture { get { return texture; } }

        TextureCUBELock locktool;

        /// <summary>
        /// Empty initialization
        /// </summary>
        private TextureCUBE(Device device, TextureUsage usageMode)
            : base(device, usageMode)
        {
            Width = Height = Depth = 0;
        }

        /// <summary>
        /// Cube texture require obviously one one dimension stored as width
        /// </summary>
        public TextureCUBE(Device device, TextureUsage usage, Format pixelFormat, int width, bool generateMipMap = false)
            : this(device, usage)
        {
            string message;
            if (!DX9Enumerations.SupportTexture(ResourceType.CubeTexture, out message, width) ||
                !DX9Enumerations.SupportTexture(ResourceType.CubeTexture, pixelFormat, out message))
                throw new NotSupportedException(message);

            this.Width = width;
            this.PixelFormat = pixelFormat;

            // if level == 0, it generate all mipmap untill 1x1
            texture = new D3D.CubeTexture(device.device, width, generateMipMap ? 0 : 1, (D3D.Usage)usage, (D3D.Format)pixelFormat, (D3D.Pool)memory);

            base.MipMapCount = generateMipMap ? GetMipMapCount(width) : 1;
        }

        public static TextureCUBE FromFilename(Device device, string myfilename)
        {
            if (!System.IO.File.Exists(myfilename)) throw new ArgumentNullException("File not found");
            string filename = System.IO.Path.GetFullPath(myfilename);


            TextureCUBE tex = new TextureCUBE(device, TextureUsage.Managed);
            tex.texture = D3D.CubeTexture.FromFile(device.device, filename, (D3D.Usage)tex.usage, (D3D.Pool)tex.memory);
            
            D3D.SurfaceDescription info = tex.texture.GetLevelDescription(0);
            tex.Width = info.Width;
            tex.Height = info.Height;
            tex.PixelFormat = (Format)info.Format;
            tex.MipMapCount = tex.texture.LevelCount;

            return tex;
        }


        /// <summary>
        /// The number of streams are the Depth size of volume when mipmap = 0 and volume.Depth select all levels, not implemented for other mipmaps (too complex)
        /// </summary>
        public TextureStream OpenStream(int mipmap, MapFace face , RectangleUV area, bool ReadOnly = false)
        {
            if ((int)face > 5) throw new ArgumentOutOfRangeException("what f*** are you doing ?");

            if (mipmap >= MipMapCount) throw new ArgumentOutOfRangeException("mipmap out of range");
            locktool = new TextureCUBELock(this);
            locktool.Open(mipmap, face, area, ReadOnly);
            return locktool.stream;
        }
        /// <summary>
        /// The number of streams are the Depth size of volume when mipmap = 0 and volume.Depth select all levels, not implemented for other mipmaps (too complex)
        /// </summary>
        public TextureStream OpenStream(int mipmap, MapFace face , bool ReadOnly = false)
        {
            return OpenStream(mipmap, face, RectangleUV.FromSize(0, 0, Width >> mipmap, Width >> mipmap), ReadOnly);
        }



        public void CloseStream()
        {
            locktool.Close();
            locktool = null;
        }

        public override void Dispose()
        {
            if (texture != null && !texture.IsDisposed)
                texture.Dispose();
            disposed = true;
        }

        public override string ToString()
        {
            return "CubeTex " + base.ToString();
        }
    }

    /// <summary>
    /// Texture3D lock/unlock tool
    /// </summary>
    internal class TextureCUBELock
    {
        TextureCUBE texture;

        public TextureStream stream { get; private set; }

        /// <summary>
        /// Size of original texture when mipmap = 0
        /// </summary>
        protected int Width;

        IntPtr bufferPtr;
        Format pixelFormat;
        bool readonlymode = false;
        bool locked = false;
        int lockedLevel = 0;
        MapFace lockedFace;
        LockedArea lockedArea;

        int levelWidth;

        public TextureCUBELock(TextureCUBE texture3d)
        {
            this.texture = texture3d;
            if (!texture.lockable) throw new ArgumentException("This texture can't be locked");
            pixelFormat = texture.PixelFormat;
            Width = texture.Width;
        }

        public void Open(int mipmap, MapFace face, RectangleUV area, bool onlyread = false)
        {
            readonlymode = onlyread;
            if (locked) throw new Exception("Already locked, please close it before");

            lockedLevel = mipmap;
            lockedFace = face;

            //D3D.SurfaceDescription info = texture.texture.GetLevelDescription(lockedLevel);
            //levelWidth = info.Width;
            //levelHeight = info.Height;
            levelWidth = Width >> lockedLevel;

            lockedArea = new LockedArea(area, levelWidth, levelWidth);

            if (mipmap > 0)
                Debug.Assert(lockedArea.wholearea, "read a smaller locked area for mipmaps > 0 don't work correctly");

            // Dxt compressed texture use a 4x4 pixels block
            if (TextureStream.isDxtcompressed(pixelFormat) && !lockedArea.IsMod4x4)
                throw new ArgumentException("For Dxt version i need to lock a rectangle with size multiple of 4 pixels");

            // only read case
            LockFlags mode = onlyread ? LockFlags.ReadOnly : LockFlags.Normal;
           
            DX.Rectangle rect = new DX.Rectangle(lockedArea.x0, lockedArea.y0, lockedArea.width, lockedArea.height);

            DX.DataRectangle data = lockedArea.wholearea ?
                texture.texture.LockRectangle((D3D.CubeMapFace)face , lockedLevel, (D3D.LockFlags)mode) :
                texture.texture.LockRectangle((D3D.CubeMapFace)face, lockedLevel, rect, (D3D.LockFlags)mode);

            bufferPtr = data.DataPointer;

            locked = bufferPtr != IntPtr.Zero;

            stream = new TextureStream(bufferPtr, 0, data.Pitch, pixelFormat, lockedArea.width, lockedArea.height);

        }
        public void Close()
        {
            if (locked) texture.texture.UnlockRectangle((D3D.CubeMapFace)lockedFace,lockedLevel);
            if (stream != null) stream.Close();
            locked = false;
            stream = null;
        }
    }

}