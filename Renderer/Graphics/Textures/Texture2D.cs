﻿
using System;
using System.Diagnostics;
using System.Drawing;

using D3D = SharpDX.Direct3D9;
using DX = SharpDX;

using Engine.Maths;

namespace Engine
{
    /// <summary>
    /// A single 2D texture
    /// </summary>
    public class Texture2D : TextureBase
    {
        //  0,0 _______--> U (1,0)
        //   |         |
        //   |  BMP    |
        //   |         |
        //   |_________| (1,1)
        //   !
        //   V (0,1)

        internal D3D.Texture texture;
        internal override D3D.BaseTexture basetexture { get { return texture; } }
       
        Texture2DLock locktool;
        
        /// <summary>
        /// Empty initialization
        /// </summary>
        private Texture2D(Device device, TextureUsage usageMode)
            : base(device, usageMode)
        {
            Depth = 0;
        }
        /// <summary>
        /// Empty texture, all pixels are black
        /// </summary>
        public Texture2D(Device device, TextureUsage usageMode, Format pixelformat, int width, int height, bool generateMipMap = false)
            : this(device, usageMode)
        {
            if (width < 1 || height < 1) throw new ArgumentOutOfRangeException("");

            string message;
            if (!DX9Enumerations.SupportTexture(ResourceType.Texture, out message, width, height) ||
                !DX9Enumerations.SupportTexture(ResourceType.Texture, pixelformat, out message))
                throw new NotSupportedException(message);

            this.Width = width;
            this.Height = height;
            this.PixelFormat = pixelformat;
            this.texture = new D3D.Texture(device.device, Width, Height, generateMipMap ? 0 : 1, (D3D.Usage)usage, (D3D.Format)pixelformat, (D3D.Pool)memory);
            this.MipMapCount = generateMipMap ? GetMipMapCount(Width, Height) : 1;
           
        }


        /// <summary>
        /// Forcing the size to avoid near-pow2 initialization
        /// </summary>
        public static Texture2D FromFilename(Device device, TextureUsage usageMode, Format pixelFormat, string myfilename, int width, int height, bool generateMipMap = false)
        {
            if (!System.IO.File.Exists(myfilename)) throw new ArgumentNullException("File not found");
            string filename = System.IO.Path.GetFullPath(myfilename);
            Texture2D tex = new Texture2D(device, usageMode);

            string message;
            if (!DX9Enumerations.SupportTexture(ResourceType.Texture, out message, width, height) ||
                !DX9Enumerations.SupportTexture(ResourceType.Texture, pixelFormat, out message))
                throw new NotSupportedException(message);

            //D3DXCreateTextureFromFileEx same of D3DXCreateTextureFromFile but with more controls
            // forcing use custom size, need to enable Filters to avoid black zones
            tex.texture = D3D.Texture.FromFile(tex.device.device, filename, width, height, generateMipMap ? 0 : 1, (D3D.Usage)tex.usage, (D3D.Format)pixelFormat, (D3D.Pool)tex.memory, D3D.Filter.Default, D3D.Filter.Default, 0);

            D3D.SurfaceDescription info = tex.texture.GetLevelDescription(0);
            tex.Width = info.Width;
            tex.Height = info.Height;
            tex.PixelFormat = (Format)info.Format;
            tex.MipMapCount = generateMipMap ? GetMipMapCount(tex.Width, tex.Height) : 1;
            tex.MipMapCount = tex.texture.LevelCount;
            return tex;
        }
        /// <summary>
        /// Pixelformat and Size from file
        /// </summary>
        public static Texture2D FromFilename(Device device, TextureUsage usageMode, string filename, bool generateMipMap = false)
        {
            return Texture2D.FromFilename(device, usageMode, Format.Unknown, filename, generateMipMap);
        }
        /// <summary>
        /// Size from file
        /// </summary>   
        public static Texture2D FromFilename(Device device, TextureUsage usageMode, Format pixelFormat, string filename, bool generateMipMap = false)
        {
            return FromFilename(device, usageMode, pixelFormat, filename, 0, 0, generateMipMap);
        }
        /// <summary>
        /// TODO : check if buffer work with differet pixelformats
        /// </summary>
        public static Texture2D FromBitmap(Device device, TextureUsage usageMode, Format pixelFormat, Bitmap bitmap, bool generateMipMap = false)
        {
            string message;
            if (!DX9Enumerations.SupportTexture(ResourceType.Texture, out message, bitmap.Width, bitmap.Height) ||
                !DX9Enumerations.SupportTexture(ResourceType.Texture, pixelFormat, out message))
                throw new NotSupportedException(message);

            Texture2D tex = new Texture2D(device, usageMode);

            ImageConverter converter = new ImageConverter();
            byte[] buffer = (byte[])converter.ConvertTo(bitmap, typeof(byte[]));

            // i know the bitmap size so Filters can be disabled
            tex.texture = D3D.Texture.FromMemory(tex.device.device, buffer, bitmap.Width, bitmap.Height, generateMipMap ? 0 : 1, (D3D.Usage)tex.usage, (D3D.Format)pixelFormat, (D3D.Pool)tex.memory, D3D.Filter.Default, D3D.Filter.Default, 0);

            tex.Width = bitmap.Width;
            tex.Height = bitmap.Height;
            tex.PixelFormat = pixelFormat;
            tex.MipMapCount = generateMipMap ? GetMipMapCount(tex.Width, tex.Height) : 1;
            return tex;
        }


        public TextureStream OpenStream(int mipmap, RectangleUV area, bool ReadOnly = false)
        {
            if (mipmap >= MipMapCount) throw new ArgumentOutOfRangeException("mipmap out of range");
            locktool = new Texture2DLock(this);
            locktool.Open(mipmap, area, ReadOnly);
            return locktool.stream;
        }
        public TextureStream OpenStream(int mipmap, bool ReadOnly = false)
        {
            return OpenStream(mipmap, RectangleUV.FromSize(0, 0, Width >> mipmap, Height >> mipmap), ReadOnly);
        }
        
        public void CloseStream()
        {
            locktool.Close();
            locktool = null;
        }

        public static bool CheckSize(int width, int height)
        {
            string message;
            return DX9Enumerations.SupportTexture(ResourceType.Texture, out message, width, height);
        }

        public override void Dispose()
        {
            if (texture != null && !texture.IsDisposed) texture.Dispose();
            texture = null;
            disposed = true;
        }
    }

    /// <summary>
    /// Texture2D lock/unlock tool
    /// </summary>
    internal class Texture2DLock
    {
        Texture2D texture;

        public TextureStream stream { get; private set; }

        /// <summary>
        /// Size of original texture when mipmap = 0
        /// </summary>
        protected int Width, Height;

        IntPtr bufferPtr;
        Format pixelFormat;
        bool readonlymode = false;
        bool locked = false;
        int lockedLevel = 0;
        LockedArea lockedArea;

        int levelWidth, levelHeight;

        public Texture2DLock(Texture2D texture2d)
        {
            this.texture = texture2d;
            if (!texture.lockable) throw new ArgumentException("This texture can't be locked");
            pixelFormat = texture.PixelFormat;

            Width = texture.Width;
            Height = texture.Height;
        }

        public void Open(int mipmap, RectangleUV area, bool onlyread = false)
        {
            readonlymode = onlyread;
            if (locked) throw new Exception("Already locked, please close it before");

            lockedLevel = mipmap;

            if (mipmap >= texture.texture.LevelCount) throw new Exception("out of mipmaps range");

            D3D.SurfaceDescription info = texture.texture.GetLevelDescription(lockedLevel);
            //levelWidth = info.Width;
            //levelHeight = info.Height;
            levelWidth = Width >> lockedLevel;
            levelHeight = Height >> lockedLevel;

            lockedArea = new LockedArea(area, levelWidth, levelHeight);

            if (mipmap > 0)
                Debug.Assert(lockedArea.wholearea, "read a smaller locked area for mipmaps > 0 don't work correctly");

            // Dxt compressed texture use a 4x4 pixels block
            if (TextureStream.isDxtcompressed(pixelFormat) && !lockedArea.IsMod4x4)
                throw new ArgumentException("For Dxt version i need to lock a rectangle with size multiple of 4 pixels");

            // only read case
            LockFlags mode = onlyread ? LockFlags.ReadOnly : LockFlags.Normal;

            DX.Rectangle rect = new DX.Rectangle(lockedArea.x0, lockedArea.y0, lockedArea.width, lockedArea.height);

            DX.DataRectangle data;
            data = lockedArea.wholearea ?
                texture.texture.LockRectangle(lockedLevel, (D3D.LockFlags)mode) :
                texture.texture.LockRectangle(lockedLevel, rect, (D3D.LockFlags)mode);

            bufferPtr = data.DataPointer;
            locked = bufferPtr != IntPtr.Zero;

            stream = new TextureStream(bufferPtr, 0, data.Pitch, pixelFormat, lockedArea.width, lockedArea.height);
        }

        public void Open(int mipmap, bool onlyread = false)
        {
            Open(mipmap, RectangleUV.FromSize(0, 0, Width >> mipmap, Height >> mipmap), onlyread);
        }
        
        public void Close()
        {
            if (locked) texture.texture.UnlockRectangle(lockedLevel);
            if (stream != null) stream.Close();
            locked = false;
            stream = null;
        }
    }
}
