﻿using System;
using System.Collections.Generic;
using System.Text;

using DX = SharpDX;
using D3D = SharpDX.Direct3D9;

namespace Engine
{
    /// <summary>
    /// A 2D texture used to write the render output
    /// </summary>
    public class Texture2DTarghet : Texture2D
    {
        internal RenderWindow window;
        internal D3D.Surface surface;
        internal override D3D.BaseTexture basetexture
        {
            get { return texture; }
        }

        /// <summary>
        /// Create a new Texture used as RenderSurface, i pass RenderWindows instead Device because the texture
        /// is linked to backbuffersize, when windows resize need to resize also texture.
        /// If size don't match with backbuffer i notice a wrong rendering
        /// </summary>
        public Texture2DTarghet(RenderWindow window, Format format = Format.Unknown) :
            base(window.Device, TextureUsage.RenderTarghet,
            format == Format.Unknown ? (Format)window.FrameSetting.parameters.BackBufferFormat : format,
            window.FrameSetting.parameters.BackBufferWidth,
            window.FrameSetting.parameters.BackBufferHeight,
            false)
        {
            Name = "Texture2DTarghet";
            surface = texture.GetSurfaceLevel(0);
            this.window = window;
        }

        void BackBufferResized(int width, int height)
        {
            this.Height = height;
            this.Width = width;
            // in this case FrameSetting are update by associated RenderWindows, i need to know only its backbuffer size
            texture = new D3D.Texture(device.device, width, height, 1, (D3D.Usage)usage, (D3D.Format)PixelFormat, (D3D.Pool)memory);
            surface = texture.GetSurfaceLevel(0);

        }

        public override void DeviceRestore()
        {
            BackBufferResized(window.FrameSetting.parameters.BackBufferWidth, window.FrameSetting.parameters.BackBufferHeight);
        }

        public override void Dispose()
        {
            base.Dispose();

            if (surface != null && !surface.IsDisposed)
            {
                surface.Dispose();
                surface = null;
            }
        }
    }
}
