﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Drawing.Imaging;
using System.Collections.Generic;
using System.Text;

using DX = SharpDX;
using D3D = SharpDX.Direct3D9;

using Engine.Tools;
using Engine.Maths;
 

namespace Engine
{
    /// <summary>
    /// All possible and tested combinations
    /// </summary>
    public enum TextureUsage
    {
        /// <summary> 
        /// Auto-restorable when device reset. LOCKABLE and READABLE, faster than Dynamic but require a copy in RAM
        /// </summary>
        Managed,
        /// <summary>
        /// Not-restorable when device reset. Best performance. NOT LOCKABLE
        /// </summary>
        Static,
        /// <summary> 
        /// Not-restorable when device reset. LOCKABLE and READABLE 
        /// </summary>
        Dynamic,
        /// <summary>
        /// Use texture as render surface
        /// </summary>
        RenderTarghet
    }

    /// <summary>
    /// The common derived class for all texture
    /// </summary>
    public abstract class TextureBase : DeviceResource
    {
        public string Name = "BaseTexture";

        /// <summary>
        /// Get base texture when apply to device
        /// </summary>
        internal abstract D3D.BaseTexture basetexture { get; }

        internal Pool memory;
        internal Usage usage;
        internal bool lockable;

        public Format PixelFormat { get; protected set; }
        public int MipMapCount { get; protected set; }

        /// <summary>
        /// The depth size of texture (number of texture). Attention, like width and height must be check if device support the values
        /// </summary>
        public int Depth { get; protected set; }
        public int Width { get; protected set; }
        public int Height { get; protected set; }

#if USE_OLD_FIXED_FUNCTION_PIPELINE
        public DX9TextureState TextureStates;
#endif
        protected TextureBase(Device device, TextureUsage usageMode)
            : base(device)
        {
            this.disposed = false;
#if USE_OLD_FIXED_FUNCTION_PIPELINE         
            this.TextureStates = DX9TextureState.GetDefaultSetting(0);
#endif
            this.IsEnabled = true;

            switch (usageMode)
            {
                case TextureUsage.Managed:
                    usage = Usage.None;
                    memory = Pool.Managed;
                    managed = true;
                    lockable = true;
                    break;
                case TextureUsage.Static:
                    usage = Usage.None;
                    memory = Pool.Default;
                    managed = false;
                    lockable = false;
                    break;
                case TextureUsage.Dynamic:
                    if (!DX9Enumerations.SupportDynamicTexture) throw new NotSupportedException("Device not support dynamic textures");
                    usage = Usage.Dynamic;
                    memory = Pool.Default;
                    managed = false;
                    lockable = true;
                    break;
                case TextureUsage.RenderTarghet:
                    usage = Usage.RenderTarget;
                    memory = Pool.Default;//render target is unmanaged resource, must be disposed
                    managed = false;
                    lockable = false;
                    break;
            }

            // if you want autogenerate mipmap u need to check if autogen is supported, if true
            // you will not be able to access to sub-mipmaps
            //usage |= Usage.AutoGenerateMipMap;

            base.SetManagedFlag(managed);
        }

        /// <summary>
        /// Use directx native method to save
        /// </summary>
        public void Save(string filename, ImageFileFormat fileformat)
        {
            filename = System.IO.Path.GetFullPath(filename);
            D3D.BaseTexture.ToFile(basetexture, filename, (D3D.ImageFileFormat)fileformat);
        }
        /// <summary>
        /// </summary>
        public void GenerateMipMaps()
        {
            basetexture.GenerateMipSubLevels();
            // in case of Texture3D, depth=0 but not change the result
            MipMapCount = GetMipMapCount(Width, Height, Depth);
        }
        /// <summary>
        /// Calculate the maximum mipmaps count using one, two or three size.
        /// mipmap[i+1].size = mipmap[i] / 2 and stop when reach 1 pixel.
        /// Example with texture2d we can optain 8 levels
        /// <para>mipmap0 = 128 x 49</para>
        /// <para>mipmap1 = 64 x 24</para>
        /// <para>mipmap2 = 32 x 12</para>
        /// <para>mipmap3 = 16 x 6</para>
        /// <para>mipmap4 = 8 x 3</para>
        /// <para>mipmap5 = 4 x 1</para>
        /// <para>mipmap6 = 2 x 1</para>
        /// <para>mipmap7 = 1 x 1</para>
        /// </summary>
        public static int GetMipMapCount(params int[] sizes)
        {
            int maxcount = 0;

            for (int i = 0; i < sizes.Length; i++)
            {
                int count = 1;
                while ((sizes[i] >>= 1) > 0) count++;
                if (maxcount < count) maxcount = count;
            }
            return maxcount;
        }
        /// <summary>
        /// Just to remember how compute it
        /// </summary>
        public static RectangleUV GetMipMapSize(int mipmap, int width, int height)
        {
            int w = width >> mipmap;
            int h = height >> mipmap;
            return RectangleUV.FromSize(0, 0, w > 0 ? w : 1, h > 0 ? h : 1);
        }
        public static VolumeUVW GetMipMapSize(int mipmap, int width, int height, int depth)
        {
            int w = width >> mipmap;
            int h = height >> mipmap;
            int d = depth >> depth;
            return VolumeUVW.FromSize(0, 0, 0, w > 0 ? w : 1, h > 0 ? h : 1, d > 0 ? d : 1);
        }

        public override void DeviceLost()
        {
            Dispose();
        }
        /// <summary>
        /// A unmanaged resource can have different way to restore the resource, example if you save
        /// the bitmap image or only the filename string
        /// </summary>
        public override void DeviceRestore()
        {
            throw new NotImplementedException();
        }

        public override string ToString()
        {
            return string.Format("{0}x{1}x{2} : {3} , {4} , {5}", Width, Height, Depth, PixelFormat, usage, memory);
        }

    }

    /// <summary>
    /// </summary>
    internal class LockedArea
    {
        /// <summary>
        /// If are you looking whole texture, you can use a different override in texture.lockrectangle.
        /// </summary>
        public bool wholearea;
        /// <summary>
        /// min-max pixel index
        /// </summary>
        public int x0, y0, x1, y1;
        /// <summary>
        /// size of locked rectangle
        /// </summary>
        public int width, height;

        public LockedArea(RectangleUV rectangle, int TextureWidth, int TextureHeight)
        {
            x0 = max(rectangle.X, 0);
            y0 = max(rectangle.Y, 0);
            x1 = min(x0 + rectangle.Width, TextureWidth) - 1;
            y1 = min(y0 + rectangle.Height, TextureHeight) - 1;

            width = x1 - x0 + 1;
            height = y1 - y0 + 1;

            wholearea = width >= TextureWidth && height >= TextureHeight;

            if (x1 - x0 < 0 || y1 - y0 < 0) throw new ArgumentException("nothing to lock ?");
        }

        int max(int a, int b) { return a > b ? a : b; }
        int min(int a, int b) { return a < b ? a : b; }

        public int PixelsCount
        {
            get { return (x1 - x0 + 1) * (y1 - y0 + 1); }
        }
        public bool IsMod4x4
        {
            get { return x0 % 4 == 0 && y0 % 4 == 0 && (x1 + 1) % 4 == 0 && (y1 + 1) % 4 == 0; }
        }
        public override string ToString()
        {
            return string.Format("x{0}, y{1}, W{2}, H{3} ", x0, y0, width, height);
        }
    }

    /// <summary>
    /// </summary>
    internal class LockedVolume
    {
        //       back..............
        //         /             /!
        //   left --------------> right
        //   top  |             ! !
        //   front|             ! !
        //        |             ! !
        //        |             ! !
        //        v............ !/
        //     bottom
        //
        // left2right = width , top2bottom = height ,  front2back = depth


        /// <summary>
        /// If are you looking whole texture, you can use a different override in texture.lockrectangle.
        /// </summary>
        public bool wholearea;
        /// <summary>
        /// min-max pixel index
        /// </summary>
        public int x0, y0, x1, y1, z0, z1;
        /// <summary>
        /// size of locked rectangle
        /// </summary>
        public int width, height, depth;
        /// <summary>
        /// pixels to jump for next row, i don't know if pich size is a multiple of pixel size
        /// </summary>
        public int Pitch;
        /// <summary>
        /// pixels to jump for next depth level
        /// </summary>
        public int Splice;

        public LockedVolume(VolumeUVW box, int TextureWidth, int TextureHeight, int TextureDepth)
        {
            x0 = max(box.Left, 0);
            y0 = max(box.Top, 0);
            z0 = max(box.Front, 0);
            x1 = min(box.Right, TextureWidth - 1);
            y1 = min(box.Bottom, TextureHeight - 1);
            z1 = min(box.Back, TextureDepth - 1);

            width = x1 - x0 + 1;
            height = y1 - y0 + 1;
            depth = z1 - z0 + 1;

            wholearea = width >= TextureWidth && height >= TextureHeight && depth >= TextureDepth;

            if (x1 - x0 < 0 || y1 - y0 < 0 || z1 - z0 < 0) throw new ArgumentException("nothing to lock ?");
        }

        int max(int a, int b) { return a > b ? a : b; }
        int min(int a, int b) { return a < b ? a : b; }

        public int PixelsCount
        {
            get { return (x1 - x0 + 1) * (y1 - y0 + 1) * (z1 - z0 + 1); }
        }
        public bool IsMod4x4
        {
            get { return x0 % 4 == 0 && y0 % 4 == 0 && (x1 + 1) % 4 == 0 && (y1 + 1) % 4 == 0; }
        }

        public override string ToString()
        {
            return string.Format("x{0},y{1},z{2}  W{3},H{4},D{5} ", x0, y0, z0, width, height, depth);
        }
    }

#if USE_OLD_FIXED_FUNCTION_PIPELINE    
    /// <summary>
    /// Used only for fixed function pipeline, these parameters are substitued in HLSL code if you use shader pipeline, in fact the
    /// texture are passed as parameter and not passed to device
    /// </summary>
    public class DX9TextureState
    {
        // default set
        public TextureFilter MinFilter = TextureFilter.None;
        public TextureFilter MagFilter = TextureFilter.None;

        internal void SetStates(D3D.Device m_device, int stage)
        {
            m_device.SetSamplerState(stage, D3D.SamplerState.MinFilter, (D3D.TextureFilter)MinFilter);
            m_device.SetSamplerState(stage, D3D.SamplerState.MagFilter, (D3D.TextureFilter)MagFilter);

            m_device.SetTextureStageState(stage, D3D.TextureStage.ColorOperation, (D3D.TextureOperation)ColorOperation);
            m_device.SetTextureStageState(stage, D3D.TextureStage.ColorArg1, (D3D.TextureArgument)ColorArgument1);
            m_device.SetTextureStageState(stage, D3D.TextureStage.ColorArg2, (D3D.TextureArgument)ColorArgument2);
            m_device.SetTextureStageState(stage, D3D.TextureStage.AlphaOperation, (D3D.TextureOperation)AlphaOperation);
            m_device.SetTextureStageState(stage, D3D.TextureStage.AlphaArg1, (D3D.TextureArgument)AlphaArgument1);

            m_device.SetTextureStageState(stage, D3D.TextureStage.TexCoordIndex, TexCoordIndex);

            m_device.SetTextureStageState(stage, D3D.TextureStage.BumpEnvironmentMat00, BumpEnvironmentMat[0, 0]);
            m_device.SetTextureStageState(stage, D3D.TextureStage.BumpEnvironmentMat01, BumpEnvironmentMat[0, 1]);
            m_device.SetTextureStageState(stage, D3D.TextureStage.BumpEnvironmentMat10, BumpEnvironmentMat[1, 0]);
            m_device.SetTextureStageState(stage, D3D.TextureStage.BumpEnvironmentMat11, BumpEnvironmentMat[1, 1]);

            m_device.SetTextureStageState(stage, D3D.TextureStage.BumpEnvironmentLOffset, BumpEnvironmentLOffset);
            m_device.SetTextureStageState(stage, D3D.TextureStage.BumpEnvironmentLScale, BumpEnvironmentLScale);
        }

        //Per-stage constant color. Use packed integer colors to set this state.
        int Constant = 0;
        //Setting to select the destination register for the result of this stage.
        TextureArgument ResultArg = TextureArgument.Current;
        //Settings for the third alpha operand for triadic operations. 
        TextureArgument AlphaArg0 = TextureArgument.Current;
        //Settings for the third color operand for triadic operations.
        TextureArgument ColorArg0 = TextureArgument.Current;
        //Specifies transformation options for texture coordinates.
        TextureTransform TextureTransformFlags = TextureTransform.Disable;
        //Floating-point offset value for bump-map luminance
        public float BumpEnvironmentLOffset = 0.0f;
        //Floating-point scale value for bump-map luminance
        public float BumpEnvironmentLScale = 0.0f;
        //Index of the texture coordinate set to use with this texture stage.
        //You can specify up to eight sets of texture coordinates per vertex.
        //If a vertex does not include a set of texture coordinates at the specified index, the system defaults to the u and v coordinates (0,0).
        public int TexCoordIndex = 0;
        //coefficient in a bump-mapping matrix
        public float[,] BumpEnvironmentMat = new float[,] { { 0, 0 }, { 0, 0 } };

        //Texture-stage state is the second alpha argument for the stage
        TextureArgument AlphaArg2 = TextureArgument.Current;
        //Texture-stage state is the first alpha argument for the stage
        public TextureArgument AlphaArgument1 = TextureArgument.Texture;
        //Texture-stage state is a texture alpha blending operation. D3D.TextureOperation.Disable for stage > 0
        public TextureOperation AlphaOperation = TextureOperation.SelectArg1;
        //Texture-stage state is the second color argument for the stage
        public TextureArgument ColorArgument2 = TextureArgument.Current;
        //Texture-stage state is the first color argument for the stage
        public TextureArgument ColorArgument1 = TextureArgument.Texture;
        //Texture-stage state is a texture color blending operation D3D.TextureOperation.Disable for stage > 0
        public TextureOperation ColorOperation = TextureOperation.Modulate;

        /// <summary>
        /// A stage > 0 use different setting
        /// </summary>
        public static DX9TextureState GetDefaultSetting(int stage)
        {
            DX9TextureState states = new DX9TextureState();
            if (stage > 0)
            {
                states.TexCoordIndex = 0;
                states.AlphaOperation = TextureOperation.Disable;
                states.ColorOperation = TextureOperation.Disable;
            }
            return states;
        }
    }
#endif
}