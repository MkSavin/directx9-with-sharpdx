﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using DX = SharpDX;
using D3D = SharpDX.Direct3D9;

using Engine.Tools;
using Engine.Maths;

namespace Engine
{
    public class MultipleRenderTarghetManager : DeviceResource
    {
        int count;
        int width;
        int height;
        Format format;
        D3D.Surface[] originals;
        DX.Viewport prev, size;

        /// <summary>
        /// The output of your rendering
        /// </summary>
        public readonly List<Texture2DTarghet> targhets;

        public MultipleRenderTarghetManager(Device device, int Width, int Height, Format Format, int TarghetsCount)
            : base(device, false)
        {
            this.count = TarghetsCount;
            this.width = Width;
            this.height = Height;
            this.format = Format;
            this.size = new DX.Viewport(0, 0, width, height);
            targhets = new List<Texture2DTarghet>(TarghetsCount);

            //for (int i = 0; i < count; i++) targhets[i] = new Texture2DTarghet(device, width, height, format);

            originals = new D3D.Surface[count];
        }

        public void PrepareDraw(Color backgroud)
        {
            for (int i = 0; i < count; i++)
            {
                originals[i] = device.device.GetRenderTarget(i);

                if (!targhets[i].IsEnabled) targhets[i].IsEnabled = true;
                if (!targhets[i].IsDisposed) targhets[i].DeviceRestore();

                device.device.SetRenderTarget(i, targhets[i].surface);
            }
            prev = device.device.Viewport;
            device.device.Viewport = size;
            device.device.Clear(D3D.ClearFlags.Target, DX.ColorBGRA.FromBgra(backgroud.ToArgb()), 1, 0);
        }

        /// <summary>
        /// 
        /// </summary>
        public void BeginDraw()
        {
            device.device.BeginScene();
        }


        public void EndDraw()
        {
            for (int i = 0; i < count; i++)
            {
                device.device.SetRenderTarget(i, originals[i]);
            }
            device.device.Viewport = prev;
        }

        /// <summary>
        /// the copy of surfaces references must be disposed
        /// </summary>
        public override void Dispose()
        {
            for (int i = 0; i < count; i++)
            {
                if (originals[i] != null && !originals[i].IsDisposed)
                    originals[i].Dispose();
                originals[i] = null;
            }
        }

        public override void DeviceLost()
        {
            Dispose();
        }

        public override void DeviceRestore()
        {

        }
    }
}
