﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Drawing.Imaging;
using System.Collections.Generic;
using System.Text;
using System.Collections;

using DX = SharpDX;
using D3D = SharpDX.Direct3D9;

using Engine.Tools;
using Engine.Maths;
 


namespace Engine
{
    /// <summary>
    /// A 3D texture
    /// </summary>
    public class Texture3D : TextureBase
    {
        internal D3D.VolumeTexture texture;
        internal override D3D.BaseTexture basetexture { get { return texture; } }

        Texture3DLock locktool;

        /// <summary>
        /// Empty initialization
        /// </summary>
        private Texture3D(Device device, TextureUsage usageMode)
            : base(device, usageMode)
        {
            Width = Height = Depth = 0;
        }

        public Texture3D(Device device, TextureUsage usageMode, Format pixelFormat, int width, int height, int depth, bool generateMipMap = false)
            : base(device, usageMode)
        {
            string message;
            if (!DX9Enumerations.SupportTexture(ResourceType.VolumeTexture, out message, width, height, depth) ||
                !DX9Enumerations.SupportTexture(ResourceType.VolumeTexture, pixelFormat, out message))
                throw new NotSupportedException(message);

            this.Width = width;
            this.Height = height;
            this.Depth = depth;
            this.PixelFormat = pixelFormat;

            // if level == 0, it generate all mipmap untill 1x1
            texture = new D3D.VolumeTexture(device.device, width, height, depth, generateMipMap ? 0 : 1, (D3D.Usage)usage, (D3D.Format)pixelFormat, (D3D.Pool)memory);

            base.MipMapCount = generateMipMap ? GetMipMapCount(width, height, depth) : 1;
        }


        /// <summary>
        /// Forcing the size to avoid near-pow2 initialization
        /// </summary>
        public static Texture3D FromFilename(Device device, string filename, TextureUsage usageMode, Format pixelFormat, int width, int height, int depth, bool generateMipMap = false)
        {
            if (!System.IO.File.Exists(filename)) throw new ArgumentNullException("File not found");
            filename = System.IO.Path.GetFullPath(filename);
            Texture3D tex = new Texture3D(device, usageMode);

            string message;
            if (!DX9Enumerations.SupportTexture(ResourceType.VolumeTexture, out message, width, height,depth) ||
                !DX9Enumerations.SupportTexture(ResourceType.VolumeTexture, pixelFormat, out message))
                throw new NotSupportedException(message);

            //D3DXCreateTextureFromFileEx same of D3DXCreateTextureFromFile but with more controls
            // forcing use custom size, need to enable Filters to avoid black zones
            tex.texture = D3D.VolumeTexture.FromFile(tex.device.device, filename, width, height, depth, generateMipMap ? 0 : 1, (D3D.Usage)tex.usage, (D3D.Format)pixelFormat, (D3D.Pool)tex.memory, D3D.Filter.Default, D3D.Filter.Default, 0);

            D3D.VolumeDescription info = tex.texture.GetLevelDescription(0);
            tex.Width = info.Width;
            tex.Height = info.Height;
            tex.Depth = info.Depth;
            tex.PixelFormat = (Format)info.Format;
            tex.MipMapCount = generateMipMap ? GetMipMapCount(tex.Width, tex.Height) : 1;

            return tex;
        }


        /// <summary>
        /// The number of streams are the Depth size of volume when mipmap = 0 and volume.Depth select all levels, not implemented for other mipmaps (too complex)
        /// </summary>
        public TextureStream[] OpenStream(int mipmap, VolumeUVW volume, bool ReadOnly = false)
        {
            if (mipmap >= MipMapCount) throw new ArgumentOutOfRangeException("mipmap out of range");
            locktool = new Texture3DLock(this);
            locktool.Open(mipmap, volume, ReadOnly);
            return locktool.streams;
        }
        /// <summary>
        /// The number of streams are the Depth size of volume when mipmap = 0 and volume.Depth select all levels, not implemented for other mipmaps (too complex)
        /// </summary>
        public TextureStream[] OpenStream(int mipmap, bool ReadOnly = false)
        {
            return OpenStream(mipmap, VolumeUVW.FromSize(0, 0, 0, Width >> mipmap, Height >> mipmap, Depth >> mipmap), ReadOnly);
        }

        public void CloseStream()
        {
            locktool.Close();
            locktool = null;
        }


        public static bool CheckSize(int width, int height, int depth)
        {
            string message;
            return DX9Enumerations.SupportTexture(ResourceType.VolumeTexture, out message, width, height, depth);
        }

        public override void Dispose()
        {
            if (texture != null && !texture.IsDisposed)
                texture.Dispose();
            disposed = true;
        }
    }

    /// <summary>
    /// Texture3D lock/unlock tool
    /// </summary>
    internal class Texture3DLock
    {
        Texture3D texture;

        public TextureStream[] streams { get; private set; }

        /// <summary>
        /// Size of original texture when mipmap = 0
        /// </summary>
        protected int Width, Height, Depth;

        IntPtr bufferPtr;
        Format pixelFormat;
        bool readonlymode = false;
        bool locked = false;
        int lockedLevel = 0;
        LockedVolume lockedVolume;

        int levelWidth, levelHeight, levelDepth;

        public Texture3DLock(Texture3D texture3d)
        {
            this.texture = texture3d;
            if (!texture.lockable) throw new ArgumentException("This texture can't be locked");
            pixelFormat = texture.PixelFormat;
            
            Width = texture.Width;
            Height = texture.Height;
            Depth = texture.Depth;
        }

        public void Open(int mipmap, VolumeUVW volume, bool onlyread = false)
        {
            readonlymode = onlyread;
            if (locked) throw new Exception("Already locked, please close it before");

            lockedLevel = mipmap;

            //D3D.SurfaceDescription info = texture.texture.GetLevelDescription(lockedLevel);
            //levelWidth = info.Width;
            //levelHeight = info.Height;
            levelWidth = Width >> lockedLevel;
            levelHeight = Height >> lockedLevel;
            levelDepth = Depth >> lockedLevel;

            lockedVolume = new LockedVolume(volume, levelWidth, levelHeight, levelDepth);

            if (mipmap > 0)
                Debug.Assert(lockedVolume.wholearea, "read a smaller locked area for mipmaps > 0 don't work correctly");

            // Dxt compressed texture use a 4x4 pixels block
            if (TextureStream.isDxtcompressed(pixelFormat) && !lockedVolume.IsMod4x4)
                throw new ArgumentException("For Dxt version i need to lock a rectangle with size multiple of 4 pixels");

            // only read case
            LockFlags mode = onlyread ? LockFlags.ReadOnly : LockFlags.Normal;

            D3D.Box box = Helper.Convert(volume);

            DX.DataBox data = lockedVolume.wholearea ?
                texture.texture.LockBox(lockedLevel, (D3D.LockFlags)mode) :
                texture.texture.LockBox(lockedLevel, box, (D3D.LockFlags)mode);

            lockedVolume.Pitch = data.RowPitch;
            lockedVolume.Splice = data.SlicePitch;

            bufferPtr = data.DataPointer;

            locked = bufferPtr != IntPtr.Zero;

            streams = new TextureStream[lockedVolume.depth];

            int depthoffset = 0;

            for (int i = 0; i < lockedVolume.depth; i++)
            {
                // if a texture is lockable, is also writable and readable
                streams[i] = new TextureStream(bufferPtr, depthoffset, data.RowPitch, pixelFormat, lockedVolume.width, lockedVolume.height);
                depthoffset += data.SlicePitch;
            }

        }
        public void Close()
        {
            if (locked) texture.texture.UnlockBox(lockedLevel);
            if (streams != null) foreach (TextureStream s in streams) s.Close();
            locked = false;
            streams = null;
        }
    }
}
