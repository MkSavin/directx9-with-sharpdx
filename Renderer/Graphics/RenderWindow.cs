﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

using DX = SharpDX;
using D3D = SharpDX.Direct3D9;

using Engine.Tools;

namespace Engine
{
    /// <summary>
    /// FrameSetting are used for all properties related to BackBuffer and DepthStencilBuffer creation
    /// </summary>
    public class FrameSetting
    {
        const int MINBACKBUFFERW = 32;
        const int MINBACKBUFFERH = 32;
        const int MAXBACKBUFFERW = 2048;
        const int MAXBACKBUFFERH = 2048;

        internal bool windowed = true;
        internal bool useZbuffer = true;
        internal bool useStencil = true;
        internal DX.Viewport viewport;
        internal D3D.PresentParameters parameters;
        internal IntPtr whandle;

        /// <summary>
        /// UseZBuffer and UseStencilBuffer can be false example when you have to render a swapchain with only
        /// texture closeup rendering
        /// </summary>
        public FrameSetting(bool UseZBuffer, bool UseStencilBuffer, Viewport viewport, IntPtr WindowsHandle, bool WindowedMode)
        {
            this.useZbuffer = UseZBuffer;
            this.useStencil = UseStencilBuffer;
            this.windowed = WindowedMode;
            this.whandle = WindowsHandle;

            if (viewport.Width < MINBACKBUFFERW) viewport.Width = MINBACKBUFFERW;
            if (viewport.Width > MAXBACKBUFFERW) viewport.Width = MAXBACKBUFFERW;
            if (viewport.Height < MINBACKBUFFERH) viewport.Height = MINBACKBUFFERH;
            if (viewport.Height > MAXBACKBUFFERH) viewport.Height = MAXBACKBUFFERH;

            this.viewport = Helper.Convert(viewport);
        }

        /// <summary>
        /// The initialization and Use of this class are in relation to card used
        /// </summary>
        internal bool InitParams(VideoAdapter card)
        {
            try
            {
                if (whandle == null) throw new NotImplementedException("I don't find in internet some valid tutorial about this");

                parameters = new D3D.PresentParameters(viewport.Width, viewport.Height);
                //parameters.BackBufferWidth = Width;
                //parameters.BackBufferHeight = Height;

                // sorted by importance, i can't test other cases
                List<Format> PossibleDisplayFormat = new List<Format>();
                PossibleDisplayFormat.Add(Format.X8R8G8B8);
                PossibleDisplayFormat.Add(Format.X1R5G5B5);
                PossibleDisplayFormat.Add(Format.R5G6B5);

                Format displayFormat = card.CurrentDisplayMode.format;
                int pos = PossibleDisplayFormat.BinarySearch(displayFormat);

                if (pos < 0) throw new ArgumentException("Display Format unknow : " + displayFormat.ToString());

                // check best backbuffer format for current display format, and they should generally, for fullscreen, the same thing
                while (pos < 3)
                {
                    displayFormat = PossibleDisplayFormat[pos];
                    if (DX9Enumerations.CheckDeviceType(displayFormat, displayFormat, true))
                    {
                        parameters.BackBufferFormat = (D3D.Format)displayFormat;
                        break;
                    }
                    pos++;
                }
                if (pos > 3) throw new ArgumentException("Unable to find valid BackBuffer Format");


                // check maximum capacity of depht buffer format , D32 is too greater than my purpose
                if (useStencil && DX9Enumerations.CheckDepthStencil(displayFormat, DepthFormat.D24S8))
                {
                    parameters.AutoDepthStencilFormat = D3D.Format.D24S8;
                }
                else if (DX9Enumerations.CheckDepthStencil(displayFormat, DepthFormat.D24X8))
                {
                    parameters.AutoDepthStencilFormat = D3D.Format.D24X8;
                }
                else if (useStencil && DX9Enumerations.CheckDepthStencil(displayFormat, DepthFormat.D15S1))
                {
                    parameters.AutoDepthStencilFormat = D3D.Format.D15S1;
                }
                else if (DX9Enumerations.CheckDepthStencil(displayFormat, DepthFormat.D16))
                {
                    parameters.AutoDepthStencilFormat = D3D.Format.D16;
                }
                else throw new Exception("Unable to find valid DepthStencilBuffer format");

                parameters.EnableAutoDepthStencil = true;
                //setting.parameters.BackBufferCount = 0;
                parameters.DeviceWindowHandle = whandle;
                parameters.SwapEffect = D3D.SwapEffect.Discard;
                parameters.Windowed = windowed;
                parameters.PresentationInterval = D3D.PresentInterval.Immediate;
                //param.FullScreenRefreshRateInHz = param.Windowed ? 0 : displayMode.RefreshRate; 
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return false;
            }
            return true;
        }

        /// <summary>
        /// Viewport size can be different from BackBuffer size
        /// </summary>
        public Viewport Viewport
        {
            get { return Helper.Convert(viewport); }
        }

        /// <summary>
        /// Aspect ration of viewport
        /// </summary>
        public float AspectRation
        {
            get { return viewport.AspectRatio; }
        }
        /// <summary>
        /// Width of viewport
        /// </summary>
        public float Width
        {
            get { return viewport.Width; }
        }
        /// <summary>
        /// Height of viewport
        /// </summary>
        public float Height
        {
            get { return viewport.Height; }
        }


        public void ResizeBackBuffer(int width, int height)
        {
            if (width < MINBACKBUFFERW) width = MINBACKBUFFERW;
            if (width > MAXBACKBUFFERW) width = MAXBACKBUFFERW;
            if (height < MINBACKBUFFERH) height = MINBACKBUFFERH;
            if (height > MAXBACKBUFFERH) height = MAXBACKBUFFERH;
            this.viewport = new DX.Viewport(0, 0, width, height);
            this.parameters.BackBufferWidth = width;
            this.parameters.BackBufferHeight = height;
        }

        public override string ToString()
        {
            return string.Format("{0}x{1} W:{2} H:{3}", parameters.BackBufferWidth, parameters.BackBufferHeight,viewport.Width,viewport.Height);
        }
    }

    /// <summary>
    /// DeviceSetting are used for Device initialization
    /// </summary>
    public class DeviceSetting
    {
        internal D3D.DeviceType devicetype;
        internal D3D.CreateFlags vertexprocessing;
        internal int adapterIdx;

        public DeviceSetting()
        {

        }

        internal bool InitParams(VideoAdapter card)
        {
            adapterIdx = card.adapterIdx;

            // Does a hardware device support shaders?
            if (DX9Enumerations.SupportsShader)
            {
                devicetype = D3D.DeviceType.Hardware;
                vertexprocessing = D3D.CreateFlags.None;

                if (DX9Enumerations.SupportsHardwareTransformAndLight)
                {
                    vertexprocessing = D3D.CreateFlags.HardwareVertexProcessing;
                    if (DX9Enumerations.SupportsPureDevice)
                        vertexprocessing |= D3D.CreateFlags.PureDevice;
                }
                else
                {
                    vertexprocessing = D3D.CreateFlags.SoftwareVertexProcessing;
                }
            }
            else
            {
                // No shader support, emulate by software but render will run to slowly
                devicetype = D3D.DeviceType.Reference;
                vertexprocessing = D3D.CreateFlags.FpuPreserve;
            }

            return true;
        }

    }


    public class RenderWindow
    {
        string name;
        bool disposed = true;
        bool begindraw = false;
        bool isFirstWindow;
        bool enablerestore = false;

        // backbuffer is the first render targhet, the first render targhet can't be null
        internal D3D.Surface backbuffer;
        protected D3D.SwapChain swapchain;
        protected D3D.Surface dephtstencil;

        public Device Device { get; private set; }

        public FrameSetting FrameSetting { get; private set; }
        
        public IntPtr WindowsHandle { get; private set; }

        public Color BackGround { get; set; }

        /// <summary>
        /// Initialize as first render windows, a new Device will be create , is the first swapchain
        /// </summary>
        public RenderWindow(FrameSetting FrameSetting) 
        {
            name = "Main_Render_Windows";
            
            VideoAdapter adapter = VideoCards.Adapter0;
            BackGround = Color.CornflowerBlue;

            this.FrameSetting = FrameSetting;

            if (!FrameSetting.InitParams(adapter)) throw new NotSupportedException("Frame Parameters Fail");

            DeviceSetting setting = new DeviceSetting();
            setting.InitParams(adapter);
            
            isFirstWindow = true;

            if (Device != null) throw new NotSupportedException("Multiple Device will be never implemented");

            // device initialization
            Device = new Device(this,setting);
            restoreDeviceSurface();

            disposed = false;
            enablerestore = true;


            // zbuffer and stecil can be disabled when rendering
            Device.renderstates.ZBufferEnable = FrameSetting.useZbuffer;
            Device.renderstates.StencilEnable = FrameSetting.useStencil;
        }

        /// <summary>
        /// Initialize an additional SwapChain
        /// </summary>
        public RenderWindow(FrameSetting FrameSetting , Device device)
        {
            name = "Swap_Render_Windows_" + Device.SwapChainList.Count;
            
            VideoAdapter adapter = VideoCards.Adapter0;
            this.Device = device;
            this.FrameSetting = FrameSetting;

            if (!FrameSetting.InitParams(adapter)) throw new NotSupportedException("Frame Parameters Fail");

            BackGround = Color.Aquamarine;

            isFirstWindow = false;

            if (Device == null) throw new ArgumentNullException("Need to initialize a first RenderWindows");

            // swapchain initialization
            swapchain = new D3D.SwapChain(Device.device, FrameSetting.parameters);
            restoreSwapSurface();
            
            Device.SwapChainList.Add(this);

            disposed = false;
            enablerestore = true;
        }


        ~RenderWindow()
        {
            if (isFirstWindow)
            {

            }
            else
            {
            }
        }


        public bool IsEnabled
        {
            get { return enablerestore; }
            set { enablerestore = value; }
        }

        public bool IsDisposed
        {
            get { return disposed; }
        }

        public void ClearDraw(Color background)
        {
            Device.device.Viewport = FrameSetting.viewport;

            D3D.ClearFlags flags = D3D.ClearFlags.Target;
            if (FrameSetting.useZbuffer) flags |= D3D.ClearFlags.ZBuffer;
            if (FrameSetting.useStencil) flags |= D3D.ClearFlags.Stencil;

            Device.device.Clear(flags, DX.ColorBGRA.FromBgra(background.ToArgb()), 1.0f, 0);
        }
        public void SetOriginalTarghet()
        {
            if (backbuffer == null) throw new Exception();
            Device.ClearRenderTarghets();
            // backbuffer is same for all swapchains
            Device.device.SetRenderTarget(0, backbuffer);
        }

        public void BeginDraw()
        {
            if (begindraw) throw new Exception("BeginScene() Already Started for " + name);

            Device.device.DepthStencilSurface = dephtstencil;
            Device.device.Viewport = FrameSetting.viewport;
            Device.renderstates.StencilEnable = FrameSetting.useStencil;
            Device.renderstates.ZBufferEnable = FrameSetting.useZbuffer;

            Device.device.BeginScene();
            begindraw = true;
        }

        public void EndDraw()
        {
            if (!begindraw) throw new Exception("BeginScene() Not Started for " + name);

            Device.device.EndScene();
            begindraw = false;
        }
        /// <summary>
        /// switch the back buffer and the front buffer
        /// </summary>
        public void Present()
        {
            if (isFirstWindow)
                Device.device.Present();
            else
                swapchain.Present(D3D.Present.None);
        }
        
        /// <summary>
        /// viewport are update only for semplicity
        /// </summary>
        public void ResizeBackBuffer(Size size)
        {
            if (size.Width > 1 && size.Height > 1)
            {
                if (size.Width < 1 || size.Height < 1) throw new ArgumentOutOfRangeException("Wrong size");

#if DEBUG
                Console.WriteLine("WINResize " + name);
#endif

                // when moving windows, the form call resize, but the backbuffer size is identical and not need to rebuild
                if (size.Width == FrameSetting.viewport.Width && size.Height == FrameSetting.viewport.Height) return;

                FrameSetting.viewport.Width = size.Width;
                FrameSetting.viewport.Height = size.Height;
                FrameSetting.parameters.BackBufferHeight = size.Height;
                FrameSetting.parameters.BackBufferWidth = size.Width;

                // in this case resize is slow because you need recreate all unmanaged resources
                if (isFirstWindow)
                {
                    Device.DeviceLost();
                    Device.DeviceRestore();
                }
                else
                {
                    DeviceLost();
                    DeviceRestore();
                }
            }
        }

        /// <summary>
        /// Restore surface reference for first swapchains case
        /// </summary>
        void restoreDeviceSurface()
        {
            //swapchain = Device.device.GetSwapChain(0);
            backbuffer = Device.device.GetBackBuffer(0, 0);
            dephtstencil = FrameSetting.useZbuffer ? Device.device.DepthStencilSurface : null;
#if DEBUG
            backbuffer.DebugName = name.ToString() + " backbuffer";
            if (dephtstencil!=null) dephtstencil.DebugName = name.ToString() + "dephtstencil";
#endif
        }
        /// <summary>
        /// Restore surface reference for other swapchains case
        /// </summary>
        void restoreSwapSurface()
        {
            backbuffer = swapchain.GetBackBuffer(0);

            if (FrameSetting.useZbuffer)
            {
                // Additional swap chains need their own depth buffer to support resizing them
                dephtstencil = D3D.Surface.CreateDepthStencil(
                    Device.device,
                    FrameSetting.viewport.Width,
                    FrameSetting.viewport.Height,
                    FrameSetting.parameters.AutoDepthStencilFormat,
                    FrameSetting.parameters.MultiSampleType,
                    FrameSetting.parameters.MultiSampleQuality,
                    (FrameSetting.parameters.PresentFlags & D3D.PresentFlags.DiscardDepthStencil) != 0);
            }
            else dephtstencil = null;

#if DEBUG
            backbuffer.DebugName = name.ToString() + " backbuffer";
            dephtstencil.DebugName = name.ToString() + "dephtstencil";
#endif
        }

        /// <summary>
        /// Dispose don't dispose device, NEVER Dispose device otherwise you have to re-initialize all resources, 
        /// vertexbuffers, indexbuffer, textures, effects, etc...
        /// </summary>
        public void Dispose()
        {
            if (swapchain != null) swapchain.Dispose();
            if (backbuffer != null) backbuffer.Dispose();
            if (dephtstencil != null) dephtstencil.Dispose();
            swapchain = null;
            backbuffer = null;
            dephtstencil = null;
            disposed = true;
        }

        /// <summary>
        /// Destroy set to unusable state
        /// </summary>
        public void Destroy()
        {
            if (!disposed) Dispose();
            disposed = true;
            IsEnabled = false;

            if (!isFirstWindow)
            {
                Device.SwapChainList.Remove(this);
            }
        }

        public void DeviceLost()
        {
#if DEBUG
            Console.WriteLine("DeviceLost " + name);
#endif
            if (!disposed) Dispose();
            disposed = true;
        }

        public void DeviceRestore()
        {
#if DEBUG
            Console.WriteLine("DeviceRestore " + name);
#endif
            if (!disposed) throw new ExecutionEngineException("Need to call DeviceLost() before restore");

            if (isFirstWindow)
            {
                restoreDeviceSurface();
            }
            // 'enabled' prevent swap restore before device restore
            else if (IsEnabled)
            {
                if (Device.IsDisposed) throw new ArgumentException("Can't restore " + name + ", Device is disposed");
                // swapchain can't be reseted
                swapchain = new D3D.SwapChain(Device.device, FrameSetting.parameters);
                restoreSwapSurface();
            }
            disposed = false;
        }
    }
}
