﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

using DX = SharpDX;
using D3D = SharpDX.Direct3D9;

using Engine.Tools;
using Engine.Maths;

namespace Engine
{
    /// <summary>
    /// My managed directx Font objects , in this case device can dispose and restore automatically
    /// the resource because i only store fontstyle and fontsize
    /// </summary>
    public class Font : DeviceResource
    {
        public enum FontName
        {
            Calibri,
            Arial
        }

        FontName fontstyle;

        int fontsize;
        D3D.Font font;

        public Font(Device device, FontName fontstyle, int fontsize)
            : base(device, false)
        {
            this.fontstyle = fontstyle;
            this.fontsize = fontsize;
            this.IsEnabled = true;

            DeviceRestore();
        }

        /// <summary>
        /// the dispose are necessary else visual studio generate a NULLEXCEPTION event when close form
        /// </summary>
        ~Font()
        {
            Destroy();
        }

        /// <summary>
        /// Set to device the texture, the method check if current class are initialized.
        /// If texture null or Empty the static int prevHandle will be set to 0;
        /// </summary>
        /// <param name="device"></param>
        public void Draw(string text, int x, int y, Color32 color)
        {
            if (!String.IsNullOrEmpty(text))
                font.DrawText(null, text, x, y, DX.ColorBGRA.FromBgra(color.argb));
        }

        public override void DeviceLost()
        {
            if (!disposed) Dispose();
        }
        public override void DeviceRestore()
        {
            if (IsEnabled)
            {
                font = new D3D.Font(device.device, new System.Drawing.Font(fontstyle.ToString(), fontsize, FontStyle.Bold));
                font.Disposing += new EventHandler<EventArgs>(SharpDxDisposingEvent);
                disposed = false;
            }
        }
        public override void Dispose()
        {
            if (!disposed && font != null) font.Dispose();
            disposed = true;
        }

        void SharpDxDisposingEvent(object sender, EventArgs args)
        {
            Console.WriteLine("Font disposing eventhandler (" + sender.ToString() + " ," + args.ToString() + "),");
        }

    }
}