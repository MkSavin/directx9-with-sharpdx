﻿#region LGPL License
/*
Axiom Graphics Engine Library
Copyright © 2003-2011 Axiom Project Team

The overall design, and a majority of the core engine and rendering code
contained within this library is a derivative of the open source Object Oriented
Graphics Engine OGRE, which can be found at http://ogre.sourceforge.net.
Many thanks to the OGRE team for maintaining such a high quality project.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/
#endregion

using System;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Text;

using Engine.Maths;
using Engine.Tools;

using DX = SharpDX;
using D3D = SharpDX.Direct3D9;
 

namespace Engine
{

    /// <summary>
    /// All possible and tested combinations
    /// </summary>
    public enum BufferUsage
    {
        /// <summary>
        /// I implement a emulator mode to check the write and read function in a temporary array (CPU), not used to draw
        /// </summary>
        Shadow,
        /// <summary> 
        /// Auto-restorable when device reset. Readable and Writable, faster than DynamicReadable but require a copy in RAM
        /// </summary>
        Managed,
        /// <summary>
        /// Not-restorable when device reset. Best performance. Only Writable
        /// </summary>
        Static,
        /// <summary> 
        /// Not-restorable when device reset. Only Writable. Optimized for many update
        /// </summary>
        Dynamic,
        /// <summary> 
        /// Not-restorable when device reset. Readable and Writable. Optimized for many update
        /// </summary>
        DynamicReadable
    }


    /// <summary>
    /// The common buffer base class, if memory pool used is default, this class will be automatically insert in
    /// the GraphicDevice events for a correct disposing. The Resetable implementation can't be called by device becuase i want not
    /// store also a shaddow copy of data, you must implement your custum restorable function.
    /// </summary>
    public abstract class BaseBuffer : DeviceResource
    {
        internal Pool memory;
        internal Usage usage;

        internal BufferLayout elementFormat;
        internal int elementCount;    // num of elements(as m_type) you are using, must be <= capacity
        internal int elementCapacity; // maximum num of elements(as m_type) can be stored
        

        // can't pinned a portion of array, so i need a offset and i will use the m_lockOffset also when write or read
        internal bool shadowMode = false;
        
        internal Array shadowArray;          // shadow buffer emulate the graphic buffer, used in debug mode without inizialize device
        internal Type shadowArrayType;       //type of buffer's elements
        internal GCHandle shadowArrayHandle;

        /// <summary>
        /// If buffer is not managed, you have to rewrite manualy
        /// </summary>
        protected BaseBuffer(Device device, BufferUsage usagemode, BufferLayout elementformat): base(device)
        {
            this.shadowMode = false;
            this.elementCount = 0;
            this.elementFormat = elementformat;
            this.IsEnabled = true;

            switch (usagemode)
            {
                case BufferUsage.Shadow:
                    shadowMode = true;
                    managed = false;
                    IsReadable = true;
                    break;
                case BufferUsage.Managed:
                    usage = Usage.None;
                    memory = Pool.Managed;
                    managed = true;
                    IsReadable = true;
                    break;
                case BufferUsage.Static:
                    usage = Usage.WriteOnly;
                    memory = Pool.Default;
                    managed = false;
                    IsReadable = false;
                    break;
                case BufferUsage.Dynamic:
                    usage = Usage.Dynamic | Usage.WriteOnly;
                    memory = Pool.Default;
                    managed = false;
                    IsReadable = false;
                    break;
                case BufferUsage.DynamicReadable:
                    usage = Usage.Dynamic;
                    memory = Pool.Default;
                    managed = false;
                    IsReadable = true;
                    break;

                    
            }
            base.SetManagedFlag(managed);
        }


        protected abstract void InitBuffer(int size);

        /// <summary>
        /// Get or Set the number of elements to use in the buffer, Very important to set manually.
        /// After device reseting and the pool is "default", the Count value was set to zero because all data are
        /// invalidated.
        /// </summary>
        /// <remarks>
        /// Count mean : use the elements from 0 to Count, the buffer can contain valid elements from Count to Capacity
        /// but the idea is to discard them during process
        /// </remarks>
        public int Count
        {
            get { return elementCount; }
            set { elementCount = (value > elementCapacity) ? elementCapacity : value; }
        }
        /// <summary>
        /// Get the maximum number of elements that buffer can store, if you need to increase it the only solution is create a new buffer
        /// CAREFULL , increase or decrease size do a completly re-initialization and is slow
        /// </summary>
        public int Capacity { get { return elementCapacity; } }
        /// <summary>
        /// Get if buffer can be read.
        /// </summary>
        public bool IsReadable { get; private set; }

        /// <summary>
        /// Device call DeviceLost
        /// </summary>
        public override void DeviceLost()
        {
            Dispose();
        }
        /// <summary>
        /// Device call DeviceRestore if IsEnabled == true
        /// </summary>
        public override void DeviceRestore()
        {
            InitBuffer(elementCapacity);
        }
    }

    /// <summary>
    /// Utility class to manage the stream's pointer optained when you look it
    /// </summary>
    internal abstract class BufferLock
    {
        protected const int sizefloat = sizeof(float);
        protected const int sizeint16 = sizeof(short);
        protected const int sizeint32 = sizeof(int);

        protected BaseBuffer buffer;
        protected IntPtr bufferPtr;
        protected bool locked = false;
        protected LockedSegment lockedSegment;

        public BufferLock(BaseBuffer buffer)
        {
            this.buffer = buffer;
        }
        /// <summary>
        /// Open the buffer. The offset and count value are relative to Type used
        /// </summary>
        /// <param name="onlyread">if you want only read from buffer, example to return the elements array when coping</param>
        /// <remarks>
        /// to use onlyread mode, ensure buffer is readable, overwise directx return always a zero buffer, so isn't usefull.
        /// </remarks>
        protected void LockSegment(int offset, int count, bool onlyread)
        {
            if (offset > 0 && buffer.shadowMode) throw new NotImplementedException("In emulator mode i must open entire buffer");

            // size in byte of whole buffer
            int buffersize = buffer.elementCapacity * buffer.elementFormat.bytesize;
            // offset in byte to lock
            int boffset = offset * buffer.elementFormat.bytesize;
            // size in byte to lock
            int bsize = count * buffer.elementFormat.bytesize;

            lockedSegment = new LockedSegment(buffersize, boffset, bsize);

            if (!buffer.IsReadable && onlyread)
                throw new Exception("Can't readonly this buffer, is writeonly");

            // only read case
            LockFlags mode = onlyread ? LockFlags.ReadOnly : LockFlags.Normal;

            // only write case
            if (!buffer.IsManaged && !buffer.IsReadable)
            {
                if ((buffer.usage & Usage.Dynamic) != 0)
                {
                    // only if dynamic and writeonly, you can discard if you are open  
                    // whole stored data. if not writeonly, use LockFlags.None
                    if (count >= buffer.elementCount)
                        mode |= LockFlags.Discard;
                    else
                        mode |= LockFlags.NoOverwrite;
                }
            }
            bufferPtr = LockImpl(lockedSegment, mode);
            locked = bufferPtr != IntPtr.Zero;
        }

        protected void UnLockSegment()
        {
            if (locked)
            {
                UnLockImpl();
                bufferPtr = IntPtr.Zero;
            }

            locked = false;    
        }

        /// <summary>
        /// Return the gpu buffer managed pointer
        /// </summary>
        protected abstract IntPtr LockImpl(LockedSegment size, LockFlags mode);
        /// <summary>
        /// Release the gpu buffer pointer
        /// </summary>
        protected abstract void UnLockImpl();

        /// <summary>
        /// Locked size in bytes
        /// </summary>
        protected struct LockedSegment
        {
            /// <summary>
            /// Main buffer bytes sizes
            /// </summary>
            public int buffersize;
            /// <summary>
            /// Offset bytes in the main buffer where locked began
            /// </summary>
            public int bufferoffset ;
            /// <summary>
            /// Size bytes of locked segment
            /// </summary>
            public int lockedsize;

            public LockedSegment(int buffersize, int byteoffset, int lockedsize)
            {
                if (byteoffset + lockedsize > buffersize || lockedsize==0) throw new ArgumentOutOfRangeException("you try to open a incorrect size");
                this.buffersize = buffersize;
                this.bufferoffset = byteoffset;
                this.lockedsize = lockedsize;
            }
        }
    }



}
