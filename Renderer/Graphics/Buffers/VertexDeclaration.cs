﻿using System;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Text;

using DX = SharpDX;
using D3D = SharpDX.Direct3D9;

using Engine.Maths;
using Engine.Tools;
 

#pragma warning disable 1591
namespace Engine
{
    /// <summary>
    /// Instruct the device about vertex format, contain all informations about vertices
    /// </summary>
    public class VertexDeclaration : DeviceResource
    {
        internal D3D.VertexDeclaration declaration;

        /// <summary>
        /// Layout of buffer's vertex element
        /// </summary>
        public VertexLayout Format { get; private set; }

        /// <summary>
        /// Initialize a new vertex descriptor
        /// </summary>
        public VertexDeclaration(Device device, params VertexLayout[] Streams)
            : this(device)
        {
            List<VertexElement> Elements = new List<VertexElement>();
            foreach (VertexLayout layout in Streams)
            {
                Elements.AddRange(layout.Elements);
            }
            Format = new VertexLayout(Elements);

            if (device != null)
            {
                int count = Elements.Count;
                D3D.VertexElement[] m_elements = new D3D.VertexElement[count + 1];

                for (int i = 0; i < count; i++) m_elements[i] = Helper.Convert(Elements[i]);
                m_elements[count] = Helper.Convert(VertexElement.VertexDeclarationEnd);

                declaration = new D3D.VertexDeclaration(device.device, m_elements);
            }
        }


        /// <summary>
        /// Initialize a new vertex descriptor
        /// </summary>
        /// <param name="device">the device to use</param>
        /// <param name="Elements">Vertex struct descriptor</param>
        public VertexDeclaration(Device device, IList<VertexElement> Elements)
            : this(device, null, Elements) { }

        /// <summary>
        /// Initialize a new vertex descriptor, the type can be null
        /// </summary>
        /// <param name="type">if Elements have a struct, you can pass it</param>
        public VertexDeclaration(Device device, Type type, IList<VertexElement> Elements)
            : this(device)
        {
            Format = new VertexLayout(Elements, type);

            if (device != null)
            {
                int count = Elements.Count;
                D3D.VertexElement[] m_elements = new D3D.VertexElement[count + 1];

                for (int i = 0; i < count; i++) m_elements[i] = Helper.Convert(Elements[i]);
                m_elements[count] = Helper.Convert(VertexElement.VertexDeclarationEnd);

                declaration = new D3D.VertexDeclaration(device.device, m_elements);
            }
        }

        /// <summary>
        /// Used only for derived class
        /// </summary>
        protected VertexDeclaration(Device device)
            : base(device, true)
        {
            IsEnabled = true;
        }

        /// <summary>
        /// size in byte of vertex structure, is the sum of each elements size
        /// </summary>
        public virtual int SizeInByte
        {
            get { return Format.bytesize; }
        }

        public bool ContainSemantic(DeclarationUsage usage, int usageidx)
        {
            VertexElement element;
            if (!Format.GetElement(usage, usageidx, out element)) return false;
            return true;
        }

        public bool GetElement(DeclarationUsage usage, int usageidx, out VertexElement element)
        {
            return Format.GetElement(usage, usageidx, out element);
        }

        public override void DeviceRestore()
        {
            return;// because is managed
        }
        public override void DeviceLost()
        {
            return;// because is managed
        }

        public override void Dispose()
        {
            declaration.Dispose();
        }

        public override string ToString()
        {
            return string.Format("Size: {0} format:[{1}]", SizeInByte, Format.ToString());
        }

#if USE_OLD_FIXED_FUNCTION_PIPELINE
        /// <summary>
        /// Maps members of a D3DVERTEXELEMENT9 declaration to a FVF code.
        /// Since the goal is not use directx8 but use shaders, FVF code are deprecated for a more powerfull
        /// custom declaration, but just for learning i implement a conversion
        /// </summary>
        public static VertexFormat EncodeFVF(IList<VertexElement> elements)
        {
            /*
            ///////////////////////////////////////////////////////////////////////////////////
            // This table maps members of a D3DVERTEXELEMENT9 declaration to a FVF code.
            ///////////////////////////////////////////////////////////////////////////////////
            Data type	            Usage	               Usage index	FVF
            D3DDECLTYPE_FLOAT3  	D3DDECLUSAGE_POSITION   	0	    D3DFVF_XYZ
            D3DDECLTYPE_FLOAT4  	D3DDECLUSAGE_POSITIONT	    0	    D3DFVF_XYZRHW
            D3DDECLTYPE_FLOATn  	D3DDECLUSAGE_BLENDWEIGHT	0	    D3DFVF_XYZBn
            D3DDECLTYPE_UBYTE4	    D3DDECLUSAGE_BLENDINDICES	0	    D3DFVF_XYZB (nWeights+1)
            D3DDECLTYPE_FLOAT3	    D3DDECLUSAGE_NORMAL       	0	    D3DFVF_NORMAL
            D3DDECLTYPE_FLOAT1	    D3DDECLUSAGE_PSIZE	        0	    D3DFVF_PSIZE
            D3DDECLTYPE_D3DCOLOR	D3DDECLUSAGE_COLOR	        0	    D3DFVF_DIFFUSE
            D3DDECLTYPE_D3DCOLOR	D3DDECLUSAGE_COLOR	        1	    D3DFVF_SPECULAR
            D3DDECLTYPE_FLOATm	    D3DDECLUSAGE_TEXCOORD   	n	    D3DFVF_TEXCOORDSIZEm(n)
            D3DDECLTYPE_FLOAT3	    D3DDECLUSAGE_POSITION	    1	    N/A
            D3DDECLTYPE_FLOAT3  	D3DDECLUSAGE_NORMAL     	1   	N/A
        
            ///////////////////////////////////////////////////////////////////////////////////
            //This table maps FVF codes to a D3DVERTEXELEMENT9 structure.
            ///////////////////////////////////////////////////////////////////////////////////
            FVF	            Data type	        Usage	                Usage index
            D3DFVF_XYZ      D3DDECLTYPE_FLOAT3	D3DDECLUSAGE_POSITION	0
        
            D3DFVF_XYZRHW	D3DDECLTYPE_FLOAT4	D3DDECLUSAGE_POSITIONT	0
        
            D3DFVF_XYZW	    D3DDECLTYPE_FLOAT4	D3DDECLUSAGE_POSITION	0
        
            D3DFVF_XYZB5             D3DVSDT_FLOAT3,   D3DDECLUSAGE_POSITION,  	0
            D3DFVF_LASTBETA_UBYTE4	 D3DVSDT_FLOAT4,   D3DDECLUSAGE_BLENDWEIGHT,
                                     D3DVSDT_UBYTE4	   D3DDECLUSAGE_BLENDINDICES
        
            D3DFVF_XYZB5                 D3DVSDT_FLOAT3,     D3DDECLUSAGE_POSITION,
            D3DFVF_LASTBETA_D3DCOLOR	 D3DVSDT_FLOAT4, 	 D3DDECLUSAGE_BLENDWEIGHT, 	0
                                         D3DVSDT_D3DCOLOR    D3DDECLUSAGE_BLENDINDICES
        
            D3DFVF_XYZB5	D3DDECLTYPE_FLOAT3, D3DDECLTYPE_FLOAT4, D3DDECLTYPE_FLOAT1	D3DDECLUSAGE_POSITION, D3DDECLUSAGE_BLENDWEIGHT, D3DDECLUSAGE_BLENDINDICES	0
        
            D3DFVF_XYZBn (n=1..4)	D3DDECLTYPE_FLOAT3, D3DDECLTYPE_FLOATn	D3DDECLUSAGE_POSITION, D3DDECLUSAGE_BLENDWEIGHT	0
        
            D3DFVF_XYZBn (n=1..4)
            D3DFVF_LASTBETA_UBYTE4	D3DDECLTYPE_FLOAT3, D3DDECLTYPE_FLOAT(n-1), D3DDECLTYPE_UBYTE4	D3DDECLUSAGE_POSITION, D3DDECLUSAGE_BLENDWEIGHT, D3DDECLUSAGE_BLENDINDICES	0
        
            D3DFVF_XYZBn (n=1..4)
            D3DFVF_LASTBETA_D3DCOLOR	D3DDECLTYPE_FLOAT3, D3DDECLTYPE_FLOAT(n-1), D3DDECLTYPE_D3DCOLOR	D3DDECLUSAGE_POSITION, D3DDECLUSAGE_BLENDWEIGHT, D3DDECLUSAGE_BLENDINDICES	0
        
            D3DFVF_NORMAL	D3DDECLTYPE_FLOAT3	       D3DDECLUSAGE_NORMAL	0
        
            D3DFVF_PSIZE	D3DDECLTYPE_FLOAT1	       D3DDECLUSAGE_PSIZE	0
        
            D3DFVF_DIFFUSE	D3DDECLTYPE_D3DCOLOR	    D3DDECLUSAGE_COLOR	0
        
            D3DFVF_SPECULAR	D3DDECLTYPE_D3DCOLOR	    D3DDECLUSAGE_COLOR	1
        
            D3DFVF_TEXCOORDSIZEm(n)	D3DDECLTYPE_FLOATm	D3DDECLUSAGE_TEXCOORD	n
            */

            VertexFormat fvf = VertexFormat.None;
            int nWeights = 0;

            foreach (VertexElement element in elements)
            {
                DeclarationType type = element.type;
                DeclarationUsage usage = element.usage;
                int usageidx = element.usageIndex;

                switch (usage)
                {
                    // POSITION
                    case DeclarationUsage.Position:
                        if (usageidx != 0)
                            throw new NotSupportedException("usage index " + usageidx + " not supported");
                        if (type != DeclarationType.Float3)
                            throw new NotSupportedException("decl type " + type + " not supported");
                        fvf |= VertexFormat.Position;
                        break;

                    // POSITIONT
                    case DeclarationUsage.PositionTransformed:
                        if (usageidx != 0)
                            throw new NotSupportedException("usage index " + usageidx + " not supported");
                        if (type != DeclarationType.Float4)
                            throw new NotSupportedException("decl type " + type + " not supported");
                        fvf |= VertexFormat.Transformed;
                        break;

                    // BLENDWEIGHT
                    case DeclarationUsage.BlendWeight:
                        if (usageidx != 0)
                            throw new NotSupportedException("usage index " + usageidx + " not supported");
                        switch (type)
                        {
                            case DeclarationType.Float1: fvf |= VertexFormat.PositionBlend1; nWeights = 1; break;
                            case DeclarationType.Float2: fvf |= VertexFormat.PositionBlend2; nWeights = 2; break;
                            case DeclarationType.Float3: fvf |= VertexFormat.PositionBlend3; nWeights = 3; break;
                            case DeclarationType.Float4: fvf |= VertexFormat.PositionBlend4; nWeights = 4; break;
                            default: throw new NotSupportedException("decl type " + type + " not supported");
                        }
                        break;

                    // BLENDINDICES
                    case DeclarationUsage.BlendIndices:
                        throw new NotSupportedException("unknow");

                    // NORMAL
                    case DeclarationUsage.Normal:
                        if (usageidx != 0)
                            throw new NotSupportedException("usage index " + usageidx + " not supported");
                        if (type != DeclarationType.Float3)
                            throw new NotSupportedException("decl type " + type + " not supported");
                        fvf |= VertexFormat.Normal;
                        break;

                    // PSIZE
                    case DeclarationUsage.PointSize:
                        if (usageidx != 0)
                            throw new NotSupportedException("usage index " + usageidx + " not supported");
                        if (type != DeclarationType.Float1)
                            throw new NotSupportedException("decl type " + type + " not supported");
                        fvf |= VertexFormat.PointSize;
                        break;

                    // COLOR
                    case DeclarationUsage.Color:
                        if (type != DeclarationType.Color)
                            throw new NotSupportedException("decl type " + type + " not supported");
                        switch (usageidx)
                        {
                            case 0: fvf |= VertexFormat.Diffuse; break;
                            case 1: fvf |= VertexFormat.Specular; break;
                            default: throw new NotSupportedException("usage index " + usageidx + " not supported");
                        }
                        break;

                    // TEXCOORD
                    case DeclarationUsage.TexCoord:
                        // example : two 1D texture coordinates
                        // = VertexFormat.Texture2 | Helper.GetVertexTextureCoordinate(1, 0) | Helper.GetVertexTextureCoordinate(1, 1)
                        switch (usageidx)
                        {
                            case 0: fvf |= VertexFormat.Texture1; break; //one 2D texture coordinates.
                            case 1: fvf |= VertexFormat.Texture2; break; //two 2D texture coordinates.
                            case 2: fvf |= VertexFormat.Texture3; break; //etc...
                            case 3: fvf |= VertexFormat.Texture4; break;
                            case 4: fvf |= VertexFormat.Texture5; break;
                            case 5: fvf |= VertexFormat.Texture6; break;
                            case 6: fvf |= VertexFormat.Texture7; break;
                            case 7: fvf |= VertexFormat.Texture8; break;
                            default: throw new NotSupportedException("usage index " + usageidx + " not supported, dx9 accept only maximum 8 textures");
                        }
                        switch (type)
                        {
                            case DeclarationType.Float1: fvf |= Helper.GetVertexTextureCoordinate(1, usageidx); break;
                            case DeclarationType.Float2: break; // i think is zero
                            case DeclarationType.Float3: fvf |= Helper.GetVertexTextureCoordinate(3, usageidx); break;
                            case DeclarationType.Float4: fvf |= Helper.GetVertexTextureCoordinate(4, usageidx); break;
                            default: throw new NotSupportedException("decl type " + type + " not supported");
                        }
                        break;

                    default: throw new NotSupportedException("usage " + usage.ToString() + " not supported");
                }
            }

            if (nWeights > 0) fvf |= (VertexFormat)(((int)VertexFormat.PositionBlend1 + (nWeights) * 2) & 0x00F);

            return fvf;
        }
#endif
    }
}
#pragma warning restore 1591
