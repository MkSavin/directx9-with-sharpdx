﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

using Engine.Maths;
using Engine.Tools;

using DX = SharpDX;
using D3D = SharpDX.Direct3D9;
 

namespace Engine
{

    /// <summary>
    /// Utility class to manage the vertexstream's pointer optained when you look it
    /// </summary>
    internal class VertexLock : BufferLock
    {
        VertexBuffer vertexbuffer;
        public VertexStream stream { get; private set; }

        public VertexLock(VertexBuffer vertexBuffer)
            : base(vertexBuffer)
        {
            this.vertexbuffer = vertexBuffer;

        }

        public void Open(int VertexOffset, int VertexCount, bool onlyread = false)
        {
            base.LockSegment(VertexOffset, VertexCount, onlyread);
            stream = new VertexStream(bufferPtr, vertexbuffer.Format, VertexCount, vertexbuffer.IsReadable);
        }
        public void Close()
        {
            base.UnLockSegment();
            stream.Close();
            stream = null;
        }

        protected override IntPtr LockImpl(LockedSegment section, LockFlags mode)
        {
            if (vertexbuffer.IsDisposed) throw new Exception("buffer not restored");

            if (vertexbuffer.vbuffer != null)
            {
                return vertexbuffer.vbuffer.Lock(section.bufferoffset, section.lockedsize, (D3D.LockFlags)mode).DataPointer;
            }
            else if (vertexbuffer.shadowArray != null)
            {
                // emulator mode
                vertexbuffer.shadowArrayHandle = GCHandle.Alloc(vertexbuffer.shadowArray, GCHandleType.Pinned);
                return vertexbuffer.shadowArrayHandle.AddrOfPinnedObject();
            }
            else
            {
                return IntPtr.Zero;
            }
        }      
        protected override void UnLockImpl()
        {
            if (vertexbuffer.IsDisposed) throw new Exception("buffer not restored");
            if (vertexbuffer.vbuffer != null)
                vertexbuffer.vbuffer.Unlock();

            if (vertexbuffer.shadowArray != null && vertexbuffer.shadowArrayHandle.IsAllocated)
                vertexbuffer.shadowArrayHandle.Free();

        }
    }

}
