﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

using DX = SharpDX;
using D3D = SharpDX.Direct3D9;

using Engine.Maths;

namespace Engine
{
    /// <summary>
    /// If buffer is not managed, you can assign the rewrite delegate function to automatize
    /// </summary>
    public class IndexBuffer : BaseBuffer
    {
        internal D3D.IndexBuffer ibuffer;

        IndexLock locktool;

        /// <summary>
        /// Is the buffer's element declaration
        /// </summary>
        public IndexLayout Format { get; private set; }
              
        /// <summary>
        /// Emulator mode, generate a Array that simulate the directx buffer
        /// </summary>
        public IndexBuffer(IndexLayout format, int size, bool readable)
            : base(null, BufferUsage.Shadow, format)
        {
            Format = format;
            shadowArrayType = format.type;

            if (!shadowArrayType.IsValueType || shadowArrayType.IsEnum)
                throw new ArgumentException("type must be a struct with fized size");

            if (format.bytesize != Marshal.SizeOf(format.type))
                throw new ArgumentException("size of type " + format.type.ToString() + " don't match with declaration");

            InitBuffer(size);
        }

        /// <summary>
        /// If buffer is not managed, you can assign the rewrite delegate function to automatize
        /// </summary>
        /// <param name="size">
        /// capacity of indices using "IndexInfo", Example if you use IndexInfo.Face16 and size 1
        /// it mean that Directx9 IndexBuffer contain 3 ushort so is equivalent of IndexInfo.One16 and size 3
        /// </param>
        /// <param name="format">Define the type used to rappresent indices, can be a 16 or 32bit number or Face struct</param>
        public IndexBuffer(Device device, IndexLayout format, BufferUsage usagemode, int size)
            : base(device, usagemode, format)
        {
            Format = format;
            InitBuffer(size);
        }
        /// <summary>
        /// Recreate a empty index buffer but with different size, Count will be set to zero
        /// </summary>
        /// <param name="count">capacity of buffer in currect elements type size, not in bytes</param>
        protected override void InitBuffer(int count)
        {
            if (count < 1) throw new Exception("Directx9 can't initialize a buffer with zero bytes");

            Dispose();
            
            elementCount = 0;
            elementCapacity = count;

            if (shadowMode)
            {
                shadowArray = Array.CreateInstance(shadowArrayType, count);
            }
            else
            {
                ibuffer = new D3D.IndexBuffer(device.device, elementCapacity * elementFormat.bytesize, (D3D.Usage)usage, (D3D.Pool)memory, !Format.is32Bit);
                ibuffer.DebugName = "ShardDx9IndexBuffer";
            }
            disposed = false;
        }


        public IndexStream OpenStream(int Offset, int Count, bool ReadOnly = false)
        {
            locktool = new IndexLock(this);
            locktool.Open(Offset, Count, ReadOnly);
            return locktool.stream;
        }
        public IndexStream OpenStream(bool ReadOnly = false)
        {
            return OpenStream(0, elementCapacity, ReadOnly);
        }

        public void CloseStream()
        {
            locktool.stream.Close();
            locktool = null;
        }


        /// <summary>
        /// Original set data implementation
        /// </summary>
        /// <param name="bstride">size in byte of one vertex</param>
        /// <param name="boffset">num of vertice to jump</param>
        public void SetData<T>(T[] vertices, int offset, int bstride, LockFlags mode) where T : struct
        {
            DX.DataStream dataStream = ibuffer.Lock(bstride * offset, bstride * vertices.Length, (D3D.LockFlags)mode);
            dataStream.WriteRange<T>(vertices, 0, vertices.Length);
            ibuffer.Unlock();
        }

        /// <summary>
        /// </summary>
        /// <param name="count">number of vertices</param>
        /// <param name="boffset">number of vertices to jump</param>
        /// <param name="bstride">size in byte of one vertex</param>
        public T[] GetData<T>(int count, int offset, int bstride) where T : struct
        {
            DX.DataStream dataStream = ibuffer.Lock(bstride * offset, bstride * count, D3D.LockFlags.ReadOnly);
            T[] data = dataStream.ReadRange<T>(count);
            ibuffer.Unlock();
            return data;
        }

        public override void Dispose()
        {
            // Check to see if Dispose has already been called.
            if (!disposed)
            {
                if (ibuffer != null && !ibuffer.IsDisposed) ibuffer.Dispose();
                ibuffer = null;
                elementCount = 0;
                disposed = true;
            }
        }
        
        
        public override string ToString()
        {
            return ibuffer.DebugName.ToString() + "_" + ibuffer.Description.Type.ToString();
        }
    }




    
}
