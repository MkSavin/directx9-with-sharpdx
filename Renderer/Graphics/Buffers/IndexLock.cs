﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

using Engine.Maths;
using Engine.Tools;

using DX = SharpDX;
using D3D = SharpDX.Direct3D9;
 

namespace Engine
{
    /// <summary>
    /// Utility class to manage the indexstream's pointer optained when you look it
    /// </summary>
    internal class IndexLock : BufferLock
    {
        IndexBuffer indexbuffer;

        public IndexStream stream { get; private set; }

        public IndexLock(IndexBuffer indexbuffer)
            : base(indexbuffer)
        {
            this.indexbuffer = indexbuffer;
        }

        public void Open(int IndexOffset, int IndexCount, bool onlyread = false)
        {
            base.LockSegment(IndexOffset, IndexCount, onlyread);
            stream = new IndexStream(bufferPtr, indexbuffer.Format, IndexCount, indexbuffer.IsReadable);
        }

        public void Close()
        {
            base.UnLockSegment();
            stream.Close();
            stream = null;
        }

        #region Lock implementation
        protected override IntPtr LockImpl(LockedSegment section, LockFlags mode)
        {
            if (indexbuffer.IsDisposed) throw new Exception("buffer not restored");

            if (indexbuffer.ibuffer != null)
            {
                return indexbuffer.ibuffer.Lock(lockedSegment.bufferoffset, lockedSegment.lockedsize, (D3D.LockFlags)mode).DataPointer;
            }
            else if (indexbuffer.shadowArray != null)
            {
                // emulator mode
                indexbuffer.shadowArrayHandle = GCHandle.Alloc(indexbuffer.shadowArray, GCHandleType.Pinned);
                return indexbuffer.shadowArrayHandle.AddrOfPinnedObject();
            }
            else
            {
                return IntPtr.Zero;
            }
        }
        protected override void UnLockImpl()
        {
            if (indexbuffer.IsDisposed) throw new Exception("buffer not restored");

            if (indexbuffer.ibuffer != null)
                indexbuffer.ibuffer.Unlock();

            if (indexbuffer.shadowArray != null && indexbuffer.shadowArrayHandle.IsAllocated)
                indexbuffer.shadowArrayHandle.Free();

        }
        #endregion
    }
}
