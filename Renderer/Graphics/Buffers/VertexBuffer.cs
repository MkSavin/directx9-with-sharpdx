﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

using DX = SharpDX;
using D3D = SharpDX.Direct3D9;

namespace Engine
{
    /// <summary>
    /// If buffer is not managed, you can assign the rewrite delegate function to automatize.
    /// If GraphicDevice is null, the buffer work in emulator mode, the data are stored in a shadow array
    /// </summary>
    public class VertexBuffer : BaseBuffer
    {
        internal D3D.VertexBuffer vbuffer;

        VertexLock locktool;

        /// <summary>
        /// Is the buffer's element declaration
        /// </summary>
        public VertexLayout Format { get; private set; }

        /// <summary>
        /// Emulator mode, generate a Array that simulate the directx buffer
        /// </summary>
        public VertexBuffer(Type vertextype, VertexLayout format, int size)
            : base(null, BufferUsage.Shadow, format)
        {
            /* EXAMPLE
            VertexBuffer shadow = new VertexBuffer(tcvertex_decl, mesh.numVertices, true);
            shadow.Open();
            shadow.Write<Vector3>(mesh.vertices.data,NTVERTEX.m_elements[0], mesh.numVertices, 0);
            shadow.Write<Vector3>(mesh.normals.data,NTVERTEX.m_elements[1] ,mesh.numVertices, 0);
            shadow.Write<Vector2>(mesh.textures.data,NTVERTEX.m_elements[2], mesh.numVertices, 0);
            shadow.Close();
            shadow.Count = mesh.numVertices;
            */
            Format = format;
            shadowArrayType = vertextype;

            if (!shadowArrayType.IsValueType || shadowArrayType.IsEnum)
                throw new ArgumentException("type must be a struct with fized size");

            if (Format.bytesize != Marshal.SizeOf(shadowArrayType))
                throw new ArgumentException("size of type " + shadowArrayType.ToString() + " don't match with declaration");

            InitBuffer(size);
        }

        /// <summary>
        /// If buffer is not managed, you can assign the rewrite delegate function to automatize
        /// </summary>
        /// <param name="declaration">vertex type information</param>
        /// <param name="size">capacity of vertices</param>
        public VertexBuffer(Device device, VertexLayout format, BufferUsage usagemode, int size)
            : base(device, usagemode, format)
        {
            Format = format;
            InitBuffer(size);
        }

    
        /// <summary>
        /// Recreate a empty vertex buffer but with different size, Count will be set to zero
        /// </summary>
        /// <param name="count">capacity of buffer in currect elements type size, not in bytes</param>
        protected override void InitBuffer(int count)
        {
            if (count < 1) throw new Exception("Directx9 can't initialize a buffer with zero bytes");
            if (!disposed) Dispose();
            
            elementCount = 0;
            elementCapacity = count;

            if (shadowMode)
            {
                shadowArray = Array.CreateInstance(shadowArrayType, count);
            }
            else
            {
                vbuffer = new D3D.VertexBuffer(device.device, elementCapacity * elementFormat.bytesize, (D3D.Usage)usage, D3D.VertexFormat.None, (D3D.Pool)memory);
                vbuffer.DebugName = "ShardDx9VertexBuffer";
                vbuffer.Disposing += new EventHandler<EventArgs>(SharpDxDisposingEvent);
                vbuffer.Disposed += new EventHandler<EventArgs>(SharpDxDisposedEvent);
            }
            disposed = false;
        }


        public VertexStream OpenStream(int Offset, int Count, bool ReadOnly=false)
        {
            locktool = new VertexLock(this);
            locktool.Open(Offset, Count, ReadOnly);
            return locktool.stream;
        }
        public VertexStream OpenStream(bool ReadOnly=false)
        {
            return OpenStream(0, elementCapacity, ReadOnly);
        }

        public void CloseStream()
        {
            locktool.stream.Close();
            locktool = null;
        }


        /// <summary>
        /// Set data original implementation
        /// </summary>
        /// <param name="bstride">size in byte of one vertex</param>
        /// <param name="boffset">num of vertice to jump</param>
        void SetData<T>(T[] vertices, int offset, int bstride, LockFlags mode) where T : struct
        {
            DX.DataStream dataStream = vbuffer.Lock(bstride * offset, bstride * vertices.Length, (D3D.LockFlags)mode);
            dataStream.WriteRange<T>(vertices, 0, vertices.Length);
            vbuffer.Unlock();
        }

        /// <summary>
        /// Get data original implementation
        /// </summary>
        /// <param name="count">number of vertices</param>
        /// <param name="boffset">number of vertices to jump</param>
        /// <param name="bstride">size in byte of one vertex</param>
        T[] GetData<T>(int count, int offset, int bstride) where T : struct
        {
            DX.DataStream dataStream = vbuffer.Lock(bstride * offset, bstride * count, D3D.LockFlags.ReadOnly);
            T[] data = dataStream.ReadRange<T>(count);
            vbuffer.Unlock();
            return data;
        }


        public override void Dispose()
        {
            // Check to see if Dispose has already been called.
            if (!disposed)
            {
                // Dispose managed resources.
                if (vbuffer != null)
                {
                    vbuffer.Dispose();
                    vbuffer = null;
                }
                else if (shadowArray != null)
                {
                    shadowArray = null;
                }
                elementCount = 0;
                disposed = true;
            }
        }


        void SharpDxDisposingEvent(object sender, EventArgs args)
        {
            D3D.VertexBuffer vbuffer = (D3D.VertexBuffer)sender;
            //Console.WriteLine("Vertex Disposing" + vbuffer.ToString());
        }
        void SharpDxDisposedEvent(object sender, EventArgs args)
        {
            D3D.VertexBuffer vbuffer = (D3D.VertexBuffer)sender;
            //Console.WriteLine("Vertex Reseted" + vbuffer.ToString());
        }

        public override string ToString()
        {
            return vbuffer.DebugName.ToString() + "_" + vbuffer.Description.Type.ToString();
        }
    }
}
