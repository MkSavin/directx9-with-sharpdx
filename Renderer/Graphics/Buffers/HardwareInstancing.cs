﻿using System;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Text;

using Engine.Maths;
using Engine.Tools;

using DX = SharpDX;
using D3D = SharpDX.Direct3D9;
 

namespace Engine
{
    /// <summary>
    /// Hardware Indexed-Geometry Instancing, simple group of necessary buffers
    /// </summary>
    /// <remarks>
    /// Drawing Non-Indexed Geometry From https://msdn.microsoft.com/en-us/library/windows/desktop/bb173349(v=vs.85).aspx
    /// "This technique is not supported by hardware acceleration on any device.
    /// It is only supported by software vertex processing and will work only with vs_3_0 shaders."
    /// 
    /// so i need to check also  if (!render.softwareProcessing)
    /// </remarks>
    public class HWInstancedGeometry
    {
        Device device;

        public Effect shadercode;
        public IndexBuffer indexbuffer;
        public VertexDeclaration vertexdeclaration;
        public VertexBuffer geomtrybuffer;
        public VertexBuffer instancebuffer;
        public int NumGeometryVertices;
        public int NumGeometryPrimitives;
        public int NumInstances;
        public PrimitiveType PrimitiveType;


        public HWInstancedGeometry(Device device)
        {
            if (DX9Enumerations.VertexShaderVersion.Major < 3)
                throw new NotSupportedException("Require Shader 3.0");
            this.device = device;

            //foreach (VertexElement element in geomtrybuffer.Format.Elements) if (element.stream != 0) throw new ArgumentOutOfRangeException("geomtrybuffer require only streamid = 0");
            //foreach (VertexElement element in instancebuffer.Format.Elements) if (element.stream != 1) throw new ArgumentOutOfRangeException("instancebuffer require only streamid = 1");

        }


        public void Draw()
        {
            device.SetIndexStream(indexbuffer);

            device.SetVertexDeclaration(vertexdeclaration);

            device.SetVertexStream(geomtrybuffer, 0);
            device.device.SetStreamSourceFrequency(0, NumInstances, D3D.StreamSource.IndexedData);

            device.SetVertexStream(instancebuffer, 1);
            device.device.SetStreamSourceFrequency(1, 1, D3D.StreamSource.InstanceData);

            int passcount = shadercode.Begin();
            for (int pass = 0; pass < passcount; pass++)
            {
                shadercode.BeginPass(pass);
                device.DrawIndexedPrimitives(PrimitiveType, 0, 0, NumGeometryVertices, 0, NumGeometryPrimitives);
                shadercode.EndPass();
            }
            shadercode.End();

            device.device.ResetStreamSourceFrequency(0);
            device.device.ResetStreamSourceFrequency(1);
        }
    }
}
