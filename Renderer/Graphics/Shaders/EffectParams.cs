﻿//#define DEBUG_PERFORMANCE

using System;
using System.Collections.Generic;
using System.Collections;

using DX = SharpDX;
using D3D = SharpDX.Direct3D9;

using Engine.Maths;
using Engine.Tools;
 

namespace Engine
{
    /// <summary>
    /// Is a managed pointer of effect handle, is not related to effect
    /// </summary>
    public abstract class EffectParam : IDisposable
    {
        /// <summary>
        /// Debug number of set to effect process
        /// </summary>
        public static int SetValueCalls = 0;

        /// <summary>
        /// used to know the effect that need to updates the parameters
        /// </summary>
        internal Effect owner = null;
        /// <summary>
        /// The "string" pointer
        /// </summary>
        internal D3D.EffectHandle m_handle;
        /// <summary>
        /// name in hlsl code of this value
        /// </summary>
        protected string m_name;
        /// <summary>
        /// semantic in shader code, can be omitted
        /// </summary>
        protected string m_semantic;
        /// <summary>
        /// value is set to a constant buffer, but if change or device resetting need to set it
        /// EffectTechniques don't require to be update, effect already set it everytime
        /// </summary>
        protected bool m_needupdate;
        /// <summary>
        /// Initialize a new Effect Handle
        /// </summary>
        /// <param name="name">must match with hlsl code</param>
        /// <param name="semantic">can be null</param>
        protected EffectParam(string name, string semantic = null)
        {
            this.owner = null;
            this.m_name = name;
            this.m_semantic = semantic;
            this.m_needupdate = false;
            this.m_handle = name != null ? new D3D.EffectHandle(m_name) : null;
            IsDisposed = false;
        }
        /// <summary>
        /// is the parameter hlsl name, i make a check if are unique and if exist in hlsl code
        /// </summary>
        public string Name { get { return m_name; } }
        /// <summary>
        /// is the parameter hlsl semantic name
        /// </summary>
        public string Semantic { get { return m_semantic; } }
        
        /// <summary>
        /// </summary>
        public bool NeedUpdate { get { return m_needupdate; } }

        /// <summary>
        /// some check before setvalue
        /// </summary>
        protected void handleSet()
        {
            if (m_handle == null) throw new ArgumentNullException("handle can't be null");
            m_needupdate = false;
#if DEBUG_PERFORMANCE
            SetValueCalls++;
#endif
        }
        /// <summary>
        /// Pass value to effect
        /// </summary>
        public virtual void SetParam(Effect effect)
        {
            handleSet();
        }
        /// <summary>
        /// Get value to effect WORKIN PROGRESS
        /// </summary>
        public virtual void GetParam(Effect effect)
        {
        }

        /// <summary>
        /// </summary>
        /// <remarks> I notice dispose a EffectParam don't change handle... </remarks>
        public virtual void Restore()
        {
            //Console.WriteLine("Restore for " + m_shadername);
            m_handle = m_name != null ? new D3D.EffectHandle(m_name) : null;
            m_needupdate = true;
            IsDisposed = false;
        }

        /// <summary>
        /// </summary>
        public virtual void Dispose()
        {
            //Console.WriteLine("Dispose for " + m_shadername);
            if (m_handle != null && !m_handle.IsDisposed) m_handle.Dispose();
            IsDisposed = true;
        }

        /// <summary>
        /// two param are equal if their shader name are same, because refered to same handle 
        /// </summary>
        public override bool Equals(object obj)
        {
            return string.Compare((obj as EffectParam).m_name, this.m_name, false) == 0;
        }

        public override int GetHashCode()
        {
            return m_name.GetHashCode();
        }

        public override string ToString()
        {
            return string.Format("{0} ", m_name);
        }
        public bool IsDisposed { get; protected set; }

    }


    /// <summary>
    /// The special technique handle is managed separatly becuase can be set only one for each effect
    /// </summary>
    public class EffectTechnique : EffectParam, IEnumerable<Pass>
    {
        private PassEnumerator passenum;
        /// <summary>
        /// The index order get from effect method
        /// </summary>
        protected int m_index = -1;

        /// <summary>
        /// Notice that i don't want change parameter value after initialization,
        /// i want a one handle for one technique. Changing technique value mean reinitialize handle 
        /// </summary>
        public EffectTechnique(Effect owner, string technique)
            : base(technique, null)
        {
            base.owner = owner;
            // before effect Begin() i set everytime the only one techniques used, so is not necessary optimize it
            m_needupdate = false;
            owner.AddParameter(this);
            passenum = new PassEnumerator(this);
        }

        internal EffectTechnique(Effect owner, string technique, D3D.EffectHandle knowhandle, int knowindex)
            : base(technique, null)
        {
            m_handle = knowhandle;
            m_index = knowindex;
            m_needupdate = false;
            owner.AddParameter(this);
            passenum = new PassEnumerator(this);
        }

        /// <summary>
        /// For debug, is the index of technique when estract using effect method
        /// </summary>
        public int Index { get { return m_index; } }


        public override void SetParam(Effect effect)
        {
            handleSet();
            if (effect.isbegin) throw new Exception("Can't change technique after effect Begin()... need to call End()");
            // generate SharpDX.SharpDXException if not mach with existing techinques
            effect.effect.Technique = m_handle;
        }

        #region forech implementation
        public IEnumerator<Pass> GetEnumerator()
        {
            return passenum;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return (IEnumerator)GetEnumerator();
        }



        private class PassEnumerator : IEnumerator<Pass>
        {
            Effect effect;
            EffectTechnique technique;
            int count;
            Pass[] passes;
            int curr;

            internal PassEnumerator(EffectTechnique technique)
            {
                this.technique = technique;
                this.effect = technique.owner;
                D3D.Effect d3deffect = effect.effect;


                D3D.TechniqueDescription info = d3deffect.GetTechniqueDescription(technique.m_handle);
                count = info.Passes;

                passes = new Pass[count];
                for (int i = 0; i < count; i++)
                {
                    D3D.PassDescription passinfo = d3deffect.GetPassDescription(d3deffect.GetPass(technique.m_handle, i));
                    passes[i] = new Pass(i, passinfo.Name);
                }
                Reset();
            }

            public Pass Current
            {
                get { return passes[curr]; }
            }

            public void Dispose()
            {
                return;
            }

            object IEnumerator.Current
            {
                get { return Current; }
            }

            public bool MoveNext()
            {
                curr++;
                bool next = curr < count;

                // first pass begin
                if (curr == 0)
                {
                    //Console.WriteLine("BEGIN");
                    // techique are call before begin start
                    effect.technique = technique;
                    effect.technique.SetParam(effect);
                    effect.Update();

                    if (effect.isbegin)
                        throw new Exception("Begin already call");

                    effect.isbegin = true;
                    if (effect.effect.Begin(effect.SaveState ? D3D.FX.None : D3D.FX.DoNotSaveState | D3D.FX.DoNotSaveShaderState | D3D.FX.DoNotSaveSamplerState) != count)
                        throw new Exception("num pass don't match");
                }
                else
                {
                    //Console.WriteLine("ENDPASS " + (curr-1));
                    effect.isbeginpass = false;
                    effect.effect.EndPass();
                }

                if (next)
                {
                    //Console.WriteLine("BEGINPASS " + curr);
                    if (effect.isbeginpass)
                        throw new Exception("BeginPass already call");
                    effect.isbeginpass = true;
                    effect.Update();
                    effect.effect.BeginPass(curr);
                }
                else
                {
                    //Console.WriteLine("END");
                    effect.effect.End();
                    effect.isbegin = false;

                    Reset();
                }

                return next;
            }

            public void Reset()
            {
                //Console.WriteLine("RESET");
                curr = -1;
            }
        }
        #endregion
    }


    public class Pass
    {
        public int index = -1;
        public string name = "";

        public Pass(int index, string name)
        {
            this.index = index;
            this.name = name;
        }

        public override string ToString()
        {
            return string.Format("Pass{0} : {1}", index, name);
        }
    }




    #region Standard parameters
    /// <summary>
    /// Implementation for a generic struct of hlsl code, but i prefer use singular elements.
    /// </summary>
    public class EffectParamStruct<T> : EffectParam where T : struct
    {
        /// <summary>
        /// the copy of value
        /// </summary>
        protected T m_value;

        /// <summary>
        /// </summary>
        /// <param name="owner">require the effect to add into its disposable list</param>
        public EffectParamStruct(Effect owner, string name, T value = default(T), string semantic = null)
            : base(name, semantic)
        {
            base.owner = owner;

            if (string.IsNullOrEmpty(name)) throw new ArgumentNullException("parameter shader's name can't be omitted");
            m_value = value;
            owner.needUpdate = m_needupdate = true;
            owner.AddParameter(this);
        }

        /// <summary>
        /// When you set a new value i check if value is equal so avoid not necessary updates,
        /// owner.needUpdate is true when exist at last one parameters with m_needupdate = true
        /// </summary>
        public virtual T Value
        {
            get { return m_value; }
            set
            { 
                // ATTENTION, need to use OR because otherwise if you set two time same value the flag will be false
                m_needupdate |= !m_value.Equals(value); 
                m_value = value;
                owner.needUpdate |= m_needupdate;
            }
        }


        public override void SetParam(Effect effect)
        {
            handleSet();
            effect.effect.SetValue<T>(m_handle, m_value);
        }

        public override void GetParam(Effect effect)
        {
            m_value = effect.effect.GetValue<T>(m_handle);
        }

        /// <summary>
        /// When restoring tell effect that all parameters need to be re-write
        /// </summary>
        public override void Restore()
        {
            base.Restore();
            owner.needUpdate = true;
        }

        public override string ToString()
        {
            return string.Format("{0} {1} {2}", m_name, typeof(T), m_value.ToString());
        }
    }
    /// <summary>
    /// Specific for float (i use original sharpdx overloads)
    /// </summary>
    public class EffectParamFloat : EffectParamStruct<float>
    {
        public EffectParamFloat(Effect owner, string name)
            : base(owner, name, 1.0f, null)
        {

        }
        public EffectParamFloat(Effect owner, string name, float value = 0.0f, string semantic = null)
            : base(owner, name, value, semantic)
        {
        }

        public override void GetParam(Effect effect)
        {
            m_value = effect.effect.GetValue<float>(m_handle);
        }

        public override void SetParam(Effect effect)
        {
            handleSet();
            effect.effect.SetValue(m_handle, m_value);
        }
    }
    /// <summary>
    /// Specific for float2 (i use sharpdx generic struct)
    /// </summary>
    public class EffectParamVector2 : EffectParamStruct<Vector2>
    {

        public EffectParamVector2(Effect owner, string name)
            : base(owner, name, Vector2.Zero, null)
        {
        }
        public EffectParamVector2(Effect owner, string name, Vector2 value = default(Vector2), string semantic = null)
            : base(owner, name, value, semantic)
        {
        }
    }
    /// <summary>
    /// Specific for float3 (i use sharpdx generic struct)
    /// </summary>
    public class EffectParamVector3 : EffectParamStruct<Vector3>
    {
        public EffectParamVector3(Effect owner, string name)
            : base(owner, name, Vector3.Zero, null)
        {
        }

        public EffectParamVector3(Effect owner, string name, Vector3 value = default(Vector3), string semantic = null)
            : base(owner, name, value, semantic)
        {
        }
    }
    /// <summary>
    /// Specific for float4 (i use original sharpdx overloads)
    /// </summary>
    public class EffectParamVector4 : EffectParamStruct<Vector4>
    {
        public EffectParamVector4(Effect owner, string name)
            : base(owner, name, Vector3.Zero, null)
        {
        }

        public EffectParamVector4(Effect owner, string name, Vector3 value = default(Vector3), string semantic = null)
            : base(owner, name, value, semantic)
        {
        }

        public override void SetParam(Effect effect)
        {
            handleSet();
            effect.effect.SetValue(m_handle, Helper.Convert(m_value));
        }
        public override void GetParam(Effect effect)
        {
            m_value = Helper.Convert(effect.effect.GetValue<DX.Vector4>(m_handle));
        }
    }
    /// <summary>
    /// Specific for float4 (i use original sharpdx overloads), is the same of float4 ???
    /// </summary>
    public class EffectParamColor : EffectParamStruct<Color32>
    {
        // i use blue to avoid 0,0,0,0 and know if there are some issue when writing
        public EffectParamColor(Effect owner, string name)
            : base(owner, name, Color32.Blue, null)
        {
        }
        public EffectParamColor(Effect owner, string name, Color32 value = default(Color32), string semantic = null)
            : base(owner, name, value, semantic)
        {
        }

        public override void SetParam(Effect effect)
        {
            handleSet();
            effect.effect.SetValue(base.m_handle, Helper.Convert(m_value));
        }
        public override void GetParam(Effect effect)
        {
            m_value = Helper.Convert(effect.effect.GetValue<DX.Color4>(m_handle));
        }
    }
    /// <summary>
    /// Specific for matrix4x4 (i use original sharpdx overloads)
    /// </summary>
    public class EffectParamMatrix4 : EffectParamStruct<Matrix4>
    {
        public EffectParamMatrix4(Effect owner, string name, Matrix4 value = default(Matrix4), string semantic = null)
            : base(owner, name, value, semantic)
        {
            // by default directx use row major instead opengl (like my maths knowledge) columns major
        }

        public override void SetParam(Effect effect)
        {
            handleSet();
            effect.effect.SetValue(m_handle, Helper.Convert(m_value));
        }
        public override void GetParam(Effect effect)
        {
            m_value = Helper.Convert(effect.effect.GetValue<DX.Matrix>(m_handle));
        }
    }
    /// <summary>
    /// Specific for matrix3x3 (i use sharpdx generic struct)
    /// </summary>
    public class EffectParamMatrix3 : EffectParamStruct<Matrix3>
    {
        public EffectParamMatrix3(Effect owner, string name, Matrix3 value = default(Matrix3), string semantic = null)
            : base(owner, name, value, semantic)
        {
            // by default directx use row major instead opengl (like my maths knowledge) columns major
        }

        public override void SetParam(Effect effect)
        {
            handleSet();
            // remeber that i'm using color_mayor like OpenGl
            effect.effect.SetValue<Matrix3>(m_handle, Matrix3.Traspose(m_value));
        }
        public override void GetParam(Effect effect)
        {
            m_value = Matrix3.Traspose(effect.effect.GetValue<Matrix3>(m_handle));
        }
    }
    /// <summary>
    /// Is abstract because the class parameters are used only for some objects type
    /// </summary>
    public abstract class EffectParamClass<T> : EffectParam where T : class
    {
        /// <summary>
        /// the copy of value
        /// </summary>
        protected T m_value;

        /// <summary>
        /// </summary>
        public EffectParamClass(Effect owner, string name, T value = null, string semantic = null)
            : base(name, semantic)
        {
            if (string.IsNullOrEmpty(name)) throw new ArgumentNullException("parameter shader's name can't be omitted");
            m_value = value;
            this.owner = owner;
            owner.needUpdate = m_needupdate = true;
            owner.AddParameter(this);
        }

        /// <summary>
        /// When you set a new value. I can't check if value is equal becuase is a reference type, only if 
        /// previous and new are NULL
        /// </summary>
        public virtual T Value
        {
            get { return m_value; }
            set 
            { 
                m_needupdate |= !(m_value == null && value == null); 
                m_value = value;
                owner.needUpdate |= m_needupdate; 
            }
        }

        public override string ToString()
        {
            return string.Format("{0} {1} {2}", m_name, typeof(T), m_value.ToString());
        }
    }
    /// <summary>
    /// Unknow texture (i use original sharpdx overloads)
    /// </summary>
    public class EffectParamTexture : EffectParamClass<TextureBase>
    {
        public EffectParamTexture(Effect owner, string name, TextureBase value = null, string semantic = null)
            : base(owner, name, value, semantic)
        {
        }

        public override void SetParam(Effect effect)
        {
            handleSet();
            effect.effect.SetTexture(m_handle, m_value != null ? m_value.basetexture : null);
        }
    }
    /// <summary>
    /// Specific for texture2d (i use original sharpdx overloads)
    /// </summary>
    public class EffectParamTexture2D : EffectParamClass<Texture2D>
    {
        public EffectParamTexture2D(Effect owner, string name, Texture2D value = null, string semantic = null)
            : base(owner, name, value , semantic)
        {
        }

        public override void SetParam(Effect effect)
        {
            handleSet();
            effect.effect.SetTexture(m_handle, m_value != null ? m_value.basetexture : null);
        }
    }
    /// <summary>
    /// Specific for texture3d (i use original sharpdx overloads)
    /// </summary>
    public class EffectParamTexture3D : EffectParamClass<Texture3D>
    {
        public EffectParamTexture3D(Effect owner, string name, Texture3D value = null, string semantic = null)
            : base(owner, name, value, semantic)
        {
        }

        public override void SetParam(Effect effect)
        {
            handleSet();
            effect.effect.SetTexture(m_handle, m_value != null ? m_value.basetexture : null);
        }
    }
    /// <summary>
    /// Specific for textureCUBE (i use original sharpdx overloads)
    /// </summary>
    public class EffectParamTextureCUBE : EffectParamClass<TextureCUBE>
    {
        public EffectParamTextureCUBE(Effect owner, string name, TextureCUBE value = null, string semantic = null)
            : base(owner, name, value, semantic)
        {
        }

        public override void SetParam(Effect effect)
        {
            handleSet();
            effect.effect.SetTexture(m_handle, m_value != null ? m_value.basetexture : null);
        }
    }

    #endregion

    #region Special parameters (work in process ...)
    
    /// <summary>
    /// A collection of EffectParam to set all in one call
    /// </summary>
    public class EffectParamBlock : EffectParam
    {
        IList<object> m_value = null;


        /// <summary>
        /// A parameter build from a list of parameters, usefull when you are sure to set everytime
        /// all these parameters, otherwise is better set them individualy
        /// </summary>
        public EffectParamBlock(Effect owner)
            : base(null, null)
        {
            this.owner = owner;
        }


        public void AppendValue<T>(T value) where T : struct
        {
            m_value.Add(value);
        }

        public override void SetParam(Effect effect)
        {
            if (m_handle == null) throw new ArgumentNullException("handle can't be null");
            effect.effect.ApplyParameterBlock(m_handle);
        }

        /// <summary>
        /// Memorize this list of parameters, if you change something in the list you have to
        /// call a new GenerateRecord(). A reference of this list are stored to restore param
        /// after effect disposing, the change in list will be applied to new params block
        /// </summary>
        public override void Restore()
        {
            owner.effect.BeginParameterBlock();
            foreach (EffectParam par in m_value) par.SetParam(owner);
            m_handle = owner.effect.EndParameterBlock();
        }
        /// <summary>
        /// Free the memory used by recorded parameter block
        /// </summary>
        public override void Dispose()
        {
            owner.effect.DeleteParameterBlock(m_handle);
            if (!m_handle.IsDisposed) m_handle.Dispose();
        }
    }

    #endregion

}
