﻿
using System;
using System.Collections.Generic;
using System.Text;

 
using Engine.Content;
using Engine.Maths;
using Engine.Tools;

using DX = SharpDX;
using D3D = SharpDX.Direct3D9;


namespace Engine
{
    /// <summary>
    /// White a texture to another (set as first rendertarghet), used for "Deferred Shading"
    /// </summary>
    public class EffectRenderTexture : Effect
    {
        public EffectTechnique TechRenderTexture1;
        public EffectTechnique TechRenderTexture2;
        
        EffectParamTexture Texture0;
        EffectParamTexture Texture1; // not used

        D3D.Sprite sprite;

        public EffectRenderTexture(Device device)
            : base(device, EngineResources.RenderTextureHLSL)
        {
            TechRenderTexture1 = new EffectTechnique(this, "TechniqueRenderTexture");
            TechRenderTexture2 = new EffectTechnique(this, "TechniqueRenderTexture2");
            Texture0 = new EffectParamTexture(this, "Texture0");
            Texture1 = new EffectParamTexture(this, "Texture1");

            sprite = new D3D.Sprite(device.device);
        }


        public void DrawTextureToTarghet(QuadBuffer2D quadmesh , TextureBase texture0)
        {
            Texture0.Value = texture0;

            device.SetVertexDeclaration(quadmesh.declaration);
            device.SetVertexStream(quadmesh.vertexbuffer);

            foreach (Pass pass in TechRenderTexture1)
                quadmesh.DrawQuad(device);
        }

        public void DrawTextureToTarghetSprite(Texture2D texture0)
        {
            this.Technique = TechRenderTexture2;
           
            sprite.Begin();
            
            Begin();
            BeginPass(0);
            sprite.Draw(texture0.texture, DX.ColorBGRA.FromBgra(0));
            EndPass();
            End();

            sprite.End();
        }

    }
}
