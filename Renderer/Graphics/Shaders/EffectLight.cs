﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Drawing;
using System.Collections.Generic;
using System.Text;

using Engine.Maths;
using Engine.Tools;
using Engine.Content;

namespace Engine
{
    /// <summary>
    /// Basic effect, the intention is to replace fixed function pipeline with my custom shader version.
    /// Due for the fact that the technique is simple and commonly used, i add many techninque in one effect
    /// </summary>
    public class EffectLight : Effect
    {
        public EffectParamMatrix4 WorldViewProj, World, View, Proj, Camera;
        public EffectParamMatrix3 WorldInvTraspose;

        public readonly EffectParamVector3 Eye;
        /// <summary>
        /// Is the AmbientColor * AmbientIntensity premultiplied
        /// </summary>
        public readonly EffectParamVector4 Ambient;
        /// <summary>
        /// Is the LightColor * LightIntensity premultiplied
        /// </summary>
        public readonly EffectParamVector4 Light;
        /// <summary>
        /// Is the MaterialColor * LightIntensity premultiplied
        /// </summary>
        public readonly EffectParamVector4 Diffuse;

        public readonly EffectParamVector3 LightDirection;

        /// <summary>
        /// pixel color from vertex
        /// </summary>
        public readonly EffectTechnique TechVertexColor;
        /// <summary>
        /// pixel color from material diffuse
        /// </summary>
        public readonly EffectTechnique TechDiffuseColor;
        /// <summary>
        /// pixel color from vertex normal and light color
        /// </summary>
        public readonly EffectTechnique TechDiffuseLightingPerVertex;
        /// <summary>
        /// pixel color from pixel normal and light color
        /// </summary>
        public readonly EffectTechnique TechDiffuseLightingPerPixel;
        public readonly EffectTechnique TechSpecularLightingPerPixel;

        //EffectParamStruct<Material> mat = new EffectParamStruct<Material>("material", null, Material.Default);

        public EffectLight(Device device)
            : base(device, EngineResources.LightHLSL)
        {
            WorldViewProj = new EffectParamMatrix4(this, "WorldViewProj");
            World = new EffectParamMatrix4(this, "World");
            View = new EffectParamMatrix4(this, "View");
            Proj = new EffectParamMatrix4(this, "Proj");
            Camera = new EffectParamMatrix4(this, "Camera");
            Eye = new EffectParamVector3(this, "Eye");

            // http://www.arcsynthesis.org/gltut/Illumination/Tut09%20Normal%20Transformation.html
            WorldInvTraspose = new EffectParamMatrix3(this, "WorldInvTraspose");

            Diffuse = new EffectParamVector4(this, "Diffuse");
            Ambient = new EffectParamVector4(this, "Ambient");
            Light = new EffectParamVector4(this, "Light");
            LightDirection = new EffectParamVector3(this, "LightDirection");


            TechVertexColor = new EffectTechnique(this, "VertexColorTechnique");
            TechDiffuseColor = new EffectTechnique(this, "VertexDiffuseTechnique");
            TechDiffuseLightingPerVertex = new EffectTechnique(this, "DiffuseLightPerVertex");
            TechDiffuseLightingPerPixel = new EffectTechnique(this, "DiffuseLight");
            TechSpecularLightingPerPixel = new EffectTechnique(this, "SpecularLight");
            Technique = TechVertexColor;
        }


        public void ApplyCameraAndWorld(ICamera camera,Matrix4 world , Matrix4 worldInvTraspose)
        {
            WorldViewProj.Value = camera.Projection * camera.View * world;
            World.Value = world;
            View.Value = camera.View;
            Proj.Value = camera.Projection;
            Camera.Value = camera.CameraView;
            WorldInvTraspose.Value = worldInvTraspose;
        }
    }
}
