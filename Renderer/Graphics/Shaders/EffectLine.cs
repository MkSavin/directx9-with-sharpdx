﻿using System;
using System.Collections.Generic;
using System.Text;


namespace Engine
{
    /// <summary>
    /// Basic effect to draw lines
    /// </summary>
    public class EffectLine : Effect
    {   
        /// <summary>
        /// pixel color from vertex
        /// </summary>
        public readonly EffectTechnique TechVertexColor;
        public readonly EffectTechnique TechColor;

        public readonly EffectParamMatrix4 WorldViewProj;
        public readonly EffectParamColor Color;
        

        public EffectLine(Device device)
            : base(device, Content.EngineResources.LineHLSL)
        {
            this.WorldViewProj = new EffectParamMatrix4(this, "WorldViewProj");
            this.TechVertexColor = new EffectTechnique(this, "TechniqueVertexColor");
            this.TechColor = new EffectTechnique(this, "TechniqueColor");
            this.Color = new EffectParamColor(this, "VertexColor");
            base.Technique = TechVertexColor;
        }

    }
}
