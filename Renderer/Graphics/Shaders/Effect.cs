﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.IO;

using Engine.Maths;
using Engine.Tools;

using DX = SharpDX;
using D3D = SharpDX.Direct3D9;



namespace Engine
{

    /// <summary>
    /// C# version for include callback, WORKINPROGRESS
    /// </summary>
    /// <remarks>
    /// This interface is necessary for #include directive in hlsl code... don't know why
    /// </remarks>
    internal class MyIncludeCallBackImpl: D3D.Include
    {
        Stream tmp_file;
        string m_shaderdir;
        string m_shadername;
        IDisposable shadow;

        public MyIncludeCallBackImpl(string filename)
        {
            filename = Path.GetFullPath(filename);
            m_shaderdir = Path.GetDirectoryName(filename);
            m_shadername = Path.GetFileName(filename) ;
        }

        public void Close(Stream stream)
        {
            stream.Close();
        }

        public Stream Open(D3D.IncludeType type, string fileName, Stream parentStream)
        {
            string finalPath = "";

            switch (type)
            {
                case D3D.IncludeType.Local: // #include "FILE"
                    finalPath = Path.Combine(m_shaderdir, fileName);
                    break;
                case D3D.IncludeType.System: // #include <FILE>
                    finalPath = Path.Combine(m_shaderdir, fileName);
                    break;
            }
            finalPath = Path.GetFullPath(finalPath);
            tmp_file = new FileStream(finalPath, FileMode.Open);
            return tmp_file;
        }

        // don't know the use of this
        public IDisposable Shadow 
        {
            get 
            { 
                Debug.Write("Call Get of Shadow for");
                if (shadow != null)
                    Debug.WriteLine(shadow.ToString());
                else
                    Debug.WriteLine("NULL");

                return shadow;
            }
            set
            {
                Debug.WriteLine("Call Set Shadow for " + value.ToString());
                shadow = value;
            }
        }

        public void Dispose()
        {
            if (tmp_file != null) tmp_file.Dispose();
            tmp_file = null;
        }
    }


    /// <summary>
    /// The render shader programm
    /// </summary>
    public partial class Effect : DeviceResource
    {
       




#if DEBUG
        D3D.ShaderFlags debug = D3D.ShaderFlags.Debug;
#else 
        D3D.ShaderFlags debug = D3D.ShaderFlags.None; 
#endif

        internal EffectTechnique technique;
        private MyIncludeCallBackImpl includecallback;

        internal bool needUpdate = false;// tell you that some parameters need to be updated
        List<string> availabletechiques; // a list of all techniques name
        List<string> availableparams;    // a list of all parameters name
        List<string> semantics;          // a list of all parameter's semantics
        
        public readonly Dictionary<string, EffectParam> Parameters; 
        public readonly Dictionary<string, EffectTechnique> Techniques;
        
        string m_code;
        string m_filename;

        bool enabled;

        internal D3D.Effect effect;
        internal bool isbegin = false;
        internal bool isbeginpass = false;
        internal bool savestates = true;


        /// <summary>
        /// </summary>
        public Effect(Device device, string filename) : base(device,false)
        {

            string fullpath = Path.GetFullPath(filename);

            if (!File.Exists(fullpath)) throw new ArgumentNullException("Filename : \"" + fullpath + "\" doen't exist");

            m_filename = filename;
            m_code = Tools.UtilsMem.GetTextFile(fullpath);
            isbegin = false;
            isbeginpass = false;
            disposed = false;
            enabled = true;

            includecallback = new MyIncludeCallBackImpl(m_filename);

            initD3DEffect();

            Parameters = new Dictionary<string, EffectParam>();
            Techniques = new Dictionary<string, EffectTechnique>();
            availabletechiques = new List<string>();
            availableparams = new List<string>();
            semantics = new List<string>();

            //-------------------------
            //   Find all parameters
            //-------------------------
            D3D.EffectHandle currHandle = null;

            int count = effect.Description.Parameters;
            for (int i = 0; i < count; i++)
            {
                D3D.EffectHandle handle = effect.GetParameter(null, i);
                D3D.ParameterDescription descr = effect.GetParameterDescription(handle);
                availableparams.Add(descr.Name);
            }

            //-------------------------
            //   Find all techniques
            //-------------------------
            count = 0;
            currHandle = null;
            while ((currHandle = effect.FindNextValidTechnique(currHandle)) != null)
            {
                D3D.TechniqueDescription descr = effect.GetTechniqueDescription(currHandle);
                availabletechiques.Add(descr.Name);
                count++;
            }
            // set default technique
            effect.Technique = new D3D.EffectHandle(availabletechiques[0]);

        }


        /// <summary>
        /// Insert a parameter to internal list
        /// </summary>
        internal void AddParameter(EffectParam parameter)
        {
            if (parameter is EffectTechnique)
            {
                if (!availabletechiques.Contains(parameter.Name))
                    throw new ArgumentException("This technique not exist");
                
                if (Techniques.ContainsKey(parameter.Name))
                    throw new ArgumentException("This technique already added, please use only one instance for each techniques");
                Techniques.Add(parameter.Name, parameter as EffectTechnique);
            }
            else
            {
                if (!availableparams.Contains(parameter.Name))
                    throw new ArgumentException("This parameter name already added");
                availableparams.Add(parameter.Name);

                if (parameter.Semantic != null)
                {
                    if (semantics.Contains(parameter.Semantic))
                        throw new ArgumentException("This parameter semantic already added");
                    semantics.Add(parameter.Semantic);
                }
                Parameters.Add(parameter.Name, parameter);

                // get initialized parameter value from effect if exist
                parameter.GetParam(this);
            }
        }
        /// <summary>
        /// Unmanage it , only for optimization
        /// </summary>
        internal void RemoveParameter(EffectParam parameter)
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Get or Set the technique to use, must be added with AddTechnique().
        /// If you lost the reference, you have it back with GetTechniqueByName();
        /// </summary>
        public EffectTechnique Technique
        {
            get { return technique; }
            set { technique = value; }
        }

        /// <summary>
        /// Get previosly added technique of Create a new one
        /// </summary>
        public EffectTechnique GetTechniqueByName(string technique)
        {
            if (Techniques.ContainsKey(technique))
            {
                return Techniques[technique];
            }
            else
            {
                return new EffectTechnique(this, technique);
            }
        }

        /// <summary>
        /// If false apply flag D3D.FX.DoNotSaveState | D3D.FX.DoNotSaveShaderState | D3D.FX.DoNotSaveSamplerState to Begin()
        /// </summary>
        public bool SaveState
        {
            get { return savestates; }
            set { savestates = value; }
        }

        /// <summary>
        /// Auto-Update internal parameters, i suppose parameters collection are small, so not need optimization
        /// </summary>
        public int Begin()
        {
            if (!IsEnabled) throw new Exception("Can't use a effect not enabled");

            // techique are call before begin start
            technique.SetParam(this);
            Update();
            isbegin = true;
            return effect.Begin(savestates ? D3D.FX.None : D3D.FX.DoNotSaveState | D3D.FX.DoNotSaveShaderState | D3D.FX.DoNotSaveSamplerState);
        }

        public void End()
        {
            isbegin = isbeginpass = false;
            effect.End();
        }
        public void BeginPass(int i)
        {
            isbeginpass = true;
            Update();
            effect.BeginPass(i);
        }
        public void EndPass()
        {
            isbeginpass = false;
            effect.EndPass();
        }

        public void Commit()
        {
            effect.CommitChanges();
        }

        /// <summary>
        /// Update all parameters, but was already called begore Begin() and BeginPass()
        /// </summary>
        public void Update(bool forceall = false)
        {
            if (needUpdate)
            {
                bool found = false;
                foreach (EffectParam par in Parameters.Values)
                {
                    if (forceall || par.NeedUpdate) { par.SetParam(this); found = true; }
                }
                // if something change after BeginPass() requre a commit
                if (found && isbeginpass) Commit();
            }
            needUpdate = false;
        }

        /// <summary>
        /// Freeze the effect, this is used to avoid Restoring or parameters updated
        /// </summary>
        public override bool IsEnabled 
        {
            get { return enabled; }
            set 
            {
                if (!enabled && value)
                {
                    if (!disposed) DeviceLost();
                    DeviceRestore();
                }
                if (enabled && !value)
                {
                    DeviceLost();
                }
                enabled = value;
            }
        }

        /// <summary>
        /// Reinitialized the parameters handles
        /// </summary>
        public override void DeviceRestore()
        {
            initD3DEffect();
            disposed = false;

            foreach (EffectParam par in Parameters.Values) par.Restore();
            foreach (EffectParam par in Techniques.Values) par.Restore();
        }

        public override void DeviceLost()
        {
            Dispose();
        }


        public override void Dispose()
        {
            if (!disposed)
            {
                effect.Dispose();
                foreach (EffectParam par in Parameters.Values) par.Dispose();
                foreach (EffectParam par in Techniques.Values) par.Dispose();
                //D3D.EffectHandle.ClearCache();
            }
            disposed = true;
        }



        void initD3DEffect()
        {
            effect = D3D.Effect.FromFile(device.device, m_filename, null, includecallback, "", debug);
        }


        public override string ToString()
        {
            D3D.TechniqueDescription info =effect.GetTechniqueDescription(effect.Technique);
            return string.Format("{0} : {1} pass" , info.Name , info.Passes);

        }
    }
}
