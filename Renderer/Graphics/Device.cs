﻿//#define USE_OLD_FIXED_FUNCTION_PIPELINE


// by johnwhile
using System;
using System.Drawing;
using System.Diagnostics;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Text;

using DX = SharpDX;
using D3D = SharpDX.Direct3D9;

using Engine.Tools;
 
using Engine.Maths;

namespace Engine
{
    /// <summary>
    /// Is the GPU interface
    /// </summary>
    public class Device : IDisposable
    {
        bool disposed; // dispose for destroy process
        bool lost;     // lost for disposing with restoring
        DeviceSetting setting;
        internal D3D.Device device;
        
        internal ResourceManager resoucesmanager;

        // render targhet index used
        bool[] rendertarghetslot;
        
        /// <summary>
        /// Render states of device, need to minimize the changes
        /// </summary>
        public RenderStatesManager renderstates { get; private set; }
        /// <summary>
        /// contain all swapchain list where first is the main backbuffer generate by device initialization
        /// </summary>
        public List<RenderWindow> SwapChainList { get; private set; }

        /// <summary>
        /// A device is linked to a main window, is the default first swapchain that directx generate after 
        /// device initialization
        /// </summary>
        internal Device(RenderWindow mainwindow , DeviceSetting setting)
        {
            // device initialization
            D3D.Direct3D d3d = new D3D.Direct3D();

            this.setting = setting;
            FrameSetting framesetting = mainwindow.FrameSetting;

            device = new D3D.Device(d3d, setting.adapterIdx, setting.devicetype, framesetting.parameters.DeviceWindowHandle, setting.vertexprocessing, framesetting.parameters);

            Console.WriteLine("Init Device " + framesetting.ToString());
            
            
            //origbackbuffer = device.GetRenderTarget(0);
            //origbackbuffer = device.GetBackBuffer(0, 0);

            disposed = false;
            lost = false;

            renderstates = new RenderStatesManager(device);
            resoucesmanager = new ResourceManager();
            SwapChainList = new List<RenderWindow>();
            SwapChainList.Add(mainwindow);

            rendertarghetslot = new bool[DX9Enumerations.MaxRenderTarghetCount];
            rendertarghetslot[0] = true;
        }

        ~Device()
        {
            Dispose();
        }

        public void DrawIndexedPrimitives(PrimitiveType primitive, int baseVertex, int minVertex, int numVertices, int startIndex, int numPrimitives)
        {
            device.DrawIndexedPrimitive((D3D.PrimitiveType)primitive, baseVertex, minVertex, numVertices, startIndex, numPrimitives);
        }
        public void DrawPrimitives(PrimitiveType primitive, int startIndex, int numPrimitives)
        {
            device.DrawPrimitives((D3D.PrimitiveType)primitive, startIndex, numPrimitives);
        }
        public void DrawUserPrimitives<T>(PrimitiveType type, int numPrimitives, T[] array) where T : struct
        {
            device.DrawUserPrimitives<T>((D3D.PrimitiveType)type, 0, numPrimitives, array);
        }

        public void SetVertexStream(VertexBuffer vertexbuffer, int streamID = 0)
        {
            if (vertexbuffer != null && vertexbuffer.vbuffer != null)
            {
                if (vertexbuffer.IsDisposed) throw new Exception("buffer disposed, need to reinitialize it before use");
                device.SetStreamSource(streamID, vertexbuffer.vbuffer, 0, vertexbuffer.Format.bytesize);
            }
            else
            {
                device.SetStreamSource(streamID, null, 0, 0);
            }
        }
        
        public void SetIndexStream(IndexBuffer indexbuffer)
        {

            if (indexbuffer != null && indexbuffer.ibuffer != null)
            {
                if (indexbuffer.IsDisposed) throw new Exception("buffer disposed, need to reinitialize it before use");
                device.Indices = indexbuffer.ibuffer;
            }
            else
            {
                device.Indices = null;
            }
        }
        
        public void SetVertexDeclaration(VertexDeclaration declaration)
        {
            device.VertexDeclaration = declaration.declaration;
        }

        public void SetTextureTarghet(Texture2DTarghet texture, int index)
        {
            if (index > 4) throw new ArgumentOutOfRangeException("Please use maximum 4 render targhets");
            rendertarghetslot[index] = texture != null;

            if (texture!=null && texture.surface != null)
            {
                device.SetRenderTarget(index, texture.surface);
            }
            else
            {
                if (index == 0) throw new ArgumentOutOfRangeException("can set backbuffer slot to NULL");
                device.SetRenderTarget(index, null);
            }
        }

        /// <summary>
        /// Restore initial state
        /// </summary>
        public void ClearRenderTarghets()
        {
            if (SwapChainList[0].backbuffer == null || SwapChainList[0].backbuffer.IsDisposed)
                throw new ArgumentNullException("Where is the original device's backbuffer ?");
            
            // first render targhet is the place of original device's backbuffer create after device initialization
            device.SetRenderTarget(0, SwapChainList[0].backbuffer);
            
            for (int i = 1; i < 4; i++) device.SetRenderTarget(i, null);
        }

        public bool CanDraw()
        {
            return CheckDeviceState() == DeviceState.OK;
        }

        public DeviceState CheckDeviceState()
        {
            // don't understand if sharpdx use these values...
            const int DeviceLost = -2005530520;
            const int DeviceNotReset = -2005530519;

            DX.Result result = device.TestCooperativeLevel();

            switch (result.Code)
            {
                //Okay to render
                case 0: return DeviceState.OK;
                // Can't Reset yet, wait for a bit
                case DeviceLost: return DeviceState.DeviceLost;
                // Device tell you that need to be reseted
                case DeviceNotReset: return DeviceState.DeviceNotReset;
                // not defined case
                default: return DeviceState.InvalidCall;
            }

        }


        public bool IsDisposed
        {
            get { return disposed; }
        }


        /// <summary>
        /// Dispose device destory all resources, used example when closing application.
        /// To reuse it you have to re-initialize all resources, vertexbuffers, indexbuffer, textures, effects, etc...
        /// </summary>
        public void Dispose()
        {
            if (!disposed)
            {
                DeviceLost();
                device.Dispose();
            }
            lost = true;
            disposed = true;
        }

        public void DeviceLost()
        {

#if DEBUG
            Console.WriteLine("D3DDeviceLost");
            if (lost) Console.WriteLine("D3DDeviceLost ALREADY LOST");
#endif
            if (lost) return;

            // Need to dispose all unmanaged (Pool.Default) and all swapchains.

            // first dispose other resources and prevent restoring
            for (int i = 1; i < SwapChainList.Count; i++)
            {
                SwapChainList[i].DeviceLost();
                SwapChainList[i].IsEnabled = false;
            }
            resoucesmanager.DeviceLost();

            // then NOT dispose device, only surfaces becuase are passed as reference by sharpdx
            SwapChainList[0].DeviceLost();
            SwapChainList[0].IsEnabled = false;

            lost = true;
        }

        public void DeviceRestore()
        {
#if DEBUG
            Console.WriteLine("D3DDeviceRestore");
#endif
            if (disposed) throw new ExecutionEngineException("Is Disposed : need to reinitialize Device");
            if (!lost) throw new ExecutionEngineException("Isn't Lost: need to call DeviceLost() before restore");

            // device reset using main present parameter
            SwapChainList[0].IsEnabled = true;
            device.Reset(SwapChainList[0].FrameSetting.parameters);
            SwapChainList[0].DeviceRestore();

            // restore all other dependent swapchains
            for (int i = 1; i < SwapChainList.Count; i++)
            {
                SwapChainList[i].IsEnabled = true;
                SwapChainList[i].DeviceRestore();
            }
            resoucesmanager.DeviceRestore();
            disposed = false;
            lost = false;
        }





#if USE_OLD_FIXED_FUNCTION_PIPELINE
        public List<Light> Lights = new List<Light>();
        void setLightCollection()
        {
            for (int i = 0; i < Lights.Count; i++)
            {
                D3D.Light light = Helper.Convert(Lights[i]);
                device.SetLight(i, ref light);
                device.EnableLight(1, new DX.Bool(true));
            }
        }
        /// <summary>
        /// Set of Get the testure, can be null
        /// </summary>
        internal D3D.Texture Texture
        {
            set { device.SetTexture(0, value != null ? value : null); }
        }

        public void SetTexture(TextureBase texture, int stageID, bool setstates)
        {
            if (texture != null && texture.basetexture != null)
            {
                if (texture.IsDisposed) throw new Exception("texture disposed, need to reinitialize it before use");
                if (setstates) texture.TextureStates.SetStates(device, stageID);
                device.SetTexture(stageID, texture.basetexture);
            }
            else
            {
                device.SetTexture(stageID, null);
            }

        }
        /// <summary>
        /// When you mix Shader Effect with Fixed Function Pipeline, you need to clear internaly the device.
        /// </summary>
        public void ClearShaderCode()
        {
            device.VertexShader = null;
            device.PixelShader = null;
        }
#endif
    }


    /// <summary>
    /// Manage the know renderstates , transformstates, etc...
    /// </summary>
    public class RenderStatesManager
    {
        internal D3D.Device device;

        internal RenderStatesManager(D3D.Device device)
        {
            this.device = device;
        }

        public Material material
        {
            get { return Helper.Convert(device.Material); }
            set { device.Material = Helper.Convert(value); }
        }

        #region Transformsstates
        public Matrix4 world
        {
            get { return Helper.Convert(device.GetTransform(D3D.TransformState.World)); }
            set { device.SetTransform(D3D.TransformState.World, Helper.Convert(value)); }
        }
        public Matrix4 projection
        {
            get { return Helper.Convert(device.GetTransform(D3D.TransformState.Projection)); }
            set { device.SetTransform(D3D.TransformState.Projection, Helper.Convert(value)); }
        }
        public Matrix4 view
        {
            get { return Helper.Convert(device.GetTransform(D3D.TransformState.View)); }
            set { device.SetTransform(D3D.TransformState.View, Helper.Convert(value)); }
        }
        #endregion

        #region Rendersstates

        public ShadeMode ShadeMode
        {
            get { return (ShadeMode)device.GetRenderState(D3D.RenderState.ShadeMode);}
            set { device.SetRenderState(D3D.RenderState.ShadeMode, (D3D.ShadeMode)value); }
        }
        public int AlphaRef
        {
            get { return device.GetRenderState(D3D.RenderState.AlphaRef); }
            set { device.SetRenderState(D3D.RenderState.AlphaRef, value); }
        }
        public bool AlphaBlendEnable
        {
            get { return device.GetRenderState(D3D.RenderState.AlphaBlendEnable) > 0; }
            set { device.SetRenderState(D3D.RenderState.AlphaBlendEnable, (bool)value); }
        }
        public Compare AlphaFunction
        {
            get { return (Compare)device.GetRenderState(D3D.RenderState.AlphaFunc); }
            set { device.SetRenderState(D3D.RenderState.AlphaFunc, (D3D.Compare)value); }
        }
        public bool AlphaTestEnable
        {
            get { return device.GetRenderState(D3D.RenderState.AlphaTestEnable) > 0; }
            set { device.SetRenderState(D3D.RenderState.AlphaTestEnable, (bool)value); }
        }
        public BlendOperation BlendOperation
        {
            get { return (BlendOperation)device.GetRenderState(D3D.RenderState.BlendOperation); }
            set { device.SetRenderState(D3D.RenderState.BlendOperation, (D3D.BlendOperation)value); }
        }
        public Blend SourceBlend
        {
            get { return (Blend)device.GetRenderState(D3D.RenderState.SourceBlend); }
            set { device.SetRenderState(D3D.RenderState.SourceBlend, (D3D.Blend)value); }
        }
        public Blend DestinationBlend
        {
            get { return (Blend)device.GetRenderState(D3D.RenderState.DestinationBlend); }
            set { device.SetRenderState(D3D.RenderState.DestinationBlend, (D3D.Blend)value); }
        }

        public Cull cullMode
        {
            get { return (Cull)device.GetRenderState(D3D.RenderState.CullMode); }
            set { device.SetRenderState(D3D.RenderState.CullMode, (D3D.Cull)value); }
        }
        public float depth
        {
            get { return (float)device.GetRenderState(D3D.RenderState.DepthBias); }
            set { device.SetRenderState(D3D.RenderState.DepthBias, (float)value); }
        }
        public FillMode fillMode
        {
            get { return (FillMode)device.GetRenderState(D3D.RenderState.FillMode); }
            set { device.SetRenderState(D3D.RenderState.FillMode, (D3D.FillMode)value); }
        }
        public bool lightEnable
        {
            get { return device.GetRenderState(D3D.RenderState.Lighting) > 0; }
            set { device.SetRenderState(D3D.RenderState.Lighting, value); }
        }
        public bool NormalizeNormals
        {
            get { return device.GetRenderState(D3D.RenderState.NormalizeNormals) > 0; }
            set { device.SetRenderState(D3D.RenderState.NormalizeNormals, value); }
        }
        public bool StencilEnable
        {
            get { return device.GetRenderState(D3D.RenderState.StencilEnable) > 0; }
            set { device.SetRenderState(D3D.RenderState.StencilEnable, value); }
        }
        public bool ZBufferEnable
        {
            get { return device.GetRenderState(D3D.RenderState.ZEnable) > 0; }
            set { device.SetRenderState(D3D.RenderState.ZEnable, value); }
        }
        /// <summary>
        /// Default is LESSEQUAL
        /// </summary>
        public Compare ZBufferFunction
        {
            get { return (Compare)device.GetRenderState(D3D.RenderState.ZFunc); }
            set { device.SetRenderState(D3D.RenderState.ZFunc, (D3D.Compare)value); }
        }
        public bool ZBufferWriteEnable
        {
            get { return device.GetRenderState(D3D.RenderState.ZWriteEnable) > 0; }
            set { device.SetRenderState(D3D.RenderState.ZWriteEnable, value); }
        }
        #endregion
    }

}