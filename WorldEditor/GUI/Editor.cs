﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Engine;
using Engine.Graphics;
using Engine.Maths;
using Engine.Utils;
using Engine.Tools;

namespace WorldEditor
{
    public partial class MyWorldEditor : Form
    {
        SceneManager scene;
        GameLoop loop;
        InputManager input;

        public MyWorldEditor()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            input = new InputManager(this);

            scene = new SceneManager(myRenderPanel.render.Device);

            loop = new GameLoop(myRenderPanel.Render, 0);
            loop.StartLooping();
        }


        private void openWorldMenuItem_Click(object sender, EventArgs e)
        {
            openFileDialog.InitialDirectory = Application.StartupPath;
            if (openFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                Console.WriteLine("open " + openFileDialog.FileName);
            }
        }
    }
}
