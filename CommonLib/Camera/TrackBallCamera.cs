﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using Engine.Maths;
using Engine.Tools;

namespace Engine
{
    /// <summary>
    /// Common math for all trackball implementations, need to link the mouse events
    /// </summary>
    public abstract class TrackBallCameraMath : Camera
    {
        protected enum Mousing
        {
            // mouse was not pressed : view or proj matrix is fixed -> avoid constanly update
            None,
            // left mouse pressed
            Traslation,
            // right mouse pressed
            Rotating,
            // wheel mouse rotating
            Zooming,
            // middle (wheel) mouse pressed
            Turning
        }
        protected Mousing mousing;

        Viewport viewport;
       
        float zoom = 1.0f;
        float radius = 1.0f;
        Vector2I start, end;

        // is use accumulation matrix, cameraStart is the initial cameraview value,
        // after mouse relase the accumulations will be applied to camera view. 
        Matrix4 rotation;
        Matrix4 traslation;
        Matrix4 cameraStart;

        public TrackBallCameraMath(Viewport viewport, Matrix4 view, Matrix4 proj) : base(proj,view)
        {
            this.zoom = 1.0f;
            cameraStart = cameraview;
            this.viewport = viewport;
            this.mousing = Mousing.None;
            this.rotation = Matrix4.Identity;
            this.traslation = Matrix4.Identity;
            start = new Vector2I(0, 0);
            end = new Vector2I(0, 0);
        }

        /// <summary>
        /// </summary>
        /// <param name="control">The ball require information about size of panel, at this moment viewport match with clientrectangle</param>
        public TrackBallCameraMath(Viewport viewport, Vector3 Eye, Vector3 Targhet, float near, float far)
            : base( Matrix4.MakeProjectionAFovYLH(near, far, viewport.AspectRatio),Matrix4.MakeViewLH(Eye, Targhet, Vector3.UnitY))
        {
            this.zoom = Vector3.Distance(Eye, Targhet);
            cameraStart = cameraview;
        }


        protected abstract void GetMouseCoord(out int X, out int Y);
        

        protected void ViewportResize(Viewport newviewport)
        {
            viewport = newviewport;
            AspectRatio = viewport.AspectRatio;
        }
        
        protected void MouseRotateDown()
        {
            mousing = Mousing.Rotating;
            cameraStart = cameraview;
            rotation = Matrix4.Identity;
            GetMouseCoord(out start.x, out start.y);
        }
        
        protected void MouseTraslateDown()
        {
            mousing = Mousing.Traslation;
            GetMouseCoord(out start.x, out start.y);
        }
        
        protected void MouseUp()
        {
            mousing = Mousing.None;
            start = end;
           
            CameraView = cameraStart * rotation;
            rotation = Matrix4.Identity;
            
        }
        
        protected void MouseWheeling(int Z)
        {
            Console.WriteLine("TrackBall Zooming");

            mousing = Mousing.Zooming;

            if (Z > 0)
                zoom *= 0.9f;
            else
                zoom *= 1.1f;

            //camera.eye = new Vector3(0, 0, -zoom);
            //camera.UpdateView();

            mousing = Mousing.None;
        }
        
        protected void MouseMoving()
        {
            GetMouseCoord(out end.x, out end.y);

            if (mousing != Mousing.None)
            {
                int dx = end.x - start.x;
                int dy = start.y - end.y;

                if (mousing == Mousing.Traslation)
                {

                    if (dx != 0 || dy != 0)
                    {
                        /*
                        Console.WriteLine(string.Format("TrackBall Traslation {0} {1}", dx, dy));
                        

                        Matrix4 view = Camera.View;
                        Vector3 vRight = new Vector3(view.m00, view.m01, view.m02);
                        Vector3 vUp = new Vector3(view.m10, view.m11, view.m12);
                        Vector3 move = (dx * vRight + dy * vUp) * (0.001f * zoom);

                        Matrix4 tras = Matrix4.Translating(ref move);
                        traslation = tras * traslation;

                        //cameraview.Position -= move;
                        //view = cameraview.Inverse();
                       */
                    }
                }
                else if (mousing == Mousing.Rotating)
                {
                    Console.WriteLine("TrackBall Rotating " + dx.ToString() + " " + dy.ToString());

                    Vector3 p0 = ProjectOnSphere(start.x, start.y);
                    Vector3 p1 = ProjectOnSphere(end.x, end.y);
                    Vector3 dir = p0 - p1;

                    if (dir.LengthSq > 0)
                    {
                        rotation = RotationFromVectors(p0, p1);
                        CameraView = cameraStart * rotation;
                    }
                }
            }
        }

        protected float GetMaxArcBallRadius()
        {
            return (int)(MathUtils.MIN(viewport.Width, viewport.Height) / 2 * radius);
        }

        /// <summary>
        /// Square viewport
        /// adjust to center screen in [-1,1] range and flip Y
        /// </summary>
        protected void ToCenter(ref float x, ref float y)
        {
            x = 2.0f * x / viewport.Width - 1.0f;
            y = 1.0f - 2.0f * y / viewport.Height;
        }

        /// <summary>
        /// Convert a screen point to the screen virtual hemisphere, the result is always a normalized vector
        /// </summary>
        protected Vector3 ProjectOnSphere(int X, int Y)
        {
            Vector3 p = new Vector3(X, Y, 0);

            // adjust to [-1,1] range with y inverted
            ToCenter(ref p.x, ref p.y);

            // distance from radius, z=0
            float distSq = p.x * p.x + p.y * p.y;
            float radiusSq = radius * radius;

            //project it to sphere surface
            if (distSq < radiusSq)
            {
                // the point is inside ball, (on hemisphere surface) then z(x,y) : z^2 = r^2 - x^2 - y^2
                p.z = (float)Math.Sqrt(radiusSq - distSq);
            }
            else
            {
                // the point is outside ball (on XY plane), is sufficent scale the vector
                p.z = 0;
                float mag = (float)Math.Sqrt(radiusSq);
                p.x /= mag;
                p.y /= mag;
            }

            p.Normalize();
            return p;
        }


        protected Matrix4 RotationFromVectors(Vector3 p0, Vector3 p1)
        {
            float dot = Vector3.Dot(p0, p1);
            float angle = (float)Math.Acos(MathUtils.MIN(1, dot)) * 0.5f;

            Vector3 axe = Vector3.Cross(p1, p0);
            axe.Normalize();

            Console.WriteLine(string.Format("p0 {0}\np1 {1}", p0.ToStringN(), p1.ToStringN()));
            Console.WriteLine("Ax " + axe.ToStringN());
            Console.WriteLine("ang " + angle);

            return Matrix4.RotateAxis(ref axe, angle);

        }

        public void Paint(System.Drawing.Graphics g)
        {
            int arcradius = (int)GetMaxArcBallRadius();
            int centerx = viewport.Width / 2;
            int centery = viewport.Height / 2;
            g.DrawEllipse(System.Drawing.Pens.Yellow, centerx - arcradius, centery - arcradius, arcradius * 2, arcradius * 2);
        }

    }

}
