﻿
using System;
using System.Collections.Generic;
using System.Text;

using Engine.Maths;

namespace Engine.Tools
{
    /// <summary>
    /// interface to access to main camera's values, Viewport is idipendet from camera value
    /// in fact in the view matrix there are only aspect ratio.
    /// </summary>
    public interface ICamera
    {
        Vector3 Eye { get; }
        Matrix4 View { get; }
        Matrix4 CameraView { get; }
        Matrix4 Projection { get; }

        float Far { get; }
        float Near { get; }
        float AspectRatio { get; }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <remarks>
    /// Remember that if Camera = View^-1, when apply a camera transformation
    /// V' = (C*T)^-1 = T^-1 * C^-1 = T^-1 * V
    /// T^-1 can be semplified if contain only rotation, only traslation, only scale
    /// 
    /// </remarks>
    public class Camera : ICamera
    {
        protected Frustum frustum;
        protected Matrix4 cameraview;  // camera transform (inverse of view)
        protected Matrix4 view;        // inverse of camera
        protected Matrix4 projection;  // camera projection
        protected bool frustumNeedUpdate = false;


        public Camera()
        {
            projection = view = cameraview = Matrix4.Identity;
            this.frustum = new Frustum(projection * view);
            frustumNeedUpdate = true;
        }

        public Camera(Matrix4 projection, Matrix4 view)
        {
            this.projection = projection;
            this.view = view;
            this.cameraview = view.Inverse();
            this.frustum = new Frustum(projection * view);
            frustumNeedUpdate = false;
        }

        public Frustum Frustum
        {
            get
            {
                if (frustumNeedUpdate)
                {
                    Matrix4 projview = projection;
                    projview.Multiply(ref view);
                    frustum.MakeFrustum(ref projview);
                }
                return frustum;
            }
        }

        #region View

        public Vector3 Eye
        {
            get { return CameraView.Position; }
        }

        public virtual Matrix4 View
        {
            get { return view; }
            set { view = value; cameraview = view.Inverse(); frustumNeedUpdate = true; }
        }

        public virtual Matrix4 CameraView
        {
            get { return cameraview; }
            set { cameraview = value; view = cameraview.Inverse(); frustumNeedUpdate = true; }
        }


        /// <summary>
        /// The UnitX vector if view matrix is Identity
        /// </summary>
        public Vector3 Left
        {
            get { return new Vector3(view.m00, view.m01, view.m02); }
        }
        /// <summary>
        /// The UnitY vector if view matrix is Identity
        /// </summary>
        public Vector3 Up
        {
            get { return new Vector3(view.m10, view.m11, view.m12); }
        }
        /// <summary>
        /// The UnitZ vector if view matrix is Identity
        /// </summary>
        public Vector3 Forward
        {
            get { return new Vector3(view.m20, view.m21, view.m22); }
        }

        #endregion

        #region Projection

        public Matrix4 Projection
        {
            get { return projection; }
            set { projection = value; frustumNeedUpdate = true; }
        }
        /// <summary>
        /// ORTHOGONAL version : Update the camera Width where Width is in world coordinate
        /// </summary>
        public float OrthoWidth
        {
            get { return ProjectionTool.GetWidth(ref projection); }
            set { ProjectionTool.SetWidth(ref projection, value); }
        }
        /// <summary>
        /// ORTHOGONAL version : Update the camera Height where Height is in world coordinate
        /// </summary>
        public float OrthoHeight
        {
            get { return ProjectionTool.GetHeight(ref projection); }
            set { ProjectionTool.SetHeight(ref projection, value); }
        }

        /// <summary>
        /// PROJECTION version : Update projection with minimum calculations, FovX will be derived from aspectratio
        /// </summary>
        public float AspectRatio
        {
            get { return ProjectionTool.GetAspectRatio(ref projection); }
            set { ProjectionTool.SetAspectRatio(ref projection, value); }
        }

        /// <summary>
        /// PROJECTION version : Update projection with minimum calculations, FovX will be derived from aspectratio
        /// </summary>
        public float FovY
        {
            get { return ProjectionTool.GetFovY(ref projection); }
            set { ProjectionTool.SetFovY(ref projection, value); }
        }

        /// <summary>
        /// Update projection with minimum calculations
        /// </summary>
        public float Far
        {
            get { return ProjectionTool.GetFar(ref projection); }
            set { ProjectionTool.SetFar(ref projection, value); }
        }

        /// <summary>
        /// Update projection with minimum calculations
        /// </summary>
        public float Near
        {
            get { return ProjectionTool.GetNear(ref projection); }
            set { ProjectionTool.SetNear(ref projection, value); }
        }

        #endregion
    }

}
