﻿/*
Axiom Graphics Engine Library
Copyright � 2003-2011 Axiom Project Team

The overall design, and a majority of the core engine and rendering code 
contained within this library is a derivative of the open source Object Oriented 
Graphics Engine OGRE, which can be found at http://ogre.sourceforge.net.  
Many thanks to the OGRE team for maintaining such a high quality project.

The math library included in this project, in addition to being a derivative of
the works of Ogre, also include derivative work of the free portion of the 
Wild Magic mathematics source code that is distributed with the excellent
book Game Engine Design.
http://www.wild-magic.com/

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

using System;
using System.Text;
using System.Runtime.InteropServices;

namespace Engine.Maths
{
    /// <summary>
    /// A 3x3 Matrix, is a transform version for 2d case
    /// </summary>
    /// <remarks>
    /// Remember the correct order of transformations L = T * R * S mean
    /// first scale, second rotation and last traslate.
    /// </remarks>
    [StructLayout(LayoutKind.Explicit)]
    public struct Matrix3
    {
        [FieldOffset(0)]
        public float m00;
        [FieldOffset(4)]
        public float m01;
        [FieldOffset(8)]
        public float m02;
        [FieldOffset(12)]
        public float m10;
        [FieldOffset(16)]
        public float m11;
        [FieldOffset(20)]
        public float m12;
        [FieldOffset(24)]
        public float m20;
        [FieldOffset(28)]
        public float m21;
        [FieldOffset(32)]
        public float m22;

        [FieldOffset(0)]
        public unsafe fixed float Dim[9];


        public static readonly Matrix3 Identity = new Matrix3(1.0f, 0, 0, 0, 1.0f, 0, 0, 0, 1.0f);
        public static readonly Matrix3 Zero = new Matrix3(0.0f, 0, 0, 0, 0, 0, 0, 0, 0);


        public Matrix3(double m00, double m01, double m02, double m10, double m11, double m12, double m20, double m21, double m22) :
            this((float)m00, (float)m01, (float)m02, (float)m10, (float)m11, (float)m12, (float)m20, (float)m21, (float)m22) { }

        public Matrix3(Vector3 right, Vector3 up, Vector3 forward)
            : this(right.x, up.x, forward.x,
                   right.y, up.y, forward.y,
                   right.z, up.z, forward.z) { }

        public Matrix3(float m00, float m01, float m02, float m10, float m11, float m12, float m20, float m21, float m22)
        {
            this.m00 = m00;
            this.m01 = m01;
            this.m02 = m02;
            this.m10 = m10;
            this.m11 = m11;
            this.m12 = m12;
            this.m20 = m20;
            this.m21 = m21;
            this.m22 = m22;
        }


        /// <summary>
        /// The most used transformation matrix create with order : Scale -> Rotation -> Traslation affine transformation.
        /// TRS name is used to remember you that this matrix are calulation with T * R * S
        /// 
        /// <para>|Cos*Sx  Sin*Sy Tx|</para>
        /// <para>|-Sin*Sx Cos*Sy Ty|</para>
        /// <para>|    0      0    1|</para>
        /// </summary>
        public static Matrix3 TRS(float CenterX, float CenterY, float SizeX, float SizeY, float Radians)
        {
            return new Matrix3(
                (float)Math.Cos(Radians) * SizeX, (float)Math.Sin(Radians) * SizeY, CenterX,
                -(float)Math.Sin(Radians) * SizeX, (float)Math.Cos(Radians) * SizeY, CenterY,
                0, 0, 1);

            // equivalent of :
            // return Matrix3.Translating(CenterX, CenterY) * Matrix3.Rotation(Radians) * Matrix3.Scaling(SizeX, SizeY);

        }

        /// <summary>
        /// <para>|Cos*Sx  Sin*Sy Tx|</para>
        /// <para>|-Sin*Sx Cos*Sy Ty|</para>
        /// <para>|    0      0    1|</para>
        /// </summary>
        public static void DecomposeTRS(Matrix3 matrix, out Vector2 traslation, out Vector2 scaling, out float angle)
        {
            traslation = new Vector2(matrix.m02, matrix.m12);

            // scale component are optain from lenght of colum vector
            // |Cos*Sx  Sin*Sy Tx|
            // |-Sin*Sx Cos*Sy Ty|
            // |    0      0    1|
            // S.x = Sqrt{(Cos*Sx)^2 + (-Sin*Sx)^2} = Sqrt{ (Cos^2 + Sin^2) * Sx^2} = Sqrt(Sx^2) = Sx

            scaling = new Vector2(
                Math.Sqrt(matrix.m00 * matrix.m00 + matrix.m10 * matrix.m10),
                Math.Sqrt(matrix.m01 * matrix.m01 + matrix.m11 * matrix.m11));

            angle = (float)Math.Acos(matrix.m00 / scaling.x);

        }

        public static readonly int sizeinbyte = sizeof(float) * 9;

        /// <summary>
        /// Indexer for accessing the matrix like a 2d array (i.e. matrix[2,3]).
        /// </summary>
        public float this[int row, int col]
        {
            get
            {
                //Debug.Assert((row >= 0 && row < 3) && (col >= 0 && col < 3), "Attempt to access Matrix3 indexer out of bounds.");

#if !UNSAFE
                switch (row)
                {
                    case 0:
                        switch (col)
                        {
                            case 0: return m00;
                            case 1: return m01;
                            case 2: return m02;
                        }
                        break;
                    case 1:
                        switch (col)
                        {
                            case 0: return m10;
                            case 1: return m11;
                            case 2: return m12;
                        }
                        break;
                    case 2:
                        switch (col)
                        {
                            case 0: return m20;
                            case 1: return m21;
                            case 2: return m22;
                        }
                        break;
                }
                throw new IndexOutOfRangeException("Attempt to access Matrix3 indexer out of bounds.");
#else
                unsafe
                {
                    fixed (float* pM = &this.m00)
                    {
                        return *(pM + ((3 * row) + col));
                    }
                }
#endif
            }
            set
            {
                //Debug.Assert((row >= 0 && row < 3) && (col >= 0 && col < 3), "Attempt to access Matrix3 indexer out of bounds.");

#if !UNSAFE
                switch (row)
                {
                    case 0:
                        switch (col)
                        {
                            case 0: m00 = value; break;
                            case 1: m01 = value; break;
                            case 2: m02 = value; break;
                        }
                        break;
                    case 1:
                        switch (col)
                        {
                            case 0: m10 = value; break;
                            case 1: m11 = value; break;
                            case 2: m12 = value; break;
                        }
                        break;
                    case 2:
                        switch (col)
                        {
                            case 0: m20 = value; break;
                            case 1: m21 = value; break;
                            case 2: m22 = value; break;
                        }
                        break;
                }
#else
                unsafe
                {
                    fixed (float* pM = &this.m00)
                    {
                        *(pM + ((3 * row) + col)) = value;
                    }
                }
#endif
            }
        }

        /// <summary>
        ///		Allows the Matrix to be accessed linearly (m[0] -> m[8]).  
        /// </summary>
        public float this[int index]
        {
            get
            {
                //Debug.Assert(index >= 0 && index <= 8, "Attempt to access Matrix3 linear indexer out of bounds.");

#if !UNSAFE
                switch (index)
                {
                    case 0: return m00;
                    case 1: return m01;
                    case 2: return m02;
                    case 3: return m10;
                    case 4: return m11;
                    case 5: return m12;
                    case 6: return m20;
                    case 7: return m21;
                    case 8: return m22;
                }
                throw new IndexOutOfRangeException("Attempt to access Matrix3 indexer out of bounds.");
#else
                unsafe
                {
                    fixed (float* pMatrix = &this.m00)
                    {
                        return *(pMatrix + index);
                    }
                }
#endif
            }
            set
            {
                //Debug.Assert(index >= 0 && index <= 8, "Attempt to access Matrix3 linear indexer out of bounds.");

#if !UNSAFE
                switch (index)
                {
                    case 0: m00 = value; break;
                    case 1: m01 = value; break;
                    case 2: m02 = value; break;
                    case 3: m10 = value; break;
                    case 4: m11 = value; break;
                    case 5: m12 = value; break;
                    case 6: m20 = value; break;
                    case 7: m21 = value; break;
                    case 8: m22 = value; break;
                }
#else
                unsafe
                {
                    fixed (float* pMatrix = &this.m00)
                    {
                        *(pMatrix + index) = value;
                    }
                }
#endif
            }
        }


        public void FromEulerAnglesXYZ(float yaw, float pitch, float roll)
        {
            float cos = (float)Math.Cos(yaw);
            float sin = (float)Math.Sin(yaw);
            Matrix3 xMat = new Matrix3(1, 0, 0, 0, cos, -sin, 0, sin, cos);

            cos = (float)Math.Cos(pitch);
            sin = (float)Math.Sin(pitch);
            Matrix3 yMat = new Matrix3(cos, 0, sin, 0, 1, 0, -sin, 0, cos);

            cos = (float)Math.Cos(roll);
            sin = (float)Math.Sin(roll);
            Matrix3 zMat = new Matrix3(cos, -sin, 0, sin, cos, 0, 0, 0, 1);

            this = xMat * (yMat * zMat);
        }

        public Vector3 ToEulerAnglesXYZ()
        {
            double yAngle;
            double rAngle;
            double pAngle;

            pAngle = Math.Asin(this.m01);
            if (pAngle < Math.PI / 2)
            {
                if (pAngle > -Math.PI / 2)
                {
                    yAngle = Math.Atan2(this.m21, this.m11);
                    rAngle = Math.Atan2(this.m02, this.m00);
                }
                else
                {
                    // WARNING. Not a unique solution.
                    var fRmY = (float)Math.Atan2(-this.m20, this.m22);
                    rAngle = 0.0f; // any angle works
                    yAngle = rAngle - fRmY;
                }
            }
            else
            {
                // WARNING. Not a unique solution.
                var fRpY = Math.Atan2(-this.m20, this.m22);
                rAngle = 0.0f; // any angle works
                yAngle = fRpY - rAngle;
            }

            return new Vector3((float)yAngle, (float)rAngle, (float)pAngle);
        }
        /// <summary>
        ///	 Get or Set the translation value of the matrix using math notation
        ///		| 1 0 Tx |
        ///		| 0 1 Ty |
        ///		| 0 0  1 |
        /// </summary>
        public Vector2 Position
        {
            get { return new Vector2(this.m02, this.m12); }
            set { this.m02 = value.x; this.m12 = value.y; }
        }

        /// <summary>
        /// Get the determinant of matrix.
        /// </summary>
        public float Determinant
        {
            get
            {
                float result = 0.0f;
                result += m00 * (m11 * m22 - m12 * m21);
                result -= m01 * (m10 * m22 - m12 * m20);
                result += m02 * (m10 * m21 - m11 * m20);
                return result;
            }
        }
        /// <summary>
        /// Internal multiplication (avoid new())
        /// </summary>
        public void Multiply(float scalar)
        {
            m00 *= scalar; m01 *= scalar; m02 *= scalar;
            m10 *= scalar; m11 *= scalar; m12 *= scalar;
            m20 *= scalar; m21 *= scalar; m22 *= scalar;
        }

        /// <summary>
        ///  Returns an inverted matrix, if not exit return Exception
        /// </summary>
        public Matrix3 Inverse()
        {
            // cofactors matrix
            float c00 = m11 * m22 - m12 * m21;
            float c01 = -m10 * m22 + m12 * m20;
            float c02 = m10 * m21 - m11 * m20;
            float c10 = -m01 * m22 + m02 * m21;
            float c11 = m00 * m22 - m02 * m20;
            float c12 = -m00 * m21 + m01 * m20;
            float c20 = m01 * m12 - m02 * m11;
            float c21 = -m00 * m12 + m02 * m10;
            float c22 = m00 * m11 - m01 * m10;

            // determinant
            float det = m00 * c00 + m01 * c01 + m02 * c02;

            if (det > -float.Epsilon && det < float.Epsilon) throw new Exception("matrix can't be inverted");

            // trasposed adjoint matrix
            Matrix3 Adjoint = new Matrix3(c00, c10, c20, c01, c11, c21, c02, c12, c22);

            // inverse = (1/det)(Adjoint^T)
            Adjoint.Multiply(1 / det);

            return Adjoint;
        }

        /// <summary>
        /// Get a 2D rotation matrix
        /// </summary>
        /// <param name="radians">if XY are the plane, the only rotation available is Z</param>
        /// <returns></returns>
        public static Matrix3 Rotation(float radians)
        {
            Matrix3 matrix = Matrix3.Identity;
            matrix.m00 = (float)Math.Cos(radians);
            matrix.m01 = (float)Math.Sin(radians);
            matrix.m10 = -(float)Math.Sin(radians);
            matrix.m11 = (float)Math.Cos(radians);
            return matrix;
        }
        public static Matrix3 Scaling(Vector3 scale)
        {
            return Matrix3.Scaling(scale.x, scale.y);
        }
        public static Matrix3 Scaling(float x, float y)
        {
            Matrix3 matrix = Matrix3.Identity;
            matrix.m00 = x;
            matrix.m11 = y;
            return matrix;
        }
        public static Matrix3 Translating(Vector2 vector)
        {
            return Matrix3.Translating(vector.x, vector.y);
        }
        public static Matrix3 Translating(float x, float y)
        {
            Matrix3 matrix = Matrix3.Identity;
            matrix.m02 = x;
            matrix.m12 = y;
            return matrix;
        }
        /// <summary>
        /// Traslation component is indipendent and can work also for TRS transformations
        /// </summary>
        public void Translate(Vector2 vector)
        {
            TranslateX(vector.x);
            TranslateY(vector.y);
        }
        /// <summary>
        /// </summary>
        public void TranslateX(float move)
        {
            this.m02 += move;
        }
        public void TranslateY(float move)
        {
            this.m12 += move;
        }

        public void Traspose()
        {
            // diagonals are the same
            MathUtils.SWAP(ref m01, ref m10);
            MathUtils.SWAP(ref m02, ref m20);
            MathUtils.SWAP(ref m12, ref m21);
        }

        public static Matrix3 Traspose(Matrix3 matrix)
        {
            Matrix3 m = new Matrix3();
            m.m00 = matrix.m00;
            m.m01 = matrix.m10;
            m.m02 = matrix.m20;

            m.m10 = matrix.m01;
            m.m11 = matrix.m11;
            m.m12 = matrix.m21;

            m.m20 = matrix.m02;
            m.m21 = matrix.m12;
            m.m22 = matrix.m22;

            return m;
        }



        void mul2x2(Matrix3 r)
        {


        }

        /// <summary>
        /// Rotate the rotation component of TRS transformations by radians value (mean rotation multiplication)
        /// </summary>
        public void RotateTRSmul(float radians)
        {
            // decomposition
            float tx = this.m02;
            float ty = this.m12;

            float sx = (float)Math.Sqrt(this.m00 * this.m00 + this.m10 * this.m10);
            float sy = (float)Math.Sqrt(this.m01 * this.m01 + this.m11 * this.m11);

            this.m02 = 0;
            this.m12 = 0;
            this.m00 /= sx;
            this.m10 /= sx;
            this.m01 /= sy;
            this.m11 /= sy;

            // recomposition
            this *= Matrix3.Rotation(radians);
            this.m00 *= sx;
            this.m10 *= sx;
            this.m01 *= sy;
            this.m11 *= sy;
            this.m02 = tx;
            this.m12 = ty;
        }
        /// <summary>
        /// Substitute the rotation component of TRS transformations by new radians value,
        /// require less computation than <see cref="RotateTRSmul"/>
        /// </summary>
        public void RotateTRSsub(float radians)
        {
            // decomposition
            float tx = this.m02;
            float ty = this.m12;

            float sx = (float)Math.Sqrt(this.m00 * this.m00 + this.m10 * this.m10);
            float sy = (float)Math.Sqrt(this.m01 * this.m01 + this.m11 * this.m11);

            // recomposition
            this = Matrix3.Rotation(radians);

            this.m00 *= sx;
            this.m10 *= sx;
            this.m01 *= sy;
            this.m11 *= sy;
            this.m02 = tx;
            this.m12 = ty;
        }
        /// <summary>
        /// Multiply the scale component of TRS transformations (mean that scalex is muliplied to existing scale value),
        /// require less computation than <see cref="ScaleTRSsub"/>
        /// </summary>
        public void ScaleTRSmul(float scalex, float scaley)
        {
            this.m00 *= scalex;
            this.m10 *= scalex;
            this.m01 *= scaley;
            this.m11 *= scaley;
        }
        /// <summary>
        /// Substitute the scale component of TRS transformations (mean the are set the scale value to current value)
        /// </summary>
        public void ScaleTRSsub(float scalex, float scaley)
        {
            // decomposition
            float sx = (float)Math.Sqrt(this.m00 * this.m00 + this.m10 * this.m10);
            float sy = (float)Math.Sqrt(this.m01 * this.m01 + this.m11 * this.m11);
            // recomposition
            this.m00 *= scalex / sx;
            this.m10 *= scalex / sx;
            this.m01 *= scaley / sy;
            this.m11 *= scaley / sy;
        }

        /// <summary>
        ///  Returns an inverted matrix, if not exit return Exception
        /// </summary>
        public static Matrix3 Inverse(Matrix3 matrix)
        {
            return matrix.Inverse();
        }



        #region operators



        public static Matrix3 operator *(Matrix3 left, Matrix3 right)
        {
            //   A = MxN  B=NxP   C = AB = MxP
            Matrix3 result = new Matrix3();

            result.m00 = left.m00 * right.m00 + left.m01 * right.m10 + left.m02 * right.m20;
            result.m01 = left.m00 * right.m01 + left.m01 * right.m11 + left.m02 * right.m21;
            result.m02 = left.m00 * right.m02 + left.m01 * right.m12 + left.m02 * right.m22;

            result.m10 = left.m10 * right.m00 + left.m11 * right.m10 + left.m12 * right.m20;
            result.m11 = left.m10 * right.m01 + left.m11 * right.m11 + left.m12 * right.m21;
            result.m12 = left.m10 * right.m02 + left.m11 * right.m12 + left.m12 * right.m22;

            result.m20 = left.m20 * right.m00 + left.m21 * right.m10 + left.m22 * right.m20;
            result.m21 = left.m20 * right.m01 + left.m21 * right.m11 + left.m22 * right.m21;
            result.m22 = left.m20 * right.m02 + left.m21 * right.m12 + left.m22 * right.m22;

            return result;
        }

        /// <summary>
        ///  vector * matrix [1x3 * 3x3 = 1x3]
        /// </summary>
        public static Vector3 operator *(Vector3 vector, Matrix3 matrix)
        {
            var product = new Vector3();

            product.x = matrix.m00 * vector.x + matrix.m01 * vector.y + matrix.m02 * vector.z;
            product.y = matrix.m10 * vector.x + matrix.m11 * vector.y + matrix.m12 * vector.z;
            product.z = matrix.m20 * vector.x + matrix.m21 * vector.y + matrix.m22 * vector.z;

            return product;
        }
        /// <summary>
        ///  M[3x3]*V[3x1] = V'[3x1]
        /// </summary>
        public static Vector3 operator *(Matrix3 matrix, Vector3 vector)
        {
            var product = new Vector3();

            product.x = matrix.m00 * vector.x + matrix.m01 * vector.y + matrix.m02 * vector.z;
            product.y = matrix.m10 * vector.x + matrix.m11 * vector.y + matrix.m12 * vector.z;
            product.z = matrix.m20 * vector.x + matrix.m21 * vector.y + matrix.m22 * vector.z;

            return product;
        }
        /// <summary>
        ///  M[3x3]*V[3x1] = V'[3x1] is used to make a transformation, the V*M isn't used
        ///  http://www.opengl-tutorial.org/beginners-tutorials/tutorial-3-matrices/
        /// </summary>
        public static Vector2 operator *(Matrix3 matrix, Vector2 vector)
        {
            //       3x3   *   3x1  =   3x1
            //		| a b c |   |x|    |ax + by + c|
            //		| d e f | * |y| =  |dx + ey + f| = Vresult
            //		| g h i |   |1|    |gx + hy + i|
            //
            //   Vresult /= (gx + hy + i)  omogeneus vector 
            //  if is a regular transform matrix, g=h=0 and inverseW = 1

            Vector2 product = new Vector2();
            float inverseW = (matrix.m20 * vector.x + matrix.m21 * vector.y + matrix.m22);
            inverseW = inverseW < -1e-6f || inverseW > 1e-6f ? 1.0f / inverseW : 1.0f;

            product.x = (matrix.m00 * vector.x + matrix.m01 * vector.y + matrix.m02) * inverseW;
            product.y = (matrix.m10 * vector.x + matrix.m11 * vector.y + matrix.m12) * inverseW;

            return product;
        }


        /// <summary>
        /// Negates all the items in the Matrix.
        /// </summary>
        public static Matrix3 operator -(Matrix3 matrix)
        {
            Matrix3 result = new Matrix3();

            result.m00 = -matrix.m00;
            result.m01 = -matrix.m01;
            result.m02 = -matrix.m02;
            result.m10 = -matrix.m10;
            result.m11 = -matrix.m11;
            result.m12 = -matrix.m12;
            result.m20 = -matrix.m20;
            result.m21 = -matrix.m21;
            result.m22 = -matrix.m22;

            return result;
        }
        /// <summary>
        ///  Used to subtract two matrices.
        /// </summary>
        public static Matrix3 operator -(Matrix3 left, Matrix3 right)
        {
            Matrix3 result = new Matrix3();

            for (int row = 0; row < 3; row++)
            {
                for (int col = 0; col < 3; col++)
                {
                    result[row, col] = left[row, col] - right[row, col];
                }
            }

            return result;
        }

        /// <summary>
        /// Convert my 2D transformation matrix in the .NET matrix class passed as reference to improve performace
        /// http://www.codeproject.com/Articles/8281/Matrix-Transformation-of-Images-using-NET-GDIplus
        /// </summary>
        public void UpdateNetMatrix(ref System.Drawing.Drawing2D.Matrix netmatrix)
        {
            netmatrix.Scale(m00, m11);
        }
        public static void UpdateNetMatrix(ref Matrix3 matrix, ref System.Drawing.Drawing2D.Matrix netmatrix)
        {

        }


        #endregion

        public override string ToString()
        {
            StringBuilder str = new StringBuilder();
            for (int i = 0; i < 3; i++)
                str.Append(String.Format("{0,4} {1,4} {2,4}\n", this[i, 0], this[i, 1], this[i, 2]));
            return str.ToString();
        }
    }
}