﻿/*
Axiom Graphics Engine Library
Copyright � 2003-2011 Axiom Project Team

The overall design, and a majority of the core engine and rendering code 
contained within this library is a derivative of the open source Object Oriented 
Graphics Engine OGRE, which can be found at http://ogre.sourceforge.net.  
Many thanks to the OGRE team for maintaining such a high quality project.

The math library included in this project, in addition to being a derivative of
the works of Ogre, also include derivative work of the free portion of the 
Wild Magic mathematics source code that is distributed with the excellent
book Game Engine Design.
http://www.wild-magic.com/

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

using System;
using System.Text;
using System.Runtime.InteropServices;

using Engine.Tools;

namespace Engine.Maths
{
    /// <summary>
    /// Directx Matrix4x4 is a trasposed matrix and don't match with the mathematical notation,
    /// but in this case i'll use always che classic math notation
    /// <para> 1  0  0  Tx</para>
    /// <para> 0  1  0  Ty</para>
    /// <para> 0  0  1  Tz</para>
    /// <para> 0  0  0  1</para> 
    /// 
    /// When i use a common transformation affine matrix SRT i mean that first Scale, second Rotate, last Traslate
    /// and is calculated as T*R*S, notice that text order is inverted.
    /// </summary>
    [StructLayout(LayoutKind.Explicit)]
    public struct Matrix4
    {
        public static readonly int sizeinbyte = 64;

        [FieldOffset(0)]
        public float m00;
        [FieldOffset(4)]
        public float m01;
        [FieldOffset(8)]
        public float m02;
        [FieldOffset(12)]
        public float m03;

        [FieldOffset(16)]
        public float m10;
        [FieldOffset(20)]
        public float m11;
        [FieldOffset(24)]
        public float m12;
        [FieldOffset(28)]
        public float m13;

        [FieldOffset(32)]
        public float m20;
        [FieldOffset(36)]
        public float m21;
        [FieldOffset(40)]
        public float m22;
        [FieldOffset(44)]
        public float m23;

        [FieldOffset(48)]
        public float m30;
        [FieldOffset(52)]
        public float m31;
        [FieldOffset(56)]
        public float m32;
        [FieldOffset(60)]
        public float m33;

        [FieldOffset(0)]
        public unsafe fixed float Dim[16];

        public static readonly Matrix4 Zero = new Matrix4(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        public static readonly Matrix4 Identity = new Matrix4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);


        public Matrix4(Vector3 X, Vector3 Y, Vector3 Z)
            : this(
                    X.x, Y.x, Z.x, 0,
                    X.y, Y.y, Z.y, 0,
                    X.z, Y.z, Z.z, 0,
                    0, 0, 0, 1)
        { }

        /// <summary>
        /// From quaternion
        /// </summary>
        public Matrix4(float qx, float qy, float qz, float qw)
        {
            m00 = 1 - 2 * qy * qy - 2 * qz * qz;
            m01 = 2 * qx * qy - 2 * qz * qw;
            m02 = 2 * qx * qz + 2 * qy * qw;
            m03 = 0;

            m10 = 2 * qx * qy + 2 * qz * qw;
            m11 = 1 - 2 * qx * qx - 2 * qz * qz;
            m12 = 2 * qy * qz - 2 * qx * qw;
            m13 = 0;

            m20 = 2 * qx * qz - 2 * qy * qw;
            m21 = 2 * qy * qz + 2 * qx * qw;
            m22 = 1 - 2 * qx * qx - 2 * qy * qy;
            m23 = 0;

            m30 = 0;
            m31 = 0;
            m32 = 0;
            m33 = 1;
        }


        public Matrix4(
            float m00, float m01, float m02, float m03,
            float m10, float m11, float m12, float m13,
            float m20, float m21, float m22, float m23,
            float m30, float m31, float m32, float m33)
        {
            this.m00 = m00;
            this.m01 = m01;
            this.m02 = m02;
            this.m03 = m03;
            this.m10 = m10;
            this.m11 = m11;
            this.m12 = m12;
            this.m13 = m13;
            this.m20 = m20;
            this.m21 = m21;
            this.m22 = m22;
            this.m23 = m23;
            this.m30 = m30;
            this.m31 = m31;
            this.m32 = m32;
            this.m33 = m33;
        }

        /// <summary>
        /// Get the i-th Row
        /// </summary>
        public Vector4 getRow(int i)
        {
            switch (i)
            {
                case 0: return new Vector4(m00, m01, m02, m03);
                case 1: return new Vector4(m10, m11, m12, m13);
                case 2: return new Vector4(m20, m21, m22, m23);
                case 3: return new Vector4(m30, m31, m32, m33);
                default: throw new IndexOutOfRangeException("index not in the range 0-3");
            }
        }
        /// <summary>
        /// Set the i-th Row
        /// </summary>
        public void setRow(int i, Vector4 val)
        {
            switch (i)
            {
                case 0: m00 = val.x; m01 = val.y; m02 = val.z; m03 = val.w; break;
                case 1: m10 = val.x; m11 = val.y; m12 = val.z; m13 = val.w; break;
                case 2: m20 = val.x; m21 = val.y; m22 = val.z; m23 = val.w; break;
                case 3: m30 = val.x; m31 = val.y; m32 = val.z; m33 = val.w; break;
                default: throw new IndexOutOfRangeException("index not in the range 0-3");
            }
        }
        /// <summary>
        /// Get the i-th Column
        /// </summary>
        public Vector4 getCol(int i)
        {
            switch (i)
            {
                case 0: return new Vector4(m00, m10, m20, m30);
                case 1: return new Vector4(m01, m11, m21, m31);
                case 2: return new Vector4(m02, m12, m22, m32);
                case 3: return new Vector4(m03, m13, m23, m33);
                default: throw new IndexOutOfRangeException("index not in the range 0-3");
            }
        }
        /// <summary>
        /// Set the i-th Column
        /// </summary>
        public void setCol(int i, Vector4 val)
        {
            switch (i)
            {
                case 0: m00 = val.x; m10 = val.y; m20 = val.z; m30 = val.w; break;
                case 1: m01 = val.x; m11 = val.y; m21 = val.z; m31 = val.w; break;
                case 2: m02 = val.x; m12 = val.y; m22 = val.z; m32 = val.w; break;
                case 3: m03 = val.x; m13 = val.y; m23 = val.z; m33 = val.w; break;
                default: throw new IndexOutOfRangeException("index not in the range 0-3");
            }
        }

        /// <summary>
        /// Slow method, is equivalent to use switch() of UNSAFE flag in this[int]
        /// </summary>
        public unsafe float GetDim(int r,int c)
        {
            fixed (float* buffer = Dim)
            {
                return buffer[r * 4 + c];
            }
        }
        /// <summary>
        ///    Allows the Matrix to be accessed like a 2d array (i.e. matrix[2,3])
        ///    unsafe version is little slower...
        /// </summary>
        /// <remarks>
        ///    This indexer is only provided as a convenience, and is <b>not</b> recommended for use in
        ///    intensive applications.  
        /// </remarks>
        public float this[int row, int col]
        {
            get
            {
#if !UNSAFE
                switch (row)
                {
                    case 0:
                        switch (col)
                        {
                            case 0: return m00;
                            case 1: return m01;
                            case 2: return m02;
                            case 3: return m03;
                        }
                        break;
                    case 1:
                        switch (col)
                        {
                            case 0: return m10;
                            case 1: return m11;
                            case 2: return m12;
                            case 3: return m13;
                        }
                        break;
                    case 2:
                        switch (col)
                        {
                            case 0: return m20;
                            case 1: return m21;
                            case 2: return m22;
                            case 3: return m23;
                        }
                        break;
                    case 3:
                        switch (col)
                        {
                            case 0: return m30;
                            case 1: return m31;
                            case 2: return m32;
                            case 3: return m33;
                        }
                        break;
                }
                throw new IndexOutOfRangeException("Attempt to access Matrix4 indexer out of bounds.");
#else
                unsafe
                {
                    fixed (float* pM = &this.m00)
                    {
                        return *(pM + ((4 * row) + col));
                    }
                }
#endif
            }
            set
            {
                //Debug.Assert((row >= 0 && row < 4) && (col >= 0 && col < 4), "Attempt to access Matrix4 indexer out of bounds.");

#if !UNSAFE
                switch (row)
                {
                    case 0:
                        switch (col)
                        {
                            case 0: m00 = value; break;
                            case 1: m01 = value; break;
                            case 2: m02 = value; break;
                            case 3: m03 = value; break;
                        }
                        break;
                    case 1:
                        switch (col)
                        {
                            case 0: m10 = value; break;
                            case 1: m11 = value; break;
                            case 2: m12 = value; break;
                            case 3: m13 = value; break;
                        }
                        break;
                    case 2:
                        switch (col)
                        {
                            case 0: m20 = value; break;
                            case 1: m21 = value; break;
                            case 2: m22 = value; break;
                            case 3: m23 = value; break;
                        }
                        break;
                    case 3:
                        switch (col)
                        {
                            case 0: m30 = value; break;
                            case 1: m31 = value; break;
                            case 2: m32 = value; break;
                            case 3: m33 = value; break;
                        }
                        break;
                }
#else
                unsafe
                {
                    fixed (float* pM = &this.m00)
                    {
                        *(pM + ((4 * row) + col)) = value;
                    }
                }
#endif
            }
        }

        /// <summary>
        ///		Allows the Matrix to be accessed linearly (m[0] -> m[15]).  
        /// </summary>
        /// <remarks>
        ///    This indexer is only provided as a convenience, and is <b>not</b> recommended for use in
        ///    intensive applications.  
        /// </remarks>
        public float this[int index]
        {
            get
            {
                //Debug.Assert(index >= 0 && index < 16, "Attempt to access Matrix4 linear indexer out of bounds.");

#if !UNSAFE

                switch (index)
                {
                    case 0: return m00;
                    case 1: return m01;
                    case 2: return m02;
                    case 3: return m03;
                    case 4: return m10;
                    case 5: return m11;
                    case 6: return m12;
                    case 7: return m13;
                    case 8: return m20;
                    case 9: return m21;
                    case 10: return m22;
                    case 11: return m23;
                    case 12: return m30;
                    case 13: return m31;
                    case 14: return m32;
                    case 15: return m33;
                }

                throw new IndexOutOfRangeException("Attempt to access Matrix4 indexer out of bounds.");
#else
                unsafe
                {
                    fixed (float* pMatrix = &this.m00)
                    {
                        return *(pMatrix + index);
                    }
                }
#endif
            }
            set
            {
                //Debug.Assert(index >= 0 && index < 16, "Attempt to access Matrix4 linear indexer out of bounds.");

#if !UNSAFE
                switch (index)
                {
                    case 0: m00 = value; break;
                    case 1: m01 = value; break;
                    case 2: m02 = value; break;
                    case 3: m03 = value; break;
                    case 4: m10 = value; break;
                    case 5: m11 = value; break;
                    case 6: m12 = value; break;
                    case 7: m13 = value; break;
                    case 8: m20 = value; break;
                    case 9: m21 = value; break;
                    case 10: m22 = value; break;
                    case 11: m23 = value; break;
                    case 12: m30 = value; break;
                    case 13: m31 = value; break;
                    case 14: m32 = value; break;
                    case 15: m33 = value; break;
                }
#else
                unsafe
                {
                    fixed (float* pMatrix = &this.m00)
                    {
                        *(pMatrix + index) = value;
                    }
                }
#endif
            }
        }

        /// <summary>
        ///  Used to generate the adjoint of this matrix.
        /// </summary>
        Matrix4 Adjoint()
        {
            // note: this is an expanded version of the Ogre adjoint() method, to give better performance in C#. Generated using a script
            float i00 = this.m11 * (this.m22 * this.m33 - this.m32 * this.m23) -
                       this.m12 * (this.m21 * this.m33 - this.m31 * this.m23) +
                       this.m13 * (this.m21 * this.m32 - this.m31 * this.m22);

            float i01 = -(this.m01 * (this.m22 * this.m33 - this.m32 * this.m23) -
                         this.m02 * (this.m21 * this.m33 - this.m31 * this.m23) +
                         this.m03 * (this.m21 * this.m32 - this.m31 * this.m22));

            float i02 = this.m01 * (this.m12 * this.m33 - this.m32 * this.m13) -
                         this.m02 * (this.m11 * this.m33 - this.m31 * this.m13) +
                         this.m03 * (this.m11 * this.m32 - this.m31 * this.m12);

            float i03 = -(this.m01 * (this.m12 * this.m23 - this.m22 * this.m13) -
                          this.m02 * (this.m11 * this.m23 - this.m21 * this.m13) +
                          this.m03 * (this.m11 * this.m22 - this.m21 * this.m12));

            float i10 = -(this.m10 * (this.m22 * this.m33 - this.m32 * this.m23) -
                          this.m12 * (this.m20 * this.m33 - this.m30 * this.m23) +
                          this.m13 * (this.m20 * this.m32 - this.m30 * this.m22));

            float i11 = this.m00 * (this.m22 * this.m33 - this.m32 * this.m23) -
                         this.m02 * (this.m20 * this.m33 - this.m30 * this.m23) +
                         this.m03 * (this.m20 * this.m32 - this.m30 * this.m22);

            float val6 = -(this.m00 * (this.m12 * this.m33 - this.m32 * this.m13) -
                           this.m02 * (this.m10 * this.m33 - this.m30 * this.m13) +
                           this.m03 * (this.m10 * this.m32 - this.m30 * this.m12));

            float val7 = this.m00 * (this.m12 * this.m23 - this.m22 * this.m13) -
                         this.m02 * (this.m10 * this.m23 - this.m20 * this.m13) +
                         this.m03 * (this.m10 * this.m22 - this.m20 * this.m12);

            float val8 = this.m10 * (this.m21 * this.m33 - this.m31 * this.m23) -
                         this.m11 * (this.m20 * this.m33 - this.m30 * this.m23) +
                         this.m13 * (this.m20 * this.m31 - this.m30 * this.m21);

            float val9 = -(this.m00 * (this.m21 * this.m33 - this.m31 * this.m23) -
                           this.m01 * (this.m20 * this.m33 - this.m30 * this.m23) +
                           this.m03 * (this.m20 * this.m31 - this.m30 * this.m21));

            float val10 = this.m00 * (this.m11 * this.m33 - this.m31 * this.m13) -
                          this.m01 * (this.m10 * this.m33 - this.m30 * this.m13) +
                         this.m03 * (this.m10 * this.m31 - this.m30 * this.m11);

            float val11 = -(this.m00 * (this.m11 * this.m23 - this.m21 * this.m13) -
                            this.m01 * (this.m10 * this.m23 - this.m20 * this.m13) +
                            this.m03 * (this.m10 * this.m21 - this.m20 * this.m11));

            float val12 = -(this.m10 * (this.m21 * this.m32 - this.m31 * this.m22) -
                            this.m11 * (this.m20 * this.m32 - this.m30 * this.m22) +
                            this.m12 * (this.m20 * this.m31 - this.m30 * this.m21));

            float val13 = this.m00 * (this.m21 * this.m32 - this.m31 * this.m22) -
                          this.m01 * (this.m20 * this.m32 - this.m30 * this.m22) +
                          this.m02 * (this.m20 * this.m31 - this.m30 * this.m21);

            float val14 = -(this.m00 * (this.m11 * this.m32 - this.m31 * this.m12) -
                            this.m01 * (this.m10 * this.m32 - this.m30 * this.m12) +
                            this.m02 * (this.m10 * this.m31 - this.m30 * this.m11));

            float val15 = this.m00 * (this.m11 * this.m22 - this.m21 * this.m12) -
                          this.m01 * (this.m10 * this.m22 - this.m20 * this.m12) +
                          this.m02 * (this.m10 * this.m21 - this.m20 * this.m11);

            return new Matrix4(i00, i01, i02, i03, i10, i11, val6, val7, val8, val9, val10, val11, val12, val13, val14, val15);
        }



        /// <summary>
        ///	 Get or Set the translation value of the matrix using math notation
        ///		| 0 0 0 Tx |
        ///		| 0 0 0 Ty |
        ///		| 0 0 0 Tz |
        ///		| 0 0 0  1 |
        /// </summary>
        public Vector3 Position
        {
            get { return new Vector3(this.m03, this.m13, this.m23); }
            set { this.m03 = value.x; this.m13 = value.y; this.m23 = value.z; }
        }


        /// <summary>
        ///  Get or Set the scale value of the matrix. Not match when matrix is rotated
        ///		|Sx 0  0  0 |
        ///		| 0 Sy 0  0 |
        ///		| 0  0 Sz 0 |
        ///		| 0  0  0 1 |
        /// </summary>
        public Vector3 DiagonalComponent
        {
            get
            {
                return new Vector3(this.m00, this.m11, this.m22);
            }
            set
            {
                this.m00 = value.x;
                this.m11 = value.y;
                this.m22 = value.z;
            }
        }
        /// <summary>
        /// Get the determinant of matrix.
        /// </summary>
        public float Determinant
        {
            get
            {
                float result = m00 *
                             (m11 * (m22 * m33 - m32 * m23) -
                              m12 * (m21 * m33 - m31 * m23) +
                              m13 * (m21 * m32 - m31 * m22)) -
                              m01 *
                             (m10 * (m22 * m33 - m32 * m23) -
                              m12 * (m20 * m33 - m30 * m23) +
                              m13 * (m20 * m32 - m30 * m22)) +
                              m02 *
                             (m10 * (m21 * m33 - m31 * m23) -
                              m11 * (m20 * m33 - m30 * m23) +
                              m13 * (m20 * m31 - m30 * m21)) -
                              m03 *
                             (m10 * (m21 * m32 - m31 * m22) -
                              m11 * (m20 * m32 - m30 * m22) +
                              m12 * (m20 * m31 - m30 * m21));

                return result;
            }
        }

        /// <summary>
        /// Extract the 3x3 matrix representing the current rotation. 
        /// </summary>
        public Matrix3 ExtractRotation()
        {
            Vector3 axis = new Vector3();
            Matrix3 rotation = Matrix3.Identity;

            axis.x = this.m00;
            axis.y = this.m10;
            axis.z = this.m20;
            axis.Normalize();
            rotation.m00 = axis.x;
            rotation.m10 = axis.y;
            rotation.m20 = axis.z;

            axis.x = this.m01;
            axis.y = this.m11;
            axis.z = this.m21;
            axis.Normalize();
            rotation.m01 = axis.x;
            rotation.m11 = axis.y;
            rotation.m21 = axis.z;

            axis.x = this.m02;
            axis.y = this.m12;
            axis.z = this.m22;
            axis.Normalize();
            rotation.m02 = axis.x;
            rotation.m12 = axis.y;
            rotation.m22 = axis.z;

            return rotation;
        }
        /// <summary>
        /// Extract scaling information.
        /// </summary>
        public Vector3 ExtractScale()
        {
            var scale = new Vector3(1, 1, 1);
            var axis = new Vector3(0, 0, 0);

            axis.x = this.m00;
            axis.y = this.m10;
            axis.z = this.m20;
            scale.x = axis.Length;

            axis.x = this.m01;
            axis.y = this.m11;
            axis.z = this.m21;
            scale.y = axis.Length;

            axis.x = this.m02;
            axis.y = this.m12;
            axis.z = this.m22;
            scale.z = axis.Length;

            return scale;
        }

        /// <summary>
        /// http://www.euclideanspace.com/maths/geometry/rotations/conversions/matrixToQuaternion/
        /// </summary>
        public Quaternion GetQuaternion()
        {
            Quaternion q = new Quaternion();
            float trace = m00 + m11 + m22;

            if (trace > 0)
            {
                float s = 0.5f / (float)Math.Sqrt(trace + 1);
                q.w = 0.25f / s;
                q.x = (m21 - m12) * s;
                q.y = (m02 - m20) * s;
                q.z = (m10 - m01) * s;
            }
            else
            {
                if (m00 > m11 && m00 > m22)
                {
                    float s = 2.0f * (float)Math.Sqrt(1.0f + m00 - m11 - m22);
                    q.w = (m21 - m12) / s;
                    q.x = 0.25f * s;
                    q.y = (m01 + m10) / s;
                    q.z = (m02 + m20) / s;
                }
                else if (m11 > m22)
                {
                    float s = 2.0f * (float)Math.Sqrt(1.0f + m11 - m00 - m22);
                    q.w = (m02 - m20) / s;
                    q.x = (m01 + m10) / s;
                    q.y = 0.25f * s;
                    q.z = (m12 + m21) / s;
                }
                else
                {
                    float s = 2.0f * (float)Math.Sqrt(1.0f + m22 - m00 - m11);
                    q.w = (m10 - m01) / s;
                    q.x = (m02 + m20) / s;
                    q.y = (m12 + m21) / s;
                    q.z = 0.25f * s;
                }
            }
            return q;
        }


        /// <summary>
        /// Decompose the matrix.
        /// </summary>
        public void Decompose(out Vector3 translation, out Vector3 scale, out Quaternion orientation)
        {
            scale = new Vector3(1, 1, 1);
            var rotation = Matrix3.Identity;
            var axis = Vector3.Zero;

            axis.x = this.m00;
            axis.y = this.m10;
            axis.z = this.m20;
            scale.x = axis.Normalize(); // Normalize() returns the vector's length before it was normalized
            rotation.m00 = axis.x;
            rotation.m10 = axis.y;
            rotation.m20 = axis.z;

            axis.x = this.m01;
            axis.y = this.m11;
            axis.z = this.m21;
            scale.y = axis.Normalize();
            rotation.m01 = axis.x;
            rotation.m11 = axis.y;
            rotation.m21 = axis.z;

            axis.x = this.m02;
            axis.y = this.m12;
            axis.z = this.m22;
            scale.z = axis.Normalize();
            rotation.m02 = axis.x;
            rotation.m12 = axis.y;
            rotation.m22 = axis.z;

            /* http://www.robertblum.com/articles/2005/02/14/decomposing-matrices check to support transforms with negative scaling */
            //thanks sebj for the info
            if (rotation.Determinant < 0)
            {
                rotation.m00 = -rotation.m00;
                rotation.m10 = -rotation.m10;
                rotation.m20 = -rotation.m20;
                scale.x = -scale.x;
            }

            orientation = Quaternion.FromRotationMatrix(rotation);
            translation = this.Position;
        }


        #region Operators
        public static implicit operator Matrix3(Matrix4 matrix)
        {
            Matrix3 m = new Matrix3();
            m.m00 = matrix.m00;
            m.m01 = matrix.m01;
            m.m02 = matrix.m02;

            m.m10 = matrix.m10;
            m.m11 = matrix.m11;
            m.m12 = matrix.m12;

            m.m20 = matrix.m20;
            m.m21 = matrix.m21;
            m.m22 = matrix.m22;

            return m;
        }

        /// <summary>
        ///          __R
        ///          \
        /// M[r,c] = /_i  A[r,i] * B[i,c]
        ///          
        /// </summary>
        public static Matrix4 operator *(Matrix4 left, Matrix4 right)
        {
            Matrix4 result = left;
            result.Multiply(ref right);
            return result;
        }

        // function for matrix * vector multiplication
        //
        // | a b c d |   |vx|
        // | 0 0 0 0 |   |vy|
        // | 0 0 0 0 | * |vz| = | x y z w |
        // | 0 0 0 0 |   |vw|
        //
        //
        // function for vector^T * Matrix multiplication
        //
        //                 | a 0 0 0 |    |x|
        //                 | b 0 0 0 |    |y|
        // |vx vy vz vw| * | c 0 0 0 |  = |z| 
        //                 | d 0 0 0 |    |w|


        /// <summary>
        /// M x V
        ///	Transforms the given 3-D vector by the matrix, projecting the 
        ///	result back into <i>w</i> = 1.
        ///	<p/>
        ///	This means that the initial <i>w</i> is considered to be 1.0,
        ///	and then all the tree elements of the resulting 3-D vector are
        ///	divided by the resulting <i>w</i>.
        /// </summary>
        /// <remarks>
        /// ABij = Sk(Aik,Bkj)
        /// </remarks>
        public static Vector3 operator *(Matrix4 matrix, Vector3 right)
        {
            Vector3 result = new Vector3();
            Vector3.TransformCoordinate(ref right, ref matrix, ref result);
            return result;
        }

        /// <summary>
        /// Never used a pre-multiply
        /// </summary>
        public static Vector3 operator *(Vector3 left, Matrix4 matrix)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///	Used to multiply a Matrix4 object by a scalar value..
        /// </summary>
        public static Matrix4 operator *(Matrix4 left, float scalar)
        {
            // left is passed as value
            left.Multiply(scalar);
            return left;
        }
        public static Matrix4 operator +(Matrix4 left, Matrix4 right)
        {
            left.m00 += right.m00;
            left.m01 += right.m01;
            left.m02 += right.m02;
            left.m03 += right.m03;

            left.m10 += right.m10;
            left.m11 += right.m11;
            left.m12 += right.m12;
            left.m13 += right.m13;

            left.m20 += right.m20;
            left.m21 += right.m21;
            left.m22 += right.m22;
            left.m23 += right.m23;

            left.m30 += right.m30;
            left.m31 += right.m31;
            left.m32 += right.m32;
            left.m33 += right.m33;

            return left;
        }
        public static Matrix4 operator -(Matrix4 left, Matrix4 right)
        {
            left.m00 -= right.m00;
            left.m01 -= right.m01;
            left.m02 -= right.m02;
            left.m03 -= right.m03;

            left.m10 -= right.m10;
            left.m11 -= right.m11;
            left.m12 -= right.m12;
            left.m13 -= right.m13;

            left.m20 -= right.m20;
            left.m21 -= right.m21;
            left.m22 -= right.m22;
            left.m23 -= right.m23;

            left.m30 -= right.m30;
            left.m31 -= right.m31;
            left.m32 -= right.m32;
            left.m33 -= right.m33;

            return left;
        }

        public static bool AlmostEqual(ref Matrix4 left, ref Matrix4 right , int maxdeltabits = 2)
        {
            return MathUtils.AlmostEqual(left.m00, right.m00, maxdeltabits) &&
                    MathUtils.AlmostEqual(left.m01, right.m01, maxdeltabits) &&
                    MathUtils.AlmostEqual(left.m02, right.m02, maxdeltabits) &&
                    MathUtils.AlmostEqual(left.m03, right.m03, maxdeltabits) &&

                    MathUtils.AlmostEqual(left.m10, right.m10, maxdeltabits) &&
                    MathUtils.AlmostEqual(left.m11, right.m11, maxdeltabits) &&
                    MathUtils.AlmostEqual(left.m12, right.m12, maxdeltabits) &&
                    MathUtils.AlmostEqual(left.m13, right.m13, maxdeltabits) &&

                    MathUtils.AlmostEqual(left.m20, right.m20, maxdeltabits) &&
                    MathUtils.AlmostEqual(left.m21, right.m21, maxdeltabits) &&
                    MathUtils.AlmostEqual(left.m22, right.m22, maxdeltabits) &&
                    MathUtils.AlmostEqual(left.m23, right.m23, maxdeltabits) &&

                    MathUtils.AlmostEqual(left.m30, right.m30, maxdeltabits) &&
                    MathUtils.AlmostEqual(left.m31, right.m31, maxdeltabits) &&
                    MathUtils.AlmostEqual(left.m32, right.m32,maxdeltabits) &&
                    MathUtils.AlmostEqual(left.m33, right.m33, maxdeltabits);
        }


        public static bool operator ==(Matrix4 left, Matrix4 right)
        {
            return left.m00 == right.m00 &&
            left.m01 == right.m01 &&
            left.m02 == right.m02 &&
            left.m03 == right.m03 &&

            left.m10 == right.m10 &&
            left.m11 == right.m11 &&
            left.m12 == right.m12 &&
            left.m13 == right.m13 &&

            left.m20 == right.m20 &&
            left.m21 == right.m21 &&
            left.m22 == right.m22 &&
            left.m23 == right.m23 &&

            left.m30 == right.m30 &&
            left.m31 == right.m31 &&
            left.m32 == right.m32 &&
            left.m33 == right.m33;
        }
        public static bool operator !=(Matrix4 left, Matrix4 right)
        {
            return !(left == right);
        }


        #endregion

        #region Static Constructors

        #region NOT TESTED

        /// <summary>
        /// Semplified inverse for projection matrix
        /// </summary>
        public static Matrix4 InverseProjection(ref Matrix4 proj)
        {
            Matrix4 inv = new Matrix4();

            //traspose rotation matrix
            inv.m00 = proj.m00;
            inv.m01 = proj.m10;
            inv.m02 = proj.m20;

            inv.m10 = proj.m01;
            inv.m11 = proj.m11;
            inv.m12 = proj.m21;

            inv.m20 = proj.m02;
            inv.m21 = proj.m12;
            inv.m22 = proj.m22;

            // fix col4
            inv.m03 = inv.m13 = inv.m23 = 0; inv.m33 = 1;

            //traslate position -p.r  -p.u -p.l
            inv.m30 = -proj.m03 * proj.m00 - proj.m13 * proj.m10 - proj.m23 * proj.m20;
            inv.m31 = -proj.m03 * proj.m01 - proj.m13 * proj.m11 - proj.m23 * proj.m21;
            inv.m32 = -proj.m03 * proj.m02 - proj.m13 * proj.m12 - proj.m23 * proj.m22;

            return inv;
        }


        #endregion



        /// <summary>
        /// Make a Left-Handle orthogonal projection matrix.
        /// The left right top bottom value are the corner in world coordinate of the AABB build with
        /// Identity View Matrix ( eye=Zero, look=unitZ  up=unitY) 
        /// https://msdn.microsoft.com/en-us/library/windows/desktop/bb205347(v=vs.85).aspx
        /// </summary>
        public static Matrix4 MakeOrthoLH(float near, float far, float left, float right, float top, float bottom)
        {
            // 2/(r-l)      0            0         0
            // 0            2/(t-b)      0         0
            // 0            0            1/(f-n)   0
            // (l+r)/(l-r)  (t+b)/(b-t)  n/(n-f)   1

            float w = right - left;
            float h = top - bottom;

            float A = 2 / w;
            float B = 2 / h;
            float C = 1.0f / (far - near);
            float D = near < 1e-16 ? -C : -C * near;
            float E = -(left + right) / w;
            float F = -(top + bottom) / h;

            // trasposed
            return new Matrix4(
                A, 0, 0, E,
                0, B, 0, F,
                0, 0, C, D,
                0, 0, 0, 1);
        }


        /// <summary>
        /// Make a Left-Handle orthogonal projection matrix.
        /// The width and height size isn't the viewport value but camera volume size.
        /// For a camera implementation you have to set at the beginning the relation of
        /// Viewport_Width and World_Width of default zoom level (100%)
        /// 
        /// https://msdn.microsoft.com/en-us/library/bb205346(v=vs.85).aspx
        /// </summary>
        /// <param name="volumeheight">sugestion : volumewidth * screen_aspect_ratio</param>
        /// <param name="volumewidth">sugestion : volumeheight / screen_aspect_ratio</param>
        public static Matrix4 MakeOrthoLH(float near, float far, float volumewidth, float volumeheight)
        {
            // 2/w  0    0        0
            // 0    2/h  0        0
            // 0    0    1/(f-n)  0
            // 0    0   -n/(f-n)  1

            float A = 2 / volumewidth;
            float B = 2 / volumeheight;
            float C = 1.0f / (far - near);
            float D = near < 1e-16 ? -C : -C * near;

            // trasposed
            return new Matrix4(
                A, 0, 0, 0,
                0, B, 0, 0,
                0, 0, C, D,
                0, 0, 0, 1);
        }

        /// <summary>
        /// NOT TESTED
        /// Make a Left-Handle prospective projection matrix with Frustum Coorner
        /// The volume of Frustum is define by rectangle :
        /// Min(Left,Bottom,Near) Max(Right,Top,Near) and Eye of view at (0,0,0) where near = 0
        /// 
        /// https://msdn.microsoft.com/en-us/library/bb205353(v=vs.85).aspx
        /// </summary>
        public static Matrix4 MakeProjectionLH(float near, float far, float left, float right, float top, float bottom)
        {
            // 2*n/(r-l)    0            0          0
            // 0            2*n/(t-b)    0          0
            // (l+r)/(l-r)  (t+b)/(b-t)  f/(f-n)    1
            // 0            0            n*f/(n-f)  0

            float w = right - left;
            float h = top - bottom;
            float A = 2 * near / w;
            float B = 2 * near / h;
            float C = far / (far - near);
            float D = near < 1e-16 ? -C : -C * near;
            float E = -(left + right) / w;
            float F = -(top + bottom) / h;

            // trasposed
            return new Matrix4(
                A, 0, E, 0,
                0, B, F, 0,
                0, 0, C, D,
                0, 0, 1, 0);
        }
        
        /// <summary>
        /// Make a Left-Handle prospective projection matrix with FOVY , FovX derived from aspect ratio
        /// https://msdn.microsoft.com/en-us/library/bb205350(v=VS.85).aspx
        /// </summary>
        public static Matrix4 MakeProjectionAFovYLH(float near, float far, float aspectratio , float fovy = MathUtils.Rad45)
        {
            // xScale     0          0           0
            // 0        yScale       0           0
            // 0          0       f/(f-n)        1
            // 0          0       -n*f/(f-n)     0

            // where:
            // yScale = cot(fovY/2)
            // xScale = yScale / aspectratio

            // by default Width size have the prioirity than Height so xscale
            float A = (float)(1.0 / Math.Tan(fovy * 0.5)) / aspectratio;
            float B = (float)(1.0 / Math.Tan(fovy * 0.5));

            // far positive infinite
            float C = far > 1e16 ? 1 : far / (far - near);
            
            // near zero ???
            float D = near < 1e-16 ? -1 : -C * near;

            // notice the D and 1 are trasposed respect standard directx because i use colum_major
            return new Matrix4(
                A, 0, 0, 0,
                0, B, 0, 0,
                0, 0, C, D,
                0, 0, 1, 0);
        }
        /// <summary>
        /// Make a Left-Handle prospective projection matrix with FOVX and FOVY.
        /// FovX override aspect ratio fix
        /// </summary>
        public static Matrix4 MakeProjectionFovXYLH(float near, float far, float fovy = MathUtils.Rad45, float fovx = MathUtils.Rad45)
        {
            float A = (float)(1.0 / Math.Tan(fovx * 0.5));
            float B = (float)(1.0 / Math.Tan(fovy * 0.5));

            // far positive infinite
            float C = far > 1e16 ? 1 : far / (far - near);

            // near zero ???
            float D = near < 1e-16 ? -1 : -C * near;

            // notice the D and 1 are trasposed respect standard directx because i use colum_major
            return new Matrix4(
                A, 0, 0, 0,
                0, B, 0, 0,
                0, 0, C, D,
                0, 0, 1, 0);
        }



        /// <summary>
        /// Make a Left-Handle view matrix.
        /// </summary>
        /// <remarks>
        /// The "up" vector by default is "Y" but will be ortogonalized in the matrix so the "up" component will be different.
        /// Carefull when eye-target axes is vertical, the up vector can't be "Y" otherwise the cross product return a ramndom rotation
        /// 
        /// Identity :
        ///       y
        ///       | z
        ///       |/____ x
        ///       
        /// where eye direction is +Z
        /// </remarks>
        /// <param name="up"> default is (0,1,0) </param>
        /// <param name="eye"> default is (0,0,0) </param>
        /// <param name="target"> default is (0,0,1) </param>
        public static Matrix4 MakeViewLH(Vector3 eye, Vector3 target, Vector3 up)
        {
            // | r   u   d   r.e |   right , up , direction , eye vectors
            // | r   u   d   u.e |
            // | r   u   d   d.e |
            // | 0   0   0    1  |

            // you pass a zero vector
            if (up.Normalize() < 1e-6) up = Vector3.UnitY;

            // targhet and eye have same coordinates
            Vector3 vz = target - eye;
            if (vz.Normalize() < 1e-6) vz = Vector3.UnitZ;

            // z vector is parallel to up vector , the cross == (0,0,0)
            Vector3 vx = Vector3.Cross(up, vz);
            if (vx.Normalize() < 1e-6) vx = Vector3.UnitX;

            // y vector is parallel to x vector, the cross == (0,0,0)
            Vector3 vy = Vector3.Cross(vz, vx);
            if (vy.Normalize() < 1e-6) vy = Vector3.UnitY;

            return new Matrix4(vx.x, vx.y, vx.z, -Vector3.Dot(vx, eye),
                               vy.x, vy.y, vy.z, -Vector3.Dot(vy, eye),
                               vz.x, vz.y, vz.z, -Vector3.Dot(vz, eye),
                               0, 0, 0, 1);
        }


        public static Matrix4 RotateAxis(Vector3 vect, float angle)
        {
            return RotateAxis(vect.x, vect.y, vect.z, angle);
        }
        public static Matrix4 RotateAxis(ref Vector3 vect, float angle)
        {
            return RotateAxis(vect.x, vect.y, vect.z, angle);
        }
        
        /// <summary>
        /// Left-Hand coordinate : if axe direction is your thumb, other fingers indicate a positive angle direction 
        /// Vector must be normalized
        /// </summary>
        /// <remarks>for a Right-Hand rule, do a traspose</remarks>
        public static Matrix4 RotateAxis(float x, float y, float z, float angle)
        {
            float c = (float)Math.Cos(angle);
            float s = (float)Math.Sin(angle);
            float t = 1 - c;

            return new Matrix4(
                c + x * x * t, x * y * t - z * s, x * z * t + y * s, 0,
                y * x * t + z * s, c + y * y * t, y * z * t - x * s, 0,
                z * x * t - y * s, z * y * t + x * s, c + z * z * t, 0,
                0, 0, 0, 1);
        }


        public static Matrix4 RotationX(float radians)
        {
            //  0  0  0  0
            //  0  c -s  0
            //  0  s  c  0
            //  0  0  0  1
            Matrix4 matrix = Matrix4.Identity;
            matrix.m11 = (float)Math.Cos(radians);
            matrix.m12 = -(float)Math.Sin(radians);
            matrix.m21 = (float)Math.Sin(radians);
            matrix.m22 = (float)Math.Cos(radians);
            return matrix;
        }
        public static Matrix4 RotationY(float radians)
        {
            //  c  0  s  0
            //  0  0  0  0
            // -s  0  c  0
            //  0  0  0  1
            Matrix4 matrix = Matrix4.Identity;
            matrix.m00 = (float)Math.Cos(radians);
            matrix.m02 = (float)Math.Sin(radians);
            matrix.m20 = -(float)Math.Sin(radians);
            matrix.m22 = (float)Math.Cos(radians);
            return matrix;
        }
        public static Matrix4 RotationZ(float radians)
        {
            //  c -s  0  0
            //  s  c  0  0
            //  0  0  0  0
            //  0  0  0  1

            Matrix4 matrix = Matrix4.Identity;
            matrix.m00 = (float)Math.Cos(radians);
            matrix.m01 = -(float)Math.Sin(radians);
            matrix.m10 = (float)Math.Sin(radians);
            matrix.m11 = (float)Math.Cos(radians);
            return matrix;
        }

        public static Matrix4 Scaling(Vector3 scale)
        {
            return Matrix4.Scaling(scale.x, scale.y, scale.z);
        }
        public static Matrix4 Scaling(ref Vector3 scale)
        {
            return Matrix4.Scaling(scale.x, scale.y, scale.z);
        }
        public static Matrix4 Scaling(float x, float y, float z)
        {
            Matrix4 matrix = Matrix4.Identity;
            matrix.m00 = x;
            matrix.m11 = y;
            matrix.m22 = z;
            return matrix;
        }

        public static Matrix4 Translating(Vector3 vector)
        {
            return Translating(ref vector);
        }
        public static Matrix4 Translating(ref Vector3 vector)
        {
            Matrix4 matrix = Matrix4.Identity;
            matrix.m03 = vector.x;
            matrix.m13 = vector.y;
            matrix.m23 = vector.z;
            return matrix;
        }
        public static Matrix4 Translating(float x, float y, float z)
        {
            Matrix4 matrix = Matrix4.Identity;
            matrix.m03 = x;
            matrix.m13 = y;
            matrix.m23 = z;
            return matrix;
        }
        
        
        public static Matrix4 Orienting(ref Vector3 axisY)
        {
            Matrix4 matrix = Matrix4.Identity;

            Vector3 row2 = Vector3.Cross(Vector3.UnitX, axisY);

            if (row2.LengthSq < 1e-6f) row2 = Vector3.UnitZ;

            Vector3 row0 = Vector3.Cross(axisY, row2);
            row2 = Vector3.Cross(row0, axisY);


            matrix.setCol(0, new Vector4(row0, 0));
            matrix.setCol(1, new Vector4(axisY, 0));
            matrix.setCol(2, new Vector4(row2, 0));

            return matrix;
        }
        public static Matrix4 Traspose(Matrix4 matrix)
        {
            matrix.Traspose();
            return matrix;
        }

        public void Invert()
        {
            float d = Determinant;

            if (d > -float.Epsilon && d < float.Epsilon)
            {
                throw new Exception("matrix can't be inverted");
            }
            else
            {
                this = this.Adjoint();
                this.Multiply(1f / d);
            }
        }
        /// <summary>
        ///  Returns an inverted matrix, if not generate Exception
        /// </summary>
        public Matrix4 Inverse()
        {
            Matrix4 result;
            Inverse(ref this, out result);
            return result;
        }  
        
        public static Matrix4 Inverse(Matrix4 matrix)
        {
            return matrix.Inverse();
        }
        /// <summary>
        /// Parameter by reference are used to eliminate value-copy for struct then can optimize the performances
        /// </summary>
        public static void Inverse(ref Matrix4 mat, out Matrix4 result)
        {
            float d = mat.Determinant;
#if DEBUG
            if (d > -float.Epsilon && d < float.Epsilon) throw new Exception("matrix can't be inverted");
#endif
            result = mat.Adjoint();
            result.Multiply(1f / d);
        }

        /// <summary>
        /// Inverse of affine transformation (only traslation)
        /// </summary>
        /// <remarks>
        /// If the affine trasformation is T , the inverse T^-1 = (-T)
        /// </remarks>
        public static void InvertTMatrix(ref Matrix4 T)
        {
            T.m03 *= -1;
            T.m13 *= -1;
            T.m23 *= -1;
        }
        /// <summary>
        /// Inverse of affine transformation (only rotation)
        /// </summary>
        /// <remarks>
        /// If the affine trasformation is R , the inverse R^-1 = R^t
        /// </remarks>
        public static void InvertRMatrix(ref Matrix4 R)
        {
            MathUtils.SWAP(ref R.m01, ref R.m10);
            MathUtils.SWAP(ref R.m02, ref R.m20);
            MathUtils.SWAP(ref R.m12, ref R.m21);
        }
        /// <summary>
        /// Inverse of affine transformation where RT = first rotation then traslation = T * R.
        /// example for view matrix
        /// </summary>
        /// <remarks>
        /// If the affine trasformation is T*R , the inverse (T*R)^-1 = R^-1 * T^-1 = R^t * (-T)
        /// is X6 faster than calculate inverse
        /// </remarks>
        public static void InvertRTMatrix(ref Matrix4 RT)
        {
            // | a  b  c  x |     | a  d  g  -(ax+dy+gz) |
            // | d  e  f  y | ->  | b  e  h  -(bx+ey+hz) |
            // | g  h  i  z |     | c  f  i  -(cx+fy+iz) |
            // | 0  0  0  1 |     | 0  0  0       1      |

            InvertRMatrix(ref RT);

            float x = RT.m03;
            float y = RT.m13;
            float z = RT.m23;

            RT.m03 = -RT.m00 * x - RT.m01 * y - RT.m02 * z;
            RT.m13 = -RT.m10 * x - RT.m11 * y - RT.m12 * z;
            RT.m23 = -RT.m20 * x - RT.m21 * y - RT.m22 * z;
        }

        /// <summary>
        /// Inverse of affine transformation where SRT = first scale , second rotation then traslation = T * R * S
        /// example for model matrix. Not performance difference with SR transformation then can be used also for T=0
        /// </summary>
        /// <remarks>
        /// If the affine trasformation is T*R*S , the inverse (T*R*S)^-1 = (1/S) * (R^t) * (-T) 
        /// is X2 faster than calculate inverse
        /// </remarks>
        public static void InvertSRTMatrix(ref Matrix4 SRT)
        {
            // | aSx bSy  cSz  x |     | a/sx  d/sx  g/sx  -(ax+dy+gz)/sx |
            // | dSx eSy  fSz  y | ->  | b/sy  e/sy  h/sy  -(bx+ey+hz)/sy |
            // | gSx hSy  iSz  z |     | c/sz  f/sz  i/sz  -(cx+fy+iz)/sz |
            // | 0    0   0    1 |     |   0     0     0           1      |


            // if is orthogonal then a^2 + d^2 + g^2 = 1 (valid for both row and colum of Rotation matrix)
            // need to extract scale component
            float sx = (float)Math.Sqrt(SRT.m00 * SRT.m00 + SRT.m10 * SRT.m10 + SRT.m20 * SRT.m20);
            float sy = (float)Math.Sqrt(SRT.m01 * SRT.m01 + SRT.m11 * SRT.m11 + SRT.m21 * SRT.m21);
            float sz = (float)Math.Sqrt(SRT.m02 * SRT.m02 + SRT.m12 * SRT.m12 + SRT.m22 * SRT.m22);

            SRT.m00 /= sx; SRT.m10 /= sx; SRT.m20 /= sx;
            SRT.m01 /= sy; SRT.m11 /= sy; SRT.m21 /= sy;
            SRT.m02 /= sz; SRT.m12 /= sz; SRT.m22 /= sz;

            // do (RT)^-1
            InvertRTMatrix(ref SRT);

            // multiply by (S)^-1
            SRT.m00 /= sx; SRT.m01 /= sx; SRT.m02 /= sx; SRT.m03 /= sx;
            SRT.m10 /= sy; SRT.m11 /= sy; SRT.m12 /= sy; SRT.m13 /= sy;
            SRT.m20 /= sz; SRT.m21 /= sz; SRT.m22 /= sz; SRT.m23 /= sz;
        }

        /// <summary>
        /// World Inverse Traspose (WIT) is the world matrix used to transform correctly the normals.
        /// X7 time faster than WIT = Inverse(Traspose(World))
        /// </summary>
        /// <remarks>
        /// If the affine trasformation is T*R*S, we need to remove traslation because don't have effect to normals
        /// the inverse-traspose of ((R*S)^-1)^t = (S^-1 * R^-1)^t = R^-1^t * S^-1*t = R * (1/S)
        /// example if RS.m00 == a*Sx we need to obtain WIT.m00 = a/Sx = RS.m00 / Sx^2. Lucky Sx^2 is optain removing sqrt
        /// </remarks>
        public static void WorldInverseTraspose(ref Matrix4 SRT)
        {
            // remove traslation
            SRT.m03 = SRT.m13 = SRT.m23 = 0;
            // extract squared scale
            float sxsq = SRT.m00 * SRT.m00 + SRT.m10 * SRT.m10 + SRT.m20 * SRT.m20;
            float sysq = SRT.m01 * SRT.m01 + SRT.m11 * SRT.m11 + SRT.m21 * SRT.m21;
            float szsq = SRT.m02 * SRT.m02 + SRT.m12 * SRT.m12 + SRT.m22 * SRT.m22;
            // convert
            SRT.m00 /= sxsq; SRT.m01 /= sysq; SRT.m02 /= szsq; 
            SRT.m10 /= sxsq; SRT.m11 /= sysq; SRT.m12 /= szsq;
            SRT.m20 /= sxsq; SRT.m21 /= sysq; SRT.m22 /= szsq;
        }

        public Matrix4 WorldInverseTraspose()
        {
            Matrix4 wit = this;
            WorldInverseTraspose(ref wit);
            return wit;
        }

        /// <summary>
        /// Composed rotation for z y x axis
        /// TODO : make direclty in math notation
        /// </summary>
        /// <param name="roll">counterclockwise rotation about the x axis</param>
        /// <param name="pitch">counterclockwise rotation about the y axis</param>
        /// <param name="yaw">counterclockwise rotation about the z axis</param>
        /// <returns></returns>
        public static Matrix4 RotationYawPitchRoll(float yaw, float pitch, float roll)
        {
 
            Matrix4 matrix = Matrix4.Identity;

            float a = yaw;
            float b = pitch;
            float c = roll;

            matrix.m00 = (float)(Math.Cos(a) * Math.Cos(b));
            matrix.m01 = (float)(Math.Cos(a) * Math.Sin(b) * Math.Sin(c) - Math.Sin(a) * Math.Cos(c));
            matrix.m02 = (float)(Math.Cos(a) * Math.Sin(b) * Math.Cos(c) + Math.Sin(a) * Math.Sin(c));

            matrix.m10 = (float)(Math.Sin(a) * Math.Cos(b));
            matrix.m11 = (float)(Math.Sin(a) * Math.Sin(b) * Math.Sin(c) + Math.Cos(a) * Math.Cos(c));
            matrix.m12 = (float)(Math.Sin(a) * Math.Sin(b) * Math.Cos(c) - Math.Cos(a) * Math.Sin(c));

            matrix.m20 = (float)(-Math.Sin(b));
            matrix.m21 = (float)(Math.Cos(b) * Math.Sin(c));
            matrix.m22 = (float)(Math.Cos(b) * Math.Cos(c));

            matrix.Traspose();

            throw new NotImplementedException();
            return matrix;
        }

        /// <summary>
        /// Not Implemented
        /// </summary>
        public static Matrix4 Orthogonalize(Matrix4 matrix)
        {
            Vector3 axisX = new Vector3();
            Vector3 axisY = new Vector3();
            Vector3 axisZ = new Vector3();
            Vector3 scale = new Vector3(1, 1, 1);

            Matrix4 result = Matrix4.Identity;

            axisX.x = matrix.m00;
            axisX.y = matrix.m10;
            axisX.z = matrix.m20;
            scale.x = axisX.Length;


            axisY.x = matrix.m01;
            axisY.y = matrix.m11;
            axisY.z = matrix.m21;
            scale.y = axisY.Length;

            axisZ.x = matrix.m02;
            axisZ.y = matrix.m12;
            axisZ.z = matrix.m22;
            scale.z = axisZ.Length;

            axisY = Vector3.Cross(axisX, axisZ);
            axisZ = Vector3.Cross(axisX, axisY);

            axisX.Normalize();
            axisY.Normalize();
            axisZ.Normalize();

            result.m00 = axisX.x;
            result.m10 = axisX.y;
            result.m20 = axisX.z;
            result.m01 = axisY.x;
            result.m11 = axisY.y;
            result.m21 = axisY.z;
            result.m02 = axisZ.x;
            result.m12 = axisZ.y;
            result.m22 = axisZ.z;

            result.Position = matrix.Position;

            result *= Matrix4.Scaling(scale.x, scale.y, scale.z);

            return result;
        }
        #endregion


        #region Operations on itself to avoid new() constructor
       
        public void Traspose()
        {
            // diagonals are the same
            MathUtils.SWAP(ref m01, ref m10);
            MathUtils.SWAP(ref m02, ref m20);
            MathUtils.SWAP(ref m03, ref m30);
            MathUtils.SWAP(ref m12, ref m21);
            MathUtils.SWAP(ref m13, ref m31);
            MathUtils.SWAP(ref m23, ref m32);
        }
        public void Translate(float x,float y,float z)
        {
            m03 += x;
            m13 += y;
            m23 += z;
        }
        
        public void Multiply(float scalar)
        {
            m00 *= scalar; m01 *= scalar; m02 *= scalar; m03 *= scalar;
            m10 *= scalar; m11 *= scalar; m12 *= scalar; m13 *= scalar;
            m20 *= scalar; m21 *= scalar; m22 *= scalar; m23 *= scalar;
            m30 *= scalar; m31 *= scalar; m32 *= scalar; m33 *= scalar;
        }
        /// <summary>
        /// This * Right , i used reference to optain the maximum performance
        /// </summary>
        public void Multiply(ref Matrix4 right)
        {
            float a, b, c, d; // i used 4 temperany value without generate a new Matrix4 struct

            a = m00 * right.m00 + m01 * right.m10 + m02 * right.m20 + m03 * right.m30;
            b = m00 * right.m01 + m01 * right.m11 + m02 * right.m21 + m03 * right.m31;
            c = m00 * right.m02 + m01 * right.m12 + m02 * right.m22 + m03 * right.m32;
            d = m00 * right.m03 + m01 * right.m13 + m02 * right.m23 + m03 * right.m33;
            m00 = a; m01 = b; m02 = c; m03 = d;

            a = m10 * right.m00 + m11 * right.m10 + m12 * right.m20 + m13 * right.m30;
            b = m10 * right.m01 + m11 * right.m11 + m12 * right.m21 + m13 * right.m31;
            c = m10 * right.m02 + m11 * right.m12 + m12 * right.m22 + m13 * right.m32;
            d = m10 * right.m03 + m11 * right.m13 + m12 * right.m23 + m13 * right.m33;
            m10 = a; m11 = b; m12 = c; m13 = d;

            a = m20 * right.m00 + m21 * right.m10 + m22 * right.m20 + m23 * right.m30;
            b = m20 * right.m01 + m21 * right.m11 + m22 * right.m21 + m23 * right.m31;
            c = m20 * right.m02 + m21 * right.m12 + m22 * right.m22 + m23 * right.m32;
            d = m20 * right.m03 + m21 * right.m13 + m22 * right.m23 + m23 * right.m33;
            m20 = a; m21 = b; m22 = c; m23 = d;

            a = m30 * right.m00 + m31 * right.m10 + m32 * right.m20 + m33 * right.m30;
            b = m30 * right.m01 + m31 * right.m11 + m32 * right.m21 + m33 * right.m31;
            c = m30 * right.m02 + m31 * right.m12 + m32 * right.m22 + m33 * right.m32;
            d = m30 * right.m03 + m31 * right.m13 + m32 * right.m23 + m33 * right.m33;
            m30 = a; m31 = b; m32 = c; m33 = d;
        }

        /// <summary>
        /// Some time need to use a optimized multiplication for two or three matrices when calculation are intensive
        /// </summary>
        public static void Multiply(ref Matrix4 m0, ref Matrix4 m1 , out Matrix4 result)
        {
            result = new Matrix4();
            result.Multiply(ref m0);
            result.Multiply(ref m1);
        }
        /// <summary>
        /// Some time need to use a optimized multiplication for two or three matrices when calculation are intensive
        /// </summary>
        public static void Multiply(ref Matrix4 m0, ref Matrix4 m1, ref Matrix4 m2, out Matrix4 result)
        {
            result = new Matrix4();
            result.Multiply(ref m0);
            result.Multiply(ref m1);
            result.Multiply(ref m2);
        }

        /// <summary>
        /// Left * This
        /// </summary>
        public void PreMultiply(ref Matrix4 left)
        {
            float a, b, c, d; // i used 4 temperany value without generate a new Matrix4 struct

            a = left.m00 * m00 + left.m01 * m10 + left.m02 * m20 + left.m03 * m30;
            b = left.m10 * m00 + left.m11 * m10 + left.m12 * m20 + left.m13 * m30;
            c = left.m20 * m00 + left.m21 * m10 + left.m22 * m20 + left.m23 * m30;
            d = left.m30 * m00 + left.m31 * m10 + left.m32 * m20 + left.m33 * m30;
            m00 = a; m10 = b; m20 = c; m30 = d;

            a = left.m00 * m01 + left.m01 * m11 + left.m02 * m21 + left.m03 * m31;
            b = left.m10 * m01 + left.m11 * m11 + left.m12 * m21 + left.m13 * m31;
            c = left.m20 * m01 + left.m21 * m11 + left.m22 * m21 + left.m23 * m31;
            d = left.m30 * m01 + left.m31 * m11 + left.m32 * m21 + left.m33 * m31;
            m01 = a; m11 = b; m21 = c; m31 = d;

            a = left.m00 * m02 + left.m01 * m12 + left.m02 * m22 + left.m03 * m32;
            b = left.m10 * m02 + left.m11 * m12 + left.m12 * m22 + left.m13 * m32;
            c = left.m20 * m02 + left.m21 * m12 + left.m22 * m22 + left.m23 * m32;
            d = left.m30 * m02 + left.m31 * m12 + left.m32 * m22 + left.m33 * m32;
            m02 = a; m12 = b; m22 = c; m32 = d;

            a = left.m00 * m03 + left.m01 * m13 + left.m02 * m23 + left.m03 * m33;
            b = left.m10 * m03 + left.m11 * m13 + left.m12 * m23 + left.m13 * m33;
            c = left.m20 * m03 + left.m21 * m13 + left.m22 * m23 + left.m23 * m33;
            d = left.m30 * m03 + left.m31 * m13 + left.m32 * m23 + left.m33 * m33;
            m03 = a; m13 = b; m23 = c; m33 = d;

        }
        #endregion


        public static implicit operator Matrix4(Matrix3 matrix)
        {
            Matrix4 result = Matrix4.Identity;

            result.m00 = matrix.m00;
            result.m01 = matrix.m01;
            result.m02 = matrix.m02;
            result.m10 = matrix.m10;
            result.m11 = matrix.m11;
            result.m12 = matrix.m12;
            result.m20 = matrix.m20;
            result.m21 = matrix.m21;
            result.m22 = matrix.m22;

            return result;
        }
        public static implicit operator Matrix4(Quaternion q)
        {
            Matrix4 matrix = Matrix4.Identity;

            matrix.m00 = 1 - 2 * q.y * q.y - 2 * q.z * q.z;
            matrix.m01 = 2 * q.x * q.y - 2 * q.z * q.w;
            matrix.m02 = 2 * q.x * q.z + 2 * q.y * q.w;

            matrix.m10 = 2 * q.x * q.y + 2 * q.z * q.w;
            matrix.m11 = 1 - 2 * q.x * q.x - 2 * q.z * q.z;
            matrix.m12 = 2 * q.y * q.z - 2 * q.x * q.w;

            matrix.m20 = 2 * q.x * q.z - 2 * q.y * q.w;
            matrix.m21 = 2 * q.y * q.z + 2 * q.x * q.w;
            matrix.m22 = 1 - 2 * q.x * q.x - 2 * q.y * q.y;

            return matrix;
        }


        // for visual studio debugger visualization 
        public Vector4 _R0 { get { return getRow(0); } }
        public Vector4 _R1 { get { return getRow(1); } }
        public Vector4 _R2 { get { return getRow(2); } }
        public Vector4 _R3 { get { return getRow(3); } }


        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override string ToString()
        {
            StringBuilder str = new StringBuilder();
            for (int r = 0; r < 4; r++)
            {
                for (int c = 0; c < 4; c++)
                {
                    str.Append(String.Format("{0:0.000} ", this[r, c]));
                }
                str.Append('\n');
            }
            return str.ToString();
        }

    }
}