#region LGPL License

/*
Axiom Graphics Engine Library
Copyright � 2003-2011 Axiom Project Team

The overall design, and a majority of the core engine and rendering code 
contained within this library is a derivative of the open source Object Oriented 
Graphics Engine OGRE, which can be found at http://ogre.sourceforge.net.  
Many thanks to the OGRE team for maintaining such a high quality project.

The math library included in this project, in addition to being a derivative of
the works of Ogre, also include derivative work of the free portion of the 
Wild Magic mathematics source code that is distributed with the excellent
book Game Engine Design.
http://www.wild-magic.com/

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#endregion

using System;
using System.Text;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Drawing;

namespace Engine.Maths
{
    /// <summary>
    /// 4D homogeneous vector.
    /// </summary>
    /// <remarks>
    /// A 3d point can be converted in vector4 with w=1, but if is a Normal need to set w=0 to avoid traslation operations
    /// A 4 channel color use X=R , Y=G , Z=B , W=A , in 0.0f-1.0f range format to respect shader language
    /// </remarks>
    [StructLayout(LayoutKind.Explicit)]
    public struct Vector4
    {
        [FieldOffset(0)]
        public float x;
        [FieldOffset(4)]
        public float y;
        [FieldOffset(8)]
        public float z;
        [FieldOffset(12)]
        public float w;
        [FieldOffset(0)]
        public unsafe fixed float Dim[4];

        public const int sizeinbyte = sizeof(float) * 4;

        static float range01(float value)
        {
            if (value > 1) return 1;
            else if (value < 0) return 0;
            else return value;
        }

        /// <summary>
        /// 1 1 1 1
        /// </summary>
        public static readonly Vector4 White = new Vector4(1.0f, 1.0f, 1.0f, 1.0f);
        /// <summary>
        /// 0 0 0 1
        /// </summary>
        public static readonly Vector4 Black = new Vector4(0.0f, 0.0f, 0.0f, 1.0f);
        /// <summary>
        /// 0 0 0 0
        /// </summary>
        public static readonly Vector4 Zero = new Vector4(0.0f, 0.0f, 0.0f, 0.0f);
        /// <summary>
        /// 1 1 1 1
        /// </summary>
        public static readonly Vector4 One = White;

        /// <summary>
        /// Use vector4 as 32bit color, the value are scaled to 0-1
        /// </summary>
        public Vector4(byte r, byte g, byte b, byte a)
        {
            float inv = 1.0f / 255.0f;
            this.x = r * inv;
            this.y = g * inv;
            this.z = b * inv;
            this.w = a * inv;
        }

        public Vector4(float x, float y, float z, float w)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            this.w = w;
        }
        public Vector4(Vector3 v, float w)
        {
            this.x = v.x;
            this.y = v.y;
            this.z = v.z;
            this.w = w;
        }



        public static float Dot(Vector4 left, Vector4 right)
        {
            return left.x * right.x + left.y * right.y + left.z * right.z + left.w * right.w;
        }


        /// <summary>
        ///     1  0  0  Tx    x
        ///     0  1  0  Ty *  y
        ///     0  0  1  Tz    z
        ///     0  0  0  1     w
        /// </summary>
        public static Vector4 operator *(Matrix4 matrix, Vector4 vector)
        {
            Vector4 result = new Vector4();

            result.x = vector.x * matrix.m00 + vector.y * matrix.m01 + vector.z * matrix.m02 + vector.w * matrix.m03;
            result.y = vector.x * matrix.m10 + vector.y * matrix.m11 + vector.z * matrix.m12 + vector.w * matrix.m13;
            result.z = vector.x * matrix.m20 + vector.y * matrix.m21 + vector.z * matrix.m22 + vector.w * matrix.m23;
            result.w = vector.x * matrix.m30 + vector.y * matrix.m31 + vector.z * matrix.m32 + vector.w * matrix.m33;

            return result;
        }
        public static Vector4 operator *(Vector4 vector, Matrix4 matrix)
        {
            Vector4 result = new Vector4();

            result.x = vector.x * matrix.m00 + vector.y * matrix.m10 + vector.z * matrix.m20 + vector.w * matrix.m30;
            result.y = vector.x * matrix.m01 + vector.y * matrix.m11 + vector.z * matrix.m21 + vector.w * matrix.m31;
            result.z = vector.x * matrix.m02 + vector.y * matrix.m12 + vector.z * matrix.m22 + vector.w * matrix.m32;
            result.w = vector.x * matrix.m03 + vector.y * matrix.m13 + vector.z * matrix.m23 + vector.w * matrix.m33;

            return result;
        }
        public static Vector4 operator *(Vector4 vector, float scalar)
        {
            Vector4 result = new Vector4();

            result.x = vector.x * scalar;
            result.y = vector.y * scalar;
            result.z = vector.z * scalar;
            result.w = vector.w * scalar;

            return result;
        }
        public static Vector4 operator /(Vector4 vector, float scalar)
        {
            return vector * (1.0f / scalar);
        }
        public static Vector4 operator +(Vector4 left, Vector4 right)
        {
            return new Vector4(left.x + right.x, left.y + right.y, left.z + right.z, left.w + right.w);
        }
        public static Vector4 operator -(Vector4 left, Vector4 right)
        {
            return new Vector4(left.x - right.x, left.y - right.y, left.z - right.z, left.w - right.w);
        }
        public static Vector4 operator -(Vector4 left)
        {
            return new Vector4(-left.x, -left.y, -left.z, -left.w);
        }

        /// <summary>
        /// Add 1 to w component
        /// </summary>
        public static implicit operator Vector4(Vector3 vector)
        {
            return new Vector4(vector.x, vector.y, vector.z, 1);
        }
        public static implicit operator Vector4(Color color)
        {
            return new Vector4(color.R / 255.0f, color.G / 255.0f, color.B / 255.0f, color.A / 255.0f);
        }
        public static implicit operator Color32(Vector4 color)
        {
            return new Color32(
                range01(color.x),
                range01(color.y),
                range01(color.z),
                range01(color.w));
        }
        /// <summary>
        ///		Used to access a Vector by index 0 = this.x, 1 = this.y, 2 = this.z, 3 = this.w.  
        /// </summary>
        /// <remarks>
        ///		Uses unsafe pointer arithmetic to reduce the code required.
        ///	</remarks>
        public float this[int index]
        {
            get
            {
                Debug.Assert(index >= 0 && index < 4, "Indexer boundaries overrun in Vector4.");
                // using pointer arithmetic here for less code.  Otherwise, we'd have a big switch statement.
#if !UNSAFE
                switch (index)
                {
                    case 0: return x;
                    case 1: return y;
                    case 2: return z;
                    case 3: return w;
                }
                return 0;
#else
				unsafe
				{
					fixed ( float* pX = &this.x )
					{
						return *( pX + index );
					}
				}
#endif
            }
            set
            {
                Debug.Assert(index >= 0 && index < 4, "Indexer boundaries overrun in Vector4.");

                // using pointer arithmetic here for less code.  Otherwise, we'd have a big switch statement.
#if !UNSAFE
                switch (index)
                {
                    case 0: x = value; break;
                    case 1: y = value; break;
                    case 2: z = value; break;
                    case 3: w = value; break;
                }
#else
				unsafe
				{
                    fixed (float* pX = &this.x)
					{
						*( pX + index ) = value;
					}
				}
#endif
            }
        }

        /// <summary>
        /// Get the squared legth
        /// </summary>
        public float LengthSq
        {
            get { return x * x + y * y + z * z + w * w; }
        }
        /// <summary>
        /// Get the length
        /// </summary>
        public float Length
        {
            get { return (float)Math.Sqrt(LengthSq); }
        }

        /// <summary>
        /// Normalize the vector and get the calculated length. 
        /// </summary>
        public float Normalize()
        {
            float length = Length;
            if (length > float.Epsilon)
            {
                x /= length;
                y /= length;
                z /= length;
                w /= length;
            }
            return length;
        }
        /// <summary>
        /// Get a new normalized vector3 copy
        /// </summary>
        public Vector4 Normal
        {
            get
            {
                Vector4 vect = new Vector4(x, y, z, w);
                vect.Normalize();
                return vect;
            }
        }
        public override string ToString()
        {
            return string.Format("{0,4} {1,4} {2,4} {3,4} ", x, y, z, w);
        }
    }
}