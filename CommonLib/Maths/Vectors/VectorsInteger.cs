using System;
using System.Text;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Drawing;

namespace Engine.Maths
{
    /// <summary>
    /// Shorts items
    /// </summary>
    [StructLayout(LayoutKind.Explicit)]
    public struct Vector2S
    {
        [FieldOffset(0)]
        public short x;
        [FieldOffset(2)]
        public short y;

        /// <summary>
        /// </summary>
        public Vector2S(short x, short y)
        {
            this.x = x;
            this.y = y;
        }

        public Vector2S(int x, int y) :
            this((short)x, (short)y)
        { }

        public Vector2S(Point point) :
            this(point.X, point.Y)
        { }


        public short this[int index]
        {
            get { return index == 0 ? x : y; }
            set { if (index == 0) x = value; else y = value; }
        }

        public static implicit operator Vector2S(Point point)
        {
            return new Vector2S(point.X, point.Y);
        }
        public static implicit operator Point(Vector2S vector)
        {
            return new Point(vector.x, vector.y);
        }

        public static Vector2 operator +(Vector2 left, Vector2S right)
        {
            return new Vector2(left.x + right.x, left.y + right.y);
        }
        public static Vector2 operator +(Vector2S left, Vector2 right)
        {
            return right + left;
        }
        public static Vector2 operator *(Vector2 left, Vector2S right)
        {
            return new Vector2(left.x * right.x, left.y * right.y);
        }
        public static Vector2 operator *(Vector2S left, Vector2 right)
        {
            return right * left;
        }

        public override string ToString()
        {
            return string.Format("{0} {1}", x, y);
        }
    }

    /// <summary>
    /// Ushorts items
    /// </summary>
    [StructLayout(LayoutKind.Explicit)]
    public struct Vector2US
    {
        [FieldOffset(0)]
        public ushort x;
        [FieldOffset(2)]
        public ushort y;

        /// <summary>
        /// </summary>
        public Vector2US(ushort x, ushort y)
        {
            this.x = x;
            this.y = y;
        }

        public Vector2US(int x, int y) : 
            this((ushort)x, (ushort)y)
        { }

        public ushort this[int index]
        {
            get { return index == 0 ? x : y; }
            set { if (index == 0) x = value; else y = value; }
        }

        public static implicit operator Point(Vector2US vector)
        {
            return new Point(vector.x, vector.y);
        }

        public static Vector2 operator +(Vector2 left, Vector2US right)
        {
            return new Vector2(left.x + right.x, left.y + right.y);
        }
        public static Vector2 operator +(Vector2US left, Vector2 right)
        {
            return right + left;
        }
        public static Vector2 operator *(Vector2 left, Vector2US right)
        {
            return new Vector2(left.x * right.x, left.y * right.y);
        }
        public static Vector2 operator *(Vector2US left, Vector2 right)
        {
            return right * left;
        }

        public override string ToString()
        {
            return string.Format("{0} {1}", x, y);
        }
    }

    /// <summary>
    /// Integers items
    /// </summary>
    [StructLayout(LayoutKind.Explicit)]
    public struct Vector2I
    {
        [FieldOffset(0)]
        public int x;
        [FieldOffset(4)]
        public int y;

        /// <summary>
        /// </summary>
        public Vector2I(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public int this[int index]
        {
            get { return index == 0 ? x : y; }
            set { if (index == 0) x = value; else y = value; }
        }


        /// <summary>
        /// Convert a rectangle to a 2d rectangle where X axis is same and Y inverted
        /// </summary>
        /// <param name="Client">the screen bound in pixel size</param>
        /// <param name="World">the screen bound in 2d coordinates</param>
        public Vector2 ConvertToWorld(RectangleUV Client, RectangleAA World)
        {
            float xf = MathUtils.Interpolate(x, World.min.x, World.max.x, Client.Left, Client.Right);
            float yf = MathUtils.Interpolate(y, World.min.y, World.max.y, Client.Bottom, Client.Top);
            return new Vector2(xf, yf);
        }


        public static implicit operator Vector2I(Point point)
        {
            return new Vector2I(point.X, point.Y);
        }
        public static implicit operator Point(Vector2I point)
        {
            return new Point(point.x, point.y);
        }

        public override string ToString()
        {
            return string.Format("{0} {1}", x, y);
        }
    }

    
    /// <summary>
    /// UInt32 items
    /// </summary>
    [StructLayout(LayoutKind.Explicit)]
    public struct Vector2UI
    {
        [FieldOffset(0)]
        public uint x;
        [FieldOffset(4)]
        public uint y;

        /// <summary>
        /// </summary>
        public Vector2UI(uint x, uint y)
        {
            this.x = x;
            this.y = y;
        }

        public uint this[int index]
        {
            get { return index == 0 ? x : y; }
            set { if (index == 0) x = value; else y = value; }
        }

        public override string ToString()
        {
            return string.Format("{0} {1}", x, y);
        }
    }

    /// <summary>
    /// Uint32 items
    /// </summary>
    [StructLayout(LayoutKind.Explicit)]
    public struct Vector3UI
    {
        [FieldOffset(0)]
        public uint x;
        [FieldOffset(4)]
        public uint y;
        [FieldOffset(8)]
        public uint z;

        /// <summary>
        /// </summary>
        public Vector3UI(uint x, uint y, uint z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }
        /// <summary>
        /// </summary>
        public Vector3UI(int x, int y,int z)
        {
            this.x = (uint)x;
            this.y = (uint)y;
            this.z = (uint)z;
        }

        public static Vector3UI operator +(Vector3UI left, Vector3UI right)
        {
            unchecked { return new Vector3UI(left.x + right.x, left.y + right.y, left.z + right.z); }
        }
        public static Vector3UI operator /(Vector3UI left, uint scalar)
        {
            unchecked { return new Vector3UI(left.x / scalar, left.y / scalar, left.z / scalar); }
        }
        public override string ToString()
        {
            return string.Format("{0} {1}", x, y);
        }
    }
    
    /// <summary>
    /// Int32 items
    /// </summary>
    [StructLayout(LayoutKind.Explicit)]
    public struct Vecto3I
    {
        [FieldOffset(0)]
        public int x;
        [FieldOffset(4)]
        public int y;
        [FieldOffset(8)]
        public int z;

        /// <summary>
        /// </summary>
        public Vecto3I(int x, int y, int z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }
        public Vecto3I(float x, float y, float z) :
            this((int)x, (int)y, (int)z)
        { }


        public static Vecto3I operator +(Vecto3I left, Vecto3I right)
        {
            unchecked { return new Vecto3I(left.x + right.x, left.y + right.y, left.z + right.z); }
        }
        public static Vecto3I operator /(Vecto3I left, float scalar)
        {
            unchecked { return new Vecto3I(left.x / scalar, left.y / scalar, left.z / scalar); }
        }
        public override string ToString()
        {
            return string.Format("{0} {1} {2}", x, y, z);
        }
    }

    /// <summary>
    /// Unsinged Bytes items
    /// </summary>
    [StructLayout(LayoutKind.Explicit)]
    public struct Vector4B
    {
        [FieldOffset(0)]
        public byte x;
        [FieldOffset(1)]
        public byte y;
        [FieldOffset(2)]
        public byte z;
        [FieldOffset(3)]
        public byte w;


        /// <summary>
        /// </summary>
        public Vector4B(byte x, byte y, byte z, byte w)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            this.w  =w;
        }

        public override string ToString()
        {
            return string.Format("{0} {1} {2} {3}", x, y, z,w);
        }
    }
}