﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Engine.Maths
{
    /// <summary>
    /// 16bit unit vector
    /// from http://oroboro.com/compressed-unit-vectors/
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 2, Size = sizeof(ushort))]
    public struct Normal16
    {
        ushort mVec;

        static float[] mUVAdjustment = initializeStatics();

        // upper 3 bits
        const ushort SIGN_MASK = 0xe000;
        const ushort XSIGN_MASK = 0x8000;
        const ushort YSIGN_MASK = 0x4000;
        const ushort ZSIGN_MASK = 0x2000;
        // middle 6 bits - xbits
        const ushort TOP_MASK = 0x1f80;
        // lower 7 bits - ybits
        const ushort BOTTOM_MASK = 0x007f;


        public Normal16(Vector3 normal) : this(normal.x, normal.y, normal.z) { }

        public Normal16(float nx, float ny, float nz)
        {
            mVec = 0;
            packVector(nx, ny, nz);
        }

        ushort clamp127(float f)
        {
            if (f >= 127) return 127;
            else if (f <= 0) return 0;
            else return (ushort)f;
        }


        void packVector(float vecx, float vecy, float vecz)
        {
            // input vector does not have to be unit length
            // assert( tmp.length() <= 1.001f );      
            mVec = 0;

            if (vecx < 0) { mVec |= XSIGN_MASK; vecx = -vecx; }
            if (vecy < 0) { mVec |= YSIGN_MASK; vecy = -vecy; }
            if (vecz < 0) { mVec |= ZSIGN_MASK; vecz = -vecz; }

            // project the normal onto the plane that goes through
            // X0=(1,0,0),Y0=(0,1,0),Z0=(0,0,1).

            // on that plane we choose an (projective!) coordinate system
            // such that X0->(0,0), Y0->(126,0), Z0->(0,126),(0,0,0)->Infinity

            // a little slower... old pack was 4 multiplies and 2 adds. 
            // This is 2 multiplies, 2 adds, and a divide....
            float w = 126.0f / (vecx + vecy + vecz);
            ushort xbits = clamp127(vecx * w);
            ushort ybits = clamp127(vecy * w);

            // Now we can be sure that 0<=xp<=126, 0<=yp<=126, 0<=xp+yp<=126

            // however for the sampling we want to transform this triangle 
            // into a rectangle.
            if (xbits >= 64)
            {
                xbits = (ushort)(127 - xbits);
                ybits = (ushort)(127 - ybits);
            }

            // now we that have xp in the range (0,127) and yp in the 
            // range (0,63), we can pack all the bits together
            mVec |= (ushort)(xbits << 7);
            mVec |= (ushort)ybits;
        }

        void unpackVector(out float vecx, out float vecy, out float vecz)
        {

            // if we do a straightforward backward transform
            // we will get points on the plane X0,Y0,Z0
            // however we need points on a sphere that goes through 
            // these points.
            // therefore we need to adjust x,y,z so that x^2+y^2+z^2=1

            // by normalizing the vector. We have already precalculated 
            // the amount by which we need to scale, so all we do is a 
            // table lookup and a multiplication

            // get the x and y bits
            long xbits = ((mVec & TOP_MASK) >> 7);
            long ybits = (mVec & BOTTOM_MASK);

            // map the numbers back to the triangle (0,0)-(0,126)-(126,0)
            if ((xbits + ybits) >= 127)
            {
                xbits = 127 - xbits;
                ybits = 127 - ybits;
            }

            // do the inverse transform and normalization
            // costs 3 extra multiplies and 2 subtracts. No big deal.         
            float uvadj = mUVAdjustment[mVec & ~SIGN_MASK];

            vecx = uvadj * (float)xbits;
            vecy = uvadj * (float)ybits;
            vecz = uvadj * (float)(126 - xbits - ybits);


            // set all the sign bits
            if ((mVec & XSIGN_MASK) != 0) vecx = -vecx;
            if ((mVec & YSIGN_MASK) != 0) vecy = -vecy;
            if ((mVec & ZSIGN_MASK) != 0) vecz = -vecz;

        }

        static float[] initializeStatics()
        {
            float[] uvadjust = new float[0x2000];

            for (int idx = 0; idx < 0x2000; idx++)
            {
                long xbits = idx >> 7;
                long ybits = idx & BOTTOM_MASK;

                // map the numbers back to the triangle (0,0)-(0,127)-(127,0)
                if ((xbits + ybits) >= 127)
                {
                    xbits = 127 - xbits;
                    ybits = 127 - ybits;
                }

                // convert to 3D vectors
                float x = (float)xbits;
                float y = (float)ybits;
                float z = (float)(126 - xbits - ybits);

                // calculate the amount of normalization required
                uvadjust[idx] = 1.0f / (float)Math.Sqrt(y * y + z * z + x * x);
            }
            return uvadjust;
        }

        public static explicit operator Vector3(Normal16 norm)
        {
            float x, y, z;
            norm.unpackVector(out x, out y, out z);
            return new Vector3(x, y, z);
        }
        /// <summary>
        /// </summary>
        /// <param name="norm">ensure it's normalize</param>
        public static explicit operator Normal16(Vector3 norm)
        {
            return new Normal16(norm);
        }

        public override string ToString()
        {
            return ((Vector3)this).ToString();
        }
    }

    /// <summary>
    /// 24bit unit vector,
    /// encode normals to 3 bytes float will have +-0.0078125f precision,
    /// used to convert in rgb color components but tested precision if very bad
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 1, Size = sizeof(byte) * 3)]
    public struct Normal24
    {
        // are coverted from [-1,1] range to [0,255]
        public byte r, g, b;

        /// <summary>
        /// From normalize coordinates in [-1,1] range
        /// </summary>
        public Normal24(Vector3 normal) :
            this(normal.x, normal.y, normal.z) { }

        public Normal24(float nx, float ny, float nz)
        {
            r = g = b = 0;
            r = encode(nx);
            g = encode(ny);
            b = encode(nz);
        }

        public float nx { get { return decode(r); } set { r = encode(value); } }
        public float ny { get { return decode(g); } set { g = encode(value); } }
        public float nz { get { return decode(b); } set { b = encode(value); } }

        public void Normalize()
        {
            float x = r - 128;
            float y = g - 128;
            float z = b - 128;
            float lenght = (float)Math.Sqrt(x * x + y * y + z * z);
            r = clamp255(x / lenght);
            g = clamp255(y / lenght);
            b = clamp255(z / lenght);
        }

        /// <summary> ensure float is in 0-255 range before casting in byte, example if f = 255.1f without clamp return 1</summary>
        byte clamp255(float i)
        {
            if (i > 255) return 255;
            else if (i < 0) return 0;
            else return (byte)i;
        }
        byte encode(float i)
        {
            return clamp255((i + 1.0f) * 128.0f);
        }

        float decode(byte i)
        {
            return (i - 128) / 128.0f;
        }

        public static explicit operator Vector3(Normal24 norm)
        {
            return new Vector3(norm.nx, norm.ny, norm.nz);
        }
        /// <summary>
        /// </summary>
        /// <param name="norm">ensure it's normalize</param>
        public static explicit operator Normal24(Vector3 norm)
        {
            return new Normal24(norm);
        }

        public override string ToString()
        {
            return ((Vector3)this).ToString();
        }
    }

    /// <summary>
    /// 32bit unit vector, encode normals to uint32
    /// from http://www.geometrictools.com/Documentation/CompressedUnitVectors.pdf
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 1, Size = sizeof(byte) * 3)]
    public struct Normal32
    {
        public uint value;

        /// <summary>
        /// From normalize coordinates in [-1,1] range
        /// </summary>
        public Normal32(Vector3 normal) :
            this(normal.x, normal.y, normal.z) { }

        public Normal32(float nx, float ny, float nz)
        {
            value = 0;
            pack(nx, ny, nz);
        }

        public Vector3 Value
        {
            get
            {
                float x, y, z;
                unpack(out x, out  y, out  z);
                return new Vector3(x, y, z);
            }
            set
            {
                pack(value.x, value.y, value.z);
            }
        }

        void pack(float x, float y, float z)
        {
            uint xc = (uint)(1023.0f * (0.5f * (x + 1.0f)));
            uint yc = (uint)(1023.0f * (0.5f * (y + 1.0f)));
            uint zc = (uint)(1023.0f * (0.5f * (z + 1.0f)));
            value = xc | (yc << 10) | (zc << 20);
        }

        void unpack(out float x, out float y, out float z)
        {
            uint xu = (value & 0x000003FF);
            uint yu = (value & 0x000FFC00) >> 10;
            uint zu = (value & 0x3FF00000) >> 20;
            x = 2.0f * xu / 1023.0f - 1.0f;
            y = 2.0f * yu / 1023.0f - 1.0f;
            z = 2.0f * zu / 1023.0f - 1.0f;
            float invLength = 1.0f / (float)Math.Sqrt(x * x + y * y + z * z);
            x *= invLength;
            y *= invLength;
            z *= invLength;
        }

        public static explicit operator Vector3(Normal32 norm)
        {
            return norm.Value;
        }
        /// <summary>
        /// </summary>
        /// <param name="norm">ensure it's normalize</param>
        public static explicit operator Normal32(Vector3 norm)
        {
            return new Normal32(norm);
        }

        public override string ToString()
        {
            return ((Vector3)this).ToString();
        }
    }
}
