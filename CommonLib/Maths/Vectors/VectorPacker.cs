﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace Engine.Maths
{

    /// <summary>
    /// Vector packer tool try to get the best compromise between accuracy and size of vectors
    /// </summary>
    public static class VectorPacker
    {
        const uint n15x15y_wsign_mask = 0x80000000; // 10000000 00000000 00000000 00000000
        const uint n15x15y_zsign_mask = 0x00008000; // 00000000 00000000 10000000 00000000
        const uint n15x15y_mask = 0x00007FFF;       // 00000000 00000000 01111111 11111111

        static uint clamp(uint value, uint min, uint max)
        {
            return value < min ? min : value > max ? max : value;
        }

        /// <summary>
        /// Used in SpaceEngineersGame for normal vector, set 1bit:sign_z ,15bit x value, 1bit sign_w , 15bit y value
        /// z value are calculated from x y
        /// w can be 1 or -1
        /// </summary>
        public static UInt32 Normalized_15X15Y(float x, float y, float z, float w = 1)
        {
            UInt32 packed = 0;
            if (z > 0) packed |= n15x15y_zsign_mask;
            if (w > 0) packed |= n15x15y_wsign_mask;

            // convert [-1.0f,1.0f] to to [0,32767]
            x = (x + 1) * 0.5f * 32767.0f;
            y = (y + 1) * 0.5f * 32767.0f;

            // fix
            uint _x = clamp((uint)x, 0, 32767);
            uint _y = clamp((uint)y, 0, 32767);

            // instead write first short x and second short y in file, i write integer.
            // when i read uint32 with LittleEndian _x become the smaller value, _y greater
            return packed | (_y << 16) | _x;
        }
        /// <summary>
        /// Used in SpaceEngineersGame, set 1bit:sign_z ,15bit x value, 1bit sign_w , 15bit y value
        /// z value are calculated from x y
        /// w value can be 1 or -1
        /// </summary>
        public static void Normalized_15X15Y(UInt32 packed, out float x, out float y, out float z, out float w)
        {
            // convert [0,32767] to [0.0f,1.0f]
            x = (packed & n15x15y_mask) / 32767.0f;
            y = ((packed >> 16) & n15x15y_mask) / 32767.0f;

            // convert [0.0f,1.0f] to [-1.0f,1.0f]
            x = 2 * x - 1;
            y = 2 * y - 1;
            
            // compute z , example if x or y = 1 , magnitude is ~ 0 and sqrt return NaN
            z = 1 - x * x - y * y;
            z = z > float.Epsilon ? (float)Math.Sqrt(z) : 0.0f; 

            if ((packed & n15x15y_zsign_mask) == 0) z *= -1;
            w = 1.0f;
            if ((packed & n15x15y_wsign_mask) == 0) w *= -1;
        }
        /// <summary>
        /// <seealso cref="VectorPacker.Normalized_15X15Y(UInt32,out float,out float,out float,out float)"/>
        /// </summary>
        public static Vector3 Normalized_15X15Y(UInt32 packed)
        {
            Vector3 vector = Vector3.Zero;
            float w;
            Normalized_15X15Y(packed, out vector.x, out vector.y, out vector.z, out w);
            return vector;
        }
    }
}
