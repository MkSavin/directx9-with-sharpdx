﻿#region LGPL License

/*
Axiom Graphics Engine Library
Copyright © 2003-2011 Axiom Project Team

The overall design, and a majority of the core engine and rendering code 
contained within this library is a derivative of the open source Object Oriented 
Graphics Engine OGRE, which can be found at http://ogre.sourceforge.net.  
Many thanks to the OGRE team for maintaining such a high quality project.

The math library included in this project, in addition to being a derivative of
the works of Ogre, also include derivative work of the free portion of the 
Wild Magic mathematics source code that is distributed with the excellent
book Game Engine Design.
http://www.wild-magic.com/

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#endregion

using System;
using System.Text;
using System.Runtime.InteropServices;
using System.Drawing;



namespace Engine.Maths
{
    [StructLayout(LayoutKind.Explicit)]
    public struct Vector2
    {
        [FieldOffset(0)]
        public float x;
        [FieldOffset(4)]
        public float y;
        [FieldOffset(0)]
        public unsafe fixed float Dim[2];


        public const int sizeinbyte = sizeof(float) * 2;

        public float u
        {
            get { return x; }
            set { x = value; }
        }
        public float v
        {
            get { return y; }
            set { y = value; }
        }

        /// <summary>
        /// In a texture notation : u = x , v = y 
        /// </summary>
        public Vector2(float u, float v)
        {
            this.x = u;
            this.y = v;
        }
        public Vector2(double u, double v)
            : this((float)u, (float)v) { }


        public float this[int i]
        {
#if UNSAFE
            get
            {
                unsafe
                {
                    fixed (float* pX = &this.x)
                    {
                        return *(pX + i);
                    }
                }
            }
             
            set
            {
                unsafe
                {
                    fixed (float* pX = &this.x)
                    {
                        *(pX + i) = value;
                    }
                }
            }
#else
            get
            {
                switch (i)
                {
                    case 0: return x;
                    case 1: return y;
                    default: throw new ArgumentException("i must be 0,1,2");
                }
            }
            set 
            {
                switch (i)
                {
                    case 0: x = value; break;
                    case 1: y = value; break;
                    default: throw new ArgumentException("i must be 0,1");
                }   
            }
#endif
        }

        public float this[eAxis axe]
        {
            get
            {
                switch (axe)
                {
                    case eAxis.X: return x;
                    case eAxis.Y: return y;
                    default: throw new ArgumentException("Wrong Axe");
                }
            }
            set
            {
                switch (axe)
                {
                    case eAxis.X: x = value; break;
                    case eAxis.Y: y = value; break;
                    default: throw new ArgumentException("Wrong Axe");
                }
            }
        }


        public override string ToString()
        {
            return (String.Format("{0,4} {1,4} ", x,y));
        }

        public bool IsNaN
        {
            get
            {
                return float.IsInfinity(x) || float.IsNaN(x) ||
                       float.IsInfinity(y) || float.IsNaN(y);
            }
        }
        public static readonly Vector2 PosInf = new Vector2(float.PositiveInfinity, float.PositiveInfinity);
        public static readonly Vector2 NegInf = new Vector2(float.NegativeInfinity, float.NegativeInfinity);
        public static readonly Vector2 NaN = new Vector2(float.NaN, float.NaN);
        public static readonly Vector2 Zero = new Vector2(0, 0);
        public static readonly Vector2 One = new Vector2(1, 1);
        public static readonly Vector2 UnitX = new Vector2(1, 0);
        public static readonly Vector2 UnitY = new Vector2(0, 1);

        #region operator overload
        public static Vector2 operator /(Vector2 left, float scalar)
        {
            if (scalar <= float.Epsilon) throw new ArgumentException("Cannot divide a Vector2 by zero");
            Vector2 vector;
            var inverse = 1.0f / scalar;
            vector.x = left.x * inverse;
            vector.y = left.y * inverse;
            return vector;
        }
        public static Vector2 operator +(Vector2 left, Vector2 right)
        {
            return new Vector2(left.x + right.x, left.y + right.y);
        }
        public static Vector2 operator +(Vector2 v, float scalar)
        {
            return new Vector2(v.x + scalar, v.y + scalar);
        }
        public static Vector2 operator -(Vector2 v, float scalar)
        {
            return new Vector2(v.x - scalar, v.y - scalar);
        }


        public static Vector2 operator *(Vector2 left, float scalar)
        {
            Vector2 retVal;
            retVal.x = left.x * scalar;
            retVal.y = left.y * scalar;
            return retVal;
        }
        public static Vector2 operator *(float scalar, Vector2 right)
        {
            return right * scalar;
        }
        public static Vector2 operator -(Vector2 left, Vector2 right)
        {
            return new Vector2(left.x - right.x, left.y - right.y);
        }
        public static Vector2 operator -(Vector2 left)
        {
            return new Vector2(-left.x, -left.y);
        }
        /// <summary>
        /// is not a dot product, simple x0*x1, y0*y1
        /// </summary>
        public static Vector2 operator *(Vector2 left, Vector2 right)
        {
            return new Vector2(left.x * right.x, left.y * right.y);
        }


        #endregion

        /// <summary>
        /// <seealso cref="GetLengthSquared"/>
        /// </summary>
        public float LengthSq
        {
            get { return x * x + y * y; }
        }
        /// <summary>
        /// <seealso cref="GetLength"/>
        /// </summary>
        public float Length
        {
            get { return (float)Math.Sqrt(LengthSq); }
        }
        /// <summary>
        /// sqrt(x*x + y*y + z*z)
        /// </summary>
        public static float GetLength(Vector2 vector)
        {
            return vector.Length;
        }
        /// <summary>
        /// x*x + y*y + z*z
        /// </summary>
        public static float GetLengthSquared(Vector2 vector)
        {
            return vector.LengthSq;
        }
        /// <summary>
        /// |x| + |y| + |z|
        /// </summary>
        public static float GetManhattanLength(Vector2 vector)
        {
            return MathUtils.ABS(vector.x) + MathUtils.ABS(vector.y);
        }
        /// <summary>
        /// Normalize the vector and get the calculated length. 
        /// </summary>
        public float Normalize()
        {
            float length = Length;

            if (length > float.Epsilon)
            {
                x /= length;
                y /= length;
            }
            return length;
        }
        /// <summary>
        /// ax * bx + ay * by
        /// </summary>
        public static float Dot(Vector2 a, Vector2 b)
        {
            return a.x * b.x + a.y * b.y;
        }

        /// <summary>
        /// is a.x * b.y - a.y * b.x, is the magnitude of vector c perpendicular to plane that lies in a b.
        /// </summary>
        /// <remarks>
        /// the formula was derived from 3d:
        /// a = Vector3(a.x,a.y,0) 
        /// b = Vector3(b.x,b.y,0)
        /// c = Vector3(0,0, a.x*b.y - a.y*b.x)
        /// 
        /// notice that is used to find the orientation of a vector:
        /// where a = p1-p0 and b = p2-p1 , the cross is positive on top, negative on bottom
        /// 
        ///            +cross
        ///     p0------------>p1
        ///            -cross    \
        ///                       \p2
        /// </remarks>
        public static float Cross(Vector2 a, Vector2 b)
        {
            return a.x * b.y - a.y * b.x;
        }
        /// <summary>
        /// <seealso cref="Normalize"/>
        /// </summary>
        public Vector2 Normal
        {
            get
            {

                Vector2 vect = new Vector2(x,y);
                vect.Normalize();
                return vect;
            }
        }
        /// <summary>
        /// <seealso cref="Normal"/>
        /// </summary>
        public static Vector2 GetNormal(Vector2 vector) 
        { 
            return vector.Normal; 
        }
        /// <summary>
        /// x XOR y
        /// </summary>
        public override int GetHashCode()
        {
            return x.GetHashCode() ^ y.GetHashCode();
        }

        /// <summary>
        /// Convert a 2d rectangle to a screen rectangle where X axis is same and Y inverted
        /// </summary>
        /// <param name="Client">the screen bound in pixel size</param>
        /// <param name="World">the screen bound in 2d coordinates</param>
        public Vector2I ConvertToScreen(RectangleUV Client, RectangleAA World)
        {
            int xi = (int)MathUtils.Interpolate(x, Client.Left, Client.Right, World.min.x, World.max.x);
            int yi = (int)MathUtils.Interpolate(y, Client.Bottom, Client.Top, World.min.y, World.max.y);
            return new Vector2I(xi, yi);
        }

        public static implicit operator Point(Vector2 vector)
        {
            return new Point((int)vector.x, (int)vector.y);
        }
        public static implicit operator PointF(Vector2 vector)
        {
            return new PointF(vector.x, vector.y);
        }
    }
}