﻿using System;
using System.Text;
using System.Runtime.InteropServices;
using System.Diagnostics;

using Engine.Tools;

namespace Engine.Maths
{
    /// <summary>
    /// Vector3D used when need accurate math precision, not used to store data
    /// </summary>
    [StructLayout(LayoutKind.Explicit)]
    [DebuggerDisplay("{x},{y},{z}")]
    public struct Vector3D
    {
        public const int sizeinbyte = sizeof(double) * 3;

        [FieldOffset(0)]
        public double x;
        [FieldOffset(8)]
        public double y;
        [FieldOffset(16)]
        public double z;

        public Vector3D(double x, double y, double z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public bool isNaN
        {
            get
            {
                return double.IsInfinity(x) || double.IsNaN(x) ||
                       double.IsInfinity(y) || double.IsNaN(y) ||
                       double.IsInfinity(z) || double.IsNaN(z);
            }
        }

        public double this[int i]
        {
            get
            {
                switch (i)
                {
                    case 0: return x;
                    case 1: return y;
                    case 2: return z;
                    default: throw new ArgumentException("i must be 0,1,2");
                }
            }
            set
            {
                switch (i)
                {
                    case 0: x = value; break;
                    case 1: y = value; break;
                    case 2: z = value; break;
                    default: throw new ArgumentException("i must be 0,1,2");
                }
            }
        }

        public override string ToString()
        {
            if (this.isNaN) return "NULL_VECTOR";
            return (string.Format("{0,4} {1,4} {2,4}", x, y, z));
        }
        /// <summary>
        /// format for normalized vector
        /// </summary>
        public string ToStringN()
        {
            return (string.Format("{0:0.000} {1:0.000} {2:0.000}", x, y, z));
        }

        /// <summary>
        /// A "null" vector, not zero, used example when you want considerate the value not processed or divided by 0
        /// </summary>
        public static readonly Vector3D NaN = new Vector3D(double.NaN, double.NaN, double.NaN);
        public static readonly Vector3D PosInf = new Vector3D(double.PositiveInfinity, double.PositiveInfinity, double.PositiveInfinity);
        public static readonly Vector3D NegInf = new Vector3D(double.NegativeInfinity, double.NegativeInfinity, double.NegativeInfinity);
        public static readonly Vector3D Zero = new Vector3D(0, 0, 0);
        public static readonly Vector3D One = new Vector3D(1, 1, 1);
        public static readonly Vector3D UnitX = new Vector3D(1, 0, 0);
        public static readonly Vector3D UnitY = new Vector3D(0, 1, 0);
        public static readonly Vector3D UnitZ = new Vector3D(0, 0, 1);
        public static readonly Vector3D NormalOne = new Vector3D(1.0 / MathUtils.Sqrt3, 1.0 / MathUtils.Sqrt3, 1.0 / MathUtils.Sqrt3);


        #region operator overload
        public static Vector3D operator +(Vector3D left, Vector3D right)
        {
            return new Vector3D(left.x + right.x, left.y + right.y, left.z + right.z);
        }
        public static Vector3D operator +(Vector3D v, double scalar)
        {
            return new Vector3D(v.x + scalar, v.y + scalar, v.z + scalar);
        }
        
        public static Vector3D operator -(Vector3D left, Vector3D right)
        {
            return new Vector3D(left.x - right.x, left.y - right.y, left.z - right.z);
        }
        public static Vector3D operator -(Vector3D v, double scalar)
        {
            return new Vector3D(v.x - scalar, v.y - scalar, v.z - scalar);
        }
        public static Vector3D operator -(Vector3D left)
        {
            return new Vector3D(-left.x, -left.y, -left.z);
        }
        
        /// <summary>
        /// Mull left.x*right.x , left.y*right.y , left.z*right.z 
        /// </summary>
        public static Vector3D operator *(Vector3D left, Vector3D right)
        {
            // because struct are passed as copy
            left.x = left.x * right.x;
            left.y = left.y * right.y;
            left.z = left.z * right.z;
            return left;
        }
        public static Vector3D operator *(Vector3D left, double scalar)
        {
            left.x = left.x * scalar;
            left.y = left.y * scalar;
            left.z = left.z * scalar;
            return left;
        }
        public static Vector3D operator *(double scalar, Vector3D right)
        {
            right.x = right.x * scalar;
            right.y = right.y * scalar;
            right.z = right.z * scalar;
            return right;
        }       

        /// <summary>
        /// Divide left.x/right.x , left.y/right.y , left.z/right.z 
        /// </summary>
        public static Vector3D operator /(Vector3D left, Vector3D right)
        {
            left.x = left.x / right.x;
            left.y = left.y / right.y;
            left.z = left.z / right.z;
            return left;
        }
        public static Vector3D operator /(Vector3D left, double scalar)
        {
            if (MathUtils.isZero(scalar)) throw new ArgumentException("Cannot divide a VectorDouble3 by zero");
            scalar = 1.0f / scalar;
            left.x *=  scalar;
            left.y *=  scalar;
            left.z *= scalar;
            return left;
        }
        public static Vector3D operator /(double scalar, Vector3D right)
        {
            if (MathUtils.isZero(scalar)) throw new ArgumentException("Cannot divide a VectorDouble3 by zero");
            scalar = 1.0f / scalar;
            right.x /= scalar;
            right.y /= scalar;
            right.z /= scalar;
            return right;
        }

        
        public static bool operator ==(Vector3D left, Vector3D right)
        {
            if (MathUtils.isZero(left.x - right.x)) return false;
            if (MathUtils.isZero(left.y - right.y)) return false;
            if (MathUtils.isZero(left.z - right.z)) return false;
            return true;
        }
        public static bool operator !=(Vector3D left, Vector3D right)
        {
            return !(left == right);
        }

        public override bool Equals(object obj)
        {
            return (this == (Vector3D)obj);
        }
        #endregion

        #region Math

        /// <summary>
        /// Get the squared Length (l*l)
        /// </summary>
        public double LengthSq
        {
            get { return x * x + y * y + z * z; }
        }
        /// <summary>
        /// Get the length (l)
        /// </summary>
        public double Length
        {
            get { return Math.Sqrt(LengthSq); }
        }
        /// <summary>
        /// sqrt(x*x + y*y + z*z)
        /// </summary>
        public static double GetLength(Vector3D vector)
        {
            return vector.Length;
        }
        /// <summary>
        /// x*x + y*y + z*z
        /// </summary>
        public static double GetLengthSquared(Vector3D vector)
        {
            return vector.LengthSq;
        }
        /// <summary>
        /// |x| + |y| + |z|
        /// </summary>
        public static double GetManhattanLength(Vector3D vector)
        {
            return MathUtils.ABS(vector.x) + MathUtils.ABS(vector.y) + MathUtils.ABS(vector.z);
        }

        /// <summary>
        /// Update minimum values
        /// </summary>
        public void Min(double x,double y,double z)
        {
            if (this.x > x) this.x = x;
            if (this.y > y) this.y = y;
            if (this.z > z) this.z = z;
        }

        /// <summary>
        /// Update maximum values
        /// </summary>
        public void Max(double x, double y, double z)
        {
            if (this.x < x) this.x = x;
            if (this.y < y) this.y = y;
            if (this.z < z) this.z = z;
        }
        /// <summary>
        /// Sum
        /// </summary>
        public void Sum(ref Vector3D vect)
        {
            x += vect.x;
            y += vect.y;
            z += vect.z;
        }
        /// <summary>
        /// Subtraction
        /// </summary>
        public void Sub(ref Vector3D vect)
        {
            x -= vect.x;
            y -= vect.y;
            z -= vect.z;
        }
        /// <summary>
        /// multiple x,y,z * scalar
        /// </summary>
        public void Multiply(double scalar)
        {
            x *= scalar;
            y *= scalar;
            z *= scalar;
        }

        /// <summary>
        /// Normalize the vector and get the calculated length. 
        /// </summary>
        public double Normalize()
        {
            double length = Length;

            if (length > double.Epsilon)
            {
                x /= length;
                y /= length;
                z /= length;
            }
            return length;
        }
        /// <summary>
        /// Get a new normalized VectorDouble3 copy
        /// </summary>
        public Vector3D Normal
        {
            get
            {
                Vector3D vect = new Vector3D(x, y, z);
                vect.Normalize();
                return vect;
            }
        }
        public static Vector3D GetNormal(Vector3D vector)
        {
            return vector.Normal; 
        }
        public static double Distance(Vector3D a, Vector3D b)
        {
            a.Sub(ref b);
            return a.Length;
        }
        public static double Dot(Vector3D a, Vector3D b) 
        { 
            return a.x * b.x + a.y * b.y + a.z * b.z;
        }

        /// <summary>
        /// remember that b × a = −(a × b) and for a LH coord system :
        /// <para> X = Y × Z </para>
        /// <para> Y = Z × X </para>
        /// <para> Z = X × Y </para>
        /// </summary>
        /// <remarks>
        /// A = direction of thumb, B = index finger, Cross = middle finger
        /// using Left-Hand rule for a Left Hand coordinate system
        /// </remarks>
        public static Vector3D Cross(Vector3D left, Vector3D right) 
        { 
            return new Vector3D(
                (left.y * right.z) - (left.z * right.y),
                (left.z * right.x) - (left.x * right.z), 
                (left.x * right.y) - (left.y * right.x));
        }
        #endregion



        /// <summary>
        /// Optimized version when you call it intensively 
        /// </summary>
        public static void TransformCoordinate(ref Vector3D vector, ref Matrix4 transform, ref Vector3D result)
        {
            double iw = transform.m30 * vector.x + transform.m31 * vector.y + transform.m32 * vector.z + transform.m33 * 1.0f; //w = 1;
            if (iw * iw < 1e-14) iw = 1.0f;
            iw = 1.0f / iw;
            result.x = (vector.x * transform.m00 + vector.y * transform.m01 + vector.z * transform.m02 + transform.m03) * iw;
            result.y = (vector.x * transform.m10 + vector.y * transform.m11 + vector.z * transform.m12 + transform.m13) * iw;
            result.z = (vector.x * transform.m20 + vector.y * transform.m21 + vector.z * transform.m22 + transform.m23) * iw;
        }

        /// <summary>
        /// Transform the Point vector using matrix, then result is transform * vector
        /// </summary>
        /// <remarks>
        /// VectorDouble3 are calculated as Vector4.w = 1 , 
        /// </remarks>
        public static Vector3D TransformCoordinate(Vector3D vector, Matrix4 transform)
        {
            Vector3D result = new Vector3D();
            Vector3D.TransformCoordinate(ref vector, ref transform, ref result);
            return result;
        }

        public static implicit operator Vector3D(Vector3 vector)
        {
            return new Vector3D(vector.x, vector.y, vector.z);
        }

        public override int GetHashCode()
        {
            return x.GetHashCode() ^ y.GetHashCode() ^ z.GetHashCode();
        }
    }
}