﻿#region LGPL License

/*
Axiom Graphics Engine Library
Copyright © 2003-2011 Axiom Project Team

The overall design, and a majority of the core engine and rendering code 
contained within this library is a derivative of the open source Object Oriented 
Graphics Engine OGRE, which can be found at http://ogre.sourceforge.net.  
Many thanks to the OGRE team for maintaining such a high quality project.

The math library included in this project, in addition to being a derivative of
the works of Ogre, also include derivative work of the free portion of the 
Wild Magic mathematics source code that is distributed with the excellent
book Game Engine Design.
http://www.wild-magic.com/

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#endregion

using System;
using System.Text;
using System.Runtime.InteropServices;
using System.Diagnostics;

using Engine.Tools;

namespace Engine.Maths
{
    [StructLayout(LayoutKind.Explicit)]
    struct FloatIntUnion
    {
        [FieldOffset(0)]
        public float f;

        [FieldOffset(0)]
        public int tmp;
    }


    /// <summary>
    /// Vector3 struct, math copy for Axiom Graphics Engine Library.
    /// I usualy write "left" an "right" when order is important
    /// </summary>
    [StructLayout(LayoutKind.Explicit)]
    [DebuggerDisplay("{x},{y},{z}")]
    public struct Vector3
    {
        [FieldOffset(0)]
        public float x;
        [FieldOffset(4)]
        public float y;
        [FieldOffset(8)]
        public float z;
        /// <summary>
        /// velocity comparable using direct access to x y and z field
        /// </summary>
        /// <example>float x = vector.Dim[0]</example>
        [FieldOffset(0)]
        public unsafe fixed float Dim[3];


        public const float EPS = 1e-6f;
        public const int sizeinbyte = sizeof(float) * 3;



        public Vector3(double x, double y, double z) :
            this((float)x, (float)y, (float)z) { }

        public Vector3(float x, float y, float z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public bool isZero
        {
            get { return LengthSq < EPS; }
        }

        public bool isNaN
        {
            get
            {
                return float.IsInfinity(x) || float.IsNaN(x) ||
                       float.IsInfinity(y) || float.IsNaN(y) ||
                       float.IsInfinity(z) || float.IsNaN(z);
            }
        }

        /// <summary>
        /// Slow method, is equivalent to use switch() of UNSAFE flag in this[int]
        /// </summary>
        public unsafe float GetDim(int i)
        {
            fixed (float* buffer = Dim)
            {
                return buffer[i];
            }
        }

        /// <summary>
        /// UNSAFE flag don't reveal a performance improvement, i suggest to use <code>float d = vector.Dim[i]</code>
        /// </summary>
        public float this[int i]
        {
#if UNSAFE
            get
            {
                unsafe
                {
                    fixed (float* pX = &this.x)
                    {
                        return *(pX + i);
                    }
                }
            }
             
            set
            {
                unsafe
                {
                    fixed (float* pX = &this.x)
                    {
                        *(pX + i) = value;
                    }
                }
            }
#else
            get
            {
                switch (i)
                {
                    case 0: return x;
                    case 1: return y;
                    case 2: return z;
                    default: throw new ArgumentException("i must be 0,1,2");
                }
            }
            set
            {
                switch (i)
                {
                    case 0: x = value; break;
                    case 1: y = value; break;
                    case 2: z = value; break;
                    default: throw new ArgumentException("i must be 0,1,2");
                }
            }
#endif
        }

        public float this[eAxis axe]
        {
            get
            {
                switch (axe)
                {
                    case eAxis.X: return x;
                    case eAxis.Y: return y;
                    case eAxis.Z: return z;
                    default: throw new ArgumentException("Wrong Axe");
                }
            }
            set
            {
                switch (axe)
                {
                    case eAxis.X: x = value; break;
                    case eAxis.Y: y = value; break;
                    case eAxis.Z: z = value; break;
                    default: throw new ArgumentException("Wrong Axe");
                }
            }
        }

        public override string ToString()
        {
            if (this.isNaN) return "NULL_VECTOR";
            return (string.Format("{0,4} {1,4} {2,4}", x, y, z));
        }
        /// <summary>
        /// format for normalized vector
        /// </summary>
        public string ToStringN()
        {
            return (string.Format("{0:0.000} {1:0.000} {2:0.000}", x, y, z));
        }

        /// <summary>
        /// A "null" vector, not zero, used example when you want considerate the value not processed or divided by 0
        /// </summary>
        public static readonly Vector3 NaN = new Vector3(float.NaN, float.NaN, float.NaN);
        public static readonly Vector3 PosInf = new Vector3(float.PositiveInfinity, float.PositiveInfinity, float.PositiveInfinity);
        public static readonly Vector3 NegInf = new Vector3(float.NegativeInfinity, float.NegativeInfinity, float.NegativeInfinity);
        public static readonly Vector3 Zero = new Vector3(0, 0, 0);
        public static readonly Vector3 One = new Vector3(1, 1, 1);
        public static readonly Vector3 UnitX = new Vector3(1, 0, 0);
        public static readonly Vector3 UnitY = new Vector3(0, 1, 0);
        public static readonly Vector3 UnitZ = new Vector3(0, 0, 1);
        public static readonly Vector3 NormalOne = new Vector3(1.0f / MathUtils.Sqrt3, 1.0f / MathUtils.Sqrt3, 1.0f / MathUtils.Sqrt3);

        // Directx Left Handled Coordinate System
        public static readonly Vector3 Right = UnitX;
        public static readonly Vector3 Left = -UnitX;
        public static readonly Vector3 Up = UnitY;
        public static readonly Vector3 Down = -UnitY;
        public static readonly Vector3 Backward = UnitZ;
        public static readonly Vector3 Forward = -UnitZ;


        #region operator overload
        public static Vector3 operator +(Vector3 left, Vector3 right)
        {
            return new Vector3(left.x + right.x, left.y + right.y, left.z + right.z);
        }
        public static Vector3 operator +(Vector3 v, float scalar)
        {
            return new Vector3(v.x + scalar, v.y + scalar, v.z + scalar);
        }

        public static Vector3 operator -(Vector3 left, Vector3 right)
        {
            return new Vector3(left.x - right.x, left.y - right.y, left.z - right.z);
        }
        public static Vector3 operator -(Vector3 v, float scalar)
        {
            return new Vector3(v.x - scalar, v.y - scalar, v.z - scalar);
        }
        public static Vector3 operator -(Vector3 left)
        {
            return new Vector3(-left.x, -left.y, -left.z);
        }

        /// <summary>
        /// Mull left.x*right.x , left.y*right.y , left.z*right.z 
        /// </summary>
        public static Vector3 operator *(Vector3 left, Vector3 right)
        {
            // because struct are passed as copy
            left.x = left.x * right.x;
            left.y = left.y * right.y;
            left.z = left.z * right.z;
            return left;
        }
        public static Vector3 operator *(Vector3 left, float scalar)
        {
            left.x = left.x * scalar;
            left.y = left.y * scalar;
            left.z = left.z * scalar;
            return left;
        }
        public static Vector3 operator *(float scalar, Vector3 right)
        {
            right.x = right.x * scalar;
            right.y = right.y * scalar;
            right.z = right.z * scalar;
            return right;
        }

        /// <summary>
        /// Divide left.x/right.x , left.y/right.y , left.z/right.z 
        /// </summary>
        public static Vector3 operator /(Vector3 left, Vector3 right)
        {
            left.x = left.x / right.x;
            left.y = left.y / right.y;
            left.z = left.z / right.z;
            return left;
        }
        public static Vector3 operator /(Vector3 left, float scalar)
        {
            if (MathUtils.isZero(scalar)) throw new ArgumentException("Cannot divide a Vector3 by zero");
            scalar = 1.0f / scalar;
            left.x *= scalar;
            left.y *= scalar;
            left.z *= scalar;
            return left;
        }
        public static Vector3 operator /(float scalar, Vector3 right)
        {
            if (MathUtils.isZero(scalar)) throw new ArgumentException("Cannot divide a Vector3 by zero");
            scalar = 1.0f / scalar;
            right.x /= scalar;
            right.y /= scalar;
            right.z /= scalar;
            return right;
        }


        public static bool operator ==(Vector3 left, Vector3 right)
        {
            return left.x == right.x && left.y == right.y && left.z == right.z;
        }
        public static bool operator !=(Vector3 left, Vector3 right)
        {
            return !(left == right);
        }

        /// <summary>
        /// Equal must be a perferct match between two vertex struct
        /// </summary>
        public override bool Equals(object obj)
        {
            if (obj is Vector3)
            {
                return ((Vector3)obj) == this;
            }
            return false;
        }
        #endregion

        #region Math

        /// <summary>
        /// Get the squared Length (l*l)
        /// </summary>
        public float LengthSq
        {
            get { return x * x + y * y + z * z; }
        }
        /// <summary>
        /// Get the length (l)
        /// </summary>
        public float Length
        {
            get { return (float)Math.Sqrt(LengthSq); }
        }
        /// <summary>
        /// sqrt(x*x + y*y + z*z)
        /// </summary>
        public static float GetLength(Vector3 vector)
        {
            return vector.Length;
        }
        /// <summary>
        /// x*x + y*y + z*z
        /// </summary>
        public static float GetLengthSquared(Vector3 vector)
        {
            return vector.LengthSq;
        }
        /// <summary>
        /// |x| + |y| + |z|
        /// </summary>
        public static float GetManhattanLength(Vector3 vector)
        {
            return MathUtils.ABS(vector.x) + MathUtils.ABS(vector.y) + MathUtils.ABS(vector.z);
        }
        /// <summary>
        /// Return the distance from a point to line
        /// </summary>
        /// <param name="line0">a point of line</param>
        /// <param name="line1">a point of line</param>
        /// <param name="point">the point from line</param>
        /// <returns></returns>
        public static float GetPointLineDistance(Vector3 line0, Vector3 line1, Vector3 point)
        {
            Vector3 line01 = line1 - line0;
            Vector3 dist = point - line0;

            return (float)Math.Sqrt(Vector3.GetLengthSquared(Vector3.Cross(line01, dist)) / Vector3.GetLengthSquared(line01));
        }
        /// <summary>
        /// Get the normal of a triangle, if is degeneralized return a Vector3.Zero
        /// </summary>
        public static Vector3 GetNormalTriangle(Vector3 v0, Vector3 v1, Vector3 v2)
        {
            Vector3 N = Vector3.Cross(v1 - v0, v2 - v0);
            float l = N.Length;
            return (l > float.Epsilon) ? N / l : Vector3.Zero;
        }

        /// <summary>
        /// Update minimum values
        /// </summary>
        public void Min(float x, float y, float z)
        {
            if (this.x > x) this.x = x;
            if (this.y > y) this.y = y;
            if (this.z > z) this.z = z;
        }

        /// <summary>
        /// Update maximum values
        /// </summary>
        public void Max(float x, float y, float z)
        {
            if (this.x < x) this.x = x;
            if (this.y < y) this.y = y;
            if (this.z < z) this.z = z;
        }
        /// <summary>
        /// Sum
        /// </summary>
        public void Sum(ref Vector3 vect)
        {
            x += vect.x;
            y += vect.y;
            z += vect.z;
        }
        /// <summary>
        /// Subtraction
        /// </summary>
        public void Sub(ref Vector3 vect)
        {
            x -= vect.x;
            y -= vect.y;
            z -= vect.z;
        }
        /// <summary>
        /// multiple x,y,z * scalar
        /// </summary>
        public void Multiply(float scalar)
        {
            x *= scalar;
            y *= scalar;
            z *= scalar;
        }

        /// <summary>
        /// Normalize the vector and get the calculated length. 
        /// </summary>
        public float Normalize()
        {
            float length = Length;

            if (length > 1e-6f)
            {
                x /= length;
                y /= length;
                z /= length;
                return length;
            }
            else
            {
                x = y = z = 0;
                return 0;
            }

        }
        /// <summary>
        /// Get a new normalized vector3 copy
        /// </summary>
        public Vector3 Normal
        {
            get
            {
                Vector3 vect = new Vector3(x, y, z);
                vect.Normalize();
                return vect;
            }
        }
        public static Vector3 GetNormal(Vector3 vector)
        {
            return vector.Normal;
        }



        public static float Distance(Vector3 a, Vector3 b)
        {
            a.Sub(ref b);
            return a.Length;
        }

        public static float DistanceSq(ref Vector3 a, ref Vector3 b)
        {
            float mm = a.x - b.x;
            float d = mm * mm;
            mm = a.y - b.y;
            d += mm * mm;
            mm = a.z - b.z;
            d += mm * mm;
            return d;
        }

        public static float Dot(Vector3 a, Vector3 b)
        {
            return a.x * b.x + a.y * b.y + a.z * b.z;
        }

        public static float Dot(ref Vector3 a, ref Vector3 b)
        {
            return a.x * b.x + a.y * b.y + a.z * b.z;
        }

        /// <summary>
        /// remember that b × a = −(a × b) and for a LH coord system :
        /// <para> X = Y × Z </para>
        /// <para> Y = Z × X </para>
        /// <para> Z = X × Y </para>
        /// </summary>
        /// <remarks>
        /// A = direction of thumb, B = index finger, Cross = middle finger
        /// using Left-Hand rule for a Left Hand coordinate system
        /// </remarks>
        public static Vector3 Cross(Vector3 left, Vector3 right)
        {
            return Vector3.Cross(ref  left, ref  right);
        }
        /// <summary>
        /// Optimized
        /// </summary>
        public static void Cross(ref Vector3 left, ref Vector3 right, ref Vector3 result)
        {
            result.x = (left.y * right.z) - (left.z * right.y);
            result.y = (left.z * right.x) - (left.x * right.z);
            result.z = (left.x * right.y) - (left.y * right.x);
        }
        public static Vector3 Cross(ref Vector3 left, ref Vector3 right)
        {
            return new Vector3(
                (left.y * right.z) - (left.z * right.y),
                (left.z * right.x) - (left.x * right.z),
                (left.x * right.y) - (left.y * right.x));
        }

        #endregion



        /// <summary>
        /// Optimized version when you call it intensively 
        /// </summary>
        public static void TransformCoordinate(ref Vector3 vector, ref Matrix4 transform, ref Vector3 result)
        {
            float iw = transform.m30 * vector.x + transform.m31 * vector.y + transform.m32 * vector.z + transform.m33 * 1.0f; //w = 1;
            if (iw * iw < 1e-14) iw = 1.0f;
            iw = 1.0f / iw;
            result.x = (vector.x * transform.m00 + vector.y * transform.m01 + vector.z * transform.m02 + transform.m03) * iw;
            result.y = (vector.x * transform.m10 + vector.y * transform.m11 + vector.z * transform.m12 + transform.m13) * iw;
            result.z = (vector.x * transform.m20 + vector.y * transform.m21 + vector.z * transform.m22 + transform.m23) * iw;
        }

        /// <summary>
        /// Transform the Point vector using matrix, then result is transform * vector
        /// </summary>
        /// <remarks>
        /// Vector3 are calculated as Vector4.w = 1 , 
        /// </remarks>
        public static Vector3 TransformCoordinate(Vector3 vector, Matrix4 transform)
        {
            Vector3 result = new Vector3();
            Vector3.TransformCoordinate(ref vector, ref transform, ref result);
            return result;
        }

        /// <summary>
        /// Transform the Normal vector using matrix, name WIT remember you that correct matrix is the 
        /// World-Inverse-Traspose instead World if you want a normal not affected by scale transformation
        /// </summary>
        /// <remarks>
        /// WIT matrix have traslation zero and last row 0,0,0,1. In fact only the 3x3 part of matrix is used
        /// and is equivalent to multiply a 4x4 matrix with a Vector4(x,y,z,0)
        /// 
        /// </remarks>
        public static Vector3 TransformNormal(Vector3 vector, Matrix4 WIT)
        {
            Vector3 result = new Vector3();

            float iw = WIT.m30 * vector.x + WIT.m31 * vector.y + WIT.m32 * vector.z; // + m33 * vector.w = 0;
            if (iw * iw < 1e-14) iw = 1.0f;
            iw = 1.0f / iw;
            result.x = (vector.x * WIT.m00 + vector.y * WIT.m01 + vector.z * WIT.m02) * iw;
            result.y = (vector.x * WIT.m10 + vector.y * WIT.m11 + vector.z * WIT.m12) * iw;
            result.z = (vector.x * WIT.m20 + vector.y * WIT.m21 + vector.z * WIT.m22) * iw;

            return result;
        }

        public static Vector3 TransformCoordinate(Vector3 vector, Quaternion rotation)
        {
            float x = rotation.x + rotation.x;
            float y = rotation.y + rotation.y;
            float z = rotation.z + rotation.z;

            float wx = rotation.w * x;
            float wy = rotation.w * y;
            float wz = rotation.w * z;

            float xx = rotation.x * x;
            float xy = rotation.x * y;
            float xz = rotation.x * z;

            float yy = rotation.y * y;
            float yz = rotation.y * z;
            float zz = rotation.z * z;

            return new Vector3(
                ((vector.x * ((1.0f - yy) - zz)) + (vector.y * (xy - wz))) + (vector.z * (xz + wy)),
                ((vector.x * (xy + wz)) + (vector.y * ((1.0f - xx) - zz))) + (vector.z * (yz - wx)),
                ((vector.x * (xz - wy)) + (vector.y * (yz + wx))) + (vector.z * ((1.0f - xx) - yy)));
        }

        #region Left Hand Rule

        /// <summary>
        /// Return the origin of ray when you click on screen, the point depend by depthZ , default is 0.0
        /// </summary>
        /// <remarks>
        /// the directx big matrix is Proj * View * World , isn't commutative
        /// </remarks>
        /// <param name="depthZ">-1.0 = in the eye postion , 0.0 = nearZ plane , 1.0 = farZ plane</param>
        public static Vector3 Unproject(int mouseX, int mouseY, float depthZ, Viewport viewport, Matrix4 proj, Matrix4 view, Matrix4 world)
        {
            Vector4 source = new Vector4();
            source.x = ((float)(mouseX - viewport.X) / (float)viewport.Width * 2f) - 1f;
            source.y = (((float)(mouseY - viewport.Y) / (float)viewport.Height) * -2f) + 1f;
            source.z = (float)(depthZ - viewport.MinDepth) / (float)(viewport.MaxDepth - viewport.MinDepth);
            source.w = 1f;

            source = Matrix4.Inverse(proj * view * world) * source;

            // homogenous calculation and test if is a zero-division
            if (source.w * source.w > float.Epsilon)
                source = source * (1f / source.w);

            return (Vector3)source;
        }



        /// <summary>
        /// get the planar projection point.
        /// </summary>
        public static Vector3 Project(Vector3 vector, Matrix4 world)
        {
            Vector4 vect = (Vector4)vector;
            vect = world * vect;
            return (Vector3)vect;
        }
        /// <summary>
        /// get the screen point, the z value are the depthZ calculate with Logarithmic algorithm
        /// http://www.gamasutra.com/blogs/BranoKemen/20090812/2725/Logarithmic_Depth_Buffer.php
        /// </summary>
        /// <param name="WVP">Proj * View (because camera world is always identity)</param>
        public static Vector3 ProjectLogZ(Vector3 vector, Viewport viewport, Matrix4 WVP, float FarPlane, float Resolution = 0.001f)
        {
            Vector4 coord = new Vector4(vector.x, vector.y, vector.z, 1.0f);
            coord = WVP * coord;

            coord.z = (float)(Math.Log10(Resolution * coord.z + 1) / Math.Log10(Resolution * FarPlane + 1) * coord.w);

            float inv_w = coord.w * coord.w > float.Epsilon ? 1.0f / coord.w : 1.0f;
            coord *= inv_w;

            // now you have the X Y coordinate in [-1,1] range(bottomleft system) and Z-depht value in [near=0, far=1] range.
            // Flip y value for a Top-Left coordinates
            vector.x = (coord.x + 1) * 0.5f * viewport.Width + viewport.X;
            vector.y = (1 - coord.y) * 0.5f * viewport.Height + viewport.Y;
            vector.z = coord.z;

            return vector;
        }

        /// <summary>
        /// get the screen point, the z value are the depthZ
        /// </summary>
        /// <param name="WVP">Proj * View (because camera world is always identity)</param>
        /// <remarks>
        /// if in directx (row major) the WVP is defined as W*V*P, in my math notation i use OpenGL colum major (like my school knowledge)
        /// where matrix are trasposed so W^t*V^t*P^t become P*V*W
        /// </remarks>
        public static Vector3 Project(Vector3 vector, Viewport viewport, Matrix4 WVP)
        {
            // multipling in colum notation we optain the trasformation of vector to wvp system and so the output of vertexshader
            Vector4 coord = new Vector4(vector.x, vector.y, vector.z, 1.0f);
            coord = WVP * coord;

            // now recent graphics cards auto-calculate the homogeneus value in pixelshader input, otherwise you have to do manualy
            // or like here: http://msdn.microsoft.com/en-us/library/windows/desktop/bb153308(v=vs.85).aspx they suggest
            // to create a compliant matrix
            float inv_w = coord.w * coord.w > float.Epsilon ? 1.0f / coord.w : 1.0f;
            coord *= inv_w;

            // now you have the X Y coordinate in [-1,1] range(bottomleft system) and Z-depht value in [near=0, far=1] range.
            // Flip y value for a Top-Left coordinates
            vector.x = (coord.x + 1) * 0.5f * viewport.Width + viewport.X;
            vector.y = (1 - coord.y) * 0.5f * viewport.Height + viewport.Y;
            vector.z = coord.z;

            return vector;
        }
        /// <summary>
        /// get the screen point
        /// remember that 'vector' must be transformed in world coordinate before pass as parameter
        /// </summary>
        public static Vector3 Project(Vector3 vector, Viewport viewport, ICamera camera)
        {
            return Vector3.Project(vector, viewport, camera.Projection * camera.View);
        }
        /// <summary>
        /// get the screen point using logarithmic z instead linear z.
        /// remember that 'vector' must be transformed in world coordinate before pass as parameter
        /// </summary>
        public static Vector3 ProjectLogZ(Vector3 vector, Viewport viewport, ICamera camera, float Resolution = 0.001f)
        {

            return Vector3.ProjectLogZ(vector, viewport, camera.Projection * camera.View, camera.Far, Resolution);
        }
        #endregion

        public static implicit operator Vector3(Vector4 vector)
        {
            return new Vector3(vector.x, vector.y, vector.z);
        }
        public static implicit operator Vector3(Vector3D vector)
        {
            return new Vector3(vector.x, vector.y, vector.z);
        }
        public override int GetHashCode()
        {
            return x.GetHashCode() ^ y.GetHashCode() ^ z.GetHashCode();
        }
    }
}