﻿using System;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace Engine.Maths
{
    /// <summary>
    /// Uint32 in ARGB format
    /// TODO : check if mach with COLOR format of shader program
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 4, Size = sizeof(int))]
    public struct Color32
    {
        public const int sizeinbyte = sizeof(int);
        public uint argb;

        public byte A { get { return (byte)((argb >> 24) & 0xFF); } set { argb = (argb & 0x00FFFFFF) | ((uint)value << 24); } }
        public byte R { get { return (byte)((argb >> 16) & 0xFF); } set { argb = (argb & 0xFF00FFFF) | ((uint)value << 16); } }
        public byte G { get { return (byte)((argb >> 8) & 0xFF); } set { argb = (argb & 0xFFFF00FF) | ((uint)value << 8); } }
        public byte B { get { return (byte)(argb & 0xFF); } set { argb = (argb & 0xFFFFFF00) | value; } }
        public float fA { get { return A / 255.0f; } }
        public float fR { get { return R / 255.0f; } }
        public float fG { get { return G / 255.0f; } }
        public float fB { get { return B / 255.0f; } }

        static float clamp(float a) { return a > 1 ? 1 : (a < 0 ? 0 : a); }


        public Color32(float r, float g, float b, float a) :
            this(
            (byte)(clamp(r) * 255.0f),
            (byte)(clamp(g) * 255.0f),
            (byte)(clamp(b) * 255.0f),
            (byte)(clamp(a) * 255.0f)) { }

        public Color32(byte r, byte g, byte b) :
            this(r, g, b, (byte)255) { }

        public Color32(int r, int g, int b, int a) :
            this((byte)r, (byte)g, (byte)b, (byte)a) { }

        public Color32(uint r, uint g, uint b, uint a) :
            this((byte)r, (byte)g, (byte)b, (byte)a) { }

        public Color32(byte r, byte g, byte b, byte a) :
            this(a << 24 | r << 16 | g << 8 | b) { }

        public Color32(int i) :
            this((uint)i) { }

        public Color32(uint i) { argb = i & 0xFFFFFFFF; }

        /// <summary>
        /// A "null" vector, not zero, used example when you want considerate the value not processed or divided by 0
        /// </summary>
        public static readonly Color32 White = new Color32(255, 255, 255);
        public static readonly Color32 Black = new Color32(0, 0, 0);
        public static readonly Color32 Red = new Color32(255, 0, 0);
        public static readonly Color32 Green = new Color32(0, 255, 0);
        public static readonly Color32 Blue = new Color32(0, 0, 255);
        public static readonly Color32 Yellow = new Color32(255, 255, 0);
        public static readonly Color32 Cyano = new Color32(0, 255, 255);
        public static readonly Color32 Mangenta = new Color32(255, 0, 255);

        /// <summary>
        /// hue 0.0 = Red;  hue 1.0 = Blue , saturation = 1(max)
        /// </summary>
        public static Color32 Rainbow(float hue)
        {
            if (hue < 0.00f) return Color.Red;
            if (hue < 0.25f) return new Color32(1, hue / 0.25f, 0, 1);
            if (hue < 0.50f) return new Color32(1 - (hue - 0.25f) / 0.25f, 1, 0, 1);
            if (hue < 0.75f) return new Color32(0, 1, (hue - 0.5f) / 0.25f, 1);
            if (hue < 1.00f) return new Color32(0, 1 - (hue - 0.75f) / 0.25f, 1, 1);
            return Color.Blue;
        }


        public float Saturation
        {
            get
            {
                float r = (float)R / 255.0f;
                float g = (float)G / 255.0f;
                float b = (float)B / 255.0f;
                float max = r;
                float min = r;
                float s = 0;

                if (g > max) max = g;
                if (b > max) max = b;

                if (g < min) min = g;
                if (b < min) min = b;

                // if max == min, then there is no color and
                // the saturation is zero.
                //
                if (max != min)
                {
                    float l = (max + min) / 2;
                    s = (l <= 0.5f) ?
                        (max - min) / (max + min) :
                        (max - min) / (2 - max - min);
                }
                return s;
            }
        }
        public float Brightness
        {
            get
            {
                float r = (float)R / 255.0f;
                float g = (float)G / 255.0f;
                float b = (float)B / 255.0f;
                float max, min;
                max = r; min = r;
                if (g > max) max = g;
                if (b > max) max = b;
                if (g < min) min = g;
                if (b < min) min = b;
                return (max + min) / 2;
            }
        }
        public float Hue
        {
            get
            {
                if (R == G && G == B)
                    return 0; // 0 makes as good an UNDEFINED value as any

                float r = (float)R / 255.0f;
                float g = (float)G / 255.0f;
                float b = (float)B / 255.0f;

                float max, min;
                float delta;
                float hue = 0.0f;

                max = r; min = r;

                if (g > max) max = g;
                if (b > max) max = b;

                if (g < min) min = g;
                if (b < min) min = b;

                delta = max - min;

                if (r == max)
                {
                    hue = (g - b) / delta;
                }
                else if (g == max)
                {
                    hue = 2 + (b - r) / delta;
                }
                else if (b == max)
                {
                    hue = 4 + (r - g) / delta;
                }
                hue *= 60;

                if (hue < 0.0f)
                {
                    hue += 360.0f;
                }
                return hue;
            }
        }

        public static implicit operator Color32(Color color)
        {
            return new Color32(color.ToArgb());
        }
        public static implicit operator Color(Color32 color)
        {
            return Color.FromArgb((int)color.argb);
        }
        public static implicit operator Vector4(Color32 color)
        {
            return new Vector4(color.fR, color.fG, color.fB, color.fA);
        }

        public static Color32 operator +(Color32 left, Color32 right)
        {
            return new Color32(
                left.fR + right.fR,
                left.fG + right.fG,
                left.fB + right.fB,
                left.fA + right.fA);
        }
        public static Color32 operator /(Color32 color, float scalar)
        {
            return color * (1.0f / scalar);
        }
        public static Color32 operator *(Color32 color, float scalar)
        {
            return new Color32(
                color.fR * scalar,
                color.fG * scalar,
                color.fB * scalar,
                color.fA * scalar);
        }

        public static bool operator ==(Color32 left, Color32 right)
        {
            return left.argb == right.argb;
        }
        public static bool operator !=(Color32 left, Color32 right)
        {
            return !(left == right);
        }
        public override bool Equals(object obj)
        {
            return (obj is Color32) && this == (Color32)obj;
        }
        public override int GetHashCode()
        {
            return (int)argb;
        }

        public override string ToString()
        {
            switch (this.argb)
            {
                case 0xff000000: return "Color32[Black]";
                case 0xFFFFFFFF: return "Color32[White]";
                case 0xffFF0000: return "Color32[Red]";
                case 0xff00FF00: return "Color32[Green]";
                case 0xff0000FF: return "Color32[Blue]";
                case 0xffFFFF00: return "Color32[Yellow]";
                case 0xff00FFFF: return "Color32[Cyano]";
                case 0xffFF00FF: return "Color32[Magenta]";
                default: return string.Format("Color32[R{0}G{1}B{2}A{3}]", R, G, B, A);
            }
        }
    }
}