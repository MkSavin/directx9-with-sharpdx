﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;

using Engine.Tools;

namespace Engine.Maths
{
    /// <summary>
    /// Infinite line
    /// </summary>
    public struct Line
    {
        // in this case, p is the closest point what satisfies dot(P,D) = 0
        public Vector3 orig, dir;

        /// <summary>
        /// </summary>
        /// <param name="point"></param>
        /// <param name="dir">will be normalized, if lenght is zero, it'll be set as UnitX</param>
        public Line(Vector3 point, Vector3 dir)
        {
            float length = dir.Normalize();
            if (length < 1e-8f) dir = Vector3.UnitX;
            float t = Vector3.Dot(ref point, ref dir);
            this.dir = dir;
            this.orig.x = point.x - t * dir.x;
            this.orig.y = point.y - t * dir.y;
            this.orig.z = point.z - t * dir.z;
        }


        /// <summary>
        /// Get the point using equation : Line(t) = Line.P + t*Line.D
        /// </summary>
        public Vector3 this[float t] { get { return orig + dir * t; } }

        /// <summary>
        /// A ray can be generalized to line but not viceversa
        /// </summary>
        public static implicit operator Line(Ray ray)
        {
            return new Line(ray.orig, ray.dir);
        }
        /// <summary>
        /// A segment can be generalized to line but not viceversa
        /// </summary>
        public static implicit operator Line(Segment seg)
        {
            return new Line(seg.orig, seg.Direction);
        }

        public static Line FromPointAndDirection(Vector3 Point, Vector3 Direction)
        {
            return new Line(Point, Direction);
        }

        public static Line FromTwoPoint(Vector3 PointA, Vector3 PointB)
        {
            return new Line(PointA, PointA - PointB);
        }


        #region Distances Line-Point
        /// <summary>
        /// Shorter distance of generic line from point
        /// http://geomalgorithms.com/a02-_lines.html
        /// </summary>
        public static float GetPointDistance(Vector3 orig, Vector3 dir, Vector3 point)
        {
            return Vector3.Cross(dir, point - orig).Length;
        }
        /// <summary>
        /// Shorter distance of generic line from point
        /// </summary>
        public float GetPointDistance(Vector3 point)
        {
            return Vector3.Cross(dir, point - orig).Length;
        }

        /// <summary>
        /// Shorter point (as line's parameter instead point) between lines and a point.
        /// </summary>
        public static float GetPointParam(ref Vector3 orig, ref Vector3 dir, Vector3 point)
        {
            point.Sub(ref orig);
            return Vector3.Dot(ref point, ref dir);
        }
        /// <summary>
        /// Shorter point (as line's parameter instead point) between lines and a point.
        /// </summary>
        public float GetPointParam(Vector3 point)
        {
            point.Sub(ref orig);
            return Vector3.Dot(ref point, ref dir);
        }
        #endregion

        #region Distances Line-Line
        /// <summary>
        /// Shorter distance between two line
        /// </summary>
        public static float GetLineDistance(Vector3 orig0, Vector3 dir0,Vector3 orig1, Vector3 dir1)
        {
            Vector3 p01 = orig1 - orig0;

            if (p01.isZero) return 0;

            Vector3 N = Vector3.Cross(dir0, dir1);
            float length = N.Normalize();

            if (length < 1e-6f)
            {
                // if two line are parallel the distance is between line and point.
                return Vector3.Cross(dir0, p01).Length;
            }
            else
            {
                return Vector3.Dot(orig0, N) / length;
            }
        }
        /// <summary>
        /// Shorter distance between two line
        /// </summary>
        public float GetLineDistance(Vector3 lineOrig, Vector3 lineDir)
        {
            return GetLineDistance(orig, dir, lineOrig, lineDir);
        }


        /// <summary>
        /// Shorter points (as line's parameters instead points) between two lines
        /// </summary>
        public static void GetPointsParams(
            ref Vector3 orig0, ref Vector3 dir0,
            ref Vector3 orig1, ref Vector3 dir1,
            out float t0, out float t1)
        {
            Vector3 p = orig1 - orig0;
            float a = Vector3.Dot(ref dir0, ref dir1);
            float b = Vector3.Dot(ref p, ref dir1);
            float c = Vector3.Dot(ref p, ref dir0);
            float aa = 1 + a * a;
            t0 = (c + a * b) / aa;
            t1 = (a * c - b) / aa;
        }
        /// <summary>
        /// <see cref="GetPointsParams"/>
        /// </summary>
        public static void GetPointsParams(ref Line line0, ref Line line1, out float t0, out float t1)
        {
            GetPointsParams(
                ref line0.orig, ref line0.dir,
                ref line1.orig, ref line1.dir,
                out t0, out t1);
        }

        #endregion

        public override string ToString()
        {
            return "Closer: " + orig.ToString() + " Dir: " + dir.ToString();
        }
    }
    /// <summary>
    /// Equivalent to a semi-Line in 3d with a point to start
    /// </summary>
    public struct Ray
    {
        internal Vector3 orig, dir;


        /// <summary>
        /// Direction will be normalized for safety
        /// </summary>
        public Ray(Vector3 Origin, Vector3 Direction)
        {
            orig = Origin;
            dir = Direction;
            float length = dir.Normalize();
            Debug.Assert(length > float.Epsilon, "invalid direction normal");
        }
        /// <summary>
        /// TODO : Get ray from screen picking
        /// </summary>
        /// <param name="screen">the z value is the nearz plane</param>
        public Ray(int X,int Y , Viewport viewport, Matrix4 proj, Matrix4 view)
        {
            Matrix4 I = Matrix4.Identity;
            Vector3 p0 = Vector3.Unproject(X, Y, 0, viewport, proj, view, I);
            Vector3 p1 = Vector3.Unproject(X, Y, 1, viewport, proj, view, I);

            orig = p0;
            dir = p1 - p0;
            float length = dir.Normalize();

            Debug.Assert(length > float.Epsilon, "invalid direction normal");
        }

        public Vector3 Origin
        { 
            get { return orig; } 
        }

        public Vector3 Direction
        { 
            get { return dir; } 
        }
        
        /// <summary>
        /// If ray isn't initialized or empty the direction length is Zero and invdir can't be used (!DIV0)
        /// </summary>
        public bool IsNaN
        {
            get { return (dir.isNaN || dir.LengthSq < float.Epsilon); }
        }
        /// <summary>
        /// Return a empty array not usable
        /// </summary>
        public static Ray NaN
        {
            get
            {
                Ray ray = new Ray();
                ray.orig = Vector3.NaN;
                ray.dir = Vector3.NaN;
                return ray;
            }

        }
        /// <summary>
        /// Get the function Ray(t)
        /// </summary>
        public Vector3 this[float t] { get { return orig + dir * t; } }

        /// <summary>
        /// A segment can be generalized to ray but not viceversa
        /// </summary>
        public static implicit operator Ray(Segment seg)
        {
            return new Ray(seg.orig, seg.dir);
        }
        
        /// <summary>
        /// Convert ray into different coordinate system
        /// </summary>
        public static Ray TransformCoordinate(Ray ray, Matrix4 coordsys)
        {
            return new Ray(Vector3.TransformCoordinate(ray.orig, coordsys), Vector3.TransformNormal(ray.dir, coordsys));
        }

        /// <summary>
        /// NOT TESTED
        /// http://geomalgorithms.com/a02-_lines.html
        /// </summary>
        public float GetDistance(Vector3 P)
        {
            Vector3 w = P - orig;

            float dot = Vector3.Dot(ref w, ref dir);

            if (dot <= 0)
                return (float)Math.Sqrt(Vector3.DistanceSq(ref P, ref orig)); // before P0
            else
                return Vector3.Cross(ref dir, ref w).Length;
        }

        #region Currently Not Used

        /// <summary>
        /// can't do a perfect selection with mouse, so use a EPSILON error
        /// </summary>
        public bool IntersectSegment(Vector3 P0, Vector3 P1, float EPSILON , out float t)
        {
            t = 0;
            Vector3 u = P0 - P1;
            u.Normalize();
            Vector3 cross = Vector3.Cross(u, dir);
            float mindist = Math.Abs(Vector3.Dot(cross, orig - P0) / cross.Length);
            return mindist < EPSILON;

            /*
            Vector3 u = this.dir;
            Vector3 v = P1 - P0;
            Vector3 w = this.orig - P0;
            float a = Vector3.Dot(u, u);         // always >= 0
            float b = Vector3.Dot(u, v);
            float c = Vector3.Dot(v, v);         // always >= 0
            float d = Vector3.Dot(u, w);
            float e = Vector3.Dot(v, w);
            float D = a * c - b * b;        // always >= 0
            float sc, tc;

            // compute the line parameters of the two closest points
            if (D < EPSILON)
            {
                // the lines are almost parallel
                sc = 0.0f;
                tc = (b > c ? d / b : e / c);    // use the largest denominator
            }
            else
            {
                sc = (b * e - c * d) / D;
                tc = (a * e - b * d) / D;
            }

            // get the difference of the two closest points
            Vector3 dP = w + (sc * u) - (tc * v);  // =  L1(sc) - L2(tc)
            float mindist = dP.LengthSq();// return the closest distance^2

            return mindist < EPSILON * EPSILON;
            */
        }
        #endregion

        public override string ToString()
        {
            return "Origin: " + orig.ToString() + " Dir: " + dir.ToString();
        }
    }
    /// <summary>
    /// A line delimited by two point
    /// </summary>
    public struct Segment
    {
        internal Vector3 orig, dir;
        //i store the t value for end point
        internal float length;

        public Vector3 Direction
        {
            get { return dir; }
        }

        /// <summary>
        /// Get or Set start point, if set the Direction and lenght are updated
        /// </summary>
        public Vector3 Start
        {
            get { return orig; }
            set { dir = End - value; orig = value;  length = dir.Normalize(); }

        }
        /// <summary>
        /// Get or Set end point, if set the Direction and lenght are updated
        /// </summary>
        public Vector3 End
        {
            get { return orig + dir * length; }
            set { dir = value - orig; length = dir.Normalize(); }
        
        }
        /// <summary>
        /// Distance from Start to End point,so End point is Direction*Lenght
        /// </summary>
        public float Lenght
        { 
            get { return length; }
        }

        /// <summary>
        /// Remember that the direction is from Start ----&gt; to End
        /// </summary>
        public Segment(Vector3 Start, Vector3 End)
        {
            orig = Start;
            dir = End - Start;
            length = dir.Normalize();
        }

        /// <summary>
        /// Get the interpolated point using equation : Segment(f) = Segment.start * (1-f) + Segment.end * (f);
        /// Start = f:0  End = f:1
        /// </summary>
        /// <remarks>
        /// faster equation is p0 + (p1-p0)*f
        /// </remarks>
        public Vector3 Lerp(float f)
        {
            Vector3 p0 = Start;
            Vector3 p1 = End;

            return new Vector3(
                p0.x + (p1.x - p0.x) * f,
                p0.y + (p1.y - p0.y) * f,
                p0.z + (p1.x - p0.z) * f);
            /*
            return new Vector3(
                p1.x * f+ p0.x * (1 - f), 
                p1.y * f + p0.y * (1 - f),
                p1.z * f + p0.z * (1 - f));
            */
        }

        /// <summary>
        /// Get the function Segment(t) where t is parametric scalar of direction
        /// </summary>
        public Vector3 this[float t] { get { return orig + dir * t; } }

        /// <summary>
        /// NOT TESTED
        /// http://geomalgorithms.com/a02-_lines.html
        /// </summary>
        public float GetDistance(Vector3 P)
        {
            float dot = Vector3.Dot(P - orig, dir);

            if (dot <= 0)
                return Vector3.Distance(P, orig); // before Start
            else if (dot >= length)
                return Vector3.Distance(P, orig + dir * length); // after End
            else
                return Vector3.Distance(P, orig + dir * dot); // between Start and End
        }

        public override string ToString()
        {
            return "Start: " + orig.ToString() + " -> End: " + End.ToString();
        }
    }

    /// <summary>
    /// A infinite line
    /// </summary>
    public struct Line2D
    {
        internal Vector2 orig, dir;

        public Vector2 Origin
        {
            get { return orig; }
            set { orig = value; }
        }
        public Vector2 Direction
        {
            get { return dir; }
            set
            {
                Debug.Assert(value.Normalize() > 1e-8f, "invalid direction normal");
                dir = value;
            }
        }

        /// <summary>
        /// Direction will be normalized for safety. Only in DEBUG mode i made a direction check
        /// </summary>
        public Line2D(Vector2 point, Vector2 dir)
        {
            float length = dir.Normalize();
#if DEBUG
            Debug.Assert(length > 1e-8f, "invalid direction normal");
#endif
            float t = Vector2.Dot(point, dir);

            this.dir = dir;
            this.orig = point - t * dir;
        }

        /// <summary>
        /// Return a ray using segment value, direction is oriented from start to end
        /// </summary>
        public static Line2D FromStartEnd(Vector2 start, Vector2 end)
        {
            return new Line2D(start, end - start);
        }

        /// <summary>
        /// A ray can be generalized to line but not viceversa
        /// </summary>
        public static implicit operator Line2D(Ray2D ray)
        {
            return new Line2D(ray.orig, ray.dir);
        }
        /// <summary>
        /// A segment can be generalized to line but not viceversa
        /// </summary>
        public static implicit operator Line2D(Segment2D seg)
        {
            return new Line2D(seg.orig, seg.dir);
        }

        public bool IsNaN
        {
            get { return orig.IsNaN || dir.IsNaN; }
        }

        public static readonly Line2D NaN = new Line2D { orig = Vector2.NaN, dir = Vector2.NaN };
    }
    /// <summary>
    /// A ray 
    /// </summary>
    public struct Ray2D
    {
        internal Vector2 orig, dir;

        public Vector2 Origin
        { 
            get { return orig; }
            set { orig = value; }
        }

        /// <summary>
        /// When set and u are in DEBUG mode, i check if direction isn't collapsed
        /// </summary>
        public Vector2 Direction
        {
            get { return dir; }
            set
            {
#if DEBUG
                Debug.Assert(value.Normalize() > 1e-8f, "invalid direction normal");
#endif
                dir = value;
            }
        }

        /// <summary>
        /// Direction will be normalized for safety
        /// </summary>
        public Ray2D(Vector2 Origin, Vector2 Direction)
        {
            orig = Origin;
            dir = Direction;
            Debug.Assert(dir.Normalize() > 1e-6f, "invalid direction normal");
        }

        /// <summary>
        /// Return a ray using segment value, direction is oriented from start to end
        /// </summary>
        public static Ray2D FromStartEnd(Vector2 start, Vector2 end)
        {
            return new Ray2D(start, end - start);
        }


        public Vector2 this[float t] { get { return orig + dir * t; } }


        public eAxis IsParallel 
        { 
            get
            {
                // work only if dir is normalized
                if (dir.x > 0.999999 || dir.x < -0.999999) return eAxis.X;
                else if (dir.y > 0.999999 || dir.y < -0.999999) return eAxis.Y;
                else return eAxis.None;
            } 
        }

        public bool IsNaN 
        { 
            get { return orig.IsNaN || dir.IsNaN; }
        }
        public static readonly Ray2D NaN = new Ray2D { orig = Vector2.NaN, dir = Vector2.NaN };
    }

    /// <summary>
    /// A segment, instead store only 2 point, i store origin direction and length because is more usefull
    /// </summary>
    public struct Segment2D
    {
        public Vector2 orig, dir;
        public float length;
        /// <summary>
        /// Direction will be normalized for safety
        /// </summary>
        public Segment2D(Vector2 P0, Vector2 P1)
        {
            orig = P0;
            dir = P1 - P0;
            length = dir.Normalize();
            Debug.Assert(length > 1e-6f, "invalid direction normal");
        }

        public bool IsNaN
        {
            get { return dir.IsNaN; }
        }

        public static readonly Segment2D NaN = new Segment2D { dir = Vector2.NaN };
    }

}
