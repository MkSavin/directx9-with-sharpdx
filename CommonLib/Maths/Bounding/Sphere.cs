﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using Engine.Tools;


namespace Engine.Maths
{
    /// <summary>
    /// Bounding Sphere
    /// </summary>
    public struct Sphere
    {
        Vector3 c;
        float r; // if radius < 0 is "Empty"

        public Vector3 center 
        { 
            get { return c; } 
            set { c = value; } 
        }
        public float radius 
        { 
            get { return r; }
            set { r = value; }
        }

        public Sphere(float x, float y, float z, float Radius)
            : this(new Vector3(x, y, z), Radius)
        {
        }

        public Sphere(Vector3 Center, float Radius)
        {
            c = Center;
            r = Radius;
        }

        public bool Intersect(Ray ray, out float t0, out float t1)
        {
            return Sphere.BoundaryIntersectRay(this, ray, out t0, out t1);
        }
        /// <summary> Sphere-Ray intersection, usefull know that if t0 &lt; 0 the ray start inside sphere </summary>
        /// <param name="t0">enter intersection at ray(t0)</param>
        /// <param name="t1">exit intersection at ray(t1)</param>
        /// <returns></returns>
        public static bool BoundaryIntersectRay(Vector3 Center, float Radius, Ray ray, out float t0, out float t1)
        {
            t0 = 0;
            t1 = 0;
            if (Radius < 0) return false;

            Vector3 p = ray.orig - Center;
            float a = Vector3.Dot(ray.dir, ray.dir);
            float b = 2.0f * Vector3.Dot(p, ray.dir);
            float c = Vector3.Dot(p, p) - (Radius * Radius);
            float d = (b * b) - (4 * a * c);

            // if discriminant is negative the picking ray missed the sphere, otherwise it intersected the sphere.
            if (d < 0.0f) return false;

            d = (float)Math.Sqrt(d);
            t0 = 0.5f * (-b + d) / a;
            t1 = 0.5f * (-b - d) / a;

            if (t1 < t0) MathUtils.SWAP(ref t1, ref t0);

            return true;
        }
        /// <summary>
        /// avoid the calculation of T0 and T1 , return true also if Ray Origin is inside sphere
        /// </summary>
        public static bool BoundaryIntersectRay(Vector3 Center, float Radius, Ray ray)
        {
            if (Radius < 0) return false;

            Vector3 p = ray.orig - Center;
            float a = Vector3.Dot(ray.dir, ray.dir);
            float b = 2.0f * Vector3.Dot(p, ray.dir);
            float c = Vector3.Dot(p, p) - (Radius * Radius);
            float d = (b * b) - (4 * a * c);

            // if discriminant is negative the picking ray missed the sphere, otherwise it intersected the sphere.
            if (d < 0.0f) return false;
            return true;
        }
              
        public static bool BoundaryIntersectRay(Sphere bs, Ray ray, out float t0, out float t1)
        {
            return BoundaryIntersectRay(bs.center, bs.radius, ray, out t0, out t1);
        }
        /// <summary>
        /// A void sphere radius = -1 , center = Zero, used example when you want never collisions 
        /// </summary>
        public static Sphere Zero
        {
            get { return new Sphere(Vector3.Zero, -1); }
        }
        /// <summary>
        /// A not usable sphere, not zero, used example when you want considerate the struct not processed or with some errors
        /// radius = -1 , center = NaN
        /// </summary>
        public static Sphere NaN
        {
            get { return new Sphere(Vector3.NaN, -1); }
        }
        public bool isNaN { get { return r < 0; } }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="inscribed">if true use the sphere inside box</param>
        /// <returns></returns>
        public static Sphere FromAABBox(IAABBox box, bool inscribed = false)
        {
            Vector3 halfsize = box.HalfSize;
            
            if (inscribed)
            {
                return new Sphere(box.Center, MathUtils.MIN(halfsize.x, halfsize.y, halfsize.z));
            }
            else
            {
                return new Sphere(box.Center, (float)Math.Sqrt(halfsize.x * halfsize.x + halfsize.y * halfsize.y + halfsize.z * halfsize.z));
            }
        }
        /// <summary>
        /// http://softsurfer.com/Archive/algorithm_0107/algorithm_0107.htm
        /// Copyright 2001, softSurfer (www.softsurfer.com)
        /// This code may be freely used and modified for any purpose
        /// providing that this copyright notice is included with it.
        /// SoftSurfer makes no warranty for this code, and cannot be held
        /// liable for any real or imagined damage resulting from its use.
        /// Users of this code must verify correctness for their application.
        /// </summary>
        public static Sphere FromDataFast(IList<Vector3> Points)
        {
            if (Points == null || Points.Count == 0)
                return Sphere.NaN;
            int count = Points.Count;
            Vector3 C = Vector3.Zero;
            float R = 0;
            //////////////////////////////////////////////////////// 1
            //  get three pair of points
            //  P1 (minimum x) P2 (maximum x)
            //  Q1 (minimum y) Q2 (maximum y)
            //  R1 (minimum z) R2 (maximum z)
            //  find the pair with mamimum distance (using square distance)
            float xmin = Points[0].x;
            float xmax = Points[0].x;
            float ymin = Points[0].y;
            float ymax = Points[0].y;
            float zmin = Points[0].z;
            float zmax = Points[0].z;
            int Pxmin = 0;
            int Pxmax = 0;
            int Pymin = 0;
            int Pymax = 0;
            int Pzmin = 0;
            int Pzmax = 0;


            //find a large diameter to start with
            for (int i = 0; i < count; i++)
            {
                if (Points[i].x < xmin)
                {
                    xmin = Points[i].x;
                    Pxmin = i;
                }
                else if (Points[i].x > xmax)
                {
                    xmax = Points[i].x;
                    Pxmax = i;
                }
                else if (Points[i].y < ymin)
                {
                    ymin = Points[i].y;
                    Pymin = i;
                }
                else if (Points[i].y > ymax)
                {
                    ymax = Points[i].y;
                    Pymax = i;
                }
                else if (Points[i].z < zmin)
                {
                    zmin = Points[i].y;
                    Pzmin = i;
                }
                else if (Points[i].z > zmax)
                {
                    zmax = Points[i].y;
                    Pzmax = i;
                }
            }
            Vector3 dx = Points[Pxmax] - Points[Pxmin];
            Vector3 dy = Points[Pymax] - Points[Pymin];
            Vector3 dz = Points[Pzmax] - Points[Pzmin];

            float dx2 = Vector3.GetLengthSquared(dx);
            float dy2 = Vector3.GetLengthSquared(dy);
            float dz2 = Vector3.GetLengthSquared(dz);

            float rad2;
            // pair x win
            if (dx2 > dy2 && dx2 > dz2)
            {
                C = Points[Pxmin] + (dx * 0.5f); //Center = midpoint of extremes
                rad2 = Vector3.GetLengthSquared(Points[Pxmax] - C);//radius squared
            }
            // pair y win
            else if (dy2 > dx2 && dy2 > dz2)
            {
                C = Points[Pymin] + (dy * 0.5f);
                rad2 = Vector3.GetLengthSquared(Points[Pymax] - C);
            }
            // pair z win
            else
            {
                C = Points[Pzmin] + (dz * 0.5f);
                rad2 = Vector3.GetLengthSquared(Points[Pzmax] - C);
            }
            float rad = (float)Math.Sqrt(rad2);


            ///////////////////////////////////////////////////////////// 2
            //  now check that all points V[i] are in the ball
            //  and if not, expand the ball just enough to include them

            Vector3 dV = Vector3.Zero;
            float dist, dist2;

            for (int i = 0; i < count; i++)
            {
                dV = Points[i] - C;
                dist2 = Vector3.GetLengthSquared(dV);

                if (dist2 > rad2)
                {
                    //V[i] not in ball, so expand ball to include it
                    dist = (float)Math.Sqrt(dist2);
                    rad = (rad + dist) / 2.0f; //enlarge radius just enough
                    rad2 = rad * rad;
                    C = Points[i] - dV * (rad / dist);  //shift Center toward V[i]
                    //C = C + dV * ((rad - dist) / dist);  //shift Center toward V[i]
                    Console.Write("");
                }
            }
            R = rad;

            return new Sphere(C, R);
        }
        /// <summary>
        /// calculate O(2n) Center = (Sum all vertices)/(num vertices) ,
        /// Radius = Max(dist(vertex i , center)).
        /// </summary>
        public static Sphere FromDataBasic(IList<Vector3> Points)
        {
            Vector3 C = Vector3.Zero;
            float R = 0.0f;

            float rad2 = 0.0f;

            foreach (Vector3 v in Points) C += v;
            C = C * (1.0f / Points.Count);

            foreach (Vector3 v in Points)
            {
                float dist2 = Vector3.GetLengthSquared(v - C);
                if (dist2 > rad2)
                {
                    rad2 = dist2;
                }
            }
            R = (float)Math.Sqrt(rad2);

            return new Sphere(C, R);
        }
        /// <summary>
        /// Merge two Sphere, if "Empty" aren't added
        /// </summary>
        public static Sphere operator +(Sphere a, Sphere b)
        {
            Vector3 v = b.c - a.c;
            float vl = v.Length;

            // b is inside a, b always inside if is b=="Empty"
            if (a.r >= vl + b.r) return a;
            // a is inside b, a always inside if is a=="Empty"
            if (b.r >= vl + a.r) return b;
            // a + b , if a & b == "Empty" the sum return always a negative radius
            return new Sphere(a.c + b.c + v * ((b.r - a.r) / vl) * 0.5f, (vl + a.r + b.r) * 0.5f);
        }

        public override string ToString()
        {
            if (this.isNaN) return "NULL_SPHERE";
            return string.Format("Center : {0,4} {1,4} {2,4} Radius : {3,4}\n", c.x, c.y, c.z , r);
        }
    }

    /// <summary>
    /// Bounding Circle
    /// </summary>
    public struct Circle
    {
        public Vector2 center;
        public float radius;

        public Circle(Vector2 Center, float Radius)
        {
            this.center = Center;
            this.radius = Radius;
        }

        public Circle(float cx, float cy, float Radius)
            : this(new Vector2(cx, cy), Radius) { }


        public static Circle Empty
        {
            get { return new Circle(Vector2.Zero, -1); }
        }

        public bool IsEmpty
        {
            get { return radius < 0; }
        }

        public float Area
        {
            get { return (float)Math.PI * radius * radius; }
        }

        /// <summary>
        /// Convert a 2d rectangle to a screen rectangle where X axis is same and Y inverted
        /// </summary>
        /// <param name="Client">the screen bound in pixel size</param>
        /// <param name="World">the screen bound in 2d coordinates</param>
        public RectangleUV ConvertToScreen(RectangleUV Client, RectangleAA World)
        {
            Vector2 min = center - radius;
            Vector2 max = center + radius;

            int x0 = (int)MathUtils.Interpolate(min.x, Client.Left, Client.Right, World.min.x, World.max.x);
            int x1 = (int)MathUtils.Interpolate(max.x, Client.Left, Client.Right, World.min.x, World.max.x);
            int y0 = (int)MathUtils.Interpolate(min.y, Client.Bottom, Client.Top, World.min.y, World.max.y);
            int y1 = (int)MathUtils.Interpolate(max.y, Client.Bottom, Client.Top, World.min.y, World.max.y);

            if (x0 > x1) MathUtils.SWAP(ref x0, ref x1);
            if (y0 > y1) MathUtils.SWAP(ref y0, ref y1);

            return RectangleUV.FromMinMax(x0, y0, x1, y1);
        }


        public override string ToString()
        {
            if (this.IsEmpty) return "NULL_SPHERE";
            return string.Format("Center : {0,4} {1,4} Radius : {2,4}\n", center.x, center.y, radius);
        }
    }
}
