﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using Engine.Tools;


namespace Engine.Maths
{
    /// <summary>
    /// Axis Aligned Bounding Box
    /// </summary>
    /// <remarks>
    /// <para>   6-----7 </para>
    /// <para>  /     /| </para>
    /// <para> 2-----3 5 </para>
    /// <para> |     | / </para>
    /// <para> 0-----1   </para>
    /// </remarks>
    public interface IAABBox
    {
        Vector3 Max { get; }
        Vector3 Min { get; }
        Vector3 Center { get; }
        Vector3 HalfSize { get; }
    }

    /// <summary>
    /// Axis Aligned Bounding Box with max and min version
    /// </summary>
    public struct AABBox : IAABBox
    {
        public Vector3 max, min;


        public AABBox(Vector3 Min, Vector3 Max)
        {
            max = Max;
            min = Min;
            minmaxfix();
        }
        public AABBox(float minx, float miny, float minz, float maxx, float maxy, float maxz)
            : this(new Vector3(minx, miny, minz), new Vector3(maxx, maxy, maxz))
        {
        }

        public Vector3 Max
        {
            get { return max; }
            set { max = value; }
        }
        public Vector3 Min
        {
            get { return min; }
            set { min = value; }
        }

        public Vector3 Center
        {
            get { return (max + min) * 0.5f; }
            set
            {
                Vector3 halfsize = (max - min) * 0.5f;
                min = value - halfsize;
                max = value + halfsize;
            }
        }
        public Vector3 HalfSize { get { return (max - min) * 0.5f; } }
        public Vector3 Corner0 { get { return new Vector3(min.x, min.y, min.z); } }
        public Vector3 Corner1 { get { return new Vector3(min.x, min.y, max.z); } }
        public Vector3 Corner2 { get { return new Vector3(min.x, max.y, min.z); } }
        public Vector3 Corner3 { get { return new Vector3(min.x, max.y, max.z); } }
        public Vector3 Corner4 { get { return new Vector3(max.x, min.y, min.z); } }
        public Vector3 Corner5 { get { return new Vector3(max.x, min.y, max.z); } }
        public Vector3 Corner6 { get { return new Vector3(max.x, max.y, min.z); } }
        public Vector3 Corner7 { get { return new Vector3(max.x, max.y, max.z); } }


        public static AABBox FromHalfSize(Vector3 center, Vector3 halfsize)
        {
            return new AABBox(center - halfsize, center + halfsize);
        }

        /// <summary>
        /// Only traslation and scale component, rotation are not allowed
        /// </summary>
        public AABBox(Matrix4 transform)
        {
            Vector3 scale = transform.ExtractScale();
            Vector3 position = transform.Position;

            max = new Vector3(position.x + scale.x / 2.0f, position.y + scale.y / 2.0f, position.z + scale.z / 2.0f);
            min = new Vector3(position.x - scale.x / 2.0f, position.y - scale.y / 2.0f, position.z - scale.z / 2.0f);

            minmaxfix();
        }

        void minmaxfix()
        {
            if (max.x < min.x) MathUtils.SWAP(ref max.x, ref min.x);
            if (max.y < min.y) MathUtils.SWAP(ref max.y, ref min.y);
            if (max.z < min.z) MathUtils.SWAP(ref max.z, ref min.z);
        }

        public bool isPointInside(Vector3 p)
        {
            return isPointInside(p.x, p.y, p.z);
        }

        public bool isPointInside(float x, float y, float z)
        {
            return x <= max.x &&
                   y <= max.y &&
                   z <= max.z &&
                   x >= min.x &&
                   y >= min.y &&
                   z >= min.z;
        }

        /// <summary>
        /// Return an empty box, where max = -INF , min = +INF so when merging or adding
        /// a box or a point the result is always the added element
        /// </summary>
        public static AABBox NaN
        {
            get
            {
                AABBox empty = new AABBox();
                // without safety check
                empty.max = new Vector3(float.MinValue, float.MinValue, float.MinValue);
                empty.min = new Vector3(float.MaxValue, float.MaxValue, float.MaxValue);
                return empty;
            }
        }

        public bool isNaN
        {
            get
            {
                return
                    max.x < min.x ||
                    max.y < min.y ||
                    max.z < min.z ||
                    max.isNaN ||
                    min.isNaN;
            }
        }
        /// <summary>
        /// Get the Axis aligned boundary from a vertices array
        /// </summary>
        public static AABBox FromData(IList<Vector3> Points)
        {
            AABBox aabb = AABBox.NaN;
            int count = Points.Count;
            if (Points == null || count == 0) return aabb;
            for (int i = 0; i < count; i++) aabb.Merge(Points[i]);
            return aabb;
        }

        /// <summary>
        /// Get the Axis aligned boundary from a sphere
        /// </summary>
        public static AABBox FromSphere(Sphere bsphere)
        {
            AABBox aabb = AABBox.NaN;
            aabb.max.x = bsphere.center.x + bsphere.radius;
            aabb.max.y = bsphere.center.y + bsphere.radius;
            aabb.max.z = bsphere.center.z + bsphere.radius;
            aabb.min.x = bsphere.center.x - bsphere.radius;
            aabb.min.y = bsphere.center.y - bsphere.radius;
            aabb.min.z = bsphere.center.z - bsphere.radius;
            aabb.minmaxfix();
            return aabb;
        }

        /// <summary>
        /// Update box to include this point, more efficent than AABBox2 
        /// </summary>
        public void Merge(Vector3 point)
        {
            if (point.x > max.x) max.x = point.x;
            if (point.y > max.y) max.y = point.y;
            if (point.z > max.z) max.z = point.z;
            if (point.x < min.x) min.x = point.x;
            if (point.y < min.y) min.y = point.y;
            if (point.z < min.z) min.z = point.z;
        }

        /// <summary>
        /// Update box to include this box
        /// </summary>
        public void Merge(IAABBox box)
        {
            // if a == "NaN" will not be added because a.max.X are always < than b.max.X
            // if a and b == "NaN" the result is a Empty box
            max.x = Math.Max(max.x, box.Max.x);
            max.y = Math.Max(max.y, box.Max.y);
            max.z = Math.Max(max.z, box.Max.z);

            min.x = Math.Min(min.x, box.Min.x);
            min.y = Math.Min(min.y, box.Min.y);
            min.z = Math.Min(min.z, box.Min.z);
        }

        /// <summary>
        /// Merge two Bounding Box, 
        /// </summary>
        public static AABBox operator +(AABBox a, AABBox b)
        {
            // if a == "NaN" will not be added because a.max.X are always < than b.max.X
            // if a and b == "NaN" the result is a Empty box
            AABBox sum = a;
            sum.Merge(b);
            return sum;
        }

        /// <summary>
        /// implicit conversion example :
        /// <code>
        /// AABBox box1 = new AABBox();
        /// AABBox2 box2 = box1;
        /// </code>
        /// </summary>
        public static implicit operator AABBox(AABBox2 box)
        {
            return AABBox.FromHalfSize(box.center, box.halfsize);
        }


        public override string ToString()
        {
            if (this.isNaN) return "NULL_AABB";

            StringBuilder str = new StringBuilder();
            str.Append(string.Format("MAX : {0,4} {1,4} {2,4}\n", max.x, max.y, max.z));
            str.Append(string.Format("Min : {0,4} {1,4} {2,4}\n", min.x, min.y, min.z));

            return str.ToString();
        }
    }

    /// <summary>
    /// Axis Aligned Bounding Box with halfsize and center version
    /// </summary>
    public struct AABBox2 : IAABBox
    {
        public Vector3 halfsize, center;

        public AABBox2(Vector3 center, Vector3 halfsize)
        {
            this.center = center;
            this.halfsize = halfsize;
        }
        public AABBox2(float cx, float cy, float cz, float hx, float hy, float hz)
            : this(new Vector3(cx, cy, cz), new Vector3(hx, hy, hz))
        {
        }

        public Vector3 Max { get { return center + halfsize; } }
        public Vector3 Min { get { return center - halfsize; } }
        public Vector3 Center
        {
            get { return center; }
            set { center = value; }
        }
        public Vector3 HalfSize
        {
            get { return halfsize; }
            set { halfsize = value; }
        }

        public Vector3 Corner0 { get { return new Vector3(center.x - halfsize.x, center.y - halfsize.y, center.z - halfsize.z); } }
        public Vector3 Corner1 { get { return new Vector3(center.x - halfsize.x, center.y - halfsize.y, center.z + halfsize.z); } }
        public Vector3 Corner2 { get { return new Vector3(center.x - halfsize.x, center.y + halfsize.y, center.z - halfsize.z); } }
        public Vector3 Corner3 { get { return new Vector3(center.x - halfsize.x, center.y + halfsize.y, center.z + halfsize.z); } }
        public Vector3 Corner4 { get { return new Vector3(center.x + halfsize.x, center.y - halfsize.y, center.z - halfsize.z); } }
        public Vector3 Corner5 { get { return new Vector3(center.x + halfsize.x, center.y - halfsize.y, center.z + halfsize.z); } }
        public Vector3 Corner6 { get { return new Vector3(center.x + halfsize.x, center.y + halfsize.y, center.z - halfsize.z); } }
        public Vector3 Corner7 { get { return new Vector3(center.x + halfsize.x, center.y + halfsize.y, center.z + halfsize.z); } }

        public static AABBox2 FromMinMax(Vector3 Min, Vector3 Max)
        {
            return new AABBox2((Max + Min) * 0.5f, (Max - Min) * 0.5f);
        }

        /// <summary>
        /// Only traslation and scale component, rotation are not allowed
        /// </summary>
        public AABBox2(Matrix4 transform)
        {
            Vector3 scale = transform.ExtractScale();
            Vector3 position = transform.Position;

            this.center = position;
            this.halfsize = scale * 0.5f;
        }

        public bool isPointInside(Vector3 p)
        {
            return isPointInside(p.x, p.y, p.z);
        }
        public bool isPointInside(float x, float y, float z)
        {
            return x <= center.x + halfsize.x &&
                   y <= center.y + halfsize.y &&
                   z <= center.z + halfsize.z &&
                   x >= center.x - halfsize.x &&
                   y >= center.y - halfsize.y &&
                   z >= center.z - halfsize.z;
        }

        /// <summary>
        /// Update box to include this point, less efficent that AABBox
        /// </summary>
        public void Merge(Vector3 point)
        {
            Vector3 max = Max;
            Vector3 min = Min;

            if (point.x > max.x) max.x = point.x;
            if (point.y > max.y) max.y = point.y;
            if (point.z > max.z) max.z = point.z;
            if (point.x < min.x) min.x = point.x;
            if (point.y < min.y) min.y = point.y;
            if (point.z < min.z) min.z = point.z;

            center = (max + min) * 0.5f;
            halfsize = (max - min) * 0.5f;
        }

        /// <summary>
        /// Update box to include this box
        /// </summary>
        public void Merge(IAABBox box)
        {
            // precompute to avoid continuos get{} function
            Vector3 amax = Max;
            Vector3 amin = Min;
            Vector3 bmax = box.Max;
            Vector3 bmin = box.Min;

            // if a == "NaN" will not be added because a.max.X are always < than b.max.X
            // if a and b == "NaN" the result is a Empty box
            amax.x = Math.Max(amax.x, bmax.x);
            amax.y = Math.Max(amax.y, bmax.y);
            amax.z = Math.Max(amax.z, bmax.z);

            amin.x = Math.Min(amin.x, bmin.x);
            amin.y = Math.Min(amin.y, bmin.y);
            amin.z = Math.Min(amin.z, bmin.z);

            center = (amax + amin) * 0.5f;
            halfsize = (amax - amin) * 0.5f;
        }


        public static AABBox2 NaN
        {
            get
            {
                AABBox2 empty = new AABBox2();
                // without safety check
                empty.halfsize = new Vector3(float.MinValue, float.MinValue, float.MinValue);
                empty.center = Vector3.Zero;
                return empty;
            }
        }
        public bool isNaN
        {
            get
            {
                return
                    halfsize.x < 0 ||
                    halfsize.y < 0 ||
                    halfsize.z < 0 ||
                    halfsize.isNaN ||
                    center.isNaN;
            }
        }
        /// <summary>
        /// Get the Axis aligned boundary from a vertices array
        /// </summary>
        public static AABBox2 FromData(IList<Vector3> Points)
        {
            AABBox2 aabb = AABBox2.NaN;
            int count = Points.Count;

            if (Points == null || count == 0)
                return aabb;

            return (AABBox2)(AABBox.FromData(Points));
        }

        /// <summary>
        /// Get the Axis aligned boundary from a sphere
        /// </summary>
        public static AABBox2 FromSphere(Sphere bsphere)
        {
            return new AABBox2(bsphere.center, new Vector3(bsphere.radius, bsphere.radius, bsphere.radius));
        }

        /// <summary>
        /// Merge two Bounding Box, 
        /// </summary>
        public static AABBox2 operator +(AABBox2 a, AABBox2 b)
        {
            // if a == "Empty" will not be added because a.max.X are always < than b.max.X
            // if a and b == "Empty" the result is a Empty box

            Vector3 maxa = a.Max;
            Vector3 maxb = b.Max;
            Vector3 mina = a.Min;
            Vector3 minb = b.Min;

            Vector3 max = new Vector3(Math.Max(maxa.x, maxb.x), Math.Max(maxa.y, maxb.y), Math.Max(maxa.z, maxb.z));
            Vector3 min = new Vector3(Math.Min(mina.x, minb.x), Math.Min(mina.y, minb.y), Math.Min(mina.z, minb.z));

            return AABBox2.FromMinMax(min, max);
        }

        /// <summary>
        /// implicit conversion example :
        /// <code>
        /// AABBox box1 = new AABBox();
        /// AABBox2 box2 = box1;
        /// </code>
        /// </summary>
        public static implicit operator AABBox2(AABBox box)
        {
            return AABBox2.FromMinMax(box.min, box.max);
        }


        public override string ToString()
        {
            if (this.isNaN) return "NULL_AABB2";

            StringBuilder str = new StringBuilder();
            str.Append(string.Format("Center : {0,4} {1,4} {2,4}\n", center.x, center.y, center.z));
            str.Append(string.Format("HSize  : {0,4} {1,4} {2,4}\n", halfsize.x, halfsize.y, halfsize.z));

            return str.ToString();
        }
    }


}