﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using Engine.Tools;


namespace Engine.Maths
{
    /// <summary>
    /// Oriented Bounding Rectangle
    /// </summary>
    public struct RectangleO
    {
        private const float pi2 = (float)(Math.PI * 2);

        private float angle;

        public Vector2 origin, axisX, halfsize;


        public Vector2 Center
        {
            get { return origin; }
        }
        public Vector2 HalfSize
        {
            get { return halfsize; }
        }
        public Vector2 AxisX
        {
            get { return axisX; }
            set { axisX = value; angle = (float)Math.Acos(Vector2.Dot(axisX, Vector2.UnitX)); }
        }
        public Vector2 AxisY
        {
            get { return new Vector2(-axisX.y, axisX.x); }
            set { axisX = new Vector2(value.y, -value.x); }
        }
        public float Angle
        {
            get { return angle; }
            set { angle = value % pi2; axisX = new Vector2(Math.Cos(angle), -Math.Sin(angle)); }
        }

        public Matrix3 Transform
        {
            get { return Matrix3.TRS(origin.x, origin.y, halfsize.x, halfsize.y, angle); }
        }

        public Vector2 Corner0
        {
            get 
            {
                Vector2 p = origin;
                p -= AxisX * HalfSize.x;
                p -= AxisY * HalfSize.y;
                return p;
            }
        }
        public Vector2 Corner1
        {
            get
            {
                Vector2 p = origin;
                p += AxisX * HalfSize.x;
                p -= AxisY * HalfSize.y;
                return p;
            }
        }
        public Vector2 Corner2
        {
            get
            {
                Vector2 p = origin;
                p += AxisX * HalfSize.x;
                p += AxisY * HalfSize.y;
                return p;
            }
        }
        public Vector2 Corner3
        {
            get
            {
                Vector2 p = origin;
                p -= AxisX * HalfSize.x;
                p += AxisY * HalfSize.y;
                return p;
            }
        }
        /// <summary>
        /// </summary>
        /// <param name="Angle">clockwise rotation around center, where Vector2.UnitX is zero</param>
        public RectangleO(Vector2 Center, Vector2 HalfSize, float Angle)
        {
            this.origin = Center;
            this.halfsize = HalfSize;
            this.axisX = new Vector2(Math.Cos(Angle), -Math.Sin(Angle));
            this.angle = Angle;
        }

        public RectangleO(float cx, float cy, float hx, float hy, float angle) :
            this(new Vector2(cx, cy), new Vector2(hx, hy), angle)
        {
        }

        /// <summary>
        /// center = Zero , halfsize = 1 , rotation = 0 -> the corners are in range [-1,1]
        /// </summary>
        public static RectangleO Unit()
        {
            return new RectangleO(0, 0, 1, 1, 0);
        }


        public override string ToString()
        {
            return string.Format("C {0} , Angle {1}", origin.ToString(), MathUtils.RadianToDegree(angle));
        }
    }
}
