﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace Engine.Maths
{
    /// <summary>
    /// A LeftTop Rectangle area for texture usage, the uv coordinated system rappresent the xy values where
    /// minimun corner uv(0,0) is (Left,Top) , maximum corner uv(1,1) is (Right,Bottom)
    /// 
    /// Remember : the smaller rectangle has Width and Height = 1 so min = max = (0,0)
    /// 
    /// </summary>
    /// <remarks>
    /// <para> +------ U,Width </para>
    /// <para> |               </para>
    /// <para> |               </para>
    /// <para> V,Height        </para>
    /// </remarks>
    public struct RectangleUV
    {
        /// <summary>
        /// Minimum corner
        /// </summary>
        public int X, Y;
        /// <summary>
        /// Edges size
        /// </summary>
        public int Width, Height;


        /// <summary>
        /// 
        /// </summary>
        public static RectangleUV FromPoint(int x0, int y0, int x1, int y1, int minX, int minY, int maxX, int maxY)
        {
            if (x0 > maxX) x0 = maxX;
            if (x1 > maxX) x1 = maxX;
            if (y0 > maxY) y0 = maxY;
            if (y1 > maxY) y1 = maxY;
            return RectangleUV.FromPoint(x0, x1, y0, y1, minX, minY);
        }

        /// <summary>
        /// 
        /// </summary>
        public static RectangleUV FromPoint(int x0, int y0, int x1, int y1, int minX,int minY)
        {
            if (x0 < minX) x0 = minX;
            if (x1 < minX) x1 = minX;
            if (y0 < minY) y0 = minY;
            if (y1 < minY) y1 = minY;

            if (x0 > x1) MathUtils.SWAP(ref x0, ref x1);
            if (y0 > y1) MathUtils.SWAP(ref y0, ref y1);

            return RectangleUV.FromSize(x0, y0, x1 - x0 + 1, y1 - y0 + 1);
        }


        /// <summary>
        /// From generic corners points, smaller rectangle is x0=x1 and y0=y1
        /// </summary>
        public static RectangleUV FromPoint(int x0, int y0, int x1, int y1)
        {
            if (x0 > x1) MathUtils.SWAP(ref x0, ref x1);
            if (y0 > y1) MathUtils.SWAP(ref y0, ref y1);
            return RectangleUV.FromSize(x0, y0, x1 - x0 + 1, y1 - y0 + 1);
        }

        /// <summary>
        /// </summary>
        public static RectangleUV FromSize(int u, int v, int width, int height)
        {
#if DEBUG
            if (u < 0 || v < 0) 
                throw new ArgumentOutOfRangeException("must be positive");
            if (width < 1 || height < 1) 
                throw new ArgumentOutOfRangeException("must be positive and at least 1");
#endif
            RectangleUV rect = new RectangleUV();
            rect.X = u;
            rect.Y = v;
            rect.Width = width;
            rect.Height = height;
            return rect;
        }

        /// <summary>
        /// </summary>
        public static RectangleUV FromMinMax(int umin, int vmin, int umax, int vmax)
        {
#if DEBUG
            if (umin < 0 || vmin < 0 || umax < 0 || vmax < 0)
                throw new ArgumentOutOfRangeException("must be positive");

            if (umin > umax || vmin > vmax)
                throw new ArgumentOutOfRangeException("min greater than max");
#endif

            RectangleUV rect = new RectangleUV();
            rect.X = umin;
            rect.Y = vmin;
            rect.Width = umax - umin + 1;
            rect.Height = vmax - vmin + 1;
            return rect;
        }

        public int Left
        {
            get { return X; }
        }

        public int Top
        {
            get { return Y; }
        }
        /// <summary>
        /// remark that if x=0 and Width=100 ---&gt; right = 99 and left = 0
        /// </summary>
        public int Right
        {
            get { return X + Width - 1; }
        }
        /// <summary>
        /// remark that if y=0 and Height=100 ---&gt; bottom = 99 and top = 0
        /// </summary>
        public int Bottom
        {
            get { return Y + Height - 1; }
        }

        /// <summary>
        /// Convert a rectangle to a 2d rectangle where X axis is same and Y inverted
        /// </summary>
        /// <param name="Client">the screen bound in pixel size</param>
        /// <param name="World">the screen bound in 2d coordinates</param>
        public RectangleAA ConvertToWorld(RectangleUV Client, RectangleAA World)
        {
            float x0 = MathUtils.Interpolate(Left, World.min.x, World.max.x, Client.Left, Client.Right);
            float x1 = MathUtils.Interpolate(Right, World.min.x, World.max.x, Client.Left, Client.Right);
            float y0 = MathUtils.Interpolate(Bottom, World.min.y, World.max.y, Client.Bottom, Client.Top);
            float y1 = MathUtils.Interpolate(Top, World.min.y, World.max.y, Client.Bottom, Client.Top);

            if (x0 > x1) MathUtils.SWAP(ref x0, ref x1);
            if (y0 > y1) MathUtils.SWAP(ref y0, ref y1);

            return new RectangleAA(x0, y0, x1, y1);
        }

        public static implicit operator RectangleUV(Rectangle rect)
        {
            return RectangleUV.FromSize(rect.X, rect.Y, rect.Width, rect.Height);
        }
        public static implicit operator Rectangle(RectangleUV rect)
        {
            return new Rectangle(rect.X, rect.Y, rect.Width, rect.Height);
        }

        public override string ToString()
        {
            return string.Format("L {0}, T {1}, R {2}, B {3}", Left, Top, Right, Bottom);
        }
    }
}
