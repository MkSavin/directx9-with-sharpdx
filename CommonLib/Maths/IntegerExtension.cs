﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace Engine.Maths
{   
    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 8, Size = 16)]
    public struct Uint128
    {
        public ulong low , high;

        public static Uint128 operator <<(Uint128 value, int i)
        {
            return new Uint128();
        }
        public static Uint128 operator >>(Uint128 value, int i)
        {
            return new Uint128();
        }
        public static Uint128 operator &(Uint128 value, Uint128 mask)
        {
            return new Uint128();
        }
    }
    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 8, Size = 16)]
    public struct UInt24
    {
        ushort low;
        byte high;

        public static readonly int MaxValue = 0x00FFFFFF;

        public static explicit operator UInt32(UInt24 value)
        {
            return (uint)(value.low | (value.high << 16));
        }
        public static explicit operator UInt24(UInt32 value)
        {
            return new UInt24 { low = (ushort)(value & 0x0000FFFF), high = (byte)((value & 0x00FF0000) >> 16) };
        }
        public static UInt24 operator +(UInt24 a, UInt24 b)
        {
            return (UInt24)((uint)a + (uint)b);
            
        }
        public override string ToString()
        {
            return ((uint)this).ToString();
        }
    }
}
