﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Drawing;

using Engine.Tools;
using System.Drawing.Drawing2D;

namespace Engine.Maths
{
    /// <summary>
    /// X,Y,Z flags
    /// </summary>
    [Flags]
    public enum eAxis : byte
    {
        /// <summary>000</summary>
        None = 0x00,
        /// <summary>001</summary>
        X = 0x01,
        /// <summary>010</summary>
        Y = 0x02,
        /// <summary>100</summary>
        Z = 0x04,
        /// <summary>011</summary>
        XY = X | Y,
        /// <summary>101</summary>
        XZ = X | Z,
        /// <summary>110</summary>
        YZ = Y | Z,
        /// <summary>111</summary>
        XYZ = X | Y | Z
    }

    /// <summary>
    /// The six faces index of a Texture cube
    /// </summary>
    public enum MapFace : byte
    {
        PositiveX = 0,
        NegativeX = 1,
        PositiveY = 2,
        NegativeY = 3,
        PositiveZ = 4,
        NegativeZ = 5,
    }

    /// <summary>
    /// Collections of all Maths utilities
    /// </summary>
    public static class MathUtils
    {
        /// <summary>
        /// one random class for all cases
        /// </summary>
        static Random rnd = new Random();

        const uint FSIGMASK = 0x80000000; //float sign mask
        const uint FEXPMASK = 0x7F800000; //float exponent mask
        const uint FFRAMASK = 0x007FFFFF; //float fraction mask
        const uint FSIGMASKneg = ~0x80000000; //float sign mask negative

        const float PI180 = (float)(Math.PI / 180.0);

        public const float Sqrt2 = 1.41421356237309504880168872420969807856967187537694f;
        public const float Sqrt3 = 1.73205080756887729352744634150587236694280525381038062805580f;

        public const float Rad30 = (float)(Math.PI / 6.0);
        public const float Rad45 = (float)(Math.PI / 3.0);
        public const float Rad60 = (float)(Math.PI / 4.0);
        public const float Rad90 = (float)(Math.PI / 2.0);
        public const float Rad180 = (float)Math.PI;
        public const float Rad225 = (float)(Math.PI * 3.0 / 2.0);
        public const float Rad270 = (float)(Math.PI * 5.0 / 4.0);
        public const float Rad315 = (float)(Math.PI * 7.0 / 4.0);

        public static float GetRandomFloat(float min = 0.0f, float max = 1.0f)
        {
            return min + (float)rnd.NextDouble() * (max - min);
        }
        public static float GetRandomFloat(float max)
        {
            return (float)rnd.NextDouble() * max;
        }
        public static double GetRandomDouble(double max)
        {
            return rnd.NextDouble() * max;
        }

        /// <summary>
        /// In my version max value is inclusive, so can be returned
        /// </summary>
        public static int GetRandomInt(int min = 0, int max = 1)
        {
            return rnd.Next(min, max + 1);
        }

        public static int[] powersOfTwo = new int[] { 1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048 };

        public static bool isZero(double x) { return (x > -1e-7) && (x < 1e-7); }
        public static bool isZero(float x) { return (x > -1e-7) && (x < 1e-7); }
        /// <summary>
        /// Cubic root
        /// </summary>
        public static double Cbrt(double x) { return Math.Pow(x, 1.0 / 3.0); }

        /// <summary>
        /// </summary>
        /// <param name="Degree"></param>
        /// <returns>Radians</returns>
        public static float DegreeToRadian(float Degree) { return (float)(Degree * PI180); }
        /// <summary>
        /// TODO : convert for directxLH
        /// </summary>
        /// <param name="r">radial distance </param>
        /// <param name="t">inclination </param>
        /// <param name="g">azimuth </param>
        public static void CartesianToSpherical(float x, float y, float z, out float r, out float t, out float g)
        {
            r = (float)Math.Sqrt(x * x + y * y + z * z);
            t = (float)Math.Acos(-z / r);
            g = (float)Math.Atan(y / r);
        }
        /// <summary>
        /// TODO : convert for directxLH
        /// </summary>
        /// <param name="r">radius </param>
        /// <param name="t">inclination </param>
        /// <param name="o">azimuth </param>
        public static void SphericalToCartesian(float r, float t, float g, out float x, out float y, out float z)
        {
            x = (float)(r * Math.Sin(t) * Math.Cos(g));
            y = (float)(r * Math.Sin(t) * Math.Sin(g));
            z = (float)(r * Math.Cos(t));
        }
        /// <summary>
        /// </summary>
        /// <param name="r">radial distance</param>
        /// <param name="g">azimuth </param>
        /// <param name="h">equal to y</param>
        public static void CartesianToCylindrical(float x, float y, float z, out float r, out float g, out float h)
        {
            r = (float)Math.Sqrt(x * x + z * z);
            g = (float)Math.Asin(z / r);
            h = y;
        }
        /// <summary>
        /// </summary>
        /// <param name="Radian"></param>
        /// <returns>Degree</returns>
        public static float RadianToDegree(float Radian)
        {
            return (float)(180.0 * Radian / Math.PI);
        }
        /// <summary>
        /// Linearly interpolates between two values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="amount">amount = 0 return value1 , amount = 1 return value2</param>
        /// <returns></returns>
        public static float Lerp(float value1, float value2, float amount)
        {
            return value1 + (value2 - value1) * amount;
        }
        /// <summary>
        /// Is power or 2 ?
        /// </summary>
        public static bool IsPowOf2(int value)
        {
            if (value < 1) return false;
            return (value & (value - 1)) == 0;
        }
        /// <summary>
        /// n!
        /// </summary>
        public static float fattoriale(float n)
        {
            float result = 1;
            for (int i = 1; i <= n; i++)
                result *= i;
            return result;
        }

        public static int ABS(int i) { return i > 0 ? i : -i; }
        public static float ABS(float f) { return f > 0 ? f : -f; }
        public static double ABS(double d) { return d > 0 ? d : -d; }

        /// <summary>
        /// using bit operations to remove the bit of sign, X1.5 faster, isn't a very improvement
        /// </summary>
        public static unsafe float ABS2(float f)
        {
            uint i = *((uint*)&f) & FSIGMASKneg;
            return *((float*)&i);
        }

        public static int MIN(int a, int b) { return a < b ? a : b; }
        public static int MIN(int a, int b, int c) { return a < b ? MIN(a, c) : MIN(b, c); }
        public static float MIN(float a, float b) { return a < b ? a : b; }
        public static float MIN(float a, float b, float c) { return a < b ? MIN(a, c) : MIN(b, c); }

        public static int MAX(int a, int b) { return a > b ? a : b; }
        public static int MAX(int a, int b, int c) { return a > b ? MAX(a, c) : MAX(b, c); }
        public static float MAX(float a, float b) { return a > b ? a : b; }
        public static float MAX(float a, float b, float c) { return a > b ? MAX(a, c) : MAX(b, c); }
        public static void MINMAX(float x0, float x1, float x2, out float min, out float max)
        {
            min = max = x0;
            if (x1 < min) min = x1;
            if (x1 > max) max = x1;
            if (x2 < min) min = x2;
            if (x2 > max) max = x2;
        }
        public static void SWAP(ref float val1, ref float val2) { float tmp = val1; val1 = val2; val2 = tmp; }
        public static void SWAP(ref int val1, ref int val2) { int tmp = val1; val1 = val2; val2 = tmp; }
        public static void SWAP<T>(ref T val1, ref T val2) { T tmp = val1; val1 = val2; val2 = tmp; }
        public static void SWAP<T>(List<T> list, int i, int j) { T temp = list[i]; list[i] = list[j]; list[j] = temp; }

        /// <summary>
        /// Swap integer with Xor algorithm, only because is a nice feature
        /// </summary>
        public static void XORSWAP(ref int x, ref int y) { if (x != y) { x ^= y; y ^= x; x ^= y; } }

        /// <summary>
        /// return n^2;
        /// </summary>
        public static int NPow2(int n) { return n << 1; }

        public static float Interpolate(float x, float miny, float maxy, float minx, float maxx)
        {
            return miny + (maxy - miny) * (x - minx) / (maxx - minx);
        }
        /// <summary>
        /// Isn't a casting conversion (truncation to smaller integer) but a bit2bit copy
        /// </summary>
        public static unsafe int ToInt32(float f)
        {
            return *((int*)&f);
        }
        public static unsafe uint ToUInt32(float f)
        {
            return *((uint*)&f);
        }
        /// <summary>
        /// Isn't a casting conversion but a bit2bit copy
        /// </summary>
        public static unsafe float ToFloat32(int i)
        {
            return *((float*)&i);
        }
        public static unsafe float ToFloat32(uint i)
        {
            return *((float*)&i);
        }
        /// <summary>
        /// from http://www.cygnus-software.com/papers/comparingfloats/comparingfloats.htm
        /// </summary>
        /// <param name="maxDeltaBits">if 0, return true if two float have same bits</param>
        public static bool AlmostEqual(float a, float b, int maxDeltaBits = 1)
        {
            int aInt = ToInt32(a);
            if (aInt < 0) aInt = Int32.MinValue - aInt;
            int bInt = ToInt32(b);
            if (bInt < 0) bInt = Int32.MinValue - bInt;
            int intDiff = ABS(aInt - bInt);
            return intDiff <= (1 << maxDeltaBits);
        }

        /// <summary>
        /// Nice numbers for graph labels http://books.google.com/books?id=fvA7zLEFWZgC&pg=PA61&lpg=PA61#v=onepage&q&f=false
        /// </summary>
        public static void LooseLabel(float graphmin, float graphmax, int ticks, 
            out float roundmin, out float roundmax, out float roundelta, out float roundigit)
        {
            double range = nicenumber(graphmax - graphmin, false);
            double delta = nicenumber(range / (ticks - 1));
            roundmin = (float)(Math.Floor(graphmin / delta) * delta);
            roundmax = (float)(Math.Ceiling(graphmax / delta) * delta);
            roundigit = (float)Math.Max(-Math.Floor(Math.Log10(delta)), 0);
            roundelta = (float)delta;
        }


        static double nicenumber(double num, bool round = true)
        {
            double exp = Math.Floor(Math.Log10(num));
            double frc = num / Math.Pow(10, exp);
            double rnd = 10.0;
            if (round)
            {
                if (frc < 1.5) rnd = 1;
                else if (frc < 3) rnd = 2;
                else if (frc < 7) rnd = 5;
            }
            else
            {
                if (frc <= 1) rnd = 1;
                else if (frc <= 2) rnd = 2;
                else if (frc <= 5) rnd = 5;
            }
            return rnd * Math.Pow(10, exp);
        }


        //System.Math.Sqrt() faster than all custom implementation... also in Release compile mode
        unsafe static float FastSqrt(float x)
        {
            uint i = *(uint*)&x;
            // adjust bias
            i += 127 << 23;
            // approximation of square root
            i >>= 1;
            return *(float*)&i;
        }
        static float FastSqrtQuake3Union(float x)
        {
            if (x == 0) return 0;
            FloatIntUnion u;
            u.tmp = 0;
            float xhalf = 0.5f * x;
            u.f = x;
            u.tmp = 0x5f375a86 - (u.tmp >> 1);
            u.f = u.f * (1.5f - xhalf * u.f * u.f);
            return u.f * x;
        }
        unsafe static float FastSqrtQuake3(float x)
        {
            float xhalf = 0.5f * x;
            int i = *(int*)&x;
            i = 0x5f3759df - (i >> 1);
            x = *(float*)&i;
            x = x * (1.5f - xhalf * x * x);
            return x;
        }


        [StructLayout(LayoutKind.Explicit)]
        struct FloatIntUnion
        {
            [FieldOffset(0)]
            public float f;

            [FieldOffset(0)]
            public int tmp;
        }


    }
}
