﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

using Engine.Maths;
using Engine.Tools;

namespace Engine.Partitions
{
    /// <summary>
    /// For example purpose
    /// </summary>
    public class BvhBoxNode : BvhNode<BvhBoxNode>, IAABBox
    {
        protected AABBox2 size;

        public Vector3 Max { get { return size.Max; } }
        public Vector3 Min { get { return size.Min; } }
        public Vector3 Center { get { return size.center; } }
        public Vector3 HalfSize { get { return size.halfsize; } }

        /// <summary>
        /// Fake initialization
        /// </summary>
        public BvhBoxNode()
            : base() {}

        /// <summary>
        /// Initialization as root node
        /// </summary>
        public BvhBoxNode(BvhTree<BvhBoxNode> main, AABBox2 size)
            : base(main)
        {
            this.size = size;
        }

        /// <summary>
        /// Initialization as child node
        /// </summary>
        /// <param name="index">from 0 to 3 it define the child id</param>
        public BvhBoxNode(BvhBoxNode parent, int index, AABBox2 size)
            : base()
        {
            SetAsNode(parent, index);
            this.size = size;
        }

        /// <summary>
        /// As child node
        /// </summary>
        /// <param name="index">from 0 to 3 it define the child id</param>
        public BvhBoxNode(BvhBoxNode parent, sbyte index, float cx, float cy, float cz, float hx, float hy, float hz) :
            this(parent, index, new AABBox2(cx, cy,cz, hx, hy,hz)) { }

    }


}
