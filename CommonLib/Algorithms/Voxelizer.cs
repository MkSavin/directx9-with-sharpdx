﻿using System;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using Engine.Maths;
using Engine.Partitions;

namespace Engine.Tools
{
    /// <summary>
    /// Generate a octree using triangles intersection.
    /// Is a brute force algorithm
    /// </summary>
    public class Voxelizer
    {
        public int ComputationsCounter { get; private set; }
        AABBox space;
        List<Vector3> vertices;
        List<Face16> faces;
        int levels;
        public Octree<OctBoxNode> octree;
        int curr;

        Vector3 v0, v1, v2;
        Matrix4 transform_inv = Matrix4.Identity;

        public Voxelizer(AABBox space , Vector3 p0, Vector3 p1, Vector3 p2, int depth)
        {
            this.vertices = new List<Vector3>();
            this.faces = new List<Face16>();
            vertices.Add(p0);
            vertices.Add(p1);
            vertices.Add(p2);
            faces.Add(new Face16(0, 1, 2));
            this.space = space;
            this.levels = depth;
            octree = new Octree<OctBoxNode>(depth);

            
        }

        public Voxelizer(AABBox space , IList<Vector3> vertices, IList<Face16> faces , int depth , Matrix4 transform)
        {
            this.space = space;
            this.vertices = new List<Vector3>(vertices);
            this.faces = new List<Face16>(faces);
            this.levels = depth;

            transform_inv =transform;

            octree = new Octree<OctBoxNode>(depth);
            octree.root = new OctBoxNode(octree, space);


            for (int i = 0; i < faces.Count; i++)
            {
                curr = i;
                Face16 face = faces[i];
                v0 = Vector3.TransformCoordinate(vertices[face.I], transform_inv);
                v1 = Vector3.TransformCoordinate(vertices[face.J], transform_inv);
                v2 = Vector3.TransformCoordinate(vertices[face.K], transform_inv);

                if (!RecursiveAddTriangle(octree.root))
                {
                    //Console.WriteLine("Octree out mesh");
                }
            }

        }

        bool RecursiveAddTriangle(OctBoxNode node)
        {
            Vector3 max = node.Max;
            Vector3 min = node.Min;

            if (PrimitiveIntersections.IntersectAABBTriangle(min, max, v0, v1, v2))
            {
                ComputationsCounter++;

                //Console.WriteLine("node " + node.flags.NodeID + " intersect triangle " + curr);
                if (!node.IsLeaf)
                {
                    float cx = node.Center.x;
                    float cy = node.Center.y;
                    float cz = node.Center.z;
                    float hx = node.HalfSize.x * 0.5f;
                    float hy = node.HalfSize.y * 0.5f;
                    float hz = node.HalfSize.z * 0.5f;

                    if (node.child == null)
                    {
                        node.Split();
                    }
                    else
                    {
                        for (int i = 0; i < 8; i++)
                        {
                            OctBoxNode childnode = node.child[i];

                            if (childnode == null)
                            {
                                switch (i)
                                {
                                    case 0: childnode = new OctBoxNode(node, 0, cx - hx, cy - hy, cz - hz, hx, hy, hz); break;
                                    case 1: childnode = new OctBoxNode(node, 1, cx + hx, cy - hy, cz - hz, hx, hy, hz); break;
                                    case 2: childnode = new OctBoxNode(node, 2, cx - hx, cy + hy, cz - hz, hx, hy, hz); break;
                                    case 3: childnode = new OctBoxNode(node, 3, cx + hx, cy + hy, cz - hz, hx, hy, hz); break;
                                    case 4: childnode = new OctBoxNode(node, 4, cx - hx, cy - hy, cz + hz, hx, hy, hz); break;
                                    case 5: childnode = new OctBoxNode(node, 5, cx + hx, cy - hy, cz + hz, hx, hy, hz); break;
                                    case 6: childnode = new OctBoxNode(node, 6, cx - hx, cy + hy, cz + hz, hx, hy, hz); break;
                                    case 7: childnode = new OctBoxNode(node, 7, cx + hx, cy + hy, cz + hz, hx, hy, hz); break;
                                }
                            }

                            if (RecursiveAddTriangle(childnode))
                            {
                                node.child[i] = childnode;
                            }
                        }
                    }
                }
                return true;
            }
            return false;
        }


    }
}
