﻿using System;
using System.Collections.Generic;
using System.Text;

using Engine.Maths;

namespace Engine.Geometry
{

    public static class GeometryTools
    {
        /// <summary>
        /// TODO : i don't like IList because if V is a struct i can access value by reference
        /// Calculate default normal using faces , vertices is a reference class
        /// </summary>
        /// <typeparam name="V">composed array list, for optimization</typeparam>
        public static void CalculateNormals<V>(IList<V> vertices, IList<Face16> faces) where V : IVertexPosition , IVertexNormal
        {
            int numVertices = vertices.Count;
            int numTriangles = faces.Count;

            float epsilon = 1e-6f;

            for (int t = 0; t < numTriangles; t++)
            {
                Face16 face = faces[t];

                V v0 = vertices[face.I];
                V v1 = vertices[face.J];
                V v2 = vertices[face.K];

                Vector3 e0 = v1.position - v0.position;
                Vector3 e1 = v2.position - v0.position;
                Vector3 e2 = v2.position - v1.position;

                Vector3 n = Vector3.Cross(e0, e1);
                float dot0 = e0.LengthSq;
                float dot1 = e1.LengthSq;
                float dot2 = e2.LengthSq;

                if (dot0 < epsilon) dot0 = 1.0f;
                if (dot1 < epsilon) dot1 = 1.0f;
                if (dot2 < epsilon) dot2 = 1.0f;

                v0.normal += n * (1.0f / (dot0 * dot1)) * 10;
                v1.normal += n * (1.0f / (dot2 * dot0)) * 10;
                v2.normal += n * (1.0f / (dot1 * dot2)) * 10;

                vertices[face.I] = v0;
                vertices[face.J] = v1;
                vertices[face.K] = v2;

            }
            for (int i = 0; i < numVertices; i++)
            {
                V vi = vertices[i];
                vi.normal = vi.normal.Normal;
                vertices[i] = vi;
            }
        }

        /// <summary>
        /// Calculate default normal using faces
        /// </summary>
        public static Vector3[] CalculateNormals(IList<Vector3> vertices, IList<Face16> faces)
        {
            int numVertices = vertices.Count;
            int numTriangles = faces.Count;

            Vector3[] normals = new Vector3[numVertices];

            float epsilon = 1e-6f;

            for (int t = 0; t < numTriangles; t++)
            {
                Face16 face = faces[t];

                Vector3 v0 = vertices[face.I];
                Vector3 v1 = vertices[face.J];
                Vector3 v2 = vertices[face.K];

                Vector3 e0 = v1 - v0;
                Vector3 e1 = v2 - v0;
                Vector3 e2 = v2 - v1;

                Vector3 n = Vector3.Cross(e0, e1);
                float dot0 = e0.LengthSq;
                float dot1 = e1.LengthSq;
                float dot2 = e2.LengthSq;

                if (dot0 < epsilon) dot0 = 1.0f;
                if (dot1 < epsilon) dot1 = 1.0f;
                if (dot2 < epsilon) dot2 = 1.0f;

                normals[face.I] += n * (1.0f / (dot0 * dot1));
                normals[face.J] += n * (1.0f / (dot2 * dot0));
                normals[face.K] += n * (1.0f / (dot1 * dot2));
            }
            for (int i = 0; i < numVertices; i++)
                normals[i].Normalize();

            return normals;
        }

        /// <summary>
        /// Lengyel, Eric. “Computing Tangent Space Basis Vectors for an Arbitrary Mesh”. 
        /// Terathon Software 3D Graphics Library, 2001. http://www.terathon.com/code/tangent.html
        /// </summary>
        /// <remarks>
        /// Remember to optain Bitangent vector as B = (NxT) * T.w
        /// </remarks>
        public static Vector4[] CalculateTangents(IList<Vector3> vertices, IList<Vector2> texcood ,IList<Vector3> normals, IList<Face16> faces)
        {
            int numVertices = vertices.Count;
            int numTriangles = faces.Count;

            if (texcood.Count < numVertices) throw new ArgumentOutOfRangeException("texcoord array smaller than vertices ?");

            Vector3[] tan1 = new Vector3[numVertices];
            Vector3[] tan2 = new Vector3[numVertices];
            Vector4[] tangents = new Vector4[numVertices];

            for (int t = 0; t < numTriangles; t++)
            {
                Face16 face = faces[t];

                Vector3 v1 = vertices[face.I];
                Vector3 v2 = vertices[face.J];
                Vector3 v3 = vertices[face.K];

                Vector2 w1 = texcood[face.I];
                Vector2 w2 = texcood[face.J];
                Vector2 w3 = texcood[face.K];

                //Vector3 e1 = v2 - v1;
                //Vector3 e2 = v3 - v1;

        
                float x1 = v2.x - v1.x;
                float x2 = v3.x - v1.x;
                float y1 = v2.y - v1.y;
                float y2 = v3.y - v1.y;
                float z1 = v2.z - v1.z;
                float z2 = v3.z - v1.z;
        
                float s1 = w2.x - w1.x;
                float s2 = w3.x - w1.x;
                float t1 = w2.y - w1.y;
                float t2 = w3.y - w1.y;
        
                float r = 1.0F / (s1 * t2 - s2 * t1);
                Vector3 sdir = new Vector3((t2 * x1 - t1 * x2) * r, (t2 * y1 - t1 * y2) * r, (t2 * z1 - t1 * z2) * r);
                Vector3 tdir = new Vector3((s1 * x2 - s2 * x1) * r, (s1 * y2 - s2 * y1) * r, (s1 * z2 - s2 * z1) * r);
        
                tan1[face.I] += sdir;
                tan1[face.J] += sdir;
                tan1[face.K] += sdir;
        
                tan2[face.I] += tdir;
                tan2[face.J] += tdir;
                tan2[face.K] += tdir;
            }


            for (int i = 0; i < numVertices; i++)
            {
                Vector3 n = normals[i];
                Vector3 t = tan1[i];
        
                // Gram-Schmidt orthogonalize
                tangents[i] = (t - n * Vector3.Dot(n, t)).Normal;
                // Calculate handedness
                tangents[i].w = (Vector3.Dot(Vector3.Cross(n, t), tan2[i]) < 0.0f) ? -1.0f : 1.0f;
            }

            return tangents;
        }



        /// <summary>
        /// Calculate default texture using and an alligned plane
        /// </summary>
        /// <param name="minBound">minimum corner</param>
        /// <param name="maxBound">maximum corner</param>
        /// <param name="plane">plane to project the texture coordinates, identity matrix is alligned with plane XY</param>
        public static Vector2[] GetPlanarProjection(IList<Vector3> vertices, Vector2 minBound, Vector2 maxBound, Matrix4 plane)
        {
            int numVertices = vertices.Count;

            Vector2[] texture = new Vector2[numVertices];

            Vector2 min = new Vector2(float.MaxValue, float.MaxValue);
            Vector2 max = new Vector2(float.MinValue, float.MinValue);

            for (int i = 0; i < numVertices; i++)
            {
                Vector3 proj = Vector3.Project(vertices[i], plane);
                texture[i] = new Vector2(proj.x, proj.y);

                if (proj.x > max.x) max.x = proj.x;
                if (proj.y > max.y) max.y = proj.y;
                if (proj.x < min.x) min.x = proj.x;
                if (proj.y < min.y) min.y = proj.y;
            }
            // interpolate the bound condition y = (x-x0)(y1-y0)/(x1-x0) + y0
            // where x are the local coordinate , y the final bounding rectangle
            // y = (x-x0) * m + y0
            float mU = (maxBound.x - minBound.x) / (max.x - min.x);
            float mV = (maxBound.y - minBound.y) / (max.y - min.y);

            for (int i = 0; i < numVertices; i++)
            {
                texture[i].x = (texture[i].x - min.x) * mU + minBound.x;
                texture[i].y = (texture[i].y - min.y) * mV + minBound.y;
            }
            return texture;
        }

        /// <summary>
        /// Calculate default texture using 
        /// </summary>
        /// <param name="cilindral">cilindral transformation</param>
        /// <returns></returns>
        public static Vector2[] GetCilindralProjection(IList<Vector3> vertices, Matrix4 cilindral)
        {
            int numvertices = vertices.Count;
            Vector2[] textures = new Vector2[numvertices];

            float minh = float.MaxValue;
            float maxh = float.MinValue;

            for (int i = 0; i < numvertices; i++)
            {
                Vector3 v = Vector3.TransformCoordinate(vertices[i], cilindral);
                float r = (float)Math.Sqrt(v.x * v.x + v.z * v.z);
                float t = (float)Math.Asin(v.z / r);
                float h = v.y;

                if (v.x < 0) t = (float)Math.PI - t;
                if (v.y < minh) minh = v.y;
                if (v.y > maxh) maxh = v.y;

                textures[i] = new Vector2((float)(t / Math.PI / 2.0), h);
            }

            for (int i = 0; i < numvertices; i++)
            {
                textures[i].y = (textures[i].y - minh) / (maxh - minh);
            }

            return textures;
        }


        public static Vector4 GetTangentVector(Vector3 p0, Vector3 p1, Vector3 p2, Vector2 uv0, Vector2 uv1, Vector2 uv2, Vector3 n)
        {
            // Given the 3 vertices (position and texture coordinates) of a triangle
            // calculate and return the triangle's tangent vector. The handedness of
            // the local coordinate system is stored in tangent.w. The bitangent is
            // then: float3 bitangent = cross(normal, tangent.xyz) * tangent.w.
            Vector3 edge0 = Vector3.GetNormal(p1 - p0);
            Vector3 edge1 = Vector3.GetNormal(p2 - p0);

            // Create 2 vectors in tangent (texture) space that point in the same
            // direction as edge1 and edge2 (in object space).
            Vector2 tedge0 = Vector2.GetNormal(uv1 - uv0);
            Vector2 tedge1 = Vector2.GetNormal(uv2 - uv0);

            // These 2 sets of vectors form the following system of equations:
            //
            //  edge1 = (texEdge1.x * tangent) + (texEdge1.y * bitangent)
            //  edge2 = (texEdge2.x * tangent) + (texEdge2.y * bitangent)
            //
            // Using matrix notation this system looks like:
            //
            //  [ edge1 ]     [ texEdge1.x  texEdge1.y ]  [ tangent   ]
            //  [       ]  =  [                        ]  [           ]
            //  [ edge2 ]     [ texEdge2.x  texEdge2.y ]  [ bitangent ]
            //
            // The solution is:
            //
            //  [ tangent   ]        1     [ texEdge2.y  -texEdge1.y ]  [ edge1 ]
            //  [           ]  =  -------  [                         ]  [       ]
            //  [ bitangent ]      det A   [-texEdge2.x   texEdge1.x ]  [ edge2 ]
            //
            //  where:
            //        [ texEdge1.x  texEdge1.y ]
            //    A = [                        ]
            //        [ texEdge2.x  texEdge2.y ]
            //
            //    det A = (texEdge1.x * texEdge2.y) - (texEdge1.y * texEdge2.x)
            //
            // From this solution the tangent space basis vectors are:
            //
            //    tangent = (1 / det A) * ( texEdge2.y * edge1 - texEdge1.y * edge2)
            //  bitangent = (1 / det A) * (-texEdge2.x * edge1 + texEdge1.x * edge2)
            //     normal = cross(tangent, bitangent)

            Vector3 bitangent = new Vector3();
            Vector3 tangent = new Vector3();

            float det = (tedge0.x * tedge1.y) - (tedge0.y * tedge1.x);

            if (MathUtils.ABS(det) < 1e-6f)    // almost equal to zero
            {
                tangent.x = 1.0f;
                tangent.y = 0.0f;
                tangent.z = 0.0f;
                bitangent.x = 0.0f;
                bitangent.y = 1.0f;
                bitangent.z = 0.0f;
            }
            else
            {
                det = 1.0f / det;

                tangent.x = (tedge1.y * edge0.x - tedge0.y * edge1.x) * det;
                tangent.y = (tedge1.y * edge0.y - tedge0.y * edge1.y) * det;
                tangent.z = (tedge1.y * edge0.z - tedge0.y * edge1.z) * det;

                bitangent.x = (-tedge1.x * edge0.x + tedge0.x * edge1.x) * det;
                bitangent.y = (-tedge1.x * edge0.y + tedge0.x * edge1.y) * det;
                bitangent.z = (-tedge1.x * edge0.z + tedge0.x * edge1.z) * det;

                tangent.Normalize();
                bitangent.Normalize();
            }
            // Calculate the handedness of the local tangent space.
            // The bitangent vector is the cross product between the triangle face
            // normal vector and the calculated tangent vector. The resulting bitangent
            // vector should be the same as the bitangent vector calculated from the
            // set of linear equations above. If they point in different directions
            // then we need to invert the cross product calculated bitangent vector. We
            // store this scalar multiplier in the tangent vector's 'w' component so
            // that the correct bitangent vector can be generated in the normal mapping
            // shader's vertex shader.
            Vector3 b = Vector3.Cross(n, tangent);
            float w = (Vector3.Dot(b, bitangent) < 0.0f) ? -1.0f : 1.0f;
            Vector4 t = new Vector4(tangent.x, tangent.y, tangent.z, w);

            return t;
        }

    }

}
