﻿// by johnwhile
using System;
using System.Diagnostics;
using System.Drawing;
using System.Collections.Generic;
using System.Text;

using Engine.Maths;

namespace Engine.Geometry
{
    /// <summary>
    /// Abstract class, used to derive all similar Primitives Lines geometries
    /// </summary>
    public abstract class BaseLineGeometry : BaseGeometry3D
    {
        public VertexAttribute<Vector3> vertices;
        public VertexAttribute<Color32> colors;

        public BaseLineGeometry()
            : base()
        {
            vertices = null;
            colors = null;
        }
        /// <summary>
        /// Copy from source instance
        /// </summary>
        public BaseLineGeometry(BaseLineGeometry src)
            : base(src)
        {
            vertices = src.vertices;
            colors = src.colors;
        }
        /// <summary>
        /// Copy from source instance
        /// </summary>
        public BaseLineGeometry(BaseGeometry3D src)
            : base(src) { }

            
        public override int numVertices 
        {
            get { return (vertices != null) ? vertices.Count : 0; } 
        }
        /// <summary>
        /// Get the vertex indices of this primitive
        /// </summary>
        public abstract void GetSegment(int primitive, out int I, out int J);

        /// <summary>
        /// change transform but vertices will be in the same position
        /// </summary>
        /// <param name="newtransform"></param>
        public override void changeTransform(Matrix4 newtransform)
        {
            Matrix4 matrix = base.transform * Matrix4.Inverse(newtransform);

            if (vertices != null)
                for (int i = 0; i < numVertices; i++)
                    vertices[i] = matrix * vertices[i];

            transform = newtransform;
        }

        /// <summary>
        /// append vertices attribute from other geometries, this geometry have the priority about 
        /// existing attribute, if source geometry don't have attribute but this yes, populate missing data with empty values
        /// </summary>
        protected int appendVertexAttribute(BaseLineGeometry geometry)
        {
            int nverts1 = this.numVertices;
            int nverts2 = geometry.numVertices;
            Matrix4 transformation = geometry.globalcoord * globalcoord_inv;

            // vertices must not be null
            if (this.vertices != null)
            {
                VertexAttribute<Vector3> tmp = new VertexAttribute<Vector3>(DeclarationUsage.Position, nverts1 + nverts2);
                for (int i = 0; i < nverts1; i++) tmp[i] = this.vertices[i];

                for (int i = 0; i < nverts2; i++)
                    tmp[i + nverts1] = Vector3.TransformCoordinate(geometry.vertices[i], transformation);

                this.vertices = tmp;
            }
            if (this.colors != null)
            {
                VertexAttribute<Color32> tmp = new VertexAttribute<Color32>(DeclarationUsage.Color, nverts1 + nverts2);
                for (int i = 0; i < nverts1; i++) tmp[i] = this.colors[i];
                if (geometry.colors != null) for (int i = 0; i < nverts2; i++) tmp[i + nverts1] = geometry.colors[i];
                this.colors = tmp;
            }

            return nverts1 + nverts2;
        }

        /// <summary>
        /// Write all vertex channels to unmanaged buffer (like the graphic buffer) using DeclarationUsage flags to match them
        /// </summary>
        /// <param name="buffer">the destination buffer</param>
        /// <param name="bufferSize">size in bytes of destination buffer</param>
        /// <param name="bufferOffset">offset in bytes of destination buffer where write beginning</param>
        /// <param name="bufferInfo">descriptor of buffer element to understand how write a fragmaented structure</param>
        /// <returns>return false if found some error</returns>
        public bool WriteAttibutesToBuffers(IntPtr buffer, VertexLayout bufferInfo, int bufferSize, int bufferOffset)
        {
            if (vertices != null) vertices.WriteToBuffers(buffer, bufferInfo, bufferSize, bufferOffset);
            if (colors != null) colors.WriteToBuffers(buffer, bufferInfo, bufferSize, bufferOffset);
            return true;
        }
        /// <summary>
        /// Write all indices to unmanaged buffer (like the graphic buffer)
        /// </summary>
        public abstract bool WriteIndicesToBuffers(IndexStream indexstream);


        public static LineListGeometry Axis3D()
        {
            LineListGeometry axis = new LineListGeometry();

            axis.name = "axis3d";

            Vector3[] vertices = new Vector3[]
            {
                new Vector3(0,0,0),
                new Vector3(1,0,0),
                new Vector3(0,0,0),
                new Vector3(0,1,0),
                new Vector3(0,0,0),
                new Vector3(0,0,1)
            };

            Color32[] colors = new Color32[]
            {
                Color.Red,
                Color.Red,
                Color.Green,
                Color.Green,
                Color.Blue , 
                Color.Blue        
            };
            axis.vertices = new VertexAttribute<Vector3>(DeclarationUsage.Position, vertices);
            axis.colors = new VertexAttribute<Color32>(DeclarationUsage.Color, colors);

            return axis;
        }
        public static LineListGeometry Grid2D()
        {
            LineListGeometry grid = new LineListGeometry();
            grid.name = "grid2d";

            grid.vertices = new VertexAttribute<Vector3>(DeclarationUsage.Position, 40);

            int i = 0;
            for (int x = -5; x < 6; x++)
            {
                if (x == 0) continue;
                grid.vertices[i++] = new Vector3(x, 0, -5);
                grid.vertices[i++] = new Vector3(x, 0, 5);
            }
            for (int z = -5; z < 6; z++)
            {
                if (z == 0) continue;
                grid.vertices[i++] = new Vector3(-5, 0, z);
                grid.vertices[i++] = new Vector3(5, 0, z);
            }

            grid.colors = new VertexAttribute<Color32>(DeclarationUsage.Color, 40);

            for (i = 0; i < 40; i++) grid.colors[i] = Color.Gray;

            return grid;
        }
        public static LineListGeometry Circle(int numpoints)
        {
            float dang = (float)Math.PI * 2.0f / numpoints;
            int numverts = numpoints + 1;

            LineListGeometry obj = new LineListGeometry();

            obj.vertices = new VertexAttribute<Vector3>(DeclarationUsage.Position, numverts);
            obj.colors = new VertexAttribute<Color32>(DeclarationUsage.Color, numverts);

            for (int i = 0; i < numverts; i++)
            {
                obj.vertices[i] = new Vector3((float)Math.Sin(dang * i), (float)Math.Cos(dang * i), 0);
                obj.colors[i] = Color.Yellow;
            }

            return obj;
        }
        /// <summary>
        /// A sphere made with three yellow circle, minimum 3 points
        /// </summary>
        public static SplineListGeometry SphereGizmo(float radius, int numpoints)
        {
            Debug.Assert(numpoints > 2, "minimum 3");

            float dang = (float)Math.PI * 2.0f / numpoints;

            SplineListGeometry shape = new SplineListGeometry();

            shape.vertices = new VertexAttribute<Vector3>(DeclarationUsage.Position, numpoints * 3);
            shape.colors = new VertexAttribute<Color32>(DeclarationUsage.Position, numpoints * 3);

            shape.indices = new IndexAttribute<Edge16>(numpoints * 3);

            /////////////////////circle A
            for (int i = 0; i < numpoints; i++)
                shape.vertices[i] = new Vector3((float)Math.Sin(dang * i), (float)Math.Cos(dang * i), 0);

            for (int i = 0; i < numpoints; i++)
                shape.indices[i] = new Edge16(i, i + 1);

            shape.indices.data[numpoints - 1].J = 0;


            /////////////////////circle B
            for (int i = numpoints; i < numpoints * 2; i++)
                shape.vertices[i] = new Vector3(0, (float)Math.Cos(dang * i), (float)Math.Sin(dang * i));

            for (int i = numpoints; i < numpoints * 2; i++)
                shape.indices[i] = new Edge16(i, i + 1);

            shape.indices.data[numpoints * 2 - 1].J = (ushort)numpoints;

            /////////////////////circle C
            for (int i = numpoints * 2; i < numpoints * 3; i++)
                shape.vertices[i] = new Vector3((float)Math.Cos(dang * i), 0, (float)Math.Sin(dang * i));

            for (int i = numpoints * 2; i < numpoints * 3; i++)
                shape.indices[i] = new Edge16(i, i + 1);

            shape.indices.data[numpoints * 3 - 1].J = (ushort)(numpoints * 2);


            for (int i = 0; i < shape.vertices.Count; i++)
            {
                shape.vertices[i] *= radius;
                shape.colors[i] = Color.Yellow;
            }
            return shape;
        }

        /// <summary>
        /// A box with semi-size
        /// </summary>
        public static SplineListGeometry BoxGizmo(float dx, float dy, float dz)
        {

            //           5______4
            //           /     /|         Y
            //         1/_____/0|         |
            //          | 7   | /6        *---> Z
            //          |_____|/         /
            //         3      2         X
            SplineListGeometry shape = new SplineListGeometry();

            shape.vertices = new VertexAttribute<Vector3>(DeclarationUsage.Position, new Vector3[]{
                new Vector3( dx, dy, dz),
                new Vector3( dx, dy,-dz),
                new Vector3( dx,-dy, dz),
                new Vector3( dx,-dy,-dz),
                new Vector3(-dx, dy, dz),
                new Vector3(-dx, dy,-dz),
                new Vector3(-dx,-dy, dz),
                new Vector3(-dx,-dy,-dz)});

            shape.colors = new VertexAttribute<Color32>(DeclarationUsage.Color, 8);
            for (int i = 0; i < 8; i++)
                shape.colors[i] = Color.Yellow;

            shape.indices = new IndexAttribute<Edge16>(new Edge16[]{
                new Edge16(0,1),
                new Edge16(1,3),
                new Edge16(3,2),
                new Edge16(2,0),
                new Edge16(4,5),
                new Edge16(5,7),
                new Edge16(7,6),
                new Edge16(6,4),
                new Edge16(0,4),
                new Edge16(2,6),
                new Edge16(3,7),
                new Edge16(1,5)});

            return shape;
        }

        /// <summary>
        /// A box with only corner, min and max are in global coordinate system
        /// </summary>
        public static SplineListGeometry SelectBoxGizmo(Vector3 min, Vector3 max)
        {
            //            3           7
            //          0/___1   5__4/     
            //          |           |
            //          |           |
            //         2            6

            SplineListGeometry shape = new SplineListGeometry();

            Vector3 d = max - min;

            float x = Math.Abs(d.x / 3.0f);
            float y = Math.Abs(d.y / 3.0f);
            float z = Math.Abs(d.z / 3.0f);

            shape.vertices = new VertexAttribute<Vector3>(DeclarationUsage.Position, 32);

            // corner +X+Y+Z
            shape.vertices[0] = new Vector3(max.x, max.y, max.z);
            shape.vertices[1] = new Vector3(max.x - x, max.y, max.z);
            shape.vertices[2] = new Vector3(max.x, max.y - y, max.z);
            shape.vertices[3] = new Vector3(max.x, max.y, max.z - z);

            // corner +X+Y-Z
            shape.vertices[4] = new Vector3(max.x, max.y, min.z);
            shape.vertices[5] = new Vector3(max.x - x, max.y, min.z);
            shape.vertices[6] = new Vector3(max.x, max.y - y, min.z);
            shape.vertices[7] = new Vector3(max.x, max.y, min.z + z);

            // corner +X-Y+Z
            shape.vertices[8] = new Vector3(max.x, min.y, max.z);
            shape.vertices[9] = new Vector3(max.x - x, min.y, max.z);
            shape.vertices[10] = new Vector3(max.x, min.y + y, max.z);
            shape.vertices[11] = new Vector3(max.x, min.y, max.z - z);

            // corner +X-Y-Z
            shape.vertices[12] = new Vector3(max.x, min.y, min.z);
            shape.vertices[13] = new Vector3(max.x - x, min.y, min.z);
            shape.vertices[14] = new Vector3(max.x, min.y + y, min.z);
            shape.vertices[15] = new Vector3(max.x, min.y, min.z + z);


            // corner -X+Y+Z
            shape.vertices[16] = new Vector3(min.x, max.y, max.z);
            shape.vertices[17] = new Vector3(min.x + x, max.y, max.z);
            shape.vertices[18] = new Vector3(min.x, max.y - y, max.z);
            shape.vertices[19] = new Vector3(min.x, max.y, max.z - z);

            // corner -X+Y-Z
            shape.vertices[20] = new Vector3(min.x, max.y, min.z);
            shape.vertices[21] = new Vector3(min.x + x, max.y, min.z);
            shape.vertices[22] = new Vector3(min.x, max.y - y, min.z);
            shape.vertices[23] = new Vector3(min.x, max.y, min.z + z);

            // corner -X-Y+Z
            shape.vertices[24] = new Vector3(min.x, min.y, max.z);
            shape.vertices[25] = new Vector3(min.x + x, min.y, max.z);
            shape.vertices[26] = new Vector3(min.x, min.y + y, max.z);
            shape.vertices[27] = new Vector3(min.x, min.y, max.z - z);

            // corner -X-Y-Z
            shape.vertices[28] = new Vector3(min.x, min.y, min.z);
            shape.vertices[29] = new Vector3(min.x + x, min.y, min.z);
            shape.vertices[30] = new Vector3(min.x, min.y + y, min.z);
            shape.vertices[31] = new Vector3(min.x, min.y, min.z + z);

            shape.indices = new IndexAttribute<Edge16>(24);

            shape.indices[0] = new Edge16(0, 1);
            shape.indices[1] = new Edge16(0, 2);
            shape.indices[2] = new Edge16(0, 3);

            int j = 3;
            int k = 4;
            for (int i = 1; i < 8; i++)
            {
                shape.indices[j++] = shape.indices[0] + k;
                shape.indices[j++] = shape.indices[1] + k;
                shape.indices[j++] = shape.indices[2] + k;
                k += 4;
            }

            shape.colors = new VertexAttribute<Color32>(DeclarationUsage.Color, 32);

            for (int i = 0; i < 32; i++)
                shape.colors[i] = Color.White;

            return shape;
        }

    }

    public class LineStripGeometry : BaseLineGeometry
    {
        //  LINE-STRIP
        //  0   2   4
        //   \ / \ /
        //    1   3
        public override PrimitiveType primitive
        {
            get { return PrimitiveType.LineStrip; }
        }
        public override bool IsIndexed
        {
            get { return false; }
        }
        public override void GetSegment(int primitive,out int I,out int J)
        {
            I = primitive;
            J = I + 1;
        }
        public override int numPrimitives
        {
            get { return numVertices - 1; }
        }
        public override int numIndices
        {
            get { return 0; }
        }
        public LineStripGeometry()
        {
        }
        public LineStripGeometry(BaseLineGeometry src)
            : base(src)
        {
        }
        /// <summary>
        /// Write all indices to unmanaged buffer (like the graphic buffer)
        /// </summary>
        public override bool WriteIndicesToBuffers(IndexStream indexstream)
        {
            return false;
        }

    }
    
    public class SplineStripGeometry : BaseLineGeometry
    {
        public IndexAttribute<ushort> indices;

        public SplineStripGeometry()
        {
        }
        public SplineStripGeometry(BaseLineGeometry src)
            : base(src)
        {
        }
        public override PrimitiveType primitive
        {
            get { return PrimitiveType.LineStrip; }
        }
        public override bool IsIndexed
        {
            get { return true; }
        }
        public override void GetSegment(int primitive, out int I, out int J)
        {
            I = indices[primitive];
            J = indices[primitive + 1];
        }
        public override int numPrimitives
        {
            get { return (indices != null || indices.Count < 2) ? indices.Count - 1 : 0; }
        }
        public override int numIndices
        {
            get { return (indices != null) ? indices.Count : 0; }
        }
        /// <summary>
        /// Write all indices to unmanaged buffer (like the graphic buffer)
        /// </summary>
        public override bool WriteIndicesToBuffers(IndexStream indexstream)
        {
            if (indices != null) indexstream.WriteCollection<ushort>(indices, 0, 0);
            return true;
        }
    }
    
    public class LineListGeometry : BaseLineGeometry
    {
        //    LINE-LIST
        //     0   2
        //      \   \
        //       1   3
        public override PrimitiveType primitive
        {
            get { return PrimitiveType.LineList; }
        }
        public override bool IsIndexed
        {
            get { return false; }
        }
        public override int numPrimitives
        {
            get { return numVertices / 2; }
        }
        public override void GetSegment(int primitive, out int I, out int J)
        {
            I = primitive * 2;
            J = I + 1;
        }
        public override int numIndices
        {
            get { return 0; }
        }
        public LineListGeometry()
        {
        }
        public LineListGeometry(BaseLineGeometry src)
            : base(src)
        {
        }
        /// <summary>
        /// Write all indices to unmanaged buffer (like the graphic buffer)
        /// </summary>
        public override bool WriteIndicesToBuffers(IndexStream indexstream)
        {
            return false;
        }

        void convertcommonformat(BaseLineGeometry src)
        {
            List<Edge16> segments = new List<Edge16>(src.numPrimitives);
            int I, J;

            for (int i = 0; i < src.numPrimitives; i++)
            {
                src.GetSegment(i, out I, out J);
                Edge16 edge = new Edge16(I, J);
                if (!edge.Degenerated) segments.Add(edge);
            }

            int nverts = segments.Count * 2;

            this.vertices = new VertexAttribute<Vector3>(DeclarationUsage.Position, nverts);

            int iv = 0;
            for (int i = 0; i < segments.Count; i++)
            {
                Edge16 edge = segments[i];
                this.vertices[iv++] = src.vertices[edge.I];
                this.vertices[iv++] = src.vertices[edge.J];
            }
            if (src.colors != null)
            {
                iv = 0;
                this.colors = new VertexAttribute<Color32>(DeclarationUsage.Color, nverts);
                for (int i = 0; i < segments.Count; i++)
                {
                    Edge16 edge = segments[i];
                    this.colors[iv++] = src.colors[edge.I];
                    this.colors[iv++] = src.colors[edge.J];
                }
            }

        }
        
        public static implicit operator LineListGeometry(SplineListGeometry generic)
        {
            LineListGeometry linelist = new LineListGeometry();
            linelist.convertcommonformat(generic);
            return linelist;
        }
    }
    
    public class SplineListGeometry : BaseLineGeometry
    {
        public IndexAttribute<Edge16> indices;

        public SplineListGeometry()
        {
        }
        public SplineListGeometry(BaseLineGeometry src)
            : base(src)
        {
        }
        public override PrimitiveType primitive
        {
            get { return PrimitiveType.LineList; }
        }
        public override bool IsIndexed
        {
            get { return true; }
        }
        public override void GetSegment(int primitive, out int I, out int J)
        {
            I = indices[primitive].I;
            J = indices[primitive].J;
        }
        public override int numPrimitives
        {
            get { return (indices != null) ? indices.Count : 0; }
        }
        public override int numIndices
        {
            get { return (indices != null) ? indices.Count * 2 : 0; }
        }
        /// <summary>
        /// Write all indices to unmanaged buffer (like the graphic buffer)
        /// </summary>
        public override bool WriteIndicesToBuffers(IndexStream indexstream)
        {
            if (indices != null) indexstream.WriteCollection<Edge16>(indices, 0, 0);
            return true;
        }

        void convertcommonformat(BaseLineGeometry src)
        {
            // build indices , the getTriangle methods return indices used for TriangleList
            if (src.numPrimitives > 0)
            {
                List<Edge16> segments = new List<Edge16>(src.numPrimitives);
                int I, J;

                for (int i = 0; i < src.numPrimitives; i++)
                {
                    src.GetSegment(i, out I, out J);
                    Edge16 f = new Edge16(I, J);
                    if (!f.Degenerated) segments.Add(f);
                }
                this.indices = new IndexAttribute<Edge16>(segments);
            }
        }
    }
}
