﻿using System;
using System.Collections.Generic;
using System.Text;

using Engine.Maths;

namespace Engine.Geometry.HalfEdge
{
    public class Mesh3D
    {
        public List<HVertex> VertsList = new List<HVertex>();
        public List<HFace> FaceList = new List<HFace>();

        /// <summary>
        /// Usefull list to search two opposite halfedge, the hashcode is unique for same vertices
        /// </summary>
        Dictionary<int, HEdge> edgemap = new Dictionary<int, HEdge>();
        /// <summary>
        /// Counter of Edges, not same of HalfEdges
        /// </summary>
        int EdgeCount = 0;

        public Mesh3D(IEnumerable<Vector3> vertices, IEnumerable<Face16> triangles)
        {

            int idx = 0;

            foreach (Vector3 p in vertices)
            {
                VertsList.Add(new HVertex(p, idx++));
            }
            idx = 0;

            foreach (Face16 f in triangles)
            {
                HFace face = new HFace();

                HEdge he0 = face.he;
                HEdge he1 = he0.next;
                HEdge he2 = he1.next;

                he0.head = VertsList[f.I];
                he1.head = VertsList[f.J];
                he2.head = VertsList[f.K];

                EdgeCount += 3;

                linkOpposite(he0);
                linkOpposite(he1);
                linkOpposite(he2);

                FaceList.Add(face);
            }

        }

        /// <summary>
        /// Estract or Insert the base geometry format (for rendering) 
        /// </summary>
        public MeshListGeometry TriMesh
        {
            get
            {
                if (VertsList.Count > ushort.MaxValue - 1)
                    throw new OverflowException("too many vertices for a 16 bit indices");

                ClearVertexList();

                MeshListGeometry mesh = new MeshListGeometry();
                mesh.vertices = new VertexAttribute<Vector3>(DeclarationUsage.Position, GetVertexList());
                mesh.colors = new VertexAttribute<Color32>(DeclarationUsage.Color, mesh.vertices.Count, Color32.Blue);
                mesh.indices = new IndexAttribute<Face16>(GetFaceList());
                return mesh;
            }
        }

        /// <summary>
        /// Search the opposite halfedge, if not found add this halfedge to edgemap
        /// </summary>
        void linkOpposite(HEdge he)
        {
            HEdge opp;
            edgemap.TryGetValue(he.GetEdgeHashCode(), out opp);

            if (opp != null)
            {
                he.opposite = opp;
                if (opp.opposite != null) throw new Exception("incoerent halfedge");
                opp.opposite = he;
                he.isEdge = true;
                opp.isEdge = false;

                // two halfedges are linked so they do one edge
                EdgeCount--;
            }
            else
            {
                he.isEdge = true;
                edgemap.Add(he.GetEdgeHashCode(), he);
            }

        }

        /// <summary>
        /// Scale all vertex idx value
        /// </summary>
        public void ClearVertexList()
        {
            for (int i = 0; i < VertsList.Count; i++)
                VertsList[i].idx = i;
        }

        public Face16[] GetFaceList()
        {
            Face16[] faces = new Face16[FaceList.Count];
            for (int i = 0; i < FaceList.Count; i++)
            {
                faces[i] = new Face16(
                    FaceList[i].he.head.idx,
                    FaceList[i].he.next.head.idx,
                    FaceList[i].he.next.next.head.idx);
            }
            return faces;
        }
        public Edge16[] GetEdgeList()
        {
            Edge16[] edges = new Edge16[EdgeCount];
            int i = 0;

            foreach (HFace face in FaceList)
            {
                HEdge he0 = face.he;
                HEdge he1 = face.he.next;
                HEdge he2 = face.he.next.next;

                if (he0.isEdge) edges[i++] = new Edge16(he0.head.idx, he1.head.idx);
                if (he1.isEdge) edges[i++] = new Edge16(he1.head.idx, he2.head.idx);
                if (he2.isEdge) edges[i++] = new Edge16(he2.head.idx, he0.head.idx);
            }
            if (i != EdgeCount) throw new Exception("incoerent halfedge");
            return edges;
        }
        public Vector3[] GetVertexList()
        {
            Vector3[] vertices = new Vector3[VertsList.Count];
            for (int i = 0; i < VertsList.Count; i++)
                vertices[i] = VertsList[i].v;
            return vertices;
        }

        public bool AddFace(HEdge e0, HEdge e1)
        {
            //       ...e2...
            //      /\      /\
            //     / e0 f  e1 \
            //    /    \  /    \
            //   /______\/______\

            if (e0.opposite == null && e1.opposite == null)
            {
                HFace face = new HFace();

                HEdge he0 = face.he;
                HEdge he1 = he0.next;
                HEdge he2 = he1.next;

                he0.head = e0.next.next.head;
                he1.head = e0.head;
                he2.head = e1.head;

                e0.opposite = he0;
                e1.opposite = he1;

                EdgeCount++;

                linkOpposite(he2);

                return true;
            }
            else
            {
                return false;
            }
        }

        public bool AddFace(HEdge e, HVertex vnew)
        {
            //      ....e1... vnew
            //     / \   f  ;
            //    /  e0    ;
            //   /     \  e2
            //  /_______\;

            if (e.opposite == null)
            {
                HFace face = new HFace();

                HEdge he0 = face.he;
                HEdge he1 = he0.next;
                HEdge he2 = he1.next;

                he0.head = e.next.next.head;
                he1.head = e.head;
                he2.head = vnew;

                EdgeCount += 2;

                e.opposite = he0;

                linkOpposite(he1);
                linkOpposite(he2);

                FaceList.Add(face);

                vnew.he = he1;
                vnew.idx = VertsList.Count;

                VertsList.Add(vnew);

                return true;

            }
            else
            {
                return false;
            }
        }
    }

}
