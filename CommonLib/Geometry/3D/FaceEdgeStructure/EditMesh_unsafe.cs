﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Runtime.InteropServices;
using System.Drawing;

using Engine.Maths;

// Pointers not work...
namespace Engine.Geometry.Unsafe
{
    /// <summary>
    /// Vertex of HalfEdge format
    /// </summary>
    public struct HVertex
    {
        public int ID;
        public Vector3 normal;
        public Vector3 position;
        public Vector2 texture;
        public Int32 color;
        public bool marked;

        public HVertex(int id)
        {
            this.ID = id;
            this.normal = Vector3.Zero;
            this.position = Vector3.Zero;
            this.texture = Vector2.Zero;
            this.color = Color.Blue.ToArgb();
            this.marked = false;
        }

        public override string ToString()
        {
            return ID.ToString();
        }
    }
    /// <summary>
    /// Edge, the halfedge structure require 2 pointers to vertices and maximum 2 faces
    /// </summary>
    public class HEdge
    {
        public int ID;
        public HFace[] f;
        public IntPtr[] v;

        public bool marked;

        public HEdge(int id)
        {
            f = new HFace[2];
            v = new IntPtr[2];
            marked = false;
            ID = id;
        }
        public HEdge(IntPtr v0, IntPtr v1) : this(-1)
        {
            v[0] = v0;
            v[1] = v1;
        }

        public override string ToString()
        {
            try
            {
                string a = v[0] != IntPtr.Zero ? ((HVertex)Marshal.PtrToStructure(v[0], typeof(HVertex))).ToString() : "-";
                string b = v[1] != IntPtr.Zero ? ((HVertex)Marshal.PtrToStructure(v[1], typeof(HVertex))).ToString() : "-";
                return string.Format("e{0}({1},{2}) f{3} f{4}", ID, a, b, (f[0] == null) ? "-" : f[0].ToString(), (f[1] == null) ? "-" : f[1].ToString());
            }
            catch
            {
                return "ERROR";
            }
        }
    }
    /// <summary>
    /// Face, in theory there aren't limit about vertex indices, depend that struct you use
    /// when convert in default format, 16 or 32 bit
    /// </summary>
    public class HFace
    {
        public int ID;
        public bool marked;
        public HEdge[] e;
        public IntPtr[] v;

        public HFace(int id)
        {
            ID = id;
            marked = false;
            e = new HEdge[3];
            v = new IntPtr[3];
        }
        public HFace(IntPtr v0, IntPtr v1, IntPtr v2, int id)  :this(id)
        {
            v[0] = v0;
            v[1] = v1;
            v[2] = v2;
        }
        public bool isVertexAssigned
        {
            get { foreach (IntPtr p in v) if (p == IntPtr.Zero) return false; return true; }
        }
        public bool isEdgeAssigned
        {
            get { foreach (HEdge ee in e) if (ee == null) return false; return true; }
        }

        /// I use this scheme:
        /// e[0] = e(v0,v1)
        /// e[1] = e(v1,v2)
        /// e[2] = e(v0,v2)
        public int GetEdgeIndex(IntPtr pvx, IntPtr pvy)
        {
            // using flag to un-mark the vertices that mach with eij
            // 110 ; 6 return edge[0] because find first and second index
            // 011 ; 3 return edge[1] because find second and third index
            // 101 ; 5 return edge[2] because find first and third index
            // 111 ; 7 face contain two vertice with same index, error
            // 000 001 010 100; 0 1 2 4 face don't contain this edge(id1,id2), return -1
            byte pos = 0;
            if (v[0] == pvx || v[0] == pvy) pos |= 4;
            if (v[1] == pvx || v[1] == pvy) pos |= 2;
            if (v[2] == pvx || v[2] == pvy) pos |= 1;

            switch (pos)
            {
                case 6: return 0;
                case 3: return 1;
                case 5: return 2;
                case 7: throw new Exception("degenerate face");
                default: return -1;
            }
        }
        /// <summary>
        /// is possible that edges are not sorted in default chain
        /// </summary>
        /// <param name="e01">the correct position what e[0] must have , if == 0 edge0 are in correct position</param>
        public void GetSortedEdges(out int i0, out int i1, out int i2)
        {
            i0 = GetEdgeIndex((e[0].v[0]), (e[0].v[1]));
            i1 = GetEdgeIndex((e[1].v[0]), (e[1].v[1]));
            i2 = GetEdgeIndex((e[2].v[0]), (e[2].v[1]));
        }

        public override string ToString()
        {
            string a = v[0] != IntPtr.Zero ? ((HVertex)Marshal.PtrToStructure(v[0], typeof(HVertex))).ToString() : "-";
            string b = v[1] != IntPtr.Zero ? ((HVertex)Marshal.PtrToStructure(v[1], typeof(HVertex))).ToString() : "-";
            string c = v[2] != IntPtr.Zero ? ((HVertex)Marshal.PtrToStructure(v[2], typeof(HVertex))).ToString() : "-";

            if (isEdgeAssigned)
                return string.Format("f{0} [{1} {2} {3}] -> {4} {5} {6}", ID, a, b, c, e[0], e[1], e[2]);
            else
                return string.Format("f{0} [{1} {2} {3}] -> - - -", ID, a, b, c);
        }
    }

    /// <summary>
    /// Half-Edge mesh structure, used to work with geometry but bad to store data
    /// </summary>
    public class HMesh
    {
        // vertes edge and faces table
        List<IntPtr> m_verts;
        public List<HEdge> m_edges;
        public List<HFace> m_faces;
        public List<HVertex> vertices
        {
            get
            {
                List<HVertex> vertex = new List<HVertex>(m_verts.Count);
                foreach (IntPtr ptr in m_verts)
                    vertex.Add((HVertex)Marshal.PtrToStructure(ptr, typeof(HVertex)));
                return vertex;
            }
        }

        /// <summary>
        /// Estract or Insert the base geometry format (for rendering) 
        /// </summary>
        public MeshListGeometry TriMesh
        {
            get { return getTriMesh(); }
            set { setTriMesh(value.indices.data, value.vertices.data, value.colors.data); }
        }

        public int NumVerts { get { return m_verts.Count; } }
        public int NumFaces { get { return m_faces.Count; } }


        public HMesh()
        {
            m_verts = new List<IntPtr>();
            m_edges = new List<HEdge>();
            m_faces = new List<HFace>();
        }

        /// <summary>
        /// Initialize a Half-Edge structure using a base static mesh, edge will be calculated
        /// </summary>
        public HMesh(IList<Face16> faces, IList<Vector3> vertices, IList<Color32> colors)
            : this()
        {
            setTriMesh(faces, vertices, colors);
            calculateEdgeTable();
        }

        /// <summary>
        /// Before extract geometry in default structure, need to clear vertices ID
        /// </summary>
        public void ClearID()
        {
            for (int i = 0; i < m_faces.Count; i++)
            {
                HFace face = m_faces[i];
                face.ID = i;
                m_faces[i] = face;
            }
            for (int i = 0; i < m_verts.Count; i++)
            {
               HVertex vert = (HVertex)Marshal.PtrToStructure(m_verts[i], typeof(HVertex));
               vert.ID = i;
            }
            for (int i = 0; i < m_edges.Count; i++)
            {
                HEdge edge = m_edges[i];
                edge.ID = i;
                m_edges[i] = edge;
            }
        }

        /// <summary>
        /// return true is HalfEdge structure is correct
        /// </summary>
        public bool CoerenceTest(out string message)
        {
            message = "notimplement";
            return false;
        }

        /// <summary>
        /// Build the edge table using vertices and faces table
        /// </summary>
        protected void calculateEdgeTable()
        {
            m_edges.Clear();
            int ecount = 0;

            int count = vertices.Count;

            for (int i = 0; i < m_faces.Count; i++)
            {
                HFace face = m_faces[i];
                if (face.marked) continue;

                for (int j = 0; j < 3; j++)
                {
                    //edge(x,y) not created
                    if (face.e[j]==null)
                    {
                        // create the edge(x,y) where x and y are the vertex ID of j-th edge
                        // using a fix order
                            switch (j)
                            {
                                case 0: face.e[j] = new HEdge(face.v[0], face.v[1]); break;
                                case 1: face.e[j] = new HEdge(face.v[1], face.v[2]); break;
                                case 2: face.e[j] = new HEdge(face.v[2], face.v[0]); break;
                            }
                         // if edge is created at first time, assign it in first position
                        face.e[j].f[0] = face;

                        // foreach all other faces, find a faces that contain same edge(x,y) using vertex id x and y
                        for (int ii = i + 1; ii < m_faces.Count; ii++)
                        {
                            // completly assigned, not need test
                            if (m_faces[ii].marked) continue;

                            // if idx == -1, the current face don't contain the x and y vertex
                            int idx = m_faces[ii].GetEdgeIndex(face.e[j].v[0], face.e[j].v[1]);
                            if (idx>-1)
                            {
                                // link the current edge
                                m_faces[ii].e[idx] = face.e[j];
                                // link in second position
                                face.e[j].f[1] = m_faces[ii];
                                // a edge contain only 2 neighbour faces, T junction is wrong,
                                // exit from searching
                                break;
                            }
                        }
                        face.e[j].ID = ecount++;
                        m_edges.Add(face.e[j]);
                    }
                }
                // mark this face as completly assigned
                m_faces[i].marked = true;
            }
            m_edges.TrimExcess();

            string stamp = "";
            foreach (HEdge e in m_edges)
                stamp += e.ToString() + "\n";
        }

        /// <summary>
        /// Collapse the edge, the two vertices will be fuse in a middle position, 
        /// incoerent or degenerated faces will be deleted.
        /// </summary>
        public bool Collapse(HEdge e)
        {
            HFace f0 = e.f[0];
            HFace f1 = e.f[1];

            //m_verts.Remove(vB);
            m_edges.Remove(e);

            return true;
        }

        /// <summary>
        /// Increase the tessellation splitting each faces in 4 sub-faces.
        /// </summary>
        /// <param name="edgeSorted">if edges index in each faces are sorted, skip some internal sorting</param>
        public void TriangleTessellate1(bool edgeSorted)
        {
            /* pseudocode:
             * foreach old faces 
             *    split in 4 faces
             *    foreach 3 border of face
             *      if not splitted, split in 2 edge and generate middle vertex
             *         add these 2 new edge to list
             *      else get the 2 splitted edges and middle vertex from e_crack list associated to this border
             *    add this 4 new faces to list
             * delete old faces and edges
             *
             *                 v0
             *                /  \
             *           eLT /    \ eRT
             *              /  T   \
             *           vcL________vcR
             *            / \  C   / \
             *       eLD /  eCL   eCR \ eRD
             *          /  L  \  /  R  \
             *         /_______\/_______\
             *        v1      vcD       v2
             *            eDL      eDR
             */

            // fix verts id
            ClearID();

            // the two crack edge for each edges. Need an array to custom access to all position 
            HEdge[] e_crack = new HEdge[m_edges.Count * 2];
            // the new center edges.
            HEdge[] tmp_edges = new HEdge[m_faces.Count * 3];

            // not need array, this temporary list will substitute the main list
            List<HFace> tmp_faces = new List<HFace>(m_faces.Count * 4);

            int vcount = m_verts.Count;
            int ecount = 0;

            for (int f = 0; f < m_faces.Count; f++)
            {
                IntPtr vL, vD, vR;
                HEdge eLT, eLD, eDR, eDL, eRD, eRT;
                HEdge eL, eD, eR;

                HFace froot = m_faces[f];



                //use always this scheme
                //f.e[0] = e(v0,v1)
                //f.e[1] = e(v1,v2)
                //f.e[2] = e(v0,v2)

                int iL = 0;
                int iD = 1;
                int iR = 2;

                if (!edgeSorted)
                {
                    // example:  eL is the e(v0,v1), but is possible that iL!=0, GetSortedEdges() resolve this issue
                    froot.GetSortedEdges(out iL, out iD, out iR);
                }
                eL = froot.e[iL];
                eD = froot.e[iD];
                eR = froot.e[iR];

                // build the reference of new child faces
                HFace fT = new HFace(tmp_faces.Count);
                tmp_faces.Add(fT);
                HFace fL = new HFace(tmp_faces.Count);
                tmp_faces.Add(fL);
                HFace fC = new HFace(tmp_faces.Count);
                tmp_faces.Add(fC);
                HFace fR = new HFace(tmp_faces.Count);
                tmp_faces.Add(fR);

                IntPtr v0 = froot.v[0];
                IntPtr v1 = froot.v[1];
                IntPtr v2 = froot.v[2];
                // link the neigthbour faces using the id of border edge splitted or not in two crack edges
                linkborder(eL.ID, e_crack, out vL, v0, v1, out eLT, out eLD, fT, fL);
                linkborder(eD.ID, e_crack, out vD, v1, v2, out eDL, out eDR, fL, fR);
                linkborder(eR.ID, e_crack, out vR, v2, v0, out eRD, out eRT, fR, fT);

                // centers faces and edges 
                HEdge eCL = new HEdge(vL, vD);
                HEdge eCR = new HEdge(vD, vR);
                HEdge eCT = new HEdge(vR, vL);

                tmp_edges[ecount++] = eCL;
                tmp_edges[ecount++] = eCR;
                tmp_edges[ecount++] = eCT;


                fT.v[0] = (v0);
                fT.v[1] = (vL);
                fT.v[2] = (vR);

                fL.v[0] = (vL);
                fL.v[1] = (v1);
                fL.v[2] = (vD);

                fC.v[0] = (vD);
                fC.v[1] = (vR);
                fC.v[2] = (vL);

                fR.v[0] = (vR);
                fR.v[1] = (vD);
                fR.v[2] = (v2);

                //use always this scheme to sort edges inside face
                //f.e[0] = e(v0,v1)
                //f.e[1] = e(v1,v2)
                //f.e[2] = e(v0,v2)

                fT.e[0] = eLT;
                fT.e[1] = eCT;
                fT.e[2] = eRT;

                fL.e[0] = eLD;
                fL.e[1] = eDL;
                fL.e[2] = eCL;

                fC.e[0] = eCR;
                fC.e[1] = eCT;
                fC.e[2] = eCL;

                fR.e[0] = eCR;
                fR.e[1] = eDR;
                fR.e[2] = eRD;

                // link central edges, they are always new so f[] is empty
                eCR.f[0] = fC;
                eCT.f[0] = fC;
                eCL.f[0] = fC;

                eCR.f[1] = fR;
                eCT.f[1] = fT;
                eCL.f[1] = fL;
            }

            m_faces = tmp_faces;

            // rewrite main edges list
            ecount = 0;
            m_edges.Clear();

            for (int i = 0; i < e_crack.Length; i++)
            {
                e_crack[i].ID = ecount++;
                m_edges.Add(e_crack[i]);
            }
            for (int i = 0; i < tmp_edges.Length; i++)
            {
                tmp_edges[i].ID = ecount++;
                m_edges.Add(tmp_edges[i]);
            }

            m_verts.TrimExcess();
            m_edges.TrimExcess();
            m_faces.TrimExcess();
        }

        /// <summary>
        /// </summary>
        /// <param name="eID">id of edged to split</param>
        /// <param name="e_crack">list of splitted edges</param>
        /// <param name="pvM">middle vertex</param>
        /// <param name="e0m">first crack edge</param>
        /// <param name="em1">second crack edge</param>
        void linkborder(int eID, HEdge[] e_crack, out IntPtr pvM, IntPtr pv0, IntPtr pv1, out HEdge e0m, out HEdge em1, HFace f0, HFace f1)
        {
            HEdge ecrack0 = e_crack[eID * 2];
            HEdge ecrack1 = e_crack[eID * 2 + 1];

            // if edges was splitted previosly
            if (ecrack0 != null)
            {
                // vM is the common vertices of two cracked edges
                pvM = ecrack0.v[0] == ecrack1.v[0] ? ecrack0.v[0] : ecrack0.v[1];

                // get the correct order
                if (ecrack0.v[0] == pv0 || ecrack0.v[1]==pv0)
                {
                    e0m = ecrack0;
                    em1 = ecrack1;
                }
                else
                {
                    e0m = ecrack1;
                    em1 = ecrack0;
                }
                //link faces to border edges, if was from splitted edge, the first face is assigned previosly
                e0m.f[1] = f0;
                em1.f[1] = f1;
            }
            // not splitted, create the middle vertex and two split edges
            else
            {
                
                HVertex vm = new HVertex(m_verts.Count);
                HVertex v0 = (HVertex)Marshal.PtrToStructure(pv0, typeof(HVertex));
                HVertex v1 = (HVertex)Marshal.PtrToStructure(pv1, typeof(HVertex));
                
                vm.color = (int)(v0.color * 0.5f + v1.color * 0.5f);
                vm.normal = v0.normal;
                vm.texture = (v0.texture + v1.texture) * 0.5f;
                vm.position = (v0.position + v1.position) * 0.5f;

                pvM = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(HVertex)));
                Marshal.StructureToPtr(vm, pvM, false);
                m_verts.Add(pvM);
                
                e0m = new HEdge(pv0, pvM);
                em1 = new HEdge(pvM, pv1);
                ecrack0 = e0m;
                ecrack1 = em1;

                // assign the currect face to first position, if the main edge have a second faces,
                // the new faces will be link in the second position
                e0m.f[0] = f0;
                em1.f[0] = f1;
            }
            e_crack[eID * 2] = ecrack0;
            e_crack[eID * 2 + 1] = ecrack1;
        }

        /// <summary>
        /// Convert the half-edge structure in the base structure
        /// </summary>
        MeshListGeometry getTriMesh()
        {
            ClearID();

            MeshListGeometry mesh = new MeshListGeometry();

            if (m_verts.Count > ushort.MaxValue - 1)
                throw new OverflowException("too many vertices for a 16 bit indices");

            mesh.vertices = new VertexAttribute<Vector3>(DeclarationUsage.Position, m_verts.Count);
            mesh.colors = new VertexAttribute<Color32>(DeclarationUsage.Color, m_verts.Count);
            mesh.normals = new VertexAttribute<Vector3>(DeclarationUsage.Normal, m_verts.Count);
            mesh.texcoords = new VertexAttribute<Vector2>(DeclarationUsage.TexCoord, m_verts.Count);

            for (int i = 0; i < m_verts.Count; i++)
            {
                HVertex v = (HVertex)Marshal.PtrToStructure(m_verts[i], typeof(HVertex));
                mesh.vertices[i] = v.position;
                mesh.colors[i] = Color.FromArgb(v.color);
                mesh.normals[i] = v.normal;
                mesh.texcoords[i] = v.texture;
            }

            mesh.indices = new IndexAttribute<Face16>(m_faces.Count);
            for (int i = 0; i < m_faces.Count; i++)
            {
                HVertex v0 = (HVertex)Marshal.PtrToStructure(m_faces[i].v[0], typeof(HVertex));
                HVertex v1 = (HVertex)Marshal.PtrToStructure(m_faces[i].v[1], typeof(HVertex));
                HVertex v2 = (HVertex)Marshal.PtrToStructure(m_faces[i].v[2], typeof(HVertex));

                mesh.indices[i] = new Face16((ushort)v0.ID, (ushort)v1.ID, (ushort)v2.ID);
            }
            return mesh;
        }
        /// <summary>
        /// Convert the base structure to half-edge structure
        /// </summary>
        void setTriMesh(IList<Face16> faces, IList<Vector3> vertices, IList<Color32> colors)
        {
            m_verts.Clear();
            m_faces.Clear();

            int i = 0;
            if (vertices != null)
            {
                for (i = 0; i < vertices.Count; i++)
                {
                    HVertex v = new HVertex(i);
                    v.position = vertices[i];
                    v.ID = i;
                    if (colors != null) v.color = (int)colors[i].argb;

                    IntPtr pnt = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(HVertex)));
                    Marshal.StructureToPtr(v, pnt, false);
                    m_verts.Add(pnt);         
                }
            }
            else
            {
                throw new ArgumentNullException();
            }
            i = 0;
            unsafe
            {
                foreach (Face16 f in faces)
                {
                    IntPtr pv0 = m_verts[f.I];
                    IntPtr pv1 = m_verts[f.J];
                    IntPtr pv2 = m_verts[f.K];
                    m_faces.Add(new HFace(pv0, pv1, pv2, i++));
                }
            }
            m_verts.TrimExcess();
            m_faces.TrimExcess();
        }
    }
}
