﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Drawing;

using Engine.Maths;

namespace Engine.Geometry.FaceEdgeVertex
{
    /// <summary>
    /// Vertex of HalfEdge format, the base version must contain a 3d vector
    /// </summary>
    public class HVertex : IVertexPosition
    {
        public int ID = -1;
        public Vector3 position { get; set; }

        public HVertex(Vector3 pos, int id)
        {
            this.position = pos;
            this.ID = id;
        }
        public HVertex(int id) : this(Vector3.Zero, id) { }
        public HVertex(Vector3 pos) : this(pos, -1) { }
        public HVertex() : this(Vector3.Zero, -1) { }

        public override string ToString()
        {
            return "v" + ID.ToString();
        }

        public static HVertex Avarage(HVertex v0, HVertex v1)
        {
            HVertex vm = new HVertex(-1);
            vm.position = (v0.position + v1.position) * 0.5f;
            return vm;
        }

    }

    /// <summary>
    /// Edge, the halfedge structure require 2 pointers to vertices and maximum 2 faces
    /// </summary>
    public class HEdge<V, E, F>
        where V : HVertex, new()
        where E : HEdge<V, E, F>, new()
        where F : HFace<V, E, F>, new()
    {
        public int ID = -1;

        /// <summary>
        /// Two endpoints, must respect chain order
        /// </summary>
        public V[] v = new V[2];
        /// <summary>
        /// Adjacent faces
        /// </summary>
        public F[] f = new F[2];

        public HEdge(V v0, V v1, F f0, F f1 ,int id)
        {
            this.v[0] = v0;
            this.v[1] = v1;
            this.f[0] = f0;
            this.f[1] = f1;
            this.ID = id;
        }
        public HEdge(V v0, V v1, int id) : this(v0, v1, null, null, id) { }
        public HEdge()
        {
            ID = -1;
        }

        /// <summary>
        /// Return index in e[] array, return -1 if not found
        /// </summary>
        public int GetVertexIndex(HVertex vertex)
        {
            if (v[0] == vertex) return 0;
            else if (v[1] == vertex) return 1;
            else return -1;
        }

        public override string ToString()
        {
            return string.Format("e{0}({1},{2}) f{3} f{4}", ID, (v[0]).ID, (v[1]).ID, (f[0] != null) ? f[0].ID.ToString() : "-", (f[1] != null) ? f[1].ID.ToString() : "-");
        }
    }

    public class HFace<V, E, F>
        where V : HVertex, new()
        where E : HEdge<V, E, F>, new()
        where F : HFace<V, E, F>, new()
    {
        public int ID = -1;

        /// <summary>
        /// The position in the array match with GetEdgeIdxContainVertsID() algorithm to mantain a sort or "chain" of edges
        /// </summary>
        /// <remarks>
        /// I use this scheme:
        /// e[0] = e(v0,v1)
        /// e[1] = e(v1,v2)
        /// e[2] = e(v2,v0)
        /// </remarks>
        public E[] e = new E[3];
        /// <summary>
        /// Set vertices in counterclockwire
        /// </summary>
        public V[] v = new V[3];

        public HFace(V v0, V v1, V v2, E e0, E e1, E e2, int id)
        {
            this.v[0] = v0;
            this.v[1] = v1;
            this.v[2] = v2;
            this.e[0] = e0;
            this.e[1] = e1;
            this.e[2] = e2;
            this.ID = id;
        }
        public HFace(V v0, V v1, V v2, int id)
        {
            this.v[0] = v0;
            this.v[1] = v1;
            this.v[2] = v2;
            this.ID = id;
        }
        public HFace()
        {
            this.ID = -1;
        }

        /// <summary>
        /// Return the edge index in this face what contain id1 and id2 vertices.
        /// </summary>
        /// <returns>
        /// -1 not fount
        /// -2 incoerent id, the face contain two identical vertices
        /// </returns>
        public int GetEdgeChainIndex(V vx, V vy)
        {
            // using flag to un-mark the vertices that mach with eij
            // 110 ; 6 return edge[0] because find first and second index
            // 011 ; 3 return edge[1] because find second and third index
            // 101 ; 5 return edge[2] because find first and third index
            // 111 ; 7 face contain two vertice with same index, error
            // 000 001 010 100; 0 1 2 4 face don't contain this edge(id1,id2), return -1
            byte pos = 0;
            byte mark = 4;
            for (int i = 0; i < 3; i++, mark >>= 1)
            {
                if (v[i] == vx || v[i] == vy)
                    pos |= mark;
            }
            if (pos == 6) return 0;
            else if (pos == 3) return 1;
            else if (pos == 5) return 2;
            else if (pos == 7) return -2;
            else return -1;
        }
        /// <summary>
        /// return the position of edge in the e[] array
        /// </summary>
        public int GetEdgeIndex(E edge)
        {
            for (int i = 0; i < 3; i++) if (e[i] == edge) return i;
            return -1;
        }
        /// <summary>
        /// return the position of vertex in the v[] array
        /// </summary>
        public int GetVertexIndex(V vertex)
        {
            for (int i = 0; i < 3; i++)
                if (v[i] == vertex) return i;
            return -1;
        }
        /// <summary>
        /// return the position of edge in the e[] array that contain vertex
        /// </summary>
        /// <param name="ie">the first e[ie] contain vertex v</param>
        /// <param name="iv">the vertex v, not check is it's in face.v[]</param>
        /// <returns>the second e[i] contain vertex v</returns>
        public int GetNextEdgeContainVertex(int ie, V vertex)
        {
            ie = (ie + 1) % 3;
            if (e[ie].v[0] == vertex || e[ie].v[0] == vertex) return ie;
            else return (ie + 1) % 3;
        }
        /// <summary>
        /// return if face don't contain duplicated vertices
        /// </summary>
        public bool IsCoerent(out string message)
        {
            if (v[0] == v[1] || v[0] == v[2] || v[1] == v[2])
            {
                message = string.Format("collapsed vertices {0} {1} {2}", v[0], v[1], v[2]);
                return false;
            }
            int i, j, k;
            GetSortedEdges(out i, out j, out k);
            if (i != 0 || j != 1 || k != 2)
            {
                message = string.Format("wrong edge order {0} {1} {2}", i, j, k);
                return false;
            }
            message = "ok";
            return true;
        }
        /// <summary>
        /// is possible that edges are not sorted in default chain
        /// </summary>
        /// <param name="i0">the correct position what e[0] must have , if == 0 edge0 are in correct position</param>
        public void GetSortedEdges(out int i0, out int i1, out int i2)
        {
            // I use this scheme:
            // e[0] = e(v0,v1)
            // e[1] = e(v1,v2)
            // e[2] = e(v2,v0)
            i0 = GetEdgeChainIndex((e[0].v[0]), (e[0].v[1]));
            i1 = GetEdgeChainIndex((e[1].v[0]), (e[1].v[1]));
            i2 = GetEdgeChainIndex((e[2].v[0]), (e[2].v[1]));
        }
        /// <summary>
        /// take a look to comment, ie0 is the first edge contain v, ie1 in the opposite edge of v,
        /// ie2 is the second edge contain v
        /// </summary>
        /// <param name="containv">next edge contain vertex</param>
        /// <param name="oppositev">edge in opposite than vertex</param>
        /// <param name="ie">edge index contain vertex and so start</param>
        public void GetSortedChainEdges(int ie, V vertex, out int oppositev, out int containv)
        {
            //       /'\
            //  ie  /   \ oppositev
            //     /     \
            //    /       \
            //  v'<--------' 
            //         containv
            oppositev = (ie + 1) % 3;
            containv = (oppositev + 1) % 3;

            if (e[oppositev].v[0] == vertex || e[oppositev].v[1] == vertex)
            {
                // swap
                int tmp = containv; containv = oppositev; oppositev = tmp;
            }
        }
        /// <summary>
        /// Get the index of edge in opposite side from vertex, is the edge that not contain v
        /// Return -1 if not found
        /// </summary>
        public int GetOppositeEdge(V vertex)
        {
            //       /'\
            //      /   \ ie
            //     /     \
            //    /       \
            //  v'<--------' 
            for (int i = 0; i < 3; i++)
                if (e[i].v[0] != vertex && e[i].v[1] != vertex)
                    return i;
            return -1;
        }
        /// <summary>
        /// Get the index of vertex in opposite side from edge, is the vertex that not contain e
        /// Return 0,1,or 2, no safety test 
        /// </summary>
        public int GetOppositeVertex(E edge)
        {
            for (int i = 0; i < 2; i++)
                if (v[i] != edge.v[0] && v[i] != edge.v[1]) return i;
            return 2;
        }

        public void SetEdgeInChainOrder()
        {
            E e0 = e[0];
            E e1 = e[1];
            E e2 = e[2];
            int i0, i1, i2;
            GetSortedEdges(out i0, out  i1, out  i2);
            e[i0] = e0;
            e[i1] = e1;
            e[i2] = e2;
        }

        public override string ToString()
        {
            return string.Format("f{0} [{1} {2} {3}] -> {4} {5} {6}", ID, (v[0]).ID, (v[1]).ID, (v[2]).ID, e[0], e[1], e[2]);
        }
    }
}
