﻿// by johnwhile
using System;
using System.Diagnostics;
using System.Drawing;
using System.Collections.Generic;
using System.Text;

using Engine.Maths;

namespace Engine.Geometry
{
    /// <summary>
    /// Menage a continue list of vertices as 2d polygon
    /// </summary>
    public class Polygon
    {
        public List<Face16> triangles;
        public List<Vector2> vertices;

        public Polygon()
        {
            vertices = new List<Vector2>();
            triangles = new List<Face16>();
        }

        public Polygon(IEnumerable<Vector2> point_chain)
        {
            vertices = new List<Vector2>(point_chain);
            triangles = new List<Face16>();
        }

        /// <summary>
        /// Return the orientation of points list. IsConvex is a useful information.
        /// calculate the sum of area
        /// </summary>
        public static Cull GetVerticesOrder(IList<Vector2> vertices, out bool isPolygonConvex)
        {
            int convexcount = 0;
            int numverts = vertices.Count;
            isPolygonConvex = false;

            if (numverts < 3) return Cull.None;

            float sum = 0;

            for (int i = 0; i < numverts; i++)
            {
                Vector2 p0 = vertices[(i - 1) % numverts];
                Vector2 p1 = vertices[i];
                Vector2 p2 = vertices[(i + 1) % numverts];

                float area = GetTriangleArea(p0, p1, p2);
                sum += area;

                if (area > 0)
                    convexcount++;
                else
                    convexcount--;
            }

            if (convexcount > 0)
                isPolygonConvex = convexcount == numverts;
            else if (convexcount < 0)
                isPolygonConvex = convexcount == -numverts;

            return sum > 0 ? Cull.Clockwise : Cull.CounterClockwise;
        }

        /// <summary>
        /// determines area of triangle formed by three points
        /// </summary>
        public static float GetTriangleArea(Vector2 a, Vector2 b, Vector2 c)
        {
            return (a.x * (c.y - b.y) + b.x * (a.y - c.y) + c.x * (b.y - a.y)) * 0.5f;
        }
        /// <summary>
        /// returns true if point 'b' is convex
        /// </summary>
        public static bool IsConvex(Vector2 a, Vector2 b, Vector2 c)
        {
            return GetTriangleArea(a, b, c) > 0;
        }
    }
}
