﻿// just to know if System.Array has same performance
#define USESTANDARDARRAY

using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Runtime.InteropServices;

namespace Engine.Tools
{
    /// <summary>
    /// My necessary common collection interface,
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IArray<T> : IEnumerable<T>, IList<T> { }

    public interface IPinnable
    {
        IntPtr GetPointer();
        void ReleasePointer();
    }

#if USESTANDARDARRAY
    /// <summary>
    /// Static array, for fized capacity size, to generalize the IList interface i add some
    /// Add/Remove/Insert function like Dynamic array
    /// </summary>
    [DebuggerDisplay("Count = {count} , Size = {Capacity}")]
    public class SArray<T> : IArray<T> , IPinnable 
        where T : struct
    {
        GCHandle handle;
        IntPtr pointer;
        internal T[] array;
        int count = 0;

        /// <summary>
        /// Initialize and populate with source elements
        /// </summary>
        public SArray(T[] source)
        {
            array = (T[])source.Clone();
            count = source.GetUpperBound(0) + 1;
        }
        /// <summary>
        /// Initialize and populate array with default value, to use Add function call Clear() before
        /// </summary>
        public SArray(int capacity)
        {
            array = new T[capacity];
            count = 0;
        }
        /// <summary>
        /// Initialize and populate array
        /// </summary>
        public SArray(int capacity, T initialvalue)
        {
            array = new T[capacity];
            for (int i = 0; i < capacity; i++) array[i] = initialvalue;
            count = capacity;
        }

        ~SArray()
        {
            ReleasePointer();
        }

        public int Count
        {
            get { return count; }
            set
            {
                if (value < 0 || value > Capacity) throw new ArgumentOutOfRangeException("not correct value");
                count = value;
            }
        }
        public int Capacity
        {
            get { return array.GetUpperBound(0) + 1; }
        }
        public T this[int index]
        {
            get { return array[index]; }
            set { array[index] = value; }
        }


        public SArray<T> Clone()
        {
            return new SArray<T>(array);
        }
        public IntPtr GetPointer()
        {
             handle = GCHandle.Alloc(array, GCHandleType.Pinned); 
            pointer= handle.AddrOfPinnedObject();
            return pointer;
        }
        public void ReleasePointer()
        {
            if (handle.IsAllocated) handle.Free();
            pointer = IntPtr.Zero;
        }


        #region Enumerable
        public IEnumerator<T> GetEnumerator()
        {
            for (int i = 0; i < Count; i++)
                yield return array[i];
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        public int IndexOf(T item)
        {
            if (count <= 0) return -1;
            return Array.BinarySearch<T>(array, 0, count, item);
        }

        /// <summary>
        /// Indert a new item in the specified index, all value from "index" to "count" are moved up.
        /// Check if capacity can store a new item
        /// </summary>
        public void Insert(int index, T item)
        {
            if (count + 1 >= Capacity) throw new IndexOutOfRangeException("reach the capacity limit");
            for (int i = count; i > index; i--)
                array[i] = array[i - 1];
            array[index] = item;
            count++;
        }

        public void RemoveAt(int index)
        {
            if (index >= count) throw new IndexOutOfRangeException("reach the count limit");
            removeat(index);
        }

        public void Add(T item)
        {
            if (count >= Capacity) throw new IndexOutOfRangeException("reach the capacity limit");
            array[count++] = item;
        }

        public void Clear()
        {
            count = 0;
        }

        public bool Contains(T item)
        {
            return IndexOf(item) > -1;
        }

        public void CopyTo(T[] destination, int destinationIndex)
        {
            if (count <= 0) return;
            // la destinazione deve avere dimensioni destinationIndex + this.count

            if (count + destinationIndex > destination.GetUpperBound(0) + 1)
                throw new IndexOutOfRangeException("destination array too small");

            for (int i = 0; i < count; i++)
                destination[i + destinationIndex] = array[i];
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(T item)
        {
            int i = IndexOf(item);
            if (i < 0) return false;
            removeat(i);
            return true;
        }

        /// <summary>
        /// not safety check
        /// </summary>
        void removeat(int index)
        {
            for (int i = index; i < count - 1; i++)
                array[i] = array[i + 1];
            count--;
        }
        #endregion
    }
#else
    /// <summary>
    /// Static array, for fized capacity size, to generalize the IList interface i add some
    /// Add/Remove/Insert function like Dynamic array
    /// </summary>
    [DebuggerDisplay("Count = {count} , Size = {Capacity}")]
    public class SArray<T> : IArray<T>
    {
        GCHandle handle;
        internal Array array;
        int count = 0;

        /// <summary>
        /// Initialize and populate with source elements
        /// </summary>
        public SArray(T[] source)
        {
            array = (T[])source.Clone();
            count = source.GetUpperBound(0) + 1;
        }
        /// <summary>
        /// Initialize and populate array with default value, to use Add function call Clear() before
        /// </summary>
        public SArray(int capacity)
        {
            array = Array.CreateInstance(typeof(T), capacity);
            count = capacity;
        }
        /// <summary>
        /// Initialize and populate array
        /// </summary>
        public SArray(int capacity, T initialvalue)
        {
            array = new T[capacity];
            for (int i = 0; i < capacity; i++) array.SetValue(initialvalue, i);
            count = capacity;
        }

        ~SArray()
        {
            if (handle.IsAllocated) handle.Free();
        }

        public int Count
        {
            get { return count; }
            set
            {
                if (value < 0 || value > Capacity) throw new ArgumentOutOfRangeException("not correct value");
                count = value;
            }
        }
        public int Capacity
        {
            get { return array.GetUpperBound(0) + 1; }
        }

        public T this[int index]
        {
            get { return (T)array.GetValue(index); }
            set { array.SetValue(value, index); }
        }

        public IEnumerator<T> GetEnumerator()
        {
            for (int i = 0; i < Count; i++)
                yield return this[i];
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        public int IndexOf(T item)
        {
            if (count <= 0) return -1;
            return Array.BinarySearch(array, 0, count, item);
        }

        /// <summary>
        /// Indert a new item in the specified index, all value from "index" to "count" are moved up.
        /// Check if capacity can store a new item
        /// </summary>
        public void Insert(int index, T item)
        {
            if (count + 1 >= Capacity) throw new IndexOutOfRangeException("reach the capacity limit");
            for (int i = count; i > index; i--)
                array.SetValue(array.GetValue(i - 1), i);
            array.SetValue(item, index);
            count++;
        }

        public void RemoveAt(int index)
        {
            if (index >= count) throw new IndexOutOfRangeException("reach the count limit");
            removeat(index);
        }

        public void Add(T item)
        {
            if (count >= Capacity) throw new IndexOutOfRangeException("reach the capacity limit");
            array.SetValue(item, count++);
        }

        public void Clear()
        {
            count = 0;
        }

        public bool Contains(T item)
        {
            return IndexOf(item) > -1;
        }

        public void CopyTo(T[] destination, int destinationIndex)
        {
            if (count <= 0) return;
            // la destinazione deve avere dimensioni destinationIndex + this.count

            if (count + destinationIndex > destination.GetUpperBound(0) + 1)
                throw new IndexOutOfRangeException("destination array too small");

            for (int i = 0; i < count; i++)
                destination[i + destinationIndex] = this[i];
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(T item)
        {
            int i = IndexOf(item);
            if (i < 0) return false;
            removeat(i);
            return true;
        }

        /// <summary>
        /// not safety check
        /// </summary>
        void removeat(int index)
        {
            for (int i = index; i < count - 1; i++)
                array.SetValue(array.GetValue(i + 1), i);
            count--;
        }
    }


#endif
    /// <summary>
    /// Dinamic array , improve the capacity resize and Add/Remove/Insert function
    /// </summary>
    [DebuggerDisplay("Count = {count} , Size = {Capacity}")]
    public class DArray<T> : IArray<T>
    {
        internal List<T> array;

        public DArray(int capacity) : this(capacity, default(T)) { }
        public DArray(int capacity, T initialvalue)
        {
            array = new List<T>(new T[capacity]);
            for (int i = 0; i < capacity; i++) array[i] = initialvalue;
        }

        public int Count
        {
            get { return array.Count; }
        }
        public int Capacity
        {
            get { return array.Capacity; }
        }
        public T this[int index]
        {
            get { return array[index]; }
            set { array[index] = value; }
        }

        #region Enumerable
        public IEnumerator<T> GetEnumerator()
        {
            for (int i = 0; i < Count; i++)
                yield return array[i];
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        public int IndexOf(T item)
        {
            return array.IndexOf(item);
        }

        public void Insert(int index, T item)
        {
            array.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            array.RemoveAt(index);
        }

        public void Add(T item)
        {
            array.Add(item);
        }

        public void Clear()
        {
            array.Clear();
        }

        public bool Contains(T item)
        {
            return array.Contains(item);
        }

        public void CopyTo(T[] destination, int destinationIndex)
        {
            array.CopyTo(destination, destinationIndex);
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(T item)
        {
            return array.Remove(item);
        }
        #endregion
    }
}
