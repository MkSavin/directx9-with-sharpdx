﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;

namespace Engine.Tools
{
    public class FilePathName
    {
        string path = "";

        public FilePathName(string filename)
        {
            path = Path.GetFullPath(filename);
        }


        public string Extension
        {
            get { return Path.GetExtension(path); }
        }

        public string Filename
        {
            get { return Path.GetFileNameWithoutExtension(path); }
        }

        public string Directory
        {
            get { return Path.GetDirectoryName(path); }
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }





    /// <summary>
    /// 
    /// </summary>
    /// <param name="percent">1.0 = 100% </param>
    public delegate void FileProgressEventHandler(float percent);

}
