﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Globalization;

using Engine.Maths;
using Engine.Geometry;

namespace Engine.Tools
{

    public class ModelMesh
    {
        public enum ModelType : byte
        {
            MESH3D = 1,
        }

        public enum ModelDataType : byte
        {
            VERTEX = 0,
            TEXCOORD = 1,
            NORMAL = 2,
            INDEX = 3,
            TEXTURE = 4
        }
        public enum ModelDataFormat : byte
        {
            FLOAT1 = 1,
            FLOAT2 = 2,
            FLOAT3 = 3,
            FLOAT4 = 4,
            FLOAT4x4 = 44,
            INDEX16 = 16,
            INDEX32 = 32
        };

        public ModelType type;
        public string name;
        public MeshListGeometry mesh;

        Vector3[] vertices;
        Vector2[] texcoords;
        Vector3[] normals;
        Face16[] faces;


        public bool readbin(BinaryReader reader)
        {
            mesh = new MeshListGeometry();


            type = (ModelType)reader.ReadByte();
            name = Model.ReadString(reader);

            ModelDataType datatype;
            ModelDataFormat dataformat;
            int size;

            datatype = (ModelDataType)reader.ReadByte();
            dataformat = (ModelDataFormat)reader.ReadByte();
            size = reader.ReadInt32();
            vertices = new Vector3[size];
            for (int i = 0; i < size; i++)
            {
                vertices[i] = new Vector3(
                    reader.ReadSingle(),
                    reader.ReadSingle(),
                    reader.ReadSingle());
            }
            mesh.vertices = new VertexAttribute<Vector3>(DeclarationUsage.Position, ref vertices);

            datatype = (ModelDataType)reader.ReadByte();
            dataformat = (ModelDataFormat)reader.ReadByte();
            size = reader.ReadInt32();
            texcoords = new Vector2[size];
            for (int i = 0; i < size; i++)
            {
                texcoords[i] = new Vector2(
                    reader.ReadSingle(),
                    reader.ReadSingle());
            }
            mesh.texcoords = new VertexAttribute<Vector2>(DeclarationUsage.TexCoord, ref texcoords);

            datatype = (ModelDataType)reader.ReadByte();
            dataformat = (ModelDataFormat)reader.ReadByte();
            size = reader.ReadInt32();
            normals = new Vector3[size];
            for (int i = 0; i < size; i++)
            {
                normals[i] = new Vector3(
                    reader.ReadSingle(),
                    reader.ReadSingle(),
                    reader.ReadSingle());
            }
            mesh.normals = new VertexAttribute<Vector3>(DeclarationUsage.Normal, ref normals);

            datatype = (ModelDataType)reader.ReadByte();
            dataformat = (ModelDataFormat)reader.ReadByte();
            size = reader.ReadInt32();
            faces = new Face16[size];
            for (int i = 0; i < size; i++)
            {
                faces[i] = new Face16(
                    reader.ReadUInt16(),
                    reader.ReadUInt16(),
                    reader.ReadUInt16());
            }
            mesh.indices = new IndexAttribute<Face16>(ref faces);

            //mesh.indices = new IndexAttribute<Face16>(readData(reader, out datatype));

            return true;
        }


        private static byte[] readData(BinaryReader reader, out ModelDataType type)
        {
            type = (ModelDataType)reader.ReadByte();
            ModelDataFormat format = (ModelDataFormat)reader.ReadByte();
            int size = reader.ReadInt32();

            int formatbytes = 0;
            switch (format)
            {
                case ModelDataFormat.FLOAT1: formatbytes = 4; break;
                case ModelDataFormat.FLOAT2: formatbytes = 8; break;
                case ModelDataFormat.FLOAT3: formatbytes = 12; break;
                case ModelDataFormat.FLOAT4: formatbytes = 16; break;
                case ModelDataFormat.FLOAT4x4: formatbytes = 16 * 4; break;
                case ModelDataFormat.INDEX16: formatbytes = 2 * 3; break;
                case ModelDataFormat.INDEX32: formatbytes = 4 * 3; break;
                default: throw new ArgumentOutOfRangeException();
            }
            return reader.ReadBytes(formatbytes * size);
        }

    }

    public class Model
    {


        string Header;
        int Version;
        public List<ModelMesh> models;

        public Model()
        {          

        }

        public static string ReadString(BinaryReader reader)
        {
            int count = reader.ReadInt32();
            if (count > 0 || count < 1000)
                return new string(reader.ReadChars(count));
            else
                return string.Empty;
        }


        public bool OpenFile(string filepath)
        {
            if (!File.Exists(filepath)) return false;
            using (FileStream file = new FileStream(filepath, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                if (!file.CanRead) return false;
                using (BinaryReader reader = new BinaryReader(file))
                {
                    Header = ReadString(reader);
                    Version = reader.ReadInt32();
                    int numgroups = reader.ReadInt32();
                    
                    models = new List<ModelMesh>(numgroups);

                    for (int i = 0; i < numgroups; i++)
                    {
                        ModelMesh mesh = new ModelMesh();
                        if (mesh.readbin(reader)) models.Add(mesh);
                    }
                }
             }

            return true;
        }

        public bool SaveFile(string filepath)
        {
            FileStream file = new FileStream(filepath, FileMode.Create, FileAccess.Write, FileShare.Write);
            if (!file.CanWrite) return false;
            
            StreamWriter writer = new StreamWriter(file);
            return true;
        }

    }
}
