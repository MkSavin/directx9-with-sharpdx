﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Engine.Tools
{
    public class OpenFileFilters
    {
        string m_name = "txt files";
        string m_ext= "txt";

        public string Name
        { 
            get { return m_name; }
            set { m_name = value; }
        }
        public string Extension
        { 
            get { return m_ext; }
            set { m_ext = value; }
        }


        public OpenFileFilters(string name, string extension)
        {
            Name = name;
            Extension = extension;
        }

        public override string ToString()
        {
            return string.Format("{0} (*.{1})|*.{1}", m_name, m_ext);
        }
    }


    public class OpenFileForm
    {
        public string InitialDirectory = "c:\\";
        public List<OpenFileFilters> Filters;

        string getfilters()
        {
            StringBuilder str = new StringBuilder();
            for (int i = 0; i < Filters.Count; i++)
            {
                str.Append(Filters[i].ToString());
                if (Filters.Count > 1 && i < Filters.Count - 1) str.Append('|');
            }
            return str.ToString();
        }

        public string GetFilePatch()
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            //openFileDialog1.InitialDirectory = "c:\\";
            openFileDialog1.Filter = getfilters();
            openFileDialog1.FilterIndex = Filters.Count;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                return openFileDialog1.FileName;
            }
            return null;
        }


        public OpenFileForm()
        {
            Filters = new List<OpenFileFilters>();
            Filters.Add(new OpenFileFilters("txt files", "txt"));
            Filters.Add(new OpenFileFilters("All files", ""));
        }
    }
}
