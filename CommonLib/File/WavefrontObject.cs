﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Globalization;

using Engine.Maths;

namespace Engine.Tools
{
    public class WavefrontMesh
    {
        public AABBox bounding;
        public List<Vector3> vertices = new List<Vector3>();
        public List<Vector2> texcoords = new List<Vector2>();
        public List<Vector3> normals = new List<Vector3>();
        public List<Face16> faces = new List<Face16>();
        public string name = "undefined";
    }

    public class Wavefront
    {
        WavefrontMesh current;
        List<WavefrontMesh> groups;
        bool readyfornewgroup = true;

        public Wavefront()
        {          
            current = new WavefrontMesh();
            groups = new List<WavefrontMesh>();
            readyfornewgroup = true;
        }

        string[] SpaceSeparator = new string[] { " " };
        string[] SlashSeparator = new string[] { "/" };

        float[] SplitFloat(string values, int count)
        {
            string[] str = values.Split(SpaceSeparator, StringSplitOptions.RemoveEmptyEntries);
            float[] array = new float[count];
            for (int i = 0; i < count; i++) array[i] = float.Parse(str[i], CultureInfo.InvariantCulture);
            return array;
        }
        int[] SplitInteger(string values, int count)
        {
            string[] str = values.Split(SpaceSeparator, StringSplitOptions.RemoveEmptyEntries);
            int[] array = new int[count];
            for (int i = 0; i < count; i++) array[i] = int.Parse(str[i]);
            return array;
        }
        int[] SplitIntegerSlash(string values, int count)
        {
            string[] str = values.Split(SpaceSeparator, StringSplitOptions.RemoveEmptyEntries);
            int[] array = new int[count];
            for (int i = 0; i < count; i++) array[i] = int.Parse(str[i]);
            return array;
        }

        void AddVertex(string values)
        {
            float[] f = SplitFloat(values, 3);
            current.vertices.Add(new Vector3(f[0], f[1], f[2]));
        }
        void AddTexCoord(string values)
        {
            float[] f = SplitFloat(values, 3);
            current.texcoords.Add(new Vector2(f[0], f[1]));
        }
        void AddNormal(string values)
        {
            float[] f = SplitFloat(values, 3);
            current.normals.Add(new Vector3(f[0], f[1], f[2]));
        }
        void AddFace(string values)
        {
            int[] idx = new int[] { 0, 0, 0 };

            string[] str = values.Split(SpaceSeparator, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < 3; i++)
            {
                string[] istr = str[i].Split(SlashSeparator, StringSplitOptions.RemoveEmptyEntries);
                switch (istr.Length)
                {
                    case 1: idx[i] = int.Parse(istr[0]) - 1; break;
                    case 2: idx[i] = int.Parse(istr[0]) - 1; break;
                    case 3: idx[i] = int.Parse(istr[0]) - 1; break;
                }
            }
            current.faces.Add(new Face16(idx[0], idx[1], idx[2]));
        }

        void AddGroup(string value)
        {
            current.name = value;
            groups.Add(current);
        }

        void SplitLine(string line, out string keyword, out string arguments)
        {
            int idx = line.IndexOf(' ');
            if (idx < 0)
            {
                keyword = line;
                arguments = null;
                return;
            }
            keyword = line.Substring(0, idx);
            arguments = line.Substring(idx + 1);
        }

        public bool OpenFile(string filepath)
        {
            if (!File.Exists(filepath)) return false;
            FileStream file = new FileStream(filepath, FileMode.Open, FileAccess.Read, FileShare.Read);

            if (!file.CanRead) return false;
            StreamReader reader = new StreamReader(file);

            while (!reader.EndOfStream)
            {
                string line = reader.ReadLine();
                if (line == null) break;

                line = line.Trim();

                if (line.StartsWith("#") || line.Length == 0) continue;

                string keyword, values;
                SplitLine(line, out keyword, out values);

                switch (keyword.ToLower())
                {
                    // Vertex data
                    case "v": AddVertex(values); break;// geometric vertices
                    case "vt": AddTexCoord(values); break;// texture vertices
                    case "vn": AddNormal(values); break;// vertex normals
                    case "vp": // parameter space vertices
                    case "cstype": // rational or non-rational forms of curve or surface type: basis matrix, Bezier, B-spline, Cardinal, Taylor
                    case "degree": // degree
                    case "bmat": // basis matrix
                    case "step": // step size
                        break;

                    // Elements
                    case "f": AddFace(values); break;// face
                    case "p": // point
                    case "l": // line
                    case "curv": // curve
                    case "curv2": // 2D curve
                    case "surf": // surface
                        break;

                    // Free-form curve/surface body statements
                    case "parm": // parameter name
                    case "trim": // outer trimming loop (trim)
                    case "hole": // inner trimming loop (hole)
                    case "scrv": // special curve (scrv)
                    case "sp":  // special point (sp)
                    case "end": // end statement (end)
                        break;

                    // Connectivity between free-form surfaces
                    case "con": // connect
                        break;

                    // Grouping
                    case "g": AddGroup(values); break;// group name
                    case "s": // smoothing group
                    case "mg": // merging group
                    case "o": // object name
                        break;

                    // Display/render attributes
                    case "mtllib": // material library
                    case "usemtl": // material name
                    case "usemap": // texture map name
                    case "bevel": // bevel interpolation
                    case "c_interp": // color interpolation
                    case "d_interp": // dissolve interpolation
                    case "lod": // level of detail
                    case "shadow_obj": // shadow casting
                    case "trace_obj": // ray tracing
                    case "ctech": // curve approximation technique
                    case "stech": // surface approximation technique
                        break;
                }
            }
            return true;
        }

        public bool SaveFile(string filepath)
        {
            FileStream file = new FileStream(filepath, FileMode.Create, FileAccess.Write, FileShare.Write);
            if (!file.CanWrite) return false;
            
            StreamWriter writer = new StreamWriter(file);

            writer.WriteLine("# Johnwhile engine Wavefront OBJ exporter");
            writer.WriteLine("# Created : " + DateTime.Now.ToString("dd-MM-yyyy"));
           
            FaceFormat format = FaceFormat.None;
            int repeat = 0;

            List<Vector3> vertices = current.vertices;
            if (vertices != null && vertices.Count > 0)
            {
                foreach (Vector3 v in vertices)
                    writer.WriteLine(string.Format("v  {0} {1} {2}", v.x, v.y, v.z));
                writer.WriteLine("# " + vertices.Count + " vertices");
                writer.WriteLine("");
                format |= FaceFormat.useVerts;
                repeat++;
            }
            List<Vector3> normals = current.normals;
            if (normals != null && normals.Count > 0)
            {
                foreach (Vector3 n in normals)
                    writer.WriteLine(string.Format("vn {0} {1} {2}", n.x, n.y, n.z));
                writer.WriteLine("# " + normals.Count + " normals");
                writer.WriteLine("");
                format |= FaceFormat.useNormals;
                repeat++;
            }
            List<Vector2> texcoords = current.texcoords;
            if (texcoords != null && texcoords.Count > 0)
            {
                foreach (Vector2 t in texcoords)
                    writer.WriteLine(string.Format("vt {0} {1} {2}", t.x, t.y, 1.0f));
                writer.WriteLine("# " + texcoords.Count + " texture coords");
                writer.WriteLine("");
                format |= FaceFormat.useTexCoord;
                repeat++;
            }

            writer.WriteLine("g " + groups[0].ToString());
            writer.WriteLine("s 1"); 

            switch (repeat)
            {
                case 1:
                    foreach (Face16 f in current.faces)
                        writer.Write(string.Format("f {0} {1} {2}", f.I, f.J, f.K));
                    break;
                case 2:
                    foreach (Face16 f in current.faces)
                        writer.Write(string.Format("f {0}/{1} {2}/{3} {4}/{5}", f.I, f.I, f.J, f.J, f.K, f.K));
                    break;
                case 3:
                    foreach (Face16 f in current.faces)
                        writer.Write(string.Format("f {0}/{1}/{2} {3}/{4}/{5} {6}/{7}/{8}", f.I, f.I, f.I, f.J, f.J, f.J, f.K, f.K, f.K));
                    break;
                default:
                    throw new NotImplementedException();
            }
            writer.WriteLine("# " + current.faces.Count + " faces");

            return true;
        }

        [Flags]
        enum FaceFormat
        {
            None = 0,
            useVerts = 1,
            useTexCoord = 2,
            useNormals = 4,
        }
    }
}
