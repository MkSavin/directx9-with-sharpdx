﻿#define DEBUG_TESTDELAY

using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;

using Engine.Tools;

namespace Engine
{
    // The intention of these class is to use one loop function that manage all functions 
    // used to render or update, a sort of program's clock. One instance prevent delay issue,
    // example for TimerLooper the delay of 15ms for each event are generated only once 


    /// <summary>
    /// Store some basic information about cycle
    /// </summary>
    public class LooperEventArgs : EventArgs
    {
        public float ElapsedMS { get; internal set; }
    }



    /// <summary>
    /// More accurate than TimerLoop, no cycle inteval limits.
    /// From blog: http://blogs.msdn.com/b/tmiller/archive/2005/05/05/415008.aspx
    /// From book: C# Game Programming: For Serious Game Creation. by Daniel Schuller
    /// From blog: http://gamedevelopment.tutsplus.com/tutorials/how-to-create-a-custom-2d-physics-engine-the-core-engine--gamedev-7493#timestepping
    /// </summary>
    public static class ApplicationLooper
    {
        static bool loopHandled;
        static bool reducecpu;
        static Message msg;
        static LooperEventArgs cycleinfo;
        static event EventHandler<LooperEventArgs> events;
        static int eventscount = 0;
        static double prevtime, currtime;
        static uint loops;

        public static readonly PreciseTimer Timer;

        public static Form windows;


        /// <summary>
        /// </summary>
        public static event EventHandler<LooperEventArgs> OnLooping
        {
            add
            {
                events += value;
                eventscount = events.GetInvocationList().Length;
                Start();
            }
            remove
            {
                events -= value;
                eventscount = events.GetInvocationList().Length;
                if (eventscount == 0) Stop();
            }
        }

        static ApplicationLooper()
        {
            Timer = new PreciseTimer();
            cycleinfo = new LooperEventArgs();
            loopHandled = false;
            reducecpu = true;
        }

        static void Start()
        {
            if (!loopHandled)
            {
                loopHandled = true;
                // not differences ?
                //Application.Idle += new EventHandler(OnApplicationEnterIdle);

                if (reducecpu) 
                    Application.Idle += OnApplicationEnterIdleReduced;
                else 
                    Application.Idle += OnApplicationEnterIdle;

                prevtime = Timer.MilliSecondsAccumulated;
                Timer.Start();
            }
        }

        static void Stop()
        {
            if (loopHandled)
            {
                // remove both for safety
                Application.Idle -= OnApplicationEnterIdleReduced;
                Application.Idle -= OnApplicationEnterIdle;
                loopHandled = false;
                Timer.Stop();
            }
        }



        static bool IsAppStillIdle()
        {
#if DEBUG
            try
            {
                return !PeekMessage(out msg, windows.Handle, 0, 0, 0);
            }
            catch (Exception exp)
            {
                Console.WriteLine("Fatal Error when call PeekMessage");
                Console.WriteLine(exp.ToString());
                return false;
            }
#else
            return !PeekMessage(out msg, IntPtr.Zero, 0, 0, 0);
#endif
        }




        /// <summary>
        /// The Application.Idle event fires once every time the application's message queue is emptied and
        /// the application is transitioning to an idle state.
        /// </summary>
        static void OnApplicationEnterIdle(object sender, EventArgs e)
        {
            Thread.Sleep(1);
            //Console.WriteLine("OnApplicationEnterIdle");
            while (IsAppStillIdle())
            {
                currtime = Timer.MilliSecondsAccumulated;
                cycleinfo.ElapsedMS = (float)(currtime - prevtime);
                prevtime = currtime;
                events(null, cycleinfo);
                unchecked { loops++; }
            }
        }
        /// <summary>
        /// </summary>
        static void OnApplicationEnterIdleReduced(object sender, EventArgs e)
        {
            Console.WriteLine("OnApplicationEnterIdle");

            while (IsAppStillIdle())
            {
                Thread.SpinWait(1000);
                currtime = Timer.MilliSecondsAccumulated;
                cycleinfo.ElapsedMS = (float)(currtime - prevtime);
                prevtime = currtime;
                events(null, cycleinfo);
                unchecked { loops++; }
                if (loops % 100 == 0) Thread.Sleep(1);
                //Application.DoEvents();
            }
        }
        [System.Security.SuppressUnmanagedCodeSecurity]
        [DllImport("User32.dll", CharSet = CharSet.Auto)]
        static extern bool PeekMessage(
            out Message msg,
            IntPtr hWnd,
            uint messageFilterMin,
            uint messageFilterMax,
            uint flags);
       
    }

    /// <summary>
    /// Using System.Winfows.Form.Timer to avoid cross-multithread error
    /// </summary>
    public static class FormLooper
    {
        static System.Windows.Forms.Timer looper;
        static bool loopstarted;
        static LooperEventArgs cycleinfo;
        static event EventHandler<LooperEventArgs> events;
        static int eventscount = 0;
        static double prevtime, currtime;
        static uint loops = 0;

        public static readonly PreciseTimer Timer;


        /// <summary>
        /// </summary>
        public static event EventHandler<LooperEventArgs> OnLooping
        {
            add
            {
                events += value;
                eventscount = events.GetInvocationList().Length;
                Start();
            }
            remove
            {
                events -= value;
                eventscount = events.GetInvocationList().Length;
                if (eventscount == 0) Stop();
            }
        }

        /// <summary>
        /// </summary>
        static FormLooper()
        {
            Timer = new PreciseTimer();
            cycleinfo = new LooperEventArgs();
            looper = new System.Windows.Forms.Timer();
            looper.Interval = 1;
            looper.Tick += cycle;
            loopstarted = false;
        }

       static void Start()
        {
            if (!loopstarted)
            {
                loopstarted = true;
                Timer.Start();
                prevtime = Timer.MilliSecondsAccumulated;
                looper.Start();
            }
        }

       static void Stop()
        {
            if (loopstarted)
            {
                loopstarted = false;
                looper.Stop();
                Timer.Stop();
            }
        }


       static void cycle(object state,EventArgs arg = null)
       {
           Console.WriteLine("Call Cycle");
           currtime = Timer.MilliSecondsAccumulated;
           cycleinfo.ElapsedMS = (float)(currtime - prevtime);
           prevtime = currtime;
           events(state, cycleinfo);
           unchecked { loops++; }
       }
    }


    /// <summary>
    /// Using System.Threading.Timer.
    /// Issue : Timer minimum interval between two cycle is 15ms for my machine
    /// </summary>
    public static class ThreadLooper
    {
        static System.Threading.Timer looper;
        static bool loopstarted;
        static LooperEventArgs cycleinfo;
        static event EventHandler<LooperEventArgs> events;
        static int eventscount = 0;
        static double prevtime, currtime;
        static uint loops = 0;

        public static readonly PreciseTimer Timer;

        public static Form owner;

        /// <summary>
        /// </summary>
        public static event EventHandler<LooperEventArgs> OnLooping
        {
            add
            {
                events += value;
                eventscount = events.GetInvocationList().Length;
                Start();
            }
            remove
            {
                events -= value;
                eventscount = events.GetInvocationList().Length;
                if (eventscount == 0) Stop();
            }
        }

        /// <summary>
        /// </summary>
        static ThreadLooper()
        {

            Timer = new PreciseTimer();
            cycleinfo = new LooperEventArgs();
            looper = new System.Threading.Timer(cycle, null, Timeout.Infinite, Timeout.Infinite);
            loopstarted = false;
        }

        static void Start()
        {
            if (!loopstarted)
            {
                loopstarted = true;
                Timer.Start();
                prevtime = Timer.MilliSecondsAccumulated;
                looper.Change(0, 1);
            }
        }

        static void Stop()
        {
            if (loopstarted)
            {
                loopstarted = false;
                looper.Change(Timeout.Infinite, Timeout.Infinite);
                Timer.Stop();
            }
        }


        static void cycle(object state)
        {
            Console.WriteLine("Call Cycle");
            currtime = Timer.MilliSecondsAccumulated;
            cycleinfo.ElapsedMS = (float)(currtime - prevtime);
            prevtime = currtime;

            //TODO : avoid cross-thread error
            owner.Invoke(events, state, cycleinfo);

            unchecked { loops++; }
        }

    }
}
