﻿#define DEBUG_TESTDELAY
//#define NEWS

using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using System.Threading;


namespace Engine.Tools
{
    public class MainLoop
    {
        public MainLoop(Form myForm)
        {
            // Hook the application's idle event
            System.Windows.Forms.Application.Idle += new EventHandler(OnApplicationIdle);
            System.Windows.Forms.Application.Run(myForm);
        }

        private void OnApplicationIdle(object sender, EventArgs e)
        {
            while (AppStillIdle)
            {
                // Render a frame during idle time (no messages are waiting)

            }
        }

        private bool AppStillIdle
        {
            get
            {
                Message msg;
                return !PeekMessage(out msg, IntPtr.Zero, 0, 0, 0);
            }
        }


        [System.Security.SuppressUnmanagedCodeSecurity] // We won't use this maliciously
        [DllImport("User32.dll", CharSet = CharSet.Auto)]
        public static extern bool PeekMessage(out Message msg, IntPtr hWnd, uint messageFilterMin, uint messageFilterMax, uint flags);
    }


    /// <summary>
    /// APPLICATION.IDLE crash after some time... ???
    /// 
    /// This game loop is designed to call all functions you want, each with its own algorithm
    /// </summary>
    public class GameLoop
    {
        // your own functions
        private int callbackCount = 0;
        private GameLoopFunction[] callbacks = new GameLoopFunction[10];
        private double[] coeficients = new double[10];
 
        double accumulated;
        double duration;
        double freetime;
        double tmp;
        bool somecalls;

        Form windows;

        /// <summary>
        /// NOT WORK
        /// call in an infinite loop the function passed as parameter in AddCallbackFunction()
        /// </summary>
        /// <param name="ReduceCpuUsage">insert a Thread.Sleep(1) for each cicle to reduce cpu usage</param>
        public GameLoop(Form MainWindows , bool ReduceCpuUsage = true)
        {
            accumulated = 0.0;
            duration = 0.0;
            Timer = ApplicationLooper.Timer;
            ApplicationLooper.windows = MainWindows;
            this.windows = MainWindows;
        }

        void ApplicationLooper_OnLooping(object sender, LooperEventArgs e)
        {
            LoopCycle(e.ElapsedMS);
        }

        public readonly ITimer Timer;

        /// <summary>
        /// Time in millisecond used by this function previosly.
        /// </summary>
        public float PrevDurationMS
        {
            get { return (float)duration; }
        }

        public void Start()
        {
            ApplicationLooper.OnLooping += ApplicationLooper_OnLooping;
            //ApplicationLoop.AddGameLoop(this);
        }
        public void Stop()
        {
            ApplicationLooper.OnLooping -= ApplicationLooper_OnLooping;
            //ApplicationLoop.RemoveGameLoop(this);
        }

        /// <summary>
        /// Is important also the order you use to insert new functions
        /// http://gafferongames.com/game-physics/fix-your-timestep/
        /// </summary>
        /// <param name="callback">the function that looping</param>
        /// <param name="mode">type of time step method</param>
        /// <param name="TimeStepMS">time used to trigger the callback, if zero will be use the brutal force method</param>
        public void AddLoopableFunction(GameLoopDelegate<GameLoopFunction> callback, TimeStepMode mode, float TimeStepMS = 0,
            GameLoopFakeFlags fakeDelay = GameLoopFakeFlags.None, float fakeDelayMS = 0)
        {
            GameLoopFunction function;
            switch (mode)
            {
                case TimeStepMode.LimitFrameRate: 
                    function = new LFRFunction(Timer, callback, TimeStepMS, fakeDelay, fakeDelayMS); 
                    break;
                case TimeStepMode.SemiFixedTimestep:
                    function = new SFTFunction(Timer, callback, TimeStepMS, fakeDelay, fakeDelayMS); 
                    break;
                case TimeStepMode.VariableTimestep:
                    function = new VTFunction(Timer, callback, TimeStepMS, fakeDelay, fakeDelayMS);
                    break;
                default:
                    function = new GameLoopFunction(Timer, callback, 0, fakeDelay, fakeDelayMS); 
                    break;
            }
            callbacks[callbackCount++] = function;
        }

        /// <summary>
        /// This is the Clicle called every time by global MainLoop. The elapsed time is the time used by MainLoop from each cycle,
        /// can be equal or greater than duration value
        /// </summary>
        public void LoopCycle(double elapsedMS)
        {
            //Console.Write("@");

            somecalls = false;
            tmp = Timer.MilliSecondsAccumulated;
            
            freetime = 0;
            for (int i = 0; i < callbackCount; i++)
            {
                callbacks[i].Update(elapsedMS);
                somecalls |= callbacks[i].callspercicle > 0;
                freetime = callbacks[i].duration / callbacks[i].targhet;
            }

            accumulated += elapsedMS;

            // update statistics every seconds
            if (accumulated > 1000)
            {
                // convert in seconds
                accumulated /= 1000.0;
                for (int i = 0; i < callbackCount; i++)
                    callbacks[i].UpdateStats(accumulated);
                accumulated = 0;
            }
            duration = Timer.MilliSecondsAccumulated - tmp;


            // update the accumulated retard and distribute to all functions proportionally
            // only if necessary
            if (duration > 0)
            {
                // if negative the gameloop are slower than necessary
                freetime = duration * (1 - freetime);

                for (int i = 0; i < callbackCount; i++)
                {
                    if (callbacks[i].callspercicle > 0)
                    {
                        callbacks[i].retardaccumulated += (freetime * callbacks[i].duration / duration) ;
                    }
                }
            }
            return;
            
            if (somecalls)
            {
                // tmp è il ritardo su millisecondo. se maggiore di uno allora è ritardo altrimenti è tempo guadagnato

                if (freetime < 0) Console.ForegroundColor = ConsoleColor.Red;
                else Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine(freetime.ToString("0.000"));
                Console.ResetColor();
            }
        }


        public override string ToString()
        {
            string str = "";

            for (int i = 0; i < callbackCount; i++)
            {
                str += " " + callbacks[i].ToString();
            }

            return str;
        }

    }

}
