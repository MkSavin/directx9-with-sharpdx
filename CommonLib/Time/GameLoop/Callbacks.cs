 using System;
 using System.Collections.Generic;
 using System.Text;
 using System.Threading;
 
 using Engine.Maths;
 
 namespace Engine.Tools
 { 
   /// <summary>
   /// http://gafferongames.com/game-physics/fix-your-timestep/
   /// </summary>
   public enum TimeStepMode
   {
       /// <summary>
       /// Match with loop cicle, the elapsed time depend only by time used by CPU to run callback function.
       /// It have a very intensive CPU usage, example for an empty function you can reach 1000 FPS.
       /// Can be used example for a performance test.
       /// </summary>
       Always = 0,
       /// <summary>
       /// Used for render frame loop, simply it don't run the callback if not enough time has elapsed,
       /// next frame the counter is reseted. I notive that using a time sleep 1ms maximum fps is 64.
       /// </summary>
       LimitFrameRate = 1,
       /// <summary>
       /// Used for deterministic physic loop, for every loop cicle the callback is called many time to ensure
       /// that each steps use a deltatime less or equal than setting time.
       /// </summary>
       SemiFixedTimestep = 2,
       /// <summary>
       /// If physic loop may take too long, is possible that it slow all other callbacks. To slow only this function
       /// a greater timestep is returned. This generate less physic loops but with a greater dt.
       /// </summary>
       VariableTimestep = 3
   }
 
   /// <summary>
   /// Game loop performance test, use some delay milliseconds to test how gameloop behaves 
   /// </summary>
   [Flags]
   public enum GameLoopFakeFlags
   {
       None = 0,
       Constant = 1,
       Noise = 2,
   }


   public interface ILoopable 
   { 
       void Update(double elapsedMS);
   }

   public interface ILoopFunction
   {
       /// <summary>
       /// Diagnostic value, number of function calls for each seconds.
       /// Is equal to FPS in a render callback
       /// </summary>
       float CallsPerSecond { get; }  
       /// <summary>
       /// Time in millisecond used by this function previosly.
       /// </summary>
       float PrevDurationMS { get; }
       /// <summary>
       /// Number of call for cicle, always 1 for a RenderFrame, &gt1 for a PhysicLoop. 
       /// It return how many time related callback is called before cicle call next callback function.
       /// </summary>
       /// </summary>
       int CallsPerCicle { get; }
   }
   
   
   public delegate void LoopDelegate(double elapsedMS);
   public delegate void GameLoopDelegate<T>(double elapsedMS, T sender);


   /// <summary>
   /// Classic brutal force loop, no delay, no trigger time, will be called every cicle
   /// </summary>
   public class GameLoopFunction : ILoopable , ILoopFunction
   {
       public double retardaccumulated = 0;

       static void VoidFunction(double elapsedMS, GameLoopFunction function)
       {
           // do nothing
       }
       protected TimeStepMode mode;
       //protected PreciseTimer timer;
       protected double tmp;
 
       /// <summary>
       /// The relative action to do when triggered
       /// </summary>
       protected GameLoopDelegate<GameLoopFunction> callback;
 
       // for test i use a fake time consuption of callback function
       protected float fakeDelayMs = 0;
       protected GameLoopFakeFlags fakeDelayFlag;       
       /// <summary>
       /// Trigger time in MS
       /// </summary>
       internal readonly double targhet;
       /// <summary>
       /// Accumulated free time in MS
       /// </summary>
       public double accumulated, accumulatelimit;
       /// <summary>
       /// Previous callback time comsuption
       /// </summary>
       internal double duration;    
       /// <summary>
       /// Number of calls for one game loop cicle (how many time is called before go to next callback function ?)
       /// </summary>
       internal int callspercicle;
       /// <summary>
       /// Number of calls for one second.
       /// </summary>
       internal float callspersecond;
       /// <summary>
       /// The reference of timer used by game loop
       /// </summary>
       public readonly ITimer Timer;


       /// <summary>
       /// </summary>
       public GameLoopFunction(ITimer Timer, GameLoopDelegate<GameLoopFunction> action, double triggertime,
           GameLoopFakeFlags fakeDelayFlag = GameLoopFakeFlags.None,
           float fakeDelayMs = 0)
       {
           this.callback = action != null ? action : VoidFunction;
           this.targhet = triggertime;
           this.accumulatelimit = triggertime * 10;
           this.fakeDelayFlag = fakeDelayFlag;
           this.fakeDelayMs = fakeDelayMs;
           this.Timer = Timer;
       }

       /// <summary>
       /// Diagnostic value, number of function calls for each seconds.
       /// Is equal to FPS in a render callback
       /// </summary>
       public float CallsPerSecond { get; internal set; }
       /// <summary>
       /// Time in millisecond used by this function previosly.
       /// </summary>
       public float PrevDurationMS
       {
           get { return (float)duration; }
       }
       /// <summary>
       /// Number of call for cicle, always 1 for a RenderFrame, &gt1 for a PhysicLoop. 
       /// It return how many time related callback is called before cicle call next callback function.
       /// </summary>
       /// </summary>
       public int CallsPerCicle
       {
           get { return callspercicle; }
       }
       /// <summary>
       /// After some defined time, update statistics, example after 1 second
       /// </summary>
       internal void UpdateStats(double seconds)
       {
           if (callspersecond > 0)
           {
               CallsPerSecond = (float)(callspersecond / seconds);
               callspersecond = 0;
           }
       }
 
       /// <summary>
       /// This function calculate if is the time to tiggers the callback function.
       /// </summary>
       public virtual void Update(double elapsedMS)
       {
           accumulated += elapsedMS;

           tmp = Timer.MilliSecondsAccumulated;
           callback(elapsedMS, this);
           DoCallbackFakeDelay();
           callspersecond++;
           callspercicle = 1;
           duration = Timer.MilliSecondsAccumulated - tmp;
       }
       /// <summary>
       /// Debug purpose, add some random time to simulate slow functions
       /// </summary>
       protected void DoCallbackFakeDelay()
       {
           if (fakeDelayFlag== GameLoopFakeFlags.None) return;
           double delay = 100000.0 / PreciseTimer.MS_For100000SpinIterations;
           delay *= fakeDelayFlag == GameLoopFakeFlags.Constant ? fakeDelayMs : MathUtils.GetRandomFloat(fakeDelayMs);
           Thread.SpinWait((int)delay);
       }
 
       public override string ToString()
       {
           return base.ToString();
       }
   }        
 
   /// <summary>
   /// LIMIT FRAME RATE
   /// Simplest callback function, it triggers only if the elapsed time is greater than that settled
   /// </summary>
   public class LFRFunction : GameLoopFunction
   {
       public LFRFunction(ITimer timer, GameLoopDelegate<GameLoopFunction> callback, double triggertimeMS,
           GameLoopFakeFlags fakeDelay = GameLoopFakeFlags.None, float fakeDelayMS = 0)
           : base( timer, callback, triggertimeMS, fakeDelay,fakeDelayMS)
       {
 
       }
 
       public override void Update(double elapsedMS)
       {
           accumulated += elapsedMS;
           callspercicle = 0;
           if (retardaccumulated < 0) retardaccumulated = 0;


           if (accumulated > targhet)
           {
               if (retardaccumulated > accumulatelimit) retardaccumulated = accumulatelimit;

               bool skeepframe = retardaccumulated > targhet ;

               if (!skeepframe)
               {
                   // truncate if too big
                   if (accumulated > accumulatelimit) accumulated = accumulatelimit;

                   //Console.WriteLine(string.Format("elapsedMS {0} accumulate {1}", elapsedMS, accumulate));
                   tmp = Timer.MilliSecondsAccumulated;
                   callback(targhet, this);
                   DoCallbackFakeDelay();
                   duration = Timer.MilliSecondsAccumulated - tmp;

                   callspersecond++;
                   callspercicle = 1;

                   // can't set accumulate=0, example if a cicle use 15ms and you set a dt 16ms
                   // need two cicle to do 30>16, and if then you reset accumulate, you will 
                   // need always 2 cicle. Need to keep some previuos time
                   //accumulate = accumulate % dt;
                   //Console.WriteLine(duration);

               }
               else
               {
                   //Console.WriteLine("KSEEP");
                   retardaccumulated -= targhet;
               }
               accumulated -= targhet;
               
           }
       }
 
       public override string ToString()
       {
           return base.ToString();
       }
   }
 
   /// <summary>
   /// SEMIFIXED TIMESTEP
   /// Ensure that each call of function return a fixed dt. If this function slow a lot, the result is 
   /// a slow FPS and always 10 calls per cicle
   /// </summary>
   public class SFTFunction : GameLoopFunction
   {
       public SFTFunction(ITimer timer, GameLoopDelegate<GameLoopFunction> callback, double triggertimeMS,
           GameLoopFakeFlags fakeDelay = GameLoopFakeFlags.None, float fakeDelayMS = 0)
           : base(timer, callback, triggertimeMS,fakeDelay, fakeDelayMS)
       {
       }

       public override void Update(double elapsedMS)
       {
           accumulated += elapsedMS;
           callspercicle = 0;

           if (accumulated > targhet)
           {
               tmp = Timer.MilliSecondsAccumulated;
 
               // fix "spiral-of-death" with a limit of maximum 10 updates per cicle
               if (accumulated > accumulatelimit) accumulated = accumulatelimit;

               callspercicle = (int)(accumulated / targhet);
 
               for (int step = 0; step < callspercicle; step++)
               {
                   callback(targhet, this);
                   DoCallbackFakeDelay();
                   callspersecond++;
                   accumulated -= targhet;
               }
               // notice that at the end of updates accumulate != 0, some fragment of time will be used for next cicle
               duration = (Timer.MilliSecondsAccumulated - tmp) / callspercicle;
           }
       }
   }
 
   /// <summary>
   /// VARIABLE TIMESTEP
   /// Ensure maximum 4 calls per cicle. If function require more time so more calls, the delta time will be
   /// reduced. The effect is a faster FPS but physic use less precision
   /// </summary>
   public class VTFunction : GameLoopFunction
   {
       /// <summary>
       /// maximum number of steps to do before call next callback function.
       /// This value don't affect directly the Updates Per Seconds, the optimal value is 1
       /// in order that next callbacks can be evaluated and tiggered if necessary
       /// </summary>
       const int MAXCOUNTPERCICLE = 1;

       public VTFunction(ITimer timer, GameLoopDelegate<GameLoopFunction> callback, double triggertimeMS,
           GameLoopFakeFlags fakeDelay = GameLoopFakeFlags.None, float fakeDelayMS = 0)
           : base( timer, callback, triggertimeMS,fakeDelay, fakeDelayMS)
       {
       }

       public override void Update(double elapsedMS)
       {
           accumulated += elapsedMS;
           callspercicle = 0;

           // measure the time used to run this callback    
           double dt_modified = targhet;
 
           if (accumulated > targhet)
           {
               tmp = Timer.MilliSecondsAccumulated;
 
               // fix "spiral-of-death" with a limit of maximum 10 updates per cicle
               if (accumulated > accumulatelimit) accumulated = accumulatelimit;
               callspercicle = (int)(accumulated / targhet);
 
               if (callspercicle > MAXCOUNTPERCICLE)
               {
                   dt_modified = accumulated / MAXCOUNTPERCICLE;
                   callspercicle = MAXCOUNTPERCICLE;
               }
 
               for (int step = 0; step < callspercicle; step++)
               {
                   callback(dt_modified, this);
                   DoCallbackFakeDelay();
                   callspersecond++;
                   accumulated -= dt_modified;
               }
               duration = Timer.MilliSecondsAccumulated - tmp;
           }
       }
   }
 
 }