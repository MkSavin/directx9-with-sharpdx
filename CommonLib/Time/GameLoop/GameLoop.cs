﻿using Engine.Tools;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace Engine.Tools
{
   /// <summary>
    /// From book: C# Game Programming: For Serious Game Creation. by Daniel Schuller
    /// and from http://gamedevelopment.tutsplus.com/tutorials/how-to-create-a-custom-2d-physics-engine-the-core-engine--gamedev-7493#timestepping
    /// </summary>
    public class GameLoopOld
    {
        PreciseTimer timer;
        GameLoopFunction[] callbacks = new GameLoopFunction[10];
        bool loopHandled;
        bool requiresleep;
        double duration, accumulated;
        double prev = 0;
        int callbackCount = 0;


        /// <summary>
        /// call in an infinite loop the function passed as parameter in AddCallbackFunction()
        /// </summary>
        /// <param name="MainWindows">will be used to syncronize events</param>
        public GameLoopOld()
            : this(null)
        {
        }
        /// <summary>
        /// call in an infinite loop the function passed as parameter in AddCallbackFunction()
        /// </summary>
        /// <param name="MainWindows">will be used to syncronize events</param>
        public GameLoopOld(Form MainWindows)
        {
            requiresleep = false;
            loopHandled = false;
            timer = new PreciseTimer();
            Timer = timer;
            accumulated = 0.0;
        }

        /// <summary>
        /// The unique timer used to calculate elapsed times
        /// </summary>
        public readonly ITimer Timer;
        /// <summary>
        /// Time in millisecond used by this function previosly.
        /// </summary>
        public float PrevDurationMS
        {
            get { return (float)duration; }
        }
        /// <summary>
        /// Start or Resume the looping. Fps , Ups and was not reseted.
        /// </summary>
        public void Start()
        {
            if (!loopHandled)
            {
                loopHandled = true;
                // not differences ?
                //Application.Idle += new EventHandler(OnApplicationEnterIdle);
                Application.Idle += OnApplicationEnterIdle;

                timer.Start();
                //Console.WriteLine("Start looping " + fps + " " + ups);

                // remember to precompute prev value before OnApplicationEnterIdle start, otherwise
                // elapsed time will be calculate with (timer.SecondsCounter - {last called}) and return the 
                // TOTAL time, from timer stoped to timer resumed + current elapsed time
                prev = timer.MilliSecondsAccumulated;
            }
        }

        /// <summary>
        /// Pause the looping.
        /// </summary>
        public void Stop()
        {
            if (loopHandled)
            {
                timer.Stop();
                Application.Idle -= OnApplicationEnterIdle;
                loopHandled = false;
                //Console.WriteLine("Stop looping " + fps + " " + ups);
            }
        }

        
        /// <summary>
        /// http://gafferongames.com/game-physics/fix-your-timestep/
        /// </summary>
        /// <param name="callback">the function that looping</param>
        /// <param name="mode">type of time step method</param>
        /// <param name="TimeStepMS">time used to trigger the callback, if zero will be use the brutal force method</param>
        public void AddLoopableFunction(GameLoopDelegate<GameLoopFunction> callback, TimeStepMode mode, float TimeStepMS = 0,
            GameLoopFakeFlags fakeDelay = GameLoopFakeFlags.None, float fakeDelayMS = 0)
        {
            GameLoopFunction function;
            switch (mode)
            {
                case TimeStepMode.LimitFrameRate: 
                    function = new LFRFunction(timer, callback, TimeStepMS, fakeDelay, fakeDelayMS); 
                    break;
                case TimeStepMode.SemiFixedTimestep:
                    function = new SFTFunction(timer, callback, TimeStepMS, fakeDelay, fakeDelayMS); 
                    break;
                case TimeStepMode.VariableTimestep:
                    function = new VTFunction(timer, callback, TimeStepMS, fakeDelay, fakeDelayMS);
                    break;
                default:
                    function = new GameLoopFunction(timer, callback, 0, fakeDelay, fakeDelayMS); 
                    break;
            }
            callbacks[callbackCount++] = function;
        }

        void OnApplicationEnterIdle(object sender, EventArgs e)
        {
            Console.WriteLine("OnApplicationEnterIdle");

            //try
            //{
                while (IsAppStillIdle())
                {
                    //if (requiresleep) System.Threading.Thread.SpinWait(10000000);
                    
                    System.Threading.Thread.SpinWait(10000);

                    double curr = timer.MilliSecondsAccumulated;
                    double elapsedMS = curr - prev;

                    prev = curr;

                    for (int i = 0; i < callbackCount; i++)
                    {
                        callbacks[i].Update(elapsedMS);
                    }


                    accumulated += elapsedMS;

                    // update statistics every seconds
                    if (accumulated > 1000)
                    {
                        // convert in seconds
                        accumulated /= 1000.0;
                        for (int i = 0; i < callbackCount; i++)
                            callbacks[i].UpdateStats(accumulated);
                        accumulated = 0;
                    }



                    duration = timer.MilliSecondsAccumulated - curr;
                }
            //}
            //catch (Exception exp)
            //{
            //    Console.WriteLine("ERRRRRRRRRRRRROOOOOOOOOOOOORRRRRRR");
            //    Console.WriteLine(exp.ToString());
            //}

        }

        bool IsAppStillIdle()
        {
            try
            {
                Message msg;
                return !PeekMessage(out msg, IntPtr.Zero, 0, 0, 0);
            }
            catch (Exception exp)
            {
                Console.WriteLine("Fatal Error when call PeekMessage");
                Console.WriteLine(exp.ToString());
                return false;
            }
        }

        [System.Security.SuppressUnmanagedCodeSecurity]
        [DllImport("User32.dll", CharSet = CharSet.Auto)]
        public static extern bool PeekMessage(
            out Message msg,
            IntPtr hWnd,
            uint messageFilterMin,
            uint messageFilterMax,
            uint flags);


        public override string ToString()
        {
            string str = "";

            for (int i = 0; i < callbackCount; i++)
            {
                str += " " + callbacks[i].ToString();
            }

            return str;
        }

    }
}
