﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;

namespace Engine.Tools
{
    public interface ITimer
    {
        long TicksAccumulated { get; }
        double SecondsAccumulated { get; }
        double MilliSecondsAccumulated { get; }
    }


    /// <summary>
    /// Use high precise timer. Is a "reader" of computer clock
    /// i take a look from : C# Game Programming: For Serious Game Creation
    /// </summary>
    public class PreciseTimer : ITimer
    {
        static TimeSpan ms1 = new TimeSpan(0, 0, 0, 0, 1);
        static long freq;
        static double freqS, freqMS;
        
        internal long beginTicks, currentTicks, accumulateTicks;    
        bool pause, stop;

        /// <summary>
        /// These value are relative to your device
        /// </summary>
        static PreciseTimer()
        {
            if (QueryPerformanceFrequency(out freq))
            {
                freqS = freq;
                freqMS = freqS / 1000.0;

                long t0, t1;

                //SpinWait measure, is about 0,3 ms
                QueryPerformanceCounter(out t0);
                Thread.SpinWait(100000);
                QueryPerformanceCounter(out t1);
                PreciseTimer.MS_For100000SpinIterations = (t1 - t0) / freqMS;

                //Sleep measure, is about 15ms
                QueryPerformanceCounter(out t0);
                for (int i = 0; i < 10; i++) Thread.Sleep(ms1);
                QueryPerformanceCounter(out t1);
                PreciseTimer.MS_SleepCost = (t1 - t0) / freqMS / 10;
            }
            else
            {
                freq = -1;
            }

        }
        /// <summary>
        /// Can't work if high-performance counter aren't supported
        /// </summary>
        public PreciseTimer()
        {
            if (freq < 0) throw new Win32Exception("high-performance counter not supported");
            accumulateTicks = currentTicks = beginTicks = 0;
            pause = true;
            stop = true;

        }
        
        /// <summary>
        /// Thread.SpinWait() iterations doesn't have relations with time, depend by your machine
        /// </summary>
        public static double MS_For100000SpinIterations { get; private set; }
        /// <summary>
        /// Thread.Sleep() require about 15ms on my pc, so don't work for smaller values
        /// </summary>
        public static double MS_SleepCost { get; private set; }
        /// <summary>
        /// Return the number of total ticks accumulated from class initialization
        /// </summary>
        public long TicksAccumulated
        {
            get
            {
                if (!pause) updateAccumulation();
                return accumulateTicks;
            }
        }
        /// <summary>
        ///  Return the number of total seconds accumulated from class initialization
        /// </summary>
        public double SecondsAccumulated 
        { 
            get { return TicksAccumulated / freqS; } 
        }
        /// <summary>
        ///  Return the number of total seconds accumulated from class initialization
        /// </summary>
        public double MilliSecondsAccumulated
        {
            get { return TicksAccumulated / freqMS; }
        }
        /// <summary>
        /// Return Ticks Per Seconds of your device
        /// </summary>
        public static long Frequence
        {
            get { return freq; }
        }
        /// <summary>
        /// Start or Resume the time counter, the TicksCounter return the total accumulated time
        /// </summary>
        public void Start()
        {
            if (stop) accumulateTicks = 0;
            if (pause) QueryPerformanceCounter(out beginTicks);
            pause = false;
            stop = false;
        }
        /// <summary>
        /// Pause the time counter, the TicksCounter are frize
        /// </summary>
        public void Pause()
        {
            if (!pause) updateAccumulation();
            pause = true;
        }
        /// <summary>
        /// Stop the time counter, the next Start will reaset the accumulated ticks
        /// </summary>
        public void Stop()
        {
            if (!stop) updateAccumulation();
            stop = pause = true;
        }

        /// <summary>
        /// Store the accumulation done until now, then reset start-end point
        /// </summary>
        void updateAccumulation()
        {
            QueryPerformanceCounter(out currentTicks);
            unchecked { accumulateTicks += currentTicks - beginTicks; }
            beginTicks = currentTicks;
        }

        [DllImport("Kernel32.dll")]
        private static extern bool QueryPerformanceCounter(out long lpPerformanceCount);

        [DllImport("Kernel32.dll")]
        private static extern bool QueryPerformanceFrequency(out long lpFrequency);


        public override string ToString()
        {
            return string.Format("Time: {0} freq: {1}", new TimeSpan(TicksAccumulated), freq);
        }
    }
}
