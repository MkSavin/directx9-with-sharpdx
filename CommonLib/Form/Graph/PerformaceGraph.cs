﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

using Engine.Maths;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.ComponentModel;

namespace Engine.Graph
{
    /// <summary>
    /// A line graph to show a value in the time
    /// </summary>
    public class PerformaceGraph : UserControl
    {
        bool isInDesignMode = false;


        Random rnd = new Random();
        int MaxPoints;
        bool PlotterConstX = true;
        public bool useAbsoluteMinMax = true;
        public bool useDoubleBuffer = true;
        public SmoothingMode smoothing = SmoothingMode.None;

        SerieCollector plotter;

        DoubleBuffered memGraphics;
        Bitmap TmpBackBuffer;

        Font font;
        int drawcount = 0;
        bool needClear = true;
        bool candraw = true;
        bool mousehold = false;

        Brush background;
        LinearGradientBrush gradientbackground;

        public ISerieCollector Plotter { get { return plotter; } }


        #region Constructors

        public PerformaceGraph()
            : this(false)
        {
        }

        public PerformaceGraph(bool PlotterConstX)
        {

            this.isInDesignMode = FormUtils.IsInDesignMode(this);

            this.PlotterConstX = PlotterConstX;

            font = new Font("Currier New", 8);
            background = new SolidBrush(BackColor);

            if (isInDesignMode)
            {
                useDoubleBuffer = false;
                plotter = new SerieCollectorConstX(Name);

                SerieLine first = plotter.AddSerie("FIRST", 50);
                SerieLine second = plotter.AddSerie("SECOND", 50);
            }
            else
            {
                if (PlotterConstX)
                    plotter = new SerieCollectorConstX(Name);
                else
                    plotter = new SerieCollectorXY(Name);
            } 
            
            Title = "MyPlotterControl";
        }


        /// <summary>
        /// Name of plotter control
        /// </summary>
        public string Title
        {
            get { return plotter.Title; }
            set { plotter.Title = value; }
        }


        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            updateSize();
            memGraphics = new DoubleBuffered(ClientSize);
            needClear = true;
        }
        #endregion


        void updateSize()
        {
            gradientbackground = new LinearGradientBrush(new Point(0, 0), new Point(0, ClientSize.Height), Color.White, Color.LightBlue);
        }

        public void PaintBackground(Graphics graphic, Rectangle client)
        {
            needClear = false;
            //graphic.Clear(Color.White);
            graphic.FillRectangle(gradientbackground, this.ClientRectangle);
        }


        public void PaintControl(Graphics graphic, Rectangle client)
        {
            plotter.Draw(graphic, client);
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);
            TmpBackBuffer = memGraphics.BackBuffer;
            mousehold = true;
        }
        protected override void OnMouseUp(MouseEventArgs e)
        {
 	         base.OnMouseUp(e);
             mousehold = false;
        }
        

        protected override void OnPaintBackground(PaintEventArgs e)
        {
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            if (!candraw) return;

            Rectangle client = ClientRectangle;
            client.X += 5;
            client.Y += 5;
            client.Width -= 10;
            client.Height -= 10;

            if (isInDesignMode)
            {
                Graphics CurGraphics = e.Graphics;
                drawcount++;

                int serie = MathUtils.GetRandomInt(0, 2);
                if (serie > 1) serie = 1;
                plotter.AddValue(serie, MathUtils.GetRandomFloat() * 100.0f);
 
                PaintBackground(CurGraphics, client);
                PaintControl(CurGraphics, client);
            }
            else
            {
                if (ParentForm != null)
                {
                    if (mousehold)
                    {
                        e.Graphics.DrawImage(TmpBackBuffer, 0, 0);
                    }
                    else
                    {
                        Graphics CurGraphics = useDoubleBuffer ? memGraphics.BufferGraphics : e.Graphics;

                        CurGraphics.SmoothingMode = smoothing;

                        PaintBackground(CurGraphics, client);
                        PaintControl(CurGraphics, client);

                        if (useDoubleBuffer)
                        {
                            memGraphics.Render(e.Graphics);
                        }
                    }
                }
                else
                {
                    PaintBackground(e.Graphics, client);
                }
            }
            base.OnPaint(e);
        }

        protected override void OnResize(EventArgs e)
        {
            if (memGraphics != null)
            {
                if (ClientSize.Width<=0 || ClientSize.Height<=0)
                {
                    candraw = false;
                }
                else
                {
                    candraw = true;
                    memGraphics.Resize(ClientSize);
                    updateSize();
                    needClear = true;
                    Invalidate();
                }
            }
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // PerformaceGraph
            // 
            this.Name = "PerformaceGraph";
            this.ResumeLayout(false);
        }

    }
}
