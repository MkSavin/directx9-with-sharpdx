﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.ComponentModel;

using Engine.Maths;

namespace Engine
{
    public static class FormUtils
    {
        /// <summary>
        /// http://stackoverflow.com/questions/336817/how-can-i-detect-whether-a-user-control-is-running-in-the-ide-in-debug-mode-or
        /// </summary>
        public static bool IsInDesignMode(IComponent component)
        {
            return LicenseManager.UsageMode == LicenseUsageMode.Designtime;
            /*
            bool ret = false;
            if (null != component)
            {
                ISite site = component.Site;
                if (null != site)
                {
                    ret = site.DesignMode;
                }
                else if (component is System.Windows.Forms.Control)
                {
                    IComponent parent = ((System.Windows.Forms.Control)component).Parent;
                    ret = IsInDesignMode(parent);
                }
            }
            return ret;
            */
        }
    }

    /// <summary>
    /// Utilities for System.Drawing
    /// </summary>
    public static class FormDrawingUtils
    {
        /// <summary>
        /// Transforms your coordinate point to screen coordinate.
        /// </summary>
        /// <param name="point">The point in "bound" coordinate</param>
        /// <param name="screen">The screen area.</param>
        /// <param name="bound">The bound rectangle that fill the screen area, it use Min and Max so best performance for RectangleAA</param>
        /// <remarks>
        /// Rectangle use TopLeft origin, bound use BottomLeft origin
        /// </remarks>
        /// <returns> non additional operation if point exit from screen</returns>
        public static Point TransformCoordinateToScreen(Vector2 vector, Rectangle screen, IRectangleAA bound)
        {
            Vector2 min = bound.Min;
            Vector2 max = bound.Max;
            float x = screen.X + (vector.x - min.x) / (max.x - min.x) * screen.Width;
            float y = screen.Y + (max.y - vector.y) / (max.y - min.y) * screen.Height;
            return new Point((int)x, (int)y);
        }
        /// <summary>
        /// Transforms the screen coordinate to your coordinate system
        /// </summary>
        /// <param name="point">The point in "screen" coordinate</param>
        /// <param name="screen">The screen area.</param>
        /// <param name="bound">The bound rectangle that fill the screen area, it use Min and Max so best performance for RectangleAA</param>
        /// <remarks>
        /// Rectangle use TopLeft origin, bound use BottomLeft origin
        /// </remarks>
        /// <returns> non additional operation if point exit from screen</returns>
        public static Vector2 TransformScreenToCoordinate(Point point, Rectangle screen, IRectangleAA bound)
        {
            Vector2 min = bound.Min;
            Vector2 max = bound.Max;
            float x = min.x + (point.X - screen.X) * (max.x - min.x) / screen.Width;
            float y = max.y + (screen.Y - point.Y) * (max.y - min.y) / screen.Height;
            return new Vector2(x, y);
        }

        /// <summary>
        /// Build the useful 2D transformation matrix to draw with Graphics.
        /// For semplicity is not rotated and not sheared.
        /// http://source.winehq.org/source/dlls/gdiplus/matrix.c
        /// </summary>
        /// <param name="MinCoord">min corner of your coordinate system</param>
        /// <param name="MaxCoord">max corner of your coordinate system</param>
        /// <param name="GraphicRect">Graphic rectangle where your coordinate fill</param>
        /// <param name="CoordToScreen">The result passed as reference to improve performace, is a TRS matrix so Scale-Rotation-Traslation order</param>
        /// <param name="ScaleMatrix">The invers of CoordToScreen scale part, is necessary to draw correctly text or line thickness</param>
        public static void MakeScreenTransform(ref Vector2 MinCoord, ref Vector2 MaxCoord, ref Rectangle GraphicRect,
            Matrix CoordToScreen, 
            Matrix ScaleMatrix)
        {
            // the custom graph coordinate bound, where Y will be inverted to match with windows coordinate system
            float CoordX = MinCoord.x;
            float CoordY = MaxCoord.y;
            float Width = MaxCoord.x - MinCoord.x;
            float Height = MinCoord.y - MaxCoord.y;

            // the three screen coorners, notice that matrix will be not Sheared because is a rectangle
            float TopLeftX = GraphicRect.X;
            float TopLeftY = GraphicRect.Y;

            float TopRightX = GraphicRect.X + GraphicRect.Width;
            float TopRightY = GraphicRect.Y;

            float BottomLeftX = GraphicRect.X;
            float BottomLeftY = GraphicRect.Y + GraphicRect.Height;

            // the exatly math used to build NET matrix
            float m11 = (TopRightX - TopLeftX) / Width;
            float m21 = (BottomLeftX - TopLeftX) / Height;
            float dx = TopLeftX - m11 * CoordX - m21 * CoordY;
            float m12 = (TopRightY - TopLeftY) / Width;
            float m22 = (BottomLeftY - TopLeftY) / Height;
            float dy = TopLeftY - m12 * CoordX - m22 * CoordY;

            // force use of Append order because i'm not sure are the default parameter.
            CoordToScreen.Reset();
            CoordToScreen.Shear(m12, m21, MatrixOrder.Append);
            CoordToScreen.Scale(m11, m22, MatrixOrder.Append);
            CoordToScreen.Translate(dx, dy, MatrixOrder.Append);

            ScaleMatrix.Reset();
            ScaleMatrix.Scale(1.0f / m11, 1.0f/ m22, MatrixOrder.Append);
        }

        /// <summary>
        /// Matrix x Vector multiplication
        /// </summary>
        public static void TransformPoint(Matrix matrix, float x, float y, out float X, out float Y)
        {
            X = x * matrix.Elements[0] + y * matrix.Elements[2] + matrix.Elements[4];
            Y = x * matrix.Elements[1] + y * matrix.Elements[3] + matrix.Elements[5];
        }
    }
}
