﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;

namespace Engine
{
    /// <summary>
    /// Using this Form you have some usefull event
    /// https://code.google.com/r/benjaminaautin-justadownload/source/browse/Source/SharpDX/Windows/RenderForm.cs?r=5de4dd66532b0012e6c3c05d7753bcf5ad7758f6
    /// </summary>
    public class RenderForm : System.Windows.Forms.Form
    {
         const int WM_SIZE = 0x0005;
         const int SIZE_RESTORED = 0;
         const int SIZE_MINIMIZED = 1;
         const int SIZE_MAXIMIZED = 2;
         const int SIZE_MAXSHOW = 3;
         const int SIZE_MAXHIDE = 4;
         const int WM_ACTIVATEAPP = 0x001C;
         const int WM_POWERBROADCAST = 0x0218;
         const int WM_MENUCHAR = 0x0120;
         const int WM_SYSCOMMAND = 0x0112;
         const uint PBT_APMRESUMESUSPEND = 7;
         const uint PBT_APMQUERYSUSPEND = 0;
         const int SC_MONITORPOWER = 0xF170;
         const int SC_SCREENSAVE = 0xF140;
         const int MNC_CLOSE = 1;
         Size cachedSize;
         bool minimized;
        // DisplayMonitor monitor;
         bool sizeMove;

         Font fontForDesignMode;

        /// <summary>
        /// Initializes a new instance of the <see cref="RenderForm"/> class.
        /// </summary>
         public RenderForm()
             : this("SharpDX")
         {
         }

        /// <summary>
        /// Initializes a new instance of the <see cref="RenderForm"/> class.
        /// </summary>
        /// <param name="text">The text.</param>
        public RenderForm(String text)
        {
            Construct(text);
            fontForDesignMode = new Font("Calibri", 12, FontStyle.Bold);
        }

        /// <summary>
        /// Occurs when [app activated].
        /// </summary>
        public event EventHandler<EventArgs> AppActivated;

        /// <summary>
        /// Occurs when [app deactivated].
        /// </summary>
        public event EventHandler<EventArgs> AppDeactivated;

        /// <summary>
        /// Occurs when [monitor changed].
        /// </summary>
        public event EventHandler<EventArgs> MonitorChanged;

        /// <summary>
        /// Occurs when [pause rendering].
        /// </summary>
        public event EventHandler<EventArgs> PauseRendering;

        /// <summary>
        /// Occurs when [resume rendering].
        /// </summary>
        public event EventHandler<EventArgs> ResumeRendering;

        /// <summary>
        /// Occurs when [screensaver].
        /// </summary>
        public event EventHandler<CancelEventArgs> Screensaver;

        /// <summary>
        /// Occurs when [system resume].
        /// </summary>
        public event EventHandler<EventArgs> SystemResume;

        /// <summary>
        /// Occurs when [system suspend].
        /// </summary>
        public event EventHandler<EventArgs> SystemSuspend;

        /// <summary>
        /// Occurs when [user resized].
        /// </summary>
        public event EventHandler<EventArgs> UserResized;

        private void Construct(String text)
        {
                Text = text;
                ClientSize = new System.Drawing.Size(800, 600);
                MinimumSize = new System.Drawing.Size(200, 200);

                ResizeRedraw = true;
                SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint, true);
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Form.ResizeBegin"/> event.
        /// </summary>
        /// <param name="e">A <see cref="T:System.EventArgs"/> that contains the event data.</param>
        protected override void OnResizeBegin(EventArgs e)
        {
            base.OnResizeBegin(e);

            sizeMove = true;
            cachedSize = Size;
            OnPauseRendering(e);
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Form.ResizeEnd"/> event.
        /// </summary>
        /// <param name="e">A <see cref="T:System.EventArgs"/> that contains the event data.</param>
        protected override void OnResizeEnd(EventArgs e)
        {
            base.OnResizeEnd(e);

            OnUserResized(e);
            UpdateScreen();

            sizeMove = false;
            OnResumeRendering(e);
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Form.Load"/> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"/> that contains the event data.</param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            cachedSize = Size;
            UpdateScreen();
        }

        /// <summary>
        /// Paints the background of the control.
        /// </summary>
        /// <param name="e">A <see cref="T:System.Windows.Forms.PaintEventArgs"/> that contains the event data.</param>
        protected override void OnPaintBackground(PaintEventArgs e)
        {
            return;
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            e.Graphics.Clear(BackColor);
            if (DesignMode)
            {
                
                string text = "SharpDX9 RenderForm";
                var sizeText = e.Graphics.MeasureString(text, fontForDesignMode);
                e.Graphics.DrawString(text, fontForDesignMode, new SolidBrush(Color.Black), (Width - sizeText.Width) / 2, (Height - sizeText.Height) / 2);
            }
        }

        /// <summary>
        /// Raises the Pause Rendering event.
        /// </summary>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void OnPauseRendering(EventArgs e)
        {
            if (PauseRendering != null)
                PauseRendering(this, e);
        }

        /// <summary>
        /// Raises the Resume Rendering event.
        /// </summary>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void OnResumeRendering(EventArgs e)
        {
            if (ResumeRendering != null)
                ResumeRendering(this, e);
        }

        /// <summary>
        /// Raises the User resized event.
        /// </summary>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void OnUserResized(EventArgs e)
        {
            if (UserResized != null)
                UserResized(this, e);
        }

        private void OnMonitorChanged(EventArgs e)
        {
            if (MonitorChanged != null)
                MonitorChanged(this, e);
        }

        /// <summary>
        /// Raises the On App Activated event.
        /// </summary>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void OnAppActivated(EventArgs e)
        {
            if (AppActivated != null)
                AppActivated(this, e);
        }

        /// <summary>
        /// Raises the App Deactivated event
        /// </summary>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void OnAppDeactivated(EventArgs e)
        {
            if (AppDeactivated != null)
                AppDeactivated(this, e);
        }

        /// <summary>
        /// Raises the System Suspend event
        /// </summary>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void OnSystemSuspend(EventArgs e)
        {
            if (SystemSuspend != null)
                SystemSuspend(this, e);
        }

        /// <summary>
        /// Raises the System Resume event
        /// </summary>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void OnSystemResume(EventArgs e)
        {
            if (SystemResume != null)
                SystemResume(this, e);
        }

        /// <summary>
        /// Raises the <see cref="E:Screensaver"/> event.
        /// </summary>
        /// <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
        private void OnScreensaver(CancelEventArgs e)
        {
            if (Screensaver != null)
                Screensaver(this, e);
        }

        /// <summary>
        /// Override windows message loop handling.
        /// </summary>
        /// <param name="m">The Windows <see cref="T:System.Windows.Forms.Message"/> to process.</param>
        protected override void WndProc(ref Message m)
        {
            long wparam = 0;
            try
            {
              wparam = m.WParam.ToInt64();

            }
            catch (Exception e)
            {
                Console.WriteLine("*** ERROR ***\n" + e.ToString());
                Console.ReadKey();
            }


            switch (m.Msg)
            {
                case WM_SIZE:
                    if (wparam == SIZE_MINIMIZED)
                    {
                        minimized = true;
                        //maximized = false;
                        OnPauseRendering(EventArgs.Empty);
                    }
                    else
                    {
                        Rectangle rect = this.ClientRectangle;
                        //Win32Native.GetClientRect(m.HWnd, out rect);
                        if (rect.Bottom - rect.Top == 0)
                        {
                            // Rapidly clicking the task bar to minimize and restore a window
                            // can cause a WM_SIZE message with SIZE_RESTORED when 
                            // the window has actually become minimized due to rapid change
                            // so just ignore this message
                        }
                        else if (wparam == SIZE_MAXIMIZED)
                        {
                            if (minimized)
                                OnResumeRendering(EventArgs.Empty);

                            minimized = false;
                            // maximized = true;

                            OnUserResized(EventArgs.Empty);
                            UpdateScreen();
                        }
                        else if (wparam == SIZE_RESTORED)
                        {
                            if (minimized)
                                OnResumeRendering(EventArgs.Empty);

                            minimized = false;
                            // maximized = false;

                            if (!sizeMove && Size != cachedSize)
                            {
                                OnUserResized(EventArgs.Empty);
                                UpdateScreen();
                                cachedSize = Size;
                            }
                        }
                    }
                    break;
                case WM_ACTIVATEAPP:
                    if (wparam != 0)
                        OnAppActivated(EventArgs.Empty);
                    else
                        OnAppDeactivated(EventArgs.Empty);
                    break;
                case WM_POWERBROADCAST:
                    if (wparam == PBT_APMQUERYSUSPEND)
                    {
                        OnSystemSuspend(EventArgs.Empty);
                        m.Result = new IntPtr(1);
                        return;
                    }
                    else if (wparam == PBT_APMRESUMESUSPEND)
                    {
                        OnSystemResume(EventArgs.Empty);
                        m.Result = new IntPtr(1);
                        return;
                    }
                    break;
                case WM_MENUCHAR:
                    m.Result = new IntPtr(MNC_CLOSE << 16); // IntPtr(MAKELRESULT(0, MNC_CLOSE));
                    return;
                case WM_SYSCOMMAND:
                    wparam &= 0xFFF0;
                    if (wparam == SC_MONITORPOWER || wparam == SC_SCREENSAVE)
                    {
                        var e = new CancelEventArgs();
                        OnScreensaver(e);
                        if (e.Cancel)
                        {
                            m.Result = IntPtr.Zero;
                            return;
                        }
                    }
                    break;
            }

            base.WndProc(ref m);
        }

        /// <summary>
        /// Updates the screen.
        /// </summary>
        private void UpdateScreen()
        {
            //DisplayMonitor current = DisplayMonitor.FromWindow(Handle);
            //if (monitor != null && monitor->DeviceName != current->DeviceName)
            //{
            //    monitor = current;
            //    OnMonitorChanged(System.EventArgs.Empty);
            //}

            //monitor = current;
        }

        protected virtual void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // RenderForm
            // 
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Name = "RenderForm";
            this.ResumeLayout(false);
        }
    }
}
