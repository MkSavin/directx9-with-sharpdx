﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Diagnostics;

using Engine.Maths;
using Engine.Tools;

namespace Engine
{
    /// <summary>
    /// Basic decriptor of a singular element used in the buffer. Can't store a type
    /// because vertices can be made using a custom format, without using a specified struct type
    /// </summary>
    public abstract class BufferLayout
    {
        /// <summary>
        /// The type of struct used to build buffer. In VertexBuffer can be null if buffer are made
        /// using only VertexElements
        /// </summary>
        public Type type { get; protected set; }
        /// <summary>
        /// Size in bytes of buffer element struct
        /// </summary>
        public int bytesize { get; protected set; }
    }

    /// <summary>
    /// Descriptor of vertex structure used in a vertex buffer, are composed by a list of <seealso cref="VertexElement"/>
    /// </summary>
    /// <remarks>VertexElement.END will be never used, only when generate a VertexDeclaration class will be add at the end of list</remarks>
    public class VertexLayout : BufferLayout
    {
        int m_streamCount = 0;
        /// <summary>
        /// vertex elements list are in correct order, VertexDeclarationEnd are omited
        /// </summary>
        public List<VertexElement> Elements { get; private set; }

        // Because DeclarationUsage can be used only once, are unique for all attribute.
        // TODO : check if these is true also when implement geometric instance of shader program
        Dictionary<string, int> dictionary;


        public VertexLayout()
        {
            this.Elements = new List<VertexElement>();
            this.dictionary = new Dictionary<string, int>();
            this.type = null;
        }


        /// <summary>
        /// Multi stream constuction, work in progress so support 2 stream at this moment
        /// </summary>
        /// <param name="elementsStream0"></param>
        /// <param name="elementsStream1"></param>
        public VertexLayout(IList<VertexElement> elementsStream0, IList<VertexElement> elementsStream1)
            : this()
        {
            foreach (VertexElement elm in elementsStream0)
            {
                VertexElement element = elm;
                if (element.type != DeclarationType.Unused)
                {
                    if (element.stream != 0) throw new ArgumentException("requre all stream 0 value");
                    element.stream = 0;
                    Elements.Add(element);
                }
            }

            foreach (VertexElement elm in elementsStream1)
            {
                VertexElement element = elm;
                if (element.type != DeclarationType.Unused)
                {
                    if (element.stream != 0) throw new ArgumentException("requre all stream 0 value");
                    element.stream = 1;
                    Elements.Add(element);
                }
            }

            Elements.Sort(ElementComparer);

            for (int i = 0; i < Elements.Count; i++)
            {
                VertexElement elm = Elements[i];
                dictionary.Add(elm.SemanticName, i);
                bytesize += elm.bytesize;
            }

            m_streamCount = 2;

            if (type != null && Marshal.SizeOf(type) != bytesize)
                throw new ArgumentException("type " + type.Name + " don't have a correct size");
        }

        /// <summary>
        /// type can be null
        /// </summary>
        public VertexLayout(IList<VertexElement> elements, Type type = null)
            : this()
        {
            this.type = type;

            foreach (VertexElement elm in elements)
                if (elm.type != DeclarationType.Unused)
                    Elements.Add(elm);

            Elements.Sort(ElementComparer);

            for (int i = 0; i < Elements.Count; i++)
            {
                VertexElement elm = Elements[i];
                dictionary.Add(elm.SemanticName, i);
                bytesize += elm.bytesize;
                if (elm.stream > m_streamCount - 1) m_streamCount++;
            }

            if (type != null && Marshal.SizeOf(type) != bytesize)
                throw new ArgumentException("type " + type.Name + " don't have a correct size");
        }


        public void Add(int streamID, DeclarationType type , DeclarationUsage usage, int usageIndex = 0)
        {
            VertexElement elm = new VertexElement(streamID, bytesize, type, DeclarationMethod.Default, usage, usageIndex);
            dictionary.Add(elm.SemanticName, Elements.Count);
            bytesize += elm.bytesize;
            Elements.Add(elm);
        }
        
        
        /// <summary>
        /// A good idea is sort by stream and by offset
        /// </summary>
        static int ElementComparer(VertexElement x, VertexElement y)
        {
            if (x.stream > y.stream) return 1;
            else if (x.stream < y.stream) return -1;
            else if (x.offset > y.offset) return 1;
            else if (x.offset < y.offset) return -1;
            else return 0;
        }

        /// <summary>
        /// Return the elements data using the unique Pair "Usage-UsageIndex" values, if not found return false
        /// </summary>
        public bool GetElement(DeclarationUsage usage, int usageidx, out VertexElement element)
        {
            string semantic = VertexElement.GetSemanticName(usage, usageidx);
            if (dictionary.ContainsKey(semantic))
            {
                element = Elements[dictionary[semantic]];
                return true;
            }
            else
            {
                element = VertexElement.VertexDeclarationEnd;
                return false;
            }
        }

        /// <summary>
        /// Get the number of stream used by 
        /// </summary>
        public int StreamCount { get { return m_streamCount; } }

        public override string ToString()
        {
            StringBuilder str = new StringBuilder();
            str.Append(string.Format("Size: {0}b, Format: ", bytesize));
            foreach (VertexElement elm in Elements)
                str.Append(elm.usage.ToString() + ",");
            return str.ToString();
        }
    }

    /// <summary>
    /// Type of indices, contain information about format, usefull to auto-conversion in write-to-stream process
    /// </summary>
    public class IndexLayout : BufferLayout
    {
        /// <summary>
        /// Supported format are 8,16 or 32, Face and Edge are only an extension
        /// </summary>
        public int Bits { get; private set; }
        /// <summary>
        /// Directx9 use only 16 or 32 bit indices, byte indices only in OpenGl
        /// </summary>
        public bool is32Bit { get; private set; }
        /// <summary>
        /// counter of indices in the Index Struct, to use Face and Edge extension
        /// </summary>
        public int numOfIndis { get; private set; }
        /// <summary>
        /// Index will be get only from manager
        /// </summary>
        private IndexLayout() { }

        /// <summary>
        /// Return Null if not found in the database
        /// </summary>
        public static IndexLayout GetIndexFormat(Type type)
        {
            return manager.GetIndexFormat(type);
        }
        /// <summary>
        /// Not Supported by Directx9, used only when you want write a byte[] indices into index stream create with
        /// all other format
        /// </summary>
        public static IndexLayout One8 { get { return manager.GetIndexFormat(typeof(byte)); } }
        /// <summary>
        /// Directx will use ushort and my buffer will be formatted to one ushort (size, capacity, etc...)
        /// </summary>
        public static IndexLayout One16 { get { return manager.GetIndexFormat(typeof(ushort)); } }
        /// <summary>
        /// <seealso cref="IndexLayout.One16"/> Buffer writing process support also all 16 bit format
        /// because is a correct cast from ushort to uint
        /// </summary>
        public static IndexLayout One32 { get { return manager.GetIndexFormat(typeof(uint)); } }
        /// <summary>
        /// Directx will use ushort and my buffer will be formatted to two ushort (size, capacity, etc...)
        /// </summary>
        public static IndexLayout Edge16 { get { return manager.GetIndexFormat(typeof(Edge16)); } }
        /// <summary>
        /// <seealso cref="IndexLayout.Edge16"/> Buffer writing process support also all 16 bit format
        /// because is a correct cast from ushort to uint
        /// </summary>
        public static IndexLayout Edge32 { get { return manager.GetIndexFormat(typeof(Edge32)); } }
        /// <summary>
        /// Directx will use ushort and my buffer will be formatted to three ushort (size, capacity, etc...)
        /// </summary>
        public static IndexLayout Face16 { get { return manager.GetIndexFormat(typeof(Face16)); } }
        /// <summary>
        /// <seealso cref="IndexLayout.Face16"/> Buffer writing process support also all 16 bit format
        /// because is a correct cast from ushort to uint
        /// </summary>
        public static IndexLayout Face32 { get { return manager.GetIndexFormat(typeof(Face32)); } }

        public override string ToString()
        {
            return string.Format("Size: {0}b, Type: {1} , {2} , num {3}", bytesize, type.Name.ToString(), "x" + Bits.ToString(), numOfIndis);
        }

        #region Singleton Implementation
        static IndexInfoManager singleton = null;
        static IndexInfoManager manager
        {
            get
            {
                if (singleton == null) singleton = new IndexInfoManager();
                return singleton;
            }
        }
        class IndexInfoManager
        {
            // the database
            Dictionary<Type, IndexLayout> dictionary;
            public IndexInfoManager()
            {
                IndexLayout int8 = new IndexLayout { Bits = 8 , type = typeof(byte), is32Bit = false, numOfIndis = 1, bytesize = Marshal.SizeOf(typeof(byte)) };
                IndexLayout int16 = new IndexLayout { Bits = 16, type = typeof(Int16), is32Bit = false, numOfIndis = 1, bytesize = Marshal.SizeOf(typeof(Int16)) };
                IndexLayout int32 = new IndexLayout { Bits = 32, type = typeof(Int32), is32Bit = true, numOfIndis = 1, bytesize = Marshal.SizeOf(typeof(Int32)) };
                IndexLayout uint16 = new IndexLayout { Bits = 16, type = typeof(UInt16), is32Bit = false, numOfIndis = 1, bytesize = Marshal.SizeOf(typeof(UInt16)) };
                IndexLayout uint32 = new IndexLayout { Bits = 32, type = typeof(UInt32), is32Bit = true, numOfIndis = 1, bytesize = Marshal.SizeOf(typeof(UInt32)) };
                IndexLayout edge16 = new IndexLayout { Bits = 16, type = typeof(Edge16), is32Bit = false, numOfIndis = 2, bytesize = Marshal.SizeOf(typeof(Edge16)) };
                IndexLayout edge32 = new IndexLayout { Bits = 32, type = typeof(Edge32), is32Bit = true, numOfIndis = 2, bytesize = Marshal.SizeOf(typeof(Edge32)) };
                IndexLayout face16 = new IndexLayout { Bits = 16, type = typeof(Face16), is32Bit = false, numOfIndis = 3, bytesize = Marshal.SizeOf(typeof(Face16)) };
                IndexLayout face32 = new IndexLayout { Bits = 32, type = typeof(Face32), is32Bit = true, numOfIndis = 3, bytesize = Marshal.SizeOf(typeof(Face32)) };

                dictionary = new Dictionary<Type, IndexLayout>();
                dictionary.Add(typeof(byte), int8);
                dictionary.Add(typeof(Int16), int16);
                dictionary.Add(typeof(Int32), int32);
                dictionary.Add(typeof(UInt16), uint16);
                dictionary.Add(typeof(UInt32), uint32);
                dictionary.Add(typeof(Face16), face16);
                dictionary.Add(typeof(Face32), face32);
                dictionary.Add(typeof(Edge16), edge16);
                dictionary.Add(typeof(Edge32), edge32);
            }

            public IndexLayout GetIndexFormat(Type type)
            {
                if (!dictionary.ContainsKey(type)) throw new ArgumentException("This type not implemented");
                return dictionary[type];
            }

            public override string ToString()
            {
                StringBuilder str = new StringBuilder();
                str.AppendLine("IndexType list:");
                foreach (IndexLayout type in dictionary.Values)
                    str.AppendLine(type.ToString());
                return str.ToString();
            }
        }
        #endregion

    }

    /// <summary>
    /// Return the wrapper of directx buffer or pinned array
    /// </summary>
    public abstract class DataStream
    {
        protected IntPtr buffer;
        protected int bufferSize;
        protected bool isReadable;
        protected bool isLocked;
        /// <summary>
        /// Position in bytes in the buffer, used only for write with increment
        /// </summary>
        public int PositionInBytes { get; set; }

        public int flushBytes { get; protected set; }

        public DataStream(IntPtr bufferPtr, int buffersize, bool isReadable)
        {
            if (bufferPtr == IntPtr.Zero) throw new ArgumentNullException("buffer pointer null");
            this.buffer = bufferPtr;
            this.bufferSize = buffersize;
            this.isReadable = isReadable;
            this.PositionInBytes = 0;
            this.flushBytes = 0;
            this.isLocked = true;
        }

        public void Close()
        {
            isLocked = false;
            buffer = IntPtr.Zero;
            bufferSize = 0;
            isReadable = false;
        }

        public override string ToString()
        {
            return string.Format("Size: {0}b , Readable: {1}", bufferSize, isReadable);
        }
    }

    /// <summary>
    /// the vertices stream is a buffer's pointer manager that implement write and read functions
    /// can be used also for pinned array
    /// </summary>
    public class VertexStream : DataStream
    {
        const int sizefloat = sizeof(float);
        const int sizeint16 = sizeof(short);
        const int sizeint32 = sizeof(int);

        /// <summary>
        /// Descriptor of buffer's element format
        /// </summary>
        public VertexLayout bufferFormat { get; protected set; }

        /// <summary>
        /// Get the graphic data tool from a graphic buffer
        /// </summary>
        /// <param name="bufferPtr">gpu graphic buffer</param>
        /// <param name="bufferFormat">buffer element descriptor</param>
        /// <param name="Count">size int elements of buffer</param>
        /// <param name="canRead">check if is a readable buffer</param>
        public VertexStream(IntPtr bufferPtr, VertexLayout bufferFormat, int Count, bool canRead)
            : base(bufferPtr, Count * bufferFormat.bytesize, canRead)
        {
            this.bufferFormat = bufferFormat;
        }

        #region Write repeatedly a singular value
        public void WriteCollection<T>(T value, VertexElement element , int Offset, int Count, int BufferOffset) where T : struct
        {
            List<T> array = new List<T>();
            array.Add(value);
            WriteCollection<T>(array, element, Offset, Count, BufferOffset);
        }

        #endregion

        #region Write fragmented collections of values
        /// <summary>
        /// Write a pinnable array, more fast than copy bytes by bytes. To match the attribute to buffer i use
        /// the Usage semantic of VertexAttribute
        /// </summary>
        /// <param name="Offset">attribute to don't write</param>
        /// <param name="Count">num of attributes to write</param>
        /// <param name="BufferOffset">number of buffer's vertex to jump before start writting</param>
        public void WriteCollection<T>(VertexAttribute<T> vertexattribute, int Offset, int Count, int BufferOffset) where T : struct
        {
            VertexElement element;

            if (bufferFormat.GetElement(vertexattribute.Usage, vertexattribute.UsageIdx, out element))
            {
                WriteCollection<T>(vertexattribute.data, element, Offset, Count, BufferOffset);
            }
        }

        /// <summary>
        /// Write a generic collection of struct, using a VertexElement to use a specified position in buffer. Take a look to this code
        /// <code>
        /// WriteCollection&lt;float&gt;(float1, texchannel, 0, 4, 0);
        /// WriteCollection&lt;Vector2&gt;(float2, texchannel, 0, 2, 0);
        /// WriteCollection&lt;Vector4&gt;(float4, texchannel, 0, 1, 0);
        /// </code>
        /// the function is designed to implement also different array's version of same format
        /// </summary>
        /// <param name="element">the information about position of T in buffer</param>
        /// <param name="Offset">number of array's elements to jump before start writting</param>
        /// <param name="Count">num of array's elements to write</param>
        /// <param name="BufferOffset">number of buffer's vertex to jump before start writting</param>
        /// <remarks>
        /// <para>........................+..............................................................+..........</para>
        /// <para>      BufferOffset      | Array[Offset] , Array[Offset+1], , , Array[Offset + Count]   |          </para>
        /// <para>........................+..............................................................+..........</para>
        /// </remarks>
        public void WriteCollection<T>(IList<T> array, VertexElement element, int Offset, int Count, int BufferOffset) where T : struct
        {
            if (element.Equals(VertexElement.VertexDeclarationEnd)) throw new ArgumentException("Invalid element structure");

            if (Count <= 0) return;

            int sourceLenght = array.Count;
            if (Offset < 0 || Count + Offset > sourceLenght)
                throw new ArgumentException("array's Count and Offset wrong");

            int source_item = Marshal.SizeOf(typeof(T));
            int buffer_offset = bufferFormat.bytesize * BufferOffset;
            int source_size = (Offset + Count) * source_item;
            int source_offset = Offset * source_item;
            int source_start = element.offset;
            int source_end = bufferFormat.bytesize - element.offset - element.bytesize;
            int source_filling = source_size / element.bytesize * bufferFormat.bytesize;

            if (bufferSize - buffer_offset < source_filling)
                throw new ArgumentException("not enought buffer space");

            if (array.GetType() == typeof(T[]))
            {
                GCHandle handle = GCHandle.Alloc(array, GCHandleType.Pinned);
                IntPtr source = handle.AddrOfPinnedObject();
                MemoryTool.WriteFragmentStruct(
                    buffer, bufferSize, buffer_offset,
                    source, source_size, source_offset,
                    source_start, element.bytesize, source_end);
                handle.Free();
            }
            else
            {
                MemoryTool.WriteFragmentStructByStruct<T>(
                    buffer, bufferSize, buffer_offset,
                    array, (Offset + Count), Offset,
                    source_start, element.bytesize, source_end);
            }
        }
        #endregion

        #region Write default compact array
        /// <summary>
        /// Write a generic collection of struct, without VertexElement info the array will be write continuously
        /// </summary>
        /// <param name="Offset">attribute to don't write</param>
        /// <param name="Count">num of attributes to write</param>
        /// <param name="BufferOffset">number of buffer's vertex to jump before start writting</param>
        /// <remarks>
        /// <para>........................+.................................................................+..........</para>
        /// <para>      BufferOffset      | Array[Offset] , Array[Offset+1],      , Array[Offset + Count]   |          </para>
        /// <para>........................+.................................................................+..........</para>
        /// </remarks>
        public void WriteCollection<T>(IList<T> array, int Offset, int Count, int BufferOffset) where T : struct
        {
            if (Count <= 0) return;
            int sourceLenght = array.Count;
            if (Offset < 0 || Count + Offset > sourceLenght)
                throw new ArgumentException("array's Count and Offset wrong");

            int typesize = Marshal.SizeOf(typeof(T));
            int sourceSize = typesize * (Count + Offset);
            int sourceOffset = typesize * Offset;
            int bufferoffset = bufferFormat.bytesize * BufferOffset;

            if (bufferSize - bufferoffset < sourceSize - sourceOffset)
                throw new ArgumentException("not enought buffer space");

            if (array.GetType() == typeof(T[]))
            {
                GCHandle handle = GCHandle.Alloc(array, GCHandleType.Pinned);
                IntPtr source = handle.AddrOfPinnedObject();
                MemoryTool.WriteStruct(buffer, bufferSize, bufferoffset, source, sourceSize, sourceOffset);
                handle.Free();
            }
            else
            {
                MemoryTool.WriteStructByStruct<T>(buffer, bufferSize, bufferoffset, array, (Offset + Count), Offset);
            }

        }
        #endregion

        #region Write with increment
        public unsafe void WriteAndIncrement(Vector3 vector)
        {
            // pointer arithmetic in bytes
            Vector3* ptr = (Vector3*)((byte*)buffer.ToPointer() + PositionInBytes);
            *ptr = vector;
            PositionInBytes += Vector3.sizeinbyte;
        }
        public unsafe void WriteAndIncrement(Color32 color)
        {
            Color32* ptr = (Color32*)((byte*)buffer.ToPointer() + PositionInBytes);
            *ptr = color;
            PositionInBytes += Color32.sizeinbyte;
        }
        public unsafe void WriteAndIncrement(Vector2 vector)
        {
            Vector2* ptr = (Vector2*)((byte*)buffer.ToPointer() + PositionInBytes);
            *ptr = vector;
            PositionInBytes += Vector2.sizeinbyte;
        }
        public unsafe void WriteAndIncrement(float value)
        {
            float* ptr = (float*)((byte*)buffer.ToPointer() + PositionInBytes);
            *ptr = value;
            PositionInBytes += sizefloat;
        }
        #endregion
    }

    /// <summary>
    /// the indices stream is a buffer's pointer manager that implement write and read functions
    /// can be used also for pinned array
    /// </summary>
    public class IndexStream : DataStream
    {
        const int sizeint8 = sizeof(byte);
        const int sizeint16 = sizeof(short);
        const int sizeint32 = sizeof(int);

        /// <summary>
        /// Descriptor of buffer's element format
        /// </summary>
        public IndexLayout bufferFormat { get; protected set; }

        /// <summary>
        /// Get the graphic data tool from a graphic buffer
        /// </summary>
        /// <param name="bufferPtr">gpu graphic buffer</param>
        /// <param name="bufferinfo">buffer element descriptor</param>
        /// <param name="Count">size int elements of buffer</param>
        /// <param name="canRead">check if is a readable buffer</param>
        public IndexStream(IntPtr bufferPtr, IndexLayout bufferFormat, int Count, bool canRead)
            : base(bufferPtr, Count * bufferFormat.bytesize, canRead)
        {
            this.bufferFormat = bufferFormat;
        }

        #region Write default compact array with casting 16bit to 32bit

        /// <summary>
        /// </summary>
        /// <param name="BufferOffset">number of buffer's vertex to jump before start writting</param>
        /// <param name="IndexOffset">an optional value to sum to each indices when writting, usefull for batching method, can be negative</param>
        public void WriteCollection<T>(IndexAttribute<T> indexattribute, int BufferOffset, int IndexOffset) where T : struct
        {
            IndexLayout format = indexattribute.format;
            int source_size = format.bytesize * indexattribute.Count;
            int buffer_offset = bufferFormat.bytesize * BufferOffset;

            int source_nindis = indexattribute.Count / format.bytesize * format.numOfIndis;
            int source_filling = source_nindis * (bufferFormat.is32Bit ? sizeof(uint) : sizeof(ushort));
            if (bufferSize - buffer_offset < source_filling)
                throw new ArgumentException("not enought buffer space");

            IntPtr source = indexattribute.GetPointer();
            MemoryTool.WriteIndex(buffer, bufferFormat, bufferSize, buffer_offset, source, format, source_size, 0, IndexOffset);
            indexattribute.FreePointer();
        }
        /// <summary>
        /// a particual implementation for attribute stored in a generic list, the indices must be write index by index and can't use the faster
        /// vertsion with a pinnable array
        /// </summary>
        /// <param name="BufferOffset">number of buffer's vertex to jump before start writting</param>
        /// <param name="IndexOffset">an optional value to sum to each indices when writting, usefull for batching method, can be negative</param>
        public void WriteCollection<T>(IList<T> indices, IndexLayout format, int Offset, int Count, int BufferOffset, int IndexOffset) where T : struct
        {
            if (Count <= 0) return;

            int sourceLenght = indices.Count;
            if (Offset < 0 || Count + Offset > sourceLenght)
                throw new ArgumentException("array's Count and Offset wrong");

            int sizeofT = Marshal.SizeOf(typeof(T));
            int source_size = format.bytesize * (Offset + Count);
            int source_offset = format.bytesize * Offset;
            int buffer_offset = bufferFormat.bytesize * BufferOffset;

            int source_filling = Count * format.numOfIndis * (bufferFormat.is32Bit ? sizeof(uint) : sizeof(ushort));
            if (bufferSize - buffer_offset < source_filling)
                throw new ArgumentException("not enought buffer space");

            if (sizeofT != format.bytesize)
                throw new Exception("source array not match with indexInfo definition");

            if (indices.GetType() == typeof(T[]) && format != IndexLayout.One8)
            {
                GCHandle handle = GCHandle.Alloc(indices, GCHandleType.Pinned);
                IntPtr source = handle.AddrOfPinnedObject();
                // can use packed version because i don't need take care about casting indices
                if (IndexOffset == 0 && (format.is32Bit == bufferFormat.is32Bit))
                    MemoryTool.WriteStruct(buffer, bufferSize, buffer_offset, source, source_size, source_offset);
                else
                    MemoryTool.WriteIndex(buffer, bufferFormat, bufferSize, buffer_offset, source, format, source_size, source_offset, IndexOffset);
                handle.Free();
            }
            else
            {
                MemoryTool.WriteIndexByIndex<T>(buffer, bufferFormat, bufferSize, buffer_offset, indices, format, Count + Offset, Offset, IndexOffset);
            }
        }

        /// <summary>
        /// Write a generic collection of struct, without IndexInfo the array will not be casting in uint or ushort
        /// </summary>
        /// <param name="Offset">attribute to don't write</param>
        /// <param name="Count">num of attributes to write</param>
        /// <param name="BufferOffset">number of buffer's vertex to jump before start writting</param>
        public void WriteCollection<T>(IList<T> array, int Offset, int Count, int BufferOffset) where T : struct
        {
            if (Count <= 0) return;
            int sourceLenght = array.Count;
            if (Offset < 0 || Count + Offset > sourceLenght)
                throw new ArgumentException("array's Count and Offset wrong");

            int typesize = Marshal.SizeOf(typeof(T));
            int sourceSize = typesize * (Count + Offset);
            int sourceOffset = typesize * Offset;
            int bufferoffset = bufferFormat.bytesize * BufferOffset;

            if (bufferSize - bufferoffset < sourceSize - sourceOffset)
                throw new ArgumentException("not enought buffer space");

            if (array.GetType() == typeof(T[]))
            {
                GCHandle handle = GCHandle.Alloc(array, GCHandleType.Pinned);
                IntPtr source = handle.AddrOfPinnedObject();
                MemoryTool.WriteStruct(buffer, bufferSize, bufferoffset, source, sourceSize, sourceOffset);
                handle.Free();
            }
            else
            {
                MemoryTool.WriteStructByStruct<T>(buffer, bufferSize, bufferoffset, array, (Offset + Count), Offset);
            }
        }

        #endregion

        #region Write with increment
        public unsafe void WriteAndIncrement(Face32 face)
        {
            // pointer arithmetic in bytes
            Face32* ptr = (Face32*)((byte*)buffer.ToPointer() + PositionInBytes);
            *ptr = face;
            PositionInBytes += Face32.sizeinbyte;
        }
        public unsafe void WriteAndIncrement(Edge32 edge)
        {
            Edge32* ptr = (Edge32*)((byte*)buffer.ToPointer() + PositionInBytes);
            *ptr = edge;
            PositionInBytes += Edge32.sizeinbyte;
        }
        public unsafe void WriteAndIncrement(Face16 face)
        {
            // pointer arithmetic in bytes
            Face16* ptr = (Face16*)((byte*)buffer.ToPointer() + PositionInBytes);
            *ptr = face;
            PositionInBytes += Face16.sizeinbyte;
        }
        public unsafe void WriteAndIncrement(Edge16 edge)
        {
            Edge16* ptr = (Edge16*)((byte*)buffer.ToPointer() + PositionInBytes);
            *ptr = edge;
            PositionInBytes += Edge16.sizeinbyte;
        }

        /// <summary>
        /// </summary>
        /// <param name="value">WARNING : no cast operation when write to a indexbuffer of 16bit</param>
        public unsafe void WriteAndIncrement(uint value)
        {
            uint* ptr = (uint*)((byte*)buffer.ToPointer() + PositionInBytes);
            *ptr = value;
            PositionInBytes += sizeint32;
        }
        /// <summary>
        /// </summary>
        /// <param name="value">WARNING : no cast operation when write to a indexbuffer of 16bit</param>
        public unsafe void WriteAndIncrement(int value)
        {
            int* ptr = (int*)((byte*)buffer.ToPointer() + PositionInBytes);
            *ptr = value;
            PositionInBytes += sizeint32;
        }
        /// <summary>
        /// </summary>
        /// <param name="value">WARNING : no cast operation when write to a indexbuffer of 32bit</param>
        public unsafe void WriteAndIncrement(ushort value)
        {
            ushort* ptr = (ushort*)((byte*)buffer.ToPointer() + PositionInBytes);
            *ptr = value;
            PositionInBytes += sizeint16;
        }
        /// <summary>
        /// </summary>
        /// <param name="value">WARNING : no cast operation when write to a indexbuffer of 32bit</param>
        public unsafe void WriteAndIncrement(short value)
        {
            short* ptr = (short*)((byte*)buffer.ToPointer() + PositionInBytes);
            *ptr = value;
            PositionInBytes += sizeint16;
        }
        #endregion
    }

    /// <summary>
    /// the pixel stream is a buffer's pointer manager that implement write and read functions
    /// can be used also for pinned array. A particular attention when work with compress Dxt version,
    /// not all functions are implemented yet.
    /// </summary>
    public class TextureStream
    {
        IntPtr bufferPtr;
        bool enabled;

        int Width, Height;
        int boffset, brow;

        int min(int a, int b) { return a < b ? a : b; }
        int max(int a, int b) { return a > b ? a : b; }


        public Format pixelFormat { get; private set; }

        /// <summary> 
        /// </summary>
        /// <param name="bufferPtr">pointer of gpu buffer</param>
        /// <param name="bufferOffsetSize">offset in bytes of buffer, used for pointer aritmetic</param>
        /// <param name="bufferRowSize">bytes of a width row, used to correct jump to next row, 90% match with Width*PixelSize</param>
        /// <param name="pixelFormat"></param>
        /// <param name="Width">Width in pixels of locked texture</param>
        /// <param name="Height">Height in pixels of locked texture</param>
        /// <remarks>
        /// The concept of TextureStream is to separate all depth levels into singular and simple manageable 2d texture, so
        /// we need a bufferoffset (data.SlicePitch) for buffer arithmetic 
        /// </remarks>
        public TextureStream(IntPtr bufferPtr, int bufferOffsetSize , int bufferRowSize , Format pixelFormat, int Width, int Height)
        {
            if (bufferPtr == IntPtr.Zero) throw new ArgumentNullException("buffer pointer null");
            this.bufferPtr = bufferPtr;
            this.enabled = true;
            this.Width = Width;
            this.Height = Height;
            this.pixelFormat = pixelFormat;
            this.boffset = bufferOffsetSize;
            this.brow = bufferRowSize;

            int pxsize = (int)getPixelSize(pixelFormat);

            if (boffset % pxsize != 0 || brow % pxsize != 0) throw new NotImplementedException("comming soon...");

        }

        public void Close()
        {
            bufferPtr = IntPtr.Zero;
            enabled = false;
        }

        public Color32[,] GetData()
        {
            if (!enabled) throw new Exception("buffer closed");

            Color32[,] data = null;

            if (isDxtcompressed(pixelFormat))
            {
                DXTtools.DecompressStream(bufferPtr, Width, Height, getDxtVersion(pixelFormat), out data);
            }
            else
            {
                int pxsize = (int)getPixelSize(pixelFormat); 
                int pitch = brow / pxsize;
                int offset = boffset / pxsize;
                data = new Color32[Width, Height];

                unsafe
                {
                    ///////////////////////////////////////////////// 16bit
                    if (pxsize == 2)
                    {
                        PixelTools.UnPacker<UInt16> unpacker16 = PixelTools.unpacker16(pixelFormat);
                        UInt16* psrc = (UInt16*)bufferPtr.ToPointer();
                        for (int y = 0; y < Height; y++) for (int x = 0; x < Width; x++) data[x, y] = unpacker16(psrc[offset + x + y * pitch]);
                    }
                    ///////////////////////////////////////////////// 32bit
                    else if (pxsize == 4)
                    {
                        PixelTools.UnPacker<UInt32> unpacker32 = PixelTools.unpacker32(pixelFormat);
                        UInt32* psrc = (UInt32*)bufferPtr.ToPointer();
                        for (int y = 0; y < Height; y++) for (int x = 0; x < Width; x++) data[x, y] = unpacker32(psrc[offset + x + y * pitch]);
                    }
                    ///////////////////////////////////////////////// 64bit
                    else if (pxsize == 8)
                    {
                        PixelTools.UnPacker<UInt64> unpacker64 = PixelTools.unpacker64(pixelFormat);
                        UInt64* psrc = (UInt64*)bufferPtr.ToPointer();
                        for (int y = 0; y < Height; y++) for (int x = 0; x < Width; x++) data[x, y] = unpacker64(psrc[offset + x + y * pitch]);
                    }
                    ///////////////////////////////////////////////// 128bit
                    else if (pxsize == 16)
                    {
                        PixelTools.UnPacker<Uint128> unpacker128 = PixelTools.unpacker128(pixelFormat);
                        Uint128* psrc = (Uint128*)bufferPtr.ToPointer();
                        for (int y = 0; y < Height; y++) for (int x = 0; x < Width; x++) data[x, y] = unpacker128(psrc[offset + x + y * pitch]);
                    }
                }         
            }
            return data;
        }

        public void SetData(Color32[,] data)
        {
            if (!enabled) throw new Exception("buffer closed");

            int width = min(data.GetLength(0), Width);
            int height = min(data.GetLength(1), Height);
            
            if (isDxtcompressed(pixelFormat))
            {
                DXTtools.CompressStream(bufferPtr, Width, Height, getDxtVersion(pixelFormat), data);
            }
            else
            {
                int pxsize = (int)getPixelSize(pixelFormat);
                int pitch = brow / pxsize;
                int offset = boffset / pxsize;
                unsafe
                {
                    ///////////////////////////////////////////////// 16bit
                    if (pxsize == 2)
                    {
                        PixelTools.Packer<UInt16> packer16 = PixelTools.packer16(pixelFormat);
                        UInt16* psrc = (UInt16*)bufferPtr.ToPointer();
                        for (int y = 0; y < height; y++) for (int x = 0; x < width; x++) psrc[offset + x + y * pitch] = packer16((Vector4)data[x, y]);
                    }
                    ///////////////////////////////////////////////// 32bit
                    else if (pxsize == 4)
                    {
                        PixelTools.Packer<UInt32> packer32 = PixelTools.packer32(pixelFormat);
                        UInt32* psrc = (UInt32*)bufferPtr.ToPointer();
                        for (int y = 0; y < height; y++) for (int x = 0; x < width; x++) psrc[offset + x + y * pitch] = packer32((Vector4)data[x, y]);
                    }
                    ///////////////////////////////////////////////// 64bit
                    else if (pxsize == 8)
                    {
                        PixelTools.Packer<UInt64> packer64 = PixelTools.packer64(pixelFormat);
                        UInt64* psrc = (UInt64*)bufferPtr.ToPointer();
                        for (int y = 0; y < height; y++) for (int x = 0; x < width; x++) psrc[offset + x + y * pitch] = packer64((Vector4)data[x, y]);
                    }
                    ///////////////////////////////////////////////// 128bit
                    else if (pxsize == 16)
                    {
                        PixelTools.Packer<Uint128> packer128 = PixelTools.packer128(pixelFormat);
                        Uint128* psrc = (Uint128*)bufferPtr.ToPointer();
                        for (int y = 0; y < height; y++) for (int x = 0; x < width; x++) psrc[offset + x + y * pitch] = packer128((Vector4)data[x, y]);
                    }
                }
            }
        }

        public void SetData(Bitmap bitmap)
        {
            if (!enabled) throw new Exception("buffer closed");

            int width = min(bitmap.Width, Width);
            int height = min(bitmap.Height, Height);

            if (isDxtcompressed(pixelFormat))
            {
                throw new NotImplementedException();
                //DXTtools.CompressStream(bufferPtr, Width, Height, getDxtVersion(pixelFormat), data);
            }
            else
            {
                BitmapLock bmptool = new BitmapLock(bitmap);
                bmptool.LockBits();

                int pxsize = (int)getPixelSize(pixelFormat);
                int pitch = brow / pxsize;
                int offset = boffset / pxsize;
                unsafe
                {
                    ///////////////////////////////////////////////// 16bit
                    if (pxsize == 2)
                    {
                        PixelTools.Packer<UInt16> packer16 = PixelTools.packer16(pixelFormat);
                        UInt16* psrc = (UInt16*)bufferPtr.ToPointer();
                        for (int y = 0; y < height; y++) for (int x = 0; x < width; x++) psrc[offset + x + y * pitch] = packer16(bmptool.GetPixelV4(x, y));
                    }
                    ///////////////////////////////////////////////// 32bit
                    else if (pxsize == 4)
                    {
                        PixelTools.Packer<UInt32> packer32 = PixelTools.packer32(pixelFormat);
                        UInt32* psrc = (UInt32*)bufferPtr.ToPointer();
                        for (int y = 0; y < height; y++) for (int x = 0; x < width; x++) psrc[offset + x + y * pitch] = packer32(bmptool.GetPixelV4(x, y));
                    }
                    ///////////////////////////////////////////////// 64bit
                    else if (pxsize == 8)
                    {
                        PixelTools.Packer<UInt64> packer64 = PixelTools.packer64(pixelFormat);
                        UInt64* psrc = (UInt64*)bufferPtr.ToPointer();
                        for (int y = 0; y < height; y++) for (int x = 0; x < width; x++) psrc[offset + x + y * pitch] = packer64(bmptool.GetPixelV4(x, y));
                    }
                    ///////////////////////////////////////////////// 128bit
                    else if (pxsize == 16)
                    {
                        PixelTools.Packer<Uint128> packer128 = PixelTools.packer128(pixelFormat);
                        Uint128* psrc = (Uint128*)bufferPtr.ToPointer();
                        for (int y = 0; y < height; y++) for (int x = 0; x < width; x++) psrc[offset + x + y * pitch] = packer128(bmptool.GetPixelV4(x, y));
                    }
                }
                bmptool.UnlockBits();
            }
 
        }


        public static bool isDxtcompressed(Format format)
        {
            return format == Format.Dxt1 ||
                format == Format.Dxt2 ||
                format == Format.Dxt3 ||
                format == Format.Dxt4 ||
                format == Format.Dxt5;
        }


        public static int getDxtVersion(Format format)
        {
            switch (format)
            {
                case Format.Dxt1: return 1;
                case Format.Dxt2: return 2;
                case Format.Dxt3: return 3;
                case Format.Dxt4: return 4;
                case Format.Dxt5: return 5;
                default: throw new NotImplementedException();
            }
        }
        /// <summary>
        /// return the size in bytes of pixels, if size is &lt;2 is compressed
        /// </summary>
        public static float getPixelSize(Format format)
        {
            switch (format)
            {
                // 128bit pixel format
                case Format.A32B32G32R32F:
                    return 16;
                // 64bit
                case Format.A16B16G16R16F:
                case Format.A16B16G16R16:
                    return 8;
                // 32bit
                case Format.A8R8G8B8:
                case Format.A8B8G8R8:
                case Format.X8R8G8B8:
                case Format.X8B8G8R8:
                    return 4;
                // 16bit
                case Format.A4R4G4B4:
                case Format.X4R4G4B4:
                case Format.A1R5G5B5:
                case Format.X1R5G5B5:
                    return 2;
                // compressed
                case Format.Dxt1:
                    return 8.0f / 16; // 8bytes for 4x4 pixels block
                case Format.Dxt2:
                case Format.Dxt3:
                case Format.Dxt4:
                case Format.Dxt5:
                    return 16.0f / 16; // 16bytes for 4x4 pixels block
                default:
                    throw new NotImplementedException("unknow format");
            }
        }

        static int Max(int a, int b) { return a > b ? a : b; }

        /// <summary>
        /// return the size in bytes of rows, from Microsodt sdk
        /// </summary>
        public static int getPitchSize(Format format, int width)
        {
            float pxsize = getPixelSize(format);

            if (pxsize < 2)
            {
                switch (format)
                {
                    case Format.Dxt2:
                    case Format.Dxt3:
                    case Format.Dxt4:
                    case Format.Dxt5:
                        return Max(1, ((width + 3) / 4)) * 16;
                    default:
                        // dxt1
                        return Max(1, ((width + 3) / 4)) * 8;
                }
            }
            else
            {
                return (int)(pxsize * width);
            }
        }
    }

}
