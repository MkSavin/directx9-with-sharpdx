﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Collections;

using Engine.Maths;
using Engine.Tools;

namespace Engine
{
    public abstract class BufferAttribute<T> : IEnumerable<T> where T : struct
    {
        protected GCHandle handle;
        protected int elementSize;
        public T[] data;

        public BufferAttribute(byte[] buffer)
        {
            elementSize = Marshal.SizeOf(typeof(T));
            int nbytes = buffer.Length;
            int count = nbytes / elementSize;
            data = new T[count];

            MemoryTool.WriteStruct<byte, T>(buffer, data, nbytes);
        }

        public BufferAttribute(int count, T initialvalue)
        {
            elementSize = Marshal.SizeOf(typeof(T));
            data = new T[count];
            for (int i = 0; i < count; i++) data[i] = initialvalue;
        }
        public BufferAttribute(ref T[] source)
        {
            elementSize = Marshal.SizeOf(typeof(T));
            data = source;
        }

        public BufferAttribute(IList<T> source)
        {
            if (source.Count == 0) throw new ArgumentException("source can't be empty");
            elementSize = Marshal.SizeOf(typeof(T));
            data = new T[source.Count];
            source.CopyTo(data, 0);
        }
  
        ~BufferAttribute()
        {
            FreePointer();
        }

        public int Count
        {
            get { return data.GetUpperBound(0) + 1; }
        }

        public T this[int index]
        {
            get { return data[index]; }
            set { data[index] = value; }
        }

        public IntPtr GetPointer()
        {
            handle = GCHandle.Alloc(data, GCHandleType.Pinned);
            return handle.AddrOfPinnedObject();
        }
        public void FreePointer()
        {
            if (handle.IsAllocated) handle.Free();
        }

        public IEnumerator<T> GetEnumerator()
        {
            return ((IEnumerable<T>)this.data).GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }


    /// <summary>
    /// Manager of vertices array fragmented by DeclarationUsage
    /// </summary>
    public class VertexAttribute<T> : BufferAttribute<T> where T : struct
    {
        /// <summary>
        /// Usage of this array, match with semantic in shader version. It'll be used also to understand the usage in fragmented buffer
        /// when you need to fill vertices in the buffer
        /// </summary>
        public DeclarationUsage Usage { get; set; }
        /// <summary>
        /// Index for different semantic with same Usage
        /// </summary>
        public int UsageIdx { get; set; }

        /// <summary>
        /// buffer can be a generic byte array, data bytes size will be equal or greater than buffer bytes size
        /// </summary>
        public VertexAttribute(DeclarationUsage usage, byte[] buffer, int usageidx = 0)
            : base(buffer)
        {
            Usage = usage;
            UsageIdx = usageidx;
        }
        /// <summary>
        /// </summary>
        public VertexAttribute(DeclarationUsage usage, int count, T initialvalue, int usageidx = 0)
            : base(count, initialvalue)
        {
            Usage = usage;
            UsageIdx = usageidx;
        }
        /// <summary>
        /// </summary>
        public VertexAttribute(DeclarationUsage usage, int count, int usageidx = 0)
            : base(count, default(T))
        {
            Usage = usage;
            UsageIdx = usageidx;
        }
        /// <summary>
        /// </summary>
        public VertexAttribute(DeclarationUsage usage, ref T[] source, int usageidx = 0)
            : base(ref source)
        {
            Usage = usage;
            UsageIdx = usageidx;
        }
        /// <summary>
        /// </summary>
        public VertexAttribute(DeclarationUsage usage, IList<T> source, int usageidx = 0)
            : base(source)
        {
            Usage = usage;
            UsageIdx = usageidx;
        }

        /// <summary>
        /// Create a copy
        /// </summary>
        public VertexAttribute<T> Clone()
        {
            return new VertexAttribute<T>(this.Usage, (T[])data.Clone());
        }
        /// <summary>
        /// Write entire attribute to a graphic buffer
        /// </summary>
        /// <param name="buffer">destination buffer</param>
        /// <param name="bufferInfo">buffer's element descriptor, used to know where write</param>
        /// <param name="bufferSize">buffer size in bytes,</param>
        /// <param name="bufferOffset">offset in bytes in buffer where write beginning</param>
        /// <returns></returns>
        public bool WriteToBuffers(IntPtr buffer, VertexLayout bufferInfo, int bufferSize, int bufferOffset)
        {
            VertexElement element;

            if (bufferInfo.GetElement(Usage, UsageIdx, out element))
            {
                if (elementSize != element.bytesize)
                    throw new ArgumentException("wrong size");

                IntPtr source = GetPointer();
                int sourcesize = Count * element.bytesize;
                int start = element.offset;
                int end = bufferInfo.bytesize - element.bytesize - element.offset;
                MemoryTool.WriteFragmentStruct(buffer, bufferSize, bufferOffset, source, sourcesize, 0, start, element.bytesize, end);
                FreePointer();
                return true;
            }
            return false;
        }
    }

    /// <summary>
    /// Manager of indices array, don't exist fragmented version, the difference is only 32bit or 16bit
    /// </summary>
    /// <remarks>when write to buffer the best way is split into base numeric uint or ushort</remarks>
    public class IndexAttribute<T> : BufferAttribute<T> where T : struct
    {
        // TODO: usefull to enable uint->ushort casting
        uint maxIndex;
        
        public IndexLayout format { get; private set; }

        /// <summary>
        /// buffer can be a generic byte array, data bytes size will be equal or greater than buffer bytes size
        /// </summary>
        public IndexAttribute(byte[] buffer)
            : base(buffer)
        {
            format = IndexLayout.GetIndexFormat(typeof(T));
            if (Marshal.SizeOf(typeof(T)) != format.bytesize)
                throw new Exception("wrong struct size");
        }
        /// <summary>
        /// </summary>
        public IndexAttribute(int count, T initialValue)
            : base(count,initialValue)
        {
            format = IndexLayout.GetIndexFormat(typeof(T)); 
            if (Marshal.SizeOf(typeof(T)) != format.bytesize)
                throw new Exception("wrong struct size");
        }
                /// <summary>
        /// </summary>
        public IndexAttribute(int count)
            : this(count, default(T))
        { }

        /// <summary>
        /// </summary>
        public IndexAttribute(ref T[] source):
            base(ref source)
        {
            format = IndexLayout.GetIndexFormat(typeof(T));
            if (Marshal.SizeOf(typeof(T)) != format.bytesize)
                throw new Exception("wrong struct size");
        }
        /// <summary>
        /// </summary>
        public IndexAttribute(IList<T> source)
            : base(source)
        {
            format = IndexLayout.GetIndexFormat(typeof(T));
            if (Marshal.SizeOf(typeof(T)) != format.bytesize)
                throw new Exception("wrong struct size");
        }

        /// <summary>
        /// Create a copy
        /// </summary>
        public IndexAttribute<T> Clone()
        {
            IndexAttribute<T> copy = new IndexAttribute<T>((T[])data.Clone());
            copy.format = this.format;
            copy.maxIndex = this.maxIndex;
            return copy;
        }

        /// <summary>
        /// Cast operation, work only if targhet buffer use a type greater than source, so is not possible
        /// copy from Uint32 to Uint16
        /// </summary>
        /// <typeparam name="K">the structure to cast, sizeof must be equal or less than currect struct</typeparam>
        public IndexAttribute<K> ConverterTo<K>() where K : struct
        {
            IndexLayout destFormat = IndexLayout.GetIndexFormat(typeof(K));

            // destination
            int destCount = format.numOfIndis * Count / destFormat.numOfIndis;
            IndexAttribute<K> dest = new IndexAttribute<K>(destCount);

            IntPtr destBuffer = dest.GetPointer();
            IntPtr srcBuffer = this.GetPointer();
            MemoryTool.WriteIndex(destBuffer, destFormat, destCount, 0, srcBuffer, this.format, 0, Count, 0);
            dest.FreePointer();
            this.FreePointer();

            dest.maxIndex = maxIndex;
            return dest;
        }

        /// <summary>
        /// Write entire attribute to a graphic buffer
        /// </summary>
        /// <param name="buffer">destination buffer</param>
        /// <param name="bufferInfo">buffer's element descriptor, used to know where write</param>
        /// <param name="bufferSize">buffer size in bytes,</param>
        /// <param name="bufferOffset">offset in bytes in buffer where write beginning</param>
        /// <param name="IndexOffset">a value to sum to each indices, usefull when use batch algorithm</param>
        /// <returns></returns>
        public bool WriteToBuffers(IntPtr buffer, IndexLayout bufferInfo, int bufferSize, int bufferOffset,int IndexOffset)
        {
            if (Count > 0)
            {
                IntPtr source = GetPointer();
                int size = format.bytesize * Count;
                MemoryTool.WriteIndex(buffer, bufferInfo, bufferSize, bufferOffset, source, format, size, 0, IndexOffset);
                FreePointer();
                return true;
            }
            return false;
        }     
    }
}
