﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Engine
{
    #region Directx9 enum <-> numeric conversion

    /*
     * int32 range : −2147483648 to 2147483647
     * 
    /// <summary>
    /// Int32 enum from Microsoft SDK
    /// </summary>
    public enum DeviceState
    {
        OutOfVideoMemory = -2005532292
        DeviceLost = -2005530520,
        DeviceRemoved = -2005530512,
        DeviceHung = -2005530508,
        Ok = 0,
        PresentModeChanged = 141953143,
        PresentOccluded = 141953144,
    }
    /// <summary>
    /// Int32 enum from SharpDx
    /// </summary>
    public enum ResultCode
    {
        WrongTextureFormat = -2005530600,
        UnsupportedColorOperation = -2005530599,
        UnsupportedColorArgument = -2005530598,
        UnsupportedAlphaOperation = -2005530597,
        UnsupportedAlphaArgument = -2005530596,
        TooManyOperations = -2005530595,
        ConflictingTextureFilter = -2005530594,
        UnsupportedFactorValue = -2005530593,
        UnsupportedTextureFilter = -2005530590,
        DeviceLost = -2005530520,
        DeviceNotReset = -2005530519,
        NotAvailable = -2005530518,
        InvalidDevice = -2005530517,
        InvalidCall = -2005530516,
        Success = 0,
    }
    // from sharpdx
    [Flags]
    public enum DeviceCaps
    {
        ExecuteSystemMemory = 16,
        ExecuteVideoMemory = 32,
        TLVertexSystemMemory = 64,
        TLVertexVideoMemory = 128,
        TextureSystemMemory = 256,
        TextureVideoMemory = 512,
        DrawPrimTLVertex = 1024,
        CanRenderAfterFlip = 2048,
        TextureNonLocalVideoMemory = 4096,
        DrawPrimitives2 = 8192,
        SeparateTextureMemory = 16384,
        DrawPrimitives2Extended = 32768,
        HWTransformAndLight = 65536,
        CanBlitSysToNonLocal = 131072,
        HWRasterization = 524288,
        PureDevice = 1048576,
        QuinticRTPatches = 2097152,
        RTPatches = 4194304,
        RTPatchHandleZero = 8388608,
        NPatches = 16777216,
    }
    */

    public enum DeviceType
    {
        Hardware = 1,
        Reference = 2,
        Software = 3,
        NullReference = 4,
    }

    /// <summary>
    /// Device state
    /// </summary>
    public enum DeviceState
    {
        OK = 0,
        DeviceNotReset = -2005530519,
        /// <summary>
        ///  Wait some ms before reseting
        /// </summary>
        DeviceLost= -2005530520,
        InvalidDevice = -2005530517,
        InvalidCall = -2005530516,
    }
    public enum ResourceType
    {
        Surface = 1,
        Volume = 2,
        Texture = 3,
        VolumeTexture = 4,
        CubeTexture = 5,
        VertexBuffer = 6,
        IndexBuffer = 7,
    }
    /// <summary>
    /// Defines the primitives supported by Microsoft Direct3D. (from microsoft sdk)
    /// I added new Instance primitive 
    /// </summary>
    public enum PrimitiveType
    {
        Unknow = -1,
        Instance = 0,

        PointList = 1,

        LineList = 2,
        LineStrip = 3,

        TriangleList = 4,
        TriangleStrip = 5,
        TriangleFan = 6,
    }
    /// <summary>
    /// Defines the type of lock to perform. (from microsoft sdk)
    /// </summary>  
    [Flags]
    public enum LockFlags
    {
        Normal = 0,
        ReadOnly = 16,
        //NoSystemLock = 2048,
        NoOverwrite = 4096,
        Discard = 8192,
        //DoNotWait = 16384,
        //NoDirtyUpdate = 32768,
    }
    /// <summary>
    /// Enum : Defines the supported culling modes, which specify how back faces are culled during geometry rendering.
    /// </summary>  
    public enum Cull
    {
        None = 1,
        Clockwise = 2,
        CounterClockwise = 3,
    }
    /// <summary>
    /// Enum : Defines constants that describe the fill mode.
    /// </summary>
    public enum FillMode
    {
        Point = 1,
        WireFrame = 2,
        Solid = 3,
    }
    /// <summary>
    /// Display format
    /// </summary>
    public enum Format
    {
        Unknown = 0,
        R8G8B8 = 20,
        A8R8G8B8 = 21,
        X8R8G8B8 = 22,
        R5G6B5 = 23,
        X1R5G5B5 = 24,
        A1R5G5B5 = 25,
        A4R4G4B4 = 26,
        R3G3B2 = 27,
        A8 = 28,
        A8R3G3B2 = 29,
        X4R4G4B4 = 30,
        A2B10G10R10 = 31,
        A8B8G8R8 = 32,
        X8B8G8R8 = 33,
        G16R16 = 34,
        A2R10G10B10 = 35,
        A16B16G16R16 = 36,
        A8P8 = 40,
        P8 = 41,
        L8 = 50,
        A8L8 = 51,
        A4L4 = 52,
        V8U8 = 60,
        L6V5U5 = 61,
        X8L8V8U8 = 62,
        Q8W8V8U8 = 63,
        V16U16 = 64,
        A2W10V10U10 = 67,
        D16Lockable = 70,
        D32 = 71,
        D15S1 = 73,
        D24S8 = 75,
        D24X8 = 77,
        D24X4S4 = 79,
        D16 = 80,
        L16 = 81,
        D32SingleLockable = 82,
        D24SingleS8 = 83,
        D32Lockable = 84,
        S8Lockable = 85,
        VertexData = 100,
        Index16 = 101,
        Index32 = 102,
        Q16W16V16U16 = 110,
        R16F = 111,
        G16R16F = 112,
        A16B16G16R16F = 113,
        R32F = 114,
        G32R32F = 115,
        A32B32G32R32F = 116,
        MtCxV8U8 = 117,
        A1 = 118,
        MtA2B10G10R10XrBias = 119,
        BinaryBuffer = 199,
        Multi2Argb8 = 827606349,
        Dxt1 = 827611204,
        Dxt2 = 844388420,
        Yuy2 = 844715353,
        Dxt3 = 861165636,
        Dxt4 = 877942852,
        Dxt5 = 894720068,
        G8R8_G8B8 = 1111970375,
        R8G8_B8G8 = 1195525970,
        Uyvy = 1498831189,
    }
    /// <summary>
    /// Copy of Format with only depth stencil values
    /// </summary>
    public enum DepthFormat
    {
        Unknown = 0,
        D16Lockable = 70,
        D32 = 71,
        D15S1 = 73,
        D24S8 = 75,
        D24X8 = 77,
        D24X4S4 = 79,
        D16 = 80,
        L16 = 81,
        D32SingleLockable = 82,
        D24SingleS8 = 83,
    }
    
    /// <summary>
    /// Flag : Describes values that define a vertex format used to describe the contents
    /// of vertices that are stored interleaved in a single data stream.
    /// </summary>
    [Flags]
    public enum VertexFormat
    {
        Texture0 = 0,
        None = 0,
        /// <summary>D3DFVF_XYZ</summary>
        Position = 2,
        /// <summary>D3DFVF_XYZRHW</summary>
        Transformed = 4,
        /// <summary>D3DFVF_XYZB1</summary>
        PositionBlend1 = 6,
        /// <summary>D3DFVF_XYZB2</summary>
        PositionBlend2 = 8,
        //TextureCountShift = 8,
        /// <summary>D3DFVF_XYZB3</summary>
        PositionBlend3 = 10,
        /// <summary>D3DFVF_XYZB4</summary>
        PositionBlend4 = 12,
        /// <summary>D3DFVF_XYZB5</summary>
        PositionBlend5 = 14,     
        Normal = 16,
        PositionNormal = 18,
        /// <summary>D3DFVF_PSIZE</summary>
        PointSize = 32,
        Diffuse = 64,
        /// <summary>D3DFVF_SPECULAR</summary>
        Specular = 128,
        Texture1 = 256,
        Texture2 = 512,
        Texture3 = 768,
        Texture4 = 1024,
        Texture5 = 1280,
        Texture6 = 1536,
        Texture7 = 1792,
        Texture8 = 2048,
        //TextureCountMask = 3840,
        //LastBetaUByte4 = 4096,
        //PositionW = 16386, //D3DFVF_XYZW
        //PositionMask = 16398, //D3DFVF_POSITION_MASK
        //LastBetaD3DColor = 32768,
    }
    /* SHARPDX
        Texture0 = 0,
        None = 0,
        Reserved0 = 1,
        Position = 2,
        PositionRhw = 4,
        PositionBlend1 = 6,
        PositionBlend2 = 8,
        TextureCountShift = 8,
        PositionBlend3 = 10,
        PositionBlend4 = 12,
        PositionBlend5 = 14,
        Normal = 16,
        PointSize = 32,
        Diffuse = 64,
        Specular = 128,
        Texture1 = 256,
        Texture2 = 512,
        Texture3 = 768,
        Texture4 = 1024,
        Texture5 = 1280,
        Texture6 = 1536,
        Texture7 = 1792,
        Texture8 = 2048,
        TextureCountMask = 3840,
        LastBetaUByte4 = 4096,
        PositionW = 16386,
        PositionMask = 16398,
        LastBetaColor = 32768,
    */
    /// <summary>
    /// Flag : Describes values that define a vertex format used to describe the contents
    /// of vertices that are stored interleaved in a single data stream.
    /// </summary>
    public enum DeclarationMethod
    {
        Default = 0,
        PartialU = 1,
        PartialV = 2,
        CrossUV = 3,
        UV = 4,
        Lookup = 5,
        LookupPresampled = 6,
    }
    /// <summary>
    /// Enum int
    /// </summary>
    public enum DeclarationType
    {
        Float1 = 0,
        Float2 = 1,
        Float3 = 2,
        Float4 = 3,
        Color = 4,
        Ubyte4 = 5,
        Short2 = 6,
        Short4 = 7,
        UByte4N = 8,
        Short2N = 9,
        Short4N = 10,
        UShort2N = 11,
        UShort4N = 12,
        UDec3 = 13,
        Dec3N = 14,
        HalfTwo = 15,
        HalfFour = 16,
        Unused = 17,
    }
    /// <summary>
    /// Enum (max value = 13)
    /// </summary>
    public enum DeclarationUsage
    {
        Position = 0,
        BlendWeight = 1,
        BlendIndices = 2,
        Normal = 3,
        PointSize = 4,
        TexCoord = 5,
        Tangent = 6,
        Binormal = 7,
        TessellateFactor = 8,
        PositionTransformed = 9,
        Color = 10,
        //Fog = 11,
        //Depth = 12,
        //Sample = 13,
    }


    /// <summary>
    /// Flag : Defines supported usage types for the current resource.
    /// </summary>
    [Flags]
    public enum Usage
    {
        None = 0,
        RenderTarget = 1,
        //DepthStencil = 2,
        WriteOnly = 8,
        SoftwareProcessing = 16,
        //DoNotClip = 32,
        //Points = 64,
        //RTPatches = 128,
        //NPatches = 256,
        Dynamic = 512,
        AutoGenerateMipMap = 1024,
        //QueryDisplacementMap = 16384,
        //QueryLegacyBumpMap = 32768,
        //QuerySrgbRead = 65536,
        //QueryFilter = 131072,
        //QuerySrgbWrite = 262144,
        //QueryPostPixelShaderBlending = 524288,
        //QueryVertexTexture = 1048576,
        //QueryWrapAndMip = 2097152,
    }
  
    /// <summary>
    /// Enum : Defines the memory class that holds buffers for a resource.
    /// </summary>
    public enum Pool
    {
        Default = 0,
        Managed = 1,
        SystemMemory = 2,
        //Scratch = 3,
    }
    /// <summary>
    /// Enum
    /// </summary>
    public enum LightType
    {
        Point = 1,
        Spot = 2,
        Directional = 3,
    }
    /// <summary>
    /// Enum
    /// </summary>
    public enum SamplerState
    {
        AddressU = 1,
        AddressV = 2,
        AddressW = 3,
        BorderColor = 4,
        MagFilter = 5,
        MinFilter = 6,
        MipFilter = 7,
        MipMapLodBias = 8,
        MaxMipLevel = 9,
        MaxAnisotropy = 10,
        SrgbTexture = 11,
        ElementIndex = 12,
        DisplacementMapOffset = 13,
    }
    /// <summary>
    /// Enum
    /// </summary>
    public enum TextureStage
    {
        ColorOperation = 1,
        ColorArg1 = 2,
        ColorArg2 = 3,
        AlphaOperation = 4,
        AlphaArg1 = 5,
        AlphaArg2 = 6,
        BumpEnvironmentMat00 = 7,
        BumpEnvironmentMat01 = 8,
        BumpEnvironmentMat10 = 9,
        BumpEnvironmentMat11 = 10,
        TexCoordIndex = 11,
        BumpEnvironmentLScale = 22,
        BumpEnvironmentLOffset = 23,
        TextureTransformFlags = 24,
        ColorArg0 = 26,
        AlphaArg0 = 27,
        ResultArg = 28,
        Constant = 32,
    }
    /// <summary>
    /// Enum
    /// </summary>
    public enum TextureFilter
    {
        None = 0,
        Point = 1,
        Linear = 2,
        Anisotropic = 3,
        PyramidalQuad = 6,
        GaussianQuad = 7,
        ConvolutionMono = 8,
    }
    /// <summary>
    /// Enum
    /// </summary>
    public enum TextureOperation
    {
        Disable = 1,
        SelectArg1 = 2,
        SelectArg2 = 3,
        Modulate = 4,
        Modulate2X = 5,
        Modulate4X = 6,
        Add = 7,
        AddSigned = 8,
        AddSigned2X = 9,
        Subtract = 10,
        AddSmooth = 11,
        BlendDiffuseAlpha = 12,
        BlendTextureAlpha = 13,
        BlendFactorAlpha = 14,
        BlendTextureAlphaPM = 15,
        BlendCurrentAlpha = 16,
        Premodulate = 17,
        ModulateAlphaAddColor = 18,
        ModulateColorAddAlpha = 19,
        ModulateInvAlphaAddColor = 20,
        ModulateInvColorAddAlpha = 21,
        BumpEnvironmentMap = 22,
        BumpEnvironmentMapLuminance = 23,
        DotProduct3 = 24,
        MultiplyAdd = 25,
        Lerp = 26,
    }
    /// <summary>
    /// Flag
    /// </summary>
    [Flags]
    public enum TextureArgument
    {
        Diffuse = 0,
        Current = 1,
        Texture = 2,
        TFactor = 3,
        Specular = 4,
        Temp = 5,
        Constant = 6,
        SelectMask = 15,
        Complement = 16,
        AlphaReplicate = 32,
    }
    /// <summary>
    /// Flag
    /// </summary>
    [Flags]
    public enum TextureTransform
    {
        Disable = 0,
        Count1 = 1,
        Count2 = 2,
        Count3 = 3,
        Count4 = 4,
        Projected = 256,
    }

    /// <summary>
    /// Enum, have 2 constant
    /// </summary>
    public enum StreamSource
    {
        /// <summary>
        /// for semplicity i use this flag to reset instancing frequency
        /// </summary>
        Reset = -1,
        None = 0,
        NonIndexedData = 1,
        IndexedData = 1 << 30, // int 1073741824
        InstanceData = 2 << 30, // int -2147483648
    }

    public enum ShadeMode
    {
        Flat = 1,
        Gouraud = 2,
        Phong = 3,
    }
    public enum Compare
    {
        Never = 1,
        Less = 2,
        Equal = 3,
        LessEqual = 4,
        Greater = 5,
        NotEqual = 6,
        GreaterEqual = 7,
        Always = 8,
    }
    public enum BlendOperation
    {
        Add = 1,
        Subtract = 2,
        ReverseSubtract = 3,
        Minimum = 4,
        Maximum = 5,
    }
    public enum Blend
    {
        Zero = 1,
        One = 2,
        SourceColor = 3,
        InverseSourceColor = 4,
        SourceAlpha = 5,
        InverseSourceAlpha = 6,
        DestinationAlpha = 7,
        InverseDestinationAlpha = 8,
        DestinationColor = 9,
        InverseDestinationColor = 10,
        SourceAlphaSaturated = 11,
        Bothsrcalpha = 12,
        BothInverseSourceAlpha = 13,
        BlendFactor = 14,
        InverseBlendFactor = 15,
        SourceColor2 = 16,
        InverseSourceColor2 = 17,
    }

    public enum ImageFileFormat
    {
        BMP = 0,
        JPG = 1,
        TGA = 2,
        PNG = 3,
        DDS = 4,
        //Ppm = 5,
        //Dib = 6,
        //Hdr = 7,
        //Pfm = 8,
    }
    #endregion
}
