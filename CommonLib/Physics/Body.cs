﻿using System;
using System.Collections.Generic;
using System.Text;

using Engine.Maths;

namespace Engine.Physics
{
    public class RigidBody2D
    {
        static Vector2 Gravity = new Vector2(0, -10);

        Matrix3 transform;

        public Vector2 Position
        {
            get { return transform.Position;}
            set { transform.Position = value; }
        }


        public float Mass;
        public Vector2 Velocity;
        public Vector2 Acceleration;

        public RigidBody2D(Vector2 Position, Vector2 Velocity)
        {
            this.Position = Position;
            this.Velocity = Velocity;
        }


        public void IntegrateStep(float dt)
        {
            Position += Velocity * dt + (Acceleration + Gravity) * (dt * dt);
        }
    }
}
