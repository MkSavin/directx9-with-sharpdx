﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Engine.Tools
{
    /// <summary>
    /// Do your custum destructor and rebuild implementation example for graphic devicelost event
    /// By defualt the disposed value is true
    /// </summary>
    public abstract class DisposableResource : IDisposable
    {
        protected bool disposed;

        protected DisposableResource()
        {
            disposed = true;
        }
        ~DisposableResource()
        {
            Dispose(false);
        }
        /// <param name="disposing">
        /// If "true", the method has been called directly or indirectly by a user's code. Managed and unmanaged resources can be disposed.
        /// if "false",the method has been called by the runtime from inside the finalizer and you should not reference other objects.
        /// Only unmanaged resources can be disposed.
        /// </param>
        protected abstract void Dispose(bool disposing);
        /// <summary>
        /// Used to destroy the object and release any managed or unmanaged resources
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to
            // take this object off the finalization queue
            // and prevent finalization code for this object
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        public bool IsDisposed { get { return disposed; } }
    }
}
