﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Text;

using Engine.Maths;
using System.IO;
using System.Drawing.Drawing2D;

namespace Engine.Tools
{
    public static class UtilsMem
    {
        public static bool GetFlag(Byte value, Byte flag) { return (value & flag) != 0; }
        public static void SetFlag(ref Byte value, Byte flag, bool flagval)
        {
            if (flagval) value |= flag;
            else value &= (byte)~flag;
        }
        public static string GetBinaryString(Byte value)
        {
            char[] bits = new char[8];
            for (int i = 0; i < 8; i++) bits[7 - i] = (value & (byte)(1 << i)) != 0 ? '1' : '0';
            return new string(bits);
        }
        public static string GetBinaryString(Int16 value)
        {
            return GetBinaryString((UInt16)value);
        }
        public static string GetBinaryString(UInt16 value)
        {
            char[] bits = new char[16];
            for (int i = 0; i < 16; i++) bits[15 - i] = (value & (ushort)(1 << i)) != 0 ? '1' : '0';
            return new string(bits, 0, 8) + " " + new string(bits, 8, 8);
        }
        public static string GetBinaryString(Int32 value)
        {
            return GetBinaryString((UInt32)value);
        }
        public static string GetBinaryString(UInt32 value)
        {
            char[] bits = new char[32];
            for (int i = 0; i < 32; i++) bits[31 - i] = (value & (1 << i)) != 0 ? '1' : '0';

            string str = "";
            for (int i = 0; i < 4; i++) str += new string(bits, i * 8, 8) + " ";
            return str;
        }
        public static string GetBinaryString(Int64 value)
        {
            return GetBinaryString((UInt64)value);
        }
        public static string GetBinaryString(UInt64 value)
        {
            char[] bits = new char[64];
            for (int i = 0; i < 64; i++) bits[63 - i] = (value & (ulong)(1 << i)) != 0 ? '1' : '0';
            string str = "";
            for (int i = 0; i < 8; i++) str += new string(bits, i * 8, 8) + " ";
            return str;
        }
        public static string GetBinaryString(Uint128 value)
        {
            return GetBinaryString(value.high) + GetBinaryString(value.low);
        }
        public static string GetBinaryString<T>(T value) where T : struct
        {
            byte[] raw = RawSerialize(value);
            char[] bits = new char[raw.Length];
            for (int i = 0; i < raw.Length; i++) bits[i] = raw[i] != 0 ? '1' : '0';
            return new string(bits);
        }
        public static object RawDeserialize(byte[] rawData, int position, Type anyType)
        {
            int rawsize = Marshal.SizeOf(anyType);
            if (rawsize > rawData.Length) return null;
            IntPtr buffer = Marshal.AllocHGlobal(rawsize);
            Marshal.Copy(rawData, position, buffer, rawsize);
            object retobj = Marshal.PtrToStructure(buffer, anyType);
            Marshal.FreeHGlobal(buffer);
            return retobj;
        }
        public static byte[] RawSerialize(object anything)
        {
            int rawSize = Marshal.SizeOf(anything);
            IntPtr buffer = Marshal.AllocHGlobal(rawSize);
            Marshal.StructureToPtr(anything, buffer, false);
            byte[] rawDatas = new byte[rawSize];
            Marshal.Copy(buffer, rawDatas, 0, rawSize);
            Marshal.FreeHGlobal(buffer);
            return rawDatas;
        }
        public static string GetTextFile(string filename)
        {
            if (!File.Exists(filename)) throw new ArgumentNullException("Can't find file : " + filename.ToString());
            return File.ReadAllText(filename);
        }
    }

    /// <summary>
    /// Improve access to bitmap's pixels
    /// </summary>
    public class BitmapLock
    {
        Bitmap source = null;
        IntPtr Iptr = IntPtr.Zero;
        BitmapData bitmapData = null;
        InterpolationMode mode;


        public byte[] Pixels { get; set; }
        public int Depth { get; private set; }
        public int Width { get; private set; }
        public int Height { get; private set; }


        /// <summary>
        /// </summary>
        /// <param name="mode">Define how i access pixel if you use float parameter in GetPixel(float,float) </param>
        public BitmapLock(Bitmap source , InterpolationMode mode = InterpolationMode.Default)
        {
            this.source = source;
            this.mode = mode;
        }

        /// <summary>
        /// Lock bitmap data
        /// </summary>
        public void LockBits()
        {
            try
            {
                // Get width and height of bitmap
                Width = source.Width;
                Height = source.Height;

                // get total locked pixels count
                int PixelCount = Width * Height;

                // Create rectangle to lock
                System.Drawing.Rectangle rect = new System.Drawing.Rectangle(0, 0, Width, Height);

                // get source bitmap pixel format size
                Depth = System.Drawing.Bitmap.GetPixelFormatSize(source.PixelFormat);

                // Check if bpp (Bits Per Pixel) is 8, 24, or 32
                if (Depth != 8 && Depth != 24 && Depth != 32)
                {
                    throw new ArgumentException("Only 8, 24 and 32 bpp images are supported.");
                }

                // Lock bitmap and return bitmap data
                bitmapData = source.LockBits(rect, ImageLockMode.ReadWrite,
                                             source.PixelFormat);

                // create byte array to copy pixel values
                int step = Depth / 8;
                Pixels = new byte[PixelCount * step];
                Iptr = bitmapData.Scan0;

                // Copy data from pointer to array
                Marshal.Copy(Iptr, Pixels, 0, Pixels.Length);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Unlock bitmap data
        /// </summary>
        public void UnlockBits()
        {
            try
            {
                // Copy data from byte array to pointer
                Marshal.Copy(Pixels, 0, Iptr, Pixels.Length);
                // Unlock bitmap data
                source.UnlockBits(bitmapData);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get the color of the interpolated pixels
        /// </summary>
        public Color GetPixel(float x, float y)
        {
            x *= (Width - 1);
            y *= (Height - 1);

            if (x < Width - 1 && y < Height - 1)
            {
                return GetPixel((int)x, (int)y);
            }
            else
            {
                return GetPixel((int)x, (int)y);
            }

        }


        /// <summary>
        /// Get the color of the specified pixel
        /// </summary>
        public Color GetPixel(int x, int y)
        {
            if (Pixels == null) throw new ArgumentNullException("Pixels buffer not created, have you locked it ?");

            Color clr = Color.Empty;

            // Get color components count
            int cCount = Depth / 8;

            // Get start index of the specified pixel
            int i = ((y * Width) + x) * cCount;



            if (i > Pixels.Length - cCount)
                throw new IndexOutOfRangeException();

            if (Depth == 32) // For 32 bpp get Red, Green, Blue and Alpha
            {
                byte b = Pixels[i];
                byte g = Pixels[i + 1];
                byte r = Pixels[i + 2];
                byte a = Pixels[i + 3]; // a
                clr = Color.FromArgb(a, r, g, b);
            }
            if (Depth == 24) // For 24 bpp get Red, Green and Blue
            {
                byte b = Pixels[i];
                byte g = Pixels[i + 1];
                byte r = Pixels[i + 2];
                clr = Color.FromArgb(r, g, b);
            }
            if (Depth == 8)
            // For 8 bpp get color value (Red, Green and Blue values are the same)
            {
                byte c = Pixels[i];
                clr = Color.FromArgb(c, c, c);
            }
            return clr;
        }

        public Vector4 GetPixelV4(int x, int y)
        {
            if (Pixels == null) throw new ArgumentNullException("Pixels buffer not created, have you locked it ?");

            Vector4 clr = Vector4.Zero;

            // Get color components count
            int cCount = Depth / 8;

            // Get start index of the specified pixel
            int i = ((y * Width) + x) * cCount;

            if (i > Pixels.Length - cCount)
                throw new IndexOutOfRangeException();

            if (Depth == 32) // For 32 bpp get Red, Green, Blue and Alpha
            {
                byte b = Pixels[i];
                byte g = Pixels[i + 1];
                byte r = Pixels[i + 2];
                byte a = Pixels[i + 3]; // a
                clr = new Vector4(r, g, b, a);
            }
            if (Depth == 24) // For 24 bpp get Red, Green and Blue
            {
                byte b = Pixels[i];
                byte g = Pixels[i + 1];
                byte r = Pixels[i + 2];
                clr = new Vector4(r, g, b, 255);
            }
            if (Depth == 8)
            // For 8 bpp get color value (Red, Green and Blue values are the same)
            {
                byte c = Pixels[i];
                clr = clr = new Vector4(c, c, c, 255);
            }
            return clr;
        }


        /// <summary>
        /// Set the color of the specified pixel
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="color"></param>
        public void SetPixel(int x, int y, Color color)
        {
            if (Pixels == null) throw new ArgumentNullException("Pixels buffer not created, have you locked it ?");

            // Get color components count
            int cCount = Depth / 8;

            // Get start index of the specified pixel
            int i = ((y * Width) + x) * cCount;

            if (Depth == 32) // For 32 bpp set Red, Green, Blue and Alpha
            {
                Pixels[i] = color.B;
                Pixels[i + 1] = color.G;
                Pixels[i + 2] = color.R;
                Pixels[i + 3] = color.A;
            }
            if (Depth == 24) // For 24 bpp set Red, Green and Blue
            {
                Pixels[i] = color.B;
                Pixels[i + 1] = color.G;
                Pixels[i + 2] = color.R;
            }
            if (Depth == 8)
            // For 8 bpp set color value (Red, Green and Blue values are the same)
            {
                Pixels[i] = color.B;
            }
        }
        public void SetPixel(int x, int y, Color32 color)
        {
            if (Pixels == null) throw new ArgumentNullException("Pixels buffer not created, have you locked it ?");

            // Get color components count
            int cCount = Depth / 8;

            // Get start index of the specified pixel
            int i = ((y * Width) + x) * cCount;

            if (Depth == 32) // For 32 bpp set Red, Green, Blue and Alpha
            {
                Pixels[i] = color.B;
                Pixels[i + 1] = color.G;
                Pixels[i + 2] = color.R;
                Pixels[i + 3] = color.A;
            }
            if (Depth == 24) // For 24 bpp set Red, Green and Blue
            {
                Pixels[i] = color.B;
                Pixels[i + 1] = color.G;
                Pixels[i + 2] = color.R;
            }
            if (Depth == 8)
            // For 8 bpp set color value (Red, Green and Blue values are the same)
            {
                Pixels[i] = color.B;
            }
        }



        private Color linear(Color a, Color b , float fa)
        {
            float fb = 1 - fa;
            int A = (int)(a.A * fa + b.A * fb);
            int R = (int)(a.R * fa + b.R * fb);
            int G = (int)(a.G * fa + b.G * fb);
            int B = (int)(a.B * fa + b.B * fb);
            return Color.FromArgb(A, R, G, B); 
        }
    }
}
