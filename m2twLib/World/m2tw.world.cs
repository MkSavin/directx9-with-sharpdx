//------------------------------------------------------------------------
//
// Author      : johnwhile
// Date        : 2012
// Version     : 1.0
// Description : M2TW *.World file
//
//------------------------------------------------------------------------	
using System;
using System.Collections;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Drawing;
using Engine.Maths;
using Engine.Graphics;

namespace M2TWLib
{

    public struct sTableTwoRow
    {
        // please don't set manually these value "value_" , use the public "value" 
        internal sbyte anim_, group_, attribcount_, cpx_, gamemain_, attrib_;
        internal Int16 effect_, short0_, wallsection1_, wallsection2_, wallsection3_;

        // value with an encode and deconde method
        public Int32 nobj;
        public int cpx { get { return (int)cpx_; } set { cpx_ = (sbyte)value; } }
        public int attrib { get { return (int)attrib_; } set { attrib_ = (sbyte)value; } }
        public int gamemain { get { return (int)gamemain_; } set { gamemain_ = (sbyte)value; } }
        public int wallsection1 { get { return (int)wallsection1_; } set { wallsection1_ = (sbyte)value; } }
        public int wallsection2 { get { return (int)wallsection2_; } set { wallsection2_ = (sbyte)value; } }
        public int wallsection3 { get { return (int)wallsection3_; } set { wallsection3_ = (sbyte)value; } }
        public int group
        {
            get { return group_ / 4; }
            set { group_ = (sbyte)(value * 4); }
        }
        public int attribcount
        {
            get { return attribcount_ / 4; }
            set { attribcount_ = (sbyte)(value * 4); }
        }
        public int anim
        {
            get { return anim_ / 4; }
            set { anim_ = (sbyte)(value * 4); }
        }
        public int effect
        {
            get { if (effect_ < 0) return (int)effect_; else return (int)(effect_ + 1) / 16; }
            set { if (value < 0) effect_ = (short)(value * 16 - 1); }
        }

        public override string ToString() { return (String.Format("{0:D}] gm{1:D} a{2:D} e{3:D} c{4:D} g{5:D} atc{6:D} at{7:D} w1{8:D} w2{9:D} w3{10:D}", nobj, gamemain, anim, effect, cpx, group, attribcount, attrib, wallsection1, wallsection2, wallsection3)); }
    }
    
    public class TableTwo
    {
        internal static bool flag = true;

        public uint numobj;
        public sTableTwoRow[] row;

        public bool ReadBin(BinaryReader file)
        {
            if (flag) file.ReadUInt16(); // short0
            numobj = file.ReadUInt32();
            // check for safety , i must exit and stop reading
            if (numobj > 1000000)
            {
                throw new ArgumentOutOfRangeException("numobj", numobj, "Table Two count too big, exit for safety");
            }
            row = new sTableTwoRow[numobj];
            for (int i = 0; i < numobj; i++)
            {
                row[i].gamemain_ = file.ReadSByte();
                row[i].anim_ = file.ReadSByte();
                row[i].effect_ = file.ReadInt16();
                row[i].cpx_ = file.ReadSByte();
                row[i].group_ = file.ReadSByte();
                row[i].attribcount_ = file.ReadSByte();
                row[i].attrib_ = file.ReadSByte();
                row[i].wallsection1_ = file.ReadInt16();
                row[i].wallsection2_ = file.ReadInt16();
                row[i].wallsection3_ = file.ReadInt16();
                row[i].short0_ = file.ReadInt16();
                row[i].nobj = file.ReadInt32();
            }
            flag = false;
            return true;
        }
        public bool WriteBin(BinaryWriter file) { return true; }
        public override string ToString()
        {
            string str = String.Format("TAB2->{0:D} obj", numobj);
            //foreach (sTableTwoRow r in row) { str += r.ToString() + "\n"; }
            return str;
        }

    }

    // the IComparable Class can be used after for QuickSort algorithm
    public class BottomRow : IComparable
    {
        public ushort i, j;
        public int CompareTo(object obj)
        {
            // Return values:
            // <0: This instance smaller than obj.
            // =0: This instance occurs in the same position as obj.
            // >0: This instance larger than obj.
            BottomRow comp = (BottomRow)obj;
            if (this.i > comp.i) return 1;
            else if (this.i < comp.i) return -1;
            else if (this.j > comp.j) return 1;
            else if (this.j < comp.j) return -1;
            return 0;
        }
        public override string ToString() { return (i.ToString() + " , " + j.ToString()); }
    }
    
    public class BottomTable
    {
        internal static bool flag = true;
        public uint numrow = 0;
        public BottomRow[] row;

        public bool ReadBin(BinaryReader file)
        {
            if (flag) file.ReadUInt16();//short0
            numrow = file.ReadUInt32();
            if (numrow > 100000) throw new ArgumentOutOfRangeException("", "ERROR : BottomTable number too big, exit for safety");
            row = new BottomRow[numrow];
            int i = 0;
            while (i < numrow)
            {
                row[i] = new BottomRow();
                row[i].i = file.ReadUInt16();
                row[i].j = file.ReadUInt16();
                i++;
            }
            flag = false;
            return true;
        }
    }
    
    public class BoundingBoxRow
    {
        // Flags system for first loop
        internal static bool flag = true;

        // Stored values
        public Vector3 pivot;
        public AABBox box;
        public Int32[] info;

        // Reading function from binary
        public bool ReadBin(BinaryReader file)
        {
            int i;
            // store data into struct
            if (flag) file.ReadUInt16(); // short0
            pivot = Utils.ReadPoint3D(file);
            info = new Int32[17];
            for (i = 0; i < 17; info[i++] = file.ReadInt32()) ;
            if (flag) file.ReadUInt16(); // short0

            Vector3 pmax = Utils.ReadPoint3D(file);
            Vector3 pmin = Utils.ReadPoint3D(file);
            box = new AABBox(pmin, pmax);

            // turn off the first utilize of this typeof data
            flag = false;
            return true;
        }

        // Writing function from binary
        public bool WriteBin(BinaryWriter file) { return true; }


        //convert into 3dstudio is not immediat, you must resort the range pmin-pmax
        public void ConvertTo3ds()
        {
            throw new NotImplementedException();
            //updateRange();
        }

        // print a debug string
        public override string ToString() { return ("BBROW:\np:" + pivot.ToString() + "box : " + box.ToString()); }

    }
    
    public class BoundingBoxTable
    {
        internal static bool flag = true;

        public int maxlevel;
        public int classid;
        public float BigFloat;
        public uint numbox;
        public BoundingBoxRow[] bbox;

        public bool ReadBin(BinaryReader file)
        {
            file.ReadInt32(); // int1
            if (flag) file.ReadUInt16(); // short0
            maxlevel = file.ReadInt32();
            classid = file.ReadInt32();
            BigFloat = file.ReadSingle();
            if (flag) file.ReadUInt16(); // short0
            numbox = file.ReadUInt32();
            if (flag) file.ReadUInt16(); // short0


            // check for safety , i must exit and stop reading
            if (numbox > 10000)
            {
                throw new ArgumentOutOfRangeException("", "Bounding Box count too big, exit for safety");
            }

            // store sub-data into struct
            bbox = new BoundingBoxRow[numbox];
            for (int i = 0; i < numbox; i++)
            {
                bbox[i] = new BoundingBoxRow();
                bbox[i].ReadBin(file);
            }
            // turn off the first utilize of this typeof data
            flag = false;
            return true;
        }
        
        public bool WriteBin(BinaryWriter file) { return true; }

        public override string ToString()
        {
            string str = String.Format("BBOX->{0:D} bbox , max {1:D}", numbox, maxlevel);
            //foreach (BoundingBoxRow b in bbox) { str += b.ToString() + "\n"; }
            return str;
        }
    }
    
    public class Point2Data
    {
        internal static bool flag = true;

        public int point2ID = 35;
        public int Type;
        public int numverts;
        public Vector2[] v;

        public bool ReadBin(BinaryReader file)
        {
            if (flag) file.ReadUInt16(); //short0
            point2ID = file.ReadUInt16();
            if (flag) file.ReadUInt16(); //short1
            Globals.counterA = file.ReadUInt32();
            Type = (int)file.ReadUInt32();
            if (flag) file.ReadUInt16(); //short0

            numverts = (int)file.ReadUInt32();
            if (numverts > 65535) { throw new ArgumentOutOfRangeException("numverts", numverts, "Vertices number in point2 out of range, exit for safety"); }

            v = new Vector2[numverts];
            for (int i = 0; i < numverts; v[i++] = Utils.ReadPoint2D(file)) ;

            file.ReadInt32(); // int0
            if (file.ReadUInt16() != point2ID - 1) { throw new ArgumentException("point2ID", "Anomalous Point2IDminus, exit for safety"); }
            if (flag) file.ReadUInt16(); // short1
            Globals.counterA = file.ReadUInt32();
            if (file.ReadUInt16() != point2ID) { throw new ArgumentException(" point2ID", "Anomalous Point2ID, exit for safety"); }
            Globals.counterA = file.ReadUInt32();

            flag = false;
            return true;
        }
    }

    public class Point3Data
    {
        internal static bool flag = true;
        public int point3ID = 35;
        public int Type;
        public int numverts;
        public Vector3[] v;

        public bool ReadBin(BinaryReader file)
        {
            if (flag) file.ReadUInt16(); //short0
            point3ID = file.ReadUInt16();
            if (flag) file.ReadUInt16(); //short1
            Globals.counterA = file.ReadUInt32();
            Type = (int)file.ReadUInt32();
            if (flag) file.ReadUInt16(); //short0

            numverts = (int)file.ReadUInt32();
            if (numverts > 65535) { throw new ArgumentOutOfRangeException("numverts", numverts, "Vertices number in point3 out of range, exit for safety"); }

            v = new Vector3[numverts];

            for (int i = 0; i < numverts; v[i++] = Utils.ReadPoint3D(file)) ;


            file.ReadInt32(); // int0
            if (file.ReadUInt16() != point3ID - 1) { throw new ArgumentException("point3ID", "Anomalous Point3IDminus, exit for safety"); }
            if (flag) file.ReadUInt16(); // short1
            Globals.counterA = file.ReadUInt32();
            if (file.ReadUInt16() != point3ID) { throw new ArgumentException("point3ID", "Anomalous Point3ID, exit for safety"); }
            Globals.counterA = file.ReadUInt32();

            flag = false;
            return true;
        }
    }
    public class Point4Data
    {
        internal static bool flag = true;
        public int point4ID = 40;
        public int Type;
        public int numverts;
        
        public UInt32[] v;


        public bool ReadBin(BinaryReader file)
        {
            if (flag) file.ReadUInt16(); //short0
            point4ID = file.ReadUInt16();
            if (flag) file.ReadUInt16(); //short1
            Globals.counterA = file.ReadUInt32();
            Type = (int)file.ReadUInt32();
            if (flag) file.ReadUInt16(); //short0

            numverts = (int)file.ReadUInt32();
            if (numverts > 65535) { throw new ArgumentOutOfRangeException("", "ERROR : Vertices number in point4 out of range, exit for safety"); }

            v = new UInt32[numverts];
            for (int i = 0; i < numverts; v[i++] = file.ReadUInt32()) ;


            file.ReadInt32(); // int0
            if (file.ReadUInt16() != point4ID - 1) { throw new ArgumentException("", "ERROR : anomalous Point4IDminus, exit for safety"); }
            if (flag) file.ReadUInt16(); // short1
            Globals.counterA = file.ReadUInt32();
            if (file.ReadUInt16() != point4ID) { throw new ArgumentException("", "ERROR : anomalous Point4ID, exit for safety"); }
            Globals.counterA = file.ReadUInt32();

            flag = false;
            return true;
        }
    }
    public class FacesGroup
    {
        internal static bool flag = true;

        public ushort faceID = 27;
        public uint numfaces = 0;
        public string firstname = "";
        public string secondname = "";
        public bool show = true;
        public Face16[] face;

        public bool ReadBin(BinaryReader file)
        {
            if (flag) file.ReadUInt16(); // short0
            faceID = file.ReadUInt16();
            if (flag) file.ReadUInt16(); // short769 0x0103
            Globals.counterA = file.ReadUInt32();

            // these strings used in mesh variant
            firstname = Utils.ReadGameString(file);
            if (firstname == null) return false;
            secondname = Utils.ReadGameString(file);
            if (secondname == null) return false;

            if (flag) file.ReadUInt16(); // short0
            numfaces = file.ReadUInt32();

            if (numfaces > 65536) { throw new ArgumentOutOfRangeException("", "ERROR : Faces number out of range, exit for safety"); }

            face = new Face16[numfaces];
            for (int i = 0; i < numfaces; face[i++] = Utils.ReadFace(file)) ;

            show = file.ReadByte() == 0 ? true : false;
            file.ReadInt32(); //int0

            if (flag) file.ReadUInt32(); // int0
            if (file.ReadUInt16() != faceID - 1) { throw new ArgumentException("", "ERROR : anomalous FaceIDminus, exit for safety"); }
            if (flag) file.ReadUInt16(); // short1
            Globals.counterA = file.ReadUInt32();
            if (file.ReadUInt16() != faceID) { throw new ArgumentException("", "ERROR : anomalous FaceID, exit for safety"); }
            Globals.counterA = file.ReadUInt32();

            flag = false;
            return true;
        }
        public bool WriteBin(BinaryWriter file) { return true; }
        public override string ToString() { return base.ToString(); }
    }
    public class FacesData
    {
        internal static bool flag = true;

        public ushort facedataID = 23;
        public uint nummesh = 0;
        public FacesGroup[] mesh;

        public bool ReadBin(BinaryReader file)
        {
            int i = 0;
            if (flag) file.ReadUInt16(); // short0
            facedataID = file.ReadUInt16();
            if (flag) file.ReadUInt16(); // short1
            Globals.counterA = file.ReadUInt32();
            if (flag) file.ReadUInt16(); // short0

            nummesh = file.ReadUInt32();
            if (nummesh > 65535) { throw new ArgumentOutOfRangeException("nummesh",nummesh, "Meshes number out of range, exit for safety"); }
            mesh = new FacesGroup[nummesh];

            i = 0; while (i < nummesh)
            {
                mesh[i] = new FacesGroup();
                mesh[i].ReadBin(file);
                i++;
            }
            if (file.ReadUInt16() != facedataID - 1) { throw new ArgumentException("", "anomalous FacesDataIDminus, exit for safety"); }
            if (flag) file.ReadUInt16(); // short1
            Globals.counterA = file.ReadUInt32();
            if (file.ReadUInt16() != facedataID) { throw new ArgumentException("", "anomalous FacesDataID, exit for safety"); }
            Globals.counterA = file.ReadUInt32();

            flag = false;
            return true;
        }
        public bool WriteBin(BinaryWriter file) { return true; }
        public override string ToString() { return base.ToString(); }
    }

    public class VerticesData
    {
        internal static bool flag = true;
        public int numverts, numpoint2, numpoint3, numpoint4;
        public Point2Data[] point2data;
        public Point3Data[] point3data;
        public Point4Data[] point4data;

        public bool ReadBin(BinaryReader file)
        {
            int i = 0;
            numverts = (int)file.ReadUInt32();
            WorldFormat.totalverts += numverts;

            if (numverts > 65535) { throw new ArgumentOutOfRangeException("", "ERROR : Vertices number out of range, exit for safety"); }
            if (flag) file.ReadUInt16(); // short0

            //-------------------------------------
            // a set of vertices defined by 2 float
            //--------------------------------------
            numpoint2 = (int)file.ReadUInt32();
            if (numpoint2 > 16) { throw new ArgumentException("", "ERROR : Point2data number with unknow vertex definition, exit for safety"); }

            point2data = new Point2Data[numpoint2];
            i = 0; while (i < numpoint2)
            {
                point2data[i] = new Point2Data();
                if (!point2data[i].ReadBin(file)) return false;
                if (point2data[i].numverts != numverts) { throw new ArgumentException("", "ERROR : Point2data have a different numverts, exit for safety"); }
                i++;
            }
            if (flag) file.ReadUInt16(); // short0

            //-------------------------------------
            // a set of vertices defined by 3 float
            //--------------------------------------
            numpoint3 = (int)file.ReadUInt32();
            if (numpoint3 > 16) { throw new ArgumentException("", "ERROR : Point3data number with unknow vertex definition, exit for safety"); }
            point3data = new Point3Data[numpoint3];

            i = 0; while (i < numpoint3)
            {
                point3data[i] = new Point3Data();
                if (!point3data[i].ReadBin(file)) return false;
                if (point3data[i].numverts != numverts) { throw new ArgumentException("", "ERROR : Point3data have a different numverts, exit for safety"); }
                i++;
            }
            if (flag) file.ReadUInt16(); // short0

            //-------------------------------------
            // a set of vertices defined by 4 bytes
            //--------------------------------------
            numpoint4 = (int)file.ReadUInt32();
            if (numpoint4 > 16) { throw new ArgumentException("", "ERROR : Point4data number with unknow vertex definition, exit for safety"); }
            point4data = new Point4Data[numpoint4];

            i = 0; while (i < numpoint4)
            {
                point4data[i] = new Point4Data();
                if (!point4data[i].ReadBin(file)) return false;
                if (point4data[i].numverts != numverts) { throw new ArgumentException("", "ERROR : Point4data have a different numverts, exit for safety"); }
                i++;
            }
            file.ReadInt32(); // int0

            flag = false;
            return true;
        }
        public bool WriteBin(BinaryWriter file) { return true; }
        public override string ToString() { return base.ToString(); }
    }
    public class StructureBones
    {
        internal static bool flag = true;
        public string name = "";
        public int reference = 0;
        public bool ReadBin(BinaryReader file)
        {
            if (flag) file.ReadUInt16(); //short0
            name = Utils.ReadGameString(file);
            reference = file.ReadInt32();
            if (name == null) return false;
            flag = false;
            return true;
        }
        public bool WriteBin(BinaryWriter file) { return false; }
    }

    /// <summary>
    /// The second suddivision of big world file, was made for direct rendering mesh into Directx 
    /// </summary>
    public class Structure
    {
        internal static bool flag = true;
        public static bool flagmesh = false;

        public ushort strID = 20;
        public FacesData facesdata;
        public VerticesData verticesdata;
        public BoundingSphere bsphere;
        public StructureBones[] structbones;
        public uint numbones = 0;

        public bool ReadBin(BinaryReader file)
        {
            if (flag) file.ReadUInt16(); // short0
            strID = file.ReadUInt16();
            if (flag) file.ReadUInt16(); // short769 0x0103
            Globals.counterA = file.ReadUInt32();

            facesdata = new FacesData();
            facesdata.ReadBin(file);

            verticesdata = new VerticesData();
            verticesdata.ReadBin(file);

            

            if (flag) file.ReadUInt32(); // integer0
            if (flag & flagmesh) file.ReadUInt16(); // short0

            bsphere = new BoundingSphere();
            bsphere.ReadBin(file);

            if (flag) file.ReadUInt32(); // integer0

            uint numbones = file.ReadUInt32(); // int0;
            if (numbones > 1000) { throw new ArgumentOutOfRangeException("", "ERROR : Structure numbones too big, exit for safety"); }

            structbones = new StructureBones[numbones];
            
            int i = 0; while (i<numbones)
            {
                structbones[i] = new StructureBones();
                structbones[i].ReadBin(file);
                i++;
            }
            if (file.ReadUInt16() != strID - 1) { throw new ArgumentException("", "ERROR : anomalous StructureIDminus, exit for safety"); }
            if (flag) file.ReadUInt16(); // short1
            Globals.counterA = file.ReadUInt32();
            if (file.ReadUInt16() != strID) { throw new ArgumentException("", "ERROR : anomalous StructureID, exit for safety"); }
            Globals.counterB = file.ReadUInt32();

            flag = false;
            return true;
        }
        public bool WriteBin(BinaryWriter file) { return true; }

        public override string ToString() { return base.ToString(); }
    }
    public class TextureUnknow
    {
        internal static bool flag = true;
        public bool ReadBin(BinaryReader file)
        {
            file.ReadUInt32(); //int2
            if (flag) file.ReadUInt16(); // short0
            for (int i = 0; i < 8; i++) file.ReadSingle();
            flag = false;
            return true;
        }
    }
    public class TextureMaterial
    {
        internal static bool flag = true;
        public int matID = -1;

        public string lod = "buildinglod0";
        public uint numpath = 17; // always 17
        public string[] paths = new string[17];
        public Int32 type = 0x12C03F00; // i prefer integer instead 4 signed bytes
        public byte[] info = null;
        public float[] values = null;
        public TextureUnknow[] unknow = null;

        public bool ReadBin(BinaryReader file)
        {
            lod = Utils.ReadGameString(file);
            if (lod == null) return false;
            numpath = file.ReadUInt32();
            if (numpath != 17) { throw new ArgumentOutOfRangeException("", "ERROR : Material paths number not 17, exit for safety"); }

            paths = new string[numpath];
            for (int i = 0; i < numpath; i++)
            {
                paths[i] = Utils.ReadGameString(file);
                if (paths == null) return false;
            }
            file.ReadUInt32(); //int4
            file.ReadUInt32(); //int1
            file.ReadUInt32(); //int3
            type = file.ReadInt32();
            file.ReadUInt32(); //int4
            info = new byte[16];
            info = file.ReadBytes(16);

            uint numval = file.ReadUInt32(); //int9
            values = new float[numval];
            for (int i = 0; i < numval; values[i++] = file.ReadSingle()) ;

            uint numpar = file.ReadUInt32(); //int8
            unknow = new TextureUnknow[numpar];
            for (int i = 0; i < numpar; i++)
            {
                unknow[i] = new TextureUnknow();
                if (!unknow[i].ReadBin(file)) return false;
            }

            flag = false;
            return true;
        }
        public bool WriteBin(BinaryWriter file) { return true; }
        public override string ToString() { return (type.ToString("X") + " " + lod.ToString() + " " + paths[0].ToString()); }
    }
    public class TextureSection
    {
        internal static bool flag = true;
        public ushort textID = 49;
        public int matID = (int)Globals.counterA; // are used also as texture identifier
        public TextureMaterial material = null;

        public bool ReadBin(BinaryReader file)
        {
            if (flag) file.ReadUInt16(); // short0
            textID = file.ReadUInt16();
            if (flag) file.ReadUInt16(); // short1025 0x0104;

            matID = file.ReadInt32(); //contA

            if (matID > Globals.counterA)
            {
                // detect a new texture never used, but is possible with a optimizer function collapse some identical texture
                // with same same value, but considered in the file as two different texture
                material = new TextureMaterial();
                material.matID = matID;
                if (!material.ReadBin(file)) return false;
                if (file.ReadUInt16() != textID - 1) { throw new ArgumentException("", "ERROR : anomalous TextureIDminus, exit for safety"); }
                if (flag) file.ReadUInt16(); // short1
                Globals.counterA = file.ReadUInt32(); //contA+1
                if (file.ReadUInt16() != textID) { throw new ArgumentException("", "ERROR : anomalous TextureID, exit for safety"); }
                Globals.counterA = file.ReadUInt32(); //contA
            }
            else
            {
                // the game use a empty data because this structure has the same previous texture with the same matID
                if (file.ReadUInt16() != textID - 1) { throw new ArgumentException("", "ERROR : anomalous TextureIDminus, exit for safety"); }
                //not change Globals.counterA , this is countA+1
                file.ReadInt32();
            }
            flag = false;
            return true;
        }
        public bool WriteBin(BinaryWriter file) { return true; }
        public override string ToString() { return base.ToString(); }
    }

    /// <summary>
    ///  The first suddivision of big world file, the partitioning was made with Octree algorithm
    /// </summary>
    public class Complex
    {
        // only a summary
        public uint nummesh = 0;

        //
        internal static bool flag = true;
        public ushort cpxID = 16;
        public uint numstr = 0;
        public uint numtext = 0;
        public uint numsum = 0;
        public uint numlight = 0;
        public int bboxref = 0;
        public int prevObjCont = 0;
        public int nextObjCont = 0;
        public BoundingSphere bsphere;
        public Structure[] structure;
        public TextureSection[] texture;
        public UInt32[] light;
        public UInt32[] argantonio;

        public bool ReadBin(BinaryReader file)
        {
            //WorldFormat.setprogress(file.BaseStream.Position);
            int i = 0;
            //------------------------
            // Header section
            //------------------------
            if (flag) file.ReadUInt16(); // short1024
            file.ReadInt32(); //int1
            if (flag) file.ReadUInt16(); // short0
            cpxID = file.ReadUInt16();
            if (flag) file.ReadUInt16(); // short1
            Globals.counterA = file.ReadUInt32();
            if (flag) file.ReadUInt16(); // short0

            //------------------------
            // Structure section
            //------------------------

            numstr = file.ReadUInt32();
            if (numstr > 256) { throw new ArgumentOutOfRangeException("numstr",numstr, "Structure number out of range, exit for safety"); }
            structure = new Structure[numstr];
            for (i = 0; i < numstr; i++)
            {
                structure[i] = new Structure();
                structure[i].ReadBin(file);
                nummesh += structure[i].facesdata.nummesh;
            }
            //------------------------
            // Texture section
            //------------------------
            if (flag) file.ReadUInt16(); // short0
            numtext = file.ReadUInt32();
            if (numtext != numstr) { throw new ArgumentOutOfRangeException("numtext", numtext, "Texture number not equal to Structure number, exit for safety"); }
            texture = new TextureSection[numtext];
            for (i = 0; i < numtext; i++)
            {
                texture[i] = new TextureSection();
                if (!texture[i].ReadBin(file)) return false;
            }

            //------------------------
            // End section
            //------------------------
            bsphere = new BoundingSphere();
            bsphere.ReadBin(file);

            if (file.ReadUInt16() != cpxID - 1) { throw new ArgumentException("", "anomalous ComplexIDminus, exit for safety"); }
            if (flag) file.ReadUInt16(); // short1
            Globals.counterA = file.ReadUInt32();
            if (file.ReadUInt16() != cpxID) { throw new ArgumentException("", "anomalous ComplexID, exit for safety"); }
            Globals.counterB = file.ReadUInt32();
            prevObjCont = file.ReadInt32();
            nextObjCont = file.ReadInt32();
            file.ReadInt32(); //int1

            if (flag) file.ReadUInt16(); // short0
            numsum = file.ReadUInt32();
            if (numsum != numstr) { throw new ArgumentException("", "ERROR : Argantonio number not equal to Structure number, exit for safety"); }
            argantonio = new UInt32[numstr];
            for (i = 0; i < numstr; argantonio[i++] = file.ReadUInt32()) ;
            bboxref = file.ReadInt32();
            numlight = file.ReadUInt32();
            if (numlight > 1000000) { throw new ArgumentOutOfRangeException("", "ERROR : Light number too big, exit for safety"); }
            light = new UInt32[numlight];
            for (i = 0; i < numlight; light[i++] = file.ReadUInt32()) ;

            flag = false;
            return true;
        }
        public bool WriteBin(BinaryWriter file) { return true; }

        public override string ToString()
        {
            string str = String.Format("STR-> {0:D}\n", numstr);
            return str;
        }
    }


    /// <summary>
    ///  The main class that contain all geometry mesh
    /// </summary>
    public class MeshData
    {
        internal static bool flag = true;
        public uint nummesh = 0;
        public uint numcpx = 0;
        public Complex[] complex;
        public BoundingSphere bsphere;
        public string terrainpath = "";
        public string pathfindingpath = "";
        public string vegetationpath = "";

        public bool ReadBin(BinaryReader file)
        {
            if (flag) file.ReadUInt16(); // short0
            numcpx = file.ReadUInt32();
            // check for safety , in tabletwo cpx value are unsinged byte
            if (numcpx > 256)
            {
                throw new ArgumentOutOfRangeException("numcpx",numcpx, "Complex number out of range, exit for safety");
            }
            complex = new Complex[numcpx];
            for (int i = 0; i < numcpx; i++)
            {
                complex[i] = new Complex();
                complex[i].ReadBin(file);
                nummesh += complex[i].nummesh;
            }

            bsphere = new BoundingSphere();
            bsphere.ReadBin(file);

            if (flag) file.ReadUInt16(); // short0

            terrainpath = Utils.ReadGameString(file);
            pathfindingpath = Utils.ReadGameString(file);
            vegetationpath = Utils.ReadGameString(file);

            flag = false;
            return true;
        }
        public bool WriteBin(BinaryWriter file) { return true; }
        public override string ToString()
        {
            string str = String.Format("COMPLEX->{0:D}\n", numcpx);
            foreach (Complex cpx in complex)
                if (cpx != null) str += cpx.ToString();

            str += "terrain : \"" + terrainpath + "\n";
            str += "pathfind : \"" + pathfindingpath + "\n";
            str += "vegetation : \"" + vegetationpath + "\n";
            return str;
        }

    }
    public class BorderOfWall
    {
        internal static bool flag = true;
        public Vector2 knot1;
        public Vector2 knot2;
        public Matrix3x3 matrix;
        public float lenght;
        public uint type;

        public bool ReadBin(BinaryReader file)
        {
            if (flag) file.ReadUInt16(); //short0
            knot1 = Utils.ReadPoint2D(file);
            knot2 = Utils.ReadPoint2D(file);
            matrix = new Matrix3x3();
            matrix.ReadBinInverted(file);
            lenght = file.ReadSingle();
            type = file.ReadUInt32();
            flag = false;
            return true;
        }
        public override string ToString()
        {
            string str = "        Border\n";
            str += "        knot1 : " + knot1.ToString();
            str += "        knot2 : " + knot2.ToString();
            str += "        lenght: " + lenght.ToString() + "\n";
            str += "        type  : " + type.ToString() + "\n";
            return str;
        }
    }
    public class SegmentOfWall
    {
        internal static bool flag = true;
        public uint numborder = 0;
        public BorderOfWall[] border;
        public Vector2 avarage;

        public bool ReadBin(BinaryReader file)
        {
            if (flag) file.ReadUInt16(); //short0
            numborder = file.ReadUInt32();
            if (flag) file.ReadUInt16(); //short0

            if (numborder > 10000) throw new ArgumentOutOfRangeException("", "ERROR : Borders number out of range, exit for safety");
            border = new BorderOfWall[numborder];
            for (int i = 0; i < numborder; i++)
            {
                border[i] = new BorderOfWall();
                border[i].ReadBin(file);
            }
            avarage = Utils.ReadPoint2D(file);
            flag = false;
            return true;
        }
        public override string ToString()
        {
            string str = "    SegmentOfWall , num border :" + border.Length + "\n";
            for (int i = 0; i < border.Length; str += "      " + border[i++].ToString()) ;
            str += "    avarage : " + avarage.ToString() + "\n";
            return str;
        }
    }
    public class SplineOfWall
    {
        internal static bool flag = true;
        public uint numsegment = 0;
        public SegmentOfWall[] segment;


        public bool ReadBin(BinaryReader file)
        {
            if (flag) file.ReadUInt16(); //short0
            numsegment = file.ReadUInt32();
            if (flag) file.ReadUInt16(); //short0
            if (numsegment > 10000) throw new ArgumentOutOfRangeException("numsegment",numsegment, "Segments number out of range, exit for safety");
            segment = new SegmentOfWall[numsegment];
            for (int i = 0; i < numsegment; i++)
            {
                segment[i] = new SegmentOfWall();
                segment[i].ReadBin(file);
            }
            flag = false;
            return true;
        }
        public override string ToString()
        {
            string str = "  SplineOfWall , num segment :" + segment.Length + "\n";
            for (int i = 0; i < segment.Length; str += "  " + segment[i++].ToString()) ;
            return str;
        }
    }
    public class DamageOfWall
    {
        internal static bool flag = true;

        public int objref = -1;
        public uint numgroup = 0; // must match with Objects's damages
        public SplineOfWall[] spline;

        public bool ReadBin(BinaryReader file)
        {
            if (flag) file.ReadUInt16();//short0
            numgroup = file.ReadUInt32();
            if (flag) file.ReadUInt16(); //short0

            if (numgroup > 100) throw new ArgumentOutOfRangeException("", "ERROR : Groups number in Damageofwall out of range, exit for safety");
            spline = new SplineOfWall[numgroup];
            for (int i = 0; i < numgroup; i++)
            {
                spline[i] = new SplineOfWall();
                spline[i].ReadBin(file);
            }
            objref = file.ReadInt32();
            uint pad = file.ReadUInt32(); //integer0

            flag = false;
            return true;
        }
    }

    /// <summary>
    /// A list of points for this level , the points are stored as 2D rototraslation matrix
    /// <param name="knot"> the 2d matrix with atan2 format, checked in 3dstudio</param>
    /// </summary>
    public class DeploymentSoldierLevel
    {
        internal static bool flag = true;
        public uint numpoint = 0;
        public Matrix3x3[] knot;

        public bool ReadBin(BinaryReader file)
        {
            if (flag) file.ReadUInt16(); //short0
            numpoint = file.ReadUInt32();
            if (flag) file.ReadUInt16(); //short0
            if (numpoint > 100000) throw new ArgumentException("", "ERROR : Points number in Deploymentlevel too big, exit for safety");

            knot = new Matrix3x3[numpoint];
            for (int i = 0; i < numpoint; i++)
            {
                knot[i] = new Matrix3x3();
                knot[i].ReadAtan2(file);
                file.ReadSByte(); //byte1
            }
            flag = false;
            return true;
        }
        public override string ToString()
        {
            string str = "DeploymentSoldierLevel , npoints : " + numpoint + "\n";
            for (int i = 0; i < numpoint; str += knot[i++].ToString()) ;
            return str;
        }
    }

    /// <summary>
    /// Wall system.
    /// <param name="Height">Is the Global Height , use it to fix all points.</param>
    /// </summary>
    public class WallSection
    {
        internal static bool flag = true;
        public static bool isnetwork = false;

        public int simladder = 0;
        public int numdoor = 0;
        public int numdamage = 0;
        public int numlevel = 0;

        public int type = 1; // 1 in building 2,4 in network

        public int[] doorref;
        public DamageOfWall[] damage;
        public SplineOfWall summary;
        public DeploymentSoldierLevel[] deploysoldier;
        public float Height = 0f;

        public bool ReadBin(BinaryReader file)
        {
            if (flag) file.ReadUInt16();//short512
            if (flag) file.ReadUInt16();//short512

            simladder = file.ReadInt32();
            numdoor = (int)file.ReadUInt32(); // always 2


            if (numdoor>10) throw new ArgumentOutOfRangeException("numdoor",numdoor, "Doors ref in wallsection wrong, exit for safety");
            doorref = new int[numdoor];
            for (int i = 0; i < numdoor; doorref[i++] = file.ReadInt32()) ;

            if (flag & isnetwork) file.ReadUInt16();//short0

            //---------------------
            // Damage of Walls
            //---------------------
            Vector3 pivot1 = Utils.ReadPoint3D(file);
            Quaternion quat1 = Utils.ReadQuaternion(file);
            if (flag) file.ReadUInt16();//short0
            numdamage = (int)file.ReadUInt32();
            if (flag) file.ReadUInt16();//short512

            if (numdamage > 100000) throw new ArgumentOutOfRangeException("numdamage",numdamage, "Damageofwall number in wallsection too big, exit for safety");
             
            damage = new DamageOfWall[numdamage];
            for (int i = 0; i < numdamage; i++)
            {
                damage[i] = new DamageOfWall();
                damage[i].ReadBin(file);
            }
            Height = file.ReadSingle();

            if (Math.Abs(Height - pivot1.y) > 0.01) WorldFormat.debug = "Warning, the pivot1 and Height don't match, check it";
            //-------------------------------------------------
            // Sum of DamageOfWalls , is the global perimeter
            //-------------------------------------------------
            Vector2 pivot2 = Utils.ReadPoint2D(file);

            type = file.ReadInt32(); // 2,4 in network , 1 in building
            if (type > 10) throw new ArgumentOutOfRangeException("type", type, "wall section type > 10, exit for safety");

            summary = new SplineOfWall();
            summary.ReadBin(file);
            float par = file.ReadSingle();

            //---------------------------------------------------
            //	Soldiers Lines, a sort of Deployments Lines
            //---------------------------------------------------
            if (flag) file.ReadUInt16();//short0
            numlevel = (int)file.ReadUInt32();
            if (numlevel > 100) throw new ArgumentOutOfRangeException("numlevel",numlevel, "Deployment Solder's level too big, exit for safety");

            deploysoldier = new DeploymentSoldierLevel[numlevel];
            for (int i = 0; i < numlevel; i++)
            {
                deploysoldier[i] = new DeploymentSoldierLevel();
                deploysoldier[i].ReadBin(file);
            }
            flag = false;
            return true;
        }
    }
    public class LadderPoint
    {
        internal static bool flag = true;
        public Vector3 p;
        public float length;

        public bool ReadBin(BinaryReader file)
        {
            if (flag) file.ReadUInt16(); // short0
            p = Utils.ReadPoint3D(file);
            length = file.ReadSingle();
            flag = false;
            return true;
        }
        public override string ToString()
        {
            string str = p.ToString() + " " + length.ToString() + "\n";
            return str;
        }
    }
    public class Ladder
    {
        internal static bool flag = true;
        public uint idx = 0;
        public uint numdoor = 0;
        public uint[] doorref = null;
        public int type = 0;

        public uint numknot = 0;
        public LadderPoint[] knot = null;

        public int set = 2;
        public Vector3 exitA;
        public Vector3 exitB;
        public float width = -1f;


        public bool ReadBin(BinaryReader file)
        {
            if (flag) file.ReadUInt16(); // short0
            idx = file.ReadUInt32();
            numdoor = file.ReadUInt32();
            if (numdoor != 2) throw new ArgumentOutOfRangeException("numdoor",numdoor, "Doors number in ladder != 2, exit for safety");
            doorref = new uint[numdoor];
            for (int i = 0; i < numdoor; doorref[i++] = file.ReadUInt32()) ;
            Vector3 avarage = Utils.ReadPoint3D(file);
            type = file.ReadInt32();
            float sumlength = file.ReadSingle();

            if (flag) file.ReadUInt16(); // short0
            numknot = file.ReadUInt32();
            if (numknot > 1000) throw new ArgumentOutOfRangeException("numknot",numknot, "Knots number in ladder too big, exit for safety");
            knot = new LadderPoint[numknot];
            for (int i = 0; i < numknot; i++)
            {
                knot[i] = new LadderPoint();
                knot[i].ReadBin(file);
            }

            set = (int)file.ReadUInt32(); // always 2 , but not in modeltraversablenetwork
            if (set > 10) throw new ArgumentOutOfRangeException("set", set, "Set number in ladder >10 , exit for safety");
            width = file.ReadSingle();

            exitA = Utils.ReadPoint3D(file);
            exitB = Utils.ReadPoint3D(file);

            flag = false;
            return true;
        }
        public override string ToString()
        {
            string str = "   doors ";
            for (int i = 0; i < numdoor; str += doorref[i++].ToString() + " ") ;
            str += "\n   ";
            for (int i = 0; i < numknot; str += knot[i++].ToString()) ;
            return str;
        }
    }
    public class DoorReference
    {
        internal static bool flag = true;
        public uint index;
        public uint type;

        public bool ReadBin(BinaryReader file)
        {
            if (flag) file.ReadUInt16(); // short0
            index = file.ReadUInt32();
            type = file.ReadUInt32();
            flag = false;
            return true;
        }
    }
    public class Door
    {
        internal static bool flag = true;

        public uint idx = 0;
        public int type;
        public float r;
        public Matrix4 matrix;
        public uint doorID = 0;
        public uint numref = 0;
        public DoorReference[] reference;

        public bool ReadBin(BinaryReader file)
        {
            if (flag) file.ReadUInt16(); // short512
            idx = file.ReadUInt32();
            r = file.ReadSingle();

            matrix = new Matrix4();

            throw new Exception();
            //matrix.ReadBin2(file);

            doorID = file.ReadUInt32();
            int int1 = file.ReadInt32(); // integer1
            if (flag) file.ReadUInt16(); // short0
            numref = file.ReadUInt32();
            if (numref > 10) throw new ArgumentOutOfRangeException("numref",numref, "numReference in door too big, exit for safety");
            reference = new DoorReference[numref];
            for (int i = 0; i < numref; i++)
            {
                reference[i] = new DoorReference();
                reference[i].ReadBin(file);
            }
            type = file.ReadInt32();

            flag = false;
            return true;
        }
    }
    public class Pathfinding
    {
        private byte[] pixelbyte = null;
        public int Row = 0;
        public int Col = 0; 
       
        public Pathfinding(int Width , int Height)
        {
            Row = Width;
            Col = Height;
        }
        
        public void ReadBin(BinaryReader file)
        {
            uint numbytes = file.ReadUInt32();
            if (numbytes != (Row * Col * 6)) throw new ArgumentOutOfRangeException("Row x Col x 6 don't match with bytes number, exit for safety");
            pixelbyte = new byte[numbytes];
            pixelbyte = file.ReadBytes((int)numbytes);
        }
        /// <summary>
        /// return a bitmap image that rappresent the pathfinding map , two color are converted to White and Black
        /// that rappresent the Open and Close
        /// <param name="first">if true extract the first 3 byte of each pixels, is the main immage , if false extract a unknow pixels</param>
        /// </summary>
        private Bitmap DecodeImage(bool first)
        {
            Bitmap bmp = new Bitmap(Row, Col ,System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            Color c = new Color();
            //offset
            int i = first ? 0 : 3;
            for (int x = 0; x < bmp.Width; x++)
            {
                for (int y = 0; y < bmp.Height; y++)
                {
                    byte r = pixelbyte[i];
                    byte g = pixelbyte[i + 1];
                    byte b = pixelbyte[i + 2];
                    c = Color.FromArgb(r, g, b);

                    if (first & r == 0 & g == 63 & b == 0) c = Color.White;
                    if (first & r == 0 & g == 64 & b == 0) c = Color.Black;

                    if (!first & r == 63 & g == 0 & b == 63) c = Color.White;
                    if (!first & r == 64 & g == 0 & b == 64) c = Color.Black;

                    i += 6; //jump to next pixel
                    bmp.SetPixel(x, y, c);
                }
            }
            return bmp;
        }
        /// <summary>
        /// Overwrite the internal pixels array, get data from an external immage
        /// <param name="first">if true import as first image else second image</param>
        /// </summary>
        private void EncodeImage(Bitmap bmp, bool first)
        {
            //offset
            int i = first ? 0 : 3;
            Color c;
            this.Row = bmp.Width;
            this.Col = bmp.Height;
            int Size = Row * Col * 6;

            // write a all write map
            if (pixelbyte == null)
            {
                pixelbyte = new byte[Size];
                for (int j = 0; j < Size; j += 2)
                {
                    pixelbyte[j] = 0;
                    pixelbyte[j + 1] = 63;
                }
            }
            // add white pixels
            if (pixelbyte.Length < Size)
            {
                int last = pixelbyte.Length - pixelbyte.Length % 2;
                Array.Resize(ref pixelbyte, Size);
                for (int j = last; j < Size; j += 2)
                {
                    pixelbyte[j] = 0;
                    pixelbyte[j + 1] = 63;
                }
            }
            // cut the array , do nothing
            if (pixelbyte.Length > Size)
            {
                Array.Resize(ref pixelbyte, Size);
            }
            for (int x = 0; x < bmp.Width; x++)
            {
                for (int y = 0; y < bmp.Height; y++)
                {
                    byte r, g, b;
                    c = bmp.GetPixel(x, y);
                    r = c.R;
                    g = c.G;
                    b = c.B;
                    if (first & c.Equals(Color.White)) { r = 0; g = 63; b = 0; };
                    if (first & c.Equals(Color.Black)) { r = 0; g = 64; b = 0; };
                    if (!first & c.Equals(Color.White)) { r = 63; g = 0; b = 63; };
                    if (!first & c.Equals(Color.Black)) { r = 64; g = 0; b = 64; };
                    pixelbyte[i] = r;
                    pixelbyte[i + 1] = g;
                    pixelbyte[i + 2] = b;
                    i += 6; //jump to next pixel
                }
            }
        }
        /// <summary>
        /// First Bmp image
        /// </summary>
        public Bitmap ImageA { get { return DecodeImage(true); } set { EncodeImage(value, true); } }
        /// <summary>
        /// Second Bmp image
        /// </summary>
        public Bitmap ImageB { get { return DecodeImage(false); } set { EncodeImage(value, false); } }

        public static void SaveAsBitmap(Bitmap bmp, string filepath)
        {
            bmp.Save(filepath, System.Drawing.Imaging.ImageFormat.Bmp);
        }

    }
    public class Bridge
    {
        internal static bool flag = true;

        public int type = 0;
        public uint idx = 0;
        public int nobj = -1; // if <0 no objects linked, is a bridge
        public float width = 0;
        public float height = 0;
        public Vector3 pos;
        public uint numdoor = 0;
        public uint[] doorref = null;
        public Pathfinding map = null;

        public bool ReadBin(BinaryReader file)
        {
            int i;
            if (flag) file.ReadUInt16(); // short256
            type = file.ReadInt32();
            idx = file.ReadUInt32();
            nobj = file.ReadInt32();
            width = file.ReadSingle();
            height = file.ReadSingle();
            numdoor = file.ReadUInt32();
            if (numdoor > 10) throw new ArgumentOutOfRangeException("", "ERROR : num doors in pathfinding too big, exit for safety");
            doorref = new uint[numdoor];
            for (i = 0; i < numdoor; doorref[i++] = file.ReadUInt32()) ;
            if (flag) file.ReadUInt16(); // short512

            int Row = (int)file.ReadUInt32();
            int Col = (int)file.ReadUInt32();
            pos = Utils.ReadPoint3D(file);

            map = new Pathfinding(Row,Col);
            map.ReadBin(file);
            flag = false;
            return true;
        }


    }
    public class PathControl
    {
        internal static bool flag = true;
        public static bool isnetwork = false;

        public ushort pathID = 57;

        public WallSection[] wallSection;
        public Ladder[] ladder;
        public Door[] door;
        public Bridge[] pathfinding;

        public uint numwall = 0;
        public uint numladder = 0;
        public uint numdoor = 0;
        public uint numpath = 0;

        public bool ReadBin(BinaryReader file)
        {
            if (!isnetwork)
            {
                if (flag) file.ReadUInt16(); // short0
                pathID = file.ReadUInt16();
                if (flag) file.ReadUInt16(); // short513
                Globals.counterA = file.ReadUInt32();
            }
            if (flag & isnetwork) file.ReadUInt16(); // short512

            //-----------------------------------
            // Wall Section table
            //-----------------------------------
            if (flag) file.ReadUInt16(); // short0
            numwall = file.ReadUInt32();
            WallSection.isnetwork = isnetwork;

            if (numwall > 1000) { throw new ArgumentOutOfRangeException("numwall",numwall, "Walls number out of range, exit for safety"); }
            wallSection = new WallSection[numwall];
            for (int i = 0; i < numwall; i++)
            {
                wallSection[i] = new WallSection();
                wallSection[i].ReadBin(file);
            }

            //-----------------------------------
            // Ladders table
            //-----------------------------------
            if (flag) file.ReadUInt16(); // short0
            numladder = file.ReadUInt32();
            if (numladder > 1000) { throw new ArgumentOutOfRangeException("numladder",numladder, "Ladders number out of range, exit for safety"); }

            ladder = new Ladder[numladder];
            for (int i = 0; i < numladder; i++)
            {
                ladder[i] = new Ladder();
                ladder[i].ReadBin(file);
            }
            //-----------------------------------
            // Doors table
            //-----------------------------------
            if (flag) file.ReadUInt16(); // short0
            numdoor = file.ReadUInt32();
            if (numdoor > 1000) { throw new ArgumentOutOfRangeException("", "ERROR : Doors number out of range, exit for safety"); }
            door = new Door[numdoor];
            for (int i = 0; i < numdoor; i++)
            {
                door[i] = new Door();
                door[i].ReadBin(file);
            }
            //-----------------------------------
            // Gates&Bridges table (pathfinding)
            //-----------------------------------
            if (flag) file.ReadUInt16(); // short0
            numpath = file.ReadUInt32();
            if (numpath > 1000) { throw new ArgumentOutOfRangeException("", "ERROR : Pathfindings number out of range, exit for safety"); }
            pathfinding = new Bridge[numpath];
            for (int i = 0; i < numpath; i++)
            {
                pathfinding[i] = new Bridge();
                pathfinding[i].ReadBin(file);
            }

            // jump not used data
            if (isnetwork) return true;

            if (file.ReadUInt16() != pathID - 1) { throw new ArgumentException("", "ERROR : anomalous PathIDminus, exit for safety"); }
            if (flag) file.ReadUInt16(); // short1
            Globals.counterA = file.ReadUInt32(); //contA+1
            if (file.ReadUInt16() != pathID) { throw new ArgumentException("", "ERROR : anomalous PathID, exit for safety"); }
            Globals.counterA = file.ReadUInt32(); //contA

            if (flag) file.ReadUInt16(); // short0
            file.ReadInt32(); // integer -1

            flag = false;
            return true;
        }
        public bool WriteBin(BinaryWriter file) { return true; }


        public override string ToString()
        {
            string str = String.Format("PATHCTRL->{0:D}\n", 0);
            return str;
        }

    }

    public class StringPadComment
    {
        internal static bool flag = true;
        public string infoA = "";
        public string infoB = "";
        public bool ReadBin(BinaryReader file)
        {
            if (flag) file.ReadUInt16(); // short0
            infoA = Utils.ReadGameString(file);
            infoB = Utils.ReadGameString(file);
            if (infoA == null | infoB == null)
                return false;
            flag = false;
            return true;
        }
        public bool WriteBin(BinaryWriter file) { return false; }
        public override string ToString() { return (infoA + " : " + infoB); }
    }
    public class DetailComment
    {
        internal static bool flag = true;
        public uint numinfo = 0;
        public StringPadComment[] comment;

        public bool ReadBin(BinaryReader file)
        {
            if (flag) file.ReadUInt16(); // short0
            numinfo = file.ReadUInt32();
            if (numinfo > 1000) { throw new ArgumentOutOfRangeException("numinfo",numinfo, "ERROR : DetailComment info number out of range, exit for safety"); }
            comment = new StringPadComment[numinfo];
            for (int i = 0; i < numinfo; i++)
            {
                comment[i] = new StringPadComment();
                comment[i].ReadBin(file);
            }
            flag = false;
            return true;
        }
        public bool WriteBin(BinaryWriter file) { return false; }
        public override string ToString()
        {
            string str = "num info : " + numinfo + "\n";
            for (int i = 0; i < numinfo; str += comment.ToString()+"\n") ;
            return str;
        }
    }
    public abstract class GameObjectData
    {
        // use it to understand the class in your custom program language if GetType don't work
        public int type;
        public string name;
        public DetailComment comment;
        public bool ReadBaseBin(BinaryReader file)
        {
            name = Utils.ReadGameString(file);
            comment = new DetailComment();
            comment.ReadBin(file);
            return true;
        }

        public abstract bool ReadBin(BinaryReader file);
        public abstract bool WriteBin(BinaryWriter file);
    }
    public class PolyLineData : GameObjectData
    {
        internal static bool flag = true;
        public uint numknot = 0;
        public Vector3[] knot;

        public override bool ReadBin(BinaryReader file)
        {
            numknot = file.ReadUInt32();
            if (numknot > 100000) { throw new ArgumentOutOfRangeException("", "ERROR : PolyLineData's points number out of range, exit for safety"); }

            knot = new Vector3[numknot];
            for (int i = 0; i < numknot; knot[i++] = Utils.ReadPoint3D(file)) ;

            flag = false;
            return true;
        }
        public override bool WriteBin(BinaryWriter file) { return false; }
    }
    public class RectangleData : GameObjectData
    {
        internal static bool flag = true;

        public Vector2 p;
        public Matrix3x3 matrix;
        public Vector2 dim;

        public override bool ReadBin(BinaryReader file)
        {
            if (flag) file.ReadUInt16(); // short0;
            // se WallSection.flag==false allora c'ha un wallsystem , se true allora mai valutato
            if (flag & WallSection.flag) file.ReadUInt16(); // short0;
            p = Utils.ReadPoint2D(file);
            file.ReadUInt32(); //integer2
            matrix = new Matrix3x3();
            matrix.ReadBin(file);
            file.ReadUInt32(); //integer2
            dim = Utils.ReadPoint2D(file);

            flag = false;
            return true;
        }
        public override bool WriteBin(BinaryWriter file) { return false; }
    }
    public class PointData : GameObjectData
    {
        internal static bool flag = true;
        public Matrix4 matrix;

        public override bool ReadBin(BinaryReader file)
        {
            if (flag) file.ReadUInt16(); // short0
            matrix = Utils.ReadMatrix4x4(file);
            flag = false;
            return true;
        }
        public override bool WriteBin(BinaryWriter file) 
        { 
            return false;
        }

        public override string ToString()
        {
            return matrix.ToString(); 
        }
    }
    public class PlaneData : GameObjectData
    {
        internal static bool flag = true;
        public Vector3 normal;
        public float z;

        public override bool ReadBin(BinaryReader file)
        {
            if (flag) file.ReadUInt16(); // short0
            normal = Utils.ReadPoint3D(file);
            z = file.ReadSingle();

            flag = false;
            return true;
        }
        public override bool WriteBin(BinaryWriter file) { return false; }
    }
    public class MarkerData
    {
        internal static bool flag = true;
        public Matrix3x3 dir;

        public bool ReadBin(BinaryReader file)
        {
            if (flag) file.ReadUInt16();//short0
            dir = new Matrix3x3();
            dir.ReadBin(file);
            file.ReadUInt32();//integer0
            file.ReadUInt32();//integer0
            file.ReadUInt32();//integer16

            flag = false;
            return true;
        }
        public bool WriteBin(BinaryWriter file) { return false; }
    }

    public class GameObject
    {
        internal static bool flag = true;
        internal static BitArray flag_newtype = new BitArray(6,true);
        public GameObjectData itemobject;

        public bool ReadBin(BinaryReader file)
        {
            ushort type = file.ReadUInt16();
            switch (type)
            {
                case 1: itemobject = new PolyLineData(); break;
                case 2: itemobject = new RectangleData(); break;
                case 4: itemobject = new PointData(); break;
                case 5: itemobject = new PlaneData(); break;
                default: throw new ArgumentException("", "GameObject type not exist, exit for safety");
            }
            if (flag_newtype[type]) file.ReadUInt16(); //New type flag ; short1
            Globals.counterA = file.ReadUInt32();
            if (flag) file.ReadUInt16(); //First Loop flag short512

            itemobject.ReadBaseBin(file);
            itemobject.ReadBin(file);
            
            flag_newtype[type] = false;
            flag = false;
            return true;
        }
    }
    public class GameMain
    {
        internal static bool flag = true;
        public string name;
        public uint numupgrade = 0;
        public ushort[][] objreference;// utilize ushort can reduce by half the size
        public GameObject[][] upgradegroup;
        public uint index = 0;
        public uint numgroup;

        public bool ReadBin(BinaryReader file)
        {
            int i = 0;
            if (flag) file.ReadUInt16(); // short1024
            name = Utils.ReadGameString(file);
            if (flag) file.ReadUInt16(); // short0
            numupgrade = file.ReadUInt32();
            if (numupgrade > 100) { throw new ArgumentOutOfRangeException("numupgrade",numupgrade, "GameMain's upgrade number too big, exit for safety"); }

            objreference = new ushort[numupgrade][];

            i = 0; while (i < numupgrade)
            {
                int cont = file.ReadInt32();
                objreference[i] = new ushort[cont];
                int j = 0;
                while (j < cont)
                    objreference[i][j++] = (ushort)file.ReadUInt32();
                i++;
            }
            index = file.ReadUInt32();
            numgroup = file.ReadUInt32();

            if (numgroup > 100) { throw new ArgumentOutOfRangeException("numgroup", numgroup, "GameMain's numgroup number too big, exit for safety"); }

            upgradegroup = new GameObject[numgroup][];

            i = 0; while (i < numgroup)
            {
                uint numitem = file.ReadUInt32();
                if (numitem > 100) { throw new ArgumentOutOfRangeException("numitem",numitem, "GameMain->upgradegroup's numitems number too big, exit for safety"); }

                upgradegroup[i] = new GameObject[numitem];

                int j = 0; while (j < numitem)
                {
                    upgradegroup[i][j] = new GameObject();
                    upgradegroup[i][j].ReadBin(file);
                    j++;
                }
                i++;
            }
            flag = false;
            return true;
        }
    }
    public class GameSection
    {
        internal static bool flag = true;
        public uint numgameobj = 0;
        public uint numgamemain = 0;
        public uint numperimeter = 0;
        public uint numdefence = 0;
        public GameObject[] gameobject;
        public GameMain[] gamemain;
        public GameMain[] perimeter;
        public string[] gamemaindictionary;
        public MarkerData[] defencedir;


        public bool ReadBin(BinaryReader file)
        {
            int i = 0;
            //-------------------------
            // Game Objects
            //-------------------------
            if (flag) file.ReadUInt16(); // short0
            numgameobj = file.ReadUInt32();
            if (numgameobj > 1000) { throw new ArgumentOutOfRangeException("", "ERROR : GameObject number out of range, exit for safety"); }

            gameobject = new GameObject[numgameobj];
            i = 0; while (i < numgameobj)
            {
                gameobject[i] = new GameObject();
                gameobject[i].ReadBin(file);
                i++;
            }

            //-------------------------
            // Game Main
            //-------------------------
            if (flag) file.ReadUInt16(); // short0
            numgamemain = file.ReadUInt32();
            if (numgamemain > 1000) { throw new ArgumentOutOfRangeException("", "ERROR : GameMain number out of range, exit for safety"); }

            gamemain = new GameMain[numgamemain];
            i = 0; while (i < numgamemain)
            {
                gamemain[i] = new GameMain();
                gamemain[i].ReadBin(file);
                i++;
            }

            //-------------------------
            // Game Main Dictionary
            //-------------------------
            if (flag) file.ReadUInt16(); // short0
            uint numstr = file.ReadUInt32();
            gamemaindictionary = new string[numstr];
            i = 0; while (i < numstr)
            {
                gamemaindictionary[i] = Utils.ReadGameString(file);
                i++;
            }
            //-------------------------
            // Level's Perimeters
            //-------------------------
            if (flag) file.ReadUInt16(); // short512
            numperimeter = file.ReadUInt32();

            if (numperimeter > 1000) { throw new ArgumentOutOfRangeException("", "ERROR : GamePerimeter number out of range, exit for safety"); }
            perimeter = new GameMain[numperimeter];
            i = 0; while (i < numperimeter)
            {
                perimeter[i] = new GameMain();
                perimeter[i].ReadBin(file);
                i++;
            }
            //-------------------------
            // Defence Directions
            //-------------------------
            if (flag) file.ReadUInt16(); // short0
            numdefence = file.ReadUInt32();
            if (numdefence > 1000) { throw new ArgumentOutOfRangeException("", "ERROR : GameDefence number out of range, exit for safety"); }
            defencedir = new MarkerData[numdefence];
            i = 0; while (i < numdefence)
            {
                defencedir[i] = new MarkerData();
                defencedir[i].ReadBin(file);
                i++;
            }

            flag = false;
            return true;
        }
    }
    public class DamageSection
    {
        internal static bool flag = true;
        public uint numanim = 0;
        public DetailComment[] anim;
        public string animinstance = "";

        public bool ReadBin(BinaryReader file)
        {
            if (flag) file.ReadUInt16();//short0
            numanim = file.ReadUInt32();

            if (numanim > 1000) throw new ArgumentOutOfRangeException("numanim",numanim, "ERROR : DamageSection's animation number too big, exit for safety");
            anim = new DetailComment[numanim];

            for (int i = 0; i < numanim; i++)
            {
                if (i == 0 && flag) file.ReadUInt16();//short0
                anim[i] = new DetailComment();
                anim[i].ReadBin(file);
            }
            animinstance = Utils.ReadGameString(file);
            flag = false;
            return true;
        }
        public bool WriteBin(BinaryWriter file) { return false; }
    }

    public class EffectMatrixPad
    {
        internal static bool flag = true;
        public int objref;
        public int lightref;
        public bool ReadBin(BinaryReader file)
        {
            if (flag) file.ReadUInt16();//short0
            objref = file.ReadInt32();
            if (flag) file.ReadUInt16();//short0
            lightref = file.ReadInt32();
            flag = false;
            return true;
        }
        public override string ToString() { return ("obj " + objref.ToString() + " light " + lightref.ToString() + "\n"); }
    }
    public class EffectMatrix
    {
        internal static bool flag = true;
        public int bsref = 0;
        public float num = 0f;
        public byte onoff = 0;
        public PointData matrix;
        public EffectMatrixPad[] pad;

        public bool ReadBin(BinaryReader file)
        {
            if (flag) file.ReadUInt32();//integer0
            bsref = file.ReadInt32();
            matrix = new PointData();
            matrix.ReadBin(file);
            num = file.ReadSingle();
            onoff = file.ReadByte();
            if (flag) file.ReadUInt16();//short0
            uint numpad = file.ReadUInt32();
            if (numpad > 100000) throw new ArgumentOutOfRangeException("numpad", numpad, "EffectMatrixEntry's pad number too big, exit for safety");
            pad = new EffectMatrixPad[numpad];
            int i = 0;
            while (i < numpad)
            {
                pad[i] = new EffectMatrixPad();
                pad[i].ReadBin(file);
                i++;
            }
            flag = false;
            return true;
        }
        public override string ToString()
        {
            string str = "bottomstringkey : " + bsref.ToString() + "\n";
            str += matrix.ToString();
            foreach (EffectMatrixPad p in pad) { str += p.ToString(); }
            return str;
        }
    }
    public class EffectMatrixBlock
    {
        internal static bool flag = true;
        public uint numentry = 2;
        public EffectMatrix[] entry;

        public bool ReadBin(BinaryReader file)
        {
            if (flag) file.ReadUInt32();//integer0
            numentry = file.ReadUInt32();
            if (numentry > 100) throw new ArgumentOutOfRangeException("", "ERROR :EffectMatrixBlock's entry number too big, exit for safety");
            entry = new EffectMatrix[numentry];
            int i = 0;
            while (i < numentry)
            {
                entry[i] = new EffectMatrix();
                entry[i].ReadBin(file);
                i++;
            }
            flag = false;
            return true;
        }
        public override string ToString()
        {
            string str = "NUMMATRIX : " + numentry.ToString() + "\n";
            foreach (EffectMatrix m in entry) { str += m.ToString() + "\n"; }
            return str;
        }
    }
    public class EffectMatrixData
    {
        internal static bool flag = true;
        public uint numblock = 2;
        public EffectMatrixBlock[] block;

        public bool ReadBin(BinaryReader file)
        {
            if (flag) file.ReadUInt32();//integer0
            numblock = file.ReadUInt32();
            if (numblock > 2) throw new ArgumentOutOfRangeException("numblock",numblock, "EffectMatrix's block number > 2 not tested, exit for safety");
            block = new EffectMatrixBlock[numblock];
            int i = 0;
            while (i < numblock)
            {
                block[i] = new EffectMatrixBlock();
                block[i].ReadBin(file);
                i++;
            }
            flag = false;
            return true;
        }
    }
    public class EffectLight
    {
        internal static bool flag = true;
        public ushort lightID = 87;
        public Vector3 p;
        public float[] info1;
        public float[] info2;


        public bool ReadBin(BinaryReader file)
        {
            if (flag) file.ReadUInt16();//short0
            lightID = file.ReadUInt16();
            if (flag) file.ReadUInt16();//short0
            Globals.counterA = file.ReadUInt32();
            p = Utils.ReadPoint3D(file);

            info1 = new float[4];
            info2 = new float[2];
            for (int i = 0; i < 4; info1[i++] = file.ReadSingle()) ;
            file.ReadByte(); //byte1
            for (int i = 0; i < 2; info2[i++] = file.ReadSingle()) ;
            file.ReadByte(); //byte1
            file.ReadInt32(); //int3

            if (file.ReadUInt16() != lightID - 1) { throw new ArgumentException("", "ERROR : anomalous EffectLightIDminus, exit for safety"); }
            if (flag) file.ReadUInt16(); // short1
            Globals.counterA = file.ReadUInt32();
            if (file.ReadUInt16() != lightID) { throw new ArgumentException("", "ERROR : anomalous EffectLightID, exit for safety"); }
            Globals.counterA = file.ReadUInt32();

            flag = false;
            return true;
        }
    }
    public class EffectSection
    {
        internal static bool flag = true;
        public uint nummatrix = 0;
        public uint numlight = 0;
        public EffectMatrixData[] matrix;
        public EffectLight[] light;


        public bool ReadBin(BinaryReader file)
        {
            int i = 0;
            if (flag) file.ReadUInt16();//short0
            nummatrix = file.ReadUInt32();
            if (nummatrix > 1000) throw new ArgumentOutOfRangeException("", "ERROR : EffectSection's matrix number too big, exit for safety");
            matrix = new EffectMatrixData[nummatrix];
            while (i < nummatrix)
            {
                matrix[i] = new EffectMatrixData();
                matrix[i].ReadBin(file);
                i++;
            }
            if (flag) file.ReadUInt16();//short0
            numlight = file.ReadUInt32();
            if (numlight > 100000) throw new ArgumentOutOfRangeException("", "ERROR : EffectSection's light number too big, exit for safety");
            light = new EffectLight[numlight];
            i = 0;
            while (i < numlight)
            {
                light[i] = new EffectLight();
                light[i].ReadBin(file);
                i++;
            }
            flag = false;
            return true;
        }
        public bool WriteBin(BinaryWriter file) { return false; }
    }
    public struct sBottomStringEntry
    {
        public int key;
        public string label;
        public override string ToString() { return (String.Format("{0:D4} : *{1:F}*\n", key, label)); }
    }
    public class BottomString
    {
        public int numentry = 0;
        public sBottomStringEntry[] entry;

        public bool ReadBin(BinaryReader file)
        {
            uint length = file.ReadUInt32();
            if (length < 0 | length > 1000000) { throw new ArgumentOutOfRangeException("length", length, "BottomString too big, exit for safety"); }
            if (length == 0) return false; // can't be zero, the game crash but the code can continue reading
            
            //--------------------------- now filter string to rappresent correctly the data.
            int i , prev;
            byte[] bites = new byte[length];
            file.Read(bites, 0, (int)length);
            numentry = 0;
            // i must know the real size
            for (i = 0; i < length; i++)
                if (bites[i] == 0) numentry++;
            
            if (numentry==0 & length>0){ throw new ArgumentException("", "BottomString don't have zero bites , wrong string"); }

            entry = new sBottomStringEntry[numentry];

            i = 0;
            prev = 0;
            for (int cont = 0; cont < length; cont++)
                if (bites[cont] == 0)
                {
                    byte[] bin = new byte[cont - prev];
                    for (int j = prev; j < cont; bin[j - prev] = bites[j++]) ;
                    string text = Encoding.UTF8.GetString(bin);
                    entry[i] = new sBottomStringEntry();
                    entry[i].key = cont;
                    entry[i].label = text;
                    prev = cont + 1;
                    i++;
                }
            return true;
        }
        public override string ToString()
        {
            string str = "BOTTOMSTRING : \n";
            for (int i = 0; i < entry.Length; str += entry[i++].ToString()) ;
            return str;
        }

    }

    /// <summary>
    ///  WorldFormat class is a collection of method to read and manage the *.world file.
    ///  the methods clearflags() was called in ReadBin and WriteBin for safety, this because
    ///  the static bool flag are like globals value, if you write or read a singular class
    ///  this value are set to false every time.
    /// </summary>
    public class WorldFormat
    {
        /// <summary>
        ///  each class contain a flag system, was true for first calling
        /// </summary>
        internal static bool flag = true;
        /// <summary>
        ///  these string store the error message, in some case the code don't get error but a "memorandum"
        ///  was written when debugging
        /// </summary>
        public static string debug = "all ok";

        ~WorldFormat()
        {
            GC.SuppressFinalize(this);
        }

        /// <summary>
        ///  these string store the error message, in some case the code don't get error but a "memorandum"
        ///  was written when debugging
        /// </summary>
        public static int totalverts = 0;
        public BoundingBoxTable boundingbox;
        public TableTwo tabletwo;
        public MeshData meshdata;
        public PathControl pathcontrol;
        public GameSection gamesection;
        public DamageSection damageanim;
        public EffectSection effectsection;
        public BottomTable bottomtab1;
        public BottomTable bottomtab2;
        public BottomTable bottomtab3;
        public BottomString bottomstring;
        public EffectMatrixBlock globalcoord;

        /// <summary>
        ///  this function initialize a new set of flag, call it if you create a new instance of WorldFormat.
        /// </summary>
        public static void clearAllflags(bool status)
        {
            // flag used to work only with worldformat variant.
            Structure.flagmesh = false;
            WallSection.isnetwork = false;

            Globals.counterA = 0;
            Globals.counterB = 0;
            flag = status;

            TableTwo.flag = status;
            BottomTable.flag = status;

            BoundingBoxRow.flag = status;
            BoundingBoxTable.flag = status;
            Point2Data.flag = status;
            Point3Data.flag = status;
            Point4Data.flag = status;
            FacesGroup.flag = status;
            FacesData.flag = status;
            VerticesData.flag = status;
            StructureBones.flag = status;
            Structure.flag = status;
            TextureUnknow.flag = status;
            TextureMaterial.flag = status;
            TextureSection.flag = status;
            Complex.flag = status;
            MeshData.flag = status;

            BorderOfWall.flag = status;
            SegmentOfWall.flag = status;
            SplineOfWall.flag = status;
            DamageOfWall.flag = status;
            DeploymentSoldierLevel.flag = status;
            WallSection.flag = status;
            
            LadderPoint.flag = status;
            Ladder.flag = status;
            DoorReference.flag = status;
            Door.flag = status;
            Bridge.flag = status;
            PathControl.flag = status;

            StringPadComment.flag = status;
            DetailComment.flag = status;
            PolyLineData.flag = status;
            PointData.flag = status;
            RectangleData.flag = status;
            PlaneData.flag = status;
            MarkerData.flag = status;

            GameObject.flag = status;
            GameObject.flag_newtype.SetAll(status);
            GameMain.flag = status;
            GameSection.flag = status;
            DamageSection.flag = status;

            EffectMatrixPad.flag = status;
            EffectMatrix.flag = status;
            EffectMatrixBlock.flag = status;
            EffectMatrixData.flag = status;
            EffectLight.flag = status;
            EffectSection.flag = status;

        }

        /// <summary>
        ///  these functions return -1 if file was correctly processed, otherwise return the last file position for debugging.
        /// </summary>
        public long OpenFile(string filepath)
        {
            Int64 filepos = -1;
            using (BinaryReader file = new BinaryReader(File.OpenRead(filepath)))
            {
                try
                {
                    ReadBin(file);
                    filepos = -1;
                }
                catch (Exception ex)
                {
                    filepos = file.BaseStream.Position;
                    WorldFormat.debug = (string)ex.Message;
                    WorldFormat.debug += "\nStop at : " + filepos.ToString();
                    Console.WriteLine("!!!! ERROR !!!!\n" + WorldFormat.debug);
                }
                finally
                {
                    file.Close();
                }
            }
            return filepos;
        }
        /// <summary>
        ///  these functions return -1 if file was correctly processed, otherwise return the last file position for debugging.
        /// </summary>
        public long SaveFile(string filepath)
        {
            using (BinaryWriter file = new BinaryWriter(File.OpenWrite(filepath)))
                if (!WriteBin(file)) return file.BaseStream.Position;
            return -1;
        }

        /// <summary>
        ///  thise function read a binary data, at the beginning use the methods "clearflags" and place file seek at
        ///  start.
        /// </summary>
        public bool ReadBin(BinaryReader file)
        {
            clearAllflags(true);
            //filesize = file.BaseStream.Length;

            file.BaseStream.Seek(0, SeekOrigin.Begin);

            // header of world file
            string headerStr = Utils.ReadGameString(file);// = "serialization::archive";
            if (headerStr == null) { return false; }
            byte[] headerBytes = new byte[11];// = { 3, 4, 4, 8, 1, 0, 0, 0, 0, 0, 11 };
            file.Read(headerBytes, 0, 11);

            boundingbox = new BoundingBoxTable();
            boundingbox.ReadBin(file);

            tabletwo = new TableTwo();
            tabletwo.ReadBin(file);

            meshdata = new MeshData();
            meshdata.ReadBin(file);

            pathcontrol = new PathControl();
            pathcontrol.ReadBin(file);

            gamesection = new GameSection();
            gamesection.ReadBin(file);

            damageanim = new DamageSection();
            damageanim.ReadBin(file);

            effectsection = new EffectSection();
            effectsection.ReadBin(file);

            if (flag) file.ReadUInt16();//short0
            bottomtab1 = new BottomTable();
            bottomtab1.ReadBin(file);

            if (flag) file.ReadUInt16();//short0
            bottomtab2 = new BottomTable();
            bottomtab2.ReadBin(file);

            if (flag) file.ReadUInt16();//short0
            bottomtab3 = new BottomTable();
            bottomtab3.ReadBin(file);

            if (flag) file.ReadUInt16();//short0
            bottomstring = new BottomString();
            bottomstring.ReadBin(file);

            globalcoord = new EffectMatrixBlock();
            globalcoord.ReadBin(file);

            flag = false;
            return true;
        }
        /// <summary>
        ///  same of ReadBin
        /// </summary>
        public bool WriteBin(BinaryWriter file)
        {
            clearAllflags(true);
            return false;
        }

        /// <summary>
        /// This function convert the global mesh index into complex structure mesh indices to acces
        /// </summary>
        private void getlocalbyglobalm(ushort global_m, out byte cpx, out byte str, out ushort local_m)
        {
            cpx = 0;
            str = 0;
            local_m = 0;

            
            //300x faster but i'm not sure if it's correct
            uint mcont = meshdata.complex[0].nummesh-1;
            while (mcont < global_m & cpx < meshdata.numcpx) mcont += meshdata.complex[++cpx].nummesh;
            mcont = mcont - meshdata.complex[cpx].nummesh + meshdata.complex[cpx].structure[0].facesdata.nummesh;
            while (mcont < global_m & str < meshdata.complex[cpx].numstr) mcont += meshdata.complex[cpx].structure[++str].facesdata.nummesh;
            mcont -= meshdata.complex[cpx].structure[str].facesdata.nummesh;
            local_m = (ushort)(global_m - mcont - 1);
            
            /*
            int m = 0;
            for (cpx = 0; cpx < meshdata.numcpx; cpx++)
                for (str = 0; str < meshdata.complex[cpx].numstr; str++)
                    for (local_m = 0; local_m < meshdata.complex[cpx].structure[str].facesdata.nummesh; local_m++)
                    {
                        if (m == global_m)
                            return;
                        m++;
                    }
             */
        }
        

        /// <summary>
        /// this functions return a dictionary of Objects's address , was used with GetMesh(cpx,str,m).
        /// Is a jagger array : MINDEX = ushort[NUMOBJECTS][NUMDAMAGE] , ushort[0][2] = objects 0 , damage 3th
        /// </summary>
        public ushort[][] GetObjectsAddress()
        {
            int numcpx = (int)meshdata.numcpx;
            int numobj = (int)tabletwo.numobj;
            int nummesh = (int)meshdata.nummesh;
            BottomRow[] grouplist = bottomtab1.row;
            BottomRow[] showList = bottomtab2.row;
            BottomRow[] enumList = bottomtab3.row;
            
            // some check
            if (numcpx == 0) { WorldFormat.debug = "No complex !"; return null; }
            if (nummesh == 0) { WorldFormat.debug = "No meshes !"; return null; }
            if (numobj == 0) { WorldFormat.debug = "No objects !"; return null; }
            if (enumList.Length != nummesh) { WorldFormat.debug = "bottomtab3 don't math with real number of meshes"; return null; }
            if (showList.Length < nummesh) { WorldFormat.debug = "Debug case : there are the presence of mesh witout objects index, we must do more attention"; }
            for (int nobj = 0; nobj < numobj; nobj++)
                if (tabletwo.row[nobj].group != grouplist[nobj].j)
                {
                    WorldFormat.debug = "ERROR for obj " + nobj.ToString() + " the damage count in tab2 and bottomtab1 don't match";
                    return null;
                }


            // inizializzo le dimensioni dell'array degli indirizzi
            // ogni Address[nobj] = {5,6,7,...} contiene gli indici di global_m
            ushort[][] Address = new ushort[numobj][];
            for (int nobj = 0; nobj < numobj; nobj++)
            {
                int numgroup = tabletwo.row[nobj].group;
                Address[nobj] = new ushort[numgroup];
                for (int g = 0; g < numgroup; g++)
                    Address[nobj][g] = 0;
            }
            
            // devo localizzare la ricerca per ogni complex, non posso per tutto insieme,
            // for (int m = mstart ; m < mend ; m++); --> usato per l'array enumlist[]
            // m_cpx = m - mstart; --> usato per l'array meshdata.complex[]
            
            ushort[] m_mesh = new ushort[nummesh];
            ushort[] m_index = new ushort[nummesh];
            for (ushort m = 0; m < m_mesh.Length; m++)
            {
                m_mesh[m] = m;
                m_index[m] = m;
            }

            // this method find the Object's sequence compared with file's mesh sequence
            QuickSort sorting = new QuickSort();
            int mstart = 0;
            for (int cpx = 0; cpx < numcpx; cpx++)
            {
                int mend = mstart + (int)meshdata.complex[cpx].nummesh - 1;
                
                sorting.sort(enumList, m_index, mstart, mend);
                mstart = mend + 1;
            }

            // ... but this method convert it to position's index
            for (ushort m = 0; m < m_index.Length; m++)
                m_mesh[m_index[m]] = m;
            m_index = null;

            // use bottom table 2 to rebuild objs-groups sorting
            for (int nobj = 0; nobj < numobj; nobj++)
            {
                ushort moffset_1 = grouplist[nobj].i; //this is the moffset of FIRST DAMAGE GROUP
                ushort numgroup = grouplist[nobj].j; //all other are calculated with moffset_1+ (->#{1..ngroups})
                for (int g=0;g<numgroup;g++)
                {
                    int moffset_g = moffset_1+g;
                    moffset_g = showList[moffset_g].i;
                    if (moffset_g > nummesh) { WorldFormat.debug = "BottomTable2 : there are a over-range error, the mesh offset is wrong"; return null; }
                    ushort im = m_mesh[moffset_g];
                    Address[nobj][g] = im;

                    //check if algorithm is corret
                    /*
				    if enumList[moffsetg].i != MeshList[im].s do
				    (
					    print ("*** ERROR at nobj "+ nobj as string + " with current m pos "+im as string+" : the number of structure of bottomTable3 don't match with real structure...")
					    format "ERROR btab3 %!= real %\n" enumList[im] MeshList[im].s
					    --return undefined
				    )
                    */
                }
            }
            return Address;
        }

        /// <summary>
        /// this functions store all mesh into one big array , each mesh contain only the address like his
        /// complex, structure , local mesh , objects , damage group , ecc.... the trimesh value can be
        /// estracted with GetTriMesh(). If no ObjAddress was passed the code can't assign Objects index
        /// <param name="ObjAddress">Each objects address contain the global mesh index for all damage group.</param>
        /// </summary>
        public M2TWWorldNode[] GetAllMeshNode(ushort[][] ObjAddress)
        {
            int nummesh = (int)meshdata.nummesh;
            // some check
            if (nummesh == 0) { WorldFormat.debug = "No meshes !"; return null; }
            M2TWWorldNode[] meshList = new M2TWWorldNode[meshdata.nummesh];

            // assign complex, structure , local mesh , global mesh(not usefull) index
            ushort m = 0;
            for (byte cpx = 0; cpx < meshdata.numcpx; cpx++)
            {
                for (byte str = 0; str < meshdata.complex[cpx].numstr; str++)
                {
                    for (ushort localm = 0; localm < meshdata.complex[cpx].structure[str].facesdata.nummesh; localm++)
                    {
                        meshList[m] = new M2TWWorldNode();
                        meshList[m].c = cpx;
                        meshList[m].s = str;
                        meshList[m].mm = localm;
                        meshList[m].m = m;
                        meshList[m].matid = meshdata.complex[cpx].texture[str].matID;
                        m++;
                    }
                }
            }
            // assign objects and group index
            m = 0;
            if (ObjAddress == null) return meshList;
            for (short nobj = 0; nobj < ObjAddress.Length; nobj++)
            {
                for (byte g = 0; g < ObjAddress[nobj].Length; g++)
                {
                    m = ObjAddress[nobj][g];
                    meshList[m].o = nobj;
                    meshList[m].g = g;
                }
            }

            return meshList;
        }

        /// <summary>
        /// print GetAllMesh() result into txt file
        /// </summary>
        public string PrintMeshStructure()
        {
            ushort[][] ObjAddress = GetObjectsAddress();
            if (ObjAddress == null) return "";
            M2TWWorldNode[] MeshList = GetAllMeshNode(ObjAddress);

            StringBuilder stringa = new StringBuilder();

            stringa.AppendLine("+============================================================================================================+");
            stringa.AppendLine("|  A rappresentation of data from m2twlibrary.dll, it print the name used in max and the recalculate data    |");
            stringa.AppendLine("+============================================================================================================+");
            ushort m = 0;
            for (byte cpx = 0; cpx < meshdata.numcpx; cpx++)
            {
                stringa.AppendLine(String.Format("(COMPLEX {0:D})",cpx));
                for (byte str = 0;str<meshdata.complex[cpx].numstr;str++)
                {

                    for (ushort mm = 0; mm < meshdata.complex[cpx].structure[str].facesdata.nummesh; mm++)
                    {
                        stringa.Append(String.Format("\tOBJECT {0:D4} name in max : \"*.*\" \t", MeshList[m].o));
                        stringa.Append(String.Format("data: m{0:D} c{1:D} s{2:D}-mat.id{3:D} o{4:D} g{5:D})\n", m, MeshList[m].c, MeshList[m].s, MeshList[m].matid, MeshList[m].o, MeshList[m].g));
                        m++;
                    }
                    stringa.AppendLine(String.Format("[structure {0:D} , vertex cont (compress) = {1:D} ]", str, meshdata.complex[cpx].structure[str].verticesdata.numverts));
                }
            }
            return stringa.ToString();
        }

        /// <summary>
        /// return a list of texture utilized by game, if MeshNode[] was passed fit all MaterialID,
        /// <param name="optimize">if true try to search the same duffuse texture channel and collapse duplicate.</param>
        /// <param name="meshlist">meshlist was passed to store recalculated MaterialID. It can be NULL but you will lost information.</param>
        /// </summary>
        public TextureMaterial[] GetMaterials(bool optimize)
        {
            return GetMaterials(new M2TWWorldNode[0], optimize);
        }
        /// <summary>
        /// return a list of texture utilized by game, if MeshNode[] was passed fit all MaterialID,
        /// <param name="optimize">if true try to search the same duffuse texture channel and collapse duplicate.</param>
        /// <param name="meshlist">meshlist was passed to store recalculated MaterialID. It can be NULL but you will lost information.</param>
        /// </summary>
        public TextureMaterial[] GetMaterials(M2TWWorldNode[] meshlist, bool optimize)
        {
            // STEEP 1 : memorizzo tutti i dati in array
            int matCont = 0;
            for (byte cpx = 0; cpx < meshdata.numcpx; cpx++) for (byte str = 0; str < meshdata.complex[cpx].numtext; str++) matCont++;

            int[] matIDlist = new int[matCont];
            string[] diffuselist = new string[matCont];

            int i = 0;
            // this array store the material that will be deleted
            BitArray delete = new BitArray(matCont, false);
            for (byte cpx = 0; cpx < meshdata.numcpx; cpx++)
            {
                for (byte str = 0; str < meshdata.complex[cpx].numtext; str++)
                {
                    TextureSection texture = meshdata.complex[cpx].texture[str];
                    matIDlist[i] = texture.matID;
                    diffuselist[i] = "";
                    if (texture.material != null)
                        diffuselist[i] = Path.GetFileNameWithoutExtension(texture.material.paths[0]).ToString();
                    else
                        delete[i] = true; // ensure that the algorithm delete the null material
                    i++;
                }
            }

            // STEEP 2 : creo un array di correzzione di tutti gli indici
            int correction_cont = 0;
            BitArray visited = new BitArray(matCont, false);
            

            //if (false)
            for (i = 0; i < matCont; i++)
            {
                if (visited[i]) continue;

                int curr_id = matIDlist[i];
                string curr_diffuse = diffuselist[i];
                // match only same id
                for (int j = i + 1; j < matCont; j++)
                {
                    if (visited[j]) continue;
                    int id = matIDlist[j];
                    if (curr_id == id)
                    {
                        matIDlist[j] = correction_cont;
                        delete[j] = visited[j] = true;
                    }
                }
                visited[i] = true;
                matIDlist[i] = correction_cont;
                correction_cont++;
            }

            // match also the diffuse channel string , the correction array fix the new matIDlist
            if (optimize)
            {
                // helper array
                visited.SetAll(false);
                int[] correction = new int[correction_cont];
                for (i = 0; i < correction_cont; i++)
                    correction[i] = i;

                
                for (i = 0; i < matCont; i++)
                {
                    //The correction must be done for each item
                    int curr_id = matIDlist[i];
                    matIDlist[i] = correction[curr_id];

                    if (visited[i]) continue;
                    visited[i] = true;

                    // jump to next match
                    string curr_diffuse = diffuselist[i];
                    for (int j = i + 1; j < matCont; j++)
                    {
                        if (visited[j]) continue;
                        int id = matIDlist[j];
                        string diffuse = diffuselist[j];  
                        if (diffuse.Length > 0 & curr_diffuse.Length > 0 & curr_diffuse.CompareTo(diffuse) == 0)
                        {
                            correction[id] = matIDlist[i];
                            delete[j] = visited[j] = true;
                        }
                    }
                }

            }
            
            // fix result with a methods used also in the GetTriMesh algorithm
            visited.SetAll(false);
            for (i = 0; i < matCont; i++)
                visited[matIDlist[i]] = true;
            int maxcont = -1;
            for (i = 0; i < matCont; i++)
            {
                int id = matIDlist[i];
                int cont = 0;
                for (int j = 0; j < id; j++)
                    if (visited[j])
                        cont++;
                matIDlist[i] = cont;
                if (cont > maxcont) maxcont = cont;
            }
            maxcont++;

            // STEEP 3 : finally build the material list
            if (maxcont == 0)
            {
                WorldFormat.debug = "the result of material list is void";
                return new TextureMaterial[0];
            }
            TextureMaterial[] material = new TextureMaterial[maxcont];
            i = 0;
            maxcont = 0;
            for (byte cpx = 0; cpx < meshdata.numcpx; cpx++)
                for (byte str = 0; str < meshdata.complex[cpx].numtext; str++)
                {
                    TextureMaterial mat = meshdata.complex[cpx].texture[str].material;
                    if (!delete[i])
                        material[maxcont++] = mat;
                    i++;
                }


            
            // STEEP 4 : for each mesh of one structure assign the same faceid
            if (meshlist == null | meshlist.Length == 0)
                WorldFormat.debug = "MeshNode[] are void or null , all new material id will be lost";
            else
            {
                int m = 0;
                i = 0;
                for (byte cpx = 0; cpx < meshdata.numcpx; cpx++)
                    for (byte str = 0; str < meshdata.complex[cpx].numstr; str++)
                    {
                        for (ushort mm = 0; mm < meshdata.complex[cpx].structure[str].facesdata.nummesh; mm++)
                            meshlist[m++].matid = matIDlist[i];
                        i++;
                    }
            }
            /*
            for (i = 0; i < matCont; i++)
            {
                Console.Write("delete : " + (delete[i] ? 1 : 0).ToString() + "\t");
                Console.Write(matIDlist[i]+1+"\t");
                Console.WriteLine(diffuselist[i].ToString());
            }*/
            return material;
        }

        public override string ToString()
        {
            string str = "WORLD debug:\n";
            str += boundingbox.ToString() + "\n";
            str += tabletwo.ToString() + "\n";
            str += meshdata.ToString() + "\n";
            return str;
        }
    }
}

