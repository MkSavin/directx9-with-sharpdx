﻿

using System;
using System.Collections.Generic;
using System.Text;

using Engine.Graphics;
using Engine.Tools;
using Engine.Maths;

namespace M2TWLib
{
    /// <summary>
    ///  To work with each mesh in the Complex's structure, i need a class that rappresent all data, address ecc...
    ///  The derived class GameTriMesh was used also for other standard m2tw geometry like mesh files. 
    /// </summary>
    public class M2TWWorldNode : GameTriMesh
    {
        public byte c = 0; //range 0-255 by bottom table 3
        public byte s = 0; //range 0-255 by bottom table 3
        public ushort m = 0; //range 0-65535 20000 objects are like two towns , global m (outside structure)
        public ushort mm = 0; // is a local m index (inside structure)
        public short o = -1; //range 0-65535 20000 objects are very espansive , the -1 rappresent the NO-OBJECT's MESH
        public byte g = 0; //range 0-255 maximum 6 damage group
        public int matid = 0; //range 0-255 maximum 80 texture but i need int for other calculation
        //68byte for each mesh * 20000 = 1.36 MB

        /// <summary>
        ///  Like 3dstudio script, the function return a rappresentative name
        /// </summary>
        public string MeshFormattedName
        {
            get { return (secondname + ":" + firstname + ":" + ((show) ? 0 : 1)); }
            set
            {
                string[] parts = value.Split(':');
                secondname = parts[0].ToString();
                firstname = parts[1].ToString();
                show = (parts[2].CompareTo("0") == 0) ? true : false;
            }
        }

        public string Get3dstudioName
        {
            // i used a string like : "mesh-0001-cpx-01-str1-obj-001-1_".split = {"mesh","0001","cpx,"01","str1","obj","001","1"}
            // str index can't be used because depend of texture. ATTENTION get are used when import a world, set are used only when
            // build a new scene
            get { return (String.Format("mesh-{0:0000}-cpx{1:00}-str{2:0}-obj-{3:000}-{4:0}_", m + 1, c + 1, s + 1, o + 1, g + 1).ToString()); }
            set
            {
                char[] delimiters = { '-', '_' };
                string[] parts = value.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                m = Convert.ToUInt16(parts[1]);
                c = Convert.ToByte(parts[3]);
                o = Convert.ToInt16(parts[6]);
                g = Convert.ToByte(parts[7]);
            }
        }

        /// <summary>
        /// Unpacking structure to optain a standard DirectX format for each mesh, the WorldFormat must
        /// be valid, if you use other WorldFormat instance, the address don't match...
        /// </summary>
        public bool ExtractTriMesh(WorldFormat world)
        {
            if (c > world.meshdata.complex.Length - 1)
                return false;
            if (s > world.meshdata.complex[c].structure.Length - 1)
                return false;
            if (mm > world.meshdata.complex[c].structure[s].facesdata.mesh.Length - 1)
                return false;
            int MAXFACES = 65536;
            try
            {
                // le dimensioni massime che un indice faccia che può avere è 65536
                Structure structure = world.meshdata.complex[c].structure[s];
                FacesGroup faces = structure.facesdata.mesh[this.mm];
                VerticesData verticesdata = structure.verticesdata;
                int nfaces = (int)faces.numfaces;

                BitArray1D sorted = new BitArray1D(MAXFACES, false);


                this.firstname = faces.firstname;
                this.secondname = faces.secondname;
                this.show = faces.show;

                // first steep , copy the faces index and build the sorted array (bitarray for this goal)
                int i = 0;
                ushort range_max = 0;
                for (i = 0; i < nfaces; i++)
                {
                    int ii;
                    Face16 f = faces.face[i];
                    for (ii = 0; ii < 3; ii++)
                    {
                        sorted[f[ii]] = true;
                        if (range_max < f[ii])
                            range_max = f[ii];
                    }
                }

                //sorted.Length = maxsize+1;
                /*
                for (i = 0; i <= maxsize + 1; i++)
                   Console.Write(sorted[(int)i]? 1 : 0);
                Console.WriteLine("");
                */
                // second steep, find the new index. min and max is the range of utilized sorted value
                // with minvertsize you can reduce the search, this don't happen for hight compressed data
                ushort range_min = 0;
                while (!sorted[range_min++]) ;
                if (range_min == 0)
                {
                    Console.WriteLine("ERROR, possible all zero index of faces");
                    return false;
                }

                range_min--;

                this.face = new Face16[nfaces];

                for (i = 0; i < nfaces; i++)
                {
                    ushort j = 0;
                    Face16 globalface = faces.face[i];
                    this.face[i] = new Face16(0, 0, 0);
                    for (byte ii = 0; ii < 3; ii++)
                        for (j = range_min; j < globalface[ii]; j++)
                            if (sorted[j])
                                this.face[i][ii]++;
                }

                // cont the vertices utilized, will be used to inizialize array size
                int vertssize = 0;
                for (i = range_min; i <= range_max; i++)
                    if (sorted[i]) vertssize++;
                //Console.WriteLine("vertssize = " + vertssize + " range_min = " + range_min + " range_max = " + range_max);

                // thirs steep, build the local vertices array, and explicit conversion was made
                // to convert sPoint4B --> sPoint2D or Vector3
                /*
                    vertex = 0,
                    weight = 1,
                    boneid = 2,
                    normal = 3,
                    uvw = 4,
                    rgblight = 8,
                    rgbanim = 9,
                    tang = 10,
                    binorm = 11,
                    meshanim1 = 13,
                    meshanim2 = 14
                */
                if (range_max - range_min + 1 < vertssize)
                {
                    Console.WriteLine("vertssize too big from real size or sorted bitarray");
                    return false;
                }


                for (i = 0; i < verticesdata.numpoint2; i++)
                {
                    Point2Data data = verticesdata.point2data[i];
                    ushort idx = 0;
                    switch (data.Type)
                    {
                        case ((int)VertexType.uvw):
                            this.uvw = new Vector2[vertssize];
                            for (ushort j = range_min; j <= range_max; j++)
                                if (sorted[j])
                                    this.uvw[idx++] = data.v[j];
                            break;

                        case ((int)VertexType.weight):
                            this.weight = new Vector2[vertssize];
                            for (ushort j = range_min; j <= range_max; j++)
                                if (sorted[j])
                                    this.weight[idx++] = data.v[j];
                            break;
                        default: { Console.WriteLine("data.Type " + data.Type + " assignment in x2float are never tested in this case"); break; };
                    }
                }

                for (i = 0; i < verticesdata.numpoint3; i++)
                {
                    Point3Data data = verticesdata.point3data[i];
                    ushort idx = 0;
                    switch (data.Type)
                    {
                        case ((int)VertexType.vertex):
                            this.vertex = new Vector3[vertssize];
                            for (ushort j = range_min; j <= range_max; j++)
                                if (sorted[j]) this.vertex[idx++] = (Vector3)data.v[j];
                            break;

                        case ((int)VertexType.normal):
                            this.normal = new Vector3[vertssize];
                            for (ushort j = range_min; j <= range_max; j++)
                                if (sorted[j]) this.normal[idx++] = (Vector3)data.v[j];
                            break;

                        default: { Console.WriteLine("data.Type " + data.Type + " assignment in 3xfloat are never tested in this case"); break; };
                    }
                }
                for (i = 0; i < verticesdata.numpoint4; i++)
                {
                    Point4Data data = verticesdata.point4data[i];
                    ushort idx = 0;
                    switch (data.Type)
                    {
                        case ((int)VertexType.vertex):
                            this.vertex = new Vector3[vertssize];
                            for (ushort j = range_min; j <= range_max; j++)
                                if (sorted[j]) VertexPacker.UnPack(data.v[j], out this.vertex[idx++]);
                            break;
                        case ((int)VertexType.normal):
                            this.normal = new Vector3[vertssize];
                            for (ushort j = range_min; j <= range_max; j++)
                                if (sorted[j]) VertexPacker.UnPack(data.v[j], out this.normal[idx++]);
                            break;
                        case ((int)VertexType.uvw):
                            this.uvw = new Vector2[vertssize];
                            for (ushort j = range_min; j <= range_max; j++)
                                if (sorted[j]) VertexPacker.UnPack(data.v[j], out this.uvw[idx++]);
                            break;
                        case ((int)VertexType.boneid):
                            this.boneid = new VectorByte4[vertssize];
                            for (ushort j = range_min; j <= range_max; j++)
                                if (sorted[j]) VertexPacker.UnPack(data.v[j], out this.boneid[idx++]);
                            break;

                        // do nothing because these data aren't utilized 
                        case ((int)VertexType.binorm): break;
                        case ((int)VertexType.tang): break;
                        case ((int)VertexType.rgblight): break;
                        case ((int)VertexType.rgbanim): break;
                        case ((int)VertexType.meshanim1): break;
                        case ((int)VertexType.meshanim2): break;
                        default: { Console.WriteLine("data.Type " + data.Type + " assignment in 4xbyte are never tested in this case"); break; };
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(String.Format("crash for c{0:D} s{1:D} m{2:D}", c, s, mm));
                Console.WriteLine(e.ToString());
                return false;
            }
            return true;
        }

        public override string ToString()
        {
            return (String.Format("cpx:{0:D3} str:{1:D3} m:{2:D4} mm:{3:D4} obj:{4:D3} group:{5:D} mat:{6:D3}\n", c, s, m, mm, o, g, matid));
        }
    }


}
