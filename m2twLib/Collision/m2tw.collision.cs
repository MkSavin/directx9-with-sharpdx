using System;
using System.Collections;
using System.IO;
using System.Text;


using MatrixLibrary;
using Engine.Maths;
using Engine.Graphics;
namespace M2TWLib
{
    public class BoundingBoxTableRefRow
    {
        internal static bool flag = true;
        public int numobj = 0;
        public int[] objref;
        public int bboxId = 0;

        public bool ReadBin(BinaryReader file)
        {
            if (flag) file.ReadUInt16();//short0
            numobj = (int)file.ReadUInt32();
            if (numobj > 65536) throw new ArgumentOutOfRangeException("numobj", numobj, "Objects number out of range, exit for safety");
            objref = new int[numobj];
            for (int i = 0; i < numobj; i++)
                objref[i] = file.ReadInt32();
            bboxId = (int)file.ReadUInt32();
            flag = false;
            return true;
        }
    }
    /// <summary>
    /// Unlike Bounding box of worldformat class, there are also a table that contain all Objects index included in the
    /// box , in WorldFormat there aren't because the Complex structure define the Objects list
    /// </summary>
    public class BoundingBoxTableRef
    {
        internal static bool flag = true;
        public int numcpx = 0;
        public BoundingBoxTableRefRow[] complex;
        public bool ReadBin(BinaryReader file)
        {
            if (flag) file.ReadUInt16();//short0
            numcpx = (int)file.ReadUInt32();
            if (numcpx > 256) throw new ArgumentOutOfRangeException("numcpx", numcpx, "Complex number out of range, exit for safety");
            complex = new BoundingBoxTableRefRow[numcpx];
            for (int i = 0; i < numcpx; i++)
            {
                complex[i] = new BoundingBoxTableRefRow();
                complex[i].ReadBin(file);
            }
            flag = false;
            return true;
        }
        public bool WriteBin(BinaryWriter file) { return false; }
    }

    /// <summary>
    /// Fized size of data used for each children, there are the parent reference, objects cont and matrix
    /// (and also an unknow inverse matrix)
    /// </summary>
    public class ChildrenTransform
    {
        public int par = -1;
        public int nobj = -1;
        public Matrix4 transform = new Matrix4();
        public Matrix4 transform_inverse = new Matrix4();
        public BoundingSphere bsphere = new BoundingSphere();

        public void ReadBin(BinaryReader file)
        {
            par = file.ReadInt32();
            nobj = file.ReadInt32();
            transform = Utils.ReadMatrix4x4(file);
            transform_inverse = Utils.ReadMatrix4x4(file);
            bsphere.ReadBin(file);
        }
        public override string ToString()
        {
            string str = par + " " + nobj + " " + transform.ToString() + transform_inverse.ToString();
            return str;
        }

    }

    /// <summary>
    ///  Collection of Objects relative collision
    /// </summary>
    public class ChildrenSection
    {
        internal static bool flag = true;
        public int numchild = 0;
        public ChildrenTransform[] children;

        public bool ReadBin(BinaryReader file)
        {
            if (flag) file.ReadUInt16();//short0
            numchild = (int)file.ReadUInt32();
            if (numchild > 65536) throw new ArgumentOutOfRangeException("numchild", numchild, "Children number out of range ( < num Objects), exit for safety");

            children = new ChildrenTransform[numchild];
            for (int i = 0; i < numchild; i++)
            {
                children[i] = new ChildrenTransform();
                children[i].ReadBin(file);
            }
            flag = false;
            return true;
        }
        public bool WriteBin(BinaryWriter file) { return false; }
    }



    /// <summary>
    /// unknow data, there are some relations with faces index but not everytime , only face value
    /// was edited, all other set to 0
    /// </summary>
    public class ParentUnknowRow
    {
        public float[] floats = {0,0,0,0};
        public ushort first = 0;
        public ushort face;
        public void ReadBin(BinaryReader file)
        {
            for (int i = 0; i < 4; floats[i++] = file.ReadSingle()) ;
            first = file.ReadUInt16();
            face = file.ReadUInt16();
        }
    }

    /// <summary>
    /// Singular Parent mesh, is a list of isolated triangle, in 3dstudio a vertex weld method can be used to
    /// reduce the amount or duplicated vertices. Triangles was stored as 
    /// face[i] --> vertex[i*3+0] vertex[i*3+1] vertex[i*3+2]
    /// </summary>
    public class ParentMesh
    {
        internal static bool flag = true;
        public ushort parID = 16;
        public int numgroup = 0;
        public int numunknow = 0;
        public ParentUnknowRow[] unknow;
        public int numfaces = 0;
        public Vector3[] vertex;

        public bool ReadBin(BinaryReader file)
        {
            if (flag) file.ReadUInt16();//short0
            parID = file.ReadUInt16();
            if (flag) file.ReadUInt16();//short1
            Globals.counterA = file.ReadUInt32();
            if (flag) file.ReadUInt16();//short0

            numunknow = (int)file.ReadUInt32(); //only first are used then write
            if (numunknow > 256*256 | numunknow==0) throw new ArgumentOutOfRangeException("numunknow", numunknow, "Parents unknow number too big or 0, exit for safety");


            unknow = new ParentUnknowRow[numunknow];
            for (int i = 0; i < numunknow; i++)
            {
                unknow[i] = new ParentUnknowRow();
                unknow[i].ReadBin(file);
            }

            // triangles sections , i store as a list of Points3d , you must access with code
            // face[i] <=> vertex[i*3+0] vertex[i*3+1] vertex[i*3+2]
            if (flag) file.ReadUInt16();//short0
            numfaces = (int)file.ReadUInt32();
            if (flag) file.ReadUInt16();//short0
            if (numfaces > 65536 | numfaces==0) throw new ArgumentOutOfRangeException("numfaces", numfaces, "Parents faces number out of range or 0, exit for safety");

            vertex = new Vector3[numfaces * 3];
            for (int i = 0; i < numfaces * 3; vertex[i++] = Utils.ReadPoint3D(file)) ;

            if (flag) file.ReadUInt16();//short0
            if (file.ReadUInt16() != parID - 1) { throw new ArgumentException("", "anomalous ParIDminus, exit for safety"); }
            if (flag) file.ReadUInt16();//short1
            Globals.counterA = file.ReadUInt32();
            if (file.ReadUInt16() != parID) { throw new ArgumentException("", "anomalous ParID, exit for safety"); }
            Globals.counterA = file.ReadUInt32();
            flag = false;
            return true;
        }
        public bool WriteBin(BinaryWriter file) { return false; }
        
        /// <summary>
        ///  get a trimesh objects to build in your program, only face and vertex, not uvw and normals
        /// </summary>
        public GameTriMesh GetTriMesh()
        {
            if (numfaces == 0 | vertex.GetUpperBound(0) == 0)
                return null;
            GameTriMesh mesh = new GameTriMesh();
            mesh.face = new Face16[numfaces];
            mesh.vertex = new Vector3[numfaces * 3];

            for (int f = 0; f < numfaces; f++)
            {
                int i = f * 3 + 0;
                int j = f * 3 + 1;
                int k = f * 3 + 2;
                mesh.face[f] = new Face16((ushort)i, (ushort)j, (ushort)k);
                mesh.vertex[i] = vertex[i];
                mesh.vertex[j] = vertex[j];
                mesh.vertex[k] = vertex[k];
            }

            return mesh;
        }

    }
    
    /// <summary>
    ///  Parents's group must be the same or more than Objects's damage group, id > don't crash but isn't
    ///  a good idea.
    /// </summary>
    public class ParentGroup
    {
        internal static bool flag = true;
        public BoundingSphere bsphere;
        public int nummesh = 0;
        public ParentMesh[] mesh;


        public bool ReadBin(BinaryReader file)
        {
            if (flag) file.ReadUInt16();//short513
            Globals.counterA = file.ReadUInt32();
            if (flag) file.ReadUInt16();//short0
            bsphere = new BoundingSphere();
            bsphere.ReadBin(file);
            if (flag) file.ReadUInt16();//short0
            nummesh = (int)file.ReadUInt32();
            if (nummesh > 256 | nummesh==0) throw new ArgumentOutOfRangeException("nummesh", nummesh, "Parents damage number out of range or 0 , exit for safety");

            mesh = new ParentMesh[nummesh];
            for (int i = 0; i < nummesh; i++)
            {
                mesh[i] = new ParentMesh();
                mesh[i].ReadBin(file);
            }

            flag = false;
            return true;
        }
        public bool WriteBin(BinaryWriter file) { return false; }
    }

    /// <summary>
    ///  Collection of Parents , a list of local 3d mesh used for one or more children 
    /// </summary>
    public class ParentSection
    {
        internal static bool flag = true;
        public int numparent = 0;
        public ParentGroup[] parent;

        public bool ReadBin(BinaryReader file)
        {
            if (flag) file.ReadUInt16();//short0
            numparent = (int)file.ReadUInt32();
            if (numparent > 65536) throw new ArgumentOutOfRangeException("numparent", numparent, "Parents number out of range ( < num Objects), exit for safety");

            parent = new ParentGroup[numparent];
            for (int i = 0; i < numparent; i++)
            {
                parent[i] = new ParentGroup();
                parent[i].ReadBin(file);
            }

            flag = false;
            return true;
        }
        public bool WriteBin(BinaryWriter file) { return false; }
    }

    /// <summary>
    ///  Collection of Bounding Sphere, the void children (without parent) have a zero sphere
    /// </summary>
    public class BoundingSphereSummary
    {
        internal static bool flag = true;
        public int numobj = 0;
        public BoundingSphere[] bsphere;

        public bool ReadBin(BinaryReader file)
        {
            if (flag) file.ReadUInt16();//short0
            numobj = (int)file.ReadUInt32();
            if (numobj > 65536) throw new ArgumentOutOfRangeException("numobj", numobj, "Summary Objects number out of range ( == num Objects), exit for safety");
            if (flag) file.ReadUInt16();//short0

            bsphere = new BoundingSphere[numobj];
            for (int i = 0; i < numobj; i++)
            {
                bsphere[i] = new BoundingSphere();
                bsphere[i].ReadBin(file);
            }

            flag = false;
            return true;
        }
        public bool WriteBin(BinaryWriter file) { return false; }
    }
    /// <summary>
    ///  CollisionFormat class is a collection of method to read and manage the *.worldcollision file.
    ///  the methods clearflags() was called in ReadBin and WriteBin for safety, this because
    ///  the static bool flag are like globals value, if you write or read a singular class
    ///  this value are set to false every time.
    /// </summary>
    public class CollisionFormat
    {
        /// <summary>
        ///  each class contain a flag system, was true for first calling
        /// </summary>
        internal static bool flag = true;

        /// <summary>
        ///  these string store the error message, in some case the code don't get error but a "memorandum"
        ///  was written when debugging
        /// </summary>
        public string debug = "all ok";

        public static uint totalverts = 0;
        public BoundingBoxTable boundingbox;
        public BoundingBoxTableRef boundingboxref;
        public ChildrenSection children;
        public ParentSection parents;
        public BoundingSphereSummary summary;

        /// <summary>
        ///  this function initialize a new set of flag, call it if you create a new instance of WorldFormat.
        /// </summary>
        public static void clearAllflags(bool status)
        {
            flag = status;
            BoundingBoxRow.flag = status;
            BoundingBoxTable.flag = status;
            BoundingBoxTableRefRow.flag = status;
            BoundingBoxTableRef.flag = status;
            ChildrenSection.flag = status;
            ParentSection.flag = status;
            ParentGroup.flag = status;
            ParentMesh.flag = status;
            BoundingSphereSummary.flag = status;
            Globals.counterA = 0;
        }

        /// <summary>
        ///  these functions return -1 if file was correctly processed, otherwise return the last file position for debugging.
        /// </summary>
        public long OpenFile(string filepath)
        {
            Int64 filepos = -1;
            using (BinaryReader file = new BinaryReader(File.OpenRead(filepath)))
            {
                try
                {
                    ReadBin(file);
                    filepos = -1;
                }
                catch (Exception ex)
                {
                    filepos = file.BaseStream.Position;
                    debug = (string)ex.Message;
                    debug += "\nStop at : " + filepos.ToString();
                    Console.WriteLine("!!!! ERROR !!!!\n" + debug);
                }
                finally
                {
                    file.Close();
                }
            }
            return filepos;
        }
        /// <summary>
        ///  these functions return -1 if file was correctly processed, otherwise return the last file position for debugging.
        /// </summary>
        public long SaveFile(string filepath)
        {
            using (BinaryWriter file = new BinaryWriter(File.OpenWrite(filepath)))
                if (!WriteBin(file)) return file.BaseStream.Position;
            return -1;
        }

        /// <summary>
        ///  thise function read a binary data, at the beginning use the methods "clearflags" and place file seek at
        ///  start.
        /// </summary>
        public bool ReadBin(BinaryReader file)
        {
            clearAllflags(true);
            flag = true;
            file.BaseStream.Seek(0, SeekOrigin.Begin);

            // header of world file
            string headerStr = Utils.ReadGameString(file);// = "serialization::archive";
            if (headerStr == null) { return false; }
            byte[] headerBytes = new byte[5];// = { 3, 4, 4, 4, 8};
            file.Read(headerBytes, 0, 5);

            if (flag) file.ReadUInt16(); //short1
            Globals.counterA = file.ReadUInt32(); //start with 0
            if (flag) file.ReadUInt16(); //short1

            //bounding box
            boundingbox = new BoundingBoxTable();
            boundingbox.ReadBin(file);
            //bounding box reference
            boundingboxref = new BoundingBoxTableRef();
            boundingboxref.ReadBin(file);
            //children matrix section
            children = new ChildrenSection();
            children.ReadBin(file);
            //parents mesh regroupments
            parents = new ParentSection();
            parents.ReadBin(file);
            //summary or bounding spheres
            summary = new BoundingSphereSummary();
            summary.ReadBin(file);

            flag = false;
            return true;
        }
        /// <summary>
        ///  same of ReadBin
        /// </summary>
        public bool WriteBin(BinaryWriter file)
        {
            clearAllflags(true);
            return false;
        }

        public override string ToString()
        {
            string str = "WORLDCOLLISION debug:\n";
            return str;
        }
    }
}

