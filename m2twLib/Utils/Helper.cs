﻿

using System;
using System.Collections.Generic;
using System.Text;

using Engine.Maths;

namespace M2TWLib
{

    /// <summary>
    /// All game use it's strategy to compress the vertex data without lost to many precision
    /// </summary>
    public static class VertexPacker
    {
        const uint XMASK = 0xFF000000;
        const uint YMASK = 0x00FF0000;
        const uint ZMASK = 0x0000FF00;
        const uint WMASK = 0x000000FF;


        private static void unpack(UInt32 packed, out byte x, out byte y, out byte z, out byte w)
        {
            x = (byte)((packed & XMASK) >> 24);
            y = (byte)((packed & YMASK) >> 16);
            z = (byte)((packed & ZMASK) >> 8);
            w = (byte)(packed & WMASK);
        }


        public static void UnPack(UInt32 packed, out Color32 unpacked)
        {
            byte r,g,b,a;
            unpack(packed,out r,out g,out b,out a);
            unpacked = new Color32(r, g, b, a);
        }

        public static void UnPack(UInt32 packed, out VectorByte4 unpacked)
        {
            unpacked = new VectorByte4();
            unpack(packed, out unpacked.x, out unpacked.y, out unpacked.z, out unpacked.w);
        }

        public static void UnPack(UInt32 packed, out Vector3 unpacked)
        {
            byte x, y, z, w;
            unpack(packed, out x, out y, out z, out w);

            unpacked = new Vector3(
                x / 127.5f - 1f,
                y / 127.5f - 1f, 
                z / 127.5f - 1f);
        }
        public static void UnPack(VectorByte4 packed, out Vector3 unpacked)
        {
            unpacked = new Vector3(
                packed.z / 127.5f - 1f, 
                packed.y / 127.5f - 1f,
                packed.x / 127.5f - 1f);
        }
        public static void UnPack(UInt32 packed, out Vector2 unpacked)
        {
            byte x, y, z, w;
            unpack(packed, out x, out y, out z, out w);

            unpacked = new Vector2(
                y / 64.0f + z / 16384.0f - 2,
                w / 64.0f + x / 16384.0f - 2);
        }
        public static void UnPack(VectorByte4 packed, out Vector2 unpacked)
        {
            unpacked = new Vector2(
                packed.y / 64f + packed.z / 16384f - 2f,
                packed.w / 64f + packed.x / 16384f - 2f);
        }

    }
}
