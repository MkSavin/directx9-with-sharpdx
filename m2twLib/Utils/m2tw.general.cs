﻿using System;
using System.Collections;
using System.Text;
using System.Runtime.InteropServices;
using System.IO;
using System.Diagnostics;

using MatrixLibrary;
using Engine.Maths;
using Engine.Graphics;
namespace M2TWLib
{

    public static class Globals
    {
        public static uint counterA = 0;
        public static uint counterB = 0;
        public static readonly Matrix MaxCoordSys = new Matrix(new double[,] { { -1, 0, 0, 0 }, { 0, 0, 1, 0 }, { 0, -1, 0, 0 }, { 0, 0, 0, 1 } });
    }


    public static class Utils
    {
        private const int MAXLENGHT = 1000;

        public static string ReadGameString(BinaryReader file)
        {
            //use it only for little string with maxlenght 1000 , for bottom string build a new function
            uint length = file.ReadUInt32();
            if (length == 0) return "";
            if (length < 0 | length > MAXLENGHT) { throw new ArgumentOutOfRangeException("length", length, "Invalid String, exit for safety"); }
            byte[] str = new byte[length];
            file.Read(str, 0, (int)length);
            return Encoding.UTF8.GetString(str);
        }
        public static bool WriteGameString(string str, BinaryWriter file)
        {
            file.Write((uint)str.Length);
            byte[] bin = System.Text.Encoding.ASCII.GetBytes(str);
            file.Write(bin);
            return true;
        }
        
        public static void Swap<T>(ref T A, ref T B)
        {
            T temp = A;
            A = B;
            B = temp;
        }


        public static void ConvertTo3ds(ref Vector3 vector)
        {
            throw new NotImplementedException();
        }
        public static void ConvertTo3ds(ref Face16 face)
        {
            throw new NotImplementedException();
        }
        public static void ConvertTo3ds(ref Matrix4 mat)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Remember that i use column-major instead directx row-majour
        /// </summary>
        public static Matrix4 ReadMatrix4x4(BinaryReader file)
        {
            Matrix4 m = new Matrix4();
            m.m00 = file.ReadSingle();
            m.m10 = file.ReadSingle();
            m.m20 = file.ReadSingle();
            m.m30 = file.ReadSingle();

            m.m01 = file.ReadSingle();
            m.m11 = file.ReadSingle();
            m.m21 = file.ReadSingle();
            m.m31 = file.ReadSingle();

            m.m02 = file.ReadSingle();
            m.m12 = file.ReadSingle();
            m.m22 = file.ReadSingle();
            m.m32 = file.ReadSingle();

            m.m03 = file.ReadSingle();
            m.m13 = file.ReadSingle();
            m.m23 = file.ReadSingle();
            m.m33 = file.ReadSingle();

            return m;
        }
        public static Vector2 ReadPoint2D(BinaryReader file)
        {
            Vector2 p = new Vector2(0, 0);
            p.u = file.ReadSingle();
            p.v = file.ReadSingle();
            return p;
        }
        public static Vector3 ReadPoint3D(BinaryReader file)
        {
            Vector3 p = new Vector3(0, 0, 0);
            p.x = file.ReadSingle();
            p.y = file.ReadSingle();
            p.z = file.ReadSingle();
            return p;
        }
        public static VectorByte4 ReadPoint4B(BinaryReader file)
        {
            VectorByte4 p;
            VertexPacker.UnPack(file.ReadUInt32(), out p);
            return p;
        }
        public static Face16 ReadFace(BinaryReader file)
        {
            Face16 f = new Face16(0, 0, 0);
            f.I = file.ReadUInt16();
            f.J = file.ReadUInt16();
            f.K = file.ReadUInt16();
            return f;
        }
        public static Quaternion ReadQuaternion(BinaryReader file)
        {
            Quaternion q = new Quaternion(0, 0, 0, 0);
            q.x = file.ReadSingle();
            q.y = file.ReadSingle();
            q.z = file.ReadSingle();
            q.w = file.ReadSingle();
            return q;
        }
    }

    public enum VertexType : int
    {
        vertex = 0,
        weight = 1,
        boneid = 2,
        normal = 3,
        uvw = 4,
        unknow5 = 5,
        unknow6 = 6,
        unknow7 = 7,
        rgblight = 8,
        rgbanim = 9,
        tang = 10,
        binorm = 11,
        unknow12 = 12,
        meshanim1 = 13,
        meshanim2 = 14
    }

    /// <summary>
    /// found in http://www.remondo.net/quicksort-algorithm-example-csharp/
    /// The algorithm don't change the array passed as parameter but build an address array called "index" ,
    /// example if you have an array {c,b,a} the index was {2,1,0} this mean first item are in index[0] = 2 -> {c,b,a}[2] = a
	/// </summary>
    public class QuickSort
    {
        public void sort(IComparable[] array, ushort[] index, int left, int right)
        {
            int l = left;
            int r = right;
            int p = (left + right) / 2;

            // 1. Pick a pivot value somewhere in the middle.
            IComparable pivot = array[index[p]];

            // 2. Loop until pointers meet on the pivot.
            while (l <= r)
            {
                // 3. Find a larger value to the right of the pivot.
                //    If there is non we end up at the pivot.
                while (array[index[l]].CompareTo(pivot) < 0) l++;

                // 4. Find a smaller value to the left of the pivot.
                //    If there is non we end up at the pivot.
                while (array[index[r]].CompareTo(pivot) > 0) r--;

                // 5. Check if both pointers are not on the pivot.
                if (l <= r)
                {
                    // 6. Swap both values to the right side.
                    ushort swap = index[l];
                    index[l] = index[r];
                    index[r] = swap;

                    l++;
                    r--;
                }
            }
            // Here's where the pivot value is in the right spot

            // 7. Recursively call the algorithm on the unsorted array 
            //    to the left of the pivot (if exists).
            if (left < r) sort(array, index, left, r);

            // 8. Recursively call the algorithm on the unsorted array 
            //    to the right of the pivot (if exists).
            if (l < right) sort(array, index, l, right);

            // 9. The algorithm returns when all sub arrays are sorted.
        }
    }

    /// <summary>
    /// For some strategies used with maxscript i build the same function that convert a bitarray (used to store indices)
    /// to an array of indices , tipically ushort because never use index more than 65536
	/// </summary>  
    public class Functions
    {
        public static ushort[] BitArrayToArray<T>(BitArray bits)
        {
            int j = 0;
            int numelements = 0;
            int i = 0;
            // first get the numbers of "true"
            for (i = 0; i < bits.Length; i++)
                if (bits[i]) numelements++;

            // second convert array value with "true" position
            ushort[] array = new ushort[numelements];
            for (i = 0; i < bits.Length & j < numelements; i++)
                if (bits[i]) array[j++] = (ushort)i;
            return array;
        }
    }

}