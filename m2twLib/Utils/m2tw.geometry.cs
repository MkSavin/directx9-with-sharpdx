using System;
using System.Drawing;
using System.Collections.Generic;
using System.IO;
using MatrixLibrary;
using Engine.Maths;
using Engine.Graphics;

namespace M2TWLib
{
    public struct sAngle
    {
        private double _radians;
        public double radians { get { return _radians; } set { _radians = value; } }
        public double degree { get { return (_radians * (180.0 / Math.PI)); } set { _radians = Math.PI * value / 180.0; } }
        public override string ToString() { return (String.Format("{0:F3}d", degree)); }
    }

    /// <summary>
    ///  For game , the 2D space is XZ plane (in 3dstudio XY) so there aren't "height" value Y because
    ///  DirectX use left-handed coordinates. This Structure can also use to store UVW coordinate as u v value
    /// </summary>
    public struct sPoint2D
    {
        public float x;
        public float z;
        public float u { get { return x; } set { x = value; } }
        public float v { get { return z; } set { z = value; } }

        public sPoint2D(float[] b) { x = b[0]; z = b[1]; }
        public sPoint2D(float X, float Z) { x = X; z = Z; }

        public sPoint2D Clone() { return new sPoint2D(x, z); }
        public void CopyFrom(sPoint2D p) { this.x = p.x; this.z = p.z; }

        public float this[int i] { get { return (i == 0) ? x : z; } set { if (i == 0) x = value; else z = value; } }

        /// <summary>
        ///  For game , the 2D space is XZ plane (in 3dstudio XY) so there aren't "height" value (Y axis)
        ///  the explicit conversion set y coordinates to 0.
        /// </summary>
        public static explicit operator Vector3(sPoint2D p)
        { 
            return new Vector3(p.x, 0f, p.z); 
        }

        public static explicit operator VectorByte4(sPoint2D p)
        {
            float xx = 64 * (p.u + 2);
            byte b2 = (byte)xx;
            byte b3 = (byte)((xx - b2) * 256f);

            float yy = 64 * (p.v + 2);
            byte b4 = (byte)yy;
            byte b1 = (byte)((yy - b4) * 256f);

            return new VectorByte4(b1, b2, b3, b4);
        }

        /// <summary>
        ///  Attention, this conversion are used for 3d coordinates (XZ version), not for texture (UVW version)
        /// </summary>
        public void ConvertTo3ds()
        {
            //Vector3 p = (sPoint2D.x , 0 , sPoint2D.z) --to3ds--> (- sPoint2D.x , - sPoint2D.z , 0)  
            this.x = this.x * (-1);
            this.z = this.z * (-1);
        }
        public override string ToString() { return (String.Format("[{0:F2} {1:F2}]\n", u, v)); }
    }
    
    /// <summary>
    ///  For 2D rotation matrix the game use tipically a 2x2 float array , unlike standard 3x3matrix there are a simple
    ///  point2d for traslation and a atan2 function for angle rotation. The internal "matrix" property is a stardard 3x3
    ///  1 0 X
    ///  0 1 Z
    ///  0 0 1
    /// </summary>  
    public class Matrix3x3
    {
        public Matrix matrix = new Matrix(3, 3);

        public sPoint2D row1
        {
            get { return new sPoint2D((float)matrix[0, 0], (float)matrix[0, 1]); }
            set { matrix[0, 0] = value.x; matrix[0, 1] = value.z; }
        }
        public sPoint2D row2
        {
            get { return new sPoint2D((float)matrix[1, 0], (float)matrix[1, 1]); }
            set { matrix[1, 0] = value.x; matrix[1, 1] = value.z; }
        }
        public Vector2 pos
        {
            get { return new Vector2((float)matrix[0, 2], (float)matrix[1, 2]); }
            set { matrix[0, 2] = value.x; matrix[1, 2] = value.y; }
        }

        public Matrix3x3()
        {
            matrix = new Matrix(Matrix.Identity(3));
        }
        public Matrix3x3(bool debug)
        {
            for (int i = 0; i < 3; i++)
                for (int j = 0; j < 3; j++)
                    matrix[i, j] = i * 3 + j + 1;
        }
        /// <summary>
        /// the explicit conversion into 3d space isn't easy
        ///  1 0 X     1 * 0 X
        ///  0 1 Z --> * * * Y
        ///            0 * 1 Z
        /// </summary> 
        public static explicit operator Matrix4(Matrix3x3 M2)
        {
            throw new Exception();

            /*
            Matrix4 M3 = new Matrix4();
            Matrix4 m3 = M3.matrix;
            Matrix4 m2 = M2.matrix;
            // X row
            m3[0, 0] = m2[0, 0];
            m3[0, 1] = 0;
            m3[0, 2] = m2[0, 1];
            m3[0, 3] = m2[0, 2];
            // Z row
            m3[2, 0] = m2[1, 0];
            m3[2, 1] = 0;
            m3[2, 2] = m2[1, 1];
            m3[2, 3] = m2[1, 2];
            return M3;
             */
        }
        
        /// <summary>
        ///  this function is only for maxscript, 3dstudio don't have the matrix2 value.
        /// </summary> 
        public Matrix4 GetAsMatrix4x4() { return (Matrix4)this; }

        public void Rotate(sAngle angle)
        {
            Matrix Rz = new Matrix(Matrix.Identity(3));
            double radians = angle.radians;
            Rz[0, 0] = Math.Cos(radians);
            Rz[0, 1] = Math.Sin(radians);
            Rz[1, 0] = Math.Sin(radians) * -1;
            Rz[1, 1] = Math.Cos(radians);
            this.matrix = Rz * this.matrix;
        }
        
        
        /// <summary>
        ///  this version read 4 float , the first two are position, last two the atan2 coordinated
        /// </summary>  
        public void ReadAtan2(BinaryReader file)
        {
            Vector2 pos = Utils.ReadPoint2D(file);
            Vector2 vector = Utils.ReadPoint2D(file);
            sAngle angle = new sAngle();
            angle.radians = Math.Atan2(vector.y, vector.x);
            matrix = new Matrix(Matrix.Identity(3));
            this.Rotate(angle);
            
            this.pos = pos;
        }

        /// <summary>
        ///  this version read 4 float , is a standard 3x3 matrix :
        ///   a  0  b
        ///   0  1  0
        ///   c  0  d
        /// </summary>  
        public void ReadBin(BinaryReader file)
        {
            matrix = new Matrix(Matrix.Identity(3));
            matrix[0, 0] = file.ReadSingle();
            matrix[0, 1] = file.ReadSingle();
            matrix[1, 0] = file.ReadSingle();
            matrix[2, 1] = file.ReadSingle();
        }
        /// <summary>
        ///  this version read 4 float :
        ///  -a  0 +b
        ///   0  1  0
        ///  +c  0 -d
        ///  a and d are inverted, i don't understand why , but in 3dstudio the matrix is correct.
        /// </summary>  
        public void ReadBinInverted(BinaryReader file)
        {
            ReadBin(file);
            matrix[0, 0] *= -1;
            matrix[2, 1] *= -1;
        }


        public void ConvertTo3ds()
        {
            Vector2 pos = this.pos;
            
            //pos.ConvertTo3ds();
            Console.WriteLine(pos.ToString());

            Matrix4 M = (Matrix4)this;
            Utils.ConvertTo3ds(ref M);

            throw new Exception();
            //this.matrix = ((Matrix3x3)M).matrix;
            //this.pos = pos;
        }
        public override string ToString()
        {
            string str = "";
            for (int i = 0; i < 2; i++) // last identity row aren't used
            {
                for (int j = 0; j < 3; j++)
                    str += String.Format("{0:F2}  ", matrix[i, j]);
                str += "\n";
            }
            return str;
        }
    }    
    
    public class BoundingSphere
    {
        private float radius = 0;
        
        public Vector3 p = new Vector3(0,0,0);
        public float r { get { return radius; } set { radius = (value > 0 ? value : -value); } }
        
        public BoundingSphere Clone()
        {
            BoundingSphere bs = new BoundingSphere();
            bs.p = p;
            bs.r = r;
            return bs;
        }


        public void ReadBin(BinaryReader file)
        {
            this.p = Utils.ReadPoint3D(file);
            this.r = file.ReadSingle();
        }

        public void CopyFrom(BoundingSphere s)
        { 
            this.p = s.p; 
            this.radius = s.radius;
        }
        public void Compute(Vector3[] vertex)
        {
            int count = vertex.Length;
            int i;

            Vector3 xmin = new Vector3(0, 0, 0);
            Vector3 ymin = new Vector3(0, 0, 0);
            Vector3 zmin = new Vector3(0, 0, 0);
            Vector3 xmax = new Vector3(0, 0, 0);
            Vector3 ymax = new Vector3(0, 0, 0);
            Vector3 zmax = new Vector3(0, 0, 0);

            /* initialize for min/max compare */
            xmin.x = ymin.y = zmin.z = float.PositiveInfinity;
            xmax.x = ymax.y = zmax.z = float.NegativeInfinity;

            for (i = 0; i < count; i++)
            {
                Vector3 v = vertex[i];

                if (v.x < xmin.x) xmin = v; /* New xminimum point */
                if (v.x > xmax.x) xmax = v;
                if (v.y < ymin.y) ymin = v;
                if (v.y > ymax.y) ymax = v;
                if (v.z < zmin.z) zmin = v;
                if (v.z > zmax.z) zmax = v;
            }

            /* Set xspan = distance between the 2 points xmin & xmax (squared) */
            float dx = xmax.x - xmin.x;
            float dy = xmax.y - xmin.y;
            float dz = xmax.z - xmin.z;
            float xspan = dx * dx + dy * dy + dz * dz;

            /* Same for y & z spans */
            dx = ymax.x - ymin.x;
            dy = ymax.y - ymin.y;
            dz = ymax.z - ymin.z;
            float yspan = dx * dx + dy * dy + dz * dz;

            dx = zmax.x - zmin.x;
            dy = zmax.y - zmin.y;
            dz = zmax.z - zmin.z;
            float zspan = dx * dx + dy * dy + dz * dz;

            /* Set points dia1 & dia2 to the maximally separated pair */
            Vector3 dia1 = xmin;
            Vector3 dia2 = xmax; /* assume xspan biggest */
            float maxspan = xspan;

            if (yspan > maxspan)
            {
                maxspan = yspan;
                dia1 = ymin;
                dia2 = ymax;
            }

            if (zspan > maxspan)
            {
                dia1 = zmin;
                dia2 = zmax;
            }


            /* dia1,dia2 is a diameter of initial sphere */
            /* calc initial center */
            Vector3 mCenter = new Vector3(0, 0, 0);
            float mRadius2, mRadius;

            mCenter.x = (dia1.x + dia2.x) * 0.5f;
            mCenter.y = (dia1.y + dia2.y) * 0.5f;
            mCenter.z = (dia1.z + dia2.z) * 0.5f;
            /* calculate initial radius**2 and radius */
            dx = dia2.x - mCenter.x; /* x component of radius vector */
            dy = dia2.y - mCenter.y; /* y component of radius vector */
            dz = dia2.z - mCenter.z; /* z component of radius vector */
            mRadius2 = dx * dx + dy * dy + dz * dz;
            mRadius = (float)Math.Sqrt(mRadius2);

            /* SECOND PASS: increment current sphere */

            for (i = 0; i < count; i++)
            {
                Vector3 v = vertex[i];
                dx = v.x - mCenter.x;
                dy = v.y - mCenter.y;
                dz = v.z - mCenter.z;
                float old_to_p_sq = dx * dx + dy * dy + dz * dz;

                if (old_to_p_sq > mRadius2) 	/* do r**2 test first */
                { 	/* this point is outside of current sphere */
                    float old_to_p = (float)Math.Sqrt(old_to_p_sq);
                    /* calc radius of new sphere */
                    mRadius = (mRadius + old_to_p) * 0.5f;
                    mRadius2 = mRadius * mRadius; 	/* for next r**2 compare */
                    float old_to_new = old_to_p - mRadius;
                    /* calc center of new sphere */
                    float recip = 1.0f / old_to_p;

                    float cx = (mRadius * mCenter.x + old_to_new * v.x) * recip;
                    float cy = (mRadius * mCenter.y + old_to_new * v.y) * recip;
                    float cz = (mRadius * mCenter.z + old_to_new * v.z) * recip;

                    mCenter.x = cx;
                    mCenter.y = cy;
                    mCenter.z = cz;
                }
            }
        }
        public static BoundingSphere operator +(BoundingSphere S1, BoundingSphere S2)
        {
            BoundingSphere bs = S1.Clone();
            bs.Add(S2);
            return bs;
        }
        public void Add(BoundingSphere add)
        {
            Vector3 v = add.p - this.p;
            float vn = v.Length;
            // if r2 > vn + r1 the "add" sphere include "this" bounding sphere include 
            if (add.r > vn + this.r) { this.p = add.p; this.r = add.r; }
            // if r1 <= vn + r2 the result is "this" + "add" spheres
            else if (this.r <= vn + add.r)
            {
                this.p = this.p + v / vn * 0.5f * (add.r + vn - this.r);
                this.r = (this.r + vn + add.r) * 0.5f;
            }
            // if r1 > vn + r2 "this" bounding sphere include the "add" sphere , not need calculation
        }
        public override string ToString() { return (p.ToString() + String.Format("r[{0:F2}]\n", r)); }
    }
    
    public class BoundingBox
    {
        public float height = 0f;
        public float width = 0f;
        public float length = 0f;

        public Matrix4 transform = new Matrix4();
        public Vector3 pmin = new Vector3(0, 0, 0);
        public Vector3 pmax = new Vector3(0, 0, 0);
        
       
        /// <summary>
        ///  find the size of box that contain mesh using a transform matrix coordinate system.
        ///  height = dZ , width = dX , length = dY; this.transform are the coordinate system used
        ///  and pmin and pmax are calculated in local
        /// </summary>
        public bool GetSize(GameTriMesh mesh)
        {
            Matrix4 coordsys = new Matrix4();
            return GetSize(mesh, coordsys);
        }

        public bool GetSize(GameTriMesh mesh, Matrix4 coordsys)
        {
            if (mesh.vertex.Length <= 0) return false;
            pmin = new Vector3(float.PositiveInfinity, float.PositiveInfinity, float.PositiveInfinity);
            pmax = new Vector3(float.NegativeInfinity, float.NegativeInfinity, float.NegativeInfinity);
            foreach (Vector3 v in mesh.vertex)
            {
                Vector3 p = v * coordsys;
                if (p.x > pmax.x) pmax.x = p.x;
                if (p.y > pmax.y) pmax.y = p.y;
                if (p.z > pmax.z) pmax.z = p.z;

                if (p.x < pmin.x) pmin.x = p.x;
                if (p.y < pmin.y) pmin.y = p.y;
                if (p.z < pmin.z) pmin.z = p.z;
            }
            transform = coordsys;
            return true;
        }
        public void SizeInGlobalCoordSys(out Vector3 Pmin, out Vector3 Pmax)
        {
            Matrix4 Icoordsys = transform.Inverse();
            Pmin = pmin * Icoordsys;
            Pmax = pmax * Icoordsys;
        }
        public override string ToString()
        {
            return transform.ToString() + "\n" + pmax.ToString() + "\n" + pmin.ToString();
        }

    }
    
    /// <summary>
    ///  A class that rappresent a tipical DirectX mesh data, was used only to store the calculated data.
    ///  GetTBN was used to recalculate tangent and binormal vectors because not all programm have these
    /// </summary>
    public class GameTriMesh
    {
        /*
        vertex = 0,
        weight = 1,
        boneid = 2,
        normal = 3,
        uvw = 4,
        rgblight = 8,
        rgbanim = 9,
        tang = 10,
        binorm = 11,
        meshanim1 = 13,
        meshanim2 = 14
        */
        /// <summary>
        ///  Was used as personal name in mesh files
        /// </summary>
        public string firstname = "";
        /// <summary>
        ///  Was used as group name in mesh files
        /// </summary>
        public string secondname = "";
        /// <summary>
        ///  Was used for appear probability in mesh files
        /// </summary>
        public bool show = true;

        public int faceID = -1; // all faces have the same id (material) , in direcx is AttributeBuffer
        public Face16[] face = null;
        public Vector3[] vertex = null;
        public Vector3[] normal = null;
        public Vector3[] tang = null;  // was recalculated from uvw,normal,vertex
        public Vector3[] binorm =  null;// was recalculated from uvw,normal,vertex 
        public Vector2[] uvw = null;
        public Vector2[] weight = null;
        public VectorByte4[] boneid = null;
        public VectorByte4[] light = null;
        public VectorByte4[] anim = null;
        public VectorByte4[] meshanim1 = null;
        public VectorByte4[] meshanim2 = null;

        public BoundingSphere bsphere = null;

        //public Matrix4x4 transform = new Matrix4x4();
        
        public Vector3 center = new Vector3(0, 0, 0);

        private void updateCenter()
        {
            if (vertex.Length<=0) return;
            center = new Vector3(0, 0, 0);
            foreach (Vector3 v in vertex) center += v;
            center /= vertex.Length;
        }

        public bool GetTBN() { return true; }

        /// <summary>
        /// Initialize new faces and new vertex array.
        /// </summary>
        public GameTriMesh(uint nfaces, uint nverts)
        {
            if (nfaces > 0 & nfaces < 65536 & nverts > 0 & nverts < 65536)
            {
                face = new Face16[nfaces];
                vertex = new Vector3[nverts];
                for (int i = 0; i < nfaces; face[i++] = new Face16(0, 0, 0)) ;
                for (int i = 0; i < nverts; vertex[i++] = new Vector3(0, 0, 0)) ;
            }
            updateCenter();
        }
        public GameTriMesh() { }

        // change the data coordinate system
        public bool ConvertToGame()
        {
            for (int i = 0; i < uvw.Length; i++)
            {
                uvw[i].v = 1f - uvw[i].v;
            }
            for (int i = 0; i < vertex.Length; i++)
            {
                Vector3 p = vertex[i];
                vertex[i].x = -p.x;
                vertex[i].y = p.z;
                vertex[i].z = -p.y;
            }
            for (int i = 0; i < face.Length; i++)
            {
                Face16 f = face[i];
                face[i].I = (ushort)(f.J - 1);
                face[i].J = (ushort)(f.I - 1);
                face[i].K = (ushort)(f.K - 1);
            }
            return true;
        }
        public bool ConvertTo3ds()
        {
            if (uvw!=null) for (int i = 0; i < uvw.Length; i++)
                uvw[i].v = 1f - uvw[i].v;

            if (vertex != null) for (int i = 0; i < vertex.Length; Utils.ConvertTo3ds(ref vertex[i++])) ;
            if (normal != null) for (int i = 0; i < normal.Length; Utils.ConvertTo3ds(ref normal[i++]) );
            if (face != null) for (int i = 0; i < face.Length; Utils.ConvertTo3ds(ref face[i++])) ;
            return true;
        }
        public bool ConvertToMilk()
        {
            return false;
        }

        /// <summary>
        /// the algorithm do these :
        /// 1) Generate a convex hull in 2d using the XY plane
        /// 2) Using Rotation Caliper to get the minimum distance
        /// 3) Build the Bounding Box with this rotation.
        /// </summary>
        public BoundingBox GetMinimumBox()
        {
            updateCenter();
            Matrix4 transform = new Matrix4();
            BoundingBox bbox = new BoundingBox();
            if (!bbox.GetSize(this, transform)) return null;

            Vector3[] hull = new Vector3[vertex.Length];
            int size = ConvexHull2D.chainHull_2D(vertex, ref hull, true);
            Array.Resize(ref hull, size);

            Matrix4 rotation = RotationCaliper.GetMinRotation(hull);
            return bbox;
        }

        public override string ToString()
        {
            int i = 0;
            string str = "";
            str += "FaCeS: " + face.Length + "\n";
            i = 0; while (i < face.Length) str += i + " " + face[i++].ToString();

            str += "VeRtEx: " + vertex.Length + "\n";
            i = 0; while (i < vertex.Length) str += i + " " + vertex[i++].ToString();

            str += "UvW: " + uvw.Length + "\n";
            i = 0; while (i < uvw.Length) str += i + " " + uvw[i++].ToString();

            str += "NoRmAl: " + normal.Length + "\n";
            i = 0; while (i < normal.Length) str += i + " " + normal[i++].ToString();

            str += "WeiGhT: " + weight.Length + "\n";
            str += "BoNeId: " + boneid.Length + "\n";

            return str;
        }
    }


    /// <summary>
    /// Generate a Convex Hull in 2D 
    /// </summary> 
    public static class ConvexHull2D
    {
        private static sbyte Compare(Vector3 a , Vector3 b)
        {
            if (a.x > b.x) return 1;
            else if (a.x < b.x) return -1;
            else if (a.z > b.z) return 1;
            else if (a.z < b.z) return -1;
            return 0;
        }
        private static void quicksort(Vector3[] array, ushort[] index, int left, int right)
        {
            int l = left;
            int r = right;
            int p = (left + right) / 2;

            // 1. Pick a pivot value somewhere in the middle.
            Vector3 pivot = array[index[p]];

            // 2. Loop until pointers meet on the pivot.
            while (l <= r)
            {
                // 3. Find a larger value to the right of the pivot.
                //    If there is non we end up at the pivot.
                while (Compare(array[index[l]], pivot) < 0) l++;

                // 4. Find a smaller value to the left of the pivot.
                //    If there is non we end up at the pivot.
                while (Compare(array[index[r]], pivot) > 0) r--;

                // 5. Check if both pointers are not on the pivot.
                if (l <= r)
                {
                    // 6. Swap both values to the right side.
                    ushort swap = index[l];
                    index[l] = index[r];
                    index[r] = swap;

                    l++;
                    r--;
                }
            }
            // Here's where the pivot value is in the right spot

            // 7. Recursively call the algorithm on the unsorted array 
            //    to the left of the pivot (if exists).
            if (left < r) quicksort(array, index, left, r);

            // 8. Recursively call the algorithm on the unsorted array 
            //    to the right of the pivot (if exists).
            if (l < right) quicksort(array, index, l, right);

            // 9. The algorithm returns when all sub arrays are sorted.
        }


        // Copyright 2001, softSurfer (www.softsurfer.com)
        // This code may be freely used and modified for any purpose
        // providing that this copyright notice is included with it.
        // SoftSurfer makes no warranty for this code, and cannot be held
        // liable for any real or imagined damage resulting from its use.
        // Users of this code must verify correctness for their application.

        // Assume that a class is already given for the object:
        //    Point with coordinates {float x, y;}
        //===================================================================

        // isLeft(): tests if a point is Left|On|Right of an infinite line.
        //    Input:  three points P0, P1, and P2
        //    Return: >0 for P2 left of the line through P0 and P1
        //            =0 for P2 on the line
        //            <0 for P2 right of the line
        //    See: the January 2001 Algorithm on Area of Triangles

        private static float isLeft(Vector3 P0, Vector3 P1, Vector3 P2)
        {
            return (P1.x - P0.x) * (P2.z - P0.z) - (P2.x - P0.x) * (P1.z - P0.z);
        }
        //===================================================================
        // chainHull_2D(): Andrew's monotone chain 2D convex hull algorithm
        //     Input:  P[] = an array of 2D points 
        //                   presorted by increasing x- and y-coordinates
        //             n = the number of points in P[]
        //     Output: H[] = an array of the convex hull vertices (max is n)
        //     Return: the number of points in H[]
        public static int chainHull_2D(Vector3[] P, ref Vector3[] H, bool needresort)
        {
            // the output array H[] will be used as the stack
            int bot = 0, top = (-1);  // indices for bottom and top of the stack
            int i;                // array scan index
            // Get the indices of points with min x-coord and min|max y-coord
            int minmin = 0, minmax;            
            
            int N = P.Length;
            ushort[] index = new ushort[N];
            for (i = 0; i < N; i++) index[i] = (ushort)i;

            
            if (needresort)
            {
                quicksort(P, index, 0, N-1);
            }
            

            float xmin = P[0].x;
            for (i = 1; i < N; i++)
                if (P[i].x != xmin) break;
            minmax = i - 1;
            if (minmax == N - 1)
            {       // degenerate case: all x-coords == xmin
                H[++top] = P[minmin];
                if (P[minmax].z != P[minmin].z) // a nontrivial segment
                    H[++top] = P[minmax];
                H[++top] = P[minmin];           // add polygon endpoint
                return top + 1;
            }

            // Get the indices of points with max x-coord and min|max y-coord
            int maxmin, maxmax = N - 1;
            float xmax = P[N - 1].x;
            for (i = N - 2; i >= 0; i--)
                if (P[i].x != xmax) break;
            maxmin = i + 1;

            // Compute the lower hull on the stack H
            H[++top] = P[minmin];      // push minmin point onto stack
            i = minmax;
            while (++i <= maxmin)
            {
                // the lower line joins P[minmin] with P[maxmin]
                if (isLeft(P[minmin], P[maxmin], P[i]) >= 0 && i < maxmin)
                    continue;          // ignore P[i] above or on the lower line

                while (top > 0)        // there are at least 2 points on the stack
                {
                    // test if P[i] is left of the line at the stack top
                    if (isLeft(H[top - 1], H[top], P[i]) > 0)
                        break;         // P[i] is a new hull vertex
                    else
                        top--;         // pop top point off stack
                }
                H[++top] = P[i];       // push P[i] onto stack
            }

            // Next, compute the upper hull on the stack H above the bottom hull
            if (maxmax != maxmin)      // if distinct xmax points
                H[++top] = P[maxmax];  // push maxmax point onto stack
            bot = top;                 // the bottom point of the upper hull stack
            i = maxmin;
            while (--i >= minmax)
            {
                // the upper line joins P[maxmax] with P[minmax]
                if (isLeft(P[maxmax], P[minmax], P[i]) >= 0 && i > minmax)
                    continue;          // ignore P[i] below or on the upper line

                while (top > bot)    // at least 2 points on the upper stack
                {
                    // test if P[i] is left of the line at the stack top
                    if (isLeft(H[top - 1], H[top], P[i]) > 0)
                        break;         // P[i] is a new hull vertex
                    else
                        top--;         // pop top point off stack
                }
                H[++top] = P[i];       // push P[i] onto stack
            }
            if (minmax != minmin)
                H[++top] = P[minmin];  // push joining endpoint onto stack

            return top + 1;
        }
    }

    /// <summary>
    /// Find minimum distance rotation with "rotation caliper algoritm"
    /// </summary> 
    public static class RotationCaliper
    {
        public static Matrix4 GetMinRotation(Vector3[] hull)
        {
            Matrix4 M = new Matrix4();
            return M;
        }
    }
}