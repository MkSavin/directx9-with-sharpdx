#define WHITE float4(1,1,1,1);
#define BLACK float4(0,0,0,1);
#define BLUE  float4(0,1,0,1);

float4x4 World;
float4x4 Proj;
float4x4 View;

float4 VertexColor = BLACK;
float  Radius;


Texture PlanetBox;

samplerCUBE PlanetBoxSampler = sampler_state 
{ 
   texture = <PlanetBox>; 
   magfilter = LINEAR; 
   minfilter = LINEAR; 
   mipfilter = LINEAR; 
   AddressU = Wrap; 
   AddressV = Wrap; 
   AddressW = Wrap;
};
 


struct VS_OUTPUT
{   
    float4  Position : POSITION;
	float4  Color    : COLOR0;
	float3  TexCoord : TEXCOORD;
};


VS_OUTPUT VShaderBox(float2 inPosition : POSITION )
{
	VS_OUTPUT output = (VS_OUTPUT)0; 
	output.Position = mul(float4(inPosition,0,1), World);
	output.Position = mul(output.Position, View);
	output.Position = mul(output.Position, Proj);
	output.Color = VertexColor;
	output.TexCoord = float3(0,0,0);
	return output;
}

VS_OUTPUT VShaderSphere(float2 inPosition : POSITION )
{
	VS_OUTPUT output = (VS_OUTPUT)0; 
	
	output.Position = mul(float4(inPosition,0,1), World);
	output.Position/= output.Position.w; // homogeinize

	output.TexCoord = normalize(output.Position.xyz);
	
	output.Position.xyz = output.TexCoord * Radius;

	output.Position = mul(output.Position, View);
	output.Position = mul(output.Position, Proj);
	output.Color = VertexColor;

	return output;
}


float4 PShader(float4 Color : COLOR) : COLOR0
{
	return Color;
}

float4 PShaderTextured(float3 inTexCoord : TEXCOORD) : COLOR0
{
	return texCUBE(PlanetBoxSampler, inTexCoord);
}


technique TechniqueBox
{
	pass p0
    {
        VertexShader = compile vs_2_0 VShaderBox();
		PixelShader =  compile ps_2_0 PShader();
		ZEnable = false;
    }
}
technique TechniqueSphere
{
	pass p0
    {
        VertexShader = compile vs_2_0 VShaderSphere();
		PixelShader =  compile ps_2_0 PShaderTextured();
		ZEnable = false;
		FillMode = Solid;
    }
	pass p1
    {
        VertexShader = compile vs_2_0 VShaderSphere();
		PixelShader =  compile ps_2_0 PShader();
		ZEnable = false;
		FillMode = Wireframe;
    }
}