﻿

using System;
using System.Collections.Generic;
using System.Text;

using Engine;
using Engine.Tools;

namespace Game
{
    public class Planet
    {
        Device device;
        TextureCUBE planetmap;
        PatchProgram effect;
        PatchGeometry geometry;
        Font font;

        public CubeTree tree;
        public float Radius;

        public Planet(Device device, float Radius)
        {
            this.device = device;
            this.Radius = Radius;

            geometry = new PatchGeometry(device);
            effect = new PatchProgram(device);
            font = new Font(device, Font.FontName.Arial, 8);

            planetmap = TextureCUBE.FromFilename(device, "EarthCubeMap.dds");
            //planetmap = TextureCUBE.FromFilename(device, Engine.Content.EngineResources.SkyBoxTextureCube);


            tree = new CubeTree(6);
            tree.Side[0].root.RecursiveSplit();
            tree.Side[1].root.RecursiveSplit();
            tree.Side[2].root.RecursiveSplit();
            tree.Side[3].root.RecursiveSplit();
            tree.Side[4].root.RecursiveSplit();
            tree.Side[5].root.RecursiveSplit();
        }

        public void Draw(ICamera camera, bool sphere = false)
        {
            effect.Radius.Value = Radius;
            effect.Proj.Value = camera.Projection;
            effect.View.Value = camera.View;
            effect.PlanetBox.Value = planetmap;

            device.SetVertexDeclaration(geometry.declaration);
            device.SetVertexStream(geometry.vertexbuffer);

            device.renderstates.fillMode = FillMode.Solid;
            device.renderstates.cullMode = Cull.CounterClockwise;


            EffectTechnique technique = sphere ? effect.TechniqueSphere : effect.TechniqueBox;

            foreach (Pass pass in technique)
            {
                for (int i = 0; i < 6; i++)
                {
                    effect.VertexColor.Value = tree.Side[i].Color;
                    effect.VertexColor.SetParam(effect);

                    foreach (Quad quad in tree.Side[i].enumerator)
                    {
                        if (quad.ChildrenFlag == 0)
                        { 
                            // manual update of these value. this because is better don't exist 
                            // from effect.Begin() loop
                            effect.World.Value = quad.transform;
                            effect.World.SetParam(effect);
                            effect.Commit();

                            geometry.DrawQuad(device);


                            //Vector3 proj = Vector3.Project(effect.World.Value.Position, camera);
                            //font.Draw(quad.ToString(), (int)proj.x, (int)proj.y, Color.Black);
                        }
                    }
                }
            }
        }

    }
}
