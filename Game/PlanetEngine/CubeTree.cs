﻿
using System;
using System.Collections.Generic;
using System.Text;

using Engine.Maths;
using Engine.Tools;
using Engine.Quadtree;

namespace Game
{
    public class CubeTree
    {
        public Tree[] Side;

        public CubeTree(int depthcount)
        {
            Side = new Tree[6];
            for (int i = 0; i < 6; i++)
                Side[i] = new Tree(depthcount, (MapFace)i);
            
        }
    }

    public class Tree : QuadTree<Quad>
    {
        public MapFace Orientation;
        public Matrix4 World;
        public Color32 Color;

        public Vector2 Size = new Vector2(20, 20);
        public Vector2 Origin = new Vector2(-10, -10);

        public QuadNodesEnumerator<Quad, Tree> enumerator;

        public QuadRectangletor<Quad, Tree> rectangleSelector;
        public QuadCircletor<Quad, Tree> circleSelector;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="DepthCount">0 : not suddivision, 1 root node, 2 first root suddivision </param>
        /// <param name="orientation"></param>
        public Tree(int DepthCount, MapFace orientation)
            : base(DepthCount)
        {
            this.Orientation = orientation;


            Matrix4 traslation = Matrix4.Translating(0, 0, 10);

            switch (orientation)
            {
                case MapFace.PositiveX:
                   Color = Color32.Red;
                   World = Matrix4.RotationY(UtilsMath.DegreeToRadian(90));
                   break;

                case MapFace.NegativeX:
                    Color = Color32.Mangenta; 
                    World = Matrix4.RotationY(UtilsMath.DegreeToRadian(-90)); 
                    break;

                case MapFace.PositiveY: 
                    Color = Color32.Green; 
                    World = Matrix4.RotationX(UtilsMath.DegreeToRadian(-90)); 
                    break;

                case MapFace.NegativeY:
                    Color = Color32.Yellow; 
                    World = Matrix4.RotationX(UtilsMath.DegreeToRadian(90));
                    break;

                case MapFace.PositiveZ:
                    Color = Color32.Blue;
                    World = Matrix4.Identity;
                    break;

                case MapFace.NegativeZ: 
                    Color = Color32.Cyano;
                    World = Matrix4.RotationY(UtilsMath.DegreeToRadian(180));
                    break;

            }

            World = World * traslation;


            base.root = new Quad(this, orientation);

            enumerator = new QuadNodesEnumerator<Quad, Tree>(base.root);
            circleSelector = new QuadCircletor<Quad, Tree>(base.root, Circle.Empty);
            rectangleSelector = new QuadRectangletor<Quad, Tree>(base.root, RectangleAA.Empty);
        }

    }
}
