﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Engine.Forms;
using Engine.Tools;
using Engine;
using Engine.Maths;
using Engine.Utils;
using Engine.Inputs;

namespace Game
{
    public partial class PlanerRender : GameForm
    {
        Axis axis;
        EffectLine simple;
        Planet planet;
        MyTrackBallWASD trackball;
        InputManager inputs;

        public PlanerRender()
            : base()
        {
            Engine.Content.EngineResources.ContentFolder = @"C:\Users\john\PROJECTS\3dEngine\JohnWhileSharpDX\Renderer\Resources\Content\";
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            Size size = this.ClientSize;

            simple = new EffectLine(render.Device);
            axis = new Axis(render.Device, 10.0f);
            planet = new Planet(render.Device, 10.0f);

            inputs = new InputManager(this);

            trackball = new MyTrackBallWASD(this, inputs.mouse, inputs.keyboard,
                Matrix4.MakeViewLH(new Vector3(10, 10, 10), Vector3.Zero, Vector3.UnitY),
                Matrix4.MakeProjectionAFovYLH(0.1f, 100.0f, base.setting.AspectRation));
        }

        public override void Render(double alpha)
        {
            render.ClearDraw(BackColor);
            render.BeginDraw();

            planet.Draw(trackball, true);

            render.Device.renderstates.ZBufferEnable = false;
            axis.Draw(trackball.Projection * trackball.View, simple);
            render.Device.renderstates.ZBufferEnable = true;

            render.EndDraw();
            render.Present();
        }

        public override void Update(double elapsed)
        {
            inputs.Update(elapsed,true);
        }
    }
}
