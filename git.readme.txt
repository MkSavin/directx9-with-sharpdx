﻿From your new local repo, simply:

> git push --force -u origin master

That will replace the history of your BitBucket master branch by the new one you have done locally.

> rm -rf .git
> git init
> git commit -m "first commit"
> git remote add origin https://johnwhile@bitbucket.org/johnwhile/directx9-with-sharpdx.git
> git push -u origin master


The git fetch command imports commits from a remote repository into your local repo.

> git fetch origin

Hard is to ignore any changes in your local copy. 

> git reset --hard origin/master

Command git pull <remote> fetches the specified remote’s copy of the current branch and immediately merge it into the local copy. This is the same as git fetch <remote> followed by git merge origin/<current-branch>.

> git pull <remote> (origin master)


Add and remove deleted
> git add -u 

############################################## NEW BANCH ##############################################

Create a new Branch
> git branch <BranchName>
> git checkout <BranchName>

Commit and push changes
> git add .
> git commit -m "upload"
> git push origin <BranchName>

Merge the branch in the master without commits
> git checkout master
> git merge --squash <BranchName>
> git add .
> git commit -m "Merged BranchName"

Delete branch
> git branch -d <BranchName>







